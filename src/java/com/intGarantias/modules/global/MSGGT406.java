// Source File Name:   MSGGT406.java

package com.intGarantias.modules.global;

import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.util.Formateo;

import java.util.*;
import java.lang.*;
import org.apache.turbine.util.Log;

public class MSGGT406
{
  public static int g_Max_GT406 = 53;
  //---------------------------------------------------------------------------------------
  public static class Bff_MsgGT406
  {
    public StringBuffer GT406_Seq = new StringBuffer(3 );   //9(03)         3 Secuencia
    public StringBuffer GT406_Cti = new StringBuffer(2 );   //9(02)         5 Tipo Inscripcion
    public StringBuffer GT406_Nti = new StringBuffer(20);   //X(20)        25 Descripcion TIns
    public StringBuffer GT406_Csv = new StringBuffer(3 );   //X(03)        28 Conservador       
    public StringBuffer GT406_Lcl = new StringBuffer(15);   //X(15)        43 Localidad    
    public StringBuffer GT406_Rpo = new StringBuffer(8 );   //9(08)        51 Fecha Repertorio
    public StringBuffer GT406_Foj = new StringBuffer(7 );   //9(07)        58 Fojas          
    public StringBuffer GT406_Nin = new StringBuffer(7 );   //9(07)        65 Numero           
    public StringBuffer GT406_Grd = new StringBuffer(1 );   //9(01)        66 Grado        

    public String getSeq()          {return GT406_Seq.toString();}
    public String getCti()          {return GT406_Cti.toString();}
    public String getNti()          {return GT406_Nti.toString();}
    public String getCsv()          {return GT406_Csv.toString();}
    public String getLcl()          {return GT406_Lcl.toString();}
    public String getRpo()          {return GT406_Rpo.toString();}
    public String getFoj()          {return GT406_Foj.toString();}
    public String getNin()          {return GT406_Nin.toString();}
    public String getGrd()          {return GT406_Grd.toString();}

    public Formateo f = new Formateo();
    public String fgetRpo()         {return f.FormatDate(GT406_Rpo.toString());}
  }
  //---------------------------------------------------------------------------------------
  public static class Buf_MsgGT406
  {
    public StringBuffer GT406_Idr = new StringBuffer(1 );   //X(01)        1 Idr Host
    public StringBuffer GT406_Gti = new StringBuffer(10);   //X(10)       11 Id.Garantia a buscar
    public StringBuffer GT406_Iln = new StringBuffer(7 );   //9(07)       18 Informe
    public StringBuffer GT406_Nro = new StringBuffer(3 );   //9(03)       21 Nro Elementos
    public Vector       GT406_Tag = new Vector();           //X(3498)   3519 Tabla Bienes

    public String getIdr(){return GT406_Idr.toString();}
    public String getGti(){return GT406_Gti.toString();}
    public String getIln(){return GT406_Iln.toString();}
    public String getNro(){return GT406_Nro.toString();}
    public Vector getTag(){return GT406_Tag;}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_MsgGT406 Inicia_MsgGT406()
  {
    Buf_MsgGT406 l_MsgGT406 = new Buf_MsgGT406();
    l_MsgGT406.GT406_Idr.replace(0,l_MsgGT406.GT406_Idr.capacity(), RUTGEN.Blancos(1 ));
    l_MsgGT406.GT406_Gti.replace(0,l_MsgGT406.GT406_Gti.capacity(), RUTGEN.Blancos(10));
    l_MsgGT406.GT406_Iln.replace(0,l_MsgGT406.GT406_Iln.capacity(), RUTGEN.Blancos(7 ));
    l_MsgGT406.GT406_Nro.replace(0,l_MsgGT406.GT406_Nro.capacity(), RUTGEN.Blancos(3 ));
    for (int i=0; i<g_Max_GT406; i++)
        {
          Bff_MsgGT406 Tap = new Bff_MsgGT406();
          Tap.GT406_Seq.replace(0,Tap.GT406_Seq.capacity(), RUTGEN.Blancos(3 ));
          Tap.GT406_Cti.replace(0,Tap.GT406_Cti.capacity(), RUTGEN.Blancos(2 ));
          Tap.GT406_Nti.replace(0,Tap.GT406_Nti.capacity(), RUTGEN.Blancos(20));
          Tap.GT406_Csv.replace(0,Tap.GT406_Csv.capacity(), RUTGEN.Blancos(3 ));
          Tap.GT406_Lcl.replace(0,Tap.GT406_Lcl.capacity(), RUTGEN.Blancos(15));
          Tap.GT406_Rpo.replace(0,Tap.GT406_Rpo.capacity(), RUTGEN.Blancos(8 ));
          Tap.GT406_Foj.replace(0,Tap.GT406_Foj.capacity(), RUTGEN.Blancos(7 ));
          Tap.GT406_Nin.replace(0,Tap.GT406_Nin.capacity(), RUTGEN.Blancos(7 ));
          Tap.GT406_Grd.replace(0,Tap.GT406_Grd.capacity(), RUTGEN.Blancos(1 ));
          l_MsgGT406.GT406_Tag.add(Tap);
        }
    return l_MsgGT406;
  }
  //---------------------------------------------------------------------------------------
  static public Buf_MsgGT406 LSet_A_MsgGT406(String pcStr)
  {
    Buf_MsgGT406 l_MsgGT406 = new Buf_MsgGT406();
    MSGGT406 vMsgGT406 = new MSGGT406();
    Vector vDatos = vMsgGT406.LSet_A_vMsgGT406(pcStr);
    l_MsgGT406 = (MSGGT406.Buf_MsgGT406)vDatos.elementAt(0);
    return l_MsgGT406;
  }
  //---------------------------------------------------------------------------------------
  public Vector LSet_A_vMsgGT406(String pcStr)
  {
    int p=0;
    Vector vec= new Vector();
    Buf_MsgGT406 l_MsgGT406 = new Buf_MsgGT406();
    l_MsgGT406.GT406_Idr.append( pcStr.substring(p, p + l_MsgGT406.GT406_Idr.capacity())); p = p + l_MsgGT406.GT406_Idr.capacity();
    l_MsgGT406.GT406_Gti.append( pcStr.substring(p, p + l_MsgGT406.GT406_Gti.capacity())); p = p + l_MsgGT406.GT406_Gti.capacity();
    l_MsgGT406.GT406_Iln.append( pcStr.substring(p, p + l_MsgGT406.GT406_Iln.capacity())); p = p + l_MsgGT406.GT406_Iln.capacity();
    l_MsgGT406.GT406_Nro.append( pcStr.substring(p, p + l_MsgGT406.GT406_Nro.capacity())); p = p + l_MsgGT406.GT406_Nro.capacity();
    for (int i=0; i<Integer.parseInt(l_MsgGT406.GT406_Nro.toString()); i++)
        {
          Bff_MsgGT406 Tap = new Bff_MsgGT406();
          Tap.GT406_Seq.append(pcStr.substring(p, p + Tap.GT406_Seq.capacity())); p = p + Tap.GT406_Seq.capacity();
          Tap.GT406_Cti.append(pcStr.substring(p, p + Tap.GT406_Cti.capacity())); p = p + Tap.GT406_Cti.capacity();
          Tap.GT406_Nti.append(pcStr.substring(p, p + Tap.GT406_Nti.capacity())); p = p + Tap.GT406_Nti.capacity();
          Tap.GT406_Csv.append(pcStr.substring(p, p + Tap.GT406_Csv.capacity())); p = p + Tap.GT406_Csv.capacity();
          Tap.GT406_Lcl.append(pcStr.substring(p, p + Tap.GT406_Lcl.capacity())); p = p + Tap.GT406_Lcl.capacity();
          Tap.GT406_Rpo.append(pcStr.substring(p, p + Tap.GT406_Rpo.capacity())); p = p + Tap.GT406_Rpo.capacity();
          Tap.GT406_Foj.append(pcStr.substring(p, p + Tap.GT406_Foj.capacity())); p = p + Tap.GT406_Foj.capacity();
          Tap.GT406_Nin.append(pcStr.substring(p, p + Tap.GT406_Nin.capacity())); p = p + Tap.GT406_Nin.capacity();
          Tap.GT406_Grd.append(pcStr.substring(p, p + Tap.GT406_Grd.capacity())); p = p + Tap.GT406_Grd.capacity();
          l_MsgGT406.GT406_Tag.add(Tap);
        }
    vec.add (l_MsgGT406);
    return vec;
  }
  //---------------------------------------------------------------------------------------
  static public String LSet_De_MsgGT406(Buf_MsgGT406 p_MsgGT406)
  {
    String pcStr = "";
    pcStr = pcStr + p_MsgGT406.GT406_Idr.toString();
    pcStr = pcStr + p_MsgGT406.GT406_Gti.toString();
    pcStr = pcStr + p_MsgGT406.GT406_Iln.toString();
    pcStr = pcStr + p_MsgGT406.GT406_Nro.toString();
    for (int i=0; i<p_MsgGT406.GT406_Tag.size(); i++)
        {
          pcStr = pcStr + ((Bff_MsgGT406)p_MsgGT406.GT406_Tag.elementAt(i)).GT406_Seq.toString();
          pcStr = pcStr + ((Bff_MsgGT406)p_MsgGT406.GT406_Tag.elementAt(i)).GT406_Cti.toString();
          pcStr = pcStr + ((Bff_MsgGT406)p_MsgGT406.GT406_Tag.elementAt(i)).GT406_Nti.toString();
          pcStr = pcStr + ((Bff_MsgGT406)p_MsgGT406.GT406_Tag.elementAt(i)).GT406_Csv.toString();
          pcStr = pcStr + ((Bff_MsgGT406)p_MsgGT406.GT406_Tag.elementAt(i)).GT406_Lcl.toString();
          pcStr = pcStr + ((Bff_MsgGT406)p_MsgGT406.GT406_Tag.elementAt(i)).GT406_Rpo.toString();
          pcStr = pcStr + ((Bff_MsgGT406)p_MsgGT406.GT406_Tag.elementAt(i)).GT406_Foj.toString();
          pcStr = pcStr + ((Bff_MsgGT406)p_MsgGT406.GT406_Tag.elementAt(i)).GT406_Nin.toString();
          pcStr = pcStr + ((Bff_MsgGT406)p_MsgGT406.GT406_Tag.elementAt(i)).GT406_Grd.toString();
        }
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
}