// Source File Name:   MSGGT421.java

package com.intGarantias.modules.global;

import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.util.Formateo;

import java.util.*;
import java.lang.*;
import org.apache.turbine.util.Log;

public class MSGGT421
{
  //---------------------------------------------------------------------------------------
  public static class Buf_MsgGT421
  {
    public StringBuffer GT421_Idr = new StringBuffer(1  );   //X(01)
    public StringBuffer GT421_Gti = new StringBuffer(10 );   //X(10)
    public StringBuffer GT421_Seq = new StringBuffer(3  );   //9(03)
    public StringBuffer GT421_Sqd = new StringBuffer(3  );   //9(03)
    public StringBuffer GT421_Fts = new StringBuffer(8  );   //9(08)
    public StringBuffer GT421_Vuf = new StringBuffer(9  );   //9(07)V9(2)
    public StringBuffer GT421_Vtc = new StringBuffer(9  );   //9(07)V9(2)
    public StringBuffer GT421_Tuf = new StringBuffer(15 );   //9(11)V9(4)
    public StringBuffer GT421_Ttc = new StringBuffer(15 );   //9(13)V9(2)
    public StringBuffer GT421_Tps = new StringBuffer(15 );   //9(13)V9(2)
    public StringBuffer GT421_Ith = new StringBuffer(2  );   //X(02)
    public StringBuffer GT421_Dsh = new StringBuffer(30 );   //X(30)
    public StringBuffer GT421_Rs1 = new StringBuffer(5  );   //9(05)
    public StringBuffer GT421_Rs2 = new StringBuffer(3  );   //9(03)
    public StringBuffer GT421_Rds = new StringBuffer(20 );   //X(20)
    public StringBuffer GT421_Faf = new StringBuffer(8  );   //9(08)
    public StringBuffer GT421_Vaf = new StringBuffer(15 );   //9(13)V9(2)
    public StringBuffer GT421_Udd = new StringBuffer(9  );   //9(07)V9(2)
    public StringBuffer GT421_Umd = new StringBuffer(2  );   //X(02)
    public StringBuffer GT421_Umt = new StringBuffer(1  );   //X(01)
    public StringBuffer GT421_Pum = new StringBuffer(15 );   //9(11)V9(4)
    public StringBuffer GT421_Vtu = new StringBuffer(15 );   //9(11)V9(4)
    public StringBuffer GT421_Vbu = new StringBuffer(15 );   //9(11)V9(4)
    public StringBuffer GT421_Plq = new StringBuffer(5  );   //9(03)V9(2)
    public StringBuffer GT421_Vlu = new StringBuffer(15 );   //9(11)V9(4)
    public StringBuffer GT421_Vsu = new StringBuffer(15 );   //9(11)V9(4)
    public StringBuffer GT421_Str = new StringBuffer(30 );   //X(30)
    public StringBuffer GT421_Mft = new StringBuffer(5  );   //9(05)
    public StringBuffer GT421_Mpf = new StringBuffer(5  );   //9(05)
    public StringBuffer GT421_Mex = new StringBuffer(5  );   //9(05)
    public StringBuffer GT421_Fma = new StringBuffer(15 );   //X(15)
    public StringBuffer GT421_Dst = new StringBuffer(15 );   //X(15)
    public StringBuffer GT421_Uso = new StringBuffer(15 );   //X(15)
    public StringBuffer GT421_Tpe = new StringBuffer(15 );   //X(15)
    public StringBuffer GT421_Obt = new StringBuffer(9  );   //9(09)
    public StringBuffer GT421_Ovb = new StringBuffer(9  );   //9(09)

    public String getIdr()         {return GT421_Idr.toString();}
    public String getGti()         {return GT421_Gti.toString();}
    public String getSeq()         {return GT421_Seq.toString();}
    public String getSqd()         {return GT421_Sqd.toString();}
    public String getFts()         {return GT421_Fts.toString();}
    public String getVuf()         {return GT421_Vuf.toString();}
    public String getVtc()         {return GT421_Vtc.toString();}
    public String getTuf()         {return GT421_Tuf.toString();}
    public String getTtc()         {return GT421_Ttc.toString();}
    public String getTps()         {return GT421_Tps.toString();}
    public String getIth()         {return GT421_Ith.toString();}
    public String getDsh()         {return GT421_Dsh.toString();}
    public String getRs1()         {return GT421_Rs1.toString();}
    public String getRs2()         {return GT421_Rs2.toString();}
    public String getRds()         {return GT421_Rds.toString();}
    public String getFaf()         {return GT421_Faf.toString();}
    public String getVaf()         {return GT421_Vaf.toString();}
    public String getUdd()         {return GT421_Udd.toString();}
    public String getUmd()         {return GT421_Umd.toString();}
    public String getUmt()         {return GT421_Umt.toString();}
    public String getPum()         {return GT421_Pum.toString();}
    public String getVtu()         {return GT421_Vtu.toString();}
    public String getVbu()         {return GT421_Vbu.toString();}
    public String getPlq()         {return GT421_Plq.toString();}
    public String getVlu()         {return GT421_Vlu.toString();}
    public String getVsu()         {return GT421_Vsu.toString();}
    public String getStr()         {return GT421_Str.toString();}
    public String getMft()         {return GT421_Mft.toString();}
    public String getMpf()         {return GT421_Mpf.toString();}
    public String getMex()         {return GT421_Mex.toString();}
    public String getFma()         {return GT421_Fma.toString();}
    public String getDst()         {return GT421_Dst.toString();}
    public String getUso()         {return GT421_Uso.toString();}
    public String getTpe()         {return GT421_Tpe.toString();}
    public String getObt()         {return GT421_Obt.toString();}
    public String getOvb()         {return GT421_Ovb.toString();}

    public Formateo f = new Formateo();
    public String fgetFts()        {return f.FormatDate(getFts());}
    public String fgetVuf()        {return f.Formato(getVuf(),"9.999.999",2,2,',');}
    public String fgetVtc()        {return f.Formato(getVtc(),"9.999.999",2,2,',');}
    public String fgetTuf()        {return f.Formato(getTuf(),"99.999.999.999",4,4,',');}
    public String fgetTtc()        {return f.Formato(getTtc(),"99.999.999.999",2,2,',');}
    public String fgetTps()        {return f.Formato(getTps(),"99.999.999.999",2,0,',');}
    public String fgetFaf()        {return f.FormatDate(getFaf());}
    public String fgetVaf()        {return f.Formato(getVaf(),"9.999.999.999.999",2,0,',');}
    public String fgetUdd()        {return f.Formato(getUdd(),"9.999.999",2,2,',');}
    public String fgetPum()
                                   {
                                     int Dec = 0;
                                     if (getUmt().equals("P"))  { Dec = 0; }
                                     if (getUmt().equals("U"))  { Dec = 4; }
                                     if (getUmt().equals("D"))  { Dec = 2; }
                                     return f.Formato(getPum(),"99.999.999.999",4,Dec,',');
                                   }
    public String fgetVtu()
                                   {
                                     int Dec = 0;
                                     if (getUmt().equals("P"))  { Dec = 0; }
                                     if (getUmt().equals("U"))  { Dec = 4; }
                                     if (getUmt().equals("D"))  { Dec = 2; }
                                     return f.Formato(getVtu(),"99.999.999.999",4,Dec,',');
                                   }
    public String fgetVbu()
                                   {
                                     int Dec = 0;
                                     if (getUmt().equals("P"))  { Dec = 0; }
                                     if (getUmt().equals("U"))  { Dec = 4; }
                                     if (getUmt().equals("D"))  { Dec = 2; }
                                     return f.Formato(getVbu(),"99.999.999.999",4,Dec,',');
                                   }
    public String fgetPlq()        {return f.Formato(getPlq(),"999",2,2,',');}
    public String fgetVlu()
                                   {
                                     int Dec = 0;
                                     if (getUmt().equals("P"))  { Dec = 0; }
                                     if (getUmt().equals("U"))  { Dec = 4; }
                                     if (getUmt().equals("D"))  { Dec = 2; }
                                     return f.Formato(getVlu(),"99.999.999.999",4,Dec,',');
                                   }
    public String fgetVsu()
                                   {
                                     int Dec = 0;
                                     if (getUmt().equals("P"))  { Dec = 0; }
                                     if (getUmt().equals("U"))  { Dec = 4; }
                                     if (getUmt().equals("D"))  { Dec = 2; }
                                     return f.Formato(getVsu(),"99.999.999.999",4,Dec,',');
                                   }
    public String fgetMft()        {return f.Formato(getMft(),"99.999",0,0,',');}
    public String fgetMpf()        {return f.Formato(getMpf(),"99.999",0,0,',');}
    public String fgetMex()        {return f.Formato(getMex(),"99.999",0,0,',');}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_MsgGT421 Inicia_MsgGT421 ()
  {
    Buf_MsgGT421 l_MsgGT421 = new Buf_MsgGT421();
    l_MsgGT421.GT421_Idr.replace(0, l_MsgGT421.GT421_Idr.capacity(), RUTGEN.Blancos(1  ));
    l_MsgGT421.GT421_Gti.replace(0, l_MsgGT421.GT421_Gti.capacity(), RUTGEN.Blancos(10 ));
    l_MsgGT421.GT421_Seq.replace(0, l_MsgGT421.GT421_Seq.capacity(), RUTGEN.Blancos(3  ));
    l_MsgGT421.GT421_Sqd.replace(0, l_MsgGT421.GT421_Sqd.capacity(), RUTGEN.Blancos(3  ));
    l_MsgGT421.GT421_Fts.replace(0, l_MsgGT421.GT421_Fts.capacity(), RUTGEN.Blancos(8  ));
    l_MsgGT421.GT421_Vuf.replace(0, l_MsgGT421.GT421_Vuf.capacity(), RUTGEN.Blancos(9  ));
    l_MsgGT421.GT421_Vtc.replace(0, l_MsgGT421.GT421_Vtc.capacity(), RUTGEN.Blancos(9  ));
    l_MsgGT421.GT421_Tuf.replace(0, l_MsgGT421.GT421_Tuf.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT421.GT421_Ttc.replace(0, l_MsgGT421.GT421_Ttc.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT421.GT421_Tps.replace(0, l_MsgGT421.GT421_Tps.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT421.GT421_Ith.replace(0, l_MsgGT421.GT421_Ith.capacity(), RUTGEN.Blancos(2  ));
    l_MsgGT421.GT421_Dsh.replace(0, l_MsgGT421.GT421_Dsh.capacity(), RUTGEN.Blancos(30 ));
    l_MsgGT421.GT421_Rs1.replace(0, l_MsgGT421.GT421_Rs1.capacity(), RUTGEN.Blancos(5  ));
    l_MsgGT421.GT421_Rs2.replace(0, l_MsgGT421.GT421_Rs2.capacity(), RUTGEN.Blancos(3  ));
    l_MsgGT421.GT421_Rds.replace(0, l_MsgGT421.GT421_Rds.capacity(), RUTGEN.Blancos(20 ));
    l_MsgGT421.GT421_Faf.replace(0, l_MsgGT421.GT421_Faf.capacity(), RUTGEN.Blancos(8  ));
    l_MsgGT421.GT421_Vaf.replace(0, l_MsgGT421.GT421_Vaf.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT421.GT421_Udd.replace(0, l_MsgGT421.GT421_Udd.capacity(), RUTGEN.Blancos(9  ));
    l_MsgGT421.GT421_Umd.replace(0, l_MsgGT421.GT421_Umd.capacity(), RUTGEN.Blancos(2  ));
    l_MsgGT421.GT421_Umt.replace(0, l_MsgGT421.GT421_Umt.capacity(), RUTGEN.Blancos(1  ));
    l_MsgGT421.GT421_Pum.replace(0, l_MsgGT421.GT421_Pum.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT421.GT421_Vtu.replace(0, l_MsgGT421.GT421_Vtu.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT421.GT421_Vbu.replace(0, l_MsgGT421.GT421_Vbu.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT421.GT421_Plq.replace(0, l_MsgGT421.GT421_Plq.capacity(), RUTGEN.Blancos(5  ));
    l_MsgGT421.GT421_Vlu.replace(0, l_MsgGT421.GT421_Vlu.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT421.GT421_Vsu.replace(0, l_MsgGT421.GT421_Vsu.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT421.GT421_Str.replace(0, l_MsgGT421.GT421_Str.capacity(), RUTGEN.Blancos(30 ));
    l_MsgGT421.GT421_Mft.replace(0, l_MsgGT421.GT421_Mft.capacity(), RUTGEN.Blancos(5  ));
    l_MsgGT421.GT421_Mpf.replace(0, l_MsgGT421.GT421_Mpf.capacity(), RUTGEN.Blancos(5  ));
    l_MsgGT421.GT421_Mex.replace(0, l_MsgGT421.GT421_Mex.capacity(), RUTGEN.Blancos(5  ));
    l_MsgGT421.GT421_Fma.replace(0, l_MsgGT421.GT421_Fma.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT421.GT421_Dst.replace(0, l_MsgGT421.GT421_Dst.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT421.GT421_Uso.replace(0, l_MsgGT421.GT421_Uso.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT421.GT421_Tpe.replace(0, l_MsgGT421.GT421_Tpe.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT421.GT421_Obt.replace(0, l_MsgGT421.GT421_Obt.capacity(), RUTGEN.Blancos(9  ));
    l_MsgGT421.GT421_Ovb.replace(0, l_MsgGT421.GT421_Ovb.capacity(), RUTGEN.Blancos(9  ));
    return l_MsgGT421;
  }
  //---------------------------------------------------------------------------------------
  public static Buf_MsgGT421 LSet_A_MsgGT421(String pcStr)
  {
    Buf_MsgGT421 l_MsgGT421 = new Buf_MsgGT421();
    Vector vDatos = LSet_A_vMsgGT421(pcStr);
    l_MsgGT421 = (Buf_MsgGT421)vDatos.elementAt(0);
    return l_MsgGT421;
  }
  //---------------------------------------------------------------------------------------
  public static Vector LSet_A_vMsgGT421(String pcStr)
  {
    Vector vec = new Vector();
    int p = 0;
    Buf_MsgGT421 l_MsgGT421 = new Buf_MsgGT421();
    l_MsgGT421.GT421_Idr.replace(0, l_MsgGT421.GT421_Idr.capacity(), pcStr.substring(p, p + l_MsgGT421.GT421_Idr.capacity())); p = p + l_MsgGT421.GT421_Idr.capacity();
    l_MsgGT421.GT421_Gti.replace(0, l_MsgGT421.GT421_Gti.capacity(), pcStr.substring(p, p + l_MsgGT421.GT421_Gti.capacity())); p = p + l_MsgGT421.GT421_Gti.capacity();
    l_MsgGT421.GT421_Seq.replace(0, l_MsgGT421.GT421_Seq.capacity(), pcStr.substring(p, p + l_MsgGT421.GT421_Seq.capacity())); p = p + l_MsgGT421.GT421_Seq.capacity();
    l_MsgGT421.GT421_Sqd.replace(0, l_MsgGT421.GT421_Sqd.capacity(), pcStr.substring(p, p + l_MsgGT421.GT421_Sqd.capacity())); p = p + l_MsgGT421.GT421_Sqd.capacity();
    l_MsgGT421.GT421_Fts.replace(0, l_MsgGT421.GT421_Fts.capacity(), pcStr.substring(p, p + l_MsgGT421.GT421_Fts.capacity())); p = p + l_MsgGT421.GT421_Fts.capacity();
    l_MsgGT421.GT421_Vuf.replace(0, l_MsgGT421.GT421_Vuf.capacity(), pcStr.substring(p, p + l_MsgGT421.GT421_Vuf.capacity())); p = p + l_MsgGT421.GT421_Vuf.capacity();
    l_MsgGT421.GT421_Vtc.replace(0, l_MsgGT421.GT421_Vtc.capacity(), pcStr.substring(p, p + l_MsgGT421.GT421_Vtc.capacity())); p = p + l_MsgGT421.GT421_Vtc.capacity();
    l_MsgGT421.GT421_Tuf.replace(0, l_MsgGT421.GT421_Tuf.capacity(), pcStr.substring(p, p + l_MsgGT421.GT421_Tuf.capacity())); p = p + l_MsgGT421.GT421_Tuf.capacity();
    l_MsgGT421.GT421_Ttc.replace(0, l_MsgGT421.GT421_Ttc.capacity(), pcStr.substring(p, p + l_MsgGT421.GT421_Ttc.capacity())); p = p + l_MsgGT421.GT421_Ttc.capacity();
    l_MsgGT421.GT421_Tps.replace(0, l_MsgGT421.GT421_Tps.capacity(), pcStr.substring(p, p + l_MsgGT421.GT421_Tps.capacity())); p = p + l_MsgGT421.GT421_Tps.capacity();
    l_MsgGT421.GT421_Ith.replace(0, l_MsgGT421.GT421_Ith.capacity(), pcStr.substring(p, p + l_MsgGT421.GT421_Ith.capacity())); p = p + l_MsgGT421.GT421_Ith.capacity();
    l_MsgGT421.GT421_Dsh.replace(0, l_MsgGT421.GT421_Dsh.capacity(), pcStr.substring(p, p + l_MsgGT421.GT421_Dsh.capacity())); p = p + l_MsgGT421.GT421_Dsh.capacity();
    l_MsgGT421.GT421_Rs1.replace(0, l_MsgGT421.GT421_Rs1.capacity(), pcStr.substring(p, p + l_MsgGT421.GT421_Rs1.capacity())); p = p + l_MsgGT421.GT421_Rs1.capacity();
    l_MsgGT421.GT421_Rs2.replace(0, l_MsgGT421.GT421_Rs2.capacity(), pcStr.substring(p, p + l_MsgGT421.GT421_Rs2.capacity())); p = p + l_MsgGT421.GT421_Rs2.capacity();
    l_MsgGT421.GT421_Rds.replace(0, l_MsgGT421.GT421_Rds.capacity(), pcStr.substring(p, p + l_MsgGT421.GT421_Rds.capacity())); p = p + l_MsgGT421.GT421_Rds.capacity();
    l_MsgGT421.GT421_Faf.replace(0, l_MsgGT421.GT421_Faf.capacity(), pcStr.substring(p, p + l_MsgGT421.GT421_Faf.capacity())); p = p + l_MsgGT421.GT421_Faf.capacity();
    l_MsgGT421.GT421_Vaf.replace(0, l_MsgGT421.GT421_Vaf.capacity(), pcStr.substring(p, p + l_MsgGT421.GT421_Vaf.capacity())); p = p + l_MsgGT421.GT421_Vaf.capacity();
    l_MsgGT421.GT421_Udd.replace(0, l_MsgGT421.GT421_Udd.capacity(), pcStr.substring(p, p + l_MsgGT421.GT421_Udd.capacity())); p = p + l_MsgGT421.GT421_Udd.capacity();
    l_MsgGT421.GT421_Umd.replace(0, l_MsgGT421.GT421_Umd.capacity(), pcStr.substring(p, p + l_MsgGT421.GT421_Umd.capacity())); p = p + l_MsgGT421.GT421_Umd.capacity();
    l_MsgGT421.GT421_Umt.replace(0, l_MsgGT421.GT421_Umt.capacity(), pcStr.substring(p, p + l_MsgGT421.GT421_Umt.capacity())); p = p + l_MsgGT421.GT421_Umt.capacity();
    l_MsgGT421.GT421_Pum.replace(0, l_MsgGT421.GT421_Pum.capacity(), pcStr.substring(p, p + l_MsgGT421.GT421_Pum.capacity())); p = p + l_MsgGT421.GT421_Pum.capacity();
    l_MsgGT421.GT421_Vtu.replace(0, l_MsgGT421.GT421_Vtu.capacity(), pcStr.substring(p, p + l_MsgGT421.GT421_Vtu.capacity())); p = p + l_MsgGT421.GT421_Vtu.capacity();
    l_MsgGT421.GT421_Vbu.replace(0, l_MsgGT421.GT421_Vbu.capacity(), pcStr.substring(p, p + l_MsgGT421.GT421_Vbu.capacity())); p = p + l_MsgGT421.GT421_Vbu.capacity();
    l_MsgGT421.GT421_Plq.replace(0, l_MsgGT421.GT421_Plq.capacity(), pcStr.substring(p, p + l_MsgGT421.GT421_Plq.capacity())); p = p + l_MsgGT421.GT421_Plq.capacity();
    l_MsgGT421.GT421_Vlu.replace(0, l_MsgGT421.GT421_Vlu.capacity(), pcStr.substring(p, p + l_MsgGT421.GT421_Vlu.capacity())); p = p + l_MsgGT421.GT421_Vlu.capacity();
    l_MsgGT421.GT421_Vsu.replace(0, l_MsgGT421.GT421_Vsu.capacity(), pcStr.substring(p, p + l_MsgGT421.GT421_Vsu.capacity())); p = p + l_MsgGT421.GT421_Vsu.capacity();
    l_MsgGT421.GT421_Str.replace(0, l_MsgGT421.GT421_Str.capacity(), pcStr.substring(p, p + l_MsgGT421.GT421_Str.capacity())); p = p + l_MsgGT421.GT421_Str.capacity();
    l_MsgGT421.GT421_Mft.replace(0, l_MsgGT421.GT421_Mft.capacity(), pcStr.substring(p, p + l_MsgGT421.GT421_Mft.capacity())); p = p + l_MsgGT421.GT421_Mft.capacity();
    l_MsgGT421.GT421_Mpf.replace(0, l_MsgGT421.GT421_Mpf.capacity(), pcStr.substring(p, p + l_MsgGT421.GT421_Mpf.capacity())); p = p + l_MsgGT421.GT421_Mpf.capacity();
    l_MsgGT421.GT421_Mex.replace(0, l_MsgGT421.GT421_Mex.capacity(), pcStr.substring(p, p + l_MsgGT421.GT421_Mex.capacity())); p = p + l_MsgGT421.GT421_Mex.capacity();
    l_MsgGT421.GT421_Fma.replace(0, l_MsgGT421.GT421_Fma.capacity(), pcStr.substring(p, p + l_MsgGT421.GT421_Fma.capacity())); p = p + l_MsgGT421.GT421_Fma.capacity();
    l_MsgGT421.GT421_Dst.replace(0, l_MsgGT421.GT421_Dst.capacity(), pcStr.substring(p, p + l_MsgGT421.GT421_Dst.capacity())); p = p + l_MsgGT421.GT421_Dst.capacity();
    l_MsgGT421.GT421_Uso.replace(0, l_MsgGT421.GT421_Uso.capacity(), pcStr.substring(p, p + l_MsgGT421.GT421_Uso.capacity())); p = p + l_MsgGT421.GT421_Uso.capacity();
    l_MsgGT421.GT421_Tpe.replace(0, l_MsgGT421.GT421_Tpe.capacity(), pcStr.substring(p, p + l_MsgGT421.GT421_Tpe.capacity())); p = p + l_MsgGT421.GT421_Tpe.capacity();
    l_MsgGT421.GT421_Obt.replace(0, l_MsgGT421.GT421_Obt.capacity(), pcStr.substring(p, p + l_MsgGT421.GT421_Obt.capacity())); p = p + l_MsgGT421.GT421_Obt.capacity();
    l_MsgGT421.GT421_Ovb.replace(0, l_MsgGT421.GT421_Ovb.capacity(), pcStr.substring(p, p + l_MsgGT421.GT421_Ovb.capacity())); p = p + l_MsgGT421.GT421_Ovb.capacity();
    vec.add (l_MsgGT421);
    return vec;
  }
  //---------------------------------------------------------------------------------------
  public static String LSet_De_MsgGT421 (Buf_MsgGT421 p_MsgGT421)
  {
    String pcStr = "";
    pcStr = pcStr + p_MsgGT421.GT421_Idr.toString();
    pcStr = pcStr + p_MsgGT421.GT421_Gti.toString();
    pcStr = pcStr + p_MsgGT421.GT421_Seq.toString();
    pcStr = pcStr + p_MsgGT421.GT421_Sqd.toString();
    pcStr = pcStr + p_MsgGT421.GT421_Fts.toString();
    pcStr = pcStr + p_MsgGT421.GT421_Vuf.toString();
    pcStr = pcStr + p_MsgGT421.GT421_Vtc.toString();
    pcStr = pcStr + p_MsgGT421.GT421_Tuf.toString();
    pcStr = pcStr + p_MsgGT421.GT421_Ttc.toString();
    pcStr = pcStr + p_MsgGT421.GT421_Tps.toString();
    pcStr = pcStr + p_MsgGT421.GT421_Ith.toString();
    pcStr = pcStr + p_MsgGT421.GT421_Dsh.toString();
    pcStr = pcStr + p_MsgGT421.GT421_Rs1.toString();
    pcStr = pcStr + p_MsgGT421.GT421_Rs2.toString();
    pcStr = pcStr + p_MsgGT421.GT421_Rds.toString();
    pcStr = pcStr + p_MsgGT421.GT421_Faf.toString();
    pcStr = pcStr + p_MsgGT421.GT421_Vaf.toString();
    pcStr = pcStr + p_MsgGT421.GT421_Udd.toString();
    pcStr = pcStr + p_MsgGT421.GT421_Umd.toString();
    pcStr = pcStr + p_MsgGT421.GT421_Umt.toString();
    pcStr = pcStr + p_MsgGT421.GT421_Pum.toString();
    pcStr = pcStr + p_MsgGT421.GT421_Vtu.toString();
    pcStr = pcStr + p_MsgGT421.GT421_Vbu.toString();
    pcStr = pcStr + p_MsgGT421.GT421_Plq.toString();
    pcStr = pcStr + p_MsgGT421.GT421_Vlu.toString();
    pcStr = pcStr + p_MsgGT421.GT421_Vsu.toString();
    pcStr = pcStr + p_MsgGT421.GT421_Str.toString();
    pcStr = pcStr + p_MsgGT421.GT421_Mft.toString();
    pcStr = pcStr + p_MsgGT421.GT421_Mpf.toString();
    pcStr = pcStr + p_MsgGT421.GT421_Mex.toString();
    pcStr = pcStr + p_MsgGT421.GT421_Fma.toString();
    pcStr = pcStr + p_MsgGT421.GT421_Dst.toString();
    pcStr = pcStr + p_MsgGT421.GT421_Uso.toString();
    pcStr = pcStr + p_MsgGT421.GT421_Tpe.toString();
    pcStr = pcStr + p_MsgGT421.GT421_Obt.toString();
    pcStr = pcStr + p_MsgGT421.GT421_Ovb.toString();
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
}