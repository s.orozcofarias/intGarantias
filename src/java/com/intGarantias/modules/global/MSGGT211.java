// Source File Name:   MSGGT211.java

package com.intGarantias.modules.global;

import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.util.Formateo;

import java.util.*;
import java.lang.*;
import org.apache.turbine.util.Log;

public class MSGGT211
{
  public static int g_Max_GT211 = 17;
  //---------------------------------------------------------------------------------------
  public static class Bff_MsgGT211
  {
    public StringBuffer Tmt_Est = new StringBuffer(5 );       //X(05)         5  Estado                      
    public StringBuffer Tmt_Suc = new StringBuffer(3 );       //9(03)         8  Sucursal                    
    public StringBuffer Tmt_Nev = new StringBuffer(7 );       //9(07)        15  Numero Evento               
    public StringBuffer Tmt_Trt = new StringBuffer(5 );       //X(05)        20  Tipo Tramite/UNICO Estado   
    public StringBuffer Evt_Eje = new StringBuffer(4 );       //9(04)        24  Ejecutivo                   
    public StringBuffer Tmt_Fen = new StringBuffer(8 );       //9(08)        32  Fecha Envio                 
    public StringBuffer Tmt_Hra = new StringBuffer(6 );       //9(06)        38  Fecha Envio                 
    public StringBuffer Evt_Cli = new StringBuffer(10);       //X(10)        48  RUT Cliente                 
    public StringBuffer Evt_Ncl = new StringBuffer(40);       //X(40)        88  Nombre Cliente              
    public StringBuffer Evt_Sis = new StringBuffer(3 );       //X(03)        91  Sistema                     
    public StringBuffer Evt_Ncn = new StringBuffer(7 );       //9(07)        98  Contrato                    
    public StringBuffer Evt_Dcn = new StringBuffer(5 );       //X(05)       103  Codigo Contrato             
    public StringBuffer Evt_Ggr = new StringBuffer(3 );       //X(03)       106  Sistema                     
    public StringBuffer Evt_Dsg = new StringBuffer(30);       //X(30)       136  Nombre Contrato             
    public StringBuffer Evt_Mnd = new StringBuffer(3 );       //9(03)       139  Moneda                      
    public StringBuffer Evt_Trj = new StringBuffer(3 );       //X(03)       142  Reajustabilidad             
    public StringBuffer Evt_Vle = new StringBuffer(15);       //9(11)V9(4)  157  Valor Estimado              
    public StringBuffer Evt_Dcm = new StringBuffer(1 );       //9(01)       158  Decimales                   
    public StringBuffer Tmt_Pgm = new StringBuffer(8 );       //X(08)       166  Programa VB Servidor        
    public StringBuffer Evt_Ttr = new StringBuffer(5 );       //X(05)       171  Transaccion                 
    public StringBuffer Tmt_Abg = new StringBuffer(4 );       //9(04)       175  Tasador                     
    public StringBuffer Evt_Nej = new StringBuffer(25);       //X(25)       200  Nombre Ejecutivo               

    public String getTEst()      {return Tmt_Est.toString();}
    public String getTSuc()      {return Tmt_Suc.toString();}
    public String getTNev()      {return Tmt_Nev.toString();}
    public String getTTrt()      {return Tmt_Trt.toString();}
    public String getEEje()      {return Evt_Eje.toString();}
    public String getTFen()      {return Tmt_Fen.toString();}
    public String getTHra()      {return Tmt_Hra.toString();}
    public String getECli()      {return Evt_Cli.toString();}
    public String getENcl()      {return Evt_Ncl.toString();}
    public String getESis()      {return Evt_Sis.toString();}
    public String getENcn()      {return Evt_Ncn.toString();}
    public String getEDcn()      {return Evt_Dcn.toString();}
    public String getEGgr()      {return Evt_Ggr.toString();}
    public String getEDsg()      {return Evt_Dsg.toString();}
    public String getEMnd()      {return Evt_Mnd.toString();}
    public String getETrj()      {return Evt_Trj.toString();}
    public String getEVle()      {return Evt_Vle.toString();}
    public String getEDcm()      {return Evt_Dcm.toString();}
    public String getTPgm()      {return Tmt_Pgm.toString();}
    public String getETtr()      {return Evt_Ttr.toString();}
    public String getTAbg()      {return Tmt_Abg.toString();}
    public String getENej()      {return Evt_Nej.toString();}

    public Formateo f = new Formateo();
    public String fgetEst()      {return Tmt_Est.toString();}
    public String fgetSuc()      {return Tmt_Suc.toString();}
    public String fgetNev()      {return Tmt_Nev.toString();}
    public String fgetTrt()      {return Tmt_Trt.toString();}
    public String fgetEje()      {return Evt_Eje.toString();}
    public String fgetFen()      {return f.FormatDate(Tmt_Fen.toString());}
    public String fgetHra()      {
    	                           String dato = Tmt_Hra.toString().substring(0,2) + ":" + Tmt_Hra.toString().substring(2,4) + ":" + Tmt_Hra.toString().substring(4);
    	                           return dato;
    	                         }
    public String fgetCli()      {return f.fmtRut(Evt_Cli.toString());}
    public String fgetNcl()      {return Evt_Ncl.toString();}
    public String fgetOpe()      {
    	                           String dato = Evt_Sis.toString() + "-" + Evt_Dcn.toString() + "-" + Evt_Ncn.toString();
    	                           return dato;
                                 }
    public String fgetGgr()      {return Evt_Ggr.toString();}
    public String fgetDsg()      {return Evt_Dsg.toString();}
    public String fgetTrj()      {return Evt_Trj.toString().trim() + "-" + Evt_Mnd.toString();}
    public String fgetVle()      {return f.Formato(Evt_Vle.toString(),"99.999.999.999",4,2,',');}
    public String fgetDcm()      {return Evt_Dcm.toString();}
    public String fgetPgm()      {return Tmt_Pgm.toString();}
    public String fgetTtr()      {return Evt_Ttr.toString();}
    public String fgetAbg()      {
    	                           if (Tmt_Abg.toString().equals("0000") )
                                      return "N/A";
    	                           else	
                                      return Tmt_Abg.toString();
                                 }
    public String egetVle()      {return f.Formato(Evt_Vle.toString(),"99.999.999.999",4,0,',');}
  }
  //---------------------------------------------------------------------------------------
  public static class Buf_MsgGT211
  {
    public StringBuffer GT211_Idr = new StringBuffer(1 );
    public StringBuffer GT211_Tpr = new StringBuffer(1 );
    public StringBuffer GT211_Ejh = new StringBuffer(4 );
    public StringBuffer GT211_Nro = new StringBuffer(3 );
    public Vector       GT211_Tab = new Vector();

    public String getIdr()         {return GT211_Idr.toString();}
    public String getTpr()         {return GT211_Tpr.toString();}
    public String getEjh()         {return GT211_Ejh.toString();}
    public String getNro()         {return GT211_Nro.toString();}
    public Vector getTab()         {return GT211_Tab;}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_MsgGT211 Inicia_MsgGT211()
  {
    Buf_MsgGT211 l_MsgGT211 = new Buf_MsgGT211();
    l_MsgGT211.GT211_Idr.replace(0, l_MsgGT211.GT211_Idr.capacity(), RUTGEN.Blancos(1 ));
    l_MsgGT211.GT211_Tpr.replace(0, l_MsgGT211.GT211_Tpr.capacity(), RUTGEN.Blancos(1 ));
    l_MsgGT211.GT211_Ejh.replace(0, l_MsgGT211.GT211_Ejh.capacity(), RUTGEN.Blancos(4 ));
    l_MsgGT211.GT211_Nro.replace(0, l_MsgGT211.GT211_Nro.capacity(), RUTGEN.Blancos(3 ));
    int p = 0;
    for (int i=0; i<g_Max_GT211; i++)
        {
          Bff_MsgGT211 Tap = new Bff_MsgGT211();
          Tap.Tmt_Est.replace(0, Tap.Tmt_Est.capacity(), RUTGEN.Blancos(5 ));
          Tap.Tmt_Suc.replace(0, Tap.Tmt_Suc.capacity(), RUTGEN.Blancos(3 ));
          Tap.Tmt_Nev.replace(0, Tap.Tmt_Nev.capacity(), RUTGEN.Blancos(7 ));
          Tap.Tmt_Trt.replace(0, Tap.Tmt_Trt.capacity(), RUTGEN.Blancos(5 ));
          Tap.Evt_Eje.replace(0, Tap.Evt_Eje.capacity(), RUTGEN.Blancos(4 ));
          Tap.Tmt_Fen.replace(0, Tap.Tmt_Fen.capacity(), RUTGEN.Blancos(8 ));
          Tap.Tmt_Hra.replace(0, Tap.Tmt_Hra.capacity(), RUTGEN.Blancos(6 ));
          Tap.Evt_Cli.replace(0, Tap.Evt_Cli.capacity(), RUTGEN.Blancos(10));
          Tap.Evt_Ncl.replace(0, Tap.Evt_Ncl.capacity(), RUTGEN.Blancos(40));
          Tap.Evt_Sis.replace(0, Tap.Evt_Sis.capacity(), RUTGEN.Blancos(3 ));
          Tap.Evt_Ncn.replace(0, Tap.Evt_Ncn.capacity(), RUTGEN.Blancos(7 ));
          Tap.Evt_Dcn.replace(0, Tap.Evt_Dcn.capacity(), RUTGEN.Blancos(5 ));
          Tap.Evt_Ggr.replace(0, Tap.Evt_Ggr.capacity(), RUTGEN.Blancos(3 ));
          Tap.Evt_Dsg.replace(0, Tap.Evt_Dsg.capacity(), RUTGEN.Blancos(30));
          Tap.Evt_Mnd.replace(0, Tap.Evt_Mnd.capacity(), RUTGEN.Blancos(3 ));
          Tap.Evt_Trj.replace(0, Tap.Evt_Trj.capacity(), RUTGEN.Blancos(3 ));
          Tap.Evt_Vle.replace(0, Tap.Evt_Vle.capacity(), RUTGEN.Blancos(15));
          Tap.Evt_Dcm.replace(0, Tap.Evt_Dcm.capacity(), RUTGEN.Blancos(1 ));
          Tap.Tmt_Pgm.replace(0, Tap.Tmt_Pgm.capacity(), RUTGEN.Blancos(8 ));
          Tap.Evt_Ttr.replace(0, Tap.Evt_Ttr.capacity(), RUTGEN.Blancos(5 ));
          Tap.Tmt_Abg.replace(0, Tap.Tmt_Abg.capacity(), RUTGEN.Blancos(4 ));
          Tap.Evt_Nej.replace(0, Tap.Evt_Nej.capacity(), RUTGEN.Blancos(25));
          l_MsgGT211.GT211_Tab.add(Tap);        
        }
    return l_MsgGT211;
  }
  //---------------------------------------------------------------------------------------
  static public Buf_MsgGT211 LSet_A_MsgGT211(String pcStr)
  {
    Buf_MsgGT211 l_MsgGT211 = new Buf_MsgGT211();
    MSGGT211 vMsgGT211 = new MSGGT211();
    Vector vDatos = vMsgGT211.LSet_A_vMsgGT211(pcStr);
    l_MsgGT211 = (MSGGT211.Buf_MsgGT211)vDatos.elementAt(0);
    return l_MsgGT211;
  }
  //---------------------------------------------------------------------------------------
  public Vector LSet_A_vMsgGT211(String pcStr)
  {
    Vector vec = new Vector();
    int p = 0;
    Buf_MsgGT211 l_MsgGT211 = new Buf_MsgGT211();                            
    l_MsgGT211.GT211_Idr.append(pcStr.substring(p, p + l_MsgGT211.GT211_Idr.capacity())); p = p + l_MsgGT211.GT211_Idr.capacity();
    l_MsgGT211.GT211_Tpr.append(pcStr.substring(p, p + l_MsgGT211.GT211_Tpr.capacity())); p = p + l_MsgGT211.GT211_Tpr.capacity();
    l_MsgGT211.GT211_Ejh.append(pcStr.substring(p, p + l_MsgGT211.GT211_Ejh.capacity())); p = p + l_MsgGT211.GT211_Ejh.capacity();
    l_MsgGT211.GT211_Nro.append(pcStr.substring(p, p + l_MsgGT211.GT211_Nro.capacity())); p = p + l_MsgGT211.GT211_Nro.capacity();
    for (int i=0; i<Integer.parseInt(l_MsgGT211.GT211_Nro.toString()); i++)
        {
          Bff_MsgGT211 Tap = new Bff_MsgGT211();
          Tap.Tmt_Est.append(pcStr.substring(p, p + Tap.Tmt_Est.capacity())); p = p + Tap.Tmt_Est.capacity();
          Tap.Tmt_Suc.append(pcStr.substring(p, p + Tap.Tmt_Suc.capacity())); p = p + Tap.Tmt_Suc.capacity();
          Tap.Tmt_Nev.append(pcStr.substring(p, p + Tap.Tmt_Nev.capacity())); p = p + Tap.Tmt_Nev.capacity();
          Tap.Tmt_Trt.append(pcStr.substring(p, p + Tap.Tmt_Trt.capacity())); p = p + Tap.Tmt_Trt.capacity();
          Tap.Evt_Eje.append(pcStr.substring(p, p + Tap.Evt_Eje.capacity())); p = p + Tap.Evt_Eje.capacity();
          Tap.Tmt_Fen.append(pcStr.substring(p, p + Tap.Tmt_Fen.capacity())); p = p + Tap.Tmt_Fen.capacity();
          Tap.Tmt_Hra.append(pcStr.substring(p, p + Tap.Tmt_Hra.capacity())); p = p + Tap.Tmt_Hra.capacity();
          Tap.Evt_Cli.append(pcStr.substring(p, p + Tap.Evt_Cli.capacity())); p = p + Tap.Evt_Cli.capacity();
          Tap.Evt_Ncl.append(pcStr.substring(p, p + Tap.Evt_Ncl.capacity())); p = p + Tap.Evt_Ncl.capacity();
          Tap.Evt_Sis.append(pcStr.substring(p, p + Tap.Evt_Sis.capacity())); p = p + Tap.Evt_Sis.capacity();
          Tap.Evt_Ncn.append(pcStr.substring(p, p + Tap.Evt_Ncn.capacity())); p = p + Tap.Evt_Ncn.capacity();
          Tap.Evt_Dcn.append(pcStr.substring(p, p + Tap.Evt_Dcn.capacity())); p = p + Tap.Evt_Dcn.capacity();
          Tap.Evt_Ggr.append(pcStr.substring(p, p + Tap.Evt_Ggr.capacity())); p = p + Tap.Evt_Ggr.capacity();
          Tap.Evt_Dsg.append(pcStr.substring(p, p + Tap.Evt_Dsg.capacity())); p = p + Tap.Evt_Dsg.capacity();
          Tap.Evt_Mnd.append(pcStr.substring(p, p + Tap.Evt_Mnd.capacity())); p = p + Tap.Evt_Mnd.capacity();
          Tap.Evt_Trj.append(pcStr.substring(p, p + Tap.Evt_Trj.capacity())); p = p + Tap.Evt_Trj.capacity();
          Tap.Evt_Vle.append(pcStr.substring(p, p + Tap.Evt_Vle.capacity())); p = p + Tap.Evt_Vle.capacity();
          Tap.Evt_Dcm.append(pcStr.substring(p, p + Tap.Evt_Dcm.capacity())); p = p + Tap.Evt_Dcm.capacity();
          Tap.Tmt_Pgm.append(pcStr.substring(p, p + Tap.Tmt_Pgm.capacity())); p = p + Tap.Tmt_Pgm.capacity();
          Tap.Evt_Ttr.append(pcStr.substring(p, p + Tap.Evt_Ttr.capacity())); p = p + Tap.Evt_Ttr.capacity();
          Tap.Tmt_Abg.append(pcStr.substring(p, p + Tap.Tmt_Abg.capacity())); p = p + Tap.Tmt_Abg.capacity();
          Tap.Evt_Nej.append(pcStr.substring(p, p + Tap.Evt_Nej.capacity())); p = p + Tap.Evt_Nej.capacity();
          l_MsgGT211.GT211_Tab.add(Tap);
        }
    vec.add (l_MsgGT211);
    return vec;
  }
  //---------------------------------------------------------------------------------------
  static public String LSet_De_MsgGT211(Buf_MsgGT211 p_MsgGT211)
  {
    String pcStr = "";
    pcStr = pcStr + p_MsgGT211.GT211_Idr.toString();
    pcStr = pcStr + p_MsgGT211.GT211_Tpr.toString();
    pcStr = pcStr + p_MsgGT211.GT211_Ejh.toString();
    pcStr = pcStr + p_MsgGT211.GT211_Nro.toString();
    for (int i=0; i<p_MsgGT211.GT211_Tab.size(); i++)
        {
          pcStr = pcStr + ((Bff_MsgGT211)p_MsgGT211.GT211_Tab.elementAt(i)).Tmt_Est.toString();
          pcStr = pcStr + ((Bff_MsgGT211)p_MsgGT211.GT211_Tab.elementAt(i)).Tmt_Suc.toString();
          pcStr = pcStr + ((Bff_MsgGT211)p_MsgGT211.GT211_Tab.elementAt(i)).Tmt_Nev.toString();
          pcStr = pcStr + ((Bff_MsgGT211)p_MsgGT211.GT211_Tab.elementAt(i)).Tmt_Trt.toString();
          pcStr = pcStr + ((Bff_MsgGT211)p_MsgGT211.GT211_Tab.elementAt(i)).Evt_Eje.toString();
          pcStr = pcStr + ((Bff_MsgGT211)p_MsgGT211.GT211_Tab.elementAt(i)).Tmt_Fen.toString();
          pcStr = pcStr + ((Bff_MsgGT211)p_MsgGT211.GT211_Tab.elementAt(i)).Tmt_Hra.toString();
          pcStr = pcStr + ((Bff_MsgGT211)p_MsgGT211.GT211_Tab.elementAt(i)).Evt_Cli.toString();
          pcStr = pcStr + ((Bff_MsgGT211)p_MsgGT211.GT211_Tab.elementAt(i)).Evt_Ncl.toString();
          pcStr = pcStr + ((Bff_MsgGT211)p_MsgGT211.GT211_Tab.elementAt(i)).Evt_Sis.toString();
          pcStr = pcStr + ((Bff_MsgGT211)p_MsgGT211.GT211_Tab.elementAt(i)).Evt_Ncn.toString();
          pcStr = pcStr + ((Bff_MsgGT211)p_MsgGT211.GT211_Tab.elementAt(i)).Evt_Dcn.toString();
          pcStr = pcStr + ((Bff_MsgGT211)p_MsgGT211.GT211_Tab.elementAt(i)).Evt_Ggr.toString();
          pcStr = pcStr + ((Bff_MsgGT211)p_MsgGT211.GT211_Tab.elementAt(i)).Evt_Dsg.toString();
          pcStr = pcStr + ((Bff_MsgGT211)p_MsgGT211.GT211_Tab.elementAt(i)).Evt_Mnd.toString();
          pcStr = pcStr + ((Bff_MsgGT211)p_MsgGT211.GT211_Tab.elementAt(i)).Evt_Trj.toString();
          pcStr = pcStr + ((Bff_MsgGT211)p_MsgGT211.GT211_Tab.elementAt(i)).Evt_Vle.toString();
          pcStr = pcStr + ((Bff_MsgGT211)p_MsgGT211.GT211_Tab.elementAt(i)).Evt_Dcm.toString();
          pcStr = pcStr + ((Bff_MsgGT211)p_MsgGT211.GT211_Tab.elementAt(i)).Tmt_Pgm.toString();
          pcStr = pcStr + ((Bff_MsgGT211)p_MsgGT211.GT211_Tab.elementAt(i)).Evt_Ttr.toString();
          pcStr = pcStr + ((Bff_MsgGT211)p_MsgGT211.GT211_Tab.elementAt(i)).Tmt_Abg.toString();
          pcStr = pcStr + ((Bff_MsgGT211)p_MsgGT211.GT211_Tab.elementAt(i)).Evt_Nej.toString();
        }
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
}