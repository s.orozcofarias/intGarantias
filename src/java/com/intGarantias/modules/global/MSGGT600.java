// Source File Name:   MSGGT600.java

package com.intGarantias.modules.global;

import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.util.Formateo;

import java.util.*;
import java.lang.*;
import org.apache.turbine.util.Log;

public class MSGGT600
{
  public static int g_Max_GT600 = 40;
  //---------------------------------------------------------------------------------------
  public static class Bff_MsgGT600
  {
    public StringBuffer GT600_Cod = new StringBuffer(11);   //9(11)      Clave (Ano,Mes,Idr's)
    public StringBuffer GT600_Dsc = new StringBuffer(15);   //X(15)      Descripcion
    public StringBuffer GT600_Itb = new StringBuffer(5 );   //X(05)      Tipo Garantia
    public StringBuffer GT600_Dsi = new StringBuffer(15);   //X(15)      Descripcion
    public StringBuffer GT600_Ntv = new StringBuffer(7 );   //9(07)      Tasaciones Visadas
    public StringBuffer GT600_Pdt = new StringBuffer(5 );   //9(03)V9(2) %Del Total
    public StringBuffer GT600_Muf = new StringBuffer(15);   //9(11)V9(4) Monto UF
    public StringBuffer GT600_Hpe = new StringBuffer(15);   //9(13)V9(2) Honorarios $

    public String getCod()       {return GT600_Cod.toString();}
    public String getDsc()       {return GT600_Dsc.toString();}
    public String getItb()       {return GT600_Itb.toString();}
    public String getDsi()       {return GT600_Dsi.toString();}
    public String getNtv()       {return GT600_Ntv.toString();}
    public String getPdt()       {return GT600_Pdt.toString();}
    public String getMuf()       {return GT600_Muf.toString();}
    public String getHpe()       {return GT600_Hpe.toString();}

    public Formateo f = new Formateo();
    public String fgetNtv()      {return f.Formato(GT600_Ntv.toString(),"9.999.999",0,0,',');}
    public String fgetPdt()      {return f.Formato(GT600_Pdt.toString(),"99.999.999.999",2,2,',');}
    public String fgetMuf()      {return f.Formato(GT600_Muf.toString(),"99.999.999.999",4,2,',');}
    public String fgetHpe()      {return f.Formato(GT600_Hpe.toString(),"99.999.999.999",2,0,',');}
  }
  //---------------------------------------------------------------------------------------
  public static class Buf_MsgGT600
  {
    public StringBuffer GT600_Idr = new StringBuffer(1 );   //X(01)      Id.Requerimiento  
    public StringBuffer GT600_Ano = new StringBuffer(4 );   //9(04)      Ano
    public StringBuffer GT600_Mes = new StringBuffer(2 );   //9(02)      Mes
    public StringBuffer GT600_Tpr = new StringBuffer(3 );   //X(03)      Tipo Registro
    public StringBuffer GT600_Nro = new StringBuffer(3 );   //9(03)      Numero Elementos  
    public Vector       GT600_Tab = new Vector();

    public String getIdr()         {return GT600_Idr.toString();}
    public String getAno()         {return GT600_Ano.toString();}
    public String getMes()         {return GT600_Mes.toString();}
    public String getTpr()         {return GT600_Tpr.toString();}
    public String getNro()         {return GT600_Nro.toString();}
    public Vector getTab()         {return GT600_Tab;}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_MsgGT600 Inicia_MsgGT600()
  {
    Buf_MsgGT600 l_MsgGT600 = new Buf_MsgGT600();
    l_MsgGT600.GT600_Idr.replace(0, l_MsgGT600.GT600_Idr.capacity(), RUTGEN.Blancos(1 ));
    l_MsgGT600.GT600_Ano.replace(0, l_MsgGT600.GT600_Ano.capacity(), RUTGEN.Blancos(4 ));
    l_MsgGT600.GT600_Mes.replace(0, l_MsgGT600.GT600_Mes.capacity(), RUTGEN.Blancos(2 ));
    l_MsgGT600.GT600_Tpr.replace(0, l_MsgGT600.GT600_Tpr.capacity(), RUTGEN.Blancos(3 ));
    l_MsgGT600.GT600_Nro.replace(0, l_MsgGT600.GT600_Nro.capacity(), RUTGEN.Blancos(3 ));
    int p = 0;
    for (int i=0; i<g_Max_GT600; i++)
        {
          Bff_MsgGT600 Tap = new Bff_MsgGT600();
          Tap.GT600_Cod.replace(0, Tap.GT600_Cod.capacity(), RUTGEN.Blancos(11));
          Tap.GT600_Dsc.replace(0, Tap.GT600_Dsc.capacity(), RUTGEN.Blancos(15));
          Tap.GT600_Itb.replace(0, Tap.GT600_Itb.capacity(), RUTGEN.Blancos(5 ));
          Tap.GT600_Dsi.replace(0, Tap.GT600_Dsi.capacity(), RUTGEN.Blancos(15));
          Tap.GT600_Ntv.replace(0, Tap.GT600_Ntv.capacity(), RUTGEN.Blancos(7 ));
          Tap.GT600_Pdt.replace(0, Tap.GT600_Pdt.capacity(), RUTGEN.Blancos(5 ));
          Tap.GT600_Muf.replace(0, Tap.GT600_Muf.capacity(), RUTGEN.Blancos(15));
          Tap.GT600_Hpe.replace(0, Tap.GT600_Hpe.capacity(), RUTGEN.Blancos(15));
          l_MsgGT600.GT600_Tab.add(Tap);        
        }
    return l_MsgGT600;
  }
  //---------------------------------------------------------------------------------------
  static public Buf_MsgGT600 LSet_A_MsgGT600(String pcStr)
  {
    Buf_MsgGT600 l_MsgGT600 = new Buf_MsgGT600();
    MSGGT600 vMsgGT600 = new MSGGT600();
    Vector vDatos = vMsgGT600.LSet_A_vMsgGT600(pcStr);
    l_MsgGT600 = (MSGGT600.Buf_MsgGT600)vDatos.elementAt(0);
    return l_MsgGT600;
  }
  //---------------------------------------------------------------------------------------
  public Vector LSet_A_vMsgGT600(String pcStr)
  {
    Vector vec = new Vector();
    int p = 0;
    Buf_MsgGT600 l_MsgGT600 = new Buf_MsgGT600();                            
    l_MsgGT600.GT600_Idr.append(pcStr.substring(p, p + l_MsgGT600.GT600_Idr.capacity())); p = p + l_MsgGT600.GT600_Idr.capacity();
    l_MsgGT600.GT600_Ano.append(pcStr.substring(p, p + l_MsgGT600.GT600_Ano.capacity())); p = p + l_MsgGT600.GT600_Ano.capacity();
    l_MsgGT600.GT600_Mes.append(pcStr.substring(p, p + l_MsgGT600.GT600_Mes.capacity())); p = p + l_MsgGT600.GT600_Mes.capacity();
    l_MsgGT600.GT600_Tpr.append(pcStr.substring(p, p + l_MsgGT600.GT600_Tpr.capacity())); p = p + l_MsgGT600.GT600_Tpr.capacity();
    l_MsgGT600.GT600_Nro.append(pcStr.substring(p, p + l_MsgGT600.GT600_Nro.capacity())); p = p + l_MsgGT600.GT600_Nro.capacity();
    for (int i=0; i<Integer.parseInt(l_MsgGT600.GT600_Nro.toString()); i++)
        {
          Bff_MsgGT600 Tap = new Bff_MsgGT600();
          Tap.GT600_Cod.append(pcStr.substring(p, p + Tap.GT600_Cod.capacity())); p = p + Tap.GT600_Cod.capacity();
          Tap.GT600_Dsc.append(pcStr.substring(p, p + Tap.GT600_Dsc.capacity())); p = p + Tap.GT600_Dsc.capacity();
          Tap.GT600_Itb.append(pcStr.substring(p, p + Tap.GT600_Itb.capacity())); p = p + Tap.GT600_Itb.capacity();
          Tap.GT600_Dsi.append(pcStr.substring(p, p + Tap.GT600_Dsi.capacity())); p = p + Tap.GT600_Dsi.capacity();
          Tap.GT600_Ntv.append(pcStr.substring(p, p + Tap.GT600_Ntv.capacity())); p = p + Tap.GT600_Ntv.capacity();
          Tap.GT600_Pdt.append(pcStr.substring(p, p + Tap.GT600_Pdt.capacity())); p = p + Tap.GT600_Pdt.capacity();
          Tap.GT600_Muf.append(pcStr.substring(p, p + Tap.GT600_Muf.capacity())); p = p + Tap.GT600_Muf.capacity();
          Tap.GT600_Hpe.append(pcStr.substring(p, p + Tap.GT600_Hpe.capacity())); p = p + Tap.GT600_Hpe.capacity();
          l_MsgGT600.GT600_Tab.add(Tap);
        }
    vec.add (l_MsgGT600);
    return vec;
  }
  //---------------------------------------------------------------------------------------
  static public String LSet_De_MsgGT600(Buf_MsgGT600 p_MsgGT600)
  {
    String pcStr = "";
    pcStr = pcStr + p_MsgGT600.GT600_Idr.toString();
    pcStr = pcStr + p_MsgGT600.GT600_Ano.toString();
    pcStr = pcStr + p_MsgGT600.GT600_Mes.toString();
    pcStr = pcStr + p_MsgGT600.GT600_Tpr.toString();
    pcStr = pcStr + p_MsgGT600.GT600_Nro.toString();
    for (int i=0; i<p_MsgGT600.GT600_Tab.size(); i++)
        {
          pcStr = pcStr + ((Bff_MsgGT600)p_MsgGT600.GT600_Tab.elementAt(i)).GT600_Cod.toString();
          pcStr = pcStr + ((Bff_MsgGT600)p_MsgGT600.GT600_Tab.elementAt(i)).GT600_Dsc.toString();
          pcStr = pcStr + ((Bff_MsgGT600)p_MsgGT600.GT600_Tab.elementAt(i)).GT600_Itb.toString();
          pcStr = pcStr + ((Bff_MsgGT600)p_MsgGT600.GT600_Tab.elementAt(i)).GT600_Dsi.toString();
          pcStr = pcStr + ((Bff_MsgGT600)p_MsgGT600.GT600_Tab.elementAt(i)).GT600_Ntv.toString();
          pcStr = pcStr + ((Bff_MsgGT600)p_MsgGT600.GT600_Tab.elementAt(i)).GT600_Pdt.toString();
          pcStr = pcStr + ((Bff_MsgGT600)p_MsgGT600.GT600_Tab.elementAt(i)).GT600_Muf.toString();
          pcStr = pcStr + ((Bff_MsgGT600)p_MsgGT600.GT600_Tab.elementAt(i)).GT600_Hpe.toString();
        }
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
}