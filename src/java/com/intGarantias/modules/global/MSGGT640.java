// Source File Name:   MSGGT640.java

package com.intGarantias.modules.global;

import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.util.Formateo;

import java.util.*;
import java.lang.*;
import org.apache.turbine.util.Log;

public class MSGGT640
{
  public static int g_Max_GT640 = 4;
  //---------------------------------------------------------------------------------------
  public static class Bff_MsgGT640
  {
    public StringBuffer Img_Ntr = new StringBuffer(7  );        //9(07)        7 Id. Transaccion
    public StringBuffer Img_Dax = new StringBuffer(5  );        //X(05)       12 Tipo Anexo
    public StringBuffer Img_Seq = new StringBuffer(3  );        //9(03)       15 Secuencia Anexo
    public StringBuffer Img_Dat = new StringBuffer(827);        //X(827)     842 Data Anexo

    public String getNtr()     {return Img_Ntr.toString();}
    public String getDax()     {return Img_Dax.toString();}
    public String getSeq()     {return Img_Seq.toString();}
    public String getDat()     {return Img_Dat.toString();}
  }
  //---------------------------------------------------------------------------------------
  public static class Buf_MsgGT640
  {
    public StringBuffer GT640_Idr = new StringBuffer(1 );       //X(01)        1 Idr. Host
    public StringBuffer GT640_Idx = new StringBuffer(4 );       //9(04)        4 Seq Imagen
    public StringBuffer GT640_Sis = new StringBuffer(3 );       //X(03)        7 Sistema
    public StringBuffer GT640_Ncn = new StringBuffer(7 );       //9(07)       14 Contrato
    public StringBuffer GT640_Ttr = new StringBuffer(5 );       //X(05)       19 Evento
    public StringBuffer GT640_Ntr = new StringBuffer(7 );       //9(07)       26 N�Evento
    public StringBuffer GT640_Err = new StringBuffer(2 );       //X(02)       28 Codigo Error
    public Vector       GT640_Img = new Vector();               //X(3368)   3396 Data Imgs

    public String getIdr()       {return GT640_Idr.toString();}
    public String getIdx()       {return GT640_Idx.toString();}
    public String getSis()       {return GT640_Sis.toString();}
    public String getNcn()       {return GT640_Ncn.toString();}
    public String getTtr()       {return GT640_Ttr.toString();}
    public String getNtr()       {return GT640_Ntr.toString();}
    public String getErr()       {return GT640_Err.toString();}
    public Vector getImg()       {return GT640_Img;}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_MsgGT640 Inicia_MsgGT640()
  {
    Buf_MsgGT640 l_MsgGT640 = new Buf_MsgGT640();
    l_MsgGT640.GT640_Idr.replace(0, l_MsgGT640.GT640_Idr.capacity(), RUTGEN.Blancos(1 ));
    l_MsgGT640.GT640_Idx.replace(0, l_MsgGT640.GT640_Idx.capacity(), RUTGEN.Blancos(4 ));
    l_MsgGT640.GT640_Sis.replace(0, l_MsgGT640.GT640_Sis.capacity(), RUTGEN.Blancos(3 ));
    l_MsgGT640.GT640_Ncn.replace(0, l_MsgGT640.GT640_Ncn.capacity(), RUTGEN.Blancos(7 ));
    l_MsgGT640.GT640_Ttr.replace(0, l_MsgGT640.GT640_Ttr.capacity(), RUTGEN.Blancos(5 ));
    l_MsgGT640.GT640_Ntr.replace(0, l_MsgGT640.GT640_Ntr.capacity(), RUTGEN.Blancos(7 ));
    l_MsgGT640.GT640_Err.replace(0, l_MsgGT640.GT640_Err.capacity(), RUTGEN.Blancos(2 ));
    for (int i=0; i<g_Max_GT640; i++)
        {
          Bff_MsgGT640 Tap = new Bff_MsgGT640();
          Tap.Img_Ntr.replace(0, Tap.Img_Ntr.capacity(), RUTGEN.Blancos(7  ));
          Tap.Img_Dax.replace(0, Tap.Img_Dax.capacity(), RUTGEN.Blancos(5  ));
          Tap.Img_Seq.replace(0, Tap.Img_Seq.capacity(), RUTGEN.Blancos(3  ));
          Tap.Img_Dat.replace(0, Tap.Img_Dat.capacity(), RUTGEN.Blancos(827));
          l_MsgGT640.GT640_Img.add(Tap);
        }
    return l_MsgGT640;
  }
  //---------------------------------------------------------------------------------------
  static public Buf_MsgGT640 LSet_A_MsgGT640(String pcStr)
  {
    Buf_MsgGT640 l_MsgGT640 = new Buf_MsgGT640();
    MSGGT640 vMsgGT640 = new MSGGT640();
    Vector vDatos = vMsgGT640.LSet_A_vMsgGT640(pcStr);
    l_MsgGT640 = (MSGGT640.Buf_MsgGT640)vDatos.elementAt(0);
    return l_MsgGT640;
  }
  //---------------------------------------------------------------------------------------
  public Vector LSet_A_vMsgGT640(String pcStr)
  {
    Vector vec = new Vector();
    int p = 0;
    Buf_MsgGT640 l_MsgGT640 = new Buf_MsgGT640();
    l_MsgGT640.GT640_Idr.append(pcStr.substring(p, p + l_MsgGT640.GT640_Idr.capacity())); p = p + l_MsgGT640.GT640_Idr.capacity();
    l_MsgGT640.GT640_Idx.append(pcStr.substring(p, p + l_MsgGT640.GT640_Idx.capacity())); p = p + l_MsgGT640.GT640_Idx.capacity();
    l_MsgGT640.GT640_Sis.append(pcStr.substring(p, p + l_MsgGT640.GT640_Sis.capacity())); p = p + l_MsgGT640.GT640_Sis.capacity();
    l_MsgGT640.GT640_Ncn.append(pcStr.substring(p, p + l_MsgGT640.GT640_Ncn.capacity())); p = p + l_MsgGT640.GT640_Ncn.capacity();
    l_MsgGT640.GT640_Ttr.append(pcStr.substring(p, p + l_MsgGT640.GT640_Ttr.capacity())); p = p + l_MsgGT640.GT640_Ttr.capacity();
    l_MsgGT640.GT640_Ntr.append(pcStr.substring(p, p + l_MsgGT640.GT640_Ntr.capacity())); p = p + l_MsgGT640.GT640_Ntr.capacity();
    l_MsgGT640.GT640_Err.append(pcStr.substring(p, p + l_MsgGT640.GT640_Err.capacity())); p = p + l_MsgGT640.GT640_Err.capacity();
    for (int i=0; i<g_Max_GT640; i++)
        {
          Bff_MsgGT640 Tap = new Bff_MsgGT640();
          Tap.Img_Ntr.append(pcStr.substring(p, p + Tap.Img_Ntr.capacity())); p = p + Tap.Img_Ntr.capacity();
          Tap.Img_Dax.append(pcStr.substring(p, p + Tap.Img_Dax.capacity())); p = p + Tap.Img_Dax.capacity();
          Tap.Img_Seq.append(pcStr.substring(p, p + Tap.Img_Seq.capacity())); p = p + Tap.Img_Seq.capacity();
          Tap.Img_Dat.append(pcStr.substring(p, p + Tap.Img_Dat.capacity())); p = p + Tap.Img_Dat.capacity();
          l_MsgGT640.GT640_Img.add(Tap);
        }
    vec.add (l_MsgGT640);
    return vec;
  }
  //---------------------------------------------------------------------------------------
  static public String LSet_De_MsgGT640(Buf_MsgGT640 p_MsgGT640)
  {
    String pcStr = "";
    pcStr = pcStr + p_MsgGT640.GT640_Idr.toString();
    pcStr = pcStr + p_MsgGT640.GT640_Idx.toString();
    pcStr = pcStr + p_MsgGT640.GT640_Sis.toString();
    pcStr = pcStr + p_MsgGT640.GT640_Ncn.toString();
    pcStr = pcStr + p_MsgGT640.GT640_Ttr.toString();
    pcStr = pcStr + p_MsgGT640.GT640_Ntr.toString();
    pcStr = pcStr + p_MsgGT640.GT640_Err.toString();
    for (int i=0; i<g_Max_GT640; i++)
        {
          pcStr = pcStr + ((Bff_MsgGT640)p_MsgGT640.GT640_Img.elementAt(i)).Img_Ntr.toString();
          pcStr = pcStr + ((Bff_MsgGT640)p_MsgGT640.GT640_Img.elementAt(i)).Img_Dax.toString();
          pcStr = pcStr + ((Bff_MsgGT640)p_MsgGT640.GT640_Img.elementAt(i)).Img_Seq.toString();
          pcStr = pcStr + ((Bff_MsgGT640)p_MsgGT640.GT640_Img.elementAt(i)).Img_Dat.toString();
        }
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
}