// Source File Name:   MSGGT200.java

package com.intGarantias.modules.global;

import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.util.Formateo;

import java.util.*;
import java.lang.*;
import org.apache.turbine.util.Log;

public class MSGGT200
{
  public static int g_Max_GT200 = 21;
  //---------------------------------------------------------------------------------------
  public static class Bff_MsgGT200
  {
    public StringBuffer Tmt_Nev = new StringBuffer(7 );       //9(07)         7  Numero Evento                
    public StringBuffer Tmt_Trt = new StringBuffer(5 );       //X(05)        12  Tipo Tramite/UNICO Estado    
    public StringBuffer Tmt_Fen = new StringBuffer(8 );       //9(08)        20  Fecha Envio                  
    public StringBuffer Evt_Cli = new StringBuffer(10);       //X(10)        30  RUT Cliente                  
    public StringBuffer Evt_Ncl = new StringBuffer(40);       //X(40)        70  Nombre Cliente               
    public StringBuffer Evt_Sis = new StringBuffer(3 );       //X(03)        73  Sistema                      
    public StringBuffer Evt_Ncn = new StringBuffer(7 );       //9(07)        80  Contrato                     
    public StringBuffer Evt_Dcn = new StringBuffer(5 );       //X(05)        85  Codigo Contrato              
    public StringBuffer Evt_Dsc = new StringBuffer(30);       //X(30)       115  Nombre Contrato              
    public StringBuffer Evt_Suc = new StringBuffer(3 );       //9(03)       118  Sucursal                     
    public StringBuffer Evt_Mnd = new StringBuffer(3 );       //9(03)       121  Moneda                       
    public StringBuffer Evt_Vle = new StringBuffer(15);       //9(11)V9(4)  136  Valor Estimado               
    public StringBuffer Evt_Eje = new StringBuffer(4 );       //9(04)       140  Ejecutivo                    
    public StringBuffer Evt_Trj = new StringBuffer(3 );       //X(03)       143  Reajustabilidad              
    public StringBuffer Evt_Dcm = new StringBuffer(1 );       //9(01)       144  Decimales                    
    public StringBuffer Tmt_Pgm = new StringBuffer(8 );       //X(08)       152  Programa VB Servidor         
    public StringBuffer Evt_Ttr = new StringBuffer(5 );       //X(05)       157  Transaccion                  
    public StringBuffer Tmt_Est = new StringBuffer(5 );       //X(05)       162  Estado TMT                   

    public String getTNev()      {return Tmt_Nev.toString();}
    public String getTTrt()      {return Tmt_Trt.toString();}
    public String getTFen()      {return Tmt_Fen.toString();}
    public String getECli()      {return Evt_Cli.toString();}
    public String getENcl()      {return Evt_Ncl.toString();}
    public String getESis()      {return Evt_Sis.toString();}
    public String getENcn()      {return Evt_Ncn.toString();}
    public String getEDcn()      {return Evt_Dcn.toString();}
    public String getEDsc()      {return Evt_Dsc.toString();}
    public String getESuc()      {return Evt_Suc.toString();}
    public String getEMnd()      {return Evt_Mnd.toString();}
    public String getEVle()      {return Evt_Vle.toString();}
    public String getEEje()      {return Evt_Eje.toString();}
    public String getETrj()      {return Evt_Trj.toString();}
    public String getEDcm()      {return Evt_Dcm.toString();}
    public String getTPgm()      {return Tmt_Pgm.toString();}
    public String getETtr()      {return Evt_Ttr.toString();}
    public String getTEst()      {return Tmt_Est.toString();}

    public Formateo f = new Formateo();
    public String fgetFen()      {return f.FormatDate(Tmt_Fen.toString());}
    public String fgetCli()      {return f.fmtRut(Evt_Cli.toString());}
    public String fgetOpe()      {
    	                           String dato = Evt_Sis.toString() + "-" + Evt_Dcn.toString() + "-" + Evt_Ncn.toString();
    	                           return dato;
                                 }
    public String fgetTrj()      {return Evt_Trj.toString().trim() + "-" + Evt_Mnd.toString();}
    public String fgetVle()      {return f.Formato(Evt_Vle.toString(),"99.999.999.999",4,4,',');}
  }
  //---------------------------------------------------------------------------------------
  public static class Buf_MsgGT200
  {
    public StringBuffer GT200_Idr = new StringBuffer(1 );
    public StringBuffer GT200_Tsd = new StringBuffer(3 );
    public StringBuffer GT200_Nts = new StringBuffer(30);
    public StringBuffer GT200_Nro = new StringBuffer(3 );
    public Vector       GT200_Tab = new Vector();

    public String getIdr()         {return GT200_Idr.toString();}
    public String getTsd()         {return GT200_Tsd.toString();}
    public String getNts()         {return GT200_Nts.toString();}
    public String getNro()         {return GT200_Nro.toString();}
    public Vector getTab()         {return GT200_Tab;}

    public String fgetTsd()      {
    	                           if (GT200_Tsd.toString().equals("000") )
                                      return "N/A";
    	                           else	
                                      return GT200_Tsd.toString();
                                 }
  }
  //---------------------------------------------------------------------------------------
  public static Buf_MsgGT200 Inicia_MsgGT200()
  {
    Buf_MsgGT200 l_MsgGT200 = new Buf_MsgGT200();
    l_MsgGT200.GT200_Idr.replace(0, l_MsgGT200.GT200_Idr.capacity(), RUTGEN.Blancos(1 ));
    l_MsgGT200.GT200_Tsd.replace(0, l_MsgGT200.GT200_Tsd.capacity(), RUTGEN.Blancos(3 ));
    l_MsgGT200.GT200_Nts.replace(0, l_MsgGT200.GT200_Nts.capacity(), RUTGEN.Blancos(30));
    l_MsgGT200.GT200_Nro.replace(0, l_MsgGT200.GT200_Nro.capacity(), RUTGEN.Blancos(3 ));
    int p = 0;
    for (int i=0; i<g_Max_GT200; i++)
        {
          Bff_MsgGT200 Tap = new Bff_MsgGT200();
          Tap.Tmt_Nev.replace(0, Tap.Tmt_Nev.capacity(), RUTGEN.Blancos(7 ));
          Tap.Tmt_Trt.replace(0, Tap.Tmt_Trt.capacity(), RUTGEN.Blancos(5 ));
          Tap.Tmt_Fen.replace(0, Tap.Tmt_Fen.capacity(), RUTGEN.Blancos(8 ));
          Tap.Evt_Cli.replace(0, Tap.Evt_Cli.capacity(), RUTGEN.Blancos(10));
          Tap.Evt_Ncl.replace(0, Tap.Evt_Ncl.capacity(), RUTGEN.Blancos(40));
          Tap.Evt_Sis.replace(0, Tap.Evt_Sis.capacity(), RUTGEN.Blancos(3 ));
          Tap.Evt_Ncn.replace(0, Tap.Evt_Ncn.capacity(), RUTGEN.Blancos(7 ));
          Tap.Evt_Dcn.replace(0, Tap.Evt_Dcn.capacity(), RUTGEN.Blancos(5 ));
          Tap.Evt_Dsc.replace(0, Tap.Evt_Dsc.capacity(), RUTGEN.Blancos(30));
          Tap.Evt_Suc.replace(0, Tap.Evt_Suc.capacity(), RUTGEN.Blancos(3 ));
          Tap.Evt_Mnd.replace(0, Tap.Evt_Mnd.capacity(), RUTGEN.Blancos(3 ));
          Tap.Evt_Vle.replace(0, Tap.Evt_Vle.capacity(), RUTGEN.Blancos(15));
          Tap.Evt_Eje.replace(0, Tap.Evt_Eje.capacity(), RUTGEN.Blancos(4 ));
          Tap.Evt_Trj.replace(0, Tap.Evt_Trj.capacity(), RUTGEN.Blancos(3 ));
          Tap.Evt_Dcm.replace(0, Tap.Evt_Dcm.capacity(), RUTGEN.Blancos(1 ));
          Tap.Tmt_Pgm.replace(0, Tap.Tmt_Pgm.capacity(), RUTGEN.Blancos(8 ));
          Tap.Evt_Ttr.replace(0, Tap.Evt_Ttr.capacity(), RUTGEN.Blancos(5 ));
          Tap.Tmt_Est.replace(0, Tap.Tmt_Est.capacity(), RUTGEN.Blancos(5 ));
          l_MsgGT200.GT200_Tab.add(Tap);        
        }
    return l_MsgGT200;
  }
  //---------------------------------------------------------------------------------------
  static public Buf_MsgGT200 LSet_A_MsgGT200(String pcStr)
  {
    Buf_MsgGT200 l_MsgGT200 = new Buf_MsgGT200();
    MSGGT200 vMsgGT200 = new MSGGT200();
    Vector vDatos = vMsgGT200.LSet_A_vMsgGT200(pcStr);
    l_MsgGT200 = (MSGGT200.Buf_MsgGT200)vDatos.elementAt(0);
    return l_MsgGT200;
  }
  //---------------------------------------------------------------------------------------
  public Vector LSet_A_vMsgGT200(String pcStr)
  {
    Vector vec = new Vector();
    int p = 0;
    Buf_MsgGT200 l_MsgGT200 = new Buf_MsgGT200();                            
    l_MsgGT200.GT200_Idr.append(pcStr.substring(p, p + l_MsgGT200.GT200_Idr.capacity())); p = p + l_MsgGT200.GT200_Idr.capacity();
    l_MsgGT200.GT200_Tsd.append(pcStr.substring(p, p + l_MsgGT200.GT200_Tsd.capacity())); p = p + l_MsgGT200.GT200_Tsd.capacity();
    l_MsgGT200.GT200_Nts.append(pcStr.substring(p, p + l_MsgGT200.GT200_Nts.capacity())); p = p + l_MsgGT200.GT200_Nts.capacity();
    l_MsgGT200.GT200_Nro.append(pcStr.substring(p, p + l_MsgGT200.GT200_Nro.capacity())); p = p + l_MsgGT200.GT200_Nro.capacity();
    for (int i=0; i<Integer.parseInt(l_MsgGT200.GT200_Nro.toString()); i++)
        {
          Bff_MsgGT200 Tap = new Bff_MsgGT200();
          Tap.Tmt_Nev.append(pcStr.substring(p, p + Tap.Tmt_Nev.capacity())); p = p + Tap.Tmt_Nev.capacity();
          Tap.Tmt_Trt.append(pcStr.substring(p, p + Tap.Tmt_Trt.capacity())); p = p + Tap.Tmt_Trt.capacity();
          Tap.Tmt_Fen.append(pcStr.substring(p, p + Tap.Tmt_Fen.capacity())); p = p + Tap.Tmt_Fen.capacity();
          Tap.Evt_Cli.append(pcStr.substring(p, p + Tap.Evt_Cli.capacity())); p = p + Tap.Evt_Cli.capacity();
          Tap.Evt_Ncl.append(pcStr.substring(p, p + Tap.Evt_Ncl.capacity())); p = p + Tap.Evt_Ncl.capacity();
          Tap.Evt_Sis.append(pcStr.substring(p, p + Tap.Evt_Sis.capacity())); p = p + Tap.Evt_Sis.capacity();
          Tap.Evt_Ncn.append(pcStr.substring(p, p + Tap.Evt_Ncn.capacity())); p = p + Tap.Evt_Ncn.capacity();
          Tap.Evt_Dcn.append(pcStr.substring(p, p + Tap.Evt_Dcn.capacity())); p = p + Tap.Evt_Dcn.capacity();
          Tap.Evt_Dsc.append(pcStr.substring(p, p + Tap.Evt_Dsc.capacity())); p = p + Tap.Evt_Dsc.capacity();
          Tap.Evt_Suc.append(pcStr.substring(p, p + Tap.Evt_Suc.capacity())); p = p + Tap.Evt_Suc.capacity();
          Tap.Evt_Mnd.append(pcStr.substring(p, p + Tap.Evt_Mnd.capacity())); p = p + Tap.Evt_Mnd.capacity();
          Tap.Evt_Vle.append(pcStr.substring(p, p + Tap.Evt_Vle.capacity())); p = p + Tap.Evt_Vle.capacity();
          Tap.Evt_Eje.append(pcStr.substring(p, p + Tap.Evt_Eje.capacity())); p = p + Tap.Evt_Eje.capacity();
          Tap.Evt_Trj.append(pcStr.substring(p, p + Tap.Evt_Trj.capacity())); p = p + Tap.Evt_Trj.capacity();
          Tap.Evt_Dcm.append(pcStr.substring(p, p + Tap.Evt_Dcm.capacity())); p = p + Tap.Evt_Dcm.capacity();
          Tap.Tmt_Pgm.append(pcStr.substring(p, p + Tap.Tmt_Pgm.capacity())); p = p + Tap.Tmt_Pgm.capacity();
          Tap.Evt_Ttr.append(pcStr.substring(p, p + Tap.Evt_Ttr.capacity())); p = p + Tap.Evt_Ttr.capacity();
          Tap.Tmt_Est.append(pcStr.substring(p, p + Tap.Tmt_Est.capacity())); p = p + Tap.Tmt_Est.capacity();
          l_MsgGT200.GT200_Tab.add(Tap);
        }
    vec.add (l_MsgGT200);
    return vec;
  }
  //---------------------------------------------------------------------------------------
  static public String LSet_De_MsgGT200(Buf_MsgGT200 p_MsgGT200)
  {
    String pcStr = "";
    pcStr = pcStr + p_MsgGT200.GT200_Idr.toString();
    pcStr = pcStr + p_MsgGT200.GT200_Tsd.toString();
    pcStr = pcStr + p_MsgGT200.GT200_Nts.toString();
    pcStr = pcStr + p_MsgGT200.GT200_Nro.toString();
    for (int i=0; i<p_MsgGT200.GT200_Tab.size(); i++)
        {
          pcStr = pcStr + ((Bff_MsgGT200)p_MsgGT200.GT200_Tab.elementAt(i)).Tmt_Nev.toString();
          pcStr = pcStr + ((Bff_MsgGT200)p_MsgGT200.GT200_Tab.elementAt(i)).Tmt_Trt.toString();
          pcStr = pcStr + ((Bff_MsgGT200)p_MsgGT200.GT200_Tab.elementAt(i)).Tmt_Fen.toString();
          pcStr = pcStr + ((Bff_MsgGT200)p_MsgGT200.GT200_Tab.elementAt(i)).Evt_Cli.toString();
          pcStr = pcStr + ((Bff_MsgGT200)p_MsgGT200.GT200_Tab.elementAt(i)).Evt_Ncl.toString();
          pcStr = pcStr + ((Bff_MsgGT200)p_MsgGT200.GT200_Tab.elementAt(i)).Evt_Sis.toString();
          pcStr = pcStr + ((Bff_MsgGT200)p_MsgGT200.GT200_Tab.elementAt(i)).Evt_Ncn.toString();
          pcStr = pcStr + ((Bff_MsgGT200)p_MsgGT200.GT200_Tab.elementAt(i)).Evt_Dcn.toString();
          pcStr = pcStr + ((Bff_MsgGT200)p_MsgGT200.GT200_Tab.elementAt(i)).Evt_Dsc.toString();
          pcStr = pcStr + ((Bff_MsgGT200)p_MsgGT200.GT200_Tab.elementAt(i)).Evt_Suc.toString();
          pcStr = pcStr + ((Bff_MsgGT200)p_MsgGT200.GT200_Tab.elementAt(i)).Evt_Mnd.toString();
          pcStr = pcStr + ((Bff_MsgGT200)p_MsgGT200.GT200_Tab.elementAt(i)).Evt_Vle.toString();
          pcStr = pcStr + ((Bff_MsgGT200)p_MsgGT200.GT200_Tab.elementAt(i)).Evt_Eje.toString();
          pcStr = pcStr + ((Bff_MsgGT200)p_MsgGT200.GT200_Tab.elementAt(i)).Evt_Trj.toString();
          pcStr = pcStr + ((Bff_MsgGT200)p_MsgGT200.GT200_Tab.elementAt(i)).Evt_Dcm.toString();
          pcStr = pcStr + ((Bff_MsgGT200)p_MsgGT200.GT200_Tab.elementAt(i)).Tmt_Pgm.toString();
          pcStr = pcStr + ((Bff_MsgGT200)p_MsgGT200.GT200_Tab.elementAt(i)).Evt_Ttr.toString();
          pcStr = pcStr + ((Bff_MsgGT200)p_MsgGT200.GT200_Tab.elementAt(i)).Tmt_Est.toString();
        }
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
}