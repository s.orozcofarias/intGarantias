// Source File Name:   GT_IMG.java

package com.intGarantias.modules.global;

import com.FHTServlet.modules.global.BF_LOB;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.util.Formateo;

import java.util.*;
import java.lang.*;
import org.apache.turbine.util.Log;

public class GT_IMG
{
  //===============================================================================================================================
  public GT_IMG()
  {
  }
  //===============================================================================================================================
  public static class Buf_Img_Visad
  {
    public StringBuffer Visad_Ggr = new StringBuffer(3  );  //X(03)          Grupo Garantia (HYP/CRH/AYF/CTW/DMN/DMX/PMN/PMX)
    public StringBuffer Visad_Dsg = new StringBuffer(100);  //X(100)         Descripcion Garantia
    public StringBuffer Visad_Fpr = new StringBuffer(8  );  //9(08)          Fecha Presentacion
    public StringBuffer Visad_Vle = new StringBuffer(15 );  //9(13)V9(2)     Valor Comercial
    public StringBuffer Visad_Fig = new StringBuffer(8  );  //9(08)          FIngreso Garantia
    public StringBuffer Visad_Cbt = new StringBuffer(1  );  //X(01)          Cobertura (Gral/Espec)
    public StringBuffer Visad_Vcn = new StringBuffer(15 );  //9(13)V9(2)     Valor Contable <Mnd>
    public StringBuffer Visad_Ttr = new StringBuffer(5  );  //X(05)          CodTrans UMovto
    public StringBuffer Visad_Tum = new StringBuffer(7  );  //9(07)          Transaccion UMovto

    public Formateo f = new Formateo();
    public String getGgr()         {return Visad_Ggr.toString();}
    public String getDsg()         {return Visad_Dsg.toString();}
    public String fgetFpr()        {return f.FormatDate(Visad_Fpr.toString());}
    public String fgetVle()        {return f.Formato(Visad_Vle.toString(),"99.999.999.999",2,2,',');}
    public String fgetFig()        {return f.FormatDate(Visad_Fig.toString());}
    public String getCbt()         {return Visad_Cbt.toString();}
    public String fgetVcn()        {return f.Formato(Visad_Vcn.toString(),"99.999.999.999",2,2,',');}
    public String getTtr()         {return Visad_Ttr.toString();}
    public String getTum()         {return Visad_Tum.toString();}
  }
  //===============================================================================================================================
  public static class Buf_Img_GtApe
  {
    public StringBuffer GtApe_Bse = new StringBuffer(358);  //X(358)     358 Area Base Fija
    public StringBuffer GtApe_Fpr = new StringBuffer(8  );  //9(08)      366 Fecha Presentacion
  //public StringBuffer GtApe_Vle = new StringBuffer(15 );  //9(11)V9(4) 381 Valor Estimado
    public StringBuffer GtApe_Rgf = new StringBuffer(2  );  //X(02)      368 Requiere GFU (SI/NO)
    public StringBuffer GtApe_Gfu = new StringBuffer(13 );  //X(13)      381 id Bien Garantia Futura
    public StringBuffer GtApe_Ftc = new StringBuffer(8  );  //9(08)      389 Fecha Tope Constitucion
    public StringBuffer GtApe_Cbp = new StringBuffer(5  );  //X(05)      394 Codigo Cargo Gastos
    public StringBuffer GtApe_Dbp = new StringBuffer(12 );  //9(12)      406 Cuenta Cargo Gastos
    public StringBuffer GtApe_Tob = new StringBuffer(1  );  //X(01)      407 DefCobertura (Sel/Gral/Espec)
    public StringBuffer GtApe_Plm = new StringBuffer(1  );  //X(01)      408 PermiteLimitacion (S/N)
    public StringBuffer GtApe_Ggr = new StringBuffer(3  );  //X(03)      411 Grupo Garantia (HYP/CRH/AYF/CTW/DMN/DMX/PMN/PMX)
    public StringBuffer GtApe_Dsg = new StringBuffer(100);  //X(100)     511 Descripcion Garantia
    public StringBuffer GtApe_Cbt = new StringBuffer(1  );  //X(01)      512 Cobertura (Gral/Espec)
    public StringBuffer GtApe_Grd = new StringBuffer(1  );  //X(01)      513 Grado (1/N)
    public StringBuffer GtApe_Cmp = new StringBuffer(1  );  //X(01)      514 Idr.Compartida (S/N)
    public StringBuffer GtApe_Sgr = new StringBuffer(1  );  //X(01)      515 Idr.Seguros (S/N)
    public StringBuffer GtApe_Ctb = new StringBuffer(1  );  //X(01)      516 Idr.Contibuciones (S/N)
    public StringBuffer GtApe_Ilm = new StringBuffer(1  );  //X(01)      517 Idr Limitada (L/N/?)
    public StringBuffer GtApe_Pcl = new StringBuffer(5  );  //9(03)V9(2) 522 Porcentaje Limitacion
    public StringBuffer GtApe_Mtl = new StringBuffer(15 );  //9(13)V9(2) 537 Valor Limitacion
    public StringBuffer GtApe_Fig = new StringBuffer(8  );  //9(08)      545 FIngreso Garantia
    public StringBuffer GtApe_Tsd = new StringBuffer(3  );  //9(03)      548 Tasador
    public StringBuffer GtApe_Fts = new StringBuffer(8  );  //9(08)      556 Fecha Tasacion
    public StringBuffer GtApe_Vuo = new StringBuffer(9  );  //9(07)V9(2) 565 Valor Conversion <Fts>
    public StringBuffer GtApe_Vtt = new StringBuffer(15 );  //9(11)V9(4) 580 Valor Tasacion <Trj>
    public StringBuffer GtApe_Vts = new StringBuffer(15 );  //9(13)V9(2) 595 Valor Tasacion <Mnd>
    public StringBuffer GtApe_Vtb = new StringBuffer(15 );  //9(13)V9(2) 610 Valor Banco <Mnd>
    public StringBuffer GtApe_Vcn = new StringBuffer(15 );  //9(13)V9(2) 625 Valor Contable <Mnd>
    public StringBuffer GtApe_Cbg = new StringBuffer(4  );  //9(04)      629 CodAlmacen
    public StringBuffer GtApe_Nct = new StringBuffer(7  );  //9(07)      636 NumCertificado
    public StringBuffer GtApe_Fct = new StringBuffer(8  );  //9(08)      644 FCertificado
    public StringBuffer GtApe_Fvt = new StringBuffer(8  );  //9(08)      652 FVencto Tasacion/Certificado
    public StringBuffer GtApe_Vuf = new StringBuffer(9  );  //9(07)V9(2) 661 Valor UF <Fts>
    public StringBuffer GtApe_Vus = new StringBuffer(9  );  //9(07)V9(2) 670 Valor US$ <Fts>
    public StringBuffer GtApe_Hvt = new StringBuffer(15 );  //9(13)V9(2) 685 VTasacion [HIP]
    public StringBuffer GtApe_Hvl = new StringBuffer(15 );  //9(13)V9(2) 700 VLiquidacion [HIP]
    public StringBuffer GtApe_Hvb = new StringBuffer(15 );  //9(13)V9(2) 715 VBanco [HIP]
    public StringBuffer GtApe_Hvc = new StringBuffer(15 );  //9(13)V9(2) 730 VContable [HIP]
    public StringBuffer GtApe_Pvt = new StringBuffer(15 );  //9(13)V9(2) 745 VTasacion [PRD]
    public StringBuffer GtApe_Pvl = new StringBuffer(15 );  //9(13)V9(2) 760 VLiquidacion [PRD]
    public StringBuffer GtApe_Pvb = new StringBuffer(15 );  //9(13)V9(2) 775 VBanco [PRD]
    public StringBuffer GtApe_Pvc = new StringBuffer(15 );  //9(13)V9(2) 790 VContable [PRD]
    public StringBuffer GtApe_Psw = new StringBuffer(1  );  //X(01)      791 PI Idr Nuevo/Modifica
    public StringBuffer GtApe_Psq = new StringBuffer(3  );  //9(03)      794 PI Seq
    public StringBuffer GtApe_Psd = new StringBuffer(3  );  //9(03)      797 PI Sqd
    public StringBuffer GtApe_Idx = new StringBuffer(3  );  //9(03)      800 Indice Activo Detalles
    public StringBuffer GtApe_Tfr = new StringBuffer(3  );  //9(03)      803 Total Indices Detalles
    public StringBuffer GtApe_Dpv = new StringBuffer(10 );  //X(10)      813 Codigo DPC para Valores
    public StringBuffer GtApe_Dpp = new StringBuffer(1  );  //X(01)      814 Tipo DPC (V,W,A) para Valores
    public StringBuffer GtApe_Dsw = new StringBuffer(1  );  //X(01)      815 Sw DPC (I,N) para Valores
    public StringBuffer GtApe_Pct = new StringBuffer(5  );  //9(03)V9(2) 820 %Castigo SBIF Valores
    public StringBuffer GtApe_Ulm = new StringBuffer(3  );  //X(03)      823 UMontaria Limitada (NO/UF/TC)
    public StringBuffer GtApe_Fll = new StringBuffer(4  );  //X(04)      827 Disponible

    public String getBse()         {return GtApe_Bse.toString();}
    public String getFpr()         {return GtApe_Fpr.toString();}
    public String getRgf()         {return GtApe_Rgf.toString();}
    public String getGfu()         {return GtApe_Gfu.toString();}
    public String getFtc()         {return GtApe_Ftc.toString();}
    public String getCbp()         {return GtApe_Cbp.toString();}
    public String getDbp()         {return GtApe_Dbp.toString();}
    public String getTob()         {return GtApe_Tob.toString();}
    public String getPlm()         {return GtApe_Plm.toString();}
    public String getGgr()         {return GtApe_Ggr.toString();}
    public String getDsg()         {return GtApe_Dsg.toString();}
    public String getCbt()         {return GtApe_Cbt.toString();}
    public String getGrd()         {return GtApe_Grd.toString();}
    public String getCmp()         {return GtApe_Cmp.toString();}
    public String getSgr()         {return GtApe_Sgr.toString();}
    public String getCtb()         {return GtApe_Ctb.toString();}
    public String getIlm()         {return GtApe_Ilm.toString();}
    public String getPcl()         {return GtApe_Pcl.toString();}
    public String getMtl()         {return GtApe_Mtl.toString();}
    public String getFig()         {return GtApe_Fig.toString();}
    public String getTsd()         {return GtApe_Tsd.toString();}
    public String getFts()         {return GtApe_Fts.toString();}
    public String getVuo()         {return GtApe_Vuo.toString();}
    public String getVtt()         {return GtApe_Vtt.toString();}
    public String getVts()         {return GtApe_Vts.toString();}
    public String getVtb()         {return GtApe_Vtb.toString();}
    public String getVcn()         {return GtApe_Vcn.toString();}
    public String getCbg()         {return GtApe_Cbg.toString();}
    public String getNct()         {return GtApe_Nct.toString();}
    public String getFct()         {return GtApe_Fct.toString();}
    public String getFvt()         {return GtApe_Fvt.toString();}
    public String getVuf()         {return GtApe_Vuf.toString();}
    public String getVus()         {return GtApe_Vus.toString();}
    public String getHvt()         {return GtApe_Hvt.toString();}
    public String getHvl()         {return GtApe_Hvl.toString();}
    public String getHvb()         {return GtApe_Hvb.toString();}
    public String getHvc()         {return GtApe_Hvc.toString();}
    public String getPvt()         {return GtApe_Pvt.toString();}
    public String getPvl()         {return GtApe_Pvl.toString();}
    public String getPvb()         {return GtApe_Pvb.toString();}
    public String getPvc()         {return GtApe_Pvc.toString();}
    public String getPsw()         {return GtApe_Psw.toString();}
    public String getPsq()         {return GtApe_Psq.toString();}
    public String getPsd()         {return GtApe_Psd.toString();}
    public String getIdx()         {return GtApe_Idx.toString();}
    public String getTfr()         {return GtApe_Tfr.toString();}
    public String getDpv()         {return GtApe_Dpv.toString();}
    public String getDpp()         {return GtApe_Dpp.toString();}
    public String getDsw()         {return GtApe_Dsw.toString();}
    public String getPct()         {return GtApe_Pct.toString();}
    public String getUlm()         {return GtApe_Ulm.toString();}
    public String getFll()         {return GtApe_Fll.toString();}

    public Formateo f = new Formateo();
    public String fgetFpr()        {return f.FormatDate(GtApe_Fpr.toString());}
    public String fgetFtc()        {return f.FormatDate(GtApe_Ftc.toString());}
    public String fgetDbp()        {return f.EditCta(GtApe_Dbp.toString());}
    public String fgetDsg()        {return GtApe_Dsg.toString().trim();}
    public String fgetCbt()        {return GtApe_Cbt.toString().trim();}
    public String fgetPcl()        {return f.Formato(GtApe_Pcl.toString(),"999",2,2,',');}
    public String fgetMtl()        {return f.Formato(GtApe_Mtl.toString(),"99.999.999.999",2,2,',');}
    public String fgetFig()        {return f.FormatDate(GtApe_Fig.toString());}
    public String fgetFts()        {return f.FormatDate(GtApe_Fts.toString());}
    public String fgetVuo()        {return f.Formato(GtApe_Vuo.toString(),"9.999.999",2,2,',');}
    public String fgetVtt()        {return f.Formato(GtApe_Vtt.toString(),"99.999.999.999",4,4,',');}
    public String fgetVts()        {return f.Formato(GtApe_Vts.toString(),"99.999.999.999",2,2,',');}
    public String fgetVtb()        {return f.Formato(GtApe_Vtb.toString(),"99.999.999.999",2,2,',');}
    public String fgetVcn()        {return f.Formato(GtApe_Vcn.toString(),"99.999.999.999",2,2,',');}
    public String egetVts()        {return f.Formato(GtApe_Vts.toString(),"99.999.999.999",2,0,',');}
    public String egetVtb()        {return f.Formato(GtApe_Vtb.toString(),"99.999.999.999",2,0,',');}
    public String egetVcn()        {return f.Formato(GtApe_Vcn.toString(),"99.999.999.999",2,0,',');}
    public String fgetNct()        {return GtApe_Nct.toString().trim();}
    public String fgetFct()        {return f.FormatDate(GtApe_Fct.toString());}
    public String fgetFvt()        {return f.FormatDate(GtApe_Fvt.toString());}
    public String fgetVuf()        {return f.Formato(GtApe_Vuf.toString(),"9.999.999",2,2,',');}
    public String fgetVus()        {return f.Formato(GtApe_Vus.toString(),"9.999.999",2,2,',');}
    public String pgetHvt()        {return f.Formato(GtApe_Hvt.toString(),"BBB.BBB.BBB.BB9",2,2,',');}
    public String pgetHvl()        {return f.Formato(GtApe_Hvl.toString(),"BBB.BBB.BBB.BB9",2,2,',');}
    public String pgetHvb()        {return f.Formato(GtApe_Hvb.toString(),"BBB.BBB.BBB.BB9",2,2,',');}
    public String pgetHvc()        {return f.Formato(GtApe_Hvc.toString(),"BBB.BBB.BBB.BB9",2,2,',');}
    public String pgetPvt()        {return f.Formato(GtApe_Pvt.toString(),"BBB.BBB.BBB.BB9",2,2,',');}
    public String pgetPvl()        {return f.Formato(GtApe_Pvl.toString(),"BBB.BBB.BBB.BB9",2,2,',');}
    public String pgetPvb()        {return f.Formato(GtApe_Pvb.toString(),"BBB.BBB.BBB.BB9",2,2,',');}
    public String pgetPvc()        {return f.Formato(GtApe_Pvc.toString(),"BBB.BBB.BBB.BB9",2,2,',');}
    public int    igetIdx()        {
                                    if (getIdx().trim().equals(""))
                                       { return Integer.parseInt("1"); }
                                    else
                                       { return Integer.parseInt(GtApe_Idx.toString()); }
                                   }
    public String f4getVtt()       {return f.Formato(GtApe_Vtt.toString(),"99.999.999.999",4,4,',');}
    public String f2getVtt()       {return f.Formato(GtApe_Vtt.toString(),"99.999.999.999",4,2,',');}
    public String f0getVtt()       {return f.Formato(GtApe_Vtt.toString(),"99.999.999.999",4,0,',');}
    public String fgetGfu()        {
                                    if (getGfu().trim().equals(""))
                                       { return ""; }
                                    else
                                       { return getGfu().substring(0,3)+"-"+getGfu().substring(3,10)+"."+getGfu().substring(10,13); }
                                   }
  }
  //---------------------------------------------------------------------------------------
  public static Buf_Img_GtApe LSet_A_ImgGtApe(String pcStr)
  {
    Buf_Img_GtApe l_Img = new Buf_Img_GtApe();
    Vector vDatos = LSet_A_vImgGtApe(pcStr);
    l_Img = (Buf_Img_GtApe)vDatos.elementAt(0);
    return l_Img;
  }
  //---------------------------------------------------------------------------------------
  public static Vector LSet_A_vImgGtApe(String pcStr)
  {
    int p = 0;
    Vector vec = new Vector();
    Buf_Img_GtApe t = new Buf_Img_GtApe();
    t.GtApe_Bse.replace(0, t.GtApe_Bse.capacity(), pcStr.substring(p, p + t.GtApe_Bse.capacity())); p = p + t.GtApe_Bse.capacity();
    t.GtApe_Fpr.replace(0, t.GtApe_Fpr.capacity(), pcStr.substring(p, p + t.GtApe_Fpr.capacity())); p = p + t.GtApe_Fpr.capacity();
    t.GtApe_Rgf.replace(0, t.GtApe_Rgf.capacity(), pcStr.substring(p, p + t.GtApe_Rgf.capacity())); p = p + t.GtApe_Rgf.capacity();
    t.GtApe_Gfu.replace(0, t.GtApe_Gfu.capacity(), pcStr.substring(p, p + t.GtApe_Gfu.capacity())); p = p + t.GtApe_Gfu.capacity();
    t.GtApe_Ftc.replace(0, t.GtApe_Ftc.capacity(), pcStr.substring(p, p + t.GtApe_Ftc.capacity())); p = p + t.GtApe_Ftc.capacity();
    t.GtApe_Cbp.replace(0, t.GtApe_Cbp.capacity(), pcStr.substring(p, p + t.GtApe_Cbp.capacity())); p = p + t.GtApe_Cbp.capacity();
    t.GtApe_Dbp.replace(0, t.GtApe_Dbp.capacity(), pcStr.substring(p, p + t.GtApe_Dbp.capacity())); p = p + t.GtApe_Dbp.capacity();
    t.GtApe_Tob.replace(0, t.GtApe_Tob.capacity(), pcStr.substring(p, p + t.GtApe_Tob.capacity())); p = p + t.GtApe_Tob.capacity();
    t.GtApe_Plm.replace(0, t.GtApe_Plm.capacity(), pcStr.substring(p, p + t.GtApe_Plm.capacity())); p = p + t.GtApe_Plm.capacity();
    t.GtApe_Ggr.replace(0, t.GtApe_Ggr.capacity(), pcStr.substring(p, p + t.GtApe_Ggr.capacity())); p = p + t.GtApe_Ggr.capacity();
    t.GtApe_Dsg.replace(0, t.GtApe_Dsg.capacity(), pcStr.substring(p, p + t.GtApe_Dsg.capacity())); p = p + t.GtApe_Dsg.capacity();
    t.GtApe_Cbt.replace(0, t.GtApe_Cbt.capacity(), pcStr.substring(p, p + t.GtApe_Cbt.capacity())); p = p + t.GtApe_Cbt.capacity();
    t.GtApe_Grd.replace(0, t.GtApe_Grd.capacity(), pcStr.substring(p, p + t.GtApe_Grd.capacity())); p = p + t.GtApe_Grd.capacity();
    t.GtApe_Cmp.replace(0, t.GtApe_Cmp.capacity(), pcStr.substring(p, p + t.GtApe_Cmp.capacity())); p = p + t.GtApe_Cmp.capacity();
    t.GtApe_Sgr.replace(0, t.GtApe_Sgr.capacity(), pcStr.substring(p, p + t.GtApe_Sgr.capacity())); p = p + t.GtApe_Sgr.capacity();
    t.GtApe_Ctb.replace(0, t.GtApe_Ctb.capacity(), pcStr.substring(p, p + t.GtApe_Ctb.capacity())); p = p + t.GtApe_Ctb.capacity();
    t.GtApe_Ilm.replace(0, t.GtApe_Ilm.capacity(), pcStr.substring(p, p + t.GtApe_Ilm.capacity())); p = p + t.GtApe_Ilm.capacity();
    t.GtApe_Pcl.replace(0, t.GtApe_Pcl.capacity(), pcStr.substring(p, p + t.GtApe_Pcl.capacity())); p = p + t.GtApe_Pcl.capacity();
    t.GtApe_Mtl.replace(0, t.GtApe_Mtl.capacity(), pcStr.substring(p, p + t.GtApe_Mtl.capacity())); p = p + t.GtApe_Mtl.capacity();
    t.GtApe_Fig.replace(0, t.GtApe_Fig.capacity(), pcStr.substring(p, p + t.GtApe_Fig.capacity())); p = p + t.GtApe_Fig.capacity();
    t.GtApe_Tsd.replace(0, t.GtApe_Tsd.capacity(), pcStr.substring(p, p + t.GtApe_Tsd.capacity())); p = p + t.GtApe_Tsd.capacity();
    t.GtApe_Fts.replace(0, t.GtApe_Fts.capacity(), pcStr.substring(p, p + t.GtApe_Fts.capacity())); p = p + t.GtApe_Fts.capacity();
    t.GtApe_Vuo.replace(0, t.GtApe_Vuo.capacity(), pcStr.substring(p, p + t.GtApe_Vuo.capacity())); p = p + t.GtApe_Vuo.capacity();
    t.GtApe_Vtt.replace(0, t.GtApe_Vtt.capacity(), pcStr.substring(p, p + t.GtApe_Vtt.capacity())); p = p + t.GtApe_Vtt.capacity();
    t.GtApe_Vts.replace(0, t.GtApe_Vts.capacity(), pcStr.substring(p, p + t.GtApe_Vts.capacity())); p = p + t.GtApe_Vts.capacity();
    t.GtApe_Vtb.replace(0, t.GtApe_Vtb.capacity(), pcStr.substring(p, p + t.GtApe_Vtb.capacity())); p = p + t.GtApe_Vtb.capacity();
    t.GtApe_Vcn.replace(0, t.GtApe_Vcn.capacity(), pcStr.substring(p, p + t.GtApe_Vcn.capacity())); p = p + t.GtApe_Vcn.capacity();
    t.GtApe_Cbg.replace(0, t.GtApe_Cbg.capacity(), pcStr.substring(p, p + t.GtApe_Cbg.capacity())); p = p + t.GtApe_Cbg.capacity();
    t.GtApe_Nct.replace(0, t.GtApe_Nct.capacity(), pcStr.substring(p, p + t.GtApe_Nct.capacity())); p = p + t.GtApe_Nct.capacity();
    t.GtApe_Fct.replace(0, t.GtApe_Fct.capacity(), pcStr.substring(p, p + t.GtApe_Fct.capacity())); p = p + t.GtApe_Fct.capacity();
    t.GtApe_Fvt.replace(0, t.GtApe_Fvt.capacity(), pcStr.substring(p, p + t.GtApe_Fvt.capacity())); p = p + t.GtApe_Fvt.capacity();
    t.GtApe_Vuf.replace(0, t.GtApe_Vuf.capacity(), pcStr.substring(p, p + t.GtApe_Vuf.capacity())); p = p + t.GtApe_Vuf.capacity();
    t.GtApe_Vus.replace(0, t.GtApe_Vus.capacity(), pcStr.substring(p, p + t.GtApe_Vus.capacity())); p = p + t.GtApe_Vus.capacity();
    t.GtApe_Hvt.replace(0, t.GtApe_Hvt.capacity(), pcStr.substring(p, p + t.GtApe_Hvt.capacity())); p = p + t.GtApe_Hvt.capacity();
    t.GtApe_Hvl.replace(0, t.GtApe_Hvl.capacity(), pcStr.substring(p, p + t.GtApe_Hvl.capacity())); p = p + t.GtApe_Hvl.capacity();
    t.GtApe_Hvb.replace(0, t.GtApe_Hvb.capacity(), pcStr.substring(p, p + t.GtApe_Hvb.capacity())); p = p + t.GtApe_Hvb.capacity();
    t.GtApe_Hvc.replace(0, t.GtApe_Hvc.capacity(), pcStr.substring(p, p + t.GtApe_Hvc.capacity())); p = p + t.GtApe_Hvc.capacity();
    t.GtApe_Pvt.replace(0, t.GtApe_Pvt.capacity(), pcStr.substring(p, p + t.GtApe_Pvt.capacity())); p = p + t.GtApe_Pvt.capacity();
    t.GtApe_Pvl.replace(0, t.GtApe_Pvl.capacity(), pcStr.substring(p, p + t.GtApe_Pvl.capacity())); p = p + t.GtApe_Pvl.capacity();
    t.GtApe_Pvb.replace(0, t.GtApe_Pvb.capacity(), pcStr.substring(p, p + t.GtApe_Pvb.capacity())); p = p + t.GtApe_Pvb.capacity();
    t.GtApe_Pvc.replace(0, t.GtApe_Pvc.capacity(), pcStr.substring(p, p + t.GtApe_Pvc.capacity())); p = p + t.GtApe_Pvc.capacity();
    t.GtApe_Psw.replace(0, t.GtApe_Psw.capacity(), pcStr.substring(p, p + t.GtApe_Psw.capacity())); p = p + t.GtApe_Psw.capacity();
    t.GtApe_Psq.replace(0, t.GtApe_Psq.capacity(), pcStr.substring(p, p + t.GtApe_Psq.capacity())); p = p + t.GtApe_Psq.capacity();
    t.GtApe_Psd.replace(0, t.GtApe_Psd.capacity(), pcStr.substring(p, p + t.GtApe_Psd.capacity())); p = p + t.GtApe_Psd.capacity();
    t.GtApe_Idx.replace(0, t.GtApe_Idx.capacity(), pcStr.substring(p, p + t.GtApe_Idx.capacity())); p = p + t.GtApe_Idx.capacity();
    t.GtApe_Tfr.replace(0, t.GtApe_Tfr.capacity(), pcStr.substring(p, p + t.GtApe_Tfr.capacity())); p = p + t.GtApe_Tfr.capacity();
    t.GtApe_Dpv.replace(0, t.GtApe_Dpv.capacity(), pcStr.substring(p, p + t.GtApe_Dpv.capacity())); p = p + t.GtApe_Dpv.capacity();
    t.GtApe_Dpp.replace(0, t.GtApe_Dpp.capacity(), pcStr.substring(p, p + t.GtApe_Dpp.capacity())); p = p + t.GtApe_Dpp.capacity();
    t.GtApe_Dsw.replace(0, t.GtApe_Dsw.capacity(), pcStr.substring(p, p + t.GtApe_Dsw.capacity())); p = p + t.GtApe_Dsw.capacity();
    t.GtApe_Pct.replace(0, t.GtApe_Pct.capacity(), pcStr.substring(p, p + t.GtApe_Pct.capacity())); p = p + t.GtApe_Pct.capacity();
    t.GtApe_Ulm.replace(0, t.GtApe_Ulm.capacity(), pcStr.substring(p, p + t.GtApe_Ulm.capacity())); p = p + t.GtApe_Ulm.capacity();
    t.GtApe_Fll.replace(0, t.GtApe_Fll.capacity(), pcStr.substring(p, p + t.GtApe_Fll.capacity())); p = p + t.GtApe_Fll.capacity();
    vec.add ( t );
    return vec;
  }
  //---------------------------------------------------------------------------------------
  public static String LSet_De_ImgGtApe(Buf_Img_GtApe p_Img)
  {
    String pcStr = "";
    pcStr = pcStr + p_Img.getBse();
    pcStr = pcStr + p_Img.getFpr();
    pcStr = pcStr + p_Img.getRgf();
    pcStr = pcStr + p_Img.getGfu();
    pcStr = pcStr + p_Img.getFtc();
    pcStr = pcStr + p_Img.getCbp();
    pcStr = pcStr + p_Img.getDbp();
    pcStr = pcStr + p_Img.getTob();
    pcStr = pcStr + p_Img.getPlm();
    pcStr = pcStr + p_Img.getGgr();
    pcStr = pcStr + p_Img.getDsg();
    pcStr = pcStr + p_Img.getCbt();
    pcStr = pcStr + p_Img.getGrd();
    pcStr = pcStr + p_Img.getCmp();
    pcStr = pcStr + p_Img.getSgr();
    pcStr = pcStr + p_Img.getCtb();
    pcStr = pcStr + p_Img.getIlm();
    pcStr = pcStr + p_Img.getPcl();
    pcStr = pcStr + p_Img.getMtl();
    pcStr = pcStr + p_Img.getFig();
    pcStr = pcStr + p_Img.getTsd();
    pcStr = pcStr + p_Img.getFts();
    pcStr = pcStr + p_Img.getVuo();
    pcStr = pcStr + p_Img.getVtt();
    pcStr = pcStr + p_Img.getVts();
    pcStr = pcStr + p_Img.getVtb();
    pcStr = pcStr + p_Img.getVcn();
    pcStr = pcStr + p_Img.getCbg();
    pcStr = pcStr + p_Img.getNct();
    pcStr = pcStr + p_Img.getFct();
    pcStr = pcStr + p_Img.getFvt();
    pcStr = pcStr + p_Img.getVuf();
    pcStr = pcStr + p_Img.getVus();
    pcStr = pcStr + p_Img.getHvt();
    pcStr = pcStr + p_Img.getHvl();
    pcStr = pcStr + p_Img.getHvb();
    pcStr = pcStr + p_Img.getHvc();
    pcStr = pcStr + p_Img.getPvt();
    pcStr = pcStr + p_Img.getPvl();
    pcStr = pcStr + p_Img.getPvb();
    pcStr = pcStr + p_Img.getPvc();
    pcStr = pcStr + p_Img.getPsw();
    pcStr = pcStr + p_Img.getPsq();
    pcStr = pcStr + p_Img.getPsd();
    pcStr = pcStr + p_Img.getIdx();
    pcStr = pcStr + p_Img.getTfr();
    pcStr = pcStr + p_Img.getDpv();
    pcStr = pcStr + p_Img.getDpp();
    pcStr = pcStr + p_Img.getDsw();
    pcStr = pcStr + p_Img.getPct();
    pcStr = pcStr + p_Img.getUlm();
    pcStr = pcStr + p_Img.getFll();
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
  public static class Buf_Img_GtTsd
  {
    public StringBuffer GtTsd_Nc1 = new StringBuffer(40 );  //X(40)       40 Nombre Contacto 1
    public StringBuffer GtTsd_T11 = new StringBuffer(10 );  //X(10)       50 Telefono 1 Contacto 1
    public StringBuffer GtTsd_Cll = new StringBuffer(34 );  //X(34)       84 Dmc.Calle
    public StringBuffer GtTsd_Cnm = new StringBuffer(6  );  //X(06)       90 Dmc.CNumero
    public StringBuffer GtTsd_Lug = new StringBuffer(2  );  //X(02)       92 Dmc.Lugar
    public StringBuffer GtTsd_Lnm = new StringBuffer(6  );  //X(06)       98 Dmc.LNumero
    public StringBuffer GtTsd_Sec = new StringBuffer(2  );  //X(02)      100 Dmc.Sector
    public StringBuffer GtTsd_Snb = new StringBuffer(20 );  //X(20)      120 Dmc.SNombre
    public StringBuffer GtTsd_Cmn = new StringBuffer(11 );  //9(11)      131 Dmc.CodComuna
    public StringBuffer GtTsd_Ncm = new StringBuffer(20 );  //X(20)      151 Dmc.NomComuna
    public StringBuffer GtTsd_Npv = new StringBuffer(17 );  //X(17)      168 Dmc.NomProvincia
    public StringBuffer GtTsd_Opt = new StringBuffer(184);  //X(184)     352 Obs para Tasador
    public StringBuffer GtTsd_Tsd = new StringBuffer(3  );  //9(03)      355 Tasador
    public StringBuffer GtTsd_Nts = new StringBuffer(25 );  //X(25)      380 Nombre Tasador
    public StringBuffer GtTsd_Bof = new StringBuffer(1  );  //X(01)      381 Boleta/Factura
    public StringBuffer GtTsd_Cct = new StringBuffer(12 );  //9(12)      393 CtaCte Tasador
    public StringBuffer GtTsd_Rts = new StringBuffer(10 );  //X(10)      403 RUT Tasador
    public StringBuffer GtTsd_Fts = new StringBuffer(8  );  //9(08)      411 FTasacion
    public StringBuffer GtTsd_Vuf = new StringBuffer(9  );  //9(07)V9(2) 420 Valor UF a Fts
    public StringBuffer GtTsd_Vtc = new StringBuffer(9  );  //9(07)V9(2) 429 Valor TC a Fts
    public StringBuffer GtTsd_Ftv = new StringBuffer(8  );  //9(08)      437 FVcto Tasacion
    public StringBuffer GtTsd_Vts = new StringBuffer(15 );  //9(13)V9(2) 452 Valor Tasacion
    public StringBuffer GtTsd_Ibf = new StringBuffer(1  );  //X(01)      453 Idr Boleta/Factura
    public StringBuffer GtTsd_Nbf = new StringBuffer(10 );  //9(10)      463 Nro Boleta/Factura
    public StringBuffer GtTsd_Pcb = new StringBuffer(5  );  //9(03)V9(2) 468 %Comis.BCO
    public StringBuffer GtTsd_Hon = new StringBuffer(11 );  //9(11)      479 Honorarios Tasador
    public StringBuffer GtTsd_Vst = new StringBuffer(11 );  //9(11)      490 Gasto Tasacion Pesos
    public StringBuffer GtTsd_Obt = new StringBuffer(128);  //X(128)     618 Obs del Tasador
    public StringBuffer GtTsd_Ept = new StringBuffer(5  );  //X(05)      623 Especialidad Tsd
    public StringBuffer GtTsd_T21 = new StringBuffer(10 );  //X(10)      633 Telefono 2 Contacto 1
    public StringBuffer GtTsd_T31 = new StringBuffer(10 );  //X(10)      643 Telefono 3 Contacto 1
    public StringBuffer GtTsd_Nc2 = new StringBuffer(40 );  //X(40)      683 Nombre Contacto 2
    public StringBuffer GtTsd_T12 = new StringBuffer(10 );  //X(10)      693 Telefono 1 Contacto 2
    public StringBuffer GtTsd_T22 = new StringBuffer(10 );  //X(10)      703 Telefono 2 Contacto 2
    public StringBuffer GtTsd_T32 = new StringBuffer(10 );  //X(10)      713 Telefono 3 Contacto 2
    public StringBuffer GtTsd_Nc3 = new StringBuffer(40 );  //X(40)      753 Nombre Contacto 3
    public StringBuffer GtTsd_T13 = new StringBuffer(10 );  //X(10)      763 Telefono 1 Contacto 3
    public StringBuffer GtTsd_T23 = new StringBuffer(10 );  //X(10)      773 Telefono 2 Contacto 3
    public StringBuffer GtTsd_T33 = new StringBuffer(10 );  //X(10)      783 Telefono 3 Contacto 3
  //public StringBuffer GtTsd_Fll = new StringBuffer(44 );  //X(44)      827 Disponible
    public StringBuffer GtTsd_Dot = new StringBuffer(9  );  //9(09)      792 @Obs para Tasador
    public StringBuffer GtTsd_Dob = new StringBuffer(9  );  //9(09)      801 @Obs del Tasador
    public StringBuffer GtTsd_Fll = new StringBuffer(26 );  //X(26)      827 Disponible

    public String getNc1()         {return GtTsd_Nc1.toString();}
    public String getT11()         {return GtTsd_T11.toString();}
    public String getCll()         {return GtTsd_Cll.toString();}
    public String getCnm()         {return GtTsd_Cnm.toString();}
    public String getLug()         {return GtTsd_Lug.toString();}
    public String getLnm()         {return GtTsd_Lnm.toString();}
    public String getSec()         {return GtTsd_Sec.toString();}
    public String getSnb()         {return GtTsd_Snb.toString();}
    public String getCmn()         {return GtTsd_Cmn.toString();}
    public String getNcm()         {return GtTsd_Ncm.toString();}
    public String getNpv()         {return GtTsd_Npv.toString();}
    public String getOpt()         {return GtTsd_Opt.toString();}
    public String getTsd()         {return GtTsd_Tsd.toString();}
    public String getNts()         {return GtTsd_Nts.toString();}
    public String getBof()         {return GtTsd_Bof.toString();}
    public String getCct()         {return GtTsd_Cct.toString();}
    public String getRts()         {return GtTsd_Rts.toString();}
    public String getFts()         {return GtTsd_Fts.toString();}
    public String getVuf()         {return GtTsd_Vuf.toString();}
    public String getVtc()         {return GtTsd_Vtc.toString();}
    public String getFtv()         {return GtTsd_Ftv.toString();}
    public String getVts()         {return GtTsd_Vts.toString();}
    public String getIbf()         {return GtTsd_Ibf.toString();}
    public String getNbf()         {return GtTsd_Nbf.toString();}
    public String getPcb()         {return GtTsd_Pcb.toString();}
    public String getHon()         {return GtTsd_Hon.toString();}
    public String getVst()         {return GtTsd_Vst.toString();}
    public String getObt()         {return GtTsd_Obt.toString();}
    public String getEpt()         {return GtTsd_Ept.toString();}
    public String getT21()         {return GtTsd_T21.toString();}
    public String getT31()         {return GtTsd_T31.toString();}
    public String getNc2()         {return GtTsd_Nc2.toString();}
    public String getT12()         {return GtTsd_T12.toString();}
    public String getT22()         {return GtTsd_T22.toString();}
    public String getT32()         {return GtTsd_T32.toString();}
    public String getNc3()         {return GtTsd_Nc3.toString();}
    public String getT13()         {return GtTsd_T13.toString();}
    public String getT23()         {return GtTsd_T23.toString();}
    public String getT33()         {return GtTsd_T33.toString();}
    public String getDot()         {return GtTsd_Dot.toString();}
    public String getDob()         {return GtTsd_Dob.toString();}
    public String getFll()         {return GtTsd_Fll.toString();}

    public Formateo f = new Formateo();
    public String fgetLug()        {return GtTsd_Lug.toString().trim();}
    public String fgetSec()        {return GtTsd_Sec.toString().trim();}
    public String fgetNcm()        {return GtTsd_Ncm.toString().trim();}
    public String fgetFts()        {return f.FormatDate(GtTsd_Fts.toString());}
    public String fgetVuf()        {return f.Formato(GtTsd_Vuf.toString(),"99.999.999.999",2,2,',');}
    public String fgetVtc()        {return f.Formato(GtTsd_Vtc.toString(),"99.999.999.999",2,2,',');}
    public String getDomicilio()
                                   {
                                     String lcTemp= GtTsd_Cll.toString().trim() + " " + GtTsd_Cnm.toString().trim() + " ";
                                     if ( GtTsd_Lug.toString().trim().equals("CA")){ lcTemp += "CASA ";    }
                                     if ( GtTsd_Lug.toString().trim().equals("DE")){ lcTemp += "DEPTO ";   }
                                     if ( GtTsd_Lug.toString().trim().equals("OF")){ lcTemp += "OFICINA "; }
                                     if ( GtTsd_Lug.toString().trim().equals("LO")){ lcTemp += "LOCAL ";   }
                                     if ( GtTsd_Lug.toString().trim().equals("PA")){ lcTemp += "PARCELA "; }
                                     lcTemp += GtTsd_Lnm.toString().trim()+ " " ;
                                     if ( GtTsd_Sec.toString().trim().equals("PO")){ lcTemp += "POBLACION "; }
                                     if ( GtTsd_Sec.toString().trim().equals("CO")){ lcTemp += "CONDOMINIO ";}
                                     if ( GtTsd_Sec.toString().trim().equals("ED")){ lcTemp += "EDIFICIO ";  }
                                     if ( GtTsd_Sec.toString().trim().equals("VI")){ lcTemp += "VILLA ";     }
                                     if ( GtTsd_Sec.toString().trim().equals("BL")){ lcTemp += "BLOCK ";     }
                                     if ( GtTsd_Sec.toString().trim().equals("TO")){ lcTemp += "TORRE ";     }
                                     if ( GtTsd_Sec.toString().trim().equals("FU")){ lcTemp += "FUNDO ";     }
                                     if ( GtTsd_Sec.toString().trim().equals("CA")){ lcTemp += "CAMPO ";     }
                                     lcTemp += GtTsd_Snb.toString().trim() + " " ;
                                     lcTemp += GtTsd_Ncm.toString().trim() + "/" + GtTsd_Npv.toString().trim();
                                     return lcTemp;
                                   }
    public String pgetVuf()        {return f.Formato(GtTsd_Vuf.toString(),"BB.BB9",2,2,',');}
    public String pgetVtc()        {return f.Formato(GtTsd_Vtc.toString(),"BB.BB9",2,2,',');}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_Img_GtTsd LSet_A_ImgGtTsd(String pcStr)
  {
    Buf_Img_GtTsd l_Img = new Buf_Img_GtTsd();
    Vector vDatos = LSet_A_vImgGtTsd(pcStr);
    l_Img = (Buf_Img_GtTsd)vDatos.elementAt(0);
    return l_Img;
  }
  //---------------------------------------------------------------------------------------
  public static Vector LSet_A_vImgGtTsd(String pcStr)
  {
    int p=0;
    Vector vec= new Vector();
    Buf_Img_GtTsd t = new Buf_Img_GtTsd();
    t.GtTsd_Nc1.replace(0,t.GtTsd_Nc1.capacity(), pcStr.substring( p , p + t.GtTsd_Nc1.capacity() ) );p = p + t.GtTsd_Nc1.capacity();
    t.GtTsd_T11.replace(0,t.GtTsd_T11.capacity(), pcStr.substring( p , p + t.GtTsd_T11.capacity() ) );p = p + t.GtTsd_T11.capacity();
    t.GtTsd_Cll.replace(0,t.GtTsd_Cll.capacity(), pcStr.substring( p , p + t.GtTsd_Cll.capacity() ) );p = p + t.GtTsd_Cll.capacity();
    t.GtTsd_Cnm.replace(0,t.GtTsd_Cnm.capacity(), pcStr.substring( p , p + t.GtTsd_Cnm.capacity() ) );p = p + t.GtTsd_Cnm.capacity();
    t.GtTsd_Lug.replace(0,t.GtTsd_Lug.capacity(), pcStr.substring( p , p + t.GtTsd_Lug.capacity() ) );p = p + t.GtTsd_Lug.capacity();
    t.GtTsd_Lnm.replace(0,t.GtTsd_Lnm.capacity(), pcStr.substring( p , p + t.GtTsd_Lnm.capacity() ) );p = p + t.GtTsd_Lnm.capacity();
    t.GtTsd_Sec.replace(0,t.GtTsd_Sec.capacity(), pcStr.substring( p , p + t.GtTsd_Sec.capacity() ) );p = p + t.GtTsd_Sec.capacity();
    t.GtTsd_Snb.replace(0,t.GtTsd_Snb.capacity(), pcStr.substring( p , p + t.GtTsd_Snb.capacity() ) );p = p + t.GtTsd_Snb.capacity();
    t.GtTsd_Cmn.replace(0,t.GtTsd_Cmn.capacity(), pcStr.substring( p , p + t.GtTsd_Cmn.capacity() ) );p = p + t.GtTsd_Cmn.capacity();
    t.GtTsd_Ncm.replace(0,t.GtTsd_Ncm.capacity(), pcStr.substring( p , p + t.GtTsd_Ncm.capacity() ) );p = p + t.GtTsd_Ncm.capacity();
    t.GtTsd_Npv.replace(0,t.GtTsd_Npv.capacity(), pcStr.substring( p , p + t.GtTsd_Npv.capacity() ) );p = p + t.GtTsd_Npv.capacity();
    t.GtTsd_Opt.replace(0,t.GtTsd_Opt.capacity(), pcStr.substring( p , p + t.GtTsd_Opt.capacity() ) );p = p + t.GtTsd_Opt.capacity();
    t.GtTsd_Tsd.replace(0,t.GtTsd_Tsd.capacity(), pcStr.substring( p , p + t.GtTsd_Tsd.capacity() ) );p = p + t.GtTsd_Tsd.capacity();
    t.GtTsd_Nts.replace(0,t.GtTsd_Nts.capacity(), pcStr.substring( p , p + t.GtTsd_Nts.capacity() ) );p = p + t.GtTsd_Nts.capacity();
    t.GtTsd_Bof.replace(0,t.GtTsd_Bof.capacity(), pcStr.substring( p , p + t.GtTsd_Bof.capacity() ) );p = p + t.GtTsd_Bof.capacity();
    t.GtTsd_Cct.replace(0,t.GtTsd_Cct.capacity(), pcStr.substring( p , p + t.GtTsd_Cct.capacity() ) );p = p + t.GtTsd_Cct.capacity();
    t.GtTsd_Rts.replace(0,t.GtTsd_Rts.capacity(), pcStr.substring( p , p + t.GtTsd_Rts.capacity() ) );p = p + t.GtTsd_Rts.capacity();
    t.GtTsd_Fts.replace(0,t.GtTsd_Fts.capacity(), pcStr.substring( p , p + t.GtTsd_Fts.capacity() ) );p = p + t.GtTsd_Fts.capacity();
    t.GtTsd_Vuf.replace(0,t.GtTsd_Vuf.capacity(), pcStr.substring( p , p + t.GtTsd_Vuf.capacity() ) );p = p + t.GtTsd_Vuf.capacity();
    t.GtTsd_Vtc.replace(0,t.GtTsd_Vtc.capacity(), pcStr.substring( p , p + t.GtTsd_Vtc.capacity() ) );p = p + t.GtTsd_Vtc.capacity();
    t.GtTsd_Ftv.replace(0,t.GtTsd_Ftv.capacity(), pcStr.substring( p , p + t.GtTsd_Ftv.capacity() ) );p = p + t.GtTsd_Ftv.capacity();
    t.GtTsd_Vts.replace(0,t.GtTsd_Vts.capacity(), pcStr.substring( p , p + t.GtTsd_Vts.capacity() ) );p = p + t.GtTsd_Vts.capacity();
    t.GtTsd_Ibf.replace(0,t.GtTsd_Ibf.capacity(), pcStr.substring( p , p + t.GtTsd_Ibf.capacity() ) );p = p + t.GtTsd_Ibf.capacity();
    t.GtTsd_Nbf.replace(0,t.GtTsd_Nbf.capacity(), pcStr.substring( p , p + t.GtTsd_Nbf.capacity() ) );p = p + t.GtTsd_Nbf.capacity();
    t.GtTsd_Pcb.replace(0,t.GtTsd_Pcb.capacity(), pcStr.substring( p , p + t.GtTsd_Pcb.capacity() ) );p = p + t.GtTsd_Pcb.capacity();
    t.GtTsd_Hon.replace(0,t.GtTsd_Hon.capacity(), pcStr.substring( p , p + t.GtTsd_Hon.capacity() ) );p = p + t.GtTsd_Hon.capacity();
    t.GtTsd_Vst.replace(0,t.GtTsd_Vst.capacity(), pcStr.substring( p , p + t.GtTsd_Vst.capacity() ) );p = p + t.GtTsd_Vst.capacity();
    t.GtTsd_Obt.replace(0,t.GtTsd_Obt.capacity(), pcStr.substring( p , p + t.GtTsd_Obt.capacity() ) );p = p + t.GtTsd_Obt.capacity();
    t.GtTsd_Ept.replace(0,t.GtTsd_Ept.capacity(), pcStr.substring( p , p + t.GtTsd_Ept.capacity() ) );p = p + t.GtTsd_Ept.capacity();
    t.GtTsd_T21.replace(0,t.GtTsd_T21.capacity(), pcStr.substring( p , p + t.GtTsd_T21.capacity() ) );p = p + t.GtTsd_T21.capacity();
    t.GtTsd_T31.replace(0,t.GtTsd_T31.capacity(), pcStr.substring( p , p + t.GtTsd_T31.capacity() ) );p = p + t.GtTsd_T31.capacity();
    t.GtTsd_Nc2.replace(0,t.GtTsd_Nc2.capacity(), pcStr.substring( p , p + t.GtTsd_Nc2.capacity() ) );p = p + t.GtTsd_Nc2.capacity();
    t.GtTsd_T12.replace(0,t.GtTsd_T12.capacity(), pcStr.substring( p , p + t.GtTsd_T12.capacity() ) );p = p + t.GtTsd_T12.capacity();
    t.GtTsd_T22.replace(0,t.GtTsd_T22.capacity(), pcStr.substring( p , p + t.GtTsd_T22.capacity() ) );p = p + t.GtTsd_T22.capacity();
    t.GtTsd_T32.replace(0,t.GtTsd_T32.capacity(), pcStr.substring( p , p + t.GtTsd_T32.capacity() ) );p = p + t.GtTsd_T32.capacity();
    t.GtTsd_Nc3.replace(0,t.GtTsd_Nc3.capacity(), pcStr.substring( p , p + t.GtTsd_Nc3.capacity() ) );p = p + t.GtTsd_Nc3.capacity();
    t.GtTsd_T13.replace(0,t.GtTsd_T13.capacity(), pcStr.substring( p , p + t.GtTsd_T13.capacity() ) );p = p + t.GtTsd_T13.capacity();
    t.GtTsd_T23.replace(0,t.GtTsd_T23.capacity(), pcStr.substring( p , p + t.GtTsd_T23.capacity() ) );p = p + t.GtTsd_T23.capacity();
    t.GtTsd_T33.replace(0,t.GtTsd_T33.capacity(), pcStr.substring( p , p + t.GtTsd_T33.capacity() ) );p = p + t.GtTsd_T33.capacity();
    t.GtTsd_Dot.replace(0,t.GtTsd_Dot.capacity(), pcStr.substring( p , p + t.GtTsd_Dot.capacity() ) );p = p + t.GtTsd_Dot.capacity();
    t.GtTsd_Dob.replace(0,t.GtTsd_Dob.capacity(), pcStr.substring( p , p + t.GtTsd_Dob.capacity() ) );p = p + t.GtTsd_Dob.capacity();
    t.GtTsd_Fll.replace(0,t.GtTsd_Fll.capacity(), pcStr.substring(p) );
    vec.add ( t );
    return vec;
  }
  //---------------------------------------------------------------------------------------
  public static String LSet_De_ImgGtTsd(Buf_Img_GtTsd p_Img)
  {
    String pcStr = "";
    pcStr = pcStr + p_Img.getNc1();
    pcStr = pcStr + p_Img.getT11();
    pcStr = pcStr + p_Img.getCll();
    pcStr = pcStr + p_Img.getCnm();
    pcStr = pcStr + p_Img.getLug();
    pcStr = pcStr + p_Img.getLnm();
    pcStr = pcStr + p_Img.getSec();
    pcStr = pcStr + p_Img.getSnb();
    pcStr = pcStr + p_Img.getCmn();
    pcStr = pcStr + p_Img.getNcm();
    pcStr = pcStr + p_Img.getNpv();
    pcStr = pcStr + p_Img.getOpt();
    pcStr = pcStr + p_Img.getTsd();
    pcStr = pcStr + p_Img.getNts();
    pcStr = pcStr + p_Img.getBof();
    pcStr = pcStr + p_Img.getCct();
    pcStr = pcStr + p_Img.getRts();
    pcStr = pcStr + p_Img.getFts();
    pcStr = pcStr + p_Img.getVuf();
    pcStr = pcStr + p_Img.getVtc();
    pcStr = pcStr + p_Img.getFtv();
    pcStr = pcStr + p_Img.getVts();
    pcStr = pcStr + p_Img.getIbf();
    pcStr = pcStr + p_Img.getNbf();
    pcStr = pcStr + p_Img.getPcb();
    pcStr = pcStr + p_Img.getHon();
    pcStr = pcStr + p_Img.getVst();
    pcStr = pcStr + p_Img.getObt();
    pcStr = pcStr + p_Img.getEpt();
    pcStr = pcStr + p_Img.getT21();
    pcStr = pcStr + p_Img.getT31();
    pcStr = pcStr + p_Img.getNc2();
    pcStr = pcStr + p_Img.getT12();
    pcStr = pcStr + p_Img.getT22();
    pcStr = pcStr + p_Img.getT32();
    pcStr = pcStr + p_Img.getNc3();
    pcStr = pcStr + p_Img.getT13();
    pcStr = pcStr + p_Img.getT23();
    pcStr = pcStr + p_Img.getT33();
    pcStr = pcStr + p_Img.getDot();
    pcStr = pcStr + p_Img.getDob();
    pcStr = pcStr + p_Img.getFll();
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
  public static class Buf_Img_GtSte
  {
    public StringBuffer GtSte_Seq = new StringBuffer(3  );  //9(03)        3 Secuencia
    public StringBuffer GtSte_Itb = new StringBuffer(2  );  //X(02)        5 Tipo Bien (HP/PD)
    public StringBuffer GtSte_Itg = new StringBuffer(5  );  //X(05)       10 Tipo Gtia CodGen
    public StringBuffer GtSte_Dpc = new StringBuffer(10 );  //X(10)       20 Producto Contable
    public StringBuffer GtSte_Dsb = new StringBuffer(30 );  //X(30)       50 Descripcion Bien
    public StringBuffer GtSte_Sqd = new StringBuffer(3  );  //9(03)       53 Secuencia
    public StringBuffer GtSte_Frv = new StringBuffer(8  );  //9(08)       61 FRevalorizacion
    public StringBuffer GtSte_Vur = new StringBuffer(9  );  //9(07)V9(2)  70 VConv <Trj> a FRV
    public StringBuffer GtSte_Udd = new StringBuffer(9  );  //9(07)V9(2)  79 Unidades (Superficie)
    public StringBuffer GtSte_Umd = new StringBuffer(2  );  //X(02)       81 UMedida (M2/HA)
    public StringBuffer GtSte_Umt = new StringBuffer(1  );  //X(01)       82 UMonetaria (P/U/D)
    public StringBuffer GtSte_Pum = new StringBuffer(15 );  //9(11)V9(4)  97 PUnitario UMedida
    public StringBuffer GtSte_Vtu = new StringBuffer(15 );  //9(11)V9(4) 112 VTasacion UMedida
    public StringBuffer GtSte_Vum = new StringBuffer(9  );  //9(07)V9(2) 121 VConversion UMonetaria
    public StringBuffer GtSte_Vtt = new StringBuffer(15 );  //9(11)V9(4) 136 Valor Tasacion <Trj>
    public StringBuffer GtSte_Vts = new StringBuffer(15 );  //9(13)V9(2) 151 Valor Tasacion <Mnd>
    public StringBuffer GtSte_Tuf = new StringBuffer(15 );  //9(11)V9(4) 166 Tasacion UF
    public StringBuffer GtSte_Ttc = new StringBuffer(15 );  //9(13)V9(2) 181 Tasacion TC
    public StringBuffer GtSte_Tps = new StringBuffer(15 );  //9(13)V9(2) 196 Tasacion $
    public StringBuffer GtSte_Pct = new StringBuffer(5  );  //9(03)V9(2) 201 %Castigo SBIF
    public StringBuffer GtSte_Vcn = new StringBuffer(15 );  //9(13)V9(2) 216 VContable Moneda
    public StringBuffer GtSte_Vlq = new StringBuffer(15 );  //9(13)V9(2) 231 VLiquidacion Moneda
    public StringBuffer GtSte_Vtb = new StringBuffer(15 );  //9(13)V9(2) 246 VRemate Moneda
    public StringBuffer GtSte_Vsg = new StringBuffer(15 );  //9(11)V9(4) 261 VSeguro <Trj>
    public StringBuffer GtSte_Fvt = new StringBuffer(8  );  //9(08)      269 FVcto Tasacion
    public StringBuffer GtSte_Fll = new StringBuffer(557);  //X(557)     826 No Usado
    public StringBuffer GtSte_Iro = new StringBuffer(1  );  //X(01)      827 Idr ReadOnly

    public String getSeq()         {return GtSte_Seq.toString();}
    public String getItb()         {return GtSte_Itb.toString();}
    public String getItg()         {return GtSte_Itg.toString();}
    public String getDpc()         {return GtSte_Dpc.toString();}
    public String getDsb()         {return GtSte_Dsb.toString();}
    public String getSqd()         {return GtSte_Sqd.toString();}
    public String getFrv()         {return GtSte_Frv.toString();}
    public String getVur()         {return GtSte_Vur.toString();}
    public String getUdd()         {return GtSte_Udd.toString();}
    public String getUmd()         {return GtSte_Umd.toString();}
    public String getUmt()         {return GtSte_Umt.toString();}
    public String getPum()         {return GtSte_Pum.toString();}
    public String getVtu()         {return GtSte_Vtu.toString();}
    public String getVum()         {return GtSte_Vum.toString();}
    public String getVtt()         {return GtSte_Vtt.toString();}
    public String getVts()         {return GtSte_Vts.toString();}
    public String getTuf()         {return GtSte_Tuf.toString();}
    public String getTtc()         {return GtSte_Ttc.toString();}
    public String getTps()         {return GtSte_Tps.toString();}
    public String getPct()         {return GtSte_Pct.toString();}
    public String getVcn()         {return GtSte_Vcn.toString();}
    public String getVlq()         {return GtSte_Vlq.toString();}
    public String getVtb()         {return GtSte_Vtb.toString();}
    public String getVsg()         {return GtSte_Vsg.toString();}
    public String getFvt()         {return GtSte_Fvt.toString();}
    public String getFll()         {return GtSte_Fll.toString();}
    public String getIro()         {return GtSte_Iro.toString();}

    public Formateo f = new Formateo();
    public String fgetUdd()        {return f.Formato(GtSte_Udd.toString(),"99.999.999.999",2,2,',');}
    public String fgetUmd()        {return GtSte_Umd.toString();}
    public String fgetUmt()
                                   {
                                     String lcUmt="";
                                     if (getUmt().equals("P"))  { lcUmt = "$"; }
                                     if (getUmt().equals("D"))  { lcUmt = "US$"; }
                                     if (getUmt().equals("U"))  { lcUmt = "UF"; }
                                     return lcUmt;
                                   }
    public String fgetPum()
                                   {
                                     int liDcm=0;
                                     if (getUmt().equals("P"))  { liDcm = 0; }
                                     if (getUmt().equals("D"))  { liDcm = 2; }
                                     if (getUmt().equals("U"))  { liDcm = 4; }
                                     return f.Formato(GtSte_Pum.toString(),"99.999.999.999",4,liDcm,',');
                                   }
    public String fgetVtu()
                                   {
                                     int liDcm=0;
                                     if (getUmt().equals("P"))  { liDcm = 0; }
                                     if (getUmt().equals("D"))  { liDcm = 2; }
                                     if (getUmt().equals("U"))  { liDcm = 4; }
                                     return f.Formato(GtSte_Vtu.toString(),"99.999.999.999",4,liDcm,',');
                                   }
    public String fgetVtt()        {return f.Formato(GtSte_Vtt.toString(),"99.999.999.999",4,4,',');}
    public String fgetVts()        {return f.Formato(GtSte_Vts.toString(),"99.999.999.999",2,0,',');}
    public String fgetTuf()        {return f.Formato(GtSte_Tuf.toString(),"99.999.999.999",4,4,',');}
    public String fgetTtc()        {return f.Formato(GtSte_Ttc.toString(),"99.999.999.999",2,2,',');}
    public String fgetTps()        {return f.Formato(GtSte_Tps.toString(),"99.999.999.999",2,0,',');}
    public String fgetPct()        {return f.Formato(GtSte_Pct.toString(),"999",2,2,',');}
    public String fgetVcn()        {return f.Formato(GtSte_Vcn.toString(),"99.999.999.999",2,0,',');}
    public String fgetVlq()        {return f.Formato(GtSte_Vlq.toString(),"99.999.999.999",2,0,',');}
    public String fgetVtb()        {return f.Formato(GtSte_Vtb.toString(),"99.999.999.999",2,0,',');}
    public String fgetVsg()        {return f.Formato(GtSte_Vsg.toString(),"99.999.999.999",4,4,',');}

    public String pgetUdd()        {return f.Formato(GtSte_Udd.toString(),"BB.BB9",2,2,',');}
    public String pgetPum()        {return f.Formato(GtSte_Pum.toString(),"B.BBB.BBB.BB9",4,2,',');}
    public String pgetVtu()        {return f.Formato(GtSte_Vtu.toString(),"B.BBB.BBB.BB9",4,2,',');}
    public String pgetVts()        {return f.Formato(GtSte_Vts.toString(),"BB.BBB.BBB.BB9",2,0,',');}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_Img_GtSte LSet_A_ImgGtSte(String pcStr)
  {
    Buf_Img_GtSte l_Img = new Buf_Img_GtSte();
    Vector vDatos = LSet_A_vImgGtSte(pcStr);
    l_Img = (Buf_Img_GtSte)vDatos.elementAt(0);
    return l_Img;
  }
  //---------------------------------------------------------------------------------------
  public static Vector LSet_A_vImgGtSte(String pcStr)
  {
    int p=0;
    Vector vec= new Vector();
    Buf_Img_GtSte t = new Buf_Img_GtSte();
    t.GtSte_Seq.replace(0,t.GtSte_Seq.capacity(), pcStr.substring( p , p + t.GtSte_Seq.capacity() ) );p = p + t.GtSte_Seq.capacity();
    t.GtSte_Itb.replace(0,t.GtSte_Itb.capacity(), pcStr.substring( p , p + t.GtSte_Itb.capacity() ) );p = p + t.GtSte_Itb.capacity();
    t.GtSte_Itg.replace(0,t.GtSte_Itg.capacity(), pcStr.substring( p , p + t.GtSte_Itg.capacity() ) );p = p + t.GtSte_Itg.capacity();
    t.GtSte_Dpc.replace(0,t.GtSte_Dpc.capacity(), pcStr.substring( p , p + t.GtSte_Dpc.capacity() ) );p = p + t.GtSte_Dpc.capacity();
    t.GtSte_Dsb.replace(0,t.GtSte_Dsb.capacity(), pcStr.substring( p , p + t.GtSte_Dsb.capacity() ) );p = p + t.GtSte_Dsb.capacity();
    t.GtSte_Sqd.replace(0,t.GtSte_Sqd.capacity(), pcStr.substring( p , p + t.GtSte_Sqd.capacity() ) );p = p + t.GtSte_Sqd.capacity();
    t.GtSte_Frv.replace(0,t.GtSte_Frv.capacity(), pcStr.substring( p , p + t.GtSte_Frv.capacity() ) );p = p + t.GtSte_Frv.capacity();
    t.GtSte_Vur.replace(0,t.GtSte_Vur.capacity(), pcStr.substring( p , p + t.GtSte_Vur.capacity() ) );p = p + t.GtSte_Vur.capacity();
    t.GtSte_Udd.replace(0,t.GtSte_Udd.capacity(), pcStr.substring( p , p + t.GtSte_Udd.capacity() ) );p = p + t.GtSte_Udd.capacity();
    t.GtSte_Umd.replace(0,t.GtSte_Umd.capacity(), pcStr.substring( p , p + t.GtSte_Umd.capacity() ) );p = p + t.GtSte_Umd.capacity();
    t.GtSte_Umt.replace(0,t.GtSte_Umt.capacity(), pcStr.substring( p , p + t.GtSte_Umt.capacity() ) );p = p + t.GtSte_Umt.capacity();
    t.GtSte_Pum.replace(0,t.GtSte_Pum.capacity(), pcStr.substring( p , p + t.GtSte_Pum.capacity() ) );p = p + t.GtSte_Pum.capacity();
    t.GtSte_Vtu.replace(0,t.GtSte_Vtu.capacity(), pcStr.substring( p , p + t.GtSte_Vtu.capacity() ) );p = p + t.GtSte_Vtu.capacity();
    t.GtSte_Vum.replace(0,t.GtSte_Vum.capacity(), pcStr.substring( p , p + t.GtSte_Vum.capacity() ) );p = p + t.GtSte_Vum.capacity();
    t.GtSte_Vtt.replace(0,t.GtSte_Vtt.capacity(), pcStr.substring( p , p + t.GtSte_Vtt.capacity() ) );p = p + t.GtSte_Vtt.capacity();
    t.GtSte_Vts.replace(0,t.GtSte_Vts.capacity(), pcStr.substring( p , p + t.GtSte_Vts.capacity() ) );p = p + t.GtSte_Vts.capacity();
    t.GtSte_Tuf.replace(0,t.GtSte_Tuf.capacity(), pcStr.substring( p , p + t.GtSte_Tuf.capacity() ) );p = p + t.GtSte_Tuf.capacity();
    t.GtSte_Ttc.replace(0,t.GtSte_Ttc.capacity(), pcStr.substring( p , p + t.GtSte_Ttc.capacity() ) );p = p + t.GtSte_Ttc.capacity();
    t.GtSte_Tps.replace(0,t.GtSte_Tps.capacity(), pcStr.substring( p , p + t.GtSte_Tps.capacity() ) );p = p + t.GtSte_Tps.capacity();
    t.GtSte_Pct.replace(0,t.GtSte_Pct.capacity(), pcStr.substring( p , p + t.GtSte_Pct.capacity() ) );p = p + t.GtSte_Pct.capacity();
    t.GtSte_Vcn.replace(0,t.GtSte_Vcn.capacity(), pcStr.substring( p , p + t.GtSte_Vcn.capacity() ) );p = p + t.GtSte_Vcn.capacity();
    t.GtSte_Vlq.replace(0,t.GtSte_Vlq.capacity(), pcStr.substring( p , p + t.GtSte_Vlq.capacity() ) );p = p + t.GtSte_Vlq.capacity();
    t.GtSte_Vtb.replace(0,t.GtSte_Vtb.capacity(), pcStr.substring( p , p + t.GtSte_Vtb.capacity() ) );p = p + t.GtSte_Vtb.capacity();
    t.GtSte_Vsg.replace(0,t.GtSte_Vsg.capacity(), pcStr.substring( p , p + t.GtSte_Vsg.capacity() ) );p = p + t.GtSte_Vsg.capacity();
    t.GtSte_Fvt.replace(0,t.GtSte_Fvt.capacity(), pcStr.substring( p , p + t.GtSte_Fvt.capacity() ) );p = p + t.GtSte_Fvt.capacity();
    t.GtSte_Fll.replace(0,t.GtSte_Fll.capacity(), pcStr.substring( p , p + t.GtSte_Fll.capacity() ) );p = p + t.GtSte_Fll.capacity();
    t.GtSte_Iro.replace(0,t.GtSte_Iro.capacity(), pcStr.substring( p , p + t.GtSte_Iro.capacity() ) );p = p + t.GtSte_Iro.capacity();
    vec.add ( t );
    return vec;
  }
  //---------------------------------------------------------------------------------------
  public String LSet_De_ImgGtSte(Buf_Img_GtSte p_Img)
  {
    String pcStr = "";
    pcStr = pcStr + p_Img.getSeq();
    pcStr = pcStr + p_Img.getItb();
    pcStr = pcStr + p_Img.getItg();
    pcStr = pcStr + p_Img.getDpc();
    pcStr = pcStr + p_Img.getDsb();
    pcStr = pcStr + p_Img.getSqd();
    pcStr = pcStr + p_Img.getFrv();
    pcStr = pcStr + p_Img.getVur();
    pcStr = pcStr + p_Img.getUdd();
    pcStr = pcStr + p_Img.getUmd();
    pcStr = pcStr + p_Img.getUmt();
    pcStr = pcStr + p_Img.getPum();
    pcStr = pcStr + p_Img.getVtu();
    pcStr = pcStr + p_Img.getVum();
    pcStr = pcStr + p_Img.getVtt();
    pcStr = pcStr + p_Img.getVts();
    pcStr = pcStr + p_Img.getTuf();
    pcStr = pcStr + p_Img.getTtc();
    pcStr = pcStr + p_Img.getTps();
    pcStr = pcStr + p_Img.getPct();
    pcStr = pcStr + p_Img.getVcn();
    pcStr = pcStr + p_Img.getVlq();
    pcStr = pcStr + p_Img.getVtb();
    pcStr = pcStr + p_Img.getVsg();
    pcStr = pcStr + p_Img.getFvt();
    pcStr = pcStr + p_Img.getFll();
    pcStr = pcStr + p_Img.getIro();
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
  public static class Buf_Img_GtHyp
  {
    public StringBuffer GtHyp_Seq = new StringBuffer(3  );  //9(03)        3 Secuencia
    public StringBuffer GtHyp_Itb = new StringBuffer(2  );  //X(02)        5 Tipo Bien (HP/PD)
    public StringBuffer GtHyp_Itg = new StringBuffer(5  );  //X(05)       10 Tipo Gtia CodGen (Especialidad Tasador??)
    public StringBuffer GtHyp_Dpc = new StringBuffer(10 );  //X(10)       20 Producto Contable
    public StringBuffer GtHyp_Pct = new StringBuffer(5  );  //9(03)V9(2)  25 %Castigo SBIF
    public StringBuffer GtHyp_Dsb = new StringBuffer(100);  //X(100)     125 Descripcion Bien
    public StringBuffer GtHyp_Csv = new StringBuffer(3  );  //X(03)      128 CSV Tipo
    public StringBuffer GtHyp_Lcl = new StringBuffer(15 );  //X(15)      143 CSV Localidad
    public StringBuffer GtHyp_Rpo = new StringBuffer(4  );  //9(04)      147 CSV A�o
    public StringBuffer GtHyp_Foj = new StringBuffer(7  );  //9(07)      154 CSV Fojas
    public StringBuffer GtHyp_Nin = new StringBuffer(7  );  //9(07)      161 CSV Numero
    public StringBuffer GtHyp_Cll = new StringBuffer(34 );  //X(34)      195 Dmc.Calle
    public StringBuffer GtHyp_Cnm = new StringBuffer(6  );  //X(06)      201 Dmc.CNumero
    public StringBuffer GtHyp_Lug = new StringBuffer(2  );  //X(02)      203 Dmc.Lugar
    public StringBuffer GtHyp_Lnm = new StringBuffer(6  );  //X(06)      209 Dmc.LNumero
    public StringBuffer GtHyp_Sec = new StringBuffer(2  );  //X(02)      211 Dmc.Sector
    public StringBuffer GtHyp_Snb = new StringBuffer(20 );  //X(20)      231 Dmc.SNombre
    public StringBuffer GtHyp_Cmn = new StringBuffer(11 );  //9(11)      242 Dmc.CodComuna
    public StringBuffer GtHyp_Coo = new StringBuffer(10 );  //X(10)      252 Dmc.Coordenadas
    public StringBuffer GtHyp_Ncm = new StringBuffer(20 );  //X(20)      272 Dmc.NomComuna
    public StringBuffer GtHyp_Npv = new StringBuffer(17 );  //X(17)      289 Dmc.NomProvincia
    public StringBuffer GtHyp_Vtf = new StringBuffer(15 );  //9(11)V9(4) 304 Valor Tasacion UF
    public StringBuffer GtHyp_Vtd = new StringBuffer(15 );  //9(11)V9(4) 319 Valor Tasacion US$
    public StringBuffer GtHyp_Vts = new StringBuffer(15 );  //9(13)V9(2) 334 Valor Tasacion <Mnd>
    public StringBuffer GtHyp_Vtb = new StringBuffer(15 );  //9(13)V9(2) 349 Valor Banco <Mnd>
    public StringBuffer GtHyp_Vlq = new StringBuffer(15 );  //9(13)V9(2) 364 VLiquidacion <Mnd>
    public StringBuffer GtHyp_Vcn = new StringBuffer(15 );  //9(13)V9(2) 379 Valor Contable <Mnd>
    public StringBuffer GtHyp_Vsg = new StringBuffer(15 );  //9(13)V9(2) 394 Valor Asegurable <Mnd>
    public StringBuffer GtHyp_Csg = new StringBuffer(3  );  //9(03)      397 Idr Aseguradora
    public StringBuffer GtHyp_Spz = new StringBuffer(10 );  //X(10)      407 Idr Poliza
    public StringBuffer GtHyp_Sfv = new StringBuffer(8  );  //9(08)      415 Idr FVencto Seguro
    public StringBuffer GtHyp_Sum = new StringBuffer(1  );  //X(01)      416 Unidad Monetaria
    public StringBuffer GtHyp_Svl = new StringBuffer(15 );  //9(11)V9(4) 431 Valor Seguro
    public StringBuffer GtHyp_Isg = new StringBuffer(1  );  //X(01)      432 Idr Seguro
    public StringBuffer GtHyp_Nsg = new StringBuffer(15 );  //X(15)      447 Nom.Cia Seguros
    public StringBuffer GtHyp_Cps = new StringBuffer(1  );  //X(01)      448 Idr CalPlusvalia
    public StringBuffer GtHyp_Ceb = new StringBuffer(1  );  //X(01)      449 Idr CalEstado
    public StringBuffer GtHyp_Cfc = new StringBuffer(1  );  //X(01)      450 Idr FactComercial
    public StringBuffer GtHyp_Cee = new StringBuffer(1  );  //X(01)      451 Idr ExpecEconomica
    public StringBuffer GtHyp_Obs = new StringBuffer(9  );  //9(09)      460 @CLB Obs Bien
    public StringBuffer GtHyp_Psw = new StringBuffer(1  );  //X(01)      461 Idr Browser
    public StringBuffer GtHyp_Idx = new StringBuffer(3  );  //9(03)      464 Nro Activo
    public StringBuffer GtHyp_Ibs = new StringBuffer(3  );  //9(03)      467 Ultimo Nro Ocupado
    public StringBuffer GtHyp_Fll = new StringBuffer(360);  //X(360)     827 Disponible

    public String getSeq()         {return GtHyp_Seq.toString();}
    public String getItb()         {return GtHyp_Itb.toString();}
    public String getItg()         {return GtHyp_Itg.toString();}
    public String getDpc()         {return GtHyp_Dpc.toString();}
    public String getPct()         {return GtHyp_Pct.toString();}
    public String getDsb()         {return GtHyp_Dsb.toString();}
    public String getCsv()         {return GtHyp_Csv.toString();}
    public String getLcl()         {return GtHyp_Lcl.toString();}
    public String getRpo()         {return GtHyp_Rpo.toString();}
    public String getFoj()         {return GtHyp_Foj.toString();}
    public String getNin()         {return GtHyp_Nin.toString();}
    public String getCll()         {return GtHyp_Cll.toString();}
    public String getCnm()         {return GtHyp_Cnm.toString();}
    public String getLug()         {return GtHyp_Lug.toString();}
    public String getLnm()         {return GtHyp_Lnm.toString();}
    public String getSec()         {return GtHyp_Sec.toString();}
    public String getSnb()         {return GtHyp_Snb.toString();}
    public String getCmn()         {return GtHyp_Cmn.toString();}
    public String getCoo()         {return GtHyp_Coo.toString();}
    public String getNcm()         {return GtHyp_Ncm.toString();}
    public String getNpv()         {return GtHyp_Npv.toString();}
    public String getVtf()         {return GtHyp_Vtf.toString();}
    public String getVtd()         {return GtHyp_Vtd.toString();}
    public String getVts()         {return GtHyp_Vts.toString();}
    public String getVtb()         {return GtHyp_Vtb.toString();}
    public String getVlq()         {return GtHyp_Vlq.toString();}
    public String getVcn()         {return GtHyp_Vcn.toString();}
    public String getVsg()         {return GtHyp_Vsg.toString();}
    public String getCsg()         {return GtHyp_Csg.toString();}
    public String getSpz()         {return GtHyp_Spz.toString();}
    public String getSfv()         {return GtHyp_Sfv.toString();}
    public String getSum()         {return GtHyp_Sum.toString();}
    public String getSvl()         {return GtHyp_Svl.toString();}
    public String getIsg()         {return GtHyp_Isg.toString();}
    public String getNsg()         {return GtHyp_Nsg.toString();}
    public String getCps()         {return GtHyp_Cps.toString();}
    public String getCeb()         {return GtHyp_Ceb.toString();}
    public String getCfc()         {return GtHyp_Cfc.toString();}
    public String getCee()         {return GtHyp_Cee.toString();}
    public String getObs()         {return GtHyp_Obs.toString();}
    public String getPsw()         {return GtHyp_Psw.toString();}
    public String getIdx()         {return GtHyp_Idx.toString();}
    public String getIbs()         {return GtHyp_Ibs.toString();}
    public String getFll()         {return GtHyp_Fll.toString();}

    public Formateo f = new Formateo();
    public String fgetPct()        {return f.Formato(GtHyp_Pct.toString(),"999",2,2,',');}
    public String fgetDsb()        {return GtHyp_Dsb.toString().trim();}
    public String fgetVtf()        {return f.Formato(GtHyp_Vtf.toString(),"99.999.999.999",4,4,',');}
    public String fgetVtd()        {return f.Formato(GtHyp_Vtd.toString(),"99.999.999.999",4,4,',');}
    public String fgetVts()        {return f.Formato(GtHyp_Vts.toString(),"99.999.999.999",2,2,',');}
    public String fgetVtb()        {return f.Formato(GtHyp_Vtb.toString(),"99.999.999.999",2,2,',');}
    public String fgetVlq()        {return f.Formato(GtHyp_Vlq.toString(),"99.999.999.999",2,2,',');}
    public String fgetVcn()        {return f.Formato(GtHyp_Vcn.toString(),"99.999.999.999",2,2,',');}
    public String fgetVsg()        {return f.Formato(GtHyp_Vsg.toString(),"99.999.999.999",2,2,',');}
    public String fgetSfv()        {return f.FormatDate(GtHyp_Sfv.toString());}
    public String fgetSvl()        {return f.Formato(GtHyp_Svl.toString(),"99.999.999.999",4,4,',');}
    public String fgetIsg()
                                   {
                                     String lcIsg="Sin seguro";
                                     if (getIsg().equals("A"))  { lcIsg = "Automatico"; }
                                     if (getIsg().equals("M"))  { lcIsg = "Manual"; }
                                     return lcIsg;
                                   }
    public String egetVts()        {return f.Formato(GtHyp_Vts.toString(),"99.999.999.999",2,0,',');}
    public String egetVtb()        {return f.Formato(GtHyp_Vtb.toString(),"99.999.999.999",2,0,',');}
    public String egetVlq()        {return f.Formato(GtHyp_Vlq.toString(),"99.999.999.999",2,0,',');}
    public String egetVcn()        {return f.Formato(GtHyp_Vcn.toString(),"99.999.999.999",2,0,',');}
    public String ggetVsg()        {return f.Formato(GtHyp_Vsg.toString(),"99.999.999.999",2,0,',');}
    public String ggetSvl()        {return f.Formato(GtHyp_Svl.toString(),"99.999.999.999",4,0,',');}
    public String fgetCmn()        {
                                    return getCmn().substring(0,2)+"."+getCmn().substring(2,6)+"."+getCmn().substring(6);
                                   }
    public int    igetIdx()        {
                                    if (getIdx().trim().equals(""))
                                       { return Integer.parseInt("1"); }
                                    else
                                       { return Integer.parseInt(GtHyp_Idx.toString()); }
                                   }
  }
  //---------------------------------------------------------------------------------------
  public static Buf_Img_GtHyp LSet_A_ImgGtHyp(String pcStr)
  {
    Buf_Img_GtHyp l_Img = new Buf_Img_GtHyp();
    Vector vDatos = LSet_A_vImgGtHyp(pcStr);
    l_Img = (Buf_Img_GtHyp)vDatos.elementAt(0);
    return l_Img;
  }
  //---------------------------------------------------------------------------------------
  public static Vector LSet_A_vImgGtHyp(String pcStr)
  {
    int p=0;
    Vector vec= new Vector();
    Buf_Img_GtHyp t = new Buf_Img_GtHyp();
    t.GtHyp_Seq.replace(0,t.GtHyp_Seq.capacity(), pcStr.substring( p , p + t.GtHyp_Seq.capacity() ) );p = p + t.GtHyp_Seq.capacity();
    t.GtHyp_Itb.replace(0,t.GtHyp_Itb.capacity(), pcStr.substring( p , p + t.GtHyp_Itb.capacity() ) );p = p + t.GtHyp_Itb.capacity();
    t.GtHyp_Itg.replace(0,t.GtHyp_Itg.capacity(), pcStr.substring( p , p + t.GtHyp_Itg.capacity() ) );p = p + t.GtHyp_Itg.capacity();
    t.GtHyp_Dpc.replace(0,t.GtHyp_Dpc.capacity(), pcStr.substring( p , p + t.GtHyp_Dpc.capacity() ) );p = p + t.GtHyp_Dpc.capacity();
    t.GtHyp_Pct.replace(0,t.GtHyp_Pct.capacity(), pcStr.substring( p , p + t.GtHyp_Pct.capacity() ) );p = p + t.GtHyp_Pct.capacity();
    t.GtHyp_Dsb.replace(0,t.GtHyp_Dsb.capacity(), pcStr.substring( p , p + t.GtHyp_Dsb.capacity() ) );p = p + t.GtHyp_Dsb.capacity();
    t.GtHyp_Csv.replace(0,t.GtHyp_Csv.capacity(), pcStr.substring( p , p + t.GtHyp_Csv.capacity() ) );p = p + t.GtHyp_Csv.capacity();
    t.GtHyp_Lcl.replace(0,t.GtHyp_Lcl.capacity(), pcStr.substring( p , p + t.GtHyp_Lcl.capacity() ) );p = p + t.GtHyp_Lcl.capacity();
    t.GtHyp_Rpo.replace(0,t.GtHyp_Rpo.capacity(), pcStr.substring( p , p + t.GtHyp_Rpo.capacity() ) );p = p + t.GtHyp_Rpo.capacity();
    t.GtHyp_Foj.replace(0,t.GtHyp_Foj.capacity(), pcStr.substring( p , p + t.GtHyp_Foj.capacity() ) );p = p + t.GtHyp_Foj.capacity();
    t.GtHyp_Nin.replace(0,t.GtHyp_Nin.capacity(), pcStr.substring( p , p + t.GtHyp_Nin.capacity() ) );p = p + t.GtHyp_Nin.capacity();
    t.GtHyp_Cll.replace(0,t.GtHyp_Cll.capacity(), pcStr.substring( p , p + t.GtHyp_Cll.capacity() ) );p = p + t.GtHyp_Cll.capacity();
    t.GtHyp_Cnm.replace(0,t.GtHyp_Cnm.capacity(), pcStr.substring( p , p + t.GtHyp_Cnm.capacity() ) );p = p + t.GtHyp_Cnm.capacity();
    t.GtHyp_Lug.replace(0,t.GtHyp_Lug.capacity(), pcStr.substring( p , p + t.GtHyp_Lug.capacity() ) );p = p + t.GtHyp_Lug.capacity();
    t.GtHyp_Lnm.replace(0,t.GtHyp_Lnm.capacity(), pcStr.substring( p , p + t.GtHyp_Lnm.capacity() ) );p = p + t.GtHyp_Lnm.capacity();
    t.GtHyp_Sec.replace(0,t.GtHyp_Sec.capacity(), pcStr.substring( p , p + t.GtHyp_Sec.capacity() ) );p = p + t.GtHyp_Sec.capacity();
    t.GtHyp_Snb.replace(0,t.GtHyp_Snb.capacity(), pcStr.substring( p , p + t.GtHyp_Snb.capacity() ) );p = p + t.GtHyp_Snb.capacity();
    t.GtHyp_Cmn.replace(0,t.GtHyp_Cmn.capacity(), pcStr.substring( p , p + t.GtHyp_Cmn.capacity() ) );p = p + t.GtHyp_Cmn.capacity();
    t.GtHyp_Coo.replace(0,t.GtHyp_Coo.capacity(), pcStr.substring( p , p + t.GtHyp_Coo.capacity() ) );p = p + t.GtHyp_Coo.capacity();
    t.GtHyp_Ncm.replace(0,t.GtHyp_Ncm.capacity(), pcStr.substring( p , p + t.GtHyp_Ncm.capacity() ) );p = p + t.GtHyp_Ncm.capacity();
    t.GtHyp_Npv.replace(0,t.GtHyp_Npv.capacity(), pcStr.substring( p , p + t.GtHyp_Npv.capacity() ) );p = p + t.GtHyp_Npv.capacity();
    t.GtHyp_Vtf.replace(0,t.GtHyp_Vtf.capacity(), pcStr.substring( p , p + t.GtHyp_Vtf.capacity() ) );p = p + t.GtHyp_Vtf.capacity();
    t.GtHyp_Vtd.replace(0,t.GtHyp_Vtd.capacity(), pcStr.substring( p , p + t.GtHyp_Vtd.capacity() ) );p = p + t.GtHyp_Vtd.capacity();
    t.GtHyp_Vts.replace(0,t.GtHyp_Vts.capacity(), pcStr.substring( p , p + t.GtHyp_Vts.capacity() ) );p = p + t.GtHyp_Vts.capacity();
    t.GtHyp_Vtb.replace(0,t.GtHyp_Vtb.capacity(), pcStr.substring( p , p + t.GtHyp_Vtb.capacity() ) );p = p + t.GtHyp_Vtb.capacity();
    t.GtHyp_Vlq.replace(0,t.GtHyp_Vlq.capacity(), pcStr.substring( p , p + t.GtHyp_Vlq.capacity() ) );p = p + t.GtHyp_Vlq.capacity();
    t.GtHyp_Vcn.replace(0,t.GtHyp_Vcn.capacity(), pcStr.substring( p , p + t.GtHyp_Vcn.capacity() ) );p = p + t.GtHyp_Vcn.capacity();
    t.GtHyp_Vsg.replace(0,t.GtHyp_Vsg.capacity(), pcStr.substring( p , p + t.GtHyp_Vsg.capacity() ) );p = p + t.GtHyp_Vsg.capacity();
    t.GtHyp_Csg.replace(0,t.GtHyp_Csg.capacity(), pcStr.substring( p , p + t.GtHyp_Csg.capacity() ) );p = p + t.GtHyp_Csg.capacity();
    t.GtHyp_Spz.replace(0,t.GtHyp_Spz.capacity(), pcStr.substring( p , p + t.GtHyp_Spz.capacity() ) );p = p + t.GtHyp_Spz.capacity();
    t.GtHyp_Sfv.replace(0,t.GtHyp_Sfv.capacity(), pcStr.substring( p , p + t.GtHyp_Sfv.capacity() ) );p = p + t.GtHyp_Sfv.capacity();
    t.GtHyp_Sum.replace(0,t.GtHyp_Sum.capacity(), pcStr.substring( p , p + t.GtHyp_Sum.capacity() ) );p = p + t.GtHyp_Sum.capacity();
    t.GtHyp_Svl.replace(0,t.GtHyp_Svl.capacity(), pcStr.substring( p , p + t.GtHyp_Svl.capacity() ) );p = p + t.GtHyp_Svl.capacity();
    t.GtHyp_Isg.replace(0,t.GtHyp_Isg.capacity(), pcStr.substring( p , p + t.GtHyp_Isg.capacity() ) );p = p + t.GtHyp_Isg.capacity();
    t.GtHyp_Nsg.replace(0,t.GtHyp_Nsg.capacity(), pcStr.substring( p , p + t.GtHyp_Nsg.capacity() ) );p = p + t.GtHyp_Nsg.capacity();
    t.GtHyp_Cps.replace(0,t.GtHyp_Cps.capacity(), pcStr.substring( p , p + t.GtHyp_Cps.capacity() ) );p = p + t.GtHyp_Cps.capacity();
    t.GtHyp_Ceb.replace(0,t.GtHyp_Ceb.capacity(), pcStr.substring( p , p + t.GtHyp_Ceb.capacity() ) );p = p + t.GtHyp_Ceb.capacity();
    t.GtHyp_Cfc.replace(0,t.GtHyp_Cfc.capacity(), pcStr.substring( p , p + t.GtHyp_Cfc.capacity() ) );p = p + t.GtHyp_Cfc.capacity();
    t.GtHyp_Cee.replace(0,t.GtHyp_Cee.capacity(), pcStr.substring( p , p + t.GtHyp_Cee.capacity() ) );p = p + t.GtHyp_Cee.capacity();
    t.GtHyp_Obs.replace(0,t.GtHyp_Obs.capacity(), pcStr.substring( p , p + t.GtHyp_Obs.capacity() ) );p = p + t.GtHyp_Obs.capacity();
    t.GtHyp_Psw.replace(0,t.GtHyp_Psw.capacity(), pcStr.substring( p , p + t.GtHyp_Psw.capacity() ) );p = p + t.GtHyp_Psw.capacity();
    t.GtHyp_Idx.replace(0,t.GtHyp_Idx.capacity(), pcStr.substring( p , p + t.GtHyp_Idx.capacity() ) );p = p + t.GtHyp_Idx.capacity();
    t.GtHyp_Ibs.replace(0,t.GtHyp_Ibs.capacity(), pcStr.substring( p , p + t.GtHyp_Ibs.capacity() ) );p = p + t.GtHyp_Ibs.capacity();
    t.GtHyp_Fll.replace(0,t.GtHyp_Fll.capacity(), pcStr.substring( p , p + t.GtHyp_Fll.capacity() ) );p = p + t.GtHyp_Fll.capacity();
    vec.add ( t );
    return vec;
  }
  //---------------------------------------------------------------------------------------
  public String LSet_De_ImgGtHyp(Buf_Img_GtHyp p_Img)
  {
    String pcStr = "";
    pcStr = pcStr + p_Img.getSeq();
    pcStr = pcStr + p_Img.getItb();
    pcStr = pcStr + p_Img.getItg();
    pcStr = pcStr + p_Img.getDpc();
    pcStr = pcStr + p_Img.getPct();
    pcStr = pcStr + p_Img.getDsb();
    pcStr = pcStr + p_Img.getCsv();
    pcStr = pcStr + p_Img.getLcl();
    pcStr = pcStr + p_Img.getRpo();
    pcStr = pcStr + p_Img.getFoj();
    pcStr = pcStr + p_Img.getNin();
    pcStr = pcStr + p_Img.getCll();
    pcStr = pcStr + p_Img.getCnm();
    pcStr = pcStr + p_Img.getLug();
    pcStr = pcStr + p_Img.getLnm();
    pcStr = pcStr + p_Img.getSec();
    pcStr = pcStr + p_Img.getSnb();
    pcStr = pcStr + p_Img.getCmn();
    pcStr = pcStr + p_Img.getCoo();
    pcStr = pcStr + p_Img.getNcm();
    pcStr = pcStr + p_Img.getNpv();
    pcStr = pcStr + p_Img.getVtf();
    pcStr = pcStr + p_Img.getVtd();
    pcStr = pcStr + p_Img.getVts();
    pcStr = pcStr + p_Img.getVtb();
    pcStr = pcStr + p_Img.getVlq();
    pcStr = pcStr + p_Img.getVcn();
    pcStr = pcStr + p_Img.getVsg();
    pcStr = pcStr + p_Img.getCsg();
    pcStr = pcStr + p_Img.getSpz();
    pcStr = pcStr + p_Img.getSfv();
    pcStr = pcStr + p_Img.getSum();
    pcStr = pcStr + p_Img.getSvl();
    pcStr = pcStr + p_Img.getIsg();
    pcStr = pcStr + p_Img.getNsg();
    pcStr = pcStr + p_Img.getCps();
    pcStr = pcStr + p_Img.getCeb();
    pcStr = pcStr + p_Img.getCfc();
    pcStr = pcStr + p_Img.getCee();
    pcStr = pcStr + p_Img.getObs();
    pcStr = pcStr + p_Img.getPsw();
    pcStr = pcStr + p_Img.getIdx();
    pcStr = pcStr + p_Img.getIbs();
    pcStr = pcStr + p_Img.getFll();
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
  public static class Buf_Img_GtHpd
  {
    public StringBuffer GtHpd_Seq = new StringBuffer(3  );   //9(03)        3 Secuencia
    public StringBuffer GtHpd_Sqd = new StringBuffer(3  );   //9(03)        6 Secuencia Detalle
    public StringBuffer GtHpd_Itp = new StringBuffer(2  );   //X(02)        8 Tipo (TE/ED/...)
    public StringBuffer GtHpd_Rs1 = new StringBuffer(5  );   //9(05)       13 Rol SII-1
    public StringBuffer GtHpd_Rs2 = new StringBuffer(3  );   //9(03)       16 Rol SII-2
    public StringBuffer GtHpd_Rds = new StringBuffer(20 );   //X(20)       36 Destino SII
    public StringBuffer GtHpd_Faf = new StringBuffer(8  );   //9(08)       44 Fecha AFiscal
    public StringBuffer GtHpd_Vaf = new StringBuffer(15 );   //9(13)V9(2)  59 Valor AFiscal Pesos
    public StringBuffer GtHpd_Umd = new StringBuffer(2  );   //X(02)       61 UMedida (M2/HA)
    public StringBuffer GtHpd_Udd = new StringBuffer(9  );   //9(07)V9(2)  70 Unidades (Cantidad <Umd>)
    public StringBuffer GtHpd_Umt = new StringBuffer(1  );   //X(01)       71 UMonetaria (P/U/D)
    public StringBuffer GtHpd_Pum = new StringBuffer(15 );   //9(11)V9(4)  86 PUnitario UMedida
    public StringBuffer GtHpd_Vtu = new StringBuffer(15 );   //9(11)V9(4) 101 VTasacion <Umt>
    public StringBuffer GtHpd_Vbu = new StringBuffer(15 );   //9(11)V9(4) 116 VBanco <Umt>
    public StringBuffer GtHpd_Vlu = new StringBuffer(15 );   //9(11)V9(4) 131 VLiquidacion <Umt>
    public StringBuffer GtHpd_Vsu = new StringBuffer(15 );   //9(11)V9(4) 146 VAsegurable <Umt>
    public StringBuffer GtHpd_Vts = new StringBuffer(15 );   //9(13)V9(2) 161 VTasacion <Mnd>
    public StringBuffer GtHpd_Vtb = new StringBuffer(15 );   //9(13)V9(2) 176 VBanco <Mnd>
    public StringBuffer GtHpd_Vlq = new StringBuffer(15 );   //9(13)V9(2) 191 VLiquidacion <Mnd>
    public StringBuffer GtHpd_Vsg = new StringBuffer(15 );   //9(11)V9(4) 206 VAsegurable <Mnd>
    public StringBuffer GtHpd_Vcn = new StringBuffer(15 );   //9(13)V9(2) 221 VContable <Mnd>
    public StringBuffer GtHpd_Ano = new StringBuffer(4  );   //9(04)      225 Prda.A�o/Edif.A�o
    public StringBuffer GtHpd_Nfa = new StringBuffer(7  );   //9(07)      232 Prda.Nro Factura
    public StringBuffer GtHpd_Mfa = new StringBuffer(15 );   //9(13)V9(2) 247 Prda.Monto Factura
    public StringBuffer GtHpd_Mka = new StringBuffer(20 );   //X(20)      267 Prda.Marca/TConstruccion
    public StringBuffer GtHpd_Mdl = new StringBuffer(20 );   //X(20)      287 Prda.Modelo/Materiales
    public StringBuffer GtHpd_Nmt = new StringBuffer(20 );   //X(20)      307 Prda.Numero Motor
    public StringBuffer GtHpd_Nse = new StringBuffer(20 );   //X(20)      327 Prda.Numero Serie
    public StringBuffer GtHpd_Nhu = new StringBuffer(7  );   //9(07)      334 Prda.Nro Horas Uso
    public StringBuffer GtHpd_Avu = new StringBuffer(7  );   //9(07)      341 Prda.A�os Vida Util
    public StringBuffer GtHpd_Plu = new StringBuffer(9  );   //X(09)      350 Prda.Patente
    public StringBuffer GtHpd_Str = new StringBuffer(30 );   //X(30)      380 Terr.Sector/Suelo.Tipo/OComp.Descripcion/Edif.Descripcion
    public StringBuffer GtHpd_Dag = new StringBuffer(1  );   //X(01)      381 Suelo.DerechosAgua
    public StringBuffer GtHpd_Dsr = new StringBuffer(1  );   //X(01)      382 Suelo.DAG SufRiego
    public StringBuffer GtHpd_Mft = new StringBuffer(5  );   //9(05)      387 Terr.Mts Frente
    public StringBuffer GtHpd_Mpf = new StringBuffer(5  );   //9(05)      392 Terr.Mts Profundidad
    public StringBuffer GtHpd_Mex = new StringBuffer(5  );   //9(05)      397 Terr.M2 Expropiable
    public StringBuffer GtHpd_Fma = new StringBuffer(15 );   //X(15)      412 Terr.Forma/Edif.Agrupacion
    public StringBuffer GtHpd_Dst = new StringBuffer(15 );   //X(15)      427 Terr.Destino/Edif.Calidad
    public StringBuffer GtHpd_Uso = new StringBuffer(15 );   //X(15)      442 Terr.Uso/Edif.Uso
    public StringBuffer GtHpd_Tpe = new StringBuffer(15 );   //X(15)      457 Terr.TpoExpropiacion/Edif.TpoConstruccion
    public StringBuffer GtHpd_Ped = new StringBuffer(7  );   //9(07)      464 Edif.PermEdificacion
    public StringBuffer GtHpd_Fed = new StringBuffer(8  );   //9(08)      472 Edif.Fecha PED
    public StringBuffer GtHpd_Rem = new StringBuffer(7  );   //9(07)      479 Edif.RecepMunicipal
    public StringBuffer GtHpd_Nam = new StringBuffer(1  );   //X(01)      480 Edif.Sin Ampliaciones
    public StringBuffer GtHpd_Ams = new StringBuffer(1  );   //X(01)      481 Edif.AmpliAprob Si
    public StringBuffer GtHpd_Amn = new StringBuffer(1  );   //X(01)      482 Edif.AmpliAprob No
    public Vector      vGtHpd_Pgm = new Vector(17);          //X(34)      516 Edif.Programa(0 To 16)
    public Vector      vGtHpd_Est = new Vector(11);          //X(11)      527 Edif.Estructura(0 To 10)
    public Vector      vGtHpd_Ter = new Vector(11);          //X(11)      538 Edif.Terminaciones(0 To 10)
    public Vector      vGtHpd_Pav = new Vector(11);          //X(11)      549 Edif.Pavimentos(0 To 10)
    public Vector      vGtHpd_Cub = new Vector(11);          //X(11)      560 Edif.Cubiertas(0 To 10)
    public Vector      vGtHpd_Int = new Vector(11);          //X(11)      571 Edif.Instalaciones(0 To 10)
    public StringBuffer GtHpd_Plq = new StringBuffer(5  );   //9(03)V9(2) 576 %Liquidacion
    public StringBuffer GtHpd_Ovb = new StringBuffer(9  );   //9(09)      585 @CLB Obs VBanco
    public StringBuffer GtHpd_Obt = new StringBuffer(9  );   //9(09)      594 @CLB Obs Detalle
    public StringBuffer GtHpd_Fll = new StringBuffer(233);   //X(233)     827 Disponible

    public StringBuffer GtHpd_Pgm = new StringBuffer(2  );   //9(02)          Edif.Programa
    public StringBuffer GtHpd_Est = new StringBuffer(1  );   //X(01)          Edif.Estructura
    public StringBuffer GtHpd_Ter = new StringBuffer(1  );   //X(01)          Edif.Terminaciones
    public StringBuffer GtHpd_Pav = new StringBuffer(1  );   //X(01)          Edif.Pavimentos
    public StringBuffer GtHpd_Cub = new StringBuffer(1  );   //X(01)          Edif.Cubiertas
    public StringBuffer GtHpd_Int = new StringBuffer(1  );   //X(01)          Edif.Instalaciones

    public String getSeq()         {return GtHpd_Seq.toString();}
    public String getSqd()         {return GtHpd_Sqd.toString();}
    public String getItp()         {return GtHpd_Itp.toString();}
    public String getRs1()         {return GtHpd_Rs1.toString();}
    public String getRs2()         {return GtHpd_Rs2.toString();}
    public String getRds()         {return GtHpd_Rds.toString();}
    public String getFaf()         {return GtHpd_Faf.toString();}
    public String getVaf()         {return GtHpd_Vaf.toString();}
    public String getUmd()         {return GtHpd_Umd.toString();}
    public String getUdd()         {return GtHpd_Udd.toString();}
    public String getUmt()         {return GtHpd_Umt.toString();}
    public String getPum()         {return GtHpd_Pum.toString();}
    public String getVtu()         {return GtHpd_Vtu.toString();}
    public String getVbu()         {return GtHpd_Vbu.toString();}
    public String getVlu()         {return GtHpd_Vlu.toString();}
    public String getVsu()         {return GtHpd_Vsu.toString();}
    public String getVts()         {return GtHpd_Vts.toString();}
    public String getVtb()         {return GtHpd_Vtb.toString();}
    public String getVlq()         {return GtHpd_Vlq.toString();}
    public String getVsg()         {return GtHpd_Vsg.toString();}
    public String getVcn()         {return GtHpd_Vcn.toString();}
    public String getAno()         {return GtHpd_Ano.toString();}
    public String getNfa()         {return GtHpd_Nfa.toString();}
    public String getMfa()         {return GtHpd_Mfa.toString();}
    public String getMka()         {return GtHpd_Mka.toString();}
    public String getMdl()         {return GtHpd_Mdl.toString();}
    public String getNmt()         {return GtHpd_Nmt.toString();}
    public String getNse()         {return GtHpd_Nse.toString();}
    public String getNhu()         {return GtHpd_Nhu.toString();}
    public String getAvu()         {return GtHpd_Avu.toString();}
    public String getPlu()         {return GtHpd_Plu.toString();}
    public String getStr()         {return GtHpd_Str.toString();}
    public String getDag()         {return GtHpd_Dag.toString();}
    public String getDsr()         {return GtHpd_Dsr.toString();}
    public String getMft()         {return GtHpd_Mft.toString();}
    public String getMpf()         {return GtHpd_Mpf.toString();}
    public String getMex()         {return GtHpd_Mex.toString();}
    public String getFma()         {return GtHpd_Fma.toString();}
    public String getDst()         {return GtHpd_Dst.toString();}
    public String getUso()         {return GtHpd_Uso.toString();}
    public String getTpe()         {return GtHpd_Tpe.toString();}
    public String getPed()         {return GtHpd_Ped.toString();}
    public String getFed()         {return GtHpd_Fed.toString();}
    public String getRem()         {return GtHpd_Rem.toString();}
    public String getNam()         {return GtHpd_Nam.toString();}
    public String getAms()         {return GtHpd_Ams.toString();}
    public String getAmn()         {return GtHpd_Amn.toString();}
    public Vector getVecPgm()      {return vGtHpd_Pgm;}
    public Vector getVecEst()      {return vGtHpd_Est;}
    public Vector getVecTer()      {return vGtHpd_Ter;}
    public Vector getVecPav()      {return vGtHpd_Pav;}
    public Vector getVecCub()      {return vGtHpd_Cub;}
    public Vector getVecInt()      {return vGtHpd_Int;}
    public String getPlq()         {return GtHpd_Plq.toString();}
    public String getOvb()         {return GtHpd_Ovb.toString();}
    public String getObt()         {return GtHpd_Obt.toString();}
    public String getFll()         {return GtHpd_Fll.toString();}

    public Formateo f = new Formateo();
    public String fgetFaf()        {return f.FormatDate(GtHpd_Faf.toString());}
    public String fgetVaf()        {return f.Formato(GtHpd_Vaf.toString(),"99.999.999.999",2,0,',');}
    public String egetVaf()        {return f.Formato(GtHpd_Vaf.toString(),"99.999.999.999",2,2,',');}
    public String fgetUmd()        {return GtHpd_Umd.toString();}
    public String fgetUdd()        {return f.Formato(GtHpd_Udd.toString(),"99.999.999.999",2,2,',');}
    public String fgetUmt()
                                   {
                                     String lcUmt="";
                                     if (getUmt().equals("P"))  { lcUmt = "$"; }
                                     if (getUmt().equals("D"))  { lcUmt = "US$"; }
                                     if (getUmt().equals("U"))  { lcUmt = "UF"; }
                                     return lcUmt;
                                   }
    public String fgetPum()
                                   {
                                     int liDcm=0;
                                     if (getUmt().equals("P"))  { liDcm = 0; }
                                     if (getUmt().equals("D"))  { liDcm = 2; }
                                     if (getUmt().equals("U"))  { liDcm = 4; }
                                     return f.Formato(GtHpd_Pum.toString(),"99.999.999.999",4,liDcm,',');
                                   }
    public String fgetVtu()
                                   {
                                     int liDcm=0;
                                     if (getUmt().equals("P"))  { liDcm = 0; }
                                     if (getUmt().equals("D"))  { liDcm = 2; }
                                     if (getUmt().equals("U"))  { liDcm = 4; }
                                     return f.Formato(GtHpd_Vtu.toString(),"99.999.999.999",4,liDcm,',');
                                   }
    public String fgetVbu()
                                   {
                                     int liDcm=0;
                                     if (getUmt().equals("P"))  { liDcm = 0; }
                                     if (getUmt().equals("D"))  { liDcm = 2; }
                                     if (getUmt().equals("U"))  { liDcm = 4; }
                                     return f.Formato(GtHpd_Vbu.toString(),"99.999.999.999",4,liDcm,',');
                                   }
    public String fgetVlu()
                                   {
                                     int liDcm=0;
                                     if (getUmt().equals("P"))  { liDcm = 0; }
                                     if (getUmt().equals("D"))  { liDcm = 2; }
                                     if (getUmt().equals("U"))  { liDcm = 4; }
                                     return f.Formato(GtHpd_Vlu.toString(),"99.999.999.999",4,liDcm,',');
                                   }
    public String fgetVsu()
                                   {
                                     int liDcm=0;
                                     if (getUmt().equals("P"))  { liDcm = 0; }
                                     if (getUmt().equals("D"))  { liDcm = 2; }
                                     if (getUmt().equals("U"))  { liDcm = 4; }
                                     return f.Formato(GtHpd_Vsu.toString(),"99.999.999.999",4,liDcm,',');
                                   }
    public String fgetVts()        {return f.Formato(GtHpd_Vts.toString(),"99.999.999.999",2,0,',');}
    public String fgetVtb()        {return f.Formato(GtHpd_Vtb.toString(),"99.999.999.999",2,0,',');}
    public String fgetVlq()        {return f.Formato(GtHpd_Vlq.toString(),"99.999.999.999",2,0,',');}
    public String fgetVsg()        {return f.Formato(GtHpd_Vsg.toString(),"99.999.999.999",2,0,',');}
    public String fgetVcn()        {return f.Formato(GtHpd_Vcn.toString(),"99.999.999.999",2,0,',');}
    public String egetVts()        {return f.Formato(GtHpd_Vts.toString(),"99.999.999.999",2,2,',');}
    public String egetVtb()        {return f.Formato(GtHpd_Vtb.toString(),"99.999.999.999",2,2,',');}
    public String egetVlq()        {return f.Formato(GtHpd_Vlq.toString(),"99.999.999.999",2,2,',');}
    public String egetVsg()        {return f.Formato(GtHpd_Vsg.toString(),"99.999.999.999",2,2,',');}
    public String egetVcn()        {return f.Formato(GtHpd_Vcn.toString(),"99.999.999.999",2,2,',');}
    public String fgetStr()        {return GtHpd_Str.toString().trim();}
    public String fgetMft()        {return f.Formato(GtHpd_Mft.toString(),"99.999",0,0,',');}
    public String fgetMpf()        {return f.Formato(GtHpd_Mpf.toString(),"99.999",0,0,',');}
    public String fgetMex()        {return f.Formato(GtHpd_Mex.toString(),"99.999",0,0,',');}
    public String fgetPlq()        {return f.Formato(GtHpd_Plq.toString(),"999",2,2,',');}

    public String pgetUdd()        {return f.Formato(GtHpd_Udd.toString(),"BB.BB9",2,2,',');}
    public String pgetPum()        {return f.Formato(GtHpd_Pum.toString(),"B.BBB.BBB.BB9",4,2,',');}
    public String pgetVtu()        {return f.Formato(GtHpd_Vtu.toString(),"B.BBB.BBB.BB9",4,2,',');}
    public String pgetVts()        {return f.Formato(GtHpd_Vts.toString(),"BB.BBB.BBB.BB9",2,0,',');}

    public String qgetVts()        {return f.Formato(GtHpd_Vts.toString(),"BB.BBB.BBB.BB9",2,2,',');}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_Img_GtHpd LSet_A_ImgGtHpd(String pcStr)
  {
    Buf_Img_GtHpd l_Img = new Buf_Img_GtHpd();
    Vector vDatos = LSet_A_vImgGtHpd(pcStr);
    l_Img = (Buf_Img_GtHpd)vDatos.elementAt(0);
    return l_Img;
  }
  //---------------------------------------------------------------------------------------
  public static Vector LSet_A_vImgGtHpd(String pcStr)
  {
    int p=0;
    Vector vec= new Vector();
    Buf_Img_GtHpd t = new Buf_Img_GtHpd();
    t.GtHpd_Seq.replace(0,t.GtHpd_Seq.capacity(), pcStr.substring( p , p + t.GtHpd_Seq.capacity() ) );p = p + t.GtHpd_Seq.capacity();
    t.GtHpd_Sqd.replace(0,t.GtHpd_Sqd.capacity(), pcStr.substring( p , p + t.GtHpd_Sqd.capacity() ) );p = p + t.GtHpd_Sqd.capacity();
    t.GtHpd_Itp.replace(0,t.GtHpd_Itp.capacity(), pcStr.substring( p , p + t.GtHpd_Itp.capacity() ) );p = p + t.GtHpd_Itp.capacity();
    t.GtHpd_Rs1.replace(0,t.GtHpd_Rs1.capacity(), pcStr.substring( p , p + t.GtHpd_Rs1.capacity() ) );p = p + t.GtHpd_Rs1.capacity();
    t.GtHpd_Rs2.replace(0,t.GtHpd_Rs2.capacity(), pcStr.substring( p , p + t.GtHpd_Rs2.capacity() ) );p = p + t.GtHpd_Rs2.capacity();
    t.GtHpd_Rds.replace(0,t.GtHpd_Rds.capacity(), pcStr.substring( p , p + t.GtHpd_Rds.capacity() ) );p = p + t.GtHpd_Rds.capacity();
    t.GtHpd_Faf.replace(0,t.GtHpd_Faf.capacity(), pcStr.substring( p , p + t.GtHpd_Faf.capacity() ) );p = p + t.GtHpd_Faf.capacity();
    t.GtHpd_Vaf.replace(0,t.GtHpd_Vaf.capacity(), pcStr.substring( p , p + t.GtHpd_Vaf.capacity() ) );p = p + t.GtHpd_Vaf.capacity();
    t.GtHpd_Umd.replace(0,t.GtHpd_Umd.capacity(), pcStr.substring( p , p + t.GtHpd_Umd.capacity() ) );p = p + t.GtHpd_Umd.capacity();
    t.GtHpd_Udd.replace(0,t.GtHpd_Udd.capacity(), pcStr.substring( p , p + t.GtHpd_Udd.capacity() ) );p = p + t.GtHpd_Udd.capacity();
    t.GtHpd_Umt.replace(0,t.GtHpd_Umt.capacity(), pcStr.substring( p , p + t.GtHpd_Umt.capacity() ) );p = p + t.GtHpd_Umt.capacity();
    t.GtHpd_Pum.replace(0,t.GtHpd_Pum.capacity(), pcStr.substring( p , p + t.GtHpd_Pum.capacity() ) );p = p + t.GtHpd_Pum.capacity();
    t.GtHpd_Vtu.replace(0,t.GtHpd_Vtu.capacity(), pcStr.substring( p , p + t.GtHpd_Vtu.capacity() ) );p = p + t.GtHpd_Vtu.capacity();
    t.GtHpd_Vbu.replace(0,t.GtHpd_Vbu.capacity(), pcStr.substring( p , p + t.GtHpd_Vbu.capacity() ) );p = p + t.GtHpd_Vbu.capacity();
    t.GtHpd_Vlu.replace(0,t.GtHpd_Vlu.capacity(), pcStr.substring( p , p + t.GtHpd_Vlu.capacity() ) );p = p + t.GtHpd_Vlu.capacity();
    t.GtHpd_Vsu.replace(0,t.GtHpd_Vsu.capacity(), pcStr.substring( p , p + t.GtHpd_Vsu.capacity() ) );p = p + t.GtHpd_Vsu.capacity();
    t.GtHpd_Vts.replace(0,t.GtHpd_Vts.capacity(), pcStr.substring( p , p + t.GtHpd_Vts.capacity() ) );p = p + t.GtHpd_Vts.capacity();
    t.GtHpd_Vtb.replace(0,t.GtHpd_Vtb.capacity(), pcStr.substring( p , p + t.GtHpd_Vtb.capacity() ) );p = p + t.GtHpd_Vtb.capacity();
    t.GtHpd_Vlq.replace(0,t.GtHpd_Vlq.capacity(), pcStr.substring( p , p + t.GtHpd_Vlq.capacity() ) );p = p + t.GtHpd_Vlq.capacity();
    t.GtHpd_Vsg.replace(0,t.GtHpd_Vsg.capacity(), pcStr.substring( p , p + t.GtHpd_Vsg.capacity() ) );p = p + t.GtHpd_Vsg.capacity();
    t.GtHpd_Vcn.replace(0,t.GtHpd_Vcn.capacity(), pcStr.substring( p , p + t.GtHpd_Vcn.capacity() ) );p = p + t.GtHpd_Vcn.capacity();
    t.GtHpd_Ano.replace(0,t.GtHpd_Ano.capacity(), pcStr.substring( p , p + t.GtHpd_Ano.capacity() ) );p = p + t.GtHpd_Ano.capacity();
    t.GtHpd_Nfa.replace(0,t.GtHpd_Nfa.capacity(), pcStr.substring( p , p + t.GtHpd_Nfa.capacity() ) );p = p + t.GtHpd_Nfa.capacity();
    t.GtHpd_Mfa.replace(0,t.GtHpd_Mfa.capacity(), pcStr.substring( p , p + t.GtHpd_Mfa.capacity() ) );p = p + t.GtHpd_Mfa.capacity();
    t.GtHpd_Mka.replace(0,t.GtHpd_Mka.capacity(), pcStr.substring( p , p + t.GtHpd_Mka.capacity() ) );p = p + t.GtHpd_Mka.capacity();
    t.GtHpd_Mdl.replace(0,t.GtHpd_Mdl.capacity(), pcStr.substring( p , p + t.GtHpd_Mdl.capacity() ) );p = p + t.GtHpd_Mdl.capacity();
    t.GtHpd_Nmt.replace(0,t.GtHpd_Nmt.capacity(), pcStr.substring( p , p + t.GtHpd_Nmt.capacity() ) );p = p + t.GtHpd_Nmt.capacity();
    t.GtHpd_Nse.replace(0,t.GtHpd_Nse.capacity(), pcStr.substring( p , p + t.GtHpd_Nse.capacity() ) );p = p + t.GtHpd_Nse.capacity();
    t.GtHpd_Nhu.replace(0,t.GtHpd_Nhu.capacity(), pcStr.substring( p , p + t.GtHpd_Nhu.capacity() ) );p = p + t.GtHpd_Nhu.capacity();
    t.GtHpd_Avu.replace(0,t.GtHpd_Avu.capacity(), pcStr.substring( p , p + t.GtHpd_Avu.capacity() ) );p = p + t.GtHpd_Avu.capacity();
    t.GtHpd_Plu.replace(0,t.GtHpd_Plu.capacity(), pcStr.substring( p , p + t.GtHpd_Plu.capacity() ) );p = p + t.GtHpd_Plu.capacity();
    t.GtHpd_Str.replace(0,t.GtHpd_Str.capacity(), pcStr.substring( p , p + t.GtHpd_Str.capacity() ) );p = p + t.GtHpd_Str.capacity();
    t.GtHpd_Dag.replace(0,t.GtHpd_Dag.capacity(), pcStr.substring( p , p + t.GtHpd_Dag.capacity() ) );p = p + t.GtHpd_Dag.capacity();
    t.GtHpd_Dsr.replace(0,t.GtHpd_Dsr.capacity(), pcStr.substring( p , p + t.GtHpd_Dsr.capacity() ) );p = p + t.GtHpd_Dsr.capacity();
    t.GtHpd_Mft.replace(0,t.GtHpd_Mft.capacity(), pcStr.substring( p , p + t.GtHpd_Mft.capacity() ) );p = p + t.GtHpd_Mft.capacity();
    t.GtHpd_Mpf.replace(0,t.GtHpd_Mpf.capacity(), pcStr.substring( p , p + t.GtHpd_Mpf.capacity() ) );p = p + t.GtHpd_Mpf.capacity();
    t.GtHpd_Mex.replace(0,t.GtHpd_Mex.capacity(), pcStr.substring( p , p + t.GtHpd_Mex.capacity() ) );p = p + t.GtHpd_Mex.capacity();
    t.GtHpd_Fma.replace(0,t.GtHpd_Fma.capacity(), pcStr.substring( p , p + t.GtHpd_Fma.capacity() ) );p = p + t.GtHpd_Fma.capacity();
    t.GtHpd_Dst.replace(0,t.GtHpd_Dst.capacity(), pcStr.substring( p , p + t.GtHpd_Dst.capacity() ) );p = p + t.GtHpd_Dst.capacity();
    t.GtHpd_Uso.replace(0,t.GtHpd_Uso.capacity(), pcStr.substring( p , p + t.GtHpd_Uso.capacity() ) );p = p + t.GtHpd_Uso.capacity();
    t.GtHpd_Tpe.replace(0,t.GtHpd_Tpe.capacity(), pcStr.substring( p , p + t.GtHpd_Tpe.capacity() ) );p = p + t.GtHpd_Tpe.capacity();
    t.GtHpd_Ped.replace(0,t.GtHpd_Ped.capacity(), pcStr.substring( p , p + t.GtHpd_Ped.capacity() ) );p = p + t.GtHpd_Ped.capacity();
    t.GtHpd_Fed.replace(0,t.GtHpd_Fed.capacity(), pcStr.substring( p , p + t.GtHpd_Fed.capacity() ) );p = p + t.GtHpd_Fed.capacity();
    t.GtHpd_Rem.replace(0,t.GtHpd_Rem.capacity(), pcStr.substring( p , p + t.GtHpd_Rem.capacity() ) );p = p + t.GtHpd_Rem.capacity();
    t.GtHpd_Nam.replace(0,t.GtHpd_Nam.capacity(), pcStr.substring( p , p + t.GtHpd_Nam.capacity() ) );p = p + t.GtHpd_Nam.capacity();
    t.GtHpd_Ams.replace(0,t.GtHpd_Ams.capacity(), pcStr.substring( p , p + t.GtHpd_Ams.capacity() ) );p = p + t.GtHpd_Ams.capacity();
    t.GtHpd_Amn.replace(0,t.GtHpd_Amn.capacity(), pcStr.substring( p , p + t.GtHpd_Amn.capacity() ) );p = p + t.GtHpd_Amn.capacity();
    String lcData="";
    for (int i=0;i<17;i++)
        {
          lcData = pcStr.substring(p, p + t.GtHpd_Pgm.capacity());
          t.vGtHpd_Pgm.add(lcData);
          p = p + t.GtHpd_Pgm.capacity();
        }
    for (int i=0;i<11;i++)
        {
          lcData = pcStr.substring(p, p + t.GtHpd_Est.capacity());
          t.vGtHpd_Est.add(lcData);
          p = p + t.GtHpd_Est.capacity();
        }
    for (int i=0;i<11;i++)
        {
          lcData = pcStr.substring(p, p + t.GtHpd_Ter.capacity());
          t.vGtHpd_Ter.add(lcData);
          p = p + t.GtHpd_Ter.capacity();
        }
    for (int i=0;i<11;i++)
        {
          lcData = pcStr.substring(p, p + t.GtHpd_Pav.capacity());
          t.vGtHpd_Pav.add(lcData);
          p = p + t.GtHpd_Pav.capacity();
        }
    for (int i=0;i<11;i++)
        {
          lcData = pcStr.substring(p, p + t.GtHpd_Cub.capacity());
          t.vGtHpd_Cub.add(lcData);
          p = p + t.GtHpd_Cub.capacity();
        }
    for (int i=0;i<11;i++)
        {
          lcData = pcStr.substring(p, p + t.GtHpd_Int.capacity());
          t.vGtHpd_Int.add(lcData);
          p = p + t.GtHpd_Int.capacity();
        }
    t.GtHpd_Plq.replace(0,t.GtHpd_Plq.capacity(), pcStr.substring( p , p + t.GtHpd_Plq.capacity() ) );p = p + t.GtHpd_Plq.capacity();
    t.GtHpd_Ovb.replace(0,t.GtHpd_Ovb.capacity(), pcStr.substring( p , p + t.GtHpd_Ovb.capacity() ) );p = p + t.GtHpd_Ovb.capacity();
    t.GtHpd_Obt.replace(0,t.GtHpd_Obt.capacity(), pcStr.substring( p , p + t.GtHpd_Obt.capacity() ) );p = p + t.GtHpd_Obt.capacity();
    t.GtHpd_Fll.replace(0,t.GtHpd_Fll.capacity(), pcStr.substring( p , p + t.GtHpd_Fll.capacity() ) );p = p + t.GtHpd_Fll.capacity();
    vec.add ( t );
    return vec;
  }
  //---------------------------------------------------------------------------------------
  public String LSet_De_ImgGtHpd(Buf_Img_GtHpd p_Img)
  {
    String pcStr = "";
    pcStr = pcStr + p_Img.getSeq();
    pcStr = pcStr + p_Img.getSqd();
    pcStr = pcStr + p_Img.getItp();
    pcStr = pcStr + p_Img.getRs1();
    pcStr = pcStr + p_Img.getRs2();
    pcStr = pcStr + p_Img.getRds();
    pcStr = pcStr + p_Img.getFaf();
    pcStr = pcStr + p_Img.getVaf();
    pcStr = pcStr + p_Img.getUmd();
    pcStr = pcStr + p_Img.getUdd();
    pcStr = pcStr + p_Img.getUmt();
    pcStr = pcStr + p_Img.getPum();
    pcStr = pcStr + p_Img.getVtu();
    pcStr = pcStr + p_Img.getVbu();
    pcStr = pcStr + p_Img.getVlu();
    pcStr = pcStr + p_Img.getVsu();
    pcStr = pcStr + p_Img.getVts();
    pcStr = pcStr + p_Img.getVtb();
    pcStr = pcStr + p_Img.getVlq();
    pcStr = pcStr + p_Img.getVsg();
    pcStr = pcStr + p_Img.getVcn();
    pcStr = pcStr + p_Img.getAno();
    pcStr = pcStr + p_Img.getNfa();
    pcStr = pcStr + p_Img.getMfa();
    pcStr = pcStr + p_Img.getMka();
    pcStr = pcStr + p_Img.getMdl();
    pcStr = pcStr + p_Img.getNmt();
    pcStr = pcStr + p_Img.getNse();
    pcStr = pcStr + p_Img.getNhu();
    pcStr = pcStr + p_Img.getAvu();
    pcStr = pcStr + p_Img.getPlu();
    pcStr = pcStr + p_Img.getStr();
    pcStr = pcStr + p_Img.getDag();
    pcStr = pcStr + p_Img.getDsr();
    pcStr = pcStr + p_Img.getMft();
    pcStr = pcStr + p_Img.getMpf();
    pcStr = pcStr + p_Img.getMex();
    pcStr = pcStr + p_Img.getFma();
    pcStr = pcStr + p_Img.getDst();
    pcStr = pcStr + p_Img.getUso();
    pcStr = pcStr + p_Img.getTpe();
    pcStr = pcStr + p_Img.getPed();
    pcStr = pcStr + p_Img.getFed();
    pcStr = pcStr + p_Img.getRem();
    pcStr = pcStr + p_Img.getNam();
    pcStr = pcStr + p_Img.getAms();
    pcStr = pcStr + p_Img.getAmn();
    String lcData ="";
    for (int i=0;i<17;i++)
        {lcData = (String)((Vector)p_Img.getVecPgm()).elementAt(i); pcStr = pcStr + lcData;}
    for (int i=0;i<11;i++)
        {lcData = (String)((Vector)p_Img.getVecEst()).elementAt(i); pcStr = pcStr + lcData;}
    for (int i=0;i<11;i++)
        {lcData = (String)((Vector)p_Img.getVecTer()).elementAt(i); pcStr = pcStr + lcData;}
    for (int i=0;i<11;i++)
        {lcData = (String)((Vector)p_Img.getVecPav()).elementAt(i); pcStr = pcStr + lcData;}
    for (int i=0;i<11;i++)
        {lcData = (String)((Vector)p_Img.getVecCub()).elementAt(i); pcStr = pcStr + lcData;}
    for (int i=0;i<11;i++)
        {lcData = (String)((Vector)p_Img.getVecInt()).elementAt(i); pcStr = pcStr + lcData;}
    pcStr = pcStr + p_Img.getPlq();
    pcStr = pcStr + p_Img.getOvb();
    pcStr = pcStr + p_Img.getObt();
    pcStr = pcStr + p_Img.getFll();
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
  public static class Buf_Img_GtAvn
  {
    public StringBuffer GtAvn_Seq = new StringBuffer(3  );  //9(03)        3 Secuencia
    public StringBuffer GtAvn_Itb = new StringBuffer(2  );  //X(02)        5 Tipo Bien (HP/PD)
    public StringBuffer GtAvn_Itg = new StringBuffer(5  );  //X(05)       10 Tipo Gtia CodGen
    public StringBuffer GtAvn_Dpc = new StringBuffer(10 );  //X(10)       20 Producto Contable
    public StringBuffer GtAvn_Dsb = new StringBuffer(30 );  //X(30)       50 Descripcion Bien
    public StringBuffer GtAvn_Sqd = new StringBuffer(3  );  //9(03)       53 Secuencia
    public StringBuffer GtAvn_Frv = new StringBuffer(8  );  //9(08)       61 FRevalorizacion
    public StringBuffer GtAvn_Vur = new StringBuffer(9  );  //9(07)V9(2)  70 VConv <Trj> a FRV
    public StringBuffer GtAvn_Vtt = new StringBuffer(15 );  //9(11)V9(4)  85 VTasacion UMedida
    public StringBuffer GtAvn_Vts = new StringBuffer(15 );  //9(13)V9(2) 100 VTasacion Moneda
    public StringBuffer GtAvn_Vcn = new StringBuffer(15 );  //9(13)V9(2) 115 VContable Moneda
    public StringBuffer GtAvn_Vlq = new StringBuffer(15 );  //9(13)V9(2) 130 VContable Moneda
    public StringBuffer GtAvn_Vrt = new StringBuffer(15 );  //9(13)V9(2) 145 VContable Moneda
    public StringBuffer GtAvn_Fvt = new StringBuffer(8  );  //9(08)      153 FVcto Tasacion
    public StringBuffer GtAvn_Ctr = new StringBuffer(30 );  //X(30)      183 Constructora
    public StringBuffer GtAvn_Fin = new StringBuffer(8  );  //9(08)      191 Fecha Inicio
    public StringBuffer GtAvn_Fte = new StringBuffer(8  );  //9(08)      199 Fecha EstTermino
    public StringBuffer GtAvn_Ped = new StringBuffer(7  );  //9(07)      206 PermEdificacion
    public StringBuffer GtAvn_Fed = new StringBuffer(8  );  //9(08)      214 Fecha PED
    public StringBuffer GtAvn_Spc = new StringBuffer(5  );  //9(05)      219 Sup a Construir [NO]
    public StringBuffer GtAvn_Sps = new StringBuffer(5  );  //9(05)      224 Sup Saldo [%IVA]
    public StringBuffer GtAvn_Rem = new StringBuffer(7  );  //9(07)      231 RecepMunicipal
    public StringBuffer GtAvn_Vsg = new StringBuffer(15 );  //9(11)V9(4) 246 VSeguro <Trj>
    public StringBuffer GtAvn_Dpy = new StringBuffer(184);  //X(184)     430 46*4 DescProyecto [NO]
    public StringBuffer GtAvn_Occ = new StringBuffer(184);  //X(184)     614 46*4 ObsContrato [NO]
    public StringBuffer GtAvn_Opp = new StringBuffer(184);  //X(184)     798 46*4 ObsEstado Pago [NO]
    public StringBuffer GtAvn_Pct = new StringBuffer(5  );  //9(03)V9(2) 803 %Castigo SBIF PROCOS
    public StringBuffer GtAvn_Fll = new StringBuffer(23 );  //X(23)      826 No Usado
    public StringBuffer GtAvn_Iro = new StringBuffer(1  );  //X(01)      827 Idr ReadOnly

    public String getSeq()         {return GtAvn_Seq.toString();}
    public String getItb()         {return GtAvn_Itb.toString();}
    public String getItg()         {return GtAvn_Itg.toString();}
    public String getDpc()         {return GtAvn_Dpc.toString();}
    public String getDsb()         {return GtAvn_Dsb.toString();}
    public String getSqd()         {return GtAvn_Sqd.toString();}
    public String getFrv()         {return GtAvn_Frv.toString();}
    public String getVur()         {return GtAvn_Vur.toString();}
    public String getVtt()         {return GtAvn_Vtt.toString();}
    public String getVts()         {return GtAvn_Vts.toString();}
    public String getVcn()         {return GtAvn_Vcn.toString();}
    public String getVlq()         {return GtAvn_Vlq.toString();}
    public String getVrt()         {return GtAvn_Vrt.toString();}
    public String getFvt()         {return GtAvn_Fvt.toString();}
    public String getCtr()         {return GtAvn_Ctr.toString();}
    public String getFin()         {return GtAvn_Fin.toString();}
    public String getFte()         {return GtAvn_Fte.toString();}
    public String getPed()         {return GtAvn_Ped.toString();}
    public String getFed()         {return GtAvn_Fed.toString();}
    public String getSpc()         {return GtAvn_Spc.toString();}
    public String getSps()         {return GtAvn_Sps.toString();}
    public String getRem()         {return GtAvn_Rem.toString();}
    public String getVsg()         {return GtAvn_Vsg.toString();}
    public String getDpy()         {return GtAvn_Dpy.toString();}
    public String getOcc()         {return GtAvn_Occ.toString();}
    public String getOpp()         {return GtAvn_Opp.toString();}
    public String getPct()         {return GtAvn_Pct.toString();}
    public String getFll()         {return GtAvn_Fll.toString();}
    public String getIro()         {return GtAvn_Iro.toString();}

    public Formateo f = new Formateo();
    public String fgetFin()        {return f.FormatDate(GtAvn_Fin.toString());}
    public String fgetFte()        {return f.FormatDate(GtAvn_Fte.toString());}
    public String fgetFed()        {return f.FormatDate(GtAvn_Fed.toString());}
  //public String fgetVtt()        {return f.Formato(GtHyp_Vtt.toString(),"99.999.999.999",4,4,',');}
  //public String fgetVts()        {return f.Formato(GtHyp_Vts.toString(),"99.999.999.999",2,2,',');}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_Img_GtAvn LSet_A_ImgGtAvn(String pcStr)
  {
    Buf_Img_GtAvn l_Img = new Buf_Img_GtAvn();
    Vector vDatos = LSet_A_vImgGtAvn(pcStr);
    l_Img = (Buf_Img_GtAvn)vDatos.elementAt(0);
    return l_Img;
  }
  //---------------------------------------------------------------------------------------
  public static Vector LSet_A_vImgGtAvn(String pcStr)
  {
    int p=0;
    Vector vec= new Vector();
    Buf_Img_GtAvn t = new Buf_Img_GtAvn();
    t.GtAvn_Seq.replace(0,t.GtAvn_Seq.capacity(), pcStr.substring( p , p + t.GtAvn_Seq.capacity() ) );p = p + t.GtAvn_Seq.capacity();
    t.GtAvn_Itb.replace(0,t.GtAvn_Itb.capacity(), pcStr.substring( p , p + t.GtAvn_Itb.capacity() ) );p = p + t.GtAvn_Itb.capacity();
    t.GtAvn_Itg.replace(0,t.GtAvn_Itg.capacity(), pcStr.substring( p , p + t.GtAvn_Itg.capacity() ) );p = p + t.GtAvn_Itg.capacity();
    t.GtAvn_Dpc.replace(0,t.GtAvn_Dpc.capacity(), pcStr.substring( p , p + t.GtAvn_Dpc.capacity() ) );p = p + t.GtAvn_Dpc.capacity();
    t.GtAvn_Dsb.replace(0,t.GtAvn_Dsb.capacity(), pcStr.substring( p , p + t.GtAvn_Dsb.capacity() ) );p = p + t.GtAvn_Dsb.capacity();
    t.GtAvn_Sqd.replace(0,t.GtAvn_Sqd.capacity(), pcStr.substring( p , p + t.GtAvn_Sqd.capacity() ) );p = p + t.GtAvn_Sqd.capacity();
    t.GtAvn_Frv.replace(0,t.GtAvn_Frv.capacity(), pcStr.substring( p , p + t.GtAvn_Frv.capacity() ) );p = p + t.GtAvn_Frv.capacity();
    t.GtAvn_Vur.replace(0,t.GtAvn_Vur.capacity(), pcStr.substring( p , p + t.GtAvn_Vur.capacity() ) );p = p + t.GtAvn_Vur.capacity();
    t.GtAvn_Vtt.replace(0,t.GtAvn_Vtt.capacity(), pcStr.substring( p , p + t.GtAvn_Vtt.capacity() ) );p = p + t.GtAvn_Vtt.capacity();
    t.GtAvn_Vts.replace(0,t.GtAvn_Vts.capacity(), pcStr.substring( p , p + t.GtAvn_Vts.capacity() ) );p = p + t.GtAvn_Vts.capacity();
    t.GtAvn_Vcn.replace(0,t.GtAvn_Vcn.capacity(), pcStr.substring( p , p + t.GtAvn_Vcn.capacity() ) );p = p + t.GtAvn_Vcn.capacity();
    t.GtAvn_Vlq.replace(0,t.GtAvn_Vlq.capacity(), pcStr.substring( p , p + t.GtAvn_Vlq.capacity() ) );p = p + t.GtAvn_Vlq.capacity();
    t.GtAvn_Vrt.replace(0,t.GtAvn_Vrt.capacity(), pcStr.substring( p , p + t.GtAvn_Vrt.capacity() ) );p = p + t.GtAvn_Vrt.capacity();
    t.GtAvn_Fvt.replace(0,t.GtAvn_Fvt.capacity(), pcStr.substring( p , p + t.GtAvn_Fvt.capacity() ) );p = p + t.GtAvn_Fvt.capacity();
    t.GtAvn_Ctr.replace(0,t.GtAvn_Ctr.capacity(), pcStr.substring( p , p + t.GtAvn_Ctr.capacity() ) );p = p + t.GtAvn_Ctr.capacity();
    t.GtAvn_Fin.replace(0,t.GtAvn_Fin.capacity(), pcStr.substring( p , p + t.GtAvn_Fin.capacity() ) );p = p + t.GtAvn_Fin.capacity();
    t.GtAvn_Fte.replace(0,t.GtAvn_Fte.capacity(), pcStr.substring( p , p + t.GtAvn_Fte.capacity() ) );p = p + t.GtAvn_Fte.capacity();
    t.GtAvn_Ped.replace(0,t.GtAvn_Ped.capacity(), pcStr.substring( p , p + t.GtAvn_Ped.capacity() ) );p = p + t.GtAvn_Ped.capacity();
    t.GtAvn_Fed.replace(0,t.GtAvn_Fed.capacity(), pcStr.substring( p , p + t.GtAvn_Fed.capacity() ) );p = p + t.GtAvn_Fed.capacity();
    t.GtAvn_Spc.replace(0,t.GtAvn_Spc.capacity(), pcStr.substring( p , p + t.GtAvn_Spc.capacity() ) );p = p + t.GtAvn_Spc.capacity();
    t.GtAvn_Sps.replace(0,t.GtAvn_Sps.capacity(), pcStr.substring( p , p + t.GtAvn_Sps.capacity() ) );p = p + t.GtAvn_Sps.capacity();
    t.GtAvn_Rem.replace(0,t.GtAvn_Rem.capacity(), pcStr.substring( p , p + t.GtAvn_Rem.capacity() ) );p = p + t.GtAvn_Rem.capacity();
    t.GtAvn_Vsg.replace(0,t.GtAvn_Vsg.capacity(), pcStr.substring( p , p + t.GtAvn_Vsg.capacity() ) );p = p + t.GtAvn_Vsg.capacity();
    t.GtAvn_Dpy.replace(0,t.GtAvn_Dpy.capacity(), pcStr.substring( p , p + t.GtAvn_Dpy.capacity() ) );p = p + t.GtAvn_Dpy.capacity();
    t.GtAvn_Occ.replace(0,t.GtAvn_Occ.capacity(), pcStr.substring( p , p + t.GtAvn_Occ.capacity() ) );p = p + t.GtAvn_Occ.capacity();
    t.GtAvn_Opp.replace(0,t.GtAvn_Opp.capacity(), pcStr.substring( p , p + t.GtAvn_Opp.capacity() ) );p = p + t.GtAvn_Opp.capacity();
    t.GtAvn_Pct.replace(0,t.GtAvn_Pct.capacity(), pcStr.substring( p , p + t.GtAvn_Pct.capacity() ) );p = p + t.GtAvn_Pct.capacity();
    t.GtAvn_Fll.replace(0,t.GtAvn_Fll.capacity(), pcStr.substring( p , p + t.GtAvn_Fll.capacity() ) );p = p + t.GtAvn_Fll.capacity();
    t.GtAvn_Iro.replace(0,t.GtAvn_Iro.capacity(), pcStr.substring( p , p + t.GtAvn_Iro.capacity() ) );p = p + t.GtAvn_Iro.capacity();
    vec.add ( t );
    return vec;
  }
  //---------------------------------------------------------------------------------------
  public String LSet_De_ImgGtAvn(Buf_Img_GtAvn p_Img)
  {
    String pcStr = "";
    pcStr = pcStr + p_Img.getSeq();
    pcStr = pcStr + p_Img.getItb();
    pcStr = pcStr + p_Img.getItg();
    pcStr = pcStr + p_Img.getDpc();
    pcStr = pcStr + p_Img.getDsb();
    pcStr = pcStr + p_Img.getSqd();
    pcStr = pcStr + p_Img.getFrv();
    pcStr = pcStr + p_Img.getVur();
    pcStr = pcStr + p_Img.getVtt();
    pcStr = pcStr + p_Img.getVts();
    pcStr = pcStr + p_Img.getVcn();
    pcStr = pcStr + p_Img.getVlq();
    pcStr = pcStr + p_Img.getVrt();
    pcStr = pcStr + p_Img.getFvt();
    pcStr = pcStr + p_Img.getCtr();
    pcStr = pcStr + p_Img.getFin();
    pcStr = pcStr + p_Img.getFte();
    pcStr = pcStr + p_Img.getPed();
    pcStr = pcStr + p_Img.getFed();
    pcStr = pcStr + p_Img.getSpc();
    pcStr = pcStr + p_Img.getSps();
    pcStr = pcStr + p_Img.getRem();
    pcStr = pcStr + p_Img.getVsg();
    pcStr = pcStr + p_Img.getDpy();
    pcStr = pcStr + p_Img.getOcc();
    pcStr = pcStr + p_Img.getOpp();
    pcStr = pcStr + p_Img.getPct();
    pcStr = pcStr + p_Img.getFll();
    pcStr = pcStr + p_Img.getIro();
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
  public static class Buf_vEao
  {
    public StringBuffer vEao_Tit = new StringBuffer(25 );   //X(25)          Titulo
    public StringBuffer vEao_Ppv = new StringBuffer(9  );   //9(07)V9(2)     VPresupuesto
    public StringBuffer vEao_Oep = new StringBuffer(5  );   //9(03)V9(2)     %AvFecha
    public StringBuffer vEao_Oev = new StringBuffer(9  );   //9(07)V9(2)     VAvFecha
    public StringBuffer vEao_Eap = new StringBuffer(5  );   //9(03)V9(2)     %Anterior
    public StringBuffer vEao_Eav = new StringBuffer(9  );   //9(07)V9(2)     VAnterior
    public StringBuffer vEao_Epp = new StringBuffer(5  );   //9(03)V9(2)     %EPago
    public StringBuffer vEao_Epv = new StringBuffer(9  );   //9(07)V9(2)     VEPago

    public Formateo f = new Formateo();
    public String getTit()        {return vEao_Tit.toString();}
    public String getPpv()        {return vEao_Ppv.toString();}
    public String getOep()        {return vEao_Oep.toString();}
    public String fgetPpv()       {return f.Formato(vEao_Ppv.toString(),"99.999.999.999",2,2,',');}
    public String fgetOep()       {return f.Formato(vEao_Oep.toString(),"999",2,2,',');}
    public String fgetOev()       {return f.Formato(vEao_Oev.toString(),"99.999.999.999",2,2,',');}
    public String fgetEap()       {return f.Formato(vEao_Eap.toString(),"999",2,2,',');}
    public String fgetEav()       {return f.Formato(vEao_Eav.toString(),"99.999.999.999",2,2,',');}
    public String fgetEpp()       {return f.Formato(vEao_Epp.toString(),"999",2,2,',');}
    public String fgetEpv()       {return f.Formato(vEao_Epv.toString(),"99.999.999.999",2,2,',');}
    public int    igetEap()       {return Integer.parseInt(vEao_Eap.toString()); }
  }
  //---------------------------------------------------------------------------------------
  public static class Buf_Img_GtEao
  {
    public StringBuffer GtEao_Seq = new StringBuffer(3  );  //9(03)        3 Secuencia
    public StringBuffer GtEao_Sqd = new StringBuffer(3  );  //9(03)        6 Secuencia
    public Vector      vGtEao_Ppv = new Vector(15);         //9(07)V9(2) 141 135 Valor Presupuesto
    public Vector      vGtEao_Oep = new Vector(15);         //9(03)V9(2) 216  75 % Avance Fecha
    public Vector      vGtEao_Oev = new Vector(15);         //9(07)V9(2) 351 135 Valor Avance Fecha
    public Vector      vGtEao_Eap = new Vector(15);         //9(03)V9(2) 426  75 % Estado Anterior
    public Vector      vGtEao_Eav = new Vector(15);         //9(07)V9(2) 561 135 Valor Estado Anterior
    public Vector      vGtEao_Epp = new Vector(15);         //9(03)V9(2) 636  75 % EPago Presente
    public Vector      vGtEao_Epv = new Vector(15);         //9(07)V9(2) 771 135 Valor EPago Presente
    public StringBuffer GtEao_Fll = new StringBuffer(56 );  //X(56)      827 No Usado

    public StringBuffer GtEao_Ppv = new StringBuffer(9  );  //9(07)V9(2)     VPresupuesto
    public StringBuffer GtEao_Oep = new StringBuffer(5  );  //9(03)V9(2)     %AvFecha
    public StringBuffer GtEao_Oev = new StringBuffer(9  );  //9(07)V9(2)     VAvFecha
    public StringBuffer GtEao_Eap = new StringBuffer(5  );  //9(03)V9(2)     %Anterior
    public StringBuffer GtEao_Eav = new StringBuffer(9  );  //9(07)V9(2)     VAnterior
    public StringBuffer GtEao_Epp = new StringBuffer(5  );  //9(03)V9(2)     %EPago
    public StringBuffer GtEao_Epv = new StringBuffer(9  );  //9(07)V9(2)     VEPago

    public String getSeq()         {return GtEao_Seq.toString();}
    public String getSqd()         {return GtEao_Sqd.toString();}
    public Vector getVecPpv()      {return vGtEao_Ppv;}
    public Vector getVecOep()      {return vGtEao_Oep;}
    public Vector getVecOev()      {return vGtEao_Oev;}
    public Vector getVecEap()      {return vGtEao_Eap;}
    public Vector getVecEav()      {return vGtEao_Eav;}
    public Vector getVecEpp()      {return vGtEao_Epp;}
    public Vector getVecEpv()      {return vGtEao_Epv;}
    public String getFll()         {return GtEao_Fll.toString();}

    public Formateo f = new Formateo();
  //public String fgetCli()        {return f.fmtRut(GtEao_Cli.toString());}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_Img_GtEao LSet_A_ImgGtEao(String pcStr)
  {
    Buf_Img_GtEao l_Img = new Buf_Img_GtEao();
    Vector vDatos = LSet_A_vImgGtEao(pcStr);
    l_Img = (Buf_Img_GtEao)vDatos.elementAt(0);
    return l_Img;
  }
  //---------------------------------------------------------------------------------------
  public static Vector LSet_A_vImgGtEao(String pcStr)
  {
    int p=0;
    Vector vec= new Vector();
    Buf_Img_GtEao t = new Buf_Img_GtEao();
    t.GtEao_Seq.replace(0,t.GtEao_Seq.capacity(), pcStr.substring( p , p + t.GtEao_Seq.capacity() ) );p = p + t.GtEao_Seq.capacity();
    t.GtEao_Sqd.replace(0,t.GtEao_Sqd.capacity(), pcStr.substring( p , p + t.GtEao_Sqd.capacity() ) );p = p + t.GtEao_Sqd.capacity();
    String lcData="";
    for (int i=0;i<15;i++)
        {
          lcData = pcStr.substring( p , p + t.GtEao_Ppv.capacity() ) ;
          t.vGtEao_Ppv.add ( lcData );
          p = p + t.GtEao_Ppv.capacity();
        }
    for (int i=0;i<15;i++)
        {
          lcData = pcStr.substring( p , p + t.GtEao_Oep.capacity() ) ;
          t.vGtEao_Oep.add ( lcData );
          p = p + t.GtEao_Oep.capacity();
        }
    for (int i=0;i<15;i++)
        {
          lcData = pcStr.substring( p , p + t.GtEao_Oev.capacity() ) ;
          t.vGtEao_Oev.add ( lcData );
          p = p + t.GtEao_Oev.capacity();
        }
    for (int i=0;i<15;i++)
        {
          lcData = pcStr.substring( p , p + t.GtEao_Eap.capacity() ) ;
          t.vGtEao_Eap.add ( lcData );
          p = p + t.GtEao_Eap.capacity();
        }
    for (int i=0;i<15;i++)
        {
          lcData = pcStr.substring( p , p + t.GtEao_Eav.capacity() ) ;
          t.vGtEao_Eav.add ( lcData );
          p = p + t.GtEao_Eav.capacity();
        }
    for (int i=0;i<15;i++)
        {
          lcData = pcStr.substring( p , p + t.GtEao_Epp.capacity() ) ;
          t.vGtEao_Epp.add ( lcData );
          p = p + t.GtEao_Epp.capacity();
        }
    for (int i=0;i<15;i++)
        {
          lcData = pcStr.substring( p , p + t.GtEao_Epv.capacity() ) ;
          t.vGtEao_Epv.add ( lcData );
          p = p + t.GtEao_Epv.capacity();
        }
    t.GtEao_Fll.replace(0,t.GtEao_Fll.capacity(), pcStr.substring( p , p + t.GtEao_Fll.capacity() ) );p = p + t.GtEao_Fll.capacity();
    vec.add ( t );
    return vec;
  }
  //---------------------------------------------------------------------------------------
  public String LSet_De_ImgGtEao(Buf_Img_GtEao p_Img)
  {
    String pcStr = "";
    pcStr = pcStr + p_Img.getSeq();
    pcStr = pcStr + p_Img.getSqd();
    String lcData ="";
    for (int i=0;i<15;i++)
        {
          lcData = ((Vector)p_Img.getVecPpv()).elementAt(i).toString();
          pcStr = pcStr + lcData;
        }
    for (int i=0;i<15;i++)
        {
          lcData = ((Vector)p_Img.getVecOep()).elementAt(i).toString();
          pcStr = pcStr + lcData;
        }
    for (int i=0;i<15;i++)
        {
          lcData = ((Vector)p_Img.getVecOev()).elementAt(i).toString();
          pcStr = pcStr + lcData;
        }
    for (int i=0;i<15;i++)
        {
          lcData = ((Vector)p_Img.getVecEap()).elementAt(i).toString();
          pcStr = pcStr + lcData;
        }
    for (int i=0;i<15;i++)
        {
          lcData = ((Vector)p_Img.getVecEav()).elementAt(i).toString();
          pcStr = pcStr + lcData;
        }
    for (int i=0;i<15;i++)
        {
          lcData = ((Vector)p_Img.getVecEpp()).elementAt(i).toString();
          pcStr = pcStr + lcData;
        }
    for (int i=0;i<15;i++)
        {
          lcData = ((Vector)p_Img.getVecEpv()).elementAt(i).toString();
          pcStr = pcStr + lcData;
        }
    pcStr = pcStr + p_Img.getFll();
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
  public static class Buf_Img_GtFia
  {
    public StringBuffer GtFia_Cli = new StringBuffer(10 );  //X(10)       10 RUT Fiador
    public StringBuffer GtFia_Ncl = new StringBuffer(40 );  //X(40)       50 Nombre Fiador
    public StringBuffer GtFia_Rcy = new StringBuffer(10 );  //X(10)       60 RUT Conyuge
    public StringBuffer GtFia_Ncy = new StringBuffer(40 );  //X(40)      100 Nombre Conyuge
    public StringBuffer GtFia_Fll = new StringBuffer(727);  //X(727)     827

    public String getCli()         {return GtFia_Cli.toString();}
    public String getNcl()         {return GtFia_Ncl.toString();}
    public String getRcy()         {return GtFia_Rcy.toString();}
    public String getNcy()         {return GtFia_Ncy.toString();}
    public String getFll()         {return GtFia_Fll.toString();}

    public Formateo f = new Formateo();
    public String fgetCli()        {return f.fmtRut(GtFia_Cli.toString());}
    public String fgetRcy()        {return f.fmtRut(GtFia_Rcy.toString());}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_Img_GtFia LSet_A_ImgGtFia(String pcStr)
  {
    Buf_Img_GtFia l_Img = new Buf_Img_GtFia();
    Vector vDatos = LSet_A_vImgGtFia(pcStr);
    l_Img = (Buf_Img_GtFia)vDatos.elementAt(0);
    return l_Img;
  }
  //---------------------------------------------------------------------------------------
  public static Vector LSet_A_vImgGtFia(String pcStr)
  {
    int p=0;
    Vector vec= new Vector();
    Buf_Img_GtFia t = new Buf_Img_GtFia();
    t.GtFia_Cli.replace(0,t.GtFia_Cli.capacity(), pcStr.substring( p , p + t.GtFia_Cli.capacity() ) );p = p + t.GtFia_Cli.capacity();
    t.GtFia_Ncl.replace(0,t.GtFia_Ncl.capacity(), pcStr.substring( p , p + t.GtFia_Ncl.capacity() ) );p = p + t.GtFia_Ncl.capacity();
    t.GtFia_Rcy.replace(0,t.GtFia_Rcy.capacity(), pcStr.substring( p , p + t.GtFia_Rcy.capacity() ) );p = p + t.GtFia_Rcy.capacity();
    t.GtFia_Ncy.replace(0,t.GtFia_Ncy.capacity(), pcStr.substring( p , p + t.GtFia_Ncy.capacity() ) );p = p + t.GtFia_Ncy.capacity();
    t.GtFia_Fll.replace(0,t.GtFia_Fll.capacity(), pcStr.substring(p) );
    vec.add ( t );
    return vec;
  }
  //---------------------------------------------------------------------------------------
  public String LSet_De_ImgGtFia(Buf_Img_GtFia p_Img)
  {
    String pcStr = "";
    pcStr = pcStr + p_Img.getCli();
    pcStr = pcStr + p_Img.getNcl();
    pcStr = pcStr + p_Img.getRcy();
    pcStr = pcStr + p_Img.getNcy();
    pcStr = pcStr + p_Img.getFll();
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
  public static class Buf_Img_GtWar
  {
    public StringBuffer GtWar_Wdm = new StringBuffer(30 );  //X(30)        30 Desc.Mercaderia
    public StringBuffer GtWar_Wum = new StringBuffer(5  );  //X(05)        35 UMedida
    public StringBuffer GtWar_Wcn = new StringBuffer(9  );  //9(07)V9(2)   44 Cantidad
    public StringBuffer GtWar_Wvl = new StringBuffer(15 );  //9(13)V9(2)   59 Valor Garantia
    public StringBuffer GtWar_Wcm = new StringBuffer(15 );  //X(15)        74 Codigo Mercaderia
    public StringBuffer GtWar_Wpu = new StringBuffer(15 );  //9(13)V9(2)   89 Precio Unitario
    public StringBuffer GtWar_Wvp = new StringBuffer(15 );  //9(13)V9(2)  104 Valor Vale Prenda
    public StringBuffer GtWar_Wmk = new StringBuffer(15 );  //X(15)       119 Marca
    public StringBuffer GtWar_Wmd = new StringBuffer(15 );  //X(15)       134 Modelo
    public StringBuffer GtWar_Wan = new StringBuffer(4  );  //9(04)       138 A�o Fabricacion
    public StringBuffer GtWar_Wns = new StringBuffer(20 );  //X(20)       158 Numero Serie
    public StringBuffer GtWar_Fll = new StringBuffer(669);  //X(669)      827 Disponible

    public String getWdm()         {return GtWar_Wdm.toString();}
    public String getWum()         {return GtWar_Wum.toString();}
    public String getWcn()         {return GtWar_Wcn.toString();}
    public String getWvl()         {return GtWar_Wvl.toString();}
    public String getWcm()         {return GtWar_Wcm.toString();}
    public String getWpu()         {return GtWar_Wpu.toString();}
    public String getWvp()         {return GtWar_Wvp.toString();}
    public String getWmk()         {return GtWar_Wmk.toString();}
    public String getWmd()         {return GtWar_Wmd.toString();}
    public String getWan()         {return GtWar_Wan.toString();}
    public String getWns()         {return GtWar_Wns.toString();}
    public String getFll()         {return GtWar_Fll.toString();}

    public Formateo f = new Formateo();
    public String fgetWcn()        {return f.Formato(GtWar_Wcn.toString(),"99.999.999.999",2,2,',');}
    public String fgetWvl()        {return f.Formato(GtWar_Wvl.toString(),"99.999.999.999",2,0,',');}
    public String fgetWpu()        {return f.Formato(GtWar_Wpu.toString(),"99.999.999.999",2,0,',');}
    public String fgetWvp()        {return f.Formato(GtWar_Wvp.toString(),"99.999.999.999",2,0,',');}
    public String ggetWvl()        {return f.Formato(GtWar_Wvl.toString(),"99.999.999.999",2,2,',');}
    public String ggetWpu()        {return f.Formato(GtWar_Wpu.toString(),"99.999.999.999",2,2,',');}
    public String ggetWvp()        {return f.Formato(GtWar_Wvp.toString(),"99.999.999.999",2,2,',');}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_Img_GtWar LSet_A_ImgGtWar(String pcStr)
  {
    Buf_Img_GtWar l_Img = new Buf_Img_GtWar();
    Vector vDatos = LSet_A_vImgGtWar(pcStr);
    l_Img = (Buf_Img_GtWar)vDatos.elementAt(0);
    return l_Img;
  }
  //---------------------------------------------------------------------------------------
  public static Vector LSet_A_vImgGtWar(String pcStr)
  {
    int p=0;
    Vector vec= new Vector();
    Buf_Img_GtWar t = new Buf_Img_GtWar();
    t.GtWar_Wdm.replace(0,t.GtWar_Wdm.capacity(), pcStr.substring( p , p + t.GtWar_Wdm.capacity() ) );p = p + t.GtWar_Wdm.capacity();
    t.GtWar_Wum.replace(0,t.GtWar_Wum.capacity(), pcStr.substring( p , p + t.GtWar_Wum.capacity() ) );p = p + t.GtWar_Wum.capacity();
    t.GtWar_Wcn.replace(0,t.GtWar_Wcn.capacity(), pcStr.substring( p , p + t.GtWar_Wcn.capacity() ) );p = p + t.GtWar_Wcn.capacity();
    t.GtWar_Wvl.replace(0,t.GtWar_Wvl.capacity(), pcStr.substring( p , p + t.GtWar_Wvl.capacity() ) );p = p + t.GtWar_Wvl.capacity();
    t.GtWar_Wcm.replace(0,t.GtWar_Wcm.capacity(), pcStr.substring( p , p + t.GtWar_Wcm.capacity() ) );p = p + t.GtWar_Wcm.capacity();
    t.GtWar_Wpu.replace(0,t.GtWar_Wpu.capacity(), pcStr.substring( p , p + t.GtWar_Wpu.capacity() ) );p = p + t.GtWar_Wpu.capacity();
    t.GtWar_Wvp.replace(0,t.GtWar_Wvp.capacity(), pcStr.substring( p , p + t.GtWar_Wvp.capacity() ) );p = p + t.GtWar_Wvp.capacity();
    t.GtWar_Wmk.replace(0,t.GtWar_Wmk.capacity(), pcStr.substring( p , p + t.GtWar_Wmk.capacity() ) );p = p + t.GtWar_Wmk.capacity();
    t.GtWar_Wmd.replace(0,t.GtWar_Wmd.capacity(), pcStr.substring( p , p + t.GtWar_Wmd.capacity() ) );p = p + t.GtWar_Wmd.capacity();
    t.GtWar_Wan.replace(0,t.GtWar_Wan.capacity(), pcStr.substring( p , p + t.GtWar_Wan.capacity() ) );p = p + t.GtWar_Wan.capacity();
    t.GtWar_Wns.replace(0,t.GtWar_Wns.capacity(), pcStr.substring( p , p + t.GtWar_Wns.capacity() ) );p = p + t.GtWar_Wns.capacity();
    t.GtWar_Fll.replace(0,t.GtWar_Fll.capacity(), pcStr.substring(p) );
    vec.add(t);
    return vec;
  }
  //---------------------------------------------------------------------------------------
  public String LSet_De_ImgGtWar(Buf_Img_GtWar p_Img)
  {
    String pcStr = "";
    pcStr = pcStr + p_Img.getWdm();
    pcStr = pcStr + p_Img.getWum();
    pcStr = pcStr + p_Img.getWcn();
    pcStr = pcStr + p_Img.getWvl();
    pcStr = pcStr + p_Img.getWcm();
    pcStr = pcStr + p_Img.getWpu();
    pcStr = pcStr + p_Img.getWvp();
    pcStr = pcStr + p_Img.getWmk();
    pcStr = pcStr + p_Img.getWmd();
    pcStr = pcStr + p_Img.getWan();
    pcStr = pcStr + p_Img.getWns();
    pcStr = pcStr + p_Img.getFll();
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
  public static class Buf_Img_GtVfi
  {
    public StringBuffer GtVfi_Tdo = new StringBuffer(2  );  //9(02)        2 Tipo Documento
    public StringBuffer GtVfi_Bco = new StringBuffer(3  );  //9(03)        5 Cod.Banco Emisor
    public StringBuffer GtVfi_Ser = new StringBuffer(10 );  //X(10)       15 Serie Documento
    public StringBuffer GtVfi_Nro = new StringBuffer(7  );  //9(07)       22 Numero Documento
    public StringBuffer GtVfi_Fem = new StringBuffer(8  );  //9(08)       30 Fecha Emision
    public StringBuffer GtVfi_Vuo = new StringBuffer(9  );  //9(07)V9(2)  39 Conversion <TRJ>
    public StringBuffer GtVfi_Fvt = new StringBuffer(8  );  //9(08)       47 Fecha Vencimiento
    public StringBuffer GtVfi_Vln = new StringBuffer(15 );  //9(11)V9(4)  62 Valor Nominal <TRJ>
    public StringBuffer GtVfi_Vlc = new StringBuffer(15 );  //9(13)V9(2)  77 Valor Contable
    public StringBuffer GtVfi_Nbe = new StringBuffer(40 );  //X(40)      117 Nombre Titular
    public StringBuffer GtVfi_Ntd = new StringBuffer(14 );  //X(14)      131 Nombre TDocumento
    public StringBuffer GtVfi_Nbc = new StringBuffer(15 );  //X(15)      146 Nombre BEmisor
    public StringBuffer GtVfi_Tpv = new StringBuffer(1  );  //X(01)      147 Tipo Valor (Fin/Otro)
    public StringBuffer GtVfi_Mnd = new StringBuffer(3  );  //9(03)      150 Cod.Moneda
    public StringBuffer GtVfi_Trj = new StringBuffer(3  );  //X(03)      153 Cod.Reajustabilidad
    public StringBuffer GtVfi_Vlt = new StringBuffer(15 );  //9(13)V9(2) 168 Valor Nominal <MND>
    public StringBuffer GtVfi_Fll = new StringBuffer(659);  //X(659)     827 Disponible

    public String getTdo()         {return GtVfi_Tdo.toString();}
    public String getBco()         {return GtVfi_Bco.toString();}
    public String getSer()         {return GtVfi_Ser.toString();}
    public String getNro()         {return GtVfi_Nro.toString();}
    public String getFem()         {return GtVfi_Fem.toString();}
    public String getVuo()         {return GtVfi_Vuo.toString();}
    public String getFvt()         {return GtVfi_Fvt.toString();}
    public String getVln()         {return GtVfi_Vln.toString();}
    public String getVlc()         {return GtVfi_Vlc.toString();}
    public String getNbe()         {return GtVfi_Nbe.toString();}
    public String getNtd()         {return GtVfi_Ntd.toString();}
    public String getNbc()         {return GtVfi_Nbc.toString();}
    public String getTpv()         {return GtVfi_Tpv.toString();}
    public String getMnd()         {return GtVfi_Mnd.toString();}
    public String getTrj()         {return GtVfi_Trj.toString();}
    public String getVlt()         {return GtVfi_Vlt.toString();}
    public String getFll()         {return GtVfi_Fll.toString();}

    public Formateo f = new Formateo();
    public String fgetTdo()        {return GtVfi_Tdo.toString();}
    public String fgetBco()        {return GtVfi_Bco.toString();}
    public String fgetSer()        {return GtVfi_Ser.toString();}
    public String fgetNro()        {return GtVfi_Nro.toString();}
    public String fgetFem()        {return f.FormatDate(GtVfi_Fem.toString().trim());}
    public String fgetFvt()        {return f.FormatDate(GtVfi_Fvt.toString());}
    public String fgetMnd()        {return GtVfi_Mnd.toString();}
    public String fgetTrj()        {return GtVfi_Trj.toString();}
    public String fgetVuo()        {return f.Formato(GtVfi_Vuo.toString(),"9.999.999",2,2,',');}
    public String fgetVln()        {return f.Formato(GtVfi_Vln.toString(),"99.999.999.999",4,4,',');}
    public String fgetVlt()        {return f.Formato(GtVfi_Vlt.toString(),"99.999.999.999",2,2,',');}
    public String fgetVlc()        {return f.Formato(GtVfi_Vlc.toString(),"99.999.999.999",2,2,',');}
    public String fgetNbe()        {return GtVfi_Nbe.toString();}
    public String fgetNtd()        {return GtVfi_Ntd.toString();}
    public String fgetNbc()        {return GtVfi_Nbc.toString();}
    public String fgetTpv()        {return GtVfi_Tpv.toString();}
    public String f0getVln()       {return f.Formato(GtVfi_Vln.toString(),"99.999.999.999",4,0,',');}
    public String f2getVln()       {return f.Formato(GtVfi_Vln.toString(),"99.999.999.999",4,2,',');}
    public String f4getVln()       {return f.Formato(GtVfi_Vln.toString(),"99.999.999.999",4,4,',');}
    public String f0getVlt()       {return f.Formato(GtVfi_Vlt.toString(),"99.999.999.999",2,0,',');}
    public String f2getVlt()       {return f.Formato(GtVfi_Vlt.toString(),"99.999.999.999",2,2,',');}
    public String f0getVlc()       {return f.Formato(GtVfi_Vlc.toString(),"99.999.999.999",2,0,',');}
    public String f2getVlc()       {return f.Formato(GtVfi_Vlc.toString(),"99.999.999.999",2,2,',');}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_Img_GtVfi LSet_A_ImgGtVfi(String pcStr)
  {
    Buf_Img_GtVfi l_Img = new Buf_Img_GtVfi();
    Vector vDatos = LSet_A_vImgGtVfi(pcStr);
    l_Img = (Buf_Img_GtVfi)vDatos.elementAt(0);
    return l_Img;
  }
  //---------------------------------------------------------------------------------------
  public static Vector LSet_A_vImgGtVfi(String pcStr)
  {
    int p=0;
    Vector vec= new Vector();
    Buf_Img_GtVfi t = new Buf_Img_GtVfi();
    t.GtVfi_Tdo.replace(0,t.GtVfi_Tdo.capacity(), pcStr.substring( p , p + t.GtVfi_Tdo.capacity() ) );p = p + t.GtVfi_Tdo.capacity();
    t.GtVfi_Bco.replace(0,t.GtVfi_Bco.capacity(), pcStr.substring( p , p + t.GtVfi_Bco.capacity() ) );p = p + t.GtVfi_Bco.capacity();
    t.GtVfi_Ser.replace(0,t.GtVfi_Ser.capacity(), pcStr.substring( p , p + t.GtVfi_Ser.capacity() ) );p = p + t.GtVfi_Ser.capacity();
    t.GtVfi_Nro.replace(0,t.GtVfi_Nro.capacity(), pcStr.substring( p , p + t.GtVfi_Nro.capacity() ) );p = p + t.GtVfi_Nro.capacity();
    t.GtVfi_Fem.replace(0,t.GtVfi_Fem.capacity(), pcStr.substring( p , p + t.GtVfi_Fem.capacity() ) );p = p + t.GtVfi_Fem.capacity();
    t.GtVfi_Vuo.replace(0,t.GtVfi_Vuo.capacity(), pcStr.substring( p , p + t.GtVfi_Vuo.capacity() ) );p = p + t.GtVfi_Vuo.capacity();
    t.GtVfi_Fvt.replace(0,t.GtVfi_Fvt.capacity(), pcStr.substring( p , p + t.GtVfi_Fvt.capacity() ) );p = p + t.GtVfi_Fvt.capacity();
    t.GtVfi_Vln.replace(0,t.GtVfi_Vln.capacity(), pcStr.substring( p , p + t.GtVfi_Vln.capacity() ) );p = p + t.GtVfi_Vln.capacity();
    t.GtVfi_Vlc.replace(0,t.GtVfi_Vlc.capacity(), pcStr.substring( p , p + t.GtVfi_Vlc.capacity() ) );p = p + t.GtVfi_Vlc.capacity();
    t.GtVfi_Nbe.replace(0,t.GtVfi_Nbe.capacity(), pcStr.substring( p , p + t.GtVfi_Nbe.capacity() ) );p = p + t.GtVfi_Nbe.capacity();
    t.GtVfi_Ntd.replace(0,t.GtVfi_Ntd.capacity(), pcStr.substring( p , p + t.GtVfi_Ntd.capacity() ) );p = p + t.GtVfi_Ntd.capacity();
    t.GtVfi_Nbc.replace(0,t.GtVfi_Nbc.capacity(), pcStr.substring( p , p + t.GtVfi_Nbc.capacity() ) );p = p + t.GtVfi_Nbc.capacity();
    t.GtVfi_Tpv.replace(0,t.GtVfi_Tpv.capacity(), pcStr.substring( p , p + t.GtVfi_Tpv.capacity() ) );p = p + t.GtVfi_Tpv.capacity();
    t.GtVfi_Mnd.replace(0,t.GtVfi_Mnd.capacity(), pcStr.substring( p , p + t.GtVfi_Mnd.capacity() ) );p = p + t.GtVfi_Mnd.capacity();
    t.GtVfi_Trj.replace(0,t.GtVfi_Trj.capacity(), pcStr.substring( p , p + t.GtVfi_Trj.capacity() ) );p = p + t.GtVfi_Trj.capacity();
    t.GtVfi_Vlt.replace(0,t.GtVfi_Vlt.capacity(), pcStr.substring( p , p + t.GtVfi_Vlt.capacity() ) );p = p + t.GtVfi_Vlt.capacity();
    t.GtVfi_Fll.replace(0,t.GtVfi_Fll.capacity(), pcStr.substring(p) );
    vec.add ( t );
    return vec;
  }
  //---------------------------------------------------------------------------------------
  public String LSet_De_ImgGtVfi(Buf_Img_GtVfi p_Img)
  {
    String pcStr = "";
    pcStr = pcStr + p_Img.getTdo();
    pcStr = pcStr + p_Img.getBco();
    pcStr = pcStr + p_Img.getSer();
    pcStr = pcStr + p_Img.getNro();
    pcStr = pcStr + p_Img.getFem();
    pcStr = pcStr + p_Img.getVuo();
    pcStr = pcStr + p_Img.getFvt();
    pcStr = pcStr + p_Img.getVln();
    pcStr = pcStr + p_Img.getVlc();
    pcStr = pcStr + p_Img.getNbe();
    pcStr = pcStr + p_Img.getNtd();
    pcStr = pcStr + p_Img.getNbc();
    pcStr = pcStr + p_Img.getTpv();
    pcStr = pcStr + p_Img.getMnd();
    pcStr = pcStr + p_Img.getTrj();
    pcStr = pcStr + p_Img.getVlt();
    pcStr = pcStr + p_Img.getFll();
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
  public static class Buf_Img_GtAcc
  {
    public StringBuffer GtAcc_Gta = new StringBuffer(14 );  //X(14)       14 Codigo Accion
    public StringBuffer GtAcc_Nse = new StringBuffer(40 );  //X(40)       54 Nombre SAEmisora
    public StringBuffer GtAcc_Mnd = new StringBuffer(3  );  //9(03)       57 Moneda Accion
    public StringBuffer GtAcc_Fut = new StringBuffer(8  );  //9(08)       65 FUVBolsa
    public StringBuffer GtAcc_Uvb = new StringBuffer(15 );  //9(13)V9(2)  80 UVBolsa
    public StringBuffer GtAcc_Can = new StringBuffer(12 );  //9(12)       92 Cantidad Acciones
    public StringBuffer GtAcc_Vlc = new StringBuffer(15 );  //9(13)V9(2) 107 VContable
    public StringBuffer GtAcc_Nbe = new StringBuffer(40 );  //X(40)      147 Nombre Titular
    public StringBuffer GtAcc_Fll = new StringBuffer(680);  //X(680)     827

    public String getGta()         {return GtAcc_Gta.toString();}
    public String getNse()         {return GtAcc_Nse.toString();}
    public String getMnd()         {return GtAcc_Mnd.toString();}
    public String getFut()         {return GtAcc_Fut.toString();}
    public String getUvb()         {return GtAcc_Uvb.toString();}
    public String getCan()         {return GtAcc_Can.toString();}
    public String getVlc()         {return GtAcc_Vlc.toString();}
    public String getNbe()         {return GtAcc_Nbe.toString();}
    public String getFll()         {return GtAcc_Fll.toString();}

    public Formateo f = new Formateo();
    public String fgetGta()        {return GtAcc_Gta.toString();}
    public String fgetNse()        {return GtAcc_Nse.toString();}
    public String fgetMnd()        {return GtAcc_Mnd.toString();}
    public String fgetFut()        {return f.FormatDate(GtAcc_Fut.toString());}
    public String fgetUvb()        {return f.Formato(GtAcc_Uvb.toString(),"9.999.999.999.999",2,2,',');}
    public String fgetCan()        {return f.Formato(GtAcc_Can.toString(),"999.999.999.999",0,0,',');}
    public String fgetVlc()        {return f.Formato(GtAcc_Vlc.toString(),"9.999.999.999.999",2,2,',');}
    public String fgetNbe()        {return GtAcc_Nbe.toString();}

    public String f0getVlc()       {return f.Formato(GtAcc_Vlc.toString(),"9.999.999.999.999",2,0,',');}
    public String f2getVlc()       {return f.Formato(GtAcc_Vlc.toString(),"9.999.999.999.999",2,2,',');}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_Img_GtAcc LSet_A_ImgGtAcc(String pcStr)
  {
    Buf_Img_GtAcc l_Img = new Buf_Img_GtAcc();
    Vector vDatos = LSet_A_vImgGtAcc(pcStr);
    l_Img = (Buf_Img_GtAcc)vDatos.elementAt(0);
    return l_Img;
  }
  //---------------------------------------------------------------------------------------
  public static Vector LSet_A_vImgGtAcc(String pcStr)
  {
    int p=0;
    Vector vec= new Vector();
    Buf_Img_GtAcc t = new Buf_Img_GtAcc();
    t.GtAcc_Gta.replace(0,t.GtAcc_Gta.capacity(), pcStr.substring( p , p + t.GtAcc_Gta.capacity() ) );p = p + t.GtAcc_Gta.capacity();
    t.GtAcc_Nse.replace(0,t.GtAcc_Nse.capacity(), pcStr.substring( p , p + t.GtAcc_Nse.capacity() ) );p = p + t.GtAcc_Nse.capacity();
    t.GtAcc_Mnd.replace(0,t.GtAcc_Mnd.capacity(), pcStr.substring( p , p + t.GtAcc_Mnd.capacity() ) );p = p + t.GtAcc_Mnd.capacity();
    t.GtAcc_Fut.replace(0,t.GtAcc_Fut.capacity(), pcStr.substring( p , p + t.GtAcc_Fut.capacity() ) );p = p + t.GtAcc_Fut.capacity();
    t.GtAcc_Uvb.replace(0,t.GtAcc_Uvb.capacity(), pcStr.substring( p , p + t.GtAcc_Uvb.capacity() ) );p = p + t.GtAcc_Uvb.capacity();
    t.GtAcc_Can.replace(0,t.GtAcc_Can.capacity(), pcStr.substring( p , p + t.GtAcc_Can.capacity() ) );p = p + t.GtAcc_Can.capacity();
    t.GtAcc_Vlc.replace(0,t.GtAcc_Vlc.capacity(), pcStr.substring( p , p + t.GtAcc_Vlc.capacity() ) );p = p + t.GtAcc_Vlc.capacity();
    t.GtAcc_Nbe.replace(0,t.GtAcc_Nbe.capacity(), pcStr.substring( p , p + t.GtAcc_Nbe.capacity() ) );p = p + t.GtAcc_Nbe.capacity();
    t.GtAcc_Fll.replace(0,t.GtAcc_Fll.capacity(), pcStr.substring( p , p + t.GtAcc_Fll.capacity() ) );p = p + t.GtAcc_Fll.capacity();
    vec.add ( t );
    return vec;
  }
  //---------------------------------------------------------------------------------------
  public String LSet_De_ImgGtAcc(Buf_Img_GtAcc p_Img)
  {
    String pcStr = "";
    pcStr = pcStr + p_Img.getGta();
    pcStr = pcStr + p_Img.getNse();
    pcStr = pcStr + p_Img.getMnd();
    pcStr = pcStr + p_Img.getFut();
    pcStr = pcStr + p_Img.getUvb();
    pcStr = pcStr + p_Img.getCan();
    pcStr = pcStr + p_Img.getVlc();
    pcStr = pcStr + p_Img.getNbe();
    pcStr = pcStr + p_Img.getFll();
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
  public static class Buf_Img_GtIfr
  {
    public StringBuffer GtIfr_Gta = new StringBuffer(36 );  //X(36)       36 Codigo Instrumento
    public StringBuffer GtIfr_Tpa = new StringBuffer(1  );  //X(01)       37 Moneda Accion
    public StringBuffer GtIfr_Nse = new StringBuffer(40 );  //X(40)       77 Nombre SAEmisora
    public StringBuffer GtIfr_Mnd = new StringBuffer(3  );  //9(03)       80 Moneda Accion
    public StringBuffer GtIfr_Fut = new StringBuffer(8  );  //9(08)       88 FUVBolsa
    public StringBuffer GtIfr_Uvb = new StringBuffer(15 );  //9(13)V9(2) 103 UVBolsa
    public StringBuffer GtIfr_Can = new StringBuffer(15 );  //9(13)V9(2) 118 Cantidad
    public StringBuffer GtIfr_Vlc = new StringBuffer(15 );  //9(13)V9(2) 133 VContable
    public StringBuffer GtIfr_Nbe = new StringBuffer(40 );  //X(40)      173 Nombre Titular
    public StringBuffer GtIfr_Ben = new StringBuffer(40 );  //X(40)      213 Nombre Beneficiario
    public StringBuffer GtIfr_Fvc = new StringBuffer(8  );  //9(08)      221 FVencto DepBBice
    public StringBuffer GtIfr_Fll = new StringBuffer(606);  //X(606)     827

    public String getGta()         {return GtIfr_Gta.toString();}
    public String getTpa()         {return GtIfr_Tpa.toString();}
    public String getNse()         {return GtIfr_Nse.toString();}
    public String getMnd()         {return GtIfr_Mnd.toString();}
    public String getFut()         {return GtIfr_Fut.toString();}
    public String getUvb()         {return GtIfr_Uvb.toString();}
    public String getCan()         {return GtIfr_Can.toString();}
    public String getVlc()         {return GtIfr_Vlc.toString();}
    public String getNbe()         {return GtIfr_Nbe.toString();}
    public String getBen()         {return GtIfr_Ben.toString();}
    public String getFvc()         {return GtIfr_Fvc.toString();}
    public String getFll()         {return GtIfr_Fll.toString();}

    public Formateo f = new Formateo();
    public String fgetGta()      {
                                  if (getTpa().trim().equals("A"))
                                     { return getGta().substring(0,10)+" - "+getGta().substring(10,12)+" - "+getGta().substring(12,14); }
                                  else if (getTpa().trim().equals("I"))
                                     {
                                       String wMto = f.Formato(getGta().substring(15,30),"99.999.999.999",2,2,',');
                                       return getGta().substring(0,12)+" - "+getGta().substring(12,15)+" - "+wMto;
                                     }
                                  else if (getTpa().trim().equals("B"))
                                     { return getGta().substring(0,2)+" - "+getGta().substring(2,5)+" - "+getGta().substring(5,14); }
                                  else if (getTpa().trim().equals("M"))
                                     { return getGta().substring(0,2)+" - "+getGta().substring(2,4); }
                                  else
                                     { return ""; }
                                 }
    public String fgetNse()        {return GtIfr_Nse.toString();}
    public String fgetMnd()        {return GtIfr_Mnd.toString();}
    public String fgetFut()        {return f.FormatDate(GtIfr_Fut.toString());}
    public String fgetFvc()        {return f.FormatDate(GtIfr_Fvc.toString());}
    public String fgetUvb()        {return f.Formato(GtIfr_Uvb.toString(),"9.999.999.999.999",2,2,',');}
    public String fgetCan()        {return f.Formato(GtIfr_Can.toString(),"9.999.999.999.999",2,2,',');}
    public String fgetVlc()        {return f.Formato(GtIfr_Vlc.toString(),"9.999.999.999.999",2,2,',');}
    public String fgetNbe()        {return GtIfr_Nbe.toString();}
    public String fgetBen()        {return GtIfr_Ben.toString();}

    public String f0getCan()       {return f.Formato(GtIfr_Can.toString(),"9.999.999.999.999",2,0,',');}
    public String f2getCan()       {return f.Formato(GtIfr_Can.toString(),"9.999.999.999.999",2,2,',');}
    public String f0getVlc()       {return f.Formato(GtIfr_Vlc.toString(),"9.999.999.999.999",2,0,',');}
    public String f2getVlc()       {return f.Formato(GtIfr_Vlc.toString(),"9.999.999.999.999",2,2,',');}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_Img_GtIfr LSet_A_ImgGtIfr(String pcStr)
  {
    Buf_Img_GtIfr l_Img = new Buf_Img_GtIfr();
    Vector vDatos = LSet_A_vImgGtIfr(pcStr);
    l_Img = (Buf_Img_GtIfr)vDatos.elementAt(0);
    return l_Img;
  }
  //---------------------------------------------------------------------------------------
  public static Vector LSet_A_vImgGtIfr(String pcStr)
  {
    int p=0;
    Vector vec= new Vector();
    Buf_Img_GtIfr t = new Buf_Img_GtIfr();
    t.GtIfr_Gta.replace(0,t.GtIfr_Gta.capacity(), pcStr.substring( p , p + t.GtIfr_Gta.capacity() ) );p = p + t.GtIfr_Gta.capacity();
    t.GtIfr_Tpa.replace(0,t.GtIfr_Tpa.capacity(), pcStr.substring( p , p + t.GtIfr_Tpa.capacity() ) );p = p + t.GtIfr_Tpa.capacity();
    t.GtIfr_Nse.replace(0,t.GtIfr_Nse.capacity(), pcStr.substring( p , p + t.GtIfr_Nse.capacity() ) );p = p + t.GtIfr_Nse.capacity();
    t.GtIfr_Mnd.replace(0,t.GtIfr_Mnd.capacity(), pcStr.substring( p , p + t.GtIfr_Mnd.capacity() ) );p = p + t.GtIfr_Mnd.capacity();
    t.GtIfr_Fut.replace(0,t.GtIfr_Fut.capacity(), pcStr.substring( p , p + t.GtIfr_Fut.capacity() ) );p = p + t.GtIfr_Fut.capacity();
    t.GtIfr_Uvb.replace(0,t.GtIfr_Uvb.capacity(), pcStr.substring( p , p + t.GtIfr_Uvb.capacity() ) );p = p + t.GtIfr_Uvb.capacity();
    t.GtIfr_Can.replace(0,t.GtIfr_Can.capacity(), pcStr.substring( p , p + t.GtIfr_Can.capacity() ) );p = p + t.GtIfr_Can.capacity();
    t.GtIfr_Vlc.replace(0,t.GtIfr_Vlc.capacity(), pcStr.substring( p , p + t.GtIfr_Vlc.capacity() ) );p = p + t.GtIfr_Vlc.capacity();
    t.GtIfr_Nbe.replace(0,t.GtIfr_Nbe.capacity(), pcStr.substring( p , p + t.GtIfr_Nbe.capacity() ) );p = p + t.GtIfr_Nbe.capacity();
    t.GtIfr_Ben.replace(0,t.GtIfr_Ben.capacity(), pcStr.substring( p , p + t.GtIfr_Ben.capacity() ) );p = p + t.GtIfr_Ben.capacity();
    t.GtIfr_Fvc.replace(0,t.GtIfr_Fvc.capacity(), pcStr.substring( p , p + t.GtIfr_Fvc.capacity() ) );p = p + t.GtIfr_Fvc.capacity();
    t.GtIfr_Fll.replace(0,t.GtIfr_Fll.capacity(), pcStr.substring( p , p + t.GtIfr_Fll.capacity() ) );p = p + t.GtIfr_Fll.capacity();
    vec.add ( t );
    return vec;
  }
  //---------------------------------------------------------------------------------------
  public String LSet_De_ImgGtIfr(Buf_Img_GtIfr p_Img)
  {
    String pcStr = "";
    pcStr = pcStr + p_Img.getGta();
    pcStr = pcStr + p_Img.getTpa();
    pcStr = pcStr + p_Img.getNse();
    pcStr = pcStr + p_Img.getMnd();
    pcStr = pcStr + p_Img.getFut();
    pcStr = pcStr + p_Img.getUvb();
    pcStr = pcStr + p_Img.getCan();
    pcStr = pcStr + p_Img.getVlc();
    pcStr = pcStr + p_Img.getNbe();
    pcStr = pcStr + p_Img.getBen();
    pcStr = pcStr + p_Img.getFvc();
    pcStr = pcStr + p_Img.getFll();
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
  public static class Buf_Img_GtCma
  {
    public StringBuffer GtCma_Fui = new StringBuffer(19 );  //X(19)       19 Folio Unico Instrumento
    public StringBuffer GtCma_Nmi = new StringBuffer(30 );  //X(30)       49 Nemotecnico Instrumento
    public StringBuffer GtCma_Nml = new StringBuffer(15 );  //9(13)V9(2)  64 Nominal Instrumento
    public StringBuffer GtCma_Lin = new StringBuffer(19 );  //X(19)       83 Linea CMA Instrumento
    public StringBuffer GtCma_Fev = new StringBuffer(8  );  //9(08)       91 Fecha Valor
    public StringBuffer GtCma_Cnt = new StringBuffer(11 );  //9(11)      102 Cantidad
    public StringBuffer GtCma_Vcn = new StringBuffer(15 );  //9(13)V9(2) 117 Valor Contable
    public StringBuffer GtCma_Dvf = new StringBuffer(1  );  //X(01)      118 Idr Diversificacion
    public StringBuffer GtCma_Fll = new StringBuffer(606);  //X(709)     827 Disponible

    public String getFui()         {return GtCma_Fui.toString();}
    public String getNmi()         {return GtCma_Nmi.toString();}
    public String getNml()         {return GtCma_Nml.toString();}
    public String getLin()         {return GtCma_Lin.toString();}
    public String getFev()         {return GtCma_Fev.toString();}
    public String getCnt()         {return GtCma_Cnt.toString();}
    public String getVcn()         {return GtCma_Vcn.toString();}
    public String getDvf()         {return GtCma_Dvf.toString();}
    public String getFll()         {return GtCma_Fll.toString();}

    public Formateo f = new Formateo();
    public String fgetLin()        {return getLin().substring(7);}
    public String fgetFev()        {return f.FormatDate(getFev());}
    public String fgetNml()        {return f.Formato(getNml(),"9.999.999.999.999",2,2,',');}
    public String fgetCnt()        {return f.Formato(getCnt(),"9.999.999.999.999",0,0,',');}
    public String fgetVcn()        {return f.Formato(getVcn(),"9.999.999.999.999",2,2,',');}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_Img_GtCma LSet_A_ImgGtCma(String pcStr)
  {
    int p=0;
    Buf_Img_GtCma l_Img = new Buf_Img_GtCma();
    l_Img.GtCma_Fui.replace(0, l_Img.GtCma_Fui.capacity(), pcStr.substring(p, p + l_Img.GtCma_Fui.capacity())); p = p + l_Img.GtCma_Fui.capacity();
    l_Img.GtCma_Nmi.replace(0, l_Img.GtCma_Nmi.capacity(), pcStr.substring(p, p + l_Img.GtCma_Nmi.capacity())); p = p + l_Img.GtCma_Nmi.capacity();
    l_Img.GtCma_Nml.replace(0, l_Img.GtCma_Nml.capacity(), pcStr.substring(p, p + l_Img.GtCma_Nml.capacity())); p = p + l_Img.GtCma_Nml.capacity();
    l_Img.GtCma_Lin.replace(0, l_Img.GtCma_Lin.capacity(), pcStr.substring(p, p + l_Img.GtCma_Lin.capacity())); p = p + l_Img.GtCma_Lin.capacity();
    l_Img.GtCma_Fev.replace(0, l_Img.GtCma_Fev.capacity(), pcStr.substring(p, p + l_Img.GtCma_Fev.capacity())); p = p + l_Img.GtCma_Fev.capacity();
    l_Img.GtCma_Cnt.replace(0, l_Img.GtCma_Cnt.capacity(), pcStr.substring(p, p + l_Img.GtCma_Cnt.capacity())); p = p + l_Img.GtCma_Cnt.capacity();
    l_Img.GtCma_Vcn.replace(0, l_Img.GtCma_Vcn.capacity(), pcStr.substring(p, p + l_Img.GtCma_Vcn.capacity())); p = p + l_Img.GtCma_Vcn.capacity();
    l_Img.GtCma_Dvf.replace(0, l_Img.GtCma_Dvf.capacity(), pcStr.substring(p, p + l_Img.GtCma_Dvf.capacity())); p = p + l_Img.GtCma_Dvf.capacity();
    l_Img.GtCma_Fll.replace(0, l_Img.GtCma_Fll.capacity(), pcStr.substring(p, p + l_Img.GtCma_Fll.capacity())); p = p + l_Img.GtCma_Fll.capacity();
    return l_Img;
  }
  //---------------------------------------------------------------------------------------
  public String LSet_De_ImgGtCma(Buf_Img_GtCma p_Img)
  {
    String pcStr = "";
    pcStr = pcStr + p_Img.getFui();
    pcStr = pcStr + p_Img.getNmi();
    pcStr = pcStr + p_Img.getNml();
    pcStr = pcStr + p_Img.getLin();
    pcStr = pcStr + p_Img.getFev();
    pcStr = pcStr + p_Img.getCnt();
    pcStr = pcStr + p_Img.getVcn();
    pcStr = pcStr + p_Img.getDvf();
    pcStr = pcStr + p_Img.getFll();
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
  public static class Buf_Img_GtCus
  {
    public StringBuffer GtCus_Num = new StringBuffer(7 );   //9(07)        7 int    numAprob
    public StringBuffer GtCus_Smu = new StringBuffer(45);   //X(45)       52 String sMargenUnico
    public StringBuffer GtCus_Csm = new StringBuffer(20);   //X(20)       72 String codSMargen
    public StringBuffer GtCus_Dsc = new StringBuffer(80);   //X(80)      152 String descripcion
    public StringBuffer GtCus_Mnd = new StringBuffer(15);   //X(15)      167 String moneda
    public StringBuffer GtCus_Mto = new StringBuffer(19);   //9(15)V9(4) 186 String montoAprobado
    public StringBuffer GtCus_Pre = new StringBuffer(80);   //X(80)      266 String smargenPreaprob
    public StringBuffer GtCus_Chk = new StringBuffer(1 );   //X(01)      267 Idr Checked (S/N)
    public StringBuffer GtCus_Fll = new StringBuffer(560);  //X(560)     827

    public String getNum()         {return GtCus_Num.toString();}
    public String getSmu()         {return GtCus_Smu.toString();}
    public String getCsm()         {return GtCus_Csm.toString();}
    public String getDsc()         {return GtCus_Dsc.toString();}
    public String getMnd()         {return GtCus_Mnd.toString();}
    public String getMto()         {return GtCus_Mto.toString();}
    public String getPre()         {return GtCus_Pre.toString();}
    public String getChk()         {return GtCus_Chk.toString();}
    public String getFll()         {return GtCus_Fll.toString();}

    public Formateo f = new Formateo();
    public int    igetNum()        {if (getNum().trim().equals("")) return 0; else return Integer.parseInt(getNum()); }
    public String fgetMto()        {return f.Formato(getMto(),"9.999.999.999.999",4,4,',');}
    public String egetMto()        {return f.Formato(getMto(),"9.999.999.999.999",4,0,',');}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_Img_GtCus LSet_A_ImgGtCus(String pcStr)
  {
    Buf_Img_GtCus l_Img = new Buf_Img_GtCus();
    Vector vDatos = LSet_A_vImgGtCus(pcStr);
    l_Img = (Buf_Img_GtCus)vDatos.elementAt(0);
    return l_Img;
  }
  //---------------------------------------------------------------------------------------
  public static Vector LSet_A_vImgGtCus(String pcStr)
  {
    int p=0;
    Vector vec= new Vector();
    Buf_Img_GtCus t = new Buf_Img_GtCus();
    t.GtCus_Num.replace(0,t.GtCus_Num.capacity(), pcStr.substring( p , p + t.GtCus_Num.capacity() ) );p = p + t.GtCus_Num.capacity();
    t.GtCus_Smu.replace(0,t.GtCus_Smu.capacity(), pcStr.substring( p , p + t.GtCus_Smu.capacity() ) );p = p + t.GtCus_Smu.capacity();
    t.GtCus_Csm.replace(0,t.GtCus_Csm.capacity(), pcStr.substring( p , p + t.GtCus_Csm.capacity() ) );p = p + t.GtCus_Csm.capacity();
    t.GtCus_Dsc.replace(0,t.GtCus_Dsc.capacity(), pcStr.substring( p , p + t.GtCus_Dsc.capacity() ) );p = p + t.GtCus_Dsc.capacity();
    t.GtCus_Mnd.replace(0,t.GtCus_Mnd.capacity(), pcStr.substring( p , p + t.GtCus_Mnd.capacity() ) );p = p + t.GtCus_Mnd.capacity();
    t.GtCus_Mto.replace(0,t.GtCus_Mto.capacity(), pcStr.substring( p , p + t.GtCus_Mto.capacity() ) );p = p + t.GtCus_Mto.capacity();
    t.GtCus_Pre.replace(0,t.GtCus_Pre.capacity(), pcStr.substring( p , p + t.GtCus_Pre.capacity() ) );p = p + t.GtCus_Pre.capacity();
    t.GtCus_Chk.replace(0,t.GtCus_Chk.capacity(), pcStr.substring( p , p + t.GtCus_Chk.capacity() ) );p = p + t.GtCus_Chk.capacity();
    t.GtCus_Fll.replace(0,t.GtCus_Fll.capacity(), pcStr.substring(p) );
    vec.add ( t );
    return vec;
  }
  //---------------------------------------------------------------------------------------
  public String LSet_De_ImgGtCus(Buf_Img_GtCus p_Img)
  {
    String pcStr = "";
    pcStr = pcStr + p_Img.getNum();
    pcStr = pcStr + p_Img.getSmu();
    pcStr = pcStr + p_Img.getCsm();
    pcStr = pcStr + p_Img.getDsc();
    pcStr = pcStr + p_Img.getMnd();
    pcStr = pcStr + p_Img.getMto();
    pcStr = pcStr + p_Img.getPre();
    pcStr = pcStr + p_Img.getChk();
    pcStr = pcStr + p_Img.getFll();
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
  public static class Buf_Img_GtDoc
  {
    public StringBuffer GtDoc_Seq = new StringBuffer(3  );  //9(03)        3 Identificador Secuencia
    public StringBuffer GtDoc_Iad = new StringBuffer(1  );  //X(01)        4 Idr Adjunto (S/N)
    public StringBuffer GtDoc_Dsc = new StringBuffer(90 );  //X(90)       94 Descripcion Documento
    public StringBuffer GtDoc_Adj = new StringBuffer(9  );  //9(09)      103 Folio Adjunto
    public StringBuffer GtDoc_Fad = new StringBuffer(8  );  //9(06)      111 Fecha Adjunto
    public StringBuffer GtDoc_Tpi = new StringBuffer(3  );  //X(03)      114 Tipo Adjunto (BMP/DOC/etc)
    public StringBuffer GtDoc_Fll = new StringBuffer(713);  //X(713)     827

    public String getSeq()         {return GtDoc_Seq.toString();}
    public String getIad()         {return GtDoc_Iad.toString();}
    public String getDsc()         {return GtDoc_Dsc.toString();}
    public String getAdj()         {return GtDoc_Adj.toString();}
    public String getFad()         {return GtDoc_Fad.toString();}
    public String getTpi()         {return GtDoc_Tpi.toString();}
    public String getFll()         {return GtDoc_Fll.toString();}

    public Formateo f = new Formateo();
    public int    igetSeq()        {if (getSeq().trim().equals("")) return 0; else return Integer.parseInt(getSeq()); }
    public int    igetAdj()        {if (getAdj().trim().equals("")) return 0; else return Integer.parseInt(getAdj()); }
  }
  //---------------------------------------------------------------------------------------
  public static Buf_Img_GtDoc LSet_A_ImgGtDoc(String pcStr)
  {
    Buf_Img_GtDoc l_Img = new Buf_Img_GtDoc();
    Vector vDatos = LSet_A_vImgGtDoc(pcStr);
    l_Img = (Buf_Img_GtDoc)vDatos.elementAt(0);
    return l_Img;
  }
  //---------------------------------------------------------------------------------------
  public static Vector LSet_A_vImgGtDoc(String pcStr)
  {
    int p=0;
    Vector vec= new Vector();
    Buf_Img_GtDoc t = new Buf_Img_GtDoc();
    t.GtDoc_Seq.replace(0,t.GtDoc_Seq.capacity(), pcStr.substring( p , p + t.GtDoc_Seq.capacity() ) );p = p + t.GtDoc_Seq.capacity();
    t.GtDoc_Iad.replace(0,t.GtDoc_Iad.capacity(), pcStr.substring( p , p + t.GtDoc_Iad.capacity() ) );p = p + t.GtDoc_Iad.capacity();
    t.GtDoc_Dsc.replace(0,t.GtDoc_Dsc.capacity(), pcStr.substring( p , p + t.GtDoc_Dsc.capacity() ) );p = p + t.GtDoc_Dsc.capacity();
    t.GtDoc_Adj.replace(0,t.GtDoc_Adj.capacity(), pcStr.substring( p , p + t.GtDoc_Adj.capacity() ) );p = p + t.GtDoc_Adj.capacity();
    t.GtDoc_Fad.replace(0,t.GtDoc_Fad.capacity(), pcStr.substring( p , p + t.GtDoc_Fad.capacity() ) );p = p + t.GtDoc_Fad.capacity();
    t.GtDoc_Tpi.replace(0,t.GtDoc_Tpi.capacity(), pcStr.substring( p , p + t.GtDoc_Tpi.capacity() ) );p = p + t.GtDoc_Tpi.capacity();
    t.GtDoc_Fll.replace(0,t.GtDoc_Fll.capacity(), pcStr.substring(p) );
    vec.add ( t );
    return vec;
  }
  //---------------------------------------------------------------------------------------
  public String LSet_De_ImgGtDoc(Buf_Img_GtDoc p_Img)
  {
    String pcStr = "";
    pcStr = pcStr + p_Img.getSeq();
    pcStr = pcStr + p_Img.getIad();
    pcStr = pcStr + p_Img.getDsc();
    pcStr = pcStr + p_Img.getAdj();
    pcStr = pcStr + p_Img.getFad();
    pcStr = pcStr + p_Img.getTpi();
    pcStr = pcStr + p_Img.getFll();
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
  public static class Buf_Img_GtEsl
  {
    public StringBuffer GtEsl_Opf = new StringBuffer(126);  //X(126)     126 Observaciones Ida
    public StringBuffer GtEsl_Abg = new StringBuffer(5  );  //9(05)      131 Codigo Abogado
    public StringBuffer GtEsl_Nag = new StringBuffer(25 );  //X(25)      156 Nombre Abogado
    public StringBuffer GtEsl_Iln = new StringBuffer(7  );  //9(07)      163 Inf.Legal Numero
    public StringBuffer GtEsl_Ilf = new StringBuffer(8  );  //9(08)      171 Inf.Legal Fecha
    public StringBuffer GtEsl_Efc = new StringBuffer(8  );  //9(08)      179 Escritura Fecha
    public StringBuffer GtEsl_Ent = new StringBuffer(25 );  //X(25)      204 Escritura Notaria
    public StringBuffer GtEsl_Pcm = new StringBuffer(11 );  //9(11)      215 Prenda Comuna
    public StringBuffer GtEsl_Ncm = new StringBuffer(20 );  //X(20)      235 Dmc.NomComuna
    public StringBuffer GtEsl_Npv = new StringBuffer(17 );  //X(17)      252 Dmc.NomProvincia
    public StringBuffer GtEsl_Tta = new StringBuffer(1  );  //X(01)      253 TitAprobados (b/S/N)
    public StringBuffer GtEsl_Obl = new StringBuffer(512);  //X(512)     765 Observacion Legal
    public StringBuffer GtEsl_Dsl = new StringBuffer(9  );  //9(09)      774 @Descripcion Legal
    public StringBuffer GtEsl_Anl = new StringBuffer(9  );  //9(09)      783 @Anotacion Legal
    public StringBuffer GtEsl_Fll = new StringBuffer(44 );  //X(44)      827 Disponible

    public String getOpf()         {return GtEsl_Opf.toString();}
    public String getAbg()         {return GtEsl_Abg.toString();}
    public String getNag()         {return GtEsl_Nag.toString();}
    public String getIln()         {return GtEsl_Iln.toString();}
    public String getIlf()         {return GtEsl_Ilf.toString();}
    public String getEfc()         {return GtEsl_Efc.toString();}
    public String getEnt()         {return GtEsl_Ent.toString();}
    public String getPcm()         {return GtEsl_Pcm.toString();}
    public String getNcm()         {return GtEsl_Ncm.toString();}
    public String getNpv()         {return GtEsl_Npv.toString();}
    public String getTta()         {return GtEsl_Tta.toString();}
    public String getObl()         {return GtEsl_Obl.toString();}
    public String getDsl()         {return GtEsl_Dsl.toString();}
    public String getAnl()         {return GtEsl_Anl.toString();}
    public String getFll()         {return GtEsl_Fll.toString();}

    public Formateo f = new Formateo();
    public String fgetOpf()        {return GtEsl_Opf.toString().trim();}
    public String fgetAbg()        {return GtEsl_Abg.toString().trim();}
    public String fgetNag()        {return GtEsl_Nag.toString();}
    public String fgetIln()        {return GtEsl_Iln.toString();}
    public String fgetIlf()        {return f.FormatDate(GtEsl_Ilf.toString());}
    public String fgetEfc()        {return f.FormatDate(GtEsl_Efc.toString());}
    public String fgetEnt()        {return GtEsl_Ent.toString();}
    public String fgetPcm()        {return GtEsl_Pcm.toString().trim();}
    public String fgetNcm()        {return GtEsl_Ncm.toString();}
    public String fgetNpv()        {return GtEsl_Npv.toString();}
    public String fgetTta()        {return GtEsl_Tta.toString();}
    public String fgetObl()        {return GtEsl_Obl.toString().trim();}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_Img_GtEsl LSet_A_ImgGtEsl(String pcStr)
  {
    Buf_Img_GtEsl l_Img = new Buf_Img_GtEsl();
    Vector vDatos = LSet_A_vImgGtEsl(pcStr);
    l_Img = (Buf_Img_GtEsl)vDatos.elementAt(0);
    return l_Img;
  }
  //---------------------------------------------------------------------------------------
  public static Vector LSet_A_vImgGtEsl(String pcStr)
  {
    int p=0;
    Vector vec= new Vector();
    Buf_Img_GtEsl t = new Buf_Img_GtEsl();
    t.GtEsl_Opf.replace(0,t.GtEsl_Opf.capacity(), pcStr.substring( p , p + t.GtEsl_Opf.capacity() ) );p = p + t.GtEsl_Opf.capacity();
    t.GtEsl_Abg.replace(0,t.GtEsl_Abg.capacity(), pcStr.substring( p , p + t.GtEsl_Abg.capacity() ) );p = p + t.GtEsl_Abg.capacity();
    t.GtEsl_Nag.replace(0,t.GtEsl_Nag.capacity(), pcStr.substring( p , p + t.GtEsl_Nag.capacity() ) );p = p + t.GtEsl_Nag.capacity();
    t.GtEsl_Iln.replace(0,t.GtEsl_Iln.capacity(), pcStr.substring( p , p + t.GtEsl_Iln.capacity() ) );p = p + t.GtEsl_Iln.capacity();
    t.GtEsl_Ilf.replace(0,t.GtEsl_Ilf.capacity(), pcStr.substring( p , p + t.GtEsl_Ilf.capacity() ) );p = p + t.GtEsl_Ilf.capacity();
    t.GtEsl_Efc.replace(0,t.GtEsl_Efc.capacity(), pcStr.substring( p , p + t.GtEsl_Efc.capacity() ) );p = p + t.GtEsl_Efc.capacity();
    t.GtEsl_Ent.replace(0,t.GtEsl_Ent.capacity(), pcStr.substring( p , p + t.GtEsl_Ent.capacity() ) );p = p + t.GtEsl_Ent.capacity();
    t.GtEsl_Pcm.replace(0,t.GtEsl_Pcm.capacity(), pcStr.substring( p , p + t.GtEsl_Pcm.capacity() ) );p = p + t.GtEsl_Pcm.capacity();
    t.GtEsl_Ncm.replace(0,t.GtEsl_Ncm.capacity(), pcStr.substring( p , p + t.GtEsl_Ncm.capacity() ) );p = p + t.GtEsl_Ncm.capacity();
    t.GtEsl_Npv.replace(0,t.GtEsl_Npv.capacity(), pcStr.substring( p , p + t.GtEsl_Npv.capacity() ) );p = p + t.GtEsl_Npv.capacity();
    t.GtEsl_Tta.replace(0,t.GtEsl_Tta.capacity(), pcStr.substring( p , p + t.GtEsl_Tta.capacity() ) );p = p + t.GtEsl_Tta.capacity();
    t.GtEsl_Obl.replace(0,t.GtEsl_Obl.capacity(), pcStr.substring( p , p + t.GtEsl_Obl.capacity() ) );p = p + t.GtEsl_Obl.capacity();
    t.GtEsl_Dsl.replace(0,t.GtEsl_Dsl.capacity(), pcStr.substring( p , p + t.GtEsl_Dsl.capacity() ) );p = p + t.GtEsl_Dsl.capacity();
    t.GtEsl_Anl.replace(0,t.GtEsl_Anl.capacity(), pcStr.substring( p , p + t.GtEsl_Anl.capacity() ) );p = p + t.GtEsl_Anl.capacity();
    t.GtEsl_Fll.replace(0,t.GtEsl_Fll.capacity(), "" );
    vec.add ( t );
    return vec;
  }
  //---------------------------------------------------------------------------------------
  public String LSet_De_ImgGtEsl(Buf_Img_GtEsl p_Img)
  {
    String pcStr = "";
    pcStr = pcStr + p_Img.getOpf();
    pcStr = pcStr + p_Img.getAbg();
    pcStr = pcStr + p_Img.getNag();
    pcStr = pcStr + p_Img.getIln();
    pcStr = pcStr + p_Img.getIlf();
    pcStr = pcStr + p_Img.getEfc();
    pcStr = pcStr + p_Img.getEnt();
    pcStr = pcStr + p_Img.getPcm();
    pcStr = pcStr + p_Img.getNcm();
    pcStr = pcStr + p_Img.getNpv();
    pcStr = pcStr + p_Img.getTta();
    pcStr = pcStr + p_Img.getObl();
    pcStr = pcStr + p_Img.getDsl();
    pcStr = pcStr + p_Img.getAnl();
    pcStr = pcStr + p_Img.getFll();
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
  public static class Buf_Img_GtUsu
  {
    public StringBuffer GtUsu_Cli = new StringBuffer(10 );  //X(10)       10 RUT Usuario
    public StringBuffer GtUsu_Ncl = new StringBuffer(40 );  //X(40)       50 Nombre Usuario
    public StringBuffer GtUsu_Idc = new StringBuffer(1  );  //X(01)       51 Idr Comparte (C)
    public StringBuffer GtUsu_Fll = new StringBuffer(776);  //X(776)     827

    public String getCli()         {return GtUsu_Cli.toString();}
    public String getNcl()         {return GtUsu_Ncl.toString();}
    public String getIdc()         {return GtUsu_Idc.toString();}
    public String getFll()         {return GtUsu_Fll.toString();}

    public Formateo f = new Formateo();
    public String fgetCli()        {return f.fmtRut(GtUsu_Cli.toString());}
    public String fgetNcl()        {return GtUsu_Ncl.toString();}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_Img_GtUsu LSet_A_ImgGtUsu(String pcStr)
  {
    Buf_Img_GtUsu l_Img = new Buf_Img_GtUsu();
    Vector vDatos = LSet_A_vImgGtUsu(pcStr);
    l_Img = (Buf_Img_GtUsu)vDatos.elementAt(0);
    return l_Img;
  }
  //---------------------------------------------------------------------------------------
  public static Vector LSet_A_vImgGtUsu(String pcStr)
  {
    int p=0;
    Vector vec= new Vector();
    Buf_Img_GtUsu t = new Buf_Img_GtUsu();
    t.GtUsu_Cli.replace(0,t.GtUsu_Cli.capacity(), pcStr.substring( p , p + t.GtUsu_Cli.capacity() ) );p = p + t.GtUsu_Cli.capacity();
    t.GtUsu_Ncl.replace(0,t.GtUsu_Ncl.capacity(), pcStr.substring( p , p + t.GtUsu_Ncl.capacity() ) );p = p + t.GtUsu_Ncl.capacity();
    t.GtUsu_Idc.replace(0,t.GtUsu_Idc.capacity(), pcStr.substring( p , p + t.GtUsu_Idc.capacity() ) );p = p + t.GtUsu_Idc.capacity();
    t.GtUsu_Fll.replace(0,t.GtUsu_Fll.capacity(), pcStr.substring(p) );
    vec.add ( t );
    return vec;
  }
  //---------------------------------------------------------------------------------------
  public String LSet_De_ImgGtUsu(Buf_Img_GtUsu p_Img)
  {
    String pcStr = "";
    pcStr = pcStr + p_Img.getCli();
    pcStr = pcStr + p_Img.getNcl();
    pcStr = pcStr + p_Img.getIdc();
    pcStr = pcStr + p_Img.getFll();
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
  public static class Buf_Img_GtPrp
  {
    public StringBuffer GtPrp_Cli = new StringBuffer(10 );  //X(10)       10 RUT Prpario
    public StringBuffer GtPrp_Ncl = new StringBuffer(40 );  //X(40)       50 Nombre Prpario
    public StringBuffer GtPrp_Fll = new StringBuffer(777);  //X(777)     827

    public String getCli()         {return GtPrp_Cli.toString();}
    public String getNcl()         {return GtPrp_Ncl.toString();}
    public String getFll()         {return GtPrp_Fll.toString();}

    public Formateo f = new Formateo();
    public String fgetCli()        {return f.fmtRut(GtPrp_Cli.toString());}
    public String fgetNcl()        {return GtPrp_Ncl.toString();}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_Img_GtPrp LSet_A_ImgGtPrp(String pcStr)
  {
    Buf_Img_GtPrp l_Img = new Buf_Img_GtPrp();
    Vector vDatos = LSet_A_vImgGtPrp(pcStr);
    l_Img = (Buf_Img_GtPrp)vDatos.elementAt(0);
    return l_Img;
  }
  //---------------------------------------------------------------------------------------
  public static Vector LSet_A_vImgGtPrp(String pcStr)
  {
    int p=0;
    Vector vec= new Vector();
    Buf_Img_GtPrp t = new Buf_Img_GtPrp();
    t.GtPrp_Cli.replace(0,t.GtPrp_Cli.capacity(), pcStr.substring( p , p + t.GtPrp_Cli.capacity() ) );p = p + t.GtPrp_Cli.capacity();
    t.GtPrp_Ncl.replace(0,t.GtPrp_Ncl.capacity(), pcStr.substring( p , p + t.GtPrp_Ncl.capacity() ) );p = p + t.GtPrp_Ncl.capacity();
    t.GtPrp_Fll.replace(0,t.GtPrp_Fll.capacity(), pcStr.substring(p) );
    vec.add ( t );
    return vec;
  }
  //---------------------------------------------------------------------------------------
  public String LSet_De_ImgGtPrp(Buf_Img_GtPrp p_Img)
  {
    String pcStr = "";
    pcStr = pcStr + p_Img.getCli();
    pcStr = pcStr + p_Img.getNcl();
    pcStr = pcStr + p_Img.getFll();
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
  public static class Buf_Img_GtIns
  {
    public StringBuffer GtIns_Cti = new StringBuffer(2  );  //9(02)
    public StringBuffer GtIns_Nti = new StringBuffer(20 );  //X(20)
    public StringBuffer GtIns_Csv = new StringBuffer(3  );  //X(03)
    public StringBuffer GtIns_Lcl = new StringBuffer(15 );  //X(15)
    public StringBuffer GtIns_Rpo = new StringBuffer(4  );  //9(04)
    public StringBuffer GtIns_Foj = new StringBuffer(7  );  //9(07)
    public StringBuffer GtIns_Nin = new StringBuffer(7  );  //9(07)
    public StringBuffer GtIns_Grd = new StringBuffer(1  );  //9(01)
    public StringBuffer GtIns_Fll = new StringBuffer(768);  //

    public String getCti()         {return GtIns_Cti.toString();}
    public String getNti()         {return GtIns_Nti.toString();}
    public String getCsv()         {return GtIns_Csv.toString();}
    public String getLcl()         {return GtIns_Lcl.toString();}
    public String getRpo()         {return GtIns_Rpo.toString();}
    public String getFoj()         {return GtIns_Foj.toString();}
    public String getNin()         {return GtIns_Nin.toString();}
    public String getGrd()         {return GtIns_Grd.toString();}
    public String getFll()         {return GtIns_Fll.toString();}

    public String fgetCti()        {return GtIns_Cti.toString()+"-"+GtIns_Nti.toString();}
    public String fgetCsv()        {return GtIns_Csv.toString();}
    public String fgetRpo()        {return GtIns_Rpo.toString();}
    public String fgetFoj()        {return GtIns_Foj.toString();}
    public String fgetNin()        {return GtIns_Nin.toString();}
    public String fgetGrd()        {return GtIns_Grd.toString();}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_Img_GtIns LSet_A_ImgGtIns(String pcStr)
  {
    Buf_Img_GtIns l_Img = new Buf_Img_GtIns();
    Vector vDatos = LSet_A_vImgGtIns(pcStr);
    l_Img = (Buf_Img_GtIns)vDatos.elementAt(0);
    return l_Img;
  }
  //---------------------------------------------------------------------------------------
  public static Vector LSet_A_vImgGtIns(String pcStr)
  {
    int p=0;
    Vector vec= new Vector();
    Buf_Img_GtIns t = new Buf_Img_GtIns();
    t.GtIns_Cti.replace(0,t.GtIns_Cti.capacity(), pcStr.substring( p , p + t.GtIns_Cti.capacity() ) );p = p + t.GtIns_Cti.capacity();
    t.GtIns_Nti.replace(0,t.GtIns_Nti.capacity(), pcStr.substring( p , p + t.GtIns_Nti.capacity() ) );p = p + t.GtIns_Nti.capacity();
    t.GtIns_Csv.replace(0,t.GtIns_Csv.capacity(), pcStr.substring( p , p + t.GtIns_Csv.capacity() ) );p = p + t.GtIns_Csv.capacity();
    t.GtIns_Lcl.replace(0,t.GtIns_Lcl.capacity(), pcStr.substring( p , p + t.GtIns_Lcl.capacity() ) );p = p + t.GtIns_Lcl.capacity();
    t.GtIns_Rpo.replace(0,t.GtIns_Rpo.capacity(), pcStr.substring( p , p + t.GtIns_Rpo.capacity() ) );p = p + t.GtIns_Rpo.capacity();
    t.GtIns_Foj.replace(0,t.GtIns_Foj.capacity(), pcStr.substring( p , p + t.GtIns_Foj.capacity() ) );p = p + t.GtIns_Foj.capacity();
    t.GtIns_Nin.replace(0,t.GtIns_Nin.capacity(), pcStr.substring( p , p + t.GtIns_Nin.capacity() ) );p = p + t.GtIns_Nin.capacity();
    t.GtIns_Grd.replace(0,t.GtIns_Grd.capacity(), pcStr.substring( p , p + t.GtIns_Grd.capacity() ) );p = p + t.GtIns_Grd.capacity();
    t.GtIns_Fll.replace(0,t.GtIns_Fll.capacity(), pcStr.substring(p) );
    vec.add ( t );
    return vec;
  }
  //---------------------------------------------------------------------------------------
  public String LSet_De_ImgGtIns(Buf_Img_GtIns p_Img)
  {
    String pcStr = "";
    pcStr = pcStr + p_Img.getCti();
    pcStr = pcStr + p_Img.getNti();
    pcStr = pcStr + p_Img.getCsv();
    pcStr = pcStr + p_Img.getLcl();
    pcStr = pcStr + p_Img.getRpo();
    pcStr = pcStr + p_Img.getFoj();
    pcStr = pcStr + p_Img.getNin();
    pcStr = pcStr + p_Img.getGrd();
    pcStr = pcStr + p_Img.getFll();
    return pcStr;
  }
  //===============================================================================================================================
  //Generica: SEGUROS
  public static class Buf_Img_GtSgr
  {
    public StringBuffer GtSgr_Bse = new StringBuffer(358);  //X(358)     358 Area Base Fija
    public StringBuffer GtSgr_Psw = new StringBuffer(1  );  //X(01)      359 SWT Ingreso Datos
    public StringBuffer GtSgr_Idx = new StringBuffer(3  );  //X(03)      362 Secuencia
    public StringBuffer GtSgr_Ggr = new StringBuffer(3  );  //X(03)      365 Grupo Garantia (HYP/CRH/AYF/CTW/DMN/DMX/PMN/PMX)
    public StringBuffer GtSgr_Umt = new StringBuffer(1  );  //X(01)      366 UMonetaria
    public StringBuffer GtSgr_Dsg = new StringBuffer(100);  //X(100)     466 Descripcion Garantia
    public StringBuffer GtSgr_Vcn = new StringBuffer(15 );  //9(13)V9(2) 481 VContable MND
    public StringBuffer GtSgr_Vsu = new StringBuffer(15 );  //9(11)V9(4) 496 Valor Asegurable
    public StringBuffer GtSgr_Vsg = new StringBuffer(15 );  //9(13)V9(2) 511 Valor Asegurable $
    public StringBuffer GtSgr_Fpr = new StringBuffer(8  );  //9(08)      519 Fecha Presentacion
    public StringBuffer GtSgr_Fig = new StringBuffer(8  );  //9(08)      527 FIngreso Garantia
    public StringBuffer GtSgr_Svl = new StringBuffer(15 );  //9(11)V9(4) 542 Valor Poliza MO
    public StringBuffer GtSgr_Fll = new StringBuffer(285);  //X(285)     827 Disponible

    public String getBse()         {return GtSgr_Bse.toString();}
    public String getPsw()         {return GtSgr_Psw.toString();}
    public String getIdx()         {return GtSgr_Idx.toString();}
    public String getGgr()         {return GtSgr_Ggr.toString();}
    public String getUmt()         {return GtSgr_Umt.toString();}
    public String getDsg()         {return GtSgr_Dsg.toString();}
    public String getVcn()         {return GtSgr_Vcn.toString();}
    public String getVsu()         {return GtSgr_Vsu.toString();}
    public String getVsg()         {return GtSgr_Vsg.toString();}
    public String getFpr()         {return GtSgr_Fpr.toString();}
    public String getFig()         {return GtSgr_Fig.toString();}
    public String getSvl()         {return GtSgr_Svl.toString();}
    public String getFll()         {return GtSgr_Fll.toString();}

    public Formateo f = new Formateo();
    public int    igetIdx()        {
                                     if (getIdx().trim().equals(""))
                                        { return Integer.parseInt("999"); }
                                     else
                                        { return Integer.parseInt(GtSgr_Idx.toString()); }
                                   }
    public String fgetVcn()        {return f.Formato(GtSgr_Vcn.toString(),"99.999.999.999",2,2,',');}
    public String fgetVsu()        {return f.Formato(GtSgr_Vsu.toString(),"99.999.999.999",4,4,',');}
    public String ggetVsu()        {return f.Formato(GtSgr_Vsu.toString(),"99.999.999.999",4,2,',');}
    public String pgetVsu()        {return f.Formato(GtSgr_Vsu.toString(),"99.999.999.999",4,0,',');}
    public String fgetVsg()        {return f.Formato(GtSgr_Vsg.toString(),"99.999.999.999",2,2,',');}
    public String fgetFpr()        {return f.FormatDate(GtSgr_Fpr.toString());}
    public String fgetFig()        {return f.FormatDate(GtSgr_Fig.toString());}
    public String fgetSvl()        {return f.Formato(GtSgr_Svl.toString(),"99.999.999.999",4,4,',');}
    public String ggetSvl()        {return f.Formato(GtSgr_Svl.toString(),"99.999.999.999",4,2,',');}
    public String pgetSvl()        {return f.Formato(GtSgr_Svl.toString(),"99.999.999.999",4,0,',');}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_Img_GtSgr LSet_A_ImgGtSgr(String pcStr)
  {
    Buf_Img_GtSgr l_Img = new Buf_Img_GtSgr();
    Vector vDatos = LSet_A_vImgGtSgr(pcStr);
    l_Img = (Buf_Img_GtSgr)vDatos.elementAt(0);
    return l_Img;
  }
  //---------------------------------------------------------------------------------------
  public static Vector LSet_A_vImgGtSgr(String pcStr)
  {
    int p=0;
    Vector vec= new Vector();
    Buf_Img_GtSgr t = new Buf_Img_GtSgr();
    t.GtSgr_Bse.replace(0,t.GtSgr_Bse.capacity(), pcStr.substring( p , p + t.GtSgr_Bse.capacity() ) );p = p + t.GtSgr_Bse.capacity();
    t.GtSgr_Psw.replace(0,t.GtSgr_Psw.capacity(), pcStr.substring( p , p + t.GtSgr_Psw.capacity() ) );p = p + t.GtSgr_Psw.capacity();
    t.GtSgr_Idx.replace(0,t.GtSgr_Idx.capacity(), pcStr.substring( p , p + t.GtSgr_Idx.capacity() ) );p = p + t.GtSgr_Idx.capacity();
    t.GtSgr_Ggr.replace(0,t.GtSgr_Ggr.capacity(), pcStr.substring( p , p + t.GtSgr_Ggr.capacity() ) );p = p + t.GtSgr_Ggr.capacity();
    t.GtSgr_Umt.replace(0,t.GtSgr_Umt.capacity(), pcStr.substring( p , p + t.GtSgr_Umt.capacity() ) );p = p + t.GtSgr_Umt.capacity();
    t.GtSgr_Dsg.replace(0,t.GtSgr_Dsg.capacity(), pcStr.substring( p , p + t.GtSgr_Dsg.capacity() ) );p = p + t.GtSgr_Dsg.capacity();
    t.GtSgr_Vcn.replace(0,t.GtSgr_Vcn.capacity(), pcStr.substring( p , p + t.GtSgr_Vcn.capacity() ) );p = p + t.GtSgr_Vcn.capacity();
    t.GtSgr_Vsu.replace(0,t.GtSgr_Vsu.capacity(), pcStr.substring( p , p + t.GtSgr_Vsu.capacity() ) );p = p + t.GtSgr_Vsu.capacity();
    t.GtSgr_Vsg.replace(0,t.GtSgr_Vsg.capacity(), pcStr.substring( p , p + t.GtSgr_Vsg.capacity() ) );p = p + t.GtSgr_Vsg.capacity();
    t.GtSgr_Fpr.replace(0,t.GtSgr_Fpr.capacity(), pcStr.substring( p , p + t.GtSgr_Fpr.capacity() ) );p = p + t.GtSgr_Fpr.capacity();
    t.GtSgr_Fig.replace(0,t.GtSgr_Fig.capacity(), pcStr.substring( p , p + t.GtSgr_Fig.capacity() ) );p = p + t.GtSgr_Fig.capacity();
    t.GtSgr_Svl.replace(0,t.GtSgr_Svl.capacity(), pcStr.substring( p , p + t.GtSgr_Svl.capacity() ) );p = p + t.GtSgr_Svl.capacity();
    t.GtSgr_Fll.replace(0,t.GtSgr_Fll.capacity(), pcStr.substring( p , p + t.GtSgr_Fll.capacity() ) );p = p + t.GtSgr_Fll.capacity();
    vec.add ( t );
    return vec;
  }
  //---------------------------------------------------------------------------------------
  public String LSet_De_ImgGtSgr(Buf_Img_GtSgr p_Img)
  {
    String pcStr = "";
    pcStr = pcStr + p_Img.getBse();
    pcStr = pcStr + p_Img.getPsw();
    pcStr = pcStr + p_Img.getIdx();
    pcStr = pcStr + p_Img.getGgr();
    pcStr = pcStr + p_Img.getUmt();
    pcStr = pcStr + p_Img.getDsg();
    pcStr = pcStr + p_Img.getVcn();
    pcStr = pcStr + p_Img.getVsu();
    pcStr = pcStr + p_Img.getVsg();
    pcStr = pcStr + p_Img.getFpr();
    pcStr = pcStr + p_Img.getFig();
    pcStr = pcStr + p_Img.getSvl();
    pcStr = pcStr + p_Img.getFll();
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
  //Operaciones: DETALLE DE SEGUROS
  public static class Buf_Img_GtSgd
  {
    public StringBuffer GtSgd_Seq = new StringBuffer(3 );   //9(03)        3 Bien
    public StringBuffer GtSgd_Itb = new StringBuffer(2 );   //X(02)        5 Tipo Bien
    public StringBuffer GtSgd_Dsb = new StringBuffer(30);   //X(30)       35 Desc. Bien
    public StringBuffer GtSgd_Frv = new StringBuffer(8 );   //9(08)       43 FValorizacion
    public StringBuffer GtSgd_Vcn = new StringBuffer(15);   //9(13)V9(2)  58 Valor Contable
    public StringBuffer GtSgd_Umt = new StringBuffer(1 );   //X(01)       59 UMonetaria
    public StringBuffer GtSgd_Vum = new StringBuffer(9 );   //9(07)V9(2)  68 Valor UMonetaria
    public StringBuffer GtSgd_Vsu = new StringBuffer(15);   //9(11)V9(4)  83 Valor Asegurable
    public StringBuffer GtSgd_Vsg = new StringBuffer(15);   //9(13)V9(2)  98 Valor Asegurable $
    public StringBuffer GtSgd_Isg = new StringBuffer(1 );   //X(01)       99 Idr. Seguro
    public StringBuffer GtSgd_Csg = new StringBuffer(3 );   //9(03)      102 Cod.Cia Seguros
    public StringBuffer GtSgd_Nsg = new StringBuffer(15);   //X(15)      117 Nom.Cia Seguros
    public StringBuffer GtSgd_Spz = new StringBuffer(10);   //X(10)      127 Id. Poliza
    public StringBuffer GtSgd_Sfv = new StringBuffer(8 );   //9(08)      135 FVencto Poliza
    public StringBuffer GtSgd_Sum = new StringBuffer(1 );   //X(01)      136 UMonetaria Seguro
    public StringBuffer GtSgd_Svl = new StringBuffer(15);   //9(11)V9(4) 151 Valor Poliza MO
    public StringBuffer GtSgd_Fll = new StringBuffer(676);  //X(676)     827 Disponible

    public String getSeq()         {return GtSgd_Seq.toString();}
    public String getItb()         {return GtSgd_Itb.toString();}
    public String getDsb()         {return GtSgd_Dsb.toString();}
    public String getFrv()         {return GtSgd_Frv.toString();}
    public String getVcn()         {return GtSgd_Vcn.toString();}
    public String getUmt()         {return GtSgd_Umt.toString();}
    public String getVum()         {return GtSgd_Vum.toString();}
    public String getVsu()         {return GtSgd_Vsu.toString();}
    public String getVsg()         {return GtSgd_Vsg.toString();}
    public String getIsg()         {return GtSgd_Isg.toString();}
    public String getCsg()         {return GtSgd_Csg.toString();}
    public String getNsg()         {return GtSgd_Nsg.toString();}
    public String getSpz()         {return GtSgd_Spz.toString();}
    public String getSfv()         {return GtSgd_Sfv.toString();}
    public String getSum()         {return GtSgd_Sum.toString();}
    public String getSvl()         {return GtSgd_Svl.toString();}
    public String getFll()         {return GtSgd_Fll.toString();}

    public Formateo f = new Formateo();
    public String fgetFrv()     {return f.FormatDate(GtSgd_Frv.toString());}
    public String fgetVcn()     {return f.Formato(GtSgd_Vcn.toString(),"99.999.999.999",2,2,',');}
    public String fgetVum()     {return f.Formato(GtSgd_Vum.toString(),"99.999.999.999",2,2,',');}
    public String fgetVsu()     {return f.Formato(GtSgd_Vsu.toString(),"99.999.999.999",4,4,',');}
    public String ggetVsu()     {return f.Formato(GtSgd_Vsu.toString(),"99.999.999.999",4,2,',');}
    public String pgetVsu()     {return f.Formato(GtSgd_Vsu.toString(),"99.999.999.999",4,0,',');}
    public String fgetVsg()     {return f.Formato(GtSgd_Vsg.toString(),"99.999.999.999",2,2,',');}
    public String fgetIsg()
                                   {
                                     String lcIsg="Sin seguro";
                                     if (getIsg().equals("A"))  { lcIsg = "Automatico"; }
                                     if (getIsg().equals("M"))  { lcIsg = "Manual"; }
                                     return lcIsg;
                                   }
    public String fgetSfv()     {return f.FormatDate(GtSgd_Sfv.toString());}
    public String fgetSvl()     {return f.Formato(GtSgd_Svl.toString(),"99.999.999.999",4,4,',');}
    public String ggetSvl()     {return f.Formato(GtSgd_Svl.toString(),"99.999.999.999",4,2,',');}
    public String pgetSvl()     {return f.Formato(GtSgd_Svl.toString(),"99.999.999.999",4,0,',');}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_Img_GtSgd LSet_A_ImgGtSgd(String pcStr)
  {
    Buf_Img_GtSgd l_Img = new Buf_Img_GtSgd();
    Vector vDatos = LSet_A_vImgGtSgd(pcStr);
    l_Img = (Buf_Img_GtSgd)vDatos.elementAt(0);
    return l_Img;
  }
  //---------------------------------------------------------------------------------------
  public static Vector LSet_A_vImgGtSgd(String pcStr)
  {
    int p=0;
    Vector vec= new Vector();
    Buf_Img_GtSgd t = new Buf_Img_GtSgd();
    t.GtSgd_Seq.replace(0,t.GtSgd_Seq.capacity(), pcStr.substring( p , p + t.GtSgd_Seq.capacity() ) );p = p + t.GtSgd_Seq.capacity();
    t.GtSgd_Itb.replace(0,t.GtSgd_Itb.capacity(), pcStr.substring( p , p + t.GtSgd_Itb.capacity() ) );p = p + t.GtSgd_Itb.capacity();
    t.GtSgd_Dsb.replace(0,t.GtSgd_Dsb.capacity(), pcStr.substring( p , p + t.GtSgd_Dsb.capacity() ) );p = p + t.GtSgd_Dsb.capacity();
    t.GtSgd_Frv.replace(0,t.GtSgd_Frv.capacity(), pcStr.substring( p , p + t.GtSgd_Frv.capacity() ) );p = p + t.GtSgd_Frv.capacity();
    t.GtSgd_Vcn.replace(0,t.GtSgd_Vcn.capacity(), pcStr.substring( p , p + t.GtSgd_Vcn.capacity() ) );p = p + t.GtSgd_Vcn.capacity();
    t.GtSgd_Umt.replace(0,t.GtSgd_Umt.capacity(), pcStr.substring( p , p + t.GtSgd_Umt.capacity() ) );p = p + t.GtSgd_Umt.capacity();
    t.GtSgd_Vum.replace(0,t.GtSgd_Vum.capacity(), pcStr.substring( p , p + t.GtSgd_Vum.capacity() ) );p = p + t.GtSgd_Vum.capacity();
    t.GtSgd_Vsu.replace(0,t.GtSgd_Vsu.capacity(), pcStr.substring( p , p + t.GtSgd_Vsu.capacity() ) );p = p + t.GtSgd_Vsu.capacity();
    t.GtSgd_Vsg.replace(0,t.GtSgd_Vsg.capacity(), pcStr.substring( p , p + t.GtSgd_Vsg.capacity() ) );p = p + t.GtSgd_Vsg.capacity();
    t.GtSgd_Isg.replace(0,t.GtSgd_Isg.capacity(), pcStr.substring( p , p + t.GtSgd_Isg.capacity() ) );p = p + t.GtSgd_Isg.capacity();
    t.GtSgd_Csg.replace(0,t.GtSgd_Csg.capacity(), pcStr.substring( p , p + t.GtSgd_Csg.capacity() ) );p = p + t.GtSgd_Csg.capacity();
    t.GtSgd_Nsg.replace(0,t.GtSgd_Nsg.capacity(), pcStr.substring( p , p + t.GtSgd_Nsg.capacity() ) );p = p + t.GtSgd_Nsg.capacity();
    t.GtSgd_Spz.replace(0,t.GtSgd_Spz.capacity(), pcStr.substring( p , p + t.GtSgd_Spz.capacity() ) );p = p + t.GtSgd_Spz.capacity();
    t.GtSgd_Sfv.replace(0,t.GtSgd_Sfv.capacity(), pcStr.substring( p , p + t.GtSgd_Sfv.capacity() ) );p = p + t.GtSgd_Sfv.capacity();
    t.GtSgd_Sum.replace(0,t.GtSgd_Sum.capacity(), pcStr.substring( p , p + t.GtSgd_Sum.capacity() ) );p = p + t.GtSgd_Sum.capacity();
    t.GtSgd_Svl.replace(0,t.GtSgd_Svl.capacity(), pcStr.substring( p , p + t.GtSgd_Svl.capacity() ) );p = p + t.GtSgd_Svl.capacity();
    t.GtSgd_Fll.replace(0,t.GtSgd_Fll.capacity(), pcStr.substring( p , p + t.GtSgd_Fll.capacity() ) );p = p + t.GtSgd_Fll.capacity();
    vec.add ( t );
    return vec;
  }
  //---------------------------------------------------------------------------------------
  public String LSet_De_ImgGtSgd(Buf_Img_GtSgd p_Img)
  {
    String pcStr = "";
    pcStr = pcStr + p_Img.getSeq();
    pcStr = pcStr + p_Img.getItb();
    pcStr = pcStr + p_Img.getDsb();
    pcStr = pcStr + p_Img.getFrv();
    pcStr = pcStr + p_Img.getVcn();
    pcStr = pcStr + p_Img.getUmt();
    pcStr = pcStr + p_Img.getVum();
    pcStr = pcStr + p_Img.getVsu();
    pcStr = pcStr + p_Img.getVsg();
    pcStr = pcStr + p_Img.getIsg();
    pcStr = pcStr + p_Img.getCsg();
    pcStr = pcStr + p_Img.getNsg();
    pcStr = pcStr + p_Img.getSpz();
    pcStr = pcStr + p_Img.getSfv();
    pcStr = pcStr + p_Img.getSum();
    pcStr = pcStr + p_Img.getSvl();
    pcStr = pcStr + p_Img.getFll();
    return pcStr;
  }
  //===============================================================================================================================
  //Generica: GARANTIAS ESPECIFICAS
  public static class Buf_Img_GtGeb
  {
    public StringBuffer GtGeb_Bse = new StringBuffer(358);  //X(358)     358 Area Base Fija
    public StringBuffer GtGeb_Idx = new StringBuffer(3  );  //9(03)      361 Idr Secuencia
    public StringBuffer GtGeb_Dsg = new StringBuffer(100);  //X(100)     461 Descripcion Garantia
    public StringBuffer GtGeb_Suc = new StringBuffer(3  );  //9(03)      464 Sucursal
    public StringBuffer GtGeb_Mnd = new StringBuffer(3  );  //9(03)      467 Moneda
    public StringBuffer GtGeb_Vcn = new StringBuffer(15 );  //9(13)V9(2) 482 VContable MND
    public StringBuffer GtGeb_Ggr = new StringBuffer(3  );  //X(03)      485 Grupo Garantia (HYP/CRH/AYF/CTW/DMN/DMX/PMN/PMX)
    public StringBuffer GtGeb_Fpr = new StringBuffer(8  );  //9(08)      493 Fecha Presentacion
    public StringBuffer GtGeb_Fig = new StringBuffer(8  );  //9(08)      501 FIngreso Garantia
    public StringBuffer GtGeb_Cbt = new StringBuffer(1  );  //X(01)      502 Cobertura (Gral/Espec)
    public StringBuffer GtGeb_Fll = new StringBuffer(325);  //X(325)     827 Disponible

    public String getBse()         {return GtGeb_Bse.toString();}
    public String getIdx()         {return GtGeb_Idx.toString();}
    public String getDsg()         {return GtGeb_Dsg.toString();}
    public String getSuc()         {return GtGeb_Suc.toString();}
    public String getMnd()         {return GtGeb_Mnd.toString();}
    public String getVcn()         {return GtGeb_Vcn.toString();}
    public String getGgr()         {return GtGeb_Ggr.toString();}
    public String getFpr()         {return GtGeb_Fpr.toString();}
    public String getFig()         {return GtGeb_Fig.toString();}
    public String getCbt()         {return GtGeb_Cbt.toString();}
    public String getFll()         {return GtGeb_Fll.toString();}

    public Formateo f = new Formateo();
    public String fgetVcn()        {return f.Formato(GtGeb_Vcn.toString(),"99.999.999.999",2,2,',');}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_Img_GtGeb LSet_A_ImgGtGeb(String pcStr)
  {
    Buf_Img_GtGeb l_Img = new Buf_Img_GtGeb();
    Vector vDatos = LSet_A_vImgGtGeb(pcStr);
    l_Img = (Buf_Img_GtGeb)vDatos.elementAt(0);
    return l_Img;
  }
  //---------------------------------------------------------------------------------------
  public static Vector LSet_A_vImgGtGeb(String pcStr)
  {
    int p=0;
    Vector vec= new Vector();
    Buf_Img_GtGeb t = new Buf_Img_GtGeb();
    t.GtGeb_Bse.replace(0,t.GtGeb_Bse.capacity(), pcStr.substring( p , p + t.GtGeb_Bse.capacity() ) );p = p + t.GtGeb_Bse.capacity();
    t.GtGeb_Idx.replace(0,t.GtGeb_Idx.capacity(), pcStr.substring( p , p + t.GtGeb_Idx.capacity() ) );p = p + t.GtGeb_Idx.capacity();
    t.GtGeb_Dsg.replace(0,t.GtGeb_Dsg.capacity(), pcStr.substring( p , p + t.GtGeb_Dsg.capacity() ) );p = p + t.GtGeb_Dsg.capacity();
    t.GtGeb_Suc.replace(0,t.GtGeb_Suc.capacity(), pcStr.substring( p , p + t.GtGeb_Suc.capacity() ) );p = p + t.GtGeb_Suc.capacity();
    t.GtGeb_Mnd.replace(0,t.GtGeb_Mnd.capacity(), pcStr.substring( p , p + t.GtGeb_Mnd.capacity() ) );p = p + t.GtGeb_Mnd.capacity();
    t.GtGeb_Vcn.replace(0,t.GtGeb_Vcn.capacity(), pcStr.substring( p , p + t.GtGeb_Vcn.capacity() ) );p = p + t.GtGeb_Vcn.capacity();
    t.GtGeb_Ggr.replace(0,t.GtGeb_Ggr.capacity(), pcStr.substring( p , p + t.GtGeb_Ggr.capacity() ) );p = p + t.GtGeb_Ggr.capacity();
    t.GtGeb_Fpr.replace(0,t.GtGeb_Fpr.capacity(), pcStr.substring( p , p + t.GtGeb_Fpr.capacity() ) );p = p + t.GtGeb_Fpr.capacity();
    t.GtGeb_Fig.replace(0,t.GtGeb_Fig.capacity(), pcStr.substring( p , p + t.GtGeb_Fig.capacity() ) );p = p + t.GtGeb_Fig.capacity();
    t.GtGeb_Cbt.replace(0,t.GtGeb_Cbt.capacity(), pcStr.substring( p , p + t.GtGeb_Cbt.capacity() ) );p = p + t.GtGeb_Cbt.capacity();
    t.GtGeb_Fll.replace(0,t.GtGeb_Fll.capacity(), pcStr.substring( p , p + t.GtGeb_Fll.capacity() ) );p = p + t.GtGeb_Fll.capacity();
    vec.add ( t );
    return vec;
  }
  //---------------------------------------------------------------------------------------
  public String LSet_De_ImgGtGeb(Buf_Img_GtGeb p_Img)
  {
    String pcStr = "";
    pcStr = pcStr + p_Img.getBse();
    pcStr = pcStr + p_Img.getIdx();
    pcStr = pcStr + p_Img.getDsg();
    pcStr = pcStr + p_Img.getSuc();
    pcStr = pcStr + p_Img.getMnd();
    pcStr = pcStr + p_Img.getVcn();
    pcStr = pcStr + p_Img.getGgr();
    pcStr = pcStr + p_Img.getFpr();
    pcStr = pcStr + p_Img.getFig();
    pcStr = pcStr + p_Img.getCbt();
    pcStr = pcStr + p_Img.getFll();
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
  //Operaciones: DETALLE DE GARANTIAS ESPECIFICAS
  public static class Buf_Img_GtGes
  {
    public StringBuffer GtGes_Ges = new StringBuffer(10 );  //X(10)       10 Id. Garantia
    public StringBuffer GtGes_Rel = new StringBuffer(5  );  //X(05)       15 Rel. Cliente/Garantia
    public StringBuffer GtGes_Cre = new StringBuffer(22 );  //X(22)       37 Id. Credito
    public StringBuffer GtGes_Est = new StringBuffer(5  );  //X(05)       42 Estado
    public StringBuffer GtGes_Mod = new StringBuffer(1  );  //X(01)       43 Modif Agrega/Elimina
    public StringBuffer GtGes_Fll = new StringBuffer(784);  //X(784)     827 Disponible

    public String getGes()         {return GtGes_Ges.toString();}
    public String getRel()         {return GtGes_Rel.toString();}
    public String getCre()         {return GtGes_Cre.toString();}
    public String getEst()         {return GtGes_Est.toString();}
    public String getMod()         {return GtGes_Mod.toString();}
    public String getFll()         {return GtGes_Fll.toString();}

    public Formateo f = new Formateo();
    public String fgetGes()        {return GtGes_Ges.toString().substring(0,3) + "-" +GtGes_Ges.toString().substring(3);}
    public String fgetCre()        {return GtGes_Cre.toString().substring(0,3) + "-" +GtGes_Cre.toString().substring(3);}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_Img_GtGes LSet_A_ImgGtGes(String pcStr)
  {
    Buf_Img_GtGes l_Img = new Buf_Img_GtGes();
    Vector vDatos = LSet_A_vImgGtGes(pcStr);
    l_Img = (Buf_Img_GtGes)vDatos.elementAt(0);
    return l_Img;
  }
  //---------------------------------------------------------------------------------------
  public static Vector LSet_A_vImgGtGes(String pcStr)
  {
    int p=0;
    Vector vec= new Vector();
    Buf_Img_GtGes t = new Buf_Img_GtGes();
    t.GtGes_Ges.replace(0,t.GtGes_Ges.capacity(), pcStr.substring( p , p + t.GtGes_Ges.capacity() ) );p = p + t.GtGes_Ges.capacity();
    t.GtGes_Rel.replace(0,t.GtGes_Rel.capacity(), pcStr.substring( p , p + t.GtGes_Rel.capacity() ) );p = p + t.GtGes_Rel.capacity();
    t.GtGes_Cre.replace(0,t.GtGes_Cre.capacity(), pcStr.substring( p , p + t.GtGes_Cre.capacity() ) );p = p + t.GtGes_Cre.capacity();
    t.GtGes_Est.replace(0,t.GtGes_Est.capacity(), pcStr.substring( p , p + t.GtGes_Est.capacity() ) );p = p + t.GtGes_Est.capacity();
    t.GtGes_Mod.replace(0,t.GtGes_Mod.capacity(), pcStr.substring( p , p + t.GtGes_Mod.capacity() ) );p = p + t.GtGes_Mod.capacity();
    t.GtGes_Fll.replace(0,t.GtGes_Fll.capacity(), pcStr.substring( p , p + t.GtGes_Fll.capacity() ) );p = p + t.GtGes_Fll.capacity();
    vec.add ( t );
    return vec;
  }
  //---------------------------------------------------------------------------------------
  public String LSet_De_ImgGtGes(Buf_Img_GtGes p_Img)
  {
    String pcStr = "";
    pcStr = pcStr + p_Img.getGes();
    pcStr = pcStr + p_Img.getRel();
    pcStr = pcStr + p_Img.getCre();
    pcStr = pcStr + p_Img.getEst();
    pcStr = pcStr + p_Img.getMod();
    pcStr = pcStr + p_Img.getFll();
    return pcStr;
  }
  //===============================================================================================================================
  //Generica: MODIFICACION MYC [MODMC]
  public static class Buf_Img_GtMmc
  {
    public StringBuffer GtMmc_Bse = new StringBuffer(358);  //X(358)     358 Area Base Fija
    public StringBuffer GtMmc_Iln = new StringBuffer(7  );  //9(07)      365 Secuencia Modificacion
    public StringBuffer GtMmc_Dsg = new StringBuffer(100);  //X(100)     465 Descripcion Garantia
    public StringBuffer GtMmc_Vcn = new StringBuffer(15 );  //9(13)V9(2) 480 VContable MND
    public StringBuffer GtMmc_Ggr = new StringBuffer(3  );  //X(03)      483 Grupo Garantia (HYP/CRH/AYF/CTW/DMN/DMX/PMN/PMX)
    public StringBuffer GtMmc_Fig = new StringBuffer(8  );  //9(08)      491 FIngreso Garantia
    public StringBuffer GtMmc_Cbt = new StringBuffer(1  );  //X(01)      492 Cobertura (Gral/Espec)
    public StringBuffer GtMmc_Trj = new StringBuffer(3  );  //X(03)      495 TReaj
    public StringBuffer GtMmc_Mnd = new StringBuffer(3  );  //9(03)      498 Moneda
    public StringBuffer GtMmc_Dcm = new StringBuffer(1  );  //9(01)      499 Decimales
    public StringBuffer GtMmc_Dsl = new StringBuffer(9  );  //9(09)      508 @Detalle Descripcion
    public StringBuffer GtMmc_Psw = new StringBuffer(1  );  //X(01)      509 SWT Ingreso Datos
    public StringBuffer GtMmc_Fmd = new StringBuffer(8  );  //9(08)      517 Fecha Tasacion
    public StringBuffer GtMmc_Vts = new StringBuffer(15 );  //9(13)V9(2) 532 VTasacion MND
    public StringBuffer GtMmc_Idx = new StringBuffer(3  );  //9(03)      535 Indice Activo Detalles
    public StringBuffer GtMmc_Tfr = new StringBuffer(3  );  //9(03)      538 Total Indices Detalles
    public StringBuffer GtMmc_Nsl = new StringBuffer(9  );  //9(09)      547 @Nuevo Detalle Descripcion
    public StringBuffer GtMmc_Fll = new StringBuffer(280);  //X(280)     827 Disponible

    public String getBse()         {return GtMmc_Bse.toString();}
    public String getIln()         {return GtMmc_Iln.toString();}
    public String getDsg()         {return GtMmc_Dsg.toString();}
    public String getVcn()         {return GtMmc_Vcn.toString();}
    public String getGgr()         {return GtMmc_Ggr.toString();}
    public String getFig()         {return GtMmc_Fig.toString();}
    public String getCbt()         {return GtMmc_Cbt.toString();}
    public String getTrj()         {return GtMmc_Trj.toString();}
    public String getMnd()         {return GtMmc_Mnd.toString();}
    public String getDcm()         {return GtMmc_Dcm.toString();}
    public String getDsl()         {return GtMmc_Dsl.toString();}
    public String getPsw()         {return GtMmc_Psw.toString();}
    public String getFmd()         {return GtMmc_Fmd.toString();}
    public String getVts()         {return GtMmc_Vts.toString();}
    public String getIdx()         {return GtMmc_Idx.toString();}
    public String getTfr()         {return GtMmc_Tfr.toString();}
    public String getNsl()         {return GtMmc_Nsl.toString();}
    public String getFll()         {return GtMmc_Fll.toString();}

    public int    igetIln()        {if (getIln().trim().equals("")) return 0; else return Integer.parseInt(getIln());}
    public int    igetDcm()        {if (getDcm().trim().equals("")) return 0; else return Integer.parseInt(getDcm());}
    public int    igetDsl()        {if (getDsl().trim().equals("")) return 0; else return Integer.parseInt(getDsl());}
    public int    igetIdx()        {if (getIdx().trim().equals("")) return 0; else return Integer.parseInt(getIdx());}
    public int    igetTfr()        {if (getTfr().trim().equals("")) return 0; else return Integer.parseInt(getTfr());}
    public int    igetNsl()        {if (getNsl().trim().equals("")) return 0; else return Integer.parseInt(getNsl());}

    public Formateo f = new Formateo();
    public String fgetFig()        {return f.FormatDate(getFig());}
    public String fgetFmd()        {return f.FormatDate(getFmd());}
    public String fgetVcn()        {return f.Formato(getVcn(),"99.999.999.999",2,2,',');}
    public String fgetVts()        {return f.Formato(getVts(),"99.999.999.999",2,2,',');}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_Img_GtMmc LSet_A_ImgGtMmc(String pcStr)
  {
    Buf_Img_GtMmc l_Img = new Buf_Img_GtMmc();
    Vector vDatos = LSet_A_vImgGtMmc(pcStr);
    l_Img = (Buf_Img_GtMmc)vDatos.elementAt(0);
    return l_Img;
  }
  //---------------------------------------------------------------------------------------
  public static Vector LSet_A_vImgGtMmc(String pcStr)
  {
    int p=0;
    Vector vec= new Vector();
    Buf_Img_GtMmc t = new Buf_Img_GtMmc();
    t.GtMmc_Bse.replace(0,t.GtMmc_Bse.capacity(), pcStr.substring( p , p + t.GtMmc_Bse.capacity() ) );p = p + t.GtMmc_Bse.capacity();
    t.GtMmc_Iln.replace(0,t.GtMmc_Iln.capacity(), pcStr.substring( p , p + t.GtMmc_Iln.capacity() ) );p = p + t.GtMmc_Iln.capacity();
    t.GtMmc_Dsg.replace(0,t.GtMmc_Dsg.capacity(), pcStr.substring( p , p + t.GtMmc_Dsg.capacity() ) );p = p + t.GtMmc_Dsg.capacity();
    t.GtMmc_Vcn.replace(0,t.GtMmc_Vcn.capacity(), pcStr.substring( p , p + t.GtMmc_Vcn.capacity() ) );p = p + t.GtMmc_Vcn.capacity();
    t.GtMmc_Ggr.replace(0,t.GtMmc_Ggr.capacity(), pcStr.substring( p , p + t.GtMmc_Ggr.capacity() ) );p = p + t.GtMmc_Ggr.capacity();
    t.GtMmc_Fig.replace(0,t.GtMmc_Fig.capacity(), pcStr.substring( p , p + t.GtMmc_Fig.capacity() ) );p = p + t.GtMmc_Fig.capacity();
    t.GtMmc_Cbt.replace(0,t.GtMmc_Cbt.capacity(), pcStr.substring( p , p + t.GtMmc_Cbt.capacity() ) );p = p + t.GtMmc_Cbt.capacity();
    t.GtMmc_Trj.replace(0,t.GtMmc_Trj.capacity(), pcStr.substring( p , p + t.GtMmc_Trj.capacity() ) );p = p + t.GtMmc_Trj.capacity();
    t.GtMmc_Mnd.replace(0,t.GtMmc_Mnd.capacity(), pcStr.substring( p , p + t.GtMmc_Mnd.capacity() ) );p = p + t.GtMmc_Mnd.capacity();
    t.GtMmc_Dcm.replace(0,t.GtMmc_Dcm.capacity(), pcStr.substring( p , p + t.GtMmc_Dcm.capacity() ) );p = p + t.GtMmc_Dcm.capacity();
    t.GtMmc_Dsl.replace(0,t.GtMmc_Dsl.capacity(), pcStr.substring( p , p + t.GtMmc_Dsl.capacity() ) );p = p + t.GtMmc_Dsl.capacity();
    t.GtMmc_Psw.replace(0,t.GtMmc_Psw.capacity(), pcStr.substring( p , p + t.GtMmc_Psw.capacity() ) );p = p + t.GtMmc_Psw.capacity();
    t.GtMmc_Fmd.replace(0,t.GtMmc_Fmd.capacity(), pcStr.substring( p , p + t.GtMmc_Fmd.capacity() ) );p = p + t.GtMmc_Fmd.capacity();
    t.GtMmc_Vts.replace(0,t.GtMmc_Vts.capacity(), pcStr.substring( p , p + t.GtMmc_Vts.capacity() ) );p = p + t.GtMmc_Vts.capacity();
    t.GtMmc_Idx.replace(0,t.GtMmc_Idx.capacity(), pcStr.substring( p , p + t.GtMmc_Idx.capacity() ) );p = p + t.GtMmc_Idx.capacity();
    t.GtMmc_Tfr.replace(0,t.GtMmc_Tfr.capacity(), pcStr.substring( p , p + t.GtMmc_Tfr.capacity() ) );p = p + t.GtMmc_Tfr.capacity();
    t.GtMmc_Nsl.replace(0,t.GtMmc_Nsl.capacity(), pcStr.substring( p , p + t.GtMmc_Nsl.capacity() ) );p = p + t.GtMmc_Nsl.capacity();
    t.GtMmc_Fll.replace(0,t.GtMmc_Fll.capacity(), pcStr.substring( p , p + t.GtMmc_Fll.capacity() ) );p = p + t.GtMmc_Fll.capacity();
    vec.add ( t );
    return vec;
  }
  //---------------------------------------------------------------------------------------
  public String LSet_De_ImgGtMmc(Buf_Img_GtMmc p_Img)
  {
    String pcStr = "";
    pcStr = pcStr + p_Img.getBse();
    pcStr = pcStr + p_Img.getIln();
    pcStr = pcStr + p_Img.getDsg();
    pcStr = pcStr + p_Img.getVcn();
    pcStr = pcStr + p_Img.getGgr();
    pcStr = pcStr + p_Img.getFig();
    pcStr = pcStr + p_Img.getCbt();
    pcStr = pcStr + p_Img.getTrj();
    pcStr = pcStr + p_Img.getMnd();
    pcStr = pcStr + p_Img.getDcm();
    pcStr = pcStr + p_Img.getDsl();
    pcStr = pcStr + p_Img.getPsw();
    pcStr = pcStr + p_Img.getFmd();
    pcStr = pcStr + p_Img.getVts();
    pcStr = pcStr + p_Img.getIdx();
    pcStr = pcStr + p_Img.getTfr();
    pcStr = pcStr + p_Img.getNsl();
    pcStr = pcStr + p_Img.getFll();
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
  //Operaciones: DETALLE DE MODIFICACION MYC [MODMC] (Solo para muestra en pantalla)
  public static class Buf_Img_GtMmd
  {
    public StringBuffer GtMmd_Iln = new StringBuffer(7  );  //9(07)        7 Numero Informe
    public StringBuffer GtMmd_Ilf = new StringBuffer(8  );  //9(08)       15 Fecha Informe
    public StringBuffer GtMmd_Dsl = new StringBuffer(9  );  //9(09)       24 @Descripcion Informe
    public StringBuffer GtMmd_Fll = new StringBuffer(803);  //X(803)     827 Disponible

    public String getIln()         {return GtMmd_Iln.toString();}
    public String getIlf()         {return GtMmd_Ilf.toString();}
    public String getDsl()         {return GtMmd_Dsl.toString();}
    public String getFll()         {return GtMmd_Fll.toString();}

    public int    igetDsl()        {if (getDsl().trim().equals("")) return 0; else return Integer.parseInt(getDsl());}

    public Formateo f = new Formateo();
    public String fgetIlf()                   {return f.FormatDate(getIlf());}
    public String fgetDsl() throws Exception  {BF_LOB lob = new BF_LOB(); if (igetDsl()==0) return ""; else return lob.leeCLOB(getDsl().trim());}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_Img_GtMmd LSet_A_ImgGtMmd(String pcStr)
  {
    Buf_Img_GtMmd l_Img = new Buf_Img_GtMmd();
    Vector vDatos = LSet_A_vImgGtMmd(pcStr);
    l_Img = (Buf_Img_GtMmd)vDatos.elementAt(0);
    return l_Img;
  }
  //---------------------------------------------------------------------------------------
  public static Vector LSet_A_vImgGtMmd(String pcStr)
  {
    int p=0;
    Vector vec= new Vector();
    Buf_Img_GtMmd t = new Buf_Img_GtMmd();
    t.GtMmd_Iln.replace(0,t.GtMmd_Iln.capacity(), pcStr.substring( p , p + t.GtMmd_Iln.capacity() ) );p = p + t.GtMmd_Iln.capacity();
    t.GtMmd_Ilf.replace(0,t.GtMmd_Ilf.capacity(), pcStr.substring( p , p + t.GtMmd_Ilf.capacity() ) );p = p + t.GtMmd_Ilf.capacity();
    t.GtMmd_Dsl.replace(0,t.GtMmd_Dsl.capacity(), pcStr.substring( p , p + t.GtMmd_Dsl.capacity() ) );p = p + t.GtMmd_Dsl.capacity();
    t.GtMmd_Fll.replace(0,t.GtMmd_Fll.capacity(), pcStr.substring( p , p + t.GtMmd_Fll.capacity() ) );p = p + t.GtMmd_Fll.capacity();
    vec.add ( t );
    return vec;
  }
  //---------------------------------------------------------------------------------------
  public String LSet_De_ImgGtMmd(Buf_Img_GtMmd p_Img)
  {
    String pcStr = "";
    pcStr = pcStr + p_Img.getIln();
    pcStr = pcStr + p_Img.getIlf();
    pcStr = pcStr + p_Img.getDsl();
    pcStr = pcStr + p_Img.getFll();
    return pcStr;
  }
  //===============================================================================================================================
  //Generica: ALZAMIENTOS
  public static class Buf_Img_GtAlz
  {
    public StringBuffer GtAlz_Bse = new StringBuffer(358);  //X(358)     358 Area Base Fija
    public StringBuffer GtAlz_Idx = new StringBuffer(3  );  //9(03)      361 Idr Secuencia
    public StringBuffer GtAlz_Dsg = new StringBuffer(100);  //X(100)     461 Descripcion Garantia
    public StringBuffer GtAlz_Suc = new StringBuffer(3  );  //9(03)      464 Sucursal
    public StringBuffer GtAlz_Mnd = new StringBuffer(3  );  //9(03)      467 Moneda
    public StringBuffer GtAlz_Vcn = new StringBuffer(15 );  //9(13)V9(2) 482 VContable MND
    public StringBuffer GtAlz_Ggr = new StringBuffer(3  );  //X(03)      485 Grupo Garantia (HYP/CRH/AYF/CTW/DMN/DMX/PMN/PMX)
    public StringBuffer GtAlz_Fpr = new StringBuffer(8  );  //9(08)      493 Fecha Presentacion
    public StringBuffer GtAlz_Fig = new StringBuffer(8  );  //9(08)      501 FIngreso Garantia
    public StringBuffer GtAlz_Cbt = new StringBuffer(1  );  //X(01)      502 Cobertura (Gral/Espec)
    public StringBuffer GtAlz_Vts = new StringBuffer(15 );  //9(13)V9(2) 517 VTasacion MND
    public StringBuffer GtAlz_Rcr = new StringBuffer(1  );  //X(01)      518 Req.CartaResg (S/Y/N)
    public StringBuffer GtAlz_Fll = new StringBuffer(309);  //X(309)     827 Disponible

    public String getBse()         {return GtAlz_Bse.toString();}
    public String getIdx()         {return GtAlz_Idx.toString();}
    public String getDsg()         {return GtAlz_Dsg.toString();}
    public String getSuc()         {return GtAlz_Suc.toString();}
    public String getMnd()         {return GtAlz_Mnd.toString();}
    public String getVcn()         {return GtAlz_Vcn.toString();}
    public String getGgr()         {return GtAlz_Ggr.toString();}
    public String getFpr()         {return GtAlz_Fpr.toString();}
    public String getFig()         {return GtAlz_Fig.toString();}
    public String getCbt()         {return GtAlz_Cbt.toString();}
    public String getVts()         {return GtAlz_Vts.toString();}
    public String getRcr()         {return GtAlz_Rcr.toString();}
    public String getFll()         {return GtAlz_Fll.toString();}

    public Formateo f = new Formateo();
    public String fgetVcn()        {return f.Formato(GtAlz_Vcn.toString(),"99.999.999.999",2,2,',');}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_Img_GtAlz LSet_A_ImgGtAlz(String pcStr)
  {
    Buf_Img_GtAlz l_Img = new Buf_Img_GtAlz();
    Vector vDatos = LSet_A_vImgGtAlz(pcStr);
    l_Img = (Buf_Img_GtAlz)vDatos.elementAt(0);
    return l_Img;
  }
  //---------------------------------------------------------------------------------------
  public static Vector LSet_A_vImgGtAlz(String pcStr)
  {
    int p=0;
    Vector vec= new Vector();
    Buf_Img_GtAlz t = new Buf_Img_GtAlz();
    t.GtAlz_Bse.replace(0,t.GtAlz_Bse.capacity(), pcStr.substring( p , p + t.GtAlz_Bse.capacity() ) );p = p + t.GtAlz_Bse.capacity();
    t.GtAlz_Idx.replace(0,t.GtAlz_Idx.capacity(), pcStr.substring( p , p + t.GtAlz_Idx.capacity() ) );p = p + t.GtAlz_Idx.capacity();
    t.GtAlz_Dsg.replace(0,t.GtAlz_Dsg.capacity(), pcStr.substring( p , p + t.GtAlz_Dsg.capacity() ) );p = p + t.GtAlz_Dsg.capacity();
    t.GtAlz_Suc.replace(0,t.GtAlz_Suc.capacity(), pcStr.substring( p , p + t.GtAlz_Suc.capacity() ) );p = p + t.GtAlz_Suc.capacity();
    t.GtAlz_Mnd.replace(0,t.GtAlz_Mnd.capacity(), pcStr.substring( p , p + t.GtAlz_Mnd.capacity() ) );p = p + t.GtAlz_Mnd.capacity();
    t.GtAlz_Vcn.replace(0,t.GtAlz_Vcn.capacity(), pcStr.substring( p , p + t.GtAlz_Vcn.capacity() ) );p = p + t.GtAlz_Vcn.capacity();
    t.GtAlz_Ggr.replace(0,t.GtAlz_Ggr.capacity(), pcStr.substring( p , p + t.GtAlz_Ggr.capacity() ) );p = p + t.GtAlz_Ggr.capacity();
    t.GtAlz_Fpr.replace(0,t.GtAlz_Fpr.capacity(), pcStr.substring( p , p + t.GtAlz_Fpr.capacity() ) );p = p + t.GtAlz_Fpr.capacity();
    t.GtAlz_Fig.replace(0,t.GtAlz_Fig.capacity(), pcStr.substring( p , p + t.GtAlz_Fig.capacity() ) );p = p + t.GtAlz_Fig.capacity();
    t.GtAlz_Cbt.replace(0,t.GtAlz_Cbt.capacity(), pcStr.substring( p , p + t.GtAlz_Cbt.capacity() ) );p = p + t.GtAlz_Cbt.capacity();
    t.GtAlz_Vts.replace(0,t.GtAlz_Vts.capacity(), pcStr.substring( p , p + t.GtAlz_Vts.capacity() ) );p = p + t.GtAlz_Vts.capacity();
    t.GtAlz_Rcr.replace(0,t.GtAlz_Rcr.capacity(), pcStr.substring( p , p + t.GtAlz_Rcr.capacity() ) );p = p + t.GtAlz_Rcr.capacity();
    t.GtAlz_Fll.replace(0,t.GtAlz_Fll.capacity(), pcStr.substring( p , p + t.GtAlz_Fll.capacity() ) );p = p + t.GtAlz_Fll.capacity();
    vec.add ( t );
    return vec;
  }
  //---------------------------------------------------------------------------------------
  public String LSet_De_ImgGtAlz(Buf_Img_GtAlz p_Img)
  {
    String pcStr = "";
    pcStr = pcStr + p_Img.getBse();
    pcStr = pcStr + p_Img.getIdx();
    pcStr = pcStr + p_Img.getDsg();
    pcStr = pcStr + p_Img.getSuc();
    pcStr = pcStr + p_Img.getMnd();
    pcStr = pcStr + p_Img.getVcn();
    pcStr = pcStr + p_Img.getGgr();
    pcStr = pcStr + p_Img.getFpr();
    pcStr = pcStr + p_Img.getFig();
    pcStr = pcStr + p_Img.getCbt();
    pcStr = pcStr + p_Img.getVts();
    pcStr = pcStr + p_Img.getRcr();
    pcStr = pcStr + p_Img.getFll();
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
  //Operaciones: DETALLE DE ALZAMIENTOS
  public static class Buf_Img_GtAld
  {
    public StringBuffer GtAld_Seq = new StringBuffer(3  );  //9(03)        3 Secuencia Detalle
    public StringBuffer GtAld_Itb = new StringBuffer(2  );  //X(02)        5 Tipo Bien
    public StringBuffer GtAld_Dsb = new StringBuffer(30 );  //X(30)       35 Descripcion
    public StringBuffer GtAld_Vts = new StringBuffer(13 );  //9(11)V9(2)  48 Valor Tasacion
    public StringBuffer GtAld_Vcn = new StringBuffer(13 );  //9(11)V9(2)  61 Valor Contable
    public StringBuffer GtAld_Est = new StringBuffer(5  );  //X(05)       66 Estado
    public StringBuffer GtAld_Sqb = new StringBuffer(3  );  //9(03)       69 Secuencia Bien
    public StringBuffer GtAld_Fll = new StringBuffer(758);  //X(758)     827 Disponible

    public String getSeq()         {return GtAld_Seq.toString();}
    public String getItb()         {return GtAld_Itb.toString();}
    public String getDsb()         {return GtAld_Dsb.toString();}
    public String getVts()         {return GtAld_Vts.toString();}
    public String getVcn()         {return GtAld_Vcn.toString();}
    public String getEst()         {return GtAld_Est.toString();}
    public String getSqb()         {return GtAld_Sqb.toString();}
    public String getFll()         {return GtAld_Fll.toString();}

    public Formateo f = new Formateo();
    public String fgetVts()        {return f.Formato(GtAld_Vts.toString(),"99.999.999.999",2,2,',');}
    public String fgetVcn()        {return f.Formato(GtAld_Vcn.toString(),"99.999.999.999",2,2,',');}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_Img_GtAld LSet_A_ImgGtAld(String pcStr)
  {
    Buf_Img_GtAld l_Img = new Buf_Img_GtAld();
    Vector vDatos = LSet_A_vImgGtAld(pcStr);
    l_Img = (Buf_Img_GtAld)vDatos.elementAt(0);
    return l_Img;
  }
  //---------------------------------------------------------------------------------------
  public static Vector LSet_A_vImgGtAld(String pcStr)
  {
    int p=0;
    Vector vec= new Vector();
    Buf_Img_GtAld t = new Buf_Img_GtAld();
    t.GtAld_Seq.replace(0,t.GtAld_Seq.capacity(), pcStr.substring( p , p + t.GtAld_Seq.capacity() ) );p = p + t.GtAld_Seq.capacity();
    t.GtAld_Itb.replace(0,t.GtAld_Itb.capacity(), pcStr.substring( p , p + t.GtAld_Itb.capacity() ) );p = p + t.GtAld_Itb.capacity();
    t.GtAld_Dsb.replace(0,t.GtAld_Dsb.capacity(), pcStr.substring( p , p + t.GtAld_Dsb.capacity() ) );p = p + t.GtAld_Dsb.capacity();
    t.GtAld_Vts.replace(0,t.GtAld_Vts.capacity(), pcStr.substring( p , p + t.GtAld_Vts.capacity() ) );p = p + t.GtAld_Vts.capacity();
    t.GtAld_Vcn.replace(0,t.GtAld_Vcn.capacity(), pcStr.substring( p , p + t.GtAld_Vcn.capacity() ) );p = p + t.GtAld_Vcn.capacity();
    t.GtAld_Est.replace(0,t.GtAld_Est.capacity(), pcStr.substring( p , p + t.GtAld_Est.capacity() ) );p = p + t.GtAld_Est.capacity();
    t.GtAld_Sqb.replace(0,t.GtAld_Sqb.capacity(), pcStr.substring( p , p + t.GtAld_Sqb.capacity() ) );p = p + t.GtAld_Sqb.capacity();
    t.GtAld_Fll.replace(0,t.GtAld_Fll.capacity(), pcStr.substring( p , p + t.GtAld_Fll.capacity() ) );p = p + t.GtAld_Fll.capacity();
    vec.add ( t );
    return vec;
  }
  //---------------------------------------------------------------------------------------
  public String LSet_De_ImgGtAld(Buf_Img_GtAld p_Img)
  {
    String pcStr = "";
    pcStr = pcStr + p_Img.getSeq();
    pcStr = pcStr + p_Img.getItb();
    pcStr = pcStr + p_Img.getDsb();
    pcStr = pcStr + p_Img.getVts();
    pcStr = pcStr + p_Img.getVcn();
    pcStr = pcStr + p_Img.getEst();
    pcStr = pcStr + p_Img.getSqb();
    pcStr = pcStr + p_Img.getFll();
    return pcStr;
  }
  //===============================================================================================================================
  //Generica: REVALORIZACION
  public static class Buf_Img_GtRvl
  {
    public StringBuffer GtRvl_Bse = new StringBuffer(358);  //X(358)     358 Area Base Fija
    public StringBuffer GtRvl_Idx = new StringBuffer(3  );  //X(03)      361 Secuencia
    public StringBuffer GtRvl_Dsg = new StringBuffer(100);  //X(100)     461 Descripcion Garantia
    public StringBuffer GtRvl_Vcn = new StringBuffer(15 );  //9(13)V9(2) 476 VContable MND
    public StringBuffer GtRvl_Ggr = new StringBuffer(3  );  //X(03)      479 Grupo Garantia (HYP/CRH/AYF/CTW/DMN/DMX/PMN/PMX)
    public StringBuffer GtRvl_Fpr = new StringBuffer(8  );  //9(08)      487 Fecha Presentacion
    public StringBuffer GtRvl_Fig = new StringBuffer(8  );  //9(08)      495 FIngreso Garantia
    public StringBuffer GtRvl_Cbt = new StringBuffer(1  );  //X(01)      496 Cobertura (Gral/Espec)
    public StringBuffer GtRvl_Trj = new StringBuffer(3  );  //X(03)      499 TReaj
    public StringBuffer GtRvl_Mnd = new StringBuffer(3  );  //9(03)      502 Moneda
    public StringBuffer GtRvl_Dcm = new StringBuffer(1  );  //9(01)      503 Decimales
    public StringBuffer GtRvl_Psw = new StringBuffer(1  );  //X(01)      504 SWT Ingreso Datos
    public StringBuffer GtRvl_Fts = new StringBuffer(8  );  //9(08)      512 Fecha Tasacion
    public StringBuffer GtRvl_Vtt = new StringBuffer(15 );  //9(11)V9(4) 527 VTasacion UMT
    public StringBuffer GtRvl_Vts = new StringBuffer(15 );  //9(13)V9(2) 542 VTasacion MND
    public StringBuffer GtRvl_Fll = new StringBuffer(285);  //X(285)     827 Disponible

    public String getBse()         {return GtRvl_Bse.toString();}
    public String getIdx()         {return GtRvl_Idx.toString();}
    public String getDsg()         {return GtRvl_Dsg.toString();}
    public String getVcn()         {return GtRvl_Vcn.toString();}
    public String getGgr()         {return GtRvl_Ggr.toString();}
    public String getFpr()         {return GtRvl_Fpr.toString();}
    public String getFig()         {return GtRvl_Fig.toString();}
    public String getCbt()         {return GtRvl_Cbt.toString();}
    public String getTrj()         {return GtRvl_Trj.toString();}
    public String getMnd()         {return GtRvl_Mnd.toString();}
    public String getDcm()         {return GtRvl_Dcm.toString();}
    public String getPsw()         {return GtRvl_Psw.toString();}
    public String getFts()         {return GtRvl_Fts.toString();}
    public String getVtt()         {return GtRvl_Vtt.toString();}
    public String getVts()         {return GtRvl_Vts.toString();}
    public String getFll()         {return GtRvl_Fll.toString();}

    public Formateo f = new Formateo();
    public String fgetVts()        {return f.Formato(GtRvl_Vts.toString(),"99.999.999.999",2,2,',');}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_Img_GtRvl LSet_A_ImgGtRvl(String pcStr)
  {
    Buf_Img_GtRvl l_Img = new Buf_Img_GtRvl();
    Vector vDatos = LSet_A_vImgGtRvl(pcStr);
    l_Img = (Buf_Img_GtRvl)vDatos.elementAt(0);
    return l_Img;
  }
  //---------------------------------------------------------------------------------------
  public static Vector LSet_A_vImgGtRvl(String pcStr)
  {
    int p=0;
    Vector vec= new Vector();
    Buf_Img_GtRvl t = new Buf_Img_GtRvl();
    t.GtRvl_Bse.replace(0,t.GtRvl_Bse.capacity(), pcStr.substring( p , p + t.GtRvl_Bse.capacity() ) );p = p + t.GtRvl_Bse.capacity();
    t.GtRvl_Idx.replace(0,t.GtRvl_Idx.capacity(), pcStr.substring( p , p + t.GtRvl_Idx.capacity() ) );p = p + t.GtRvl_Idx.capacity();
    t.GtRvl_Dsg.replace(0,t.GtRvl_Dsg.capacity(), pcStr.substring( p , p + t.GtRvl_Dsg.capacity() ) );p = p + t.GtRvl_Dsg.capacity();
    t.GtRvl_Vcn.replace(0,t.GtRvl_Vcn.capacity(), pcStr.substring( p , p + t.GtRvl_Vcn.capacity() ) );p = p + t.GtRvl_Vcn.capacity();
    t.GtRvl_Ggr.replace(0,t.GtRvl_Ggr.capacity(), pcStr.substring( p , p + t.GtRvl_Ggr.capacity() ) );p = p + t.GtRvl_Ggr.capacity();
    t.GtRvl_Fpr.replace(0,t.GtRvl_Fpr.capacity(), pcStr.substring( p , p + t.GtRvl_Fpr.capacity() ) );p = p + t.GtRvl_Fpr.capacity();
    t.GtRvl_Fig.replace(0,t.GtRvl_Fig.capacity(), pcStr.substring( p , p + t.GtRvl_Fig.capacity() ) );p = p + t.GtRvl_Fig.capacity();
    t.GtRvl_Cbt.replace(0,t.GtRvl_Cbt.capacity(), pcStr.substring( p , p + t.GtRvl_Cbt.capacity() ) );p = p + t.GtRvl_Cbt.capacity();
    t.GtRvl_Trj.replace(0,t.GtRvl_Trj.capacity(), pcStr.substring( p , p + t.GtRvl_Trj.capacity() ) );p = p + t.GtRvl_Trj.capacity();
    t.GtRvl_Mnd.replace(0,t.GtRvl_Mnd.capacity(), pcStr.substring( p , p + t.GtRvl_Mnd.capacity() ) );p = p + t.GtRvl_Mnd.capacity();
    t.GtRvl_Dcm.replace(0,t.GtRvl_Dcm.capacity(), pcStr.substring( p , p + t.GtRvl_Dcm.capacity() ) );p = p + t.GtRvl_Dcm.capacity();
    t.GtRvl_Psw.replace(0,t.GtRvl_Psw.capacity(), pcStr.substring( p , p + t.GtRvl_Psw.capacity() ) );p = p + t.GtRvl_Psw.capacity();
    t.GtRvl_Fts.replace(0,t.GtRvl_Fts.capacity(), pcStr.substring( p , p + t.GtRvl_Fts.capacity() ) );p = p + t.GtRvl_Fts.capacity();
    t.GtRvl_Vtt.replace(0,t.GtRvl_Vtt.capacity(), pcStr.substring( p , p + t.GtRvl_Vtt.capacity() ) );p = p + t.GtRvl_Vtt.capacity();
    t.GtRvl_Vts.replace(0,t.GtRvl_Vts.capacity(), pcStr.substring( p , p + t.GtRvl_Vts.capacity() ) );p = p + t.GtRvl_Vts.capacity();
    t.GtRvl_Fll.replace(0,t.GtRvl_Fll.capacity(), pcStr.substring( p , p + t.GtRvl_Fll.capacity() ) );p = p + t.GtRvl_Fll.capacity();
    vec.add ( t );
    return vec;
  }
  //---------------------------------------------------------------------------------------
  public String LSet_De_ImgGtRvl(Buf_Img_GtRvl p_Img)
  {
    String pcStr = "";
    pcStr = pcStr + p_Img.getBse();
    pcStr = pcStr + p_Img.getIdx();
    pcStr = pcStr + p_Img.getDsg();
    pcStr = pcStr + p_Img.getVcn();
    pcStr = pcStr + p_Img.getGgr();
    pcStr = pcStr + p_Img.getFpr();
    pcStr = pcStr + p_Img.getFig();
    pcStr = pcStr + p_Img.getCbt();
    pcStr = pcStr + p_Img.getTrj();
    pcStr = pcStr + p_Img.getMnd();
    pcStr = pcStr + p_Img.getDcm();
    pcStr = pcStr + p_Img.getPsw();
    pcStr = pcStr + p_Img.getFts();
    pcStr = pcStr + p_Img.getVtt();
    pcStr = pcStr + p_Img.getVts();
    pcStr = pcStr + p_Img.getFll();
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
  //Operaciones: DETALLE DE REVALORIZACION
  public static class Buf_Img_GtRvd
  {
    public StringBuffer GtRvd_Seq = new StringBuffer(3  );  //9(03)        3 Cuota
    public StringBuffer GtRvd_Itb = new StringBuffer(2  );  //X(02)        5 Tipo Bien
    public StringBuffer GtRvd_Ide = new StringBuffer(36 );  //X(36)       41 Id. Bien
    public StringBuffer GtRvd_Dsb = new StringBuffer(30 );  //X(30)       71 Desc. Bien
    public StringBuffer GtRvd_Umd = new StringBuffer(2  );  //X(02)       73 UMedida
    public StringBuffer GtRvd_Pum = new StringBuffer(15 );  //9(11)V9(4)  88 PUnitario
    public StringBuffer GtRvd_Udd = new StringBuffer(9  );  //9(07)V9(2)  97 Cantidad
    public StringBuffer GtRvd_Vtt = new StringBuffer(15 );  //9(11)V9(4) 112 VNominal/Prenda
    public StringBuffer GtRvd_Frv = new StringBuffer(8  );  //9(08)      120 FRevAnterior
    public StringBuffer GtRvd_Vur = new StringBuffer(9  );  //9(07)V9(2) 129 VuoAnterior
    public StringBuffer GtRvd_Vts = new StringBuffer(15 );  //9(13)V9(2) 144 VTasacion/Banco
    public StringBuffer GtRvd_Fvt = new StringBuffer(8  );  //9(08)      152 FVctoAnterior
    public StringBuffer GtRvd_Fre = new StringBuffer(8  );  //9(08)      160 FRevNueva
    public StringBuffer GtRvd_Vue = new StringBuffer(9  );  //9(07)V9(2) 169 VuoNueva
    public StringBuffer GtRvd_Nvt = new StringBuffer(8  );  //9(08)      177 FVctoNueva
    public StringBuffer GtRvd_Ntt = new StringBuffer(15 );  //9(11)V9(4) 192 NvoNominal/Prenda
    public StringBuffer GtRvd_Nts = new StringBuffer(15 );  //9(13)V9(2) 207 NvaTasacion/Banco
    public StringBuffer GtRvd_Idr = new StringBuffer(1  );  //X(01)      208 IdrMod (S/N)
    public StringBuffer GtRvd_Udn = new StringBuffer(9  );  //9(07)V9(2) 217 Cantidad Nueva
    public StringBuffer GtRvd_Pct = new StringBuffer(5  );  //9(03)V9(2) 222 %SBIF
    public StringBuffer GtRvd_Fll = new StringBuffer(605);  //X(605)     827 Disponible

    public String getSeq()         {return GtRvd_Seq.toString();}
    public String getItb()         {return GtRvd_Itb.toString();}
    public String getIde()         {return GtRvd_Ide.toString();}
    public String getDsb()         {return GtRvd_Dsb.toString();}
    public String getUmd()         {return GtRvd_Umd.toString();}
    public String getPum()         {return GtRvd_Pum.toString();}
    public String getUdd()         {return GtRvd_Udd.toString();}
    public String getVtt()         {return GtRvd_Vtt.toString();}
    public String getFrv()         {return GtRvd_Frv.toString();}
    public String getVur()         {return GtRvd_Vur.toString();}
    public String getVts()         {return GtRvd_Vts.toString();}
    public String getFvt()         {return GtRvd_Fvt.toString();}
    public String getFre()         {return GtRvd_Fre.toString();}
    public String getVue()         {return GtRvd_Vue.toString();}
    public String getNvt()         {return GtRvd_Nvt.toString();}
    public String getNtt()         {return GtRvd_Ntt.toString();}
    public String getNts()         {return GtRvd_Nts.toString();}
    public String getIdr()         {return GtRvd_Idr.toString();}
    public String getUdn()         {return GtRvd_Udn.toString();}
    public String getPct()         {return GtRvd_Pct.toString();}
    public String getFll()         {return GtRvd_Fll.toString();}

    public Formateo f = new Formateo();
    public String fgetFrv()        {return f.FormatDate(GtRvd_Frv.toString());}
    public String fgetFvt()        {return f.FormatDate(GtRvd_Fvt.toString());}
    public String fgetFre()        {return f.FormatDate(GtRvd_Fre.toString());}
    public String fgetNvt()        {return f.FormatDate(GtRvd_Nvt.toString());}
    public String fgetVur()        {return f.Formato(GtRvd_Vur.toString(),"99.999.999.999",2,2,',');}
    public String fgetVue()        {return f.Formato(GtRvd_Vue.toString(),"99.999.999.999",2,2,',');}
    public String fgetUdd()        {return f.Formato(GtRvd_Udd.toString(),"99.999.999.999",2,2,',');}
    public String fgetUdn()        {return f.Formato(GtRvd_Udn.toString(),"99.999.999.999",2,2,',');}
    public String fgetPum()        {return f.Formato(GtRvd_Pum.toString(),"99.999.999.999",4,4,',');}
    public String fgetVtt()        {return f.Formato(GtRvd_Vtt.toString(),"99.999.999.999",4,4,',');}
    public String fgetVts()        {return f.Formato(GtRvd_Vts.toString(),"99.999.999.999",2,2,',');}
    public String fgetNtt()        {return f.Formato(GtRvd_Ntt.toString(),"99.999.999.999",4,4,',');}
    public String fgetNts()        {return f.Formato(GtRvd_Nts.toString(),"99.999.999.999",2,2,',');}

    public String f4getPum()       {return f.Formato(GtRvd_Pum.toString(),"99.999.999.999",4,4,',');}
    public String f4getVtt()       {return f.Formato(GtRvd_Vtt.toString(),"99.999.999.999",4,4,',');}
    public String f4getNtt()       {return f.Formato(GtRvd_Ntt.toString(),"99.999.999.999",4,4,',');}
    public String f2getPum()       {return f.Formato(GtRvd_Pum.toString(),"99.999.999.999",4,2,',');}
    public String f2getVtt()       {return f.Formato(GtRvd_Vtt.toString(),"99.999.999.999",4,2,',');}
    public String f2getVts()       {return f.Formato(GtRvd_Vts.toString(),"99.999.999.999",2,2,',');}
    public String f2getNtt()       {return f.Formato(GtRvd_Ntt.toString(),"99.999.999.999",4,2,',');}
    public String f2getNts()       {return f.Formato(GtRvd_Nts.toString(),"99.999.999.999",2,2,',');}
    public String f0getPum()       {return f.Formato(GtRvd_Pum.toString(),"99.999.999.999",4,0,',');}
    public String f0getVtt()       {return f.Formato(GtRvd_Vtt.toString(),"99.999.999.999",4,0,',');}
    public String f0getVts()       {return f.Formato(GtRvd_Vts.toString(),"99.999.999.999",2,0,',');}
    public String f0getNtt()       {return f.Formato(GtRvd_Ntt.toString(),"99.999.999.999",4,0,',');}
    public String f0getNts()       {return f.Formato(GtRvd_Nts.toString(),"99.999.999.999",2,0,',');}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_Img_GtRvd LSet_A_ImgGtRvd(String pcStr)
  {
    Buf_Img_GtRvd l_Img = new Buf_Img_GtRvd();
    Vector vDatos = LSet_A_vImgGtRvd(pcStr);
    l_Img = (Buf_Img_GtRvd)vDatos.elementAt(0);
    return l_Img;
  }
  //---------------------------------------------------------------------------------------
  public static Vector LSet_A_vImgGtRvd(String pcStr)
  {
    int p=0;
    Vector vec= new Vector();
    Buf_Img_GtRvd t = new Buf_Img_GtRvd();
    t.GtRvd_Seq.replace(0,t.GtRvd_Seq.capacity(), pcStr.substring( p , p + t.GtRvd_Seq.capacity() ) );p = p + t.GtRvd_Seq.capacity();
    t.GtRvd_Itb.replace(0,t.GtRvd_Itb.capacity(), pcStr.substring( p , p + t.GtRvd_Itb.capacity() ) );p = p + t.GtRvd_Itb.capacity();
    t.GtRvd_Ide.replace(0,t.GtRvd_Ide.capacity(), pcStr.substring( p , p + t.GtRvd_Ide.capacity() ) );p = p + t.GtRvd_Ide.capacity();
    t.GtRvd_Dsb.replace(0,t.GtRvd_Dsb.capacity(), pcStr.substring( p , p + t.GtRvd_Dsb.capacity() ) );p = p + t.GtRvd_Dsb.capacity();
    t.GtRvd_Umd.replace(0,t.GtRvd_Umd.capacity(), pcStr.substring( p , p + t.GtRvd_Umd.capacity() ) );p = p + t.GtRvd_Umd.capacity();
    t.GtRvd_Pum.replace(0,t.GtRvd_Pum.capacity(), pcStr.substring( p , p + t.GtRvd_Pum.capacity() ) );p = p + t.GtRvd_Pum.capacity();
    t.GtRvd_Udd.replace(0,t.GtRvd_Udd.capacity(), pcStr.substring( p , p + t.GtRvd_Udd.capacity() ) );p = p + t.GtRvd_Udd.capacity();
    t.GtRvd_Vtt.replace(0,t.GtRvd_Vtt.capacity(), pcStr.substring( p , p + t.GtRvd_Vtt.capacity() ) );p = p + t.GtRvd_Vtt.capacity();
    t.GtRvd_Frv.replace(0,t.GtRvd_Frv.capacity(), pcStr.substring( p , p + t.GtRvd_Frv.capacity() ) );p = p + t.GtRvd_Frv.capacity();
    t.GtRvd_Vur.replace(0,t.GtRvd_Vur.capacity(), pcStr.substring( p , p + t.GtRvd_Vur.capacity() ) );p = p + t.GtRvd_Vur.capacity();
    t.GtRvd_Vts.replace(0,t.GtRvd_Vts.capacity(), pcStr.substring( p , p + t.GtRvd_Vts.capacity() ) );p = p + t.GtRvd_Vts.capacity();
    t.GtRvd_Fvt.replace(0,t.GtRvd_Fvt.capacity(), pcStr.substring( p , p + t.GtRvd_Fvt.capacity() ) );p = p + t.GtRvd_Fvt.capacity();
    t.GtRvd_Fre.replace(0,t.GtRvd_Fre.capacity(), pcStr.substring( p , p + t.GtRvd_Fre.capacity() ) );p = p + t.GtRvd_Fre.capacity();
    t.GtRvd_Vue.replace(0,t.GtRvd_Vue.capacity(), pcStr.substring( p , p + t.GtRvd_Vue.capacity() ) );p = p + t.GtRvd_Vue.capacity();
    t.GtRvd_Nvt.replace(0,t.GtRvd_Nvt.capacity(), pcStr.substring( p , p + t.GtRvd_Nvt.capacity() ) );p = p + t.GtRvd_Nvt.capacity();
    t.GtRvd_Ntt.replace(0,t.GtRvd_Ntt.capacity(), pcStr.substring( p , p + t.GtRvd_Ntt.capacity() ) );p = p + t.GtRvd_Ntt.capacity();
    t.GtRvd_Nts.replace(0,t.GtRvd_Nts.capacity(), pcStr.substring( p , p + t.GtRvd_Nts.capacity() ) );p = p + t.GtRvd_Nts.capacity();
    t.GtRvd_Idr.replace(0,t.GtRvd_Idr.capacity(), pcStr.substring( p , p + t.GtRvd_Idr.capacity() ) );p = p + t.GtRvd_Idr.capacity();
    t.GtRvd_Udn.replace(0,t.GtRvd_Udn.capacity(), pcStr.substring( p , p + t.GtRvd_Udn.capacity() ) );p = p + t.GtRvd_Udn.capacity();
    t.GtRvd_Pct.replace(0,t.GtRvd_Pct.capacity(), pcStr.substring( p , p + t.GtRvd_Pct.capacity() ) );p = p + t.GtRvd_Pct.capacity();
    t.GtRvd_Fll.replace(0,t.GtRvd_Fll.capacity(), pcStr.substring( p , p + t.GtRvd_Fll.capacity() ) );p = p + t.GtRvd_Fll.capacity();
    vec.add ( t );
    return vec;
  }
  //---------------------------------------------------------------------------------------
  public String LSet_De_ImgGtRvd(Buf_Img_GtRvd p_Img)
  {
    String pcStr = "";
    pcStr = pcStr + p_Img.getSeq();
    pcStr = pcStr + p_Img.getItb();
    pcStr = pcStr + p_Img.getIde();
    pcStr = pcStr + p_Img.getDsb();
    pcStr = pcStr + p_Img.getUmd();
    pcStr = pcStr + p_Img.getPum();
    pcStr = pcStr + p_Img.getUdd();
    pcStr = pcStr + p_Img.getVtt();
    pcStr = pcStr + p_Img.getFrv();
    pcStr = pcStr + p_Img.getVur();
    pcStr = pcStr + p_Img.getVts();
    pcStr = pcStr + p_Img.getFvt();
    pcStr = pcStr + p_Img.getFre();
    pcStr = pcStr + p_Img.getVue();
    pcStr = pcStr + p_Img.getNvt();
    pcStr = pcStr + p_Img.getNtt();
    pcStr = pcStr + p_Img.getNts();
    pcStr = pcStr + p_Img.getIdr();
    pcStr = pcStr + p_Img.getUdn();
    pcStr = pcStr + p_Img.getPct();
    pcStr = pcStr + p_Img.getFll();
    return pcStr;
  }
  //===============================================================================================================================
  public static class Buf_Img_GtAnu
  {
    public StringBuffer GtAnu_Bse = new StringBuffer(358);  //X(358)     358 Area Base Fija
    public StringBuffer GtAnu_Fum = new StringBuffer(8  );  //9(08)      366 Fecha UMovto
    public StringBuffer GtAnu_Tum = new StringBuffer(7  );  //9(07)      373 Transaccion UMovto
    public StringBuffer GtAnu_Ttr = new StringBuffer(5  );  //X(05)      378 CodTrans UMovto
    public StringBuffer GtAnu_Ntt = new StringBuffer(30 );  //X(30)      408 DscTrans UMovto
    public StringBuffer GtAnu_Ggr = new StringBuffer(3  );  //X(03)      411 Grupo Garantia (HYP/CRH/AYF/CTW/DMN/DMX/PMN/PMX)
    public StringBuffer GtAnu_Fll = new StringBuffer(416);  //X(416)     827 Disponible

    public String getBse()         {return GtAnu_Bse.toString();}
    public String getFum()         {return GtAnu_Fum.toString();}
    public String getTum()         {return GtAnu_Tum.toString();}
    public String getTtr()         {return GtAnu_Ttr.toString();}
    public String getNtt()         {return GtAnu_Ntt.toString();}
    public String getGgr()         {return GtAnu_Ggr.toString();}
    public String getFll()         {return GtAnu_Fll.toString();}

    public Formateo f = new Formateo();
    public String fgetFum()        {return f.FormatDate(GtAnu_Fum.toString());}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_Img_GtAnu LSet_A_ImgGtAnu(String pcStr)
  {
    Buf_Img_GtAnu l_Img = new Buf_Img_GtAnu();
    Vector vDatos = LSet_A_vImgGtAnu(pcStr);
    l_Img = (Buf_Img_GtAnu)vDatos.elementAt(0);
    return l_Img;
  }
  //---------------------------------------------------------------------------------------
  public static Vector LSet_A_vImgGtAnu(String pcStr)
  {
    int p=0;
    Vector vec= new Vector();
    Buf_Img_GtAnu t = new Buf_Img_GtAnu();
    t.GtAnu_Bse.replace(0,t.GtAnu_Bse.capacity(), pcStr.substring( p , p + t.GtAnu_Bse.capacity() ) );p = p + t.GtAnu_Bse.capacity();
    t.GtAnu_Fum.replace(0,t.GtAnu_Fum.capacity(), pcStr.substring( p , p + t.GtAnu_Fum.capacity() ) );p = p + t.GtAnu_Fum.capacity();
    t.GtAnu_Tum.replace(0,t.GtAnu_Tum.capacity(), pcStr.substring( p , p + t.GtAnu_Tum.capacity() ) );p = p + t.GtAnu_Tum.capacity();
    t.GtAnu_Ttr.replace(0,t.GtAnu_Ttr.capacity(), pcStr.substring( p , p + t.GtAnu_Ttr.capacity() ) );p = p + t.GtAnu_Ttr.capacity();
    t.GtAnu_Ntt.replace(0,t.GtAnu_Ntt.capacity(), pcStr.substring( p , p + t.GtAnu_Ntt.capacity() ) );p = p + t.GtAnu_Ntt.capacity();
    t.GtAnu_Ggr.replace(0,t.GtAnu_Ggr.capacity(), pcStr.substring( p , p + t.GtAnu_Ggr.capacity() ) );p = p + t.GtAnu_Ggr.capacity();
    t.GtAnu_Fll.replace(0,t.GtAnu_Fll.capacity(), pcStr.substring( p , p + t.GtAnu_Fll.capacity() ) );p = p + t.GtAnu_Fll.capacity();
    vec.add ( t );
    return vec;
  }
  //---------------------------------------------------------------------------------------
  public String LSet_De_ImgGtAnu(Buf_Img_GtAnu p_Img)
  {
    String pcStr = "";
    pcStr = pcStr + p_Img.getBse();
    pcStr = pcStr + p_Img.getFum();
    pcStr = pcStr + p_Img.getTum();
    pcStr = pcStr + p_Img.getTtr();
    pcStr = pcStr + p_Img.getNtt();
    pcStr = pcStr + p_Img.getGgr();
    pcStr = pcStr + p_Img.getFll();
    return pcStr;
  }
  //===============================================================================================================================
  //CambioOficina: BASE
  public static class Buf_Img_GtCof
  {
    public StringBuffer GtCof_Bse = new StringBuffer(358);  //X(358)     358 Area Base Fija
    public StringBuffer GtCof_Fot = new StringBuffer(8  );  //9(08)      366 Fecha Escritura
    public StringBuffer GtCof_Sin = new StringBuffer(15 );  //9(11)V9(4) 381 Saldo Moneda Origen
    public StringBuffer GtCof_Cro = new StringBuffer(11 );  //9(09)V9(2) 392 VConversion Origen
    public StringBuffer GtCof_Ncp = new StringBuffer(3  );  //9(03)      395 NCuotas Pagadas
    public StringBuffer GtCof_Ntc = new StringBuffer(3  );  //9(03)      398 NCuotas Total
    public StringBuffer GtCof_Fmd = new StringBuffer(8  );  //9(08)      406 Fecha Modificacion
    public StringBuffer GtCof_Suc = new StringBuffer(3  );  //9(03)      409 Suc Actual
    public StringBuffer GtCof_Nsu = new StringBuffer(15 );  //X(15)      424 Nombre Suc Actual
    public StringBuffer GtCof_Nof = new StringBuffer(3  );  //9(03)      427 Suc Nueva
    public StringBuffer GtCof_Nno = new StringBuffer(15 );  //X(15)      442 Nombre Suc Nueva
    public StringBuffer GtCof_Snp = new StringBuffer(15 );  //9(13)V9(2) 457 Saldo Nominal MO
    public StringBuffer GtCof_Ggr = new StringBuffer(3  );  //X(03)      460 Grupo Garantia (HYP/CRH/AYF/CTW/DMN/DMX/PMN/PMX)
    public StringBuffer GtCof_Dsg = new StringBuffer(100);  //X(100)     560 Descripcion Garantia
    public StringBuffer GtCof_Fll = new StringBuffer(267);  //X(267)     827 Disponible

    public String getBse()         {return GtCof_Bse.toString();}
    public String getFot()         {return GtCof_Fot.toString();}
    public String getSin()         {return GtCof_Sin.toString();}
    public String getCro()         {return GtCof_Cro.toString();}
    public String getNcp()         {return GtCof_Ncp.toString();}
    public String getNtc()         {return GtCof_Ntc.toString();}
    public String getFmd()         {return GtCof_Fmd.toString();}
    public String getSuc()         {return GtCof_Suc.toString();}
    public String getNsu()         {return GtCof_Nsu.toString();}
    public String getNof()         {return GtCof_Nof.toString();}
    public String getNno()         {return GtCof_Nno.toString();}
    public String getSnp()         {return GtCof_Snp.toString();}
    public String getGgr()         {return GtCof_Ggr.toString();}
    public String getDsg()         {return GtCof_Dsg.toString();}
    public String getFll()         {return GtCof_Fll.toString();}

    public Formateo f = new Formateo();
    public String fgetFot()        {return f.FormatDate(GtCof_Fot.toString());}
    public String fgetFmd()        {return f.FormatDate(GtCof_Fmd.toString());}
    public String fgetCro()        {return f.Formato(GtCof_Cro.toString(),"999.999.999",2,2,',');}
    public String fgetSin()        {return f.Formato(GtCof_Sin.toString(),"99.999.999.999",4,4,',');}

    public String egetSin()        {return f.Formato(GtCof_Sin.toString(),"99.999.999.999",4,2,',');}

    public String ggetSin()        {return f.Formato(GtCof_Sin.toString(),"99.999.999.999",4,0,',');}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_Img_GtCof LSet_A_ImgGtCof(String pcStr)
  {
    Buf_Img_GtCof l_Img = new Buf_Img_GtCof();
    Vector vDatos = LSet_A_vImgGtCof(pcStr);
    l_Img = (Buf_Img_GtCof)vDatos.elementAt(0);
    return l_Img;
  }
  //---------------------------------------------------------------------------------------
  public static Vector LSet_A_vImgGtCof(String pcStr)
  {
    int p=0;
    Vector vec= new Vector();
    Buf_Img_GtCof t = new Buf_Img_GtCof();
    t.GtCof_Bse.replace(0,t.GtCof_Bse.capacity(), pcStr.substring( p , p + t.GtCof_Bse.capacity() ) );p = p + t.GtCof_Bse.capacity();
    t.GtCof_Fot.replace(0,t.GtCof_Fot.capacity(), pcStr.substring( p , p + t.GtCof_Fot.capacity() ) );p = p + t.GtCof_Fot.capacity();
    t.GtCof_Sin.replace(0,t.GtCof_Sin.capacity(), pcStr.substring( p , p + t.GtCof_Sin.capacity() ) );p = p + t.GtCof_Sin.capacity();
    t.GtCof_Cro.replace(0,t.GtCof_Cro.capacity(), pcStr.substring( p , p + t.GtCof_Cro.capacity() ) );p = p + t.GtCof_Cro.capacity();
    t.GtCof_Ncp.replace(0,t.GtCof_Ncp.capacity(), pcStr.substring( p , p + t.GtCof_Ncp.capacity() ) );p = p + t.GtCof_Ncp.capacity();
    t.GtCof_Ntc.replace(0,t.GtCof_Ntc.capacity(), pcStr.substring( p , p + t.GtCof_Ntc.capacity() ) );p = p + t.GtCof_Ntc.capacity();
    t.GtCof_Fmd.replace(0,t.GtCof_Fmd.capacity(), pcStr.substring( p , p + t.GtCof_Fmd.capacity() ) );p = p + t.GtCof_Fmd.capacity();
    t.GtCof_Suc.replace(0,t.GtCof_Suc.capacity(), pcStr.substring( p , p + t.GtCof_Suc.capacity() ) );p = p + t.GtCof_Suc.capacity();
    t.GtCof_Nsu.replace(0,t.GtCof_Nsu.capacity(), pcStr.substring( p , p + t.GtCof_Nsu.capacity() ) );p = p + t.GtCof_Nsu.capacity();
    t.GtCof_Nof.replace(0,t.GtCof_Nof.capacity(), pcStr.substring( p , p + t.GtCof_Nof.capacity() ) );p = p + t.GtCof_Nof.capacity();
    t.GtCof_Nno.replace(0,t.GtCof_Nno.capacity(), pcStr.substring( p , p + t.GtCof_Nno.capacity() ) );p = p + t.GtCof_Nno.capacity();
    t.GtCof_Snp.replace(0,t.GtCof_Snp.capacity(), pcStr.substring( p , p + t.GtCof_Snp.capacity() ) );p = p + t.GtCof_Snp.capacity();
    t.GtCof_Ggr.replace(0,t.GtCof_Ggr.capacity(), pcStr.substring( p , p + t.GtCof_Ggr.capacity() ) );p = p + t.GtCof_Ggr.capacity();
    t.GtCof_Dsg.replace(0,t.GtCof_Dsg.capacity(), pcStr.substring( p , p + t.GtCof_Dsg.capacity() ) );p = p + t.GtCof_Dsg.capacity();
    t.GtCof_Fll.replace(0,t.GtCof_Fll.capacity(), pcStr.substring( p , p + t.GtCof_Fll.capacity() ) );p = p + t.GtCof_Fll.capacity();
    vec.add ( t );
    return vec;
  }
  //---------------------------------------------------------------------------------------
  public String LSet_De_ImgGtCof(Buf_Img_GtCof p_Img)
  {
    String pcStr = "";
    pcStr = pcStr + p_Img.getBse();
    pcStr = pcStr + p_Img.getFot();
    pcStr = pcStr + p_Img.getSin();
    pcStr = pcStr + p_Img.getCro();
    pcStr = pcStr + p_Img.getNcp();
    pcStr = pcStr + p_Img.getNtc();
    pcStr = pcStr + p_Img.getFmd();
    pcStr = pcStr + p_Img.getSuc();
    pcStr = pcStr + p_Img.getNsu();
    pcStr = pcStr + p_Img.getNof();
    pcStr = pcStr + p_Img.getNno();
    pcStr = pcStr + p_Img.getSnp();
    pcStr = pcStr + p_Img.getGgr();
    pcStr = pcStr + p_Img.getDsg();
    pcStr = pcStr + p_Img.getFll();
    return pcStr;
  }
  //===============================================================================================================================
  //CambioProducto: BASE
  public static class Buf_Img_GtCpd
  {
    public StringBuffer GtCpd_Bse = new StringBuffer(358);  //X(358)     358 Area Base Fija
    public StringBuffer GtCpd_Fot = new StringBuffer(8  );  //9(08)      366 Fecha Escritura
    public StringBuffer GtCpd_Sin = new StringBuffer(15 );  //9(11)V9(4) 381 Saldo Moneda Origen
    public StringBuffer GtCpd_Cro = new StringBuffer(11 );  //9(09)V9(2) 392 VConversion Origen
    public StringBuffer GtCpd_Ncp = new StringBuffer(3  );  //9(03)      395 NCuotas Pagadas
    public StringBuffer GtCpd_Ntc = new StringBuffer(3  );  //9(03)      398 NCuotas Total
    public StringBuffer GtCpd_Fmd = new StringBuffer(8  );  //9(08)      406 Fecha Modificacion
    public StringBuffer GtCpd_Dcn = new StringBuffer(5  );  //X(05)      411 DCN Actual
    public StringBuffer GtCpd_Dpc = new StringBuffer(10 );  //X(10)      421 DPC Actual
    public StringBuffer GtCpd_Npc = new StringBuffer(30 );  //X(30)      451 Nombre DPC Actual
    public StringBuffer GtCpd_Ndc = new StringBuffer(5  );  //X(05)      456 DCN Nuevo
    public StringBuffer GtCpd_Ndp = new StringBuffer(10 );  //X(10)      466 DPC Nuevo
    public StringBuffer GtCpd_Nnp = new StringBuffer(30 );  //X(30)      496 Nombre DPC Nuevo
    public StringBuffer GtCpd_Snp = new StringBuffer(15 );  //9(13)V9(2) 511 Saldo Nominal MO
    public StringBuffer GtCpd_Ggr = new StringBuffer(3  );  //X(03)      514 Grupo Garantia (HYP/CRH/AYF/CTW/DMN/DMX/PMN/PMX)
    public StringBuffer GtCpd_Dsg = new StringBuffer(100);  //X(100)     614 Descripcion Garantia
    public StringBuffer GtCpd_Seq = new StringBuffer(3  );  //9(03)      617 GBS Seq
    public StringBuffer GtCpd_Fll = new StringBuffer(210);  //X(210)     827 Disponible

    public String getBse()         {return GtCpd_Bse.toString();}
    public String getFot()         {return GtCpd_Fot.toString();}
    public String getSin()         {return GtCpd_Sin.toString();}
    public String getCro()         {return GtCpd_Cro.toString();}
    public String getNcp()         {return GtCpd_Ncp.toString();}
    public String getNtc()         {return GtCpd_Ntc.toString();}
    public String getFmd()         {return GtCpd_Fmd.toString();}
    public String getDcn()         {return GtCpd_Dcn.toString();}
    public String getDpc()         {return GtCpd_Dpc.toString();}
    public String getNpc()         {return GtCpd_Npc.toString();}
    public String getNdc()         {return GtCpd_Ndc.toString();}
    public String getNdp()         {return GtCpd_Ndp.toString();}
    public String getNnp()         {return GtCpd_Nnp.toString();}
    public String getSnp()         {return GtCpd_Snp.toString();}
    public String getGgr()         {return GtCpd_Ggr.toString();}
    public String getDsg()         {return GtCpd_Dsg.toString();}
    public String getSeq()         {return GtCpd_Seq.toString();}
    public String getFll()         {return GtCpd_Fll.toString();}

    public Formateo f = new Formateo();
    public String fgetFot()        {return f.FormatDate(GtCpd_Fot.toString());}
    public String fgetFmd()        {return f.FormatDate(GtCpd_Fmd.toString());}
    public String fgetCro()        {return f.Formato(GtCpd_Cro.toString(),"999.999.999",2,2,',');}
    public String fgetSin()        {return f.Formato(GtCpd_Sin.toString(),"99.999.999.999",4,4,',');}

    public String egetSin()        {return f.Formato(GtCpd_Sin.toString(),"99.999.999.999",4,2,',');}

    public String ggetSin()        {return f.Formato(GtCpd_Sin.toString(),"99.999.999.999",4,0,',');}
  }
  //-------------------------------------------------------------------------------------------
  public static Buf_Img_GtCpd LSet_A_ImgGtCpd(String pcStr)
  {
    int p=0;
    Buf_Img_GtCpd l_Img = new Buf_Img_GtCpd();
    l_Img.GtCpd_Bse.replace(0, l_Img.GtCpd_Bse.capacity(), pcStr.substring(p, p + l_Img.GtCpd_Bse.capacity())); p = p + l_Img.GtCpd_Bse.capacity();
    l_Img.GtCpd_Fot.replace(0, l_Img.GtCpd_Fot.capacity(), pcStr.substring(p, p + l_Img.GtCpd_Fot.capacity())); p = p + l_Img.GtCpd_Fot.capacity();
    l_Img.GtCpd_Sin.replace(0, l_Img.GtCpd_Sin.capacity(), pcStr.substring(p, p + l_Img.GtCpd_Sin.capacity())); p = p + l_Img.GtCpd_Sin.capacity();
    l_Img.GtCpd_Cro.replace(0, l_Img.GtCpd_Cro.capacity(), pcStr.substring(p, p + l_Img.GtCpd_Cro.capacity())); p = p + l_Img.GtCpd_Cro.capacity();
    l_Img.GtCpd_Ncp.replace(0, l_Img.GtCpd_Ncp.capacity(), pcStr.substring(p, p + l_Img.GtCpd_Ncp.capacity())); p = p + l_Img.GtCpd_Ncp.capacity();
    l_Img.GtCpd_Ntc.replace(0, l_Img.GtCpd_Ntc.capacity(), pcStr.substring(p, p + l_Img.GtCpd_Ntc.capacity())); p = p + l_Img.GtCpd_Ntc.capacity();
    l_Img.GtCpd_Fmd.replace(0, l_Img.GtCpd_Fmd.capacity(), pcStr.substring(p, p + l_Img.GtCpd_Fmd.capacity())); p = p + l_Img.GtCpd_Fmd.capacity();
    l_Img.GtCpd_Dcn.replace(0, l_Img.GtCpd_Dcn.capacity(), pcStr.substring(p, p + l_Img.GtCpd_Dcn.capacity())); p = p + l_Img.GtCpd_Dcn.capacity();
    l_Img.GtCpd_Dpc.replace(0, l_Img.GtCpd_Dpc.capacity(), pcStr.substring(p, p + l_Img.GtCpd_Dpc.capacity())); p = p + l_Img.GtCpd_Dpc.capacity();
    l_Img.GtCpd_Npc.replace(0, l_Img.GtCpd_Npc.capacity(), pcStr.substring(p, p + l_Img.GtCpd_Npc.capacity())); p = p + l_Img.GtCpd_Npc.capacity();
    l_Img.GtCpd_Ndc.replace(0, l_Img.GtCpd_Ndc.capacity(), pcStr.substring(p, p + l_Img.GtCpd_Ndc.capacity())); p = p + l_Img.GtCpd_Ndc.capacity();
    l_Img.GtCpd_Ndp.replace(0, l_Img.GtCpd_Ndp.capacity(), pcStr.substring(p, p + l_Img.GtCpd_Ndp.capacity())); p = p + l_Img.GtCpd_Ndp.capacity();
    l_Img.GtCpd_Nnp.replace(0, l_Img.GtCpd_Nnp.capacity(), pcStr.substring(p, p + l_Img.GtCpd_Nnp.capacity())); p = p + l_Img.GtCpd_Nnp.capacity();
    l_Img.GtCpd_Snp.replace(0, l_Img.GtCpd_Snp.capacity(), pcStr.substring(p, p + l_Img.GtCpd_Snp.capacity())); p = p + l_Img.GtCpd_Snp.capacity();
    l_Img.GtCpd_Ggr.replace(0, l_Img.GtCpd_Ggr.capacity(), pcStr.substring(p, p + l_Img.GtCpd_Ggr.capacity())); p = p + l_Img.GtCpd_Ggr.capacity();
    l_Img.GtCpd_Dsg.replace(0, l_Img.GtCpd_Dsg.capacity(), pcStr.substring(p, p + l_Img.GtCpd_Dsg.capacity())); p = p + l_Img.GtCpd_Dsg.capacity();
    l_Img.GtCpd_Seq.replace(0, l_Img.GtCpd_Seq.capacity(), pcStr.substring(p, p + l_Img.GtCpd_Seq.capacity())); p = p + l_Img.GtCpd_Seq.capacity();
    l_Img.GtCpd_Fll.replace(0, l_Img.GtCpd_Fll.capacity(), pcStr.substring(p, p + l_Img.GtCpd_Fll.capacity())); p = p + l_Img.GtCpd_Fll.capacity();
    return l_Img;
  }
  //-------------------------------------------------------------------------------------------
  public String LSet_De_ImgGtCpd(Buf_Img_GtCpd p_Img)
  {
    String pcStr = "";
    pcStr = pcStr + p_Img.getBse();
    pcStr = pcStr + p_Img.getFot();
    pcStr = pcStr + p_Img.getSin();
    pcStr = pcStr + p_Img.getCro();
    pcStr = pcStr + p_Img.getNcp();
    pcStr = pcStr + p_Img.getNtc();
    pcStr = pcStr + p_Img.getFmd();
    pcStr = pcStr + p_Img.getDcn();
    pcStr = pcStr + p_Img.getDpc();
    pcStr = pcStr + p_Img.getNpc();
    pcStr = pcStr + p_Img.getNdc();
    pcStr = pcStr + p_Img.getNdp();
    pcStr = pcStr + p_Img.getNnp();
    pcStr = pcStr + p_Img.getSnp();
    pcStr = pcStr + p_Img.getGgr();
    pcStr = pcStr + p_Img.getDsg();
    pcStr = pcStr + p_Img.getSeq();
    pcStr = pcStr + p_Img.getFll();
    return pcStr;
  }
  //===============================================================================================================================
  public static class Buf_Img_GtPts
  {
    public StringBuffer GtPts_Cbn = new StringBuffer(5  );  //X(05)          Cod Cuenta MFF
    public StringBuffer GtPts_Dbn = new StringBuffer(12 );  //9(12)          Nro Cuenta MFF
    public StringBuffer GtPts_Prc = new StringBuffer(6  );  //9(03)V9(3)     Porcentaje MFF
    //     ------------
    public StringBuffer GtPts_Bse = new StringBuffer(358);  //X(358)     358 Area Base Fija
    public StringBuffer GtPts_Fec = new StringBuffer(8  );  //9(08)      366 Fecha Formalizacion
    public StringBuffer GtPts_Cbp = new StringBuffer(5  );  //X(05)      371 Codigo CCargo
    public StringBuffer GtPts_Dbp = new StringBuffer(12 );  //9(12)      383 Id. Cta.Cargo
    public StringBuffer GtPts_Gts = new StringBuffer(11 );  //9(11)      394 Gasto Tasacion
    public StringBuffer GtPts_Hon = new StringBuffer(11 );  //9(11)      405 Honorarios Tasacion
    public StringBuffer GtPts_Pcb = new StringBuffer(5  );  //9(03)V9(2) 410 %Comision BCO
    public StringBuffer GtPts_Cct = new StringBuffer(12 );  //9(12)      422 Cta Cte Tasador
    public StringBuffer GtPts_Bof = new StringBuffer(1  );  //X(01)      423 Boleta o Factura
    public StringBuffer GtPts_Nbf = new StringBuffer(10 );  //9(10)      433 N� Bol/Fact
    public StringBuffer GtPts_Nts = new StringBuffer(25 );  //X(25)      458 Nombre Tasador
    public StringBuffer GtPts_Rts = new StringBuffer(10 );  //X(10)      468 RUT Tasador
    public StringBuffer GtPts_Dpc = new StringBuffer(10 );  //X(10)      478 PContable
    public Vector      vGtPts_Ctg = new Vector(15);         //X(345)     823 Tabla Cuentas Gastos
    public StringBuffer GtPts_Fll = new StringBuffer(4  );  //X(04)      827 Disponible

    public String getBse()         {return GtPts_Bse.toString();}
    public String getFec()         {return GtPts_Fec.toString();}
    public String getCbp()         {return GtPts_Cbp.toString();}
    public String getDbp()         {return GtPts_Dbp.toString();}
    public String getGts()         {return GtPts_Gts.toString();}
    public String getHon()         {return GtPts_Hon.toString();}
    public String getPcb()         {return GtPts_Pcb.toString();}
    public String getCct()         {return GtPts_Cct.toString();}
    public String getBof()         {return GtPts_Bof.toString();}
    public String getNbf()         {return GtPts_Nbf.toString();}
    public String getNts()         {return GtPts_Nts.toString();}
    public String getRts()         {return GtPts_Rts.toString();}
    public String getDpc()         {return GtPts_Dpc.toString();}
    public Vector getVecCtg()      {return vGtPts_Ctg;}
    public String getFll()         {return GtPts_Fll.toString();}

    public Formateo f = new Formateo();
    public String fgetFec()        {return f.FormatDate(GtPts_Fec.toString());}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_Img_GtPts LSet_A_ImgGtPts(String pcStr)
  {
    Buf_Img_GtPts l_Img = new Buf_Img_GtPts();
    Vector vDatos = LSet_A_vImgGtPts(pcStr);
    l_Img = (Buf_Img_GtPts)vDatos.elementAt(0);
    return l_Img;
  }
  //---------------------------------------------------------------------------------------
  public static Vector LSet_A_vImgGtPts(String pcStr)
  {
    int p=0;
    Vector vec= new Vector();
    Buf_Img_GtPts t = new Buf_Img_GtPts();
    t.GtPts_Bse.replace(0,t.GtPts_Bse.capacity(), pcStr.substring( p , p + t.GtPts_Bse.capacity() ) );p = p + t.GtPts_Bse.capacity();
    t.GtPts_Fec.replace(0,t.GtPts_Fec.capacity(), pcStr.substring( p , p + t.GtPts_Fec.capacity() ) );p = p + t.GtPts_Fec.capacity();
    t.GtPts_Cbp.replace(0,t.GtPts_Cbp.capacity(), pcStr.substring( p , p + t.GtPts_Cbp.capacity() ) );p = p + t.GtPts_Cbp.capacity();
    t.GtPts_Dbp.replace(0,t.GtPts_Dbp.capacity(), pcStr.substring( p , p + t.GtPts_Dbp.capacity() ) );p = p + t.GtPts_Dbp.capacity();
    t.GtPts_Gts.replace(0,t.GtPts_Gts.capacity(), pcStr.substring( p , p + t.GtPts_Gts.capacity() ) );p = p + t.GtPts_Gts.capacity();
    t.GtPts_Hon.replace(0,t.GtPts_Hon.capacity(), pcStr.substring( p , p + t.GtPts_Hon.capacity() ) );p = p + t.GtPts_Hon.capacity();
    t.GtPts_Pcb.replace(0,t.GtPts_Pcb.capacity(), pcStr.substring( p , p + t.GtPts_Pcb.capacity() ) );p = p + t.GtPts_Pcb.capacity();
    t.GtPts_Cct.replace(0,t.GtPts_Cct.capacity(), pcStr.substring( p , p + t.GtPts_Cct.capacity() ) );p = p + t.GtPts_Cct.capacity();
    t.GtPts_Bof.replace(0,t.GtPts_Bof.capacity(), pcStr.substring( p , p + t.GtPts_Bof.capacity() ) );p = p + t.GtPts_Bof.capacity();
    t.GtPts_Nbf.replace(0,t.GtPts_Nbf.capacity(), pcStr.substring( p , p + t.GtPts_Nbf.capacity() ) );p = p + t.GtPts_Nbf.capacity();
    t.GtPts_Nts.replace(0,t.GtPts_Nts.capacity(), pcStr.substring( p , p + t.GtPts_Nts.capacity() ) );p = p + t.GtPts_Nts.capacity();
    t.GtPts_Rts.replace(0,t.GtPts_Rts.capacity(), pcStr.substring( p , p + t.GtPts_Rts.capacity() ) );p = p + t.GtPts_Rts.capacity();
    t.GtPts_Dpc.replace(0,t.GtPts_Dpc.capacity(), pcStr.substring( p , p + t.GtPts_Dpc.capacity() ) );p = p + t.GtPts_Dpc.capacity();
    String lcData="";
    for (int i=0; i<15; i++)
        {
          lcData = pcStr.substring(p, p + t.GtPts_Cbn.capacity() ) ;
          lcData = lcData + pcStr.substring(p, p + t.GtPts_Dbn.capacity() ) ;
          lcData = lcData + pcStr.substring(p, p + t.GtPts_Prc.capacity() ) ;
          t.vGtPts_Ctg.add ( lcData );
          p = p + t.GtPts_Cbn.capacity();
          p = p + t.GtPts_Dbn.capacity();
          p = p + t.GtPts_Prc.capacity();
        }
    t.GtPts_Fll.replace(0,t.GtPts_Fll.capacity(), pcStr.substring( p , p + t.GtPts_Fll.capacity() ) );p = p + t.GtPts_Fll.capacity();
    vec.add ( t );
    return vec;
  }
  //---------------------------------------------------------------------------------------
  public String LSet_De_ImgGtPts(Buf_Img_GtPts p_Img)
  {
    String pcStr = "";
    pcStr = pcStr + p_Img.getBse();
    pcStr = pcStr + p_Img.getFec();
    pcStr = pcStr + p_Img.getCbp();
    pcStr = pcStr + p_Img.getDbp();
    pcStr = pcStr + p_Img.getGts();
    pcStr = pcStr + p_Img.getHon();
    pcStr = pcStr + p_Img.getPcb();
    pcStr = pcStr + p_Img.getCct();
    pcStr = pcStr + p_Img.getBof();
    pcStr = pcStr + p_Img.getNbf();
    pcStr = pcStr + p_Img.getNts();
    pcStr = pcStr + p_Img.getRts();
    pcStr = pcStr + p_Img.getDpc();
    String lcData ="";
    for (int i=0; i<15; i++)
        {
          lcData = ((Vector)p_Img.getVecCtg()).elementAt(i).toString();
          pcStr = pcStr + lcData;
        }
    pcStr = pcStr + p_Img.getFll();
    return pcStr;
  }
  //===============================================================================================================================
  public static class Buf_Img_GtMff
  {
    public StringBuffer GtMff_Cct = new StringBuffer(5  );  //X(05)        5 Codigo Cuenta
    public StringBuffer GtMff_Dct = new StringBuffer(12 );  //9(12)       17 Id Cuenta
    public StringBuffer GtMff_Prc = new StringBuffer(6  );  //9(03)V9(3)  23 Porcentaje
    public StringBuffer GtMff_Rut = new StringBuffer(10 );  //X(10)       33 Id. Cliente Titular
    public StringBuffer GtMff_Nom = new StringBuffer(40 );  //X(40)       73 Nombre Cliente Titular
    public StringBuffer GtMff_Fll = new StringBuffer(754);  //X(754)     827 Disponible

    public String getCct()         {return GtMff_Cct.toString();}
    public String getDct()         {return GtMff_Dct.toString();}
    public String getPrc()         {return GtMff_Prc.toString();}
    public String getRut()         {return GtMff_Rut.toString();}
    public String getNom()         {return GtMff_Nom.toString();}
    public String getFll()         {return GtMff_Fll.toString();}

    public Formateo f = new Formateo();
    public String fgetDct()        {return f.EditCta(GtMff_Dct.toString());}
    public String fgetPrc()        {return f.Formato(GtMff_Prc.toString(),"999",3,3,',');}
    public String fgetRut()        {return f.fmtRut(GtMff_Rut.toString());}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_Img_GtMff LSet_A_ImgGtMff(String pcStr)
  {
    Buf_Img_GtMff l_Img = new Buf_Img_GtMff();
    Vector vDatos = LSet_A_vImgGtMff(pcStr);
    l_Img = (Buf_Img_GtMff)vDatos.elementAt(0);
    return l_Img;
  }
  //---------------------------------------------------------------------------------------
  public static Vector LSet_A_vImgGtMff(String pcStr)
  {
    int p=0;
    Vector vec= new Vector();
    Buf_Img_GtMff t = new Buf_Img_GtMff();
    t.GtMff_Cct.replace(0,t.GtMff_Cct.capacity(), pcStr.substring( p , p + t.GtMff_Cct.capacity() ) );p = p + t.GtMff_Cct.capacity();
    t.GtMff_Dct.replace(0,t.GtMff_Dct.capacity(), pcStr.substring( p , p + t.GtMff_Dct.capacity() ) );p = p + t.GtMff_Dct.capacity();
    t.GtMff_Prc.replace(0,t.GtMff_Prc.capacity(), pcStr.substring( p , p + t.GtMff_Prc.capacity() ) );p = p + t.GtMff_Prc.capacity();
    t.GtMff_Rut.replace(0,t.GtMff_Rut.capacity(), pcStr.substring( p , p + t.GtMff_Rut.capacity() ) );p = p + t.GtMff_Rut.capacity();
    t.GtMff_Nom.replace(0,t.GtMff_Nom.capacity(), pcStr.substring( p , p + t.GtMff_Nom.capacity() ) );p = p + t.GtMff_Nom.capacity();
    t.GtMff_Fll.replace(0,t.GtMff_Fll.capacity(), pcStr.substring(p) );
    vec.add ( t );
    return vec;
  }
  //---------------------------------------------------------------------------------------
  public String LSet_De_ImgGtMff(Buf_Img_GtMff p_Img)
  {
    String pcStr = "";
    pcStr = pcStr + p_Img.getCct();
    pcStr = pcStr + p_Img.getDct();
    pcStr = pcStr + p_Img.getPrc();
    pcStr = pcStr + p_Img.getRut();
    pcStr = pcStr + p_Img.getNom();
    pcStr = pcStr + p_Img.getFll();
    return pcStr;
  }
  //===============================================================================================================================
  public static class Buf_Img_GtGfu
  {
    public StringBuffer GtGfu_Seq = new StringBuffer(3  );  //9(03)        3 Secuencia
    public StringBuffer GtGfu_Itb = new StringBuffer(2  );  //X(02)        5 Tipo Bien (GF)
    public StringBuffer GtGfu_Itg = new StringBuffer(5  );  //X(05)       10 Producto Futuro
    public StringBuffer GtGfu_Dpc = new StringBuffer(10 );  //X(10)       20 Producto Contable
    public StringBuffer GtGfu_Pct = new StringBuffer(5  );  //9(03)V9(2)  25 %Castigo SBIF Valores
    public StringBuffer GtGfu_Dsb = new StringBuffer(100);  //X(100)     125 Descripcion Bien
    public StringBuffer GtGfu_Fvt = new StringBuffer(8  );  //9(08)      133 FEstimada Termino
    public StringBuffer GtGfu_Vln = new StringBuffer(15 );  //9(11)V9(4) 148 Valor Nominal <TRJ>
    public StringBuffer GtGfu_Vlc = new StringBuffer(15 );  //9(13)V9(2) 163 Valor Contable
    public StringBuffer GtGfu_Dob = new StringBuffer(9  );  //9(09)      172 @Observaciones
    public StringBuffer GtGfu_Umt = new StringBuffer(3  );  //X(03)      175 UMonetaria ($/UF/MND)
    public StringBuffer GtGfu_Udc = new StringBuffer(1  );  //9(01)      176 Decimales (0/2/4)
    public StringBuffer GtGfu_Vum = new StringBuffer(9  );  //9(07)V9(2) 185 Valor UMonetaria FTS
    public StringBuffer GtGfu_Fll = new StringBuffer(642);  //X(642)     827 Disponible

    public String getSeq()         {return GtGfu_Seq.toString();}
    public String getItb()         {return GtGfu_Itb.toString();}
    public String getItg()         {return GtGfu_Itg.toString();}
    public String getDpc()         {return GtGfu_Dpc.toString();}
    public String getPct()         {return GtGfu_Pct.toString();}
    public String getDsb()         {return GtGfu_Dsb.toString();}
    public String getFvt()         {return GtGfu_Fvt.toString();}
    public String getVln()         {return GtGfu_Vln.toString();}
    public String getVlc()         {return GtGfu_Vlc.toString();}
    public String getDob()         {return GtGfu_Dob.toString();}
    public String getUmt()         {return GtGfu_Umt.toString();}
    public String getUdc()         {return GtGfu_Udc.toString();}
    public String getVum()         {return GtGfu_Vum.toString();}
    public String getFll()         {return GtGfu_Fll.toString();}

    public Formateo f = new Formateo();
    public int    igetUdc()        {if (getUdc().trim().equals("")) return 0; else return Integer.parseInt(getUdc()); }
    public String fgetFvt()        {
                                    if (getFvt().trim().equals("10010101"))
                                       { return "S/D"; }
                                    else if (getFvt().trim().equals("10020101"))
                                       { return "SinFecha"; }
                                    else
                                       { return f.FormatDate(getFvt()); }
                                   }
    public String fgetPct()        {return f.Formato(getPct(),"999",2,2,',');}
    public String fgetVum()        {return f.Formato(getVum(),"99.999",2,2,',');}
    public String fgetVln(int Dcu) {return f.Formato(getVln(),"99.999.999.999",4,Dcu,',');}
    public String fgetVlc(int Dcm) {return f.Formato(getVlc(),"99.999.999.999",2,Dcm,',');}

    public double ngetVlc()        {if (getVlc().trim().equals("")) return 0; else return Double.parseDouble(getVlc());}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_Img_GtGfu LSet_A_ImgGtGfu(String pcStr)
  {
    Buf_Img_GtGfu l_Img = new Buf_Img_GtGfu();
    Vector vDatos = LSet_A_vImgGtGfu(pcStr);
    l_Img = (Buf_Img_GtGfu)vDatos.elementAt(0);
    return l_Img;
  }
  //---------------------------------------------------------------------------------------
  public static Vector LSet_A_vImgGtGfu(String pcStr)
  {
    int p=0;
    Vector vec= new Vector();
    Buf_Img_GtGfu t = new Buf_Img_GtGfu();
    t.GtGfu_Seq.replace(0,t.GtGfu_Seq.capacity(), pcStr.substring( p , p + t.GtGfu_Seq.capacity() ) );p = p + t.GtGfu_Seq.capacity();
    t.GtGfu_Itb.replace(0,t.GtGfu_Itb.capacity(), pcStr.substring( p , p + t.GtGfu_Itb.capacity() ) );p = p + t.GtGfu_Itb.capacity();
    t.GtGfu_Itg.replace(0,t.GtGfu_Itg.capacity(), pcStr.substring( p , p + t.GtGfu_Itg.capacity() ) );p = p + t.GtGfu_Itg.capacity();
    t.GtGfu_Dpc.replace(0,t.GtGfu_Dpc.capacity(), pcStr.substring( p , p + t.GtGfu_Dpc.capacity() ) );p = p + t.GtGfu_Dpc.capacity();
    t.GtGfu_Pct.replace(0,t.GtGfu_Pct.capacity(), pcStr.substring( p , p + t.GtGfu_Pct.capacity() ) );p = p + t.GtGfu_Pct.capacity();
    t.GtGfu_Dsb.replace(0,t.GtGfu_Dsb.capacity(), pcStr.substring( p , p + t.GtGfu_Dsb.capacity() ) );p = p + t.GtGfu_Dsb.capacity();
    t.GtGfu_Fvt.replace(0,t.GtGfu_Fvt.capacity(), pcStr.substring( p , p + t.GtGfu_Fvt.capacity() ) );p = p + t.GtGfu_Fvt.capacity();
    t.GtGfu_Vln.replace(0,t.GtGfu_Vln.capacity(), pcStr.substring( p , p + t.GtGfu_Vln.capacity() ) );p = p + t.GtGfu_Vln.capacity();
    t.GtGfu_Vlc.replace(0,t.GtGfu_Vlc.capacity(), pcStr.substring( p , p + t.GtGfu_Vlc.capacity() ) );p = p + t.GtGfu_Vlc.capacity();
    t.GtGfu_Dob.replace(0,t.GtGfu_Dob.capacity(), pcStr.substring( p , p + t.GtGfu_Dob.capacity() ) );p = p + t.GtGfu_Dob.capacity();
    t.GtGfu_Umt.replace(0,t.GtGfu_Umt.capacity(), pcStr.substring( p , p + t.GtGfu_Umt.capacity() ) );p = p + t.GtGfu_Umt.capacity();
    t.GtGfu_Udc.replace(0,t.GtGfu_Udc.capacity(), pcStr.substring( p , p + t.GtGfu_Udc.capacity() ) );p = p + t.GtGfu_Udc.capacity();
    t.GtGfu_Vum.replace(0,t.GtGfu_Vum.capacity(), pcStr.substring( p , p + t.GtGfu_Vum.capacity() ) );p = p + t.GtGfu_Vum.capacity();
    t.GtGfu_Fll.replace(0,t.GtGfu_Fll.capacity(), pcStr.substring( p , p + t.GtGfu_Fll.capacity() ) );p = p + t.GtGfu_Fll.capacity();
    vec.add ( t );
    return vec;
  }
  //---------------------------------------------------------------------------------------
  public String LSet_De_ImgGtGfu(Buf_Img_GtGfu p_Img)
  {
    String pcStr = "";
    pcStr = pcStr + p_Img.getSeq();
    pcStr = pcStr + p_Img.getItb();
    pcStr = pcStr + p_Img.getItg();
    pcStr = pcStr + p_Img.getDpc();
    pcStr = pcStr + p_Img.getPct();
    pcStr = pcStr + p_Img.getDsb();
    pcStr = pcStr + p_Img.getFvt();
    pcStr = pcStr + p_Img.getVln();
    pcStr = pcStr + p_Img.getVlc();
    pcStr = pcStr + p_Img.getDob();
    pcStr = pcStr + p_Img.getUmt();
    pcStr = pcStr + p_Img.getUdc();
    pcStr = pcStr + p_Img.getVum();
    pcStr = pcStr + p_Img.getFll();
    return pcStr;
  }
  //===============================================================================================================================
}