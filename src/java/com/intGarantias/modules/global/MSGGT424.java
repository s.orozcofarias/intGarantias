// Source File Name:   MSGGT424.java

package com.intGarantias.modules.global;

import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.util.Formateo;

import java.util.*;
import java.lang.*;
import org.apache.turbine.util.Log;

public class MSGGT424
{
  //---------------------------------------------------------------------------------------
  public static class Buf_MsgGT424
  {
    public StringBuffer GT424_Idr = new StringBuffer(1  );   //X(01)     
    public StringBuffer GT424_Gti = new StringBuffer(10 );   //X(10)     
    public StringBuffer GT424_Seq = new StringBuffer(3  );   //9(03)     
    public StringBuffer GT424_Sqd = new StringBuffer(3  );   //9(03)     
    public StringBuffer GT424_Fts = new StringBuffer(8  );   //9(08)     
    public StringBuffer GT424_Vuf = new StringBuffer(9  );   //9(07)V9(2)
    public StringBuffer GT424_Vtc = new StringBuffer(9  );   //9(07)V9(2)
    public StringBuffer GT424_Tuf = new StringBuffer(15 );   //9(11)V9(4)
    public StringBuffer GT424_Ttc = new StringBuffer(15 );   //9(13)V9(2)
    public StringBuffer GT424_Tps = new StringBuffer(15 );   //9(13)V9(2)
    public StringBuffer GT424_Ith = new StringBuffer(2  );   //X(02)     
    public StringBuffer GT424_Dsh = new StringBuffer(30 );   //X(30)     
    public StringBuffer GT424_Rs1 = new StringBuffer(5  );   //9(05)     
    public StringBuffer GT424_Rs2 = new StringBuffer(3  );   //9(03)     
    public StringBuffer GT424_Rds = new StringBuffer(20 );   //X(20)     
    public StringBuffer GT424_Faf = new StringBuffer(8  );   //9(08)     
    public StringBuffer GT424_Vaf = new StringBuffer(15 );   //9(13)V9(2)
    public StringBuffer GT424_Udd = new StringBuffer(9  );   //9(07)V9(2)
    public StringBuffer GT424_Umd = new StringBuffer(2  );   //X(02)     
    public StringBuffer GT424_Umt = new StringBuffer(1  );   //X(01)     
    public StringBuffer GT424_Pum = new StringBuffer(15 );   //9(11)V9(4)
    public StringBuffer GT424_Vtu = new StringBuffer(15 );   //9(11)V9(4)
    public StringBuffer GT424_Vbu = new StringBuffer(15 );   //9(11)V9(4)
    public StringBuffer GT424_Plq = new StringBuffer(5  );   //9(03)V9(2)
    public StringBuffer GT424_Vlu = new StringBuffer(15 );   //9(11)V9(4)
    public StringBuffer GT424_Vsu = new StringBuffer(15 );   //9(11)V9(4)
    public StringBuffer GT424_Tsu = new StringBuffer(15 );   //X(15)     
    public StringBuffer GT424_Dag = new StringBuffer(1  );   //X(01)     
    public StringBuffer GT424_Dsr = new StringBuffer(1  );   //X(01)     
    public StringBuffer GT424_Obt = new StringBuffer(9  );   //9(09)
    public StringBuffer GT424_Ovb = new StringBuffer(9  );   //9(09)

    public String getIdr()         {return GT424_Idr.toString();}
    public String getGti()         {return GT424_Gti.toString();}
    public String getSeq()         {return GT424_Seq.toString();}
    public String getSqd()         {return GT424_Sqd.toString();}
    public String getFts()         {return GT424_Fts.toString();}
    public String getVuf()         {return GT424_Vuf.toString();}
    public String getVtc()         {return GT424_Vtc.toString();}
    public String getTuf()         {return GT424_Tuf.toString();}
    public String getTtc()         {return GT424_Ttc.toString();}
    public String getTps()         {return GT424_Tps.toString();}
    public String getIth()         {return GT424_Ith.toString();}
    public String getDsh()         {return GT424_Dsh.toString();}
    public String getRs1()         {return GT424_Rs1.toString();}
    public String getRs2()         {return GT424_Rs2.toString();}
    public String getRds()         {return GT424_Rds.toString();}
    public String getFaf()         {return GT424_Faf.toString();}
    public String getVaf()         {return GT424_Vaf.toString();}
    public String getUdd()         {return GT424_Udd.toString();}
    public String getUmd()         {return GT424_Umd.toString();}
    public String getUmt()         {return GT424_Umt.toString();}
    public String getPum()         {return GT424_Pum.toString();}
    public String getVtu()         {return GT424_Vtu.toString();}
    public String getVbu()         {return GT424_Vbu.toString();}
    public String getPlq()         {return GT424_Plq.toString();}
    public String getVlu()         {return GT424_Vlu.toString();}
    public String getVsu()         {return GT424_Vsu.toString();}
    public String getTsu()         {return GT424_Tsu.toString();}
    public String getDag()         {return GT424_Dag.toString();}
    public String getDsr()         {return GT424_Dsr.toString();}
    public String getObt()         {return GT424_Obt.toString();}
    public String getOvb()         {return GT424_Ovb.toString();}

    public Formateo f = new Formateo();
    public String fgetFts()        {return f.FormatDate(getFts());}
    public String fgetVuf()        {return f.Formato(getVuf(),"9.999.999",2,2,',');}
    public String fgetVtc()        {return f.Formato(getVtc(),"9.999.999",2,2,',');}
    public String fgetTuf()        {return f.Formato(getTuf(),"99.999.999.999",4,4,',');}
    public String fgetTtc()        {return f.Formato(getTtc(),"99.999.999.999",2,2,',');}
    public String fgetTps()        {return f.Formato(getTps(),"99.999.999.999",2,0,',');}
    public String fgetFaf()        {return f.FormatDate(getFaf());}
    public String fgetVaf()        {return f.Formato(getVaf(),"9.999.999.999.999",2,0,',');}
    public String fgetUdd()        {return f.Formato(getUdd(),"9.999.999",2,2,',');}
    public String fgetPum()
                                   {
                                     int Dec = 0;
                                     if (getUmt().equals("P"))  { Dec = 0; }
                                     if (getUmt().equals("U"))  { Dec = 4; }
                                     if (getUmt().equals("D"))  { Dec = 2; }
                                     return f.Formato(getPum(),"99.999.999.999",4,Dec,',');
                                   }
    public String fgetVtu()
                                   {
                                     int Dec = 0;
                                     if (getUmt().equals("P"))  { Dec = 0; }
                                     if (getUmt().equals("U"))  { Dec = 4; }
                                     if (getUmt().equals("D"))  { Dec = 2; }
                                     return f.Formato(getVtu(),"99.999.999.999",4,Dec,',');
                                   }
    public String fgetVbu()
                                   {
                                     int Dec = 0;
                                     if (getUmt().equals("P"))  { Dec = 0; }
                                     if (getUmt().equals("U"))  { Dec = 4; }
                                     if (getUmt().equals("D"))  { Dec = 2; }
                                     return f.Formato(getVbu(),"99.999.999.999",4,Dec,',');
                                   }
    public String fgetPlq()        {return f.Formato(getPlq(),"999",2,2,',');}
    public String fgetVlu()
                                   {
                                     int Dec = 0;
                                     if (getUmt().equals("P"))  { Dec = 0; }
                                     if (getUmt().equals("U"))  { Dec = 4; }
                                     if (getUmt().equals("D"))  { Dec = 2; }
                                     return f.Formato(getVlu(),"99.999.999.999",4,Dec,',');
                                   }
    public String fgetVsu()
                                   {
                                     int Dec = 0;
                                     if (getUmt().equals("P"))  { Dec = 0; }
                                     if (getUmt().equals("U"))  { Dec = 4; }
                                     if (getUmt().equals("D"))  { Dec = 2; }
                                     return f.Formato(getVsu(),"99.999.999.999",4,Dec,',');
                                   }
  }
  //---------------------------------------------------------------------------------------
  public static Buf_MsgGT424 Inicia_MsgGT424 ()
  {
    Buf_MsgGT424 l_MsgGT424 = new Buf_MsgGT424();
    l_MsgGT424.GT424_Idr.replace(0, l_MsgGT424.GT424_Idr.capacity(), RUTGEN.Blancos(1  ));
    l_MsgGT424.GT424_Gti.replace(0, l_MsgGT424.GT424_Gti.capacity(), RUTGEN.Blancos(10 ));
    l_MsgGT424.GT424_Seq.replace(0, l_MsgGT424.GT424_Seq.capacity(), RUTGEN.Blancos(3  ));
    l_MsgGT424.GT424_Sqd.replace(0, l_MsgGT424.GT424_Sqd.capacity(), RUTGEN.Blancos(3  ));
    l_MsgGT424.GT424_Fts.replace(0, l_MsgGT424.GT424_Fts.capacity(), RUTGEN.Blancos(8  ));
    l_MsgGT424.GT424_Vuf.replace(0, l_MsgGT424.GT424_Vuf.capacity(), RUTGEN.Blancos(9  ));
    l_MsgGT424.GT424_Vtc.replace(0, l_MsgGT424.GT424_Vtc.capacity(), RUTGEN.Blancos(9  ));
    l_MsgGT424.GT424_Tuf.replace(0, l_MsgGT424.GT424_Tuf.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT424.GT424_Ttc.replace(0, l_MsgGT424.GT424_Ttc.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT424.GT424_Tps.replace(0, l_MsgGT424.GT424_Tps.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT424.GT424_Ith.replace(0, l_MsgGT424.GT424_Ith.capacity(), RUTGEN.Blancos(2  ));
    l_MsgGT424.GT424_Dsh.replace(0, l_MsgGT424.GT424_Dsh.capacity(), RUTGEN.Blancos(30 ));
    l_MsgGT424.GT424_Rs1.replace(0, l_MsgGT424.GT424_Rs1.capacity(), RUTGEN.Blancos(5  ));
    l_MsgGT424.GT424_Rs2.replace(0, l_MsgGT424.GT424_Rs2.capacity(), RUTGEN.Blancos(3  ));
    l_MsgGT424.GT424_Rds.replace(0, l_MsgGT424.GT424_Rds.capacity(), RUTGEN.Blancos(20 ));
    l_MsgGT424.GT424_Faf.replace(0, l_MsgGT424.GT424_Faf.capacity(), RUTGEN.Blancos(8  ));
    l_MsgGT424.GT424_Vaf.replace(0, l_MsgGT424.GT424_Vaf.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT424.GT424_Udd.replace(0, l_MsgGT424.GT424_Udd.capacity(), RUTGEN.Blancos(9  ));
    l_MsgGT424.GT424_Umd.replace(0, l_MsgGT424.GT424_Umd.capacity(), RUTGEN.Blancos(2  ));
    l_MsgGT424.GT424_Umt.replace(0, l_MsgGT424.GT424_Umt.capacity(), RUTGEN.Blancos(1  ));
    l_MsgGT424.GT424_Pum.replace(0, l_MsgGT424.GT424_Pum.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT424.GT424_Vtu.replace(0, l_MsgGT424.GT424_Vtu.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT424.GT424_Vbu.replace(0, l_MsgGT424.GT424_Vbu.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT424.GT424_Plq.replace(0, l_MsgGT424.GT424_Plq.capacity(), RUTGEN.Blancos(5  ));
    l_MsgGT424.GT424_Vlu.replace(0, l_MsgGT424.GT424_Vlu.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT424.GT424_Vsu.replace(0, l_MsgGT424.GT424_Vsu.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT424.GT424_Tsu.replace(0, l_MsgGT424.GT424_Tsu.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT424.GT424_Dag.replace(0, l_MsgGT424.GT424_Dag.capacity(), RUTGEN.Blancos(1  ));
    l_MsgGT424.GT424_Dsr.replace(0, l_MsgGT424.GT424_Dsr.capacity(), RUTGEN.Blancos(1  ));
    l_MsgGT424.GT424_Obt.replace(0, l_MsgGT424.GT424_Obt.capacity(), RUTGEN.Blancos(9  ));
    l_MsgGT424.GT424_Ovb.replace(0, l_MsgGT424.GT424_Ovb.capacity(), RUTGEN.Blancos(9  ));
    return l_MsgGT424;
  }
  //---------------------------------------------------------------------------------------
  public static Buf_MsgGT424 LSet_A_MsgGT424(String pcStr)
  {
    Buf_MsgGT424 l_MsgGT424 = new Buf_MsgGT424();
    Vector vDatos = LSet_A_vMsgGT424(pcStr);
    l_MsgGT424 = (Buf_MsgGT424)vDatos.elementAt(0);
    return l_MsgGT424;
  }
  //---------------------------------------------------------------------------------------
  public static Vector LSet_A_vMsgGT424(String pcStr)
  {
    int p = 0;
    Vector vec = new Vector();
    Buf_MsgGT424 l_MsgGT424 = new Buf_MsgGT424();
    l_MsgGT424.GT424_Idr.replace(0, l_MsgGT424.GT424_Idr.capacity(), pcStr.substring(p, p + l_MsgGT424.GT424_Idr.capacity())); p = p + l_MsgGT424.GT424_Idr.capacity();
    l_MsgGT424.GT424_Gti.replace(0, l_MsgGT424.GT424_Gti.capacity(), pcStr.substring(p, p + l_MsgGT424.GT424_Gti.capacity())); p = p + l_MsgGT424.GT424_Gti.capacity();
    l_MsgGT424.GT424_Seq.replace(0, l_MsgGT424.GT424_Seq.capacity(), pcStr.substring(p, p + l_MsgGT424.GT424_Seq.capacity())); p = p + l_MsgGT424.GT424_Seq.capacity();
    l_MsgGT424.GT424_Sqd.replace(0, l_MsgGT424.GT424_Sqd.capacity(), pcStr.substring(p, p + l_MsgGT424.GT424_Sqd.capacity())); p = p + l_MsgGT424.GT424_Sqd.capacity();
    l_MsgGT424.GT424_Fts.replace(0, l_MsgGT424.GT424_Fts.capacity(), pcStr.substring(p, p + l_MsgGT424.GT424_Fts.capacity())); p = p + l_MsgGT424.GT424_Fts.capacity();
    l_MsgGT424.GT424_Vuf.replace(0, l_MsgGT424.GT424_Vuf.capacity(), pcStr.substring(p, p + l_MsgGT424.GT424_Vuf.capacity())); p = p + l_MsgGT424.GT424_Vuf.capacity();
    l_MsgGT424.GT424_Vtc.replace(0, l_MsgGT424.GT424_Vtc.capacity(), pcStr.substring(p, p + l_MsgGT424.GT424_Vtc.capacity())); p = p + l_MsgGT424.GT424_Vtc.capacity();
    l_MsgGT424.GT424_Tuf.replace(0, l_MsgGT424.GT424_Tuf.capacity(), pcStr.substring(p, p + l_MsgGT424.GT424_Tuf.capacity())); p = p + l_MsgGT424.GT424_Tuf.capacity();
    l_MsgGT424.GT424_Ttc.replace(0, l_MsgGT424.GT424_Ttc.capacity(), pcStr.substring(p, p + l_MsgGT424.GT424_Ttc.capacity())); p = p + l_MsgGT424.GT424_Ttc.capacity();
    l_MsgGT424.GT424_Tps.replace(0, l_MsgGT424.GT424_Tps.capacity(), pcStr.substring(p, p + l_MsgGT424.GT424_Tps.capacity())); p = p + l_MsgGT424.GT424_Tps.capacity();
    l_MsgGT424.GT424_Ith.replace(0, l_MsgGT424.GT424_Ith.capacity(), pcStr.substring(p, p + l_MsgGT424.GT424_Ith.capacity())); p = p + l_MsgGT424.GT424_Ith.capacity();
    l_MsgGT424.GT424_Dsh.replace(0, l_MsgGT424.GT424_Dsh.capacity(), pcStr.substring(p, p + l_MsgGT424.GT424_Dsh.capacity())); p = p + l_MsgGT424.GT424_Dsh.capacity();
    l_MsgGT424.GT424_Rs1.replace(0, l_MsgGT424.GT424_Rs1.capacity(), pcStr.substring(p, p + l_MsgGT424.GT424_Rs1.capacity())); p = p + l_MsgGT424.GT424_Rs1.capacity();
    l_MsgGT424.GT424_Rs2.replace(0, l_MsgGT424.GT424_Rs2.capacity(), pcStr.substring(p, p + l_MsgGT424.GT424_Rs2.capacity())); p = p + l_MsgGT424.GT424_Rs2.capacity();
    l_MsgGT424.GT424_Rds.replace(0, l_MsgGT424.GT424_Rds.capacity(), pcStr.substring(p, p + l_MsgGT424.GT424_Rds.capacity())); p = p + l_MsgGT424.GT424_Rds.capacity();
    l_MsgGT424.GT424_Faf.replace(0, l_MsgGT424.GT424_Faf.capacity(), pcStr.substring(p, p + l_MsgGT424.GT424_Faf.capacity())); p = p + l_MsgGT424.GT424_Faf.capacity();
    l_MsgGT424.GT424_Vaf.replace(0, l_MsgGT424.GT424_Vaf.capacity(), pcStr.substring(p, p + l_MsgGT424.GT424_Vaf.capacity())); p = p + l_MsgGT424.GT424_Vaf.capacity();
    l_MsgGT424.GT424_Udd.replace(0, l_MsgGT424.GT424_Udd.capacity(), pcStr.substring(p, p + l_MsgGT424.GT424_Udd.capacity())); p = p + l_MsgGT424.GT424_Udd.capacity();
    l_MsgGT424.GT424_Umd.replace(0, l_MsgGT424.GT424_Umd.capacity(), pcStr.substring(p, p + l_MsgGT424.GT424_Umd.capacity())); p = p + l_MsgGT424.GT424_Umd.capacity();
    l_MsgGT424.GT424_Umt.replace(0, l_MsgGT424.GT424_Umt.capacity(), pcStr.substring(p, p + l_MsgGT424.GT424_Umt.capacity())); p = p + l_MsgGT424.GT424_Umt.capacity();
    l_MsgGT424.GT424_Pum.replace(0, l_MsgGT424.GT424_Pum.capacity(), pcStr.substring(p, p + l_MsgGT424.GT424_Pum.capacity())); p = p + l_MsgGT424.GT424_Pum.capacity();
    l_MsgGT424.GT424_Vtu.replace(0, l_MsgGT424.GT424_Vtu.capacity(), pcStr.substring(p, p + l_MsgGT424.GT424_Vtu.capacity())); p = p + l_MsgGT424.GT424_Vtu.capacity();
    l_MsgGT424.GT424_Vbu.replace(0, l_MsgGT424.GT424_Vbu.capacity(), pcStr.substring(p, p + l_MsgGT424.GT424_Vbu.capacity())); p = p + l_MsgGT424.GT424_Vbu.capacity();
    l_MsgGT424.GT424_Plq.replace(0, l_MsgGT424.GT424_Plq.capacity(), pcStr.substring(p, p + l_MsgGT424.GT424_Plq.capacity())); p = p + l_MsgGT424.GT424_Plq.capacity();
    l_MsgGT424.GT424_Vlu.replace(0, l_MsgGT424.GT424_Vlu.capacity(), pcStr.substring(p, p + l_MsgGT424.GT424_Vlu.capacity())); p = p + l_MsgGT424.GT424_Vlu.capacity();
    l_MsgGT424.GT424_Vsu.replace(0, l_MsgGT424.GT424_Vsu.capacity(), pcStr.substring(p, p + l_MsgGT424.GT424_Vsu.capacity())); p = p + l_MsgGT424.GT424_Vsu.capacity();
    l_MsgGT424.GT424_Tsu.replace(0, l_MsgGT424.GT424_Tsu.capacity(), pcStr.substring(p, p + l_MsgGT424.GT424_Tsu.capacity())); p = p + l_MsgGT424.GT424_Tsu.capacity();
    l_MsgGT424.GT424_Dag.replace(0, l_MsgGT424.GT424_Dag.capacity(), pcStr.substring(p, p + l_MsgGT424.GT424_Dag.capacity())); p = p + l_MsgGT424.GT424_Dag.capacity();
    l_MsgGT424.GT424_Dsr.replace(0, l_MsgGT424.GT424_Dsr.capacity(), pcStr.substring(p, p + l_MsgGT424.GT424_Dsr.capacity())); p = p + l_MsgGT424.GT424_Dsr.capacity();
    l_MsgGT424.GT424_Obt.replace(0, l_MsgGT424.GT424_Obt.capacity(), pcStr.substring(p, p + l_MsgGT424.GT424_Obt.capacity())); p = p + l_MsgGT424.GT424_Obt.capacity();
    l_MsgGT424.GT424_Ovb.replace(0, l_MsgGT424.GT424_Ovb.capacity(), pcStr.substring(p, p + l_MsgGT424.GT424_Ovb.capacity())); p = p + l_MsgGT424.GT424_Ovb.capacity();
    vec.add(l_MsgGT424);
    return vec;
  }
  //---------------------------------------------------------------------------------------
  public static String LSet_De_MsgGT424 (Buf_MsgGT424 p_MsgGT424)
  {
    String pcStr = "";
    pcStr = pcStr + p_MsgGT424.GT424_Idr.toString();
    pcStr = pcStr + p_MsgGT424.GT424_Gti.toString();
    pcStr = pcStr + p_MsgGT424.GT424_Seq.toString();
    pcStr = pcStr + p_MsgGT424.GT424_Sqd.toString();
    pcStr = pcStr + p_MsgGT424.GT424_Fts.toString();
    pcStr = pcStr + p_MsgGT424.GT424_Vuf.toString();
    pcStr = pcStr + p_MsgGT424.GT424_Vtc.toString();
    pcStr = pcStr + p_MsgGT424.GT424_Tuf.toString();
    pcStr = pcStr + p_MsgGT424.GT424_Ttc.toString();
    pcStr = pcStr + p_MsgGT424.GT424_Tps.toString();
    pcStr = pcStr + p_MsgGT424.GT424_Ith.toString();
    pcStr = pcStr + p_MsgGT424.GT424_Dsh.toString();
    pcStr = pcStr + p_MsgGT424.GT424_Rs1.toString();
    pcStr = pcStr + p_MsgGT424.GT424_Rs2.toString();
    pcStr = pcStr + p_MsgGT424.GT424_Rds.toString();
    pcStr = pcStr + p_MsgGT424.GT424_Faf.toString();
    pcStr = pcStr + p_MsgGT424.GT424_Vaf.toString();
    pcStr = pcStr + p_MsgGT424.GT424_Udd.toString();
    pcStr = pcStr + p_MsgGT424.GT424_Umd.toString();
    pcStr = pcStr + p_MsgGT424.GT424_Umt.toString();
    pcStr = pcStr + p_MsgGT424.GT424_Pum.toString();
    pcStr = pcStr + p_MsgGT424.GT424_Vtu.toString();
    pcStr = pcStr + p_MsgGT424.GT424_Vbu.toString();
    pcStr = pcStr + p_MsgGT424.GT424_Plq.toString();
    pcStr = pcStr + p_MsgGT424.GT424_Vlu.toString();
    pcStr = pcStr + p_MsgGT424.GT424_Vsu.toString();
    pcStr = pcStr + p_MsgGT424.GT424_Tsu.toString();
    pcStr = pcStr + p_MsgGT424.GT424_Dag.toString();
    pcStr = pcStr + p_MsgGT424.GT424_Dsr.toString();
    pcStr = pcStr + p_MsgGT424.GT424_Obt.toString();
    pcStr = pcStr + p_MsgGT424.GT424_Ovb.toString();
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
}