// Source File Name:   MSGGT453.java

package com.intGarantias.modules.global;

import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.util.Formateo;

import java.util.*;
import java.lang.*;
import org.apache.turbine.util.Log;

public class MSGGT453
{
  public static int g_Max_GT453 = 99;
  //---------------------------------------------------------------------------------------
  public static class Bff_MsgGT453
  {
    public StringBuffer GT453_Cre = new StringBuffer(22);  //'X(22)       22 Id. Credito  
    public StringBuffer GT453_Fdt = new StringBuffer(8 );  //'9(08)       30 Fecha Relacion 

    public String getCre()          {return GT453_Cre.toString();}
    public String getFdt()          {return GT453_Fdt.toString();}

    public Formateo f = new Formateo();
    public String fgetCre()         {return getCre().substring(0,3) + "-" + getCre().substring(3,10);}
    public String fgetFdt()         {return f.FormatDate(GT453_Fdt.toString());}
    public String bgetCre()         {return getCre().substring(0,3) + "-" + getCre().substring(3,10);}
  }
  //---------------------------------------------------------------------------------------
  public static class Buf_MsgGT453
  {
    public StringBuffer GT453_Idr = new StringBuffer(1 );  //'X(01)        1 Idr Host        
    public StringBuffer GT453_Gti = new StringBuffer(10);  //'X(10)       11 Id. Garantia    
    public StringBuffer GT453_Nro = new StringBuffer(3 );  //'9(03)       14 Total Indices   
    public Vector       GT453_Mod = new Vector();          //'X(2970)   2984 Tabla Detalle   

    public String getIdr(){return GT453_Idr.toString();}
    public String getGti(){return GT453_Gti.toString();}
    public String getNro(){return GT453_Nro.toString();}
    public Vector getMod(){return GT453_Mod;}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_MsgGT453 Inicia_MsgGT453()
  {
    Buf_MsgGT453 l_MsgGT453 = new Buf_MsgGT453();
    l_MsgGT453.GT453_Idr.replace(0,l_MsgGT453.GT453_Idr.capacity(), RUTGEN.Blancos(1 ));
    l_MsgGT453.GT453_Gti.replace(0,l_MsgGT453.GT453_Gti.capacity(), RUTGEN.Blancos(10));
    l_MsgGT453.GT453_Nro.replace(0,l_MsgGT453.GT453_Nro.capacity(), RUTGEN.Blancos(3 ));
    int p=0;
    for(int i=0; i<g_Max_GT453; i++)
    {
      Bff_MsgGT453 Tap = new Bff_MsgGT453();
      Tap.GT453_Cre.replace(0,Tap.GT453_Cre.capacity(), RUTGEN.Blancos(22));
      Tap.GT453_Fdt.replace(0,Tap.GT453_Fdt.capacity(), RUTGEN.Blancos(8 ));
      l_MsgGT453.GT453_Mod.add(Tap);
    }
    return l_MsgGT453;
  }
  //---------------------------------------------------------------------------------------
  static public Buf_MsgGT453 LSet_A_MsgGT453(String pcStr)
  {
    Buf_MsgGT453 l_MsgGT453 = new Buf_MsgGT453();
    MSGGT453 vMsgGT453 = new MSGGT453();
    Vector vDatos = vMsgGT453.LSet_A_vMsgGT453(pcStr);
    l_MsgGT453 = (MSGGT453.Buf_MsgGT453)vDatos.elementAt(0);
    return l_MsgGT453;
  }
  //---------------------------------------------------------------------------------------
  public Vector LSet_A_vMsgGT453(String pcStr)
  {
    Vector vec = new Vector();
    int p = 0;
    Buf_MsgGT453 l_MsgGT453 = new Buf_MsgGT453();	    		    	
    l_MsgGT453.GT453_Idr.append(pcStr.substring(p, p + l_MsgGT453.GT453_Idr.capacity())); p = p + l_MsgGT453.GT453_Idr.capacity();
    l_MsgGT453.GT453_Gti.append(pcStr.substring(p, p + l_MsgGT453.GT453_Gti.capacity())); p = p + l_MsgGT453.GT453_Gti.capacity();
    l_MsgGT453.GT453_Nro.append(pcStr.substring(p, p + l_MsgGT453.GT453_Nro.capacity())); p = p + l_MsgGT453.GT453_Nro.capacity();
    for (int i=0; i<Integer.parseInt(l_MsgGT453.GT453_Nro.toString()); i++)
        {
          Bff_MsgGT453 Tap = new Bff_MsgGT453();
          Tap.GT453_Cre.append(pcStr.substring(p, p + Tap.GT453_Cre.capacity())); p = p + Tap.GT453_Cre.capacity();
          Tap.GT453_Fdt.append(pcStr.substring(p, p + Tap.GT453_Fdt.capacity())); p = p + Tap.GT453_Fdt.capacity();
          l_MsgGT453.GT453_Mod.add(Tap);
        }
    vec.add (l_MsgGT453);	    	
    return vec;
  }
  //---------------------------------------------------------------------------------------
  static public String LSet_De_MsgGT453(Buf_MsgGT453 p_MsgGT453)
  {
    String pcStr = "";
    pcStr = pcStr + p_MsgGT453.GT453_Idr.toString();
    pcStr = pcStr + p_MsgGT453.GT453_Gti.toString();
    pcStr = pcStr + p_MsgGT453.GT453_Nro.toString();
    for(int i=0; i<p_MsgGT453.GT453_Mod.size(); i++)
    {
      pcStr = pcStr + ((Bff_MsgGT453)p_MsgGT453.GT453_Mod.elementAt(i)).GT453_Cre.toString();
      pcStr = pcStr + ((Bff_MsgGT453)p_MsgGT453.GT453_Mod.elementAt(i)).GT453_Fdt.toString();
    }
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
}