// Source File Name:   MSGGT450.java

package com.intGarantias.modules.global;

import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.util.Formateo;

import java.util.*;
import java.lang.*;
import org.apache.turbine.util.Log;

public class MSGGT450
{
  //---------------------------------------------------------------------------------------
  public static class Buf_MsgGT450
  {
    public StringBuffer GT450_Idr = new StringBuffer(1  );    //X(01)        1 Inicio/Especifico/Avanza/Retrocede
    public StringBuffer GT450_Gti = new StringBuffer(10 );    //X(10)       11 Operacion a Buscar
    public StringBuffer GT450_Swt = new StringBuffer(1  );    //9(01)       12 Retorno
    public StringBuffer GT450_Men = new StringBuffer(8  );    //X(08)       20 MensajeError
    public StringBuffer GT450_Cli = new StringBuffer(10 );    //X(10)       30 Id.Cliente
    public StringBuffer GT450_Ncl = new StringBuffer(40 );    //X(40)       70 Nom.Cliente
    public StringBuffer GT450_Ggr = new StringBuffer(3  );    //X(03)       73 Producto
    public StringBuffer GT450_Dcn = new StringBuffer(5  );    //X(05)       78 Producto
    public StringBuffer GT450_Ndc = new StringBuffer(30 );    //X(30)      108 Desc.Producto
    public StringBuffer GT450_Suc = new StringBuffer(3  );    //9(03)      111 Sucursal
    public StringBuffer GT450_Nsu = new StringBuffer(15 );    //X(15)      126 Nom.Sucursal
    public StringBuffer GT450_Eje = new StringBuffer(4  );    //9(04)      130 Ejecutivo
    public StringBuffer GT450_Nej = new StringBuffer(25 );    //X(25)      155 Nom.Ejecutivo
    public StringBuffer GT450_Tej = new StringBuffer(10 );    //X(10)      165 Fono.Ejecutivo
    public StringBuffer GT450_Tmn = new StringBuffer(3  );    //X(03)      168 Tipo Moneda
    public StringBuffer GT450_Trj = new StringBuffer(3  );    //X(03)      171 Reajustabilidad
    public StringBuffer GT450_Dcm = new StringBuffer(1  );    //9(01)      172 Decimales
    public StringBuffer GT450_Fco = new StringBuffer(8  );    //9(08)      180 Fecha Apertura
    public StringBuffer GT450_Mnd = new StringBuffer(3  );    //9(03)      183 Moneda
    public StringBuffer GT450_Nmn = new StringBuffer(15 );    //X(15)      198 Nom.Moneda
    public StringBuffer GT450_Vco = new StringBuffer(15 );    //9(11)V9(4) 213 Valor
    public StringBuffer GT450_Vcn = new StringBuffer(15 );    //9(13)V9(2) 228 Valor Actualizado
    public StringBuffer GT450_Fcn = new StringBuffer(8  );    //9(08)      236 Fecha Actualiza
    public StringBuffer GT450_Cbt = new StringBuffer(1  );    //X(01)      237 Cobertura
    public StringBuffer GT450_Est = new StringBuffer(5  );    //X(05)      242 Estado
    public StringBuffer GT450_Dpc = new StringBuffer(10 );    //X(10)      252 PContable
    public StringBuffer GT450_Dir = new StringBuffer(107);    //X(107)     359 Direccion Cliente
    public StringBuffer GT450_Tfc = new StringBuffer(10 );    //X(10)      369 Telefono Cliente
    public StringBuffer GT450_Dmc = new StringBuffer(107);    //X(107)     476 Domicilio Garantia
    public StringBuffer GT450_Dsg = new StringBuffer(100);    //X(100)     576 Desc.Garantia
    public StringBuffer GT450_Ilm = new StringBuffer(1  );    //X(01)      577 Limitacion
    public StringBuffer GT450_Mtl = new StringBuffer(15 );    //9(13)V9(2) 592 Limite Valor
    public StringBuffer GT450_Pcl = new StringBuffer(5  );    //9(03)V9(2) 597 Limite Porcentual
    public StringBuffer GT450_Prp = new StringBuffer(5  );    //X(05)      602 Propietario
    public StringBuffer GT450_Tsd = new StringBuffer(3  );    //9(03)      605 Perito
    public StringBuffer GT450_Nts = new StringBuffer(30 );    //X(30)      635 Nombre Perito
    public StringBuffer GT450_Mve = new StringBuffer(15 );    //9(13)V9(2) 650 Valor Economico
    public Vector       GT450_Tab = new Vector();

    public String getIdr()          {return GT450_Idr.toString();}
    public String getGti()          {return GT450_Gti.toString();}
    public String getSwt()          {return GT450_Swt.toString();}
    public String getMen()          {return GT450_Men.toString();}
    public String getCli()          {return GT450_Cli.toString();}
    public String getNcl()          {return GT450_Ncl.toString();}
    public String getGgr()          {return GT450_Ggr.toString();}
    public String getDcn()          {return GT450_Dcn.toString();}
    public String getNdc()          {return GT450_Ndc.toString();}
    public String getSuc()          {return GT450_Suc.toString();}
    public String getNsu()          {return GT450_Nsu.toString();}
    public String getEje()          {return GT450_Eje.toString();}
    public String getNej()          {return GT450_Nej.toString();}
    public String getTej()          {return GT450_Tej.toString();}
    public String getTmn()          {return GT450_Tmn.toString();}
    public String getTrj()          {return GT450_Trj.toString();}
    public String getDcm()          {return GT450_Dcm.toString();}
    public String getFco()          {return GT450_Fco.toString();}
    public String getMnd()          {return GT450_Mnd.toString();}
    public String getNmn()          {return GT450_Nmn.toString();}
    public String getVco()          {return GT450_Vco.toString();}
    public String getVcn()          {return GT450_Vcn.toString();}
    public String getFcn()          {return GT450_Fcn.toString();}
    public String getCbt()          {return GT450_Cbt.toString();}
    public String getEst()          {return GT450_Est.toString();}
    public String getDpc()          {return GT450_Dpc.toString();}
    public String getDir()          {return GT450_Dir.toString();}
    public String getTfc()          {return GT450_Tfc.toString();}
    public String getDmc()          {return GT450_Dmc.toString();}
    public String getDsg()          {return GT450_Dsg.toString();}
    public String getIlm()          {return GT450_Ilm.toString();}
    public String getMtl()          {return GT450_Mtl.toString();}
    public String getPcl()          {return GT450_Pcl.toString();}
    public String getPrp()          {return GT450_Prp.toString();}
    public String getTsd()          {return GT450_Tsd.toString();}
    public String getNts()          {return GT450_Nts.toString();}
    public String getMve()          {return GT450_Mve.toString();}

    public Vector getTab()          {return GT450_Tab; }

    public Formateo f = new Formateo();
    public String fgetCli()         {return f.fmtRut(GT450_Cli.toString());}
    public String fgetGti()         {return getGti().substring(0,3) + "-" + getGti().substring(3,10);}
    public String fgetFco()         {return f.FormatDate(GT450_Fco.toString());}
    public String fgetFcn()         {return f.FormatDate(GT450_Fcn.toString());}
    public String fgetVco()         {return f.Formato(GT450_Vco.toString(),"99.999.999.999",4,Integer.parseInt(GT450_Dcm.toString()),',');}
    public String fgetVcn()         {return f.Formato(GT450_Vcn.toString(),"99.999.999.999",2,2,',');}
    public String fgetMtl()         {
                                      if (Long.parseLong(getMtl()) != 0)
                                         {return f.Formato(getMtl(),"99.999.999.999",2,2,',');}
                                      else
                                         {return "SinLValor"; }
                                    }
    public String fgetPcl()         {
                                      if (Long.parseLong(getPcl()) != 0)
                                         {return f.Formato(getPcl(),"999",2,2,',');}
                                      else
                                         {return "SinL%"; }
                                    }
    public String fgetMve()         {return f.Formato(GT450_Mve.toString(),"99.999.999.999",2,2,',');}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_MsgGT450 Inicia_MsgGT450()
  {
    Buf_MsgGT450 l_MsgGT450 = new Buf_MsgGT450();
    l_MsgGT450.GT450_Idr.replace(0,l_MsgGT450.GT450_Idr.capacity(), RUTGEN.Blancos(1  ));
    l_MsgGT450.GT450_Gti.replace(0,l_MsgGT450.GT450_Gti.capacity(), RUTGEN.Blancos(10 ));
    l_MsgGT450.GT450_Swt.replace(0,l_MsgGT450.GT450_Swt.capacity(), RUTGEN.Blancos(1  ));
    l_MsgGT450.GT450_Men.replace(0,l_MsgGT450.GT450_Men.capacity(), RUTGEN.Blancos(8  ));
    l_MsgGT450.GT450_Cli.replace(0,l_MsgGT450.GT450_Cli.capacity(), RUTGEN.Blancos(10 ));
    l_MsgGT450.GT450_Ncl.replace(0,l_MsgGT450.GT450_Ncl.capacity(), RUTGEN.Blancos(40 ));
    l_MsgGT450.GT450_Ggr.replace(0,l_MsgGT450.GT450_Ggr.capacity(), RUTGEN.Blancos(3  ));
    l_MsgGT450.GT450_Dcn.replace(0,l_MsgGT450.GT450_Dcn.capacity(), RUTGEN.Blancos(5  ));
    l_MsgGT450.GT450_Ndc.replace(0,l_MsgGT450.GT450_Ndc.capacity(), RUTGEN.Blancos(30 ));
    l_MsgGT450.GT450_Suc.replace(0,l_MsgGT450.GT450_Suc.capacity(), RUTGEN.Blancos(3  ));
    l_MsgGT450.GT450_Nsu.replace(0,l_MsgGT450.GT450_Nsu.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT450.GT450_Eje.replace(0,l_MsgGT450.GT450_Eje.capacity(), RUTGEN.Blancos(4  ));
    l_MsgGT450.GT450_Nej.replace(0,l_MsgGT450.GT450_Nej.capacity(), RUTGEN.Blancos(25 ));
    l_MsgGT450.GT450_Tej.replace(0,l_MsgGT450.GT450_Tej.capacity(), RUTGEN.Blancos(10 ));
    l_MsgGT450.GT450_Tmn.replace(0,l_MsgGT450.GT450_Tmn.capacity(), RUTGEN.Blancos(3  ));
    l_MsgGT450.GT450_Trj.replace(0,l_MsgGT450.GT450_Trj.capacity(), RUTGEN.Blancos(3  ));
    l_MsgGT450.GT450_Dcm.replace(0,l_MsgGT450.GT450_Dcm.capacity(), RUTGEN.Blancos(1  ));
    l_MsgGT450.GT450_Fco.replace(0,l_MsgGT450.GT450_Fco.capacity(), RUTGEN.Blancos(8  ));
    l_MsgGT450.GT450_Mnd.replace(0,l_MsgGT450.GT450_Mnd.capacity(), RUTGEN.Blancos(3  ));
    l_MsgGT450.GT450_Nmn.replace(0,l_MsgGT450.GT450_Nmn.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT450.GT450_Vco.replace(0,l_MsgGT450.GT450_Vco.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT450.GT450_Vcn.replace(0,l_MsgGT450.GT450_Vcn.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT450.GT450_Fcn.replace(0,l_MsgGT450.GT450_Fcn.capacity(), RUTGEN.Blancos(8  ));
    l_MsgGT450.GT450_Cbt.replace(0,l_MsgGT450.GT450_Cbt.capacity(), RUTGEN.Blancos(1  ));
    l_MsgGT450.GT450_Est.replace(0,l_MsgGT450.GT450_Est.capacity(), RUTGEN.Blancos(5  ));
    l_MsgGT450.GT450_Dpc.replace(0,l_MsgGT450.GT450_Dpc.capacity(), RUTGEN.Blancos(10 ));
    l_MsgGT450.GT450_Dir.replace(0,l_MsgGT450.GT450_Dir.capacity(), RUTGEN.Blancos(107));
    l_MsgGT450.GT450_Tfc.replace(0,l_MsgGT450.GT450_Tfc.capacity(), RUTGEN.Blancos(10 ));
    l_MsgGT450.GT450_Dmc.replace(0,l_MsgGT450.GT450_Dmc.capacity(), RUTGEN.Blancos(107));
    l_MsgGT450.GT450_Dsg.replace(0,l_MsgGT450.GT450_Dsg.capacity(), RUTGEN.Blancos(100));
    l_MsgGT450.GT450_Ilm.replace(0,l_MsgGT450.GT450_Ilm.capacity(), RUTGEN.Blancos(1  ));
    l_MsgGT450.GT450_Mtl.replace(0,l_MsgGT450.GT450_Mtl.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT450.GT450_Pcl.replace(0,l_MsgGT450.GT450_Pcl.capacity(), RUTGEN.Blancos(5  ));
    l_MsgGT450.GT450_Prp.replace(0,l_MsgGT450.GT450_Prp.capacity(), RUTGEN.Blancos(5  ));
    l_MsgGT450.GT450_Tsd.replace(0,l_MsgGT450.GT450_Tsd.capacity(), RUTGEN.Blancos(3  ));
    l_MsgGT450.GT450_Nts.replace(0,l_MsgGT450.GT450_Nts.capacity(), RUTGEN.Blancos(30 ));
    l_MsgGT450.GT450_Mve.replace(0,l_MsgGT450.GT450_Mve.capacity(), RUTGEN.Blancos(15 ));
    return l_MsgGT450;
  }
  //---------------------------------------------------------------------------------------
  static public Buf_MsgGT450 LSet_A_MsgGT450 (String pcStr)
  {
    Buf_MsgGT450 l_MsgGT450 = new Buf_MsgGT450();
    MSGGT450 vMsgGT450 = new MSGGT450();
    Vector vDatos = vMsgGT450.LSet_A_vMsgGT450(pcStr);
    l_MsgGT450 = (MSGGT450.Buf_MsgGT450)vDatos.elementAt(0);
    return l_MsgGT450;
  }
  //---------------------------------------------------------------------------------------
  public Vector LSet_A_vMsgGT450(String pcStr)
  {
    int p = 0;
    Buf_MsgGT450 Tap = new Buf_MsgGT450();
    Tap.GT450_Idr.append(pcStr.substring(p, p + Tap.GT450_Idr.capacity())); p = p + Tap.GT450_Idr.capacity();
    Tap.GT450_Gti.append(pcStr.substring(p, p + Tap.GT450_Gti.capacity())); p = p + Tap.GT450_Gti.capacity();
    Tap.GT450_Swt.append(pcStr.substring(p, p + Tap.GT450_Swt.capacity())); p = p + Tap.GT450_Swt.capacity();
    Tap.GT450_Men.append(pcStr.substring(p, p + Tap.GT450_Men.capacity())); p = p + Tap.GT450_Men.capacity();
    Tap.GT450_Cli.append(pcStr.substring(p, p + Tap.GT450_Cli.capacity())); p = p + Tap.GT450_Cli.capacity();
    Tap.GT450_Ncl.append(pcStr.substring(p, p + Tap.GT450_Ncl.capacity())); p = p + Tap.GT450_Ncl.capacity();
    Tap.GT450_Ggr.append(pcStr.substring(p, p + Tap.GT450_Ggr.capacity())); p = p + Tap.GT450_Ggr.capacity();
    Tap.GT450_Dcn.append(pcStr.substring(p, p + Tap.GT450_Dcn.capacity())); p = p + Tap.GT450_Dcn.capacity();
    Tap.GT450_Ndc.append(pcStr.substring(p, p + Tap.GT450_Ndc.capacity())); p = p + Tap.GT450_Ndc.capacity();
    Tap.GT450_Suc.append(pcStr.substring(p, p + Tap.GT450_Suc.capacity())); p = p + Tap.GT450_Suc.capacity();
    Tap.GT450_Nsu.append(pcStr.substring(p, p + Tap.GT450_Nsu.capacity())); p = p + Tap.GT450_Nsu.capacity();
    Tap.GT450_Eje.append(pcStr.substring(p, p + Tap.GT450_Eje.capacity())); p = p + Tap.GT450_Eje.capacity();
    Tap.GT450_Nej.append(pcStr.substring(p, p + Tap.GT450_Nej.capacity())); p = p + Tap.GT450_Nej.capacity();
    Tap.GT450_Tej.append(pcStr.substring(p, p + Tap.GT450_Tej.capacity())); p = p + Tap.GT450_Tej.capacity();
    Tap.GT450_Tmn.append(pcStr.substring(p, p + Tap.GT450_Tmn.capacity())); p = p + Tap.GT450_Tmn.capacity();
    Tap.GT450_Trj.append(pcStr.substring(p, p + Tap.GT450_Trj.capacity())); p = p + Tap.GT450_Trj.capacity();
    Tap.GT450_Dcm.append(pcStr.substring(p, p + Tap.GT450_Dcm.capacity())); p = p + Tap.GT450_Dcm.capacity();
    Tap.GT450_Fco.append(pcStr.substring(p, p + Tap.GT450_Fco.capacity())); p = p + Tap.GT450_Fco.capacity();
    Tap.GT450_Mnd.append(pcStr.substring(p, p + Tap.GT450_Mnd.capacity())); p = p + Tap.GT450_Mnd.capacity();
    Tap.GT450_Nmn.append(pcStr.substring(p, p + Tap.GT450_Nmn.capacity())); p = p + Tap.GT450_Nmn.capacity();
    Tap.GT450_Vco.append(pcStr.substring(p, p + Tap.GT450_Vco.capacity())); p = p + Tap.GT450_Vco.capacity();
    Tap.GT450_Vcn.append(pcStr.substring(p, p + Tap.GT450_Vcn.capacity())); p = p + Tap.GT450_Vcn.capacity();
    Tap.GT450_Fcn.append(pcStr.substring(p, p + Tap.GT450_Fcn.capacity())); p = p + Tap.GT450_Fcn.capacity();
    Tap.GT450_Cbt.append(pcStr.substring(p, p + Tap.GT450_Cbt.capacity())); p = p + Tap.GT450_Cbt.capacity();
    Tap.GT450_Est.append(pcStr.substring(p, p + Tap.GT450_Est.capacity())); p = p + Tap.GT450_Est.capacity();
    Tap.GT450_Dpc.append(pcStr.substring(p, p + Tap.GT450_Dpc.capacity())); p = p + Tap.GT450_Dpc.capacity();
    Tap.GT450_Dir.append(pcStr.substring(p, p + Tap.GT450_Dir.capacity())); p = p + Tap.GT450_Dir.capacity();
    Tap.GT450_Tfc.append(pcStr.substring(p, p + Tap.GT450_Tfc.capacity())); p = p + Tap.GT450_Tfc.capacity();
    Tap.GT450_Dmc.append(pcStr.substring(p, p + Tap.GT450_Dmc.capacity())); p = p + Tap.GT450_Dmc.capacity();
    Tap.GT450_Dsg.append(pcStr.substring(p, p + Tap.GT450_Dsg.capacity())); p = p + Tap.GT450_Dsg.capacity();
    Tap.GT450_Ilm.append(pcStr.substring(p, p + Tap.GT450_Ilm.capacity())); p = p + Tap.GT450_Ilm.capacity();
    Tap.GT450_Mtl.append(pcStr.substring(p, p + Tap.GT450_Mtl.capacity())); p = p + Tap.GT450_Mtl.capacity();
    Tap.GT450_Pcl.append(pcStr.substring(p, p + Tap.GT450_Pcl.capacity())); p = p + Tap.GT450_Pcl.capacity();
    Tap.GT450_Prp.append(pcStr.substring(p, p + Tap.GT450_Prp.capacity())); p = p + Tap.GT450_Prp.capacity();
    Tap.GT450_Tsd.append(pcStr.substring(p, p + Tap.GT450_Tsd.capacity())); p = p + Tap.GT450_Tsd.capacity();
    Tap.GT450_Nts.append(pcStr.substring(p, p + Tap.GT450_Nts.capacity())); p = p + Tap.GT450_Nts.capacity();
    Tap.GT450_Mve.append(pcStr.substring(p, p + Tap.GT450_Mve.capacity())); p = p + Tap.GT450_Mve.capacity();
    Tap.GT450_Tab.add(Tap);	    	
    return Tap.GT450_Tab;
  }
  //---------------------------------------------------------------------------------------
  static public String LSet_De_MsgGT450 (Buf_MsgGT450 p_MsgGT450)
  {
    String pcStr = "";
    pcStr = pcStr + p_MsgGT450.GT450_Idr.toString();
    pcStr = pcStr + p_MsgGT450.GT450_Gti.toString();
    pcStr = pcStr + p_MsgGT450.GT450_Swt.toString();
    pcStr = pcStr + p_MsgGT450.GT450_Men.toString();
    pcStr = pcStr + p_MsgGT450.GT450_Cli.toString();
    pcStr = pcStr + p_MsgGT450.GT450_Ncl.toString();
    pcStr = pcStr + p_MsgGT450.GT450_Ggr.toString();
    pcStr = pcStr + p_MsgGT450.GT450_Dcn.toString();
    pcStr = pcStr + p_MsgGT450.GT450_Ndc.toString();
    pcStr = pcStr + p_MsgGT450.GT450_Suc.toString();
    pcStr = pcStr + p_MsgGT450.GT450_Nsu.toString();
    pcStr = pcStr + p_MsgGT450.GT450_Eje.toString();
    pcStr = pcStr + p_MsgGT450.GT450_Nej.toString();
    pcStr = pcStr + p_MsgGT450.GT450_Tej.toString();
    pcStr = pcStr + p_MsgGT450.GT450_Tmn.toString();
    pcStr = pcStr + p_MsgGT450.GT450_Trj.toString();
    pcStr = pcStr + p_MsgGT450.GT450_Dcm.toString();
    pcStr = pcStr + p_MsgGT450.GT450_Fco.toString();
    pcStr = pcStr + p_MsgGT450.GT450_Mnd.toString();
    pcStr = pcStr + p_MsgGT450.GT450_Nmn.toString();
    pcStr = pcStr + p_MsgGT450.GT450_Vco.toString();
    pcStr = pcStr + p_MsgGT450.GT450_Vcn.toString();
    pcStr = pcStr + p_MsgGT450.GT450_Fcn.toString();
    pcStr = pcStr + p_MsgGT450.GT450_Cbt.toString();
    pcStr = pcStr + p_MsgGT450.GT450_Est.toString();
    pcStr = pcStr + p_MsgGT450.GT450_Dpc.toString();
    pcStr = pcStr + p_MsgGT450.GT450_Dir.toString();
    pcStr = pcStr + p_MsgGT450.GT450_Tfc.toString();
    pcStr = pcStr + p_MsgGT450.GT450_Dmc.toString();
    pcStr = pcStr + p_MsgGT450.GT450_Dsg.toString();
    pcStr = pcStr + p_MsgGT450.GT450_Ilm.toString();
    pcStr = pcStr + p_MsgGT450.GT450_Mtl.toString();
    pcStr = pcStr + p_MsgGT450.GT450_Pcl.toString();
    pcStr = pcStr + p_MsgGT450.GT450_Prp.toString();
    pcStr = pcStr + p_MsgGT450.GT450_Tsd.toString();
    pcStr = pcStr + p_MsgGT450.GT450_Nts.toString();
    pcStr = pcStr + p_MsgGT450.GT450_Mve.toString();
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
}