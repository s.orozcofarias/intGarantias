// Source File Name:   MSGGT452.java

package com.intGarantias.modules.global;

import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.util.Formateo;

import java.util.*;
import java.lang.*;
import org.apache.turbine.util.Log;

public class MSGGT452
{
  public static int g_Max_GT452 = 30;
  //---------------------------------------------------------------------------------------
  public static class Bff_MsgGT452
  {
    public StringBuffer GT452_Seq = new StringBuffer(3);   //9(03)        3 Secuencia         
    public StringBuffer GT452_Itg = new StringBuffer(2);   //X(02)        5 Tipo Bien         
    public StringBuffer GT452_Dsb = new StringBuffer(30);  //X(30)       35 Descripcion Bien  
    public StringBuffer GT452_Frv = new StringBuffer(8);   //9(08)       43 Fecha Revaloracion    
    public StringBuffer GT452_Vts = new StringBuffer(15);  //9(13)V9(2)  58 Valor Tasacion    
    public StringBuffer GT452_Vcn = new StringBuffer(15);  //9(13)V9(2)  73 Valor Contable    
    public StringBuffer GT452_Vlq = new StringBuffer(15);  //9(13)V9(2)  88 Valor Liquidacion 
    public StringBuffer GT452_Vrt = new StringBuffer(15);  //9(13)V9(2) 103 Valor Remate      
    public StringBuffer GT452_Mve = new StringBuffer(15);  //9(13)V9(2) 118 Valor Economico 

    public String getSeq()          {return GT452_Seq.toString();}
    public String getItg()          {return GT452_Itg.toString();}
    public String getDsb()          {return GT452_Dsb.toString();}
    public String getFrv()          {return GT452_Frv.toString();}
    public String getVts()          {return GT452_Vts.toString();}
    public String getVcn()          {return GT452_Vcn.toString();}
    public String getVlq()          {return GT452_Vlq.toString();}
    public String getVrt()          {return GT452_Vrt.toString();}
    public String getMve()          {return GT452_Mve.toString();}

    public Formateo f = new Formateo();
    public String fgetFrv()         {return f.FormatDate(GT452_Frv.toString());}
    public String fgetVts(String Dcm){return f.Formato(GT452_Vts.toString(),"99.999.999.999",2,Integer.parseInt(Dcm),',');}
    public String fgetVcn(String Dcm){return f.Formato(GT452_Vcn.toString(),"99.999.999.999",2,Integer.parseInt(Dcm),',');}
    public String fgetVlq(String Dcm){return f.Formato(GT452_Vlq.toString(),"99.999.999.999",2,Integer.parseInt(Dcm),',');}
    public String fgetVrt(String Dcm){return f.Formato(GT452_Vrt.toString(),"99.999.999.999",2,Integer.parseInt(Dcm),',');}
    public String fgetMve(String Dcm){return f.Formato(GT452_Mve.toString(),"99.999.999.999",2,Integer.parseInt(Dcm),',');}
  }
  //---------------------------------------------------------------------------------------
  public static class Buf_MsgGT452
  {
    public StringBuffer GT452_Idr = new StringBuffer(1 );  //X(01)        1 Idr Host              
    public StringBuffer GT452_Gti = new StringBuffer(10);  //X(10)       11 Id.Garantia a buscar  
    public StringBuffer GT452_Nro = new StringBuffer(3 );  //9(03)       14 Nro Bienes            
    public Vector       GT452_Tag = new Vector();          //X(3540)   3554 Tabla Bienes          

    public String getIdr(){return GT452_Idr.toString();}
    public String getGti(){return GT452_Gti.toString();}
    public String getNro(){return GT452_Nro.toString();}
    public Vector getTag(){return GT452_Tag;}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_MsgGT452 Inicia_MsgGT452()
  {
    Buf_MsgGT452 l_MsgGT452 = new Buf_MsgGT452();
    l_MsgGT452.GT452_Idr.replace(0,l_MsgGT452.GT452_Idr.capacity(), RUTGEN.Blancos(1 ));
    l_MsgGT452.GT452_Gti.replace(0,l_MsgGT452.GT452_Gti.capacity(), RUTGEN.Blancos(10));
    l_MsgGT452.GT452_Nro.replace(0,l_MsgGT452.GT452_Nro.capacity(), RUTGEN.Blancos(3 ));
    int p=0;
    for(int i=0; i<g_Max_GT452; i++)
    {
      Bff_MsgGT452 Tap = new Bff_MsgGT452();
      Tap.GT452_Seq.replace(0,Tap.GT452_Seq.capacity(), RUTGEN.Blancos(3 ));
      Tap.GT452_Itg.replace(0,Tap.GT452_Itg.capacity(), RUTGEN.Blancos(2 ));
      Tap.GT452_Dsb.replace(0,Tap.GT452_Dsb.capacity(), RUTGEN.Blancos(30));
      Tap.GT452_Frv.replace(0,Tap.GT452_Frv.capacity(), RUTGEN.Blancos(8 ));
      Tap.GT452_Vts.replace(0,Tap.GT452_Vts.capacity(), RUTGEN.Blancos(15));
      Tap.GT452_Vcn.replace(0,Tap.GT452_Vcn.capacity(), RUTGEN.Blancos(15));
      Tap.GT452_Vlq.replace(0,Tap.GT452_Vlq.capacity(), RUTGEN.Blancos(15));
      Tap.GT452_Vrt.replace(0,Tap.GT452_Vrt.capacity(), RUTGEN.Blancos(15));
      Tap.GT452_Mve.replace(0,Tap.GT452_Mve.capacity(), RUTGEN.Blancos(15));
      l_MsgGT452.GT452_Tag.add(Tap);
    }
    return l_MsgGT452;
  }
  //---------------------------------------------------------------------------------------
  static public Buf_MsgGT452 LSet_A_MsgGT452(String pcStr)
  {
    Buf_MsgGT452 l_MsgGT452 = new Buf_MsgGT452();
    MSGGT452 vMsgGT452 = new MSGGT452();
    Vector vDatos = vMsgGT452.LSet_A_vMsgGT452(pcStr);
    l_MsgGT452 = (MSGGT452.Buf_MsgGT452)vDatos.elementAt(0);
    return l_MsgGT452;
  }
  //---------------------------------------------------------------------------------------
  public Vector LSet_A_vMsgGT452(String pcStr)
  {
    int p=0;
    Vector vec= new Vector();
    Buf_MsgGT452 l_MsgGT452 = new Buf_MsgGT452();                              
    l_MsgGT452.GT452_Idr.append( pcStr.substring(p, p + l_MsgGT452.GT452_Idr.capacity())); p = p + l_MsgGT452.GT452_Idr.capacity();
    l_MsgGT452.GT452_Gti.append( pcStr.substring(p, p + l_MsgGT452.GT452_Gti.capacity())); p = p + l_MsgGT452.GT452_Gti.capacity();
    l_MsgGT452.GT452_Nro.append( pcStr.substring(p, p + l_MsgGT452.GT452_Nro.capacity())); p = p + l_MsgGT452.GT452_Nro.capacity();
    for (int i=0;i< Integer.parseInt(l_MsgGT452.GT452_Nro.toString());i++)
        {
          Bff_MsgGT452 Tap = new Bff_MsgGT452();
          Tap.GT452_Seq.append(pcStr.substring(p, p + Tap.GT452_Seq.capacity())); p = p + Tap.GT452_Seq.capacity();
          Tap.GT452_Itg.append(pcStr.substring(p, p + Tap.GT452_Itg.capacity())); p = p + Tap.GT452_Itg.capacity();
          Tap.GT452_Dsb.append(pcStr.substring(p, p + Tap.GT452_Dsb.capacity())); p = p + Tap.GT452_Dsb.capacity();
          Tap.GT452_Frv.append(pcStr.substring(p, p + Tap.GT452_Frv.capacity())); p = p + Tap.GT452_Frv.capacity();
          Tap.GT452_Vts.append(pcStr.substring(p, p + Tap.GT452_Vts.capacity())); p = p + Tap.GT452_Vts.capacity();
          Tap.GT452_Vcn.append(pcStr.substring(p, p + Tap.GT452_Vcn.capacity())); p = p + Tap.GT452_Vcn.capacity();
          Tap.GT452_Vlq.append(pcStr.substring(p, p + Tap.GT452_Vlq.capacity())); p = p + Tap.GT452_Vlq.capacity();
          Tap.GT452_Vrt.append(pcStr.substring(p, p + Tap.GT452_Vrt.capacity())); p = p + Tap.GT452_Vrt.capacity();
          Tap.GT452_Mve.append(pcStr.substring(p, p + Tap.GT452_Mve.capacity())); p = p + Tap.GT452_Mve.capacity();
          l_MsgGT452.GT452_Tag.add(Tap);
        }
    vec.add (l_MsgGT452);            
    return vec;
  }
  //---------------------------------------------------------------------------------------
  static public String LSet_De_MsgGT452(Buf_MsgGT452 p_MsgGT452)
  {
    String pcStr = "";
    pcStr = pcStr + p_MsgGT452.GT452_Idr.toString();
    pcStr = pcStr + p_MsgGT452.GT452_Gti.toString();
    pcStr = pcStr + p_MsgGT452.GT452_Nro.toString();
    for(int i=0; i<p_MsgGT452.GT452_Tag.size(); i++)
    {
      pcStr = pcStr + ((Bff_MsgGT452)p_MsgGT452.GT452_Tag.elementAt(i)).GT452_Seq.toString();
      pcStr = pcStr + ((Bff_MsgGT452)p_MsgGT452.GT452_Tag.elementAt(i)).GT452_Itg.toString();
      pcStr = pcStr + ((Bff_MsgGT452)p_MsgGT452.GT452_Tag.elementAt(i)).GT452_Dsb.toString();
      pcStr = pcStr + ((Bff_MsgGT452)p_MsgGT452.GT452_Tag.elementAt(i)).GT452_Frv.toString();
      pcStr = pcStr + ((Bff_MsgGT452)p_MsgGT452.GT452_Tag.elementAt(i)).GT452_Vts.toString();
      pcStr = pcStr + ((Bff_MsgGT452)p_MsgGT452.GT452_Tag.elementAt(i)).GT452_Vcn.toString();
      pcStr = pcStr + ((Bff_MsgGT452)p_MsgGT452.GT452_Tag.elementAt(i)).GT452_Vlq.toString();
      pcStr = pcStr + ((Bff_MsgGT452)p_MsgGT452.GT452_Tag.elementAt(i)).GT452_Vrt.toString();
      pcStr = pcStr + ((Bff_MsgGT452)p_MsgGT452.GT452_Tag.elementAt(i)).GT452_Mve.toString();
    }
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
}