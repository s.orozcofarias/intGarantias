// Source File Name:   MSGGT175.java

package com.intGarantias.modules.global;

import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.util.Formateo;

import java.util.*;
import java.lang.*;
import org.apache.turbine.util.Log;

public class MSGGT175
{
  public static int g_Max_GT175 = 60;
  //---------------------------------------------------------------------------------------
  public static class Bff_MsgGT175
  {
    public StringBuffer GtGeb_Ges = new StringBuffer(10);       //X(10)       10 Id. Garantia             
    public StringBuffer GtGeb_Rel = new StringBuffer(5 );       //X(05)       15 Rel. Cliente/Garantia    
    public StringBuffer GtGeb_Cre = new StringBuffer(22);       //X(22)       37 Id. Credito            
    public StringBuffer GtGeb_Est = new StringBuffer(5 );       //X(05)       42 Estado                   
    public StringBuffer GtGeb_Mod = new StringBuffer(1 );       //X(01)       43 Modif Agrega/Elimina     

    public String getGes()      {return GtGeb_Ges.toString();}
    public String getRel()      {return GtGeb_Rel.toString();}
    public String getCre()      {return GtGeb_Cre.toString();}
    public String getEst()      {return GtGeb_Est.toString();}
    public String getMod()      {return GtGeb_Mod.toString();}
  }
  //---------------------------------------------------------------------------------------
  public static class Buf_MsgGT175
  {
    public StringBuffer GT175_Idr     = new StringBuffer(1  );
    public StringBuffer GT175_Img     = new StringBuffer(827);
    public StringBuffer GT175_Swt_Dcn = new StringBuffer(1  );
    public StringBuffer GT175_Swt_Gti = new StringBuffer(1  );
    public StringBuffer GT175_Swt_Cnr = new StringBuffer(1  );
    public StringBuffer GT175_Swt_Evt = new StringBuffer(1  );
    public StringBuffer GT175_Swt_Cli = new StringBuffer(1  );
    public StringBuffer GT175_Idt     = new StringBuffer(1  );
    public StringBuffer GT175_Nro     = new StringBuffer(2  );
    public Vector       GT175_Geb     = new Vector();

    public String getIdr()           {return GT175_Idr.toString();}
    public String getImg()           {return GT175_Img.toString();}
    public String getSwt_Dcn()       {return GT175_Swt_Dcn.toString();}
    public String getSwt_Gti()       {return GT175_Swt_Gti.toString();}
    public String getSwt_Cnr()       {return GT175_Swt_Cnr.toString();}
    public String getSwt_Evt()       {return GT175_Swt_Evt.toString();}
    public String getSwt_Cli()       {return GT175_Swt_Cli.toString();}
    public String getIdt()           {return GT175_Idt.toString();}
    public String getNro()           {return GT175_Nro.toString();}
    public Vector getGeb()           {return GT175_Geb;}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_MsgGT175 Inicia_MsgGT175()
  {
    Buf_MsgGT175 l_MsgGT175 = new Buf_MsgGT175();
    l_MsgGT175.GT175_Idr.replace    (0,l_MsgGT175.GT175_Idr.capacity(),     RUTGEN.Blancos(1  ));
    l_MsgGT175.GT175_Img.replace    (0,l_MsgGT175.GT175_Img.capacity(),     RUTGEN.Blancos(827));
    l_MsgGT175.GT175_Swt_Dcn.replace(0,l_MsgGT175.GT175_Swt_Dcn.capacity(), RUTGEN.Blancos(1  ));
    l_MsgGT175.GT175_Swt_Gti.replace(0,l_MsgGT175.GT175_Swt_Gti.capacity(), RUTGEN.Blancos(1  ));
    l_MsgGT175.GT175_Swt_Cnr.replace(0,l_MsgGT175.GT175_Swt_Cnr.capacity(), RUTGEN.Blancos(1  ));
    l_MsgGT175.GT175_Swt_Evt.replace(0,l_MsgGT175.GT175_Swt_Evt.capacity(), RUTGEN.Blancos(1  ));
    l_MsgGT175.GT175_Swt_Cli.replace(0,l_MsgGT175.GT175_Swt_Cli.capacity(), RUTGEN.Blancos(1  ));
    l_MsgGT175.GT175_Idt.replace    (0,l_MsgGT175.GT175_Idt.capacity(),     RUTGEN.Blancos(1  ));
    l_MsgGT175.GT175_Nro.replace    (0,l_MsgGT175.GT175_Nro.capacity(),     RUTGEN.Blancos(2  ));
    int p = 0;
    for (int i=0; i<g_Max_GT175; i++)
        {
          Bff_MsgGT175 Tap = new Bff_MsgGT175();
          Tap.GtGeb_Ges.replace(0, Tap.GtGeb_Ges.capacity(), RUTGEN.Blancos(10));
          Tap.GtGeb_Rel.replace(0, Tap.GtGeb_Rel.capacity(), RUTGEN.Blancos(5 ));
          Tap.GtGeb_Cre.replace(0, Tap.GtGeb_Cre.capacity(), RUTGEN.Blancos(22));
          Tap.GtGeb_Est.replace(0, Tap.GtGeb_Est.capacity(), RUTGEN.Blancos(5 ));
          Tap.GtGeb_Mod.replace(0, Tap.GtGeb_Mod.capacity(), RUTGEN.Blancos(1 ));
          l_MsgGT175.GT175_Geb.add(Tap);        
        }
    return l_MsgGT175;
  }
  //---------------------------------------------------------------------------------------
  static public Buf_MsgGT175 LSet_A_MsgGT175(String pcStr)
  {
    Buf_MsgGT175 l_MsgGT175 = new Buf_MsgGT175();
    MSGGT175 vMsgGT175 = new MSGGT175();
    Vector vDatos = vMsgGT175.LSet_A_vMsgGT175(pcStr);
    l_MsgGT175 = (MSGGT175.Buf_MsgGT175)vDatos.elementAt(0);
    return l_MsgGT175;
  }
  //---------------------------------------------------------------------------------------
  public Vector LSet_A_vMsgGT175(String pcStr)
  {
    Vector vec = new Vector();
    int p = 0;
    Buf_MsgGT175 l_MsgGT175 = new Buf_MsgGT175();                            
    l_MsgGT175.GT175_Idr.append     (pcStr.substring(p, p + l_MsgGT175.GT175_Idr.capacity()));     p = p + l_MsgGT175.GT175_Idr.capacity();
    l_MsgGT175.GT175_Img.append     (pcStr.substring(p, p + l_MsgGT175.GT175_Img.capacity()));     p = p + l_MsgGT175.GT175_Img.capacity();
    l_MsgGT175.GT175_Swt_Dcn.append (pcStr.substring(p, p + l_MsgGT175.GT175_Swt_Dcn.capacity())); p = p + l_MsgGT175.GT175_Swt_Dcn.capacity();
    l_MsgGT175.GT175_Swt_Gti.append (pcStr.substring(p, p + l_MsgGT175.GT175_Swt_Gti.capacity())); p = p + l_MsgGT175.GT175_Swt_Gti.capacity();
    l_MsgGT175.GT175_Swt_Cnr.append (pcStr.substring(p, p + l_MsgGT175.GT175_Swt_Cnr.capacity())); p = p + l_MsgGT175.GT175_Swt_Cnr.capacity();
    l_MsgGT175.GT175_Swt_Evt.append (pcStr.substring(p, p + l_MsgGT175.GT175_Swt_Evt.capacity())); p = p + l_MsgGT175.GT175_Swt_Evt.capacity();
    l_MsgGT175.GT175_Swt_Cli.append (pcStr.substring(p, p + l_MsgGT175.GT175_Swt_Cli.capacity())); p = p + l_MsgGT175.GT175_Swt_Cli.capacity();
    l_MsgGT175.GT175_Idt.append     (pcStr.substring(p, p + l_MsgGT175.GT175_Idt.capacity()));     p = p + l_MsgGT175.GT175_Idt.capacity();
    l_MsgGT175.GT175_Nro.append     (pcStr.substring(p, p + l_MsgGT175.GT175_Nro.capacity()));     p = p + l_MsgGT175.GT175_Nro.capacity();
//    for (int i=0; i<Integer.parseInt(l_MsgGT175.GT175_Nro.toString()); i++)
    for (int i=0; i<g_Max_GT175; i++)
        {
          Bff_MsgGT175 Tap = new Bff_MsgGT175();
          Tap.GtGeb_Ges.append(pcStr.substring(p, p + Tap.GtGeb_Ges.capacity())); p = p + Tap.GtGeb_Ges.capacity();
          Tap.GtGeb_Rel.append(pcStr.substring(p, p + Tap.GtGeb_Rel.capacity())); p = p + Tap.GtGeb_Rel.capacity();
          Tap.GtGeb_Cre.append(pcStr.substring(p, p + Tap.GtGeb_Cre.capacity())); p = p + Tap.GtGeb_Cre.capacity();
          Tap.GtGeb_Est.append(pcStr.substring(p, p + Tap.GtGeb_Est.capacity())); p = p + Tap.GtGeb_Est.capacity();
          Tap.GtGeb_Mod.append(pcStr.substring(p, p + Tap.GtGeb_Mod.capacity())); p = p + Tap.GtGeb_Mod.capacity();
          l_MsgGT175.GT175_Geb.add(Tap);
        }
    vec.add (l_MsgGT175);
    return vec;
  }
  //---------------------------------------------------------------------------------------
  static public String LSet_De_MsgGT175(Buf_MsgGT175 p_MsgGT175)
  {
    String pcStr = "";
    pcStr = pcStr + p_MsgGT175.GT175_Idr.toString();
    pcStr = pcStr + p_MsgGT175.GT175_Img.toString();
    pcStr = pcStr + p_MsgGT175.GT175_Swt_Dcn.toString();
    pcStr = pcStr + p_MsgGT175.GT175_Swt_Gti.toString();
    pcStr = pcStr + p_MsgGT175.GT175_Swt_Cnr.toString();
    pcStr = pcStr + p_MsgGT175.GT175_Swt_Evt.toString();
    pcStr = pcStr + p_MsgGT175.GT175_Swt_Cli.toString();
    pcStr = pcStr + p_MsgGT175.GT175_Idt.toString();
    pcStr = pcStr + p_MsgGT175.GT175_Nro.toString();
    for (int i=0; i<p_MsgGT175.GT175_Geb.size(); i++)
        {
          pcStr = pcStr + ((Bff_MsgGT175)p_MsgGT175.GT175_Geb.elementAt(i)).GtGeb_Ges.toString();
          pcStr = pcStr + ((Bff_MsgGT175)p_MsgGT175.GT175_Geb.elementAt(i)).GtGeb_Rel.toString();
          pcStr = pcStr + ((Bff_MsgGT175)p_MsgGT175.GT175_Geb.elementAt(i)).GtGeb_Cre.toString();
          pcStr = pcStr + ((Bff_MsgGT175)p_MsgGT175.GT175_Geb.elementAt(i)).GtGeb_Est.toString();
          pcStr = pcStr + ((Bff_MsgGT175)p_MsgGT175.GT175_Geb.elementAt(i)).GtGeb_Mod.toString();
        }
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
}