// Source File Name:   MSGGT436.java

package com.intGarantias.modules.global;

import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.util.Formateo;

import java.util.*;
import java.lang.*;
import org.apache.turbine.util.Log;

public class MSGGT436
{
  public static int g_Max_GT436 = 25;
  //---------------------------------------------------------------------------------------
  public static class Buf_Gbs
  {
    public StringBuffer Gbs_Seq = new StringBuffer(3 );         //'9(03)         3 Id. Seq Bien         
    public StringBuffer Gbs_Itb = new StringBuffer(2 );         //'X(02)         5 Id Tipo Bien         
    public StringBuffer Ghd_Sqd = new StringBuffer(3 );         //'9(03)         8 Sequencia Detalle    
    public StringBuffer Ghd_Ith = new StringBuffer(2 );         //'X(02)        10 Idr Tipo Hipoteca    
    public StringBuffer Ghd_Dsh = new StringBuffer(30);         //'X(30)        40 Descripcion          
    public StringBuffer Ghd_Pet = new StringBuffer(5 );         //'9(03)V9(2)   45 %Avance              

    public String getSeq()        {return Gbs_Seq.toString();}
    public String getItb()        {return Gbs_Itb.toString();}
    public String getSqd()        {return Ghd_Sqd.toString();}
    public String getIth()        {return Ghd_Ith.toString();}
    public String getDsh()        {return Ghd_Dsh.toString();}
    public String getPet()        {return Ghd_Pet.toString();}

    public Formateo f = new Formateo();
    public String fgetPet()       {return f.Formato(Ghd_Pet.toString(),"999",2,2,','); }
  }
  //---------------------------------------------------------------------------------------
  public static class Buf_MsgGT436
  {
    public StringBuffer GT436_Idr = new StringBuffer(1 );       //'X(01)         1 Idr Host        
    public StringBuffer GT436_Gti = new StringBuffer(10);       //'X(10)        11 Id. Garantia    
    public StringBuffer GT436_Nro = new StringBuffer(2 );       //'9(02)        13 Total Indices   
    public Vector       GT436_Rgs = new Vector();               //'X(2900)    2913 Registros
    
    public String getIdr()        {return GT436_Idr.toString();}
    public String getGti()        {return GT436_Gti.toString();}
    public String getNro()        {return GT436_Nro.toString();}
    public Vector getRgs()        {return GT436_Rgs;}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_MsgGT436 Inicia_MsgGT436()
  {
    Buf_MsgGT436 l_MsgGT436 = new Buf_MsgGT436();
    l_MsgGT436.GT436_Idr.replace(0,l_MsgGT436.GT436_Idr.capacity(), RUTGEN.Blancos(1 ));
    l_MsgGT436.GT436_Gti.replace(0,l_MsgGT436.GT436_Gti.capacity(), RUTGEN.Blancos(10));
    l_MsgGT436.GT436_Nro.replace(0,l_MsgGT436.GT436_Nro.capacity(), RUTGEN.Blancos(2 ));
    int p=0;
    for (int i=0; i<g_Max_GT436; i++)
        {
          Buf_Gbs Tap = new Buf_Gbs();
          Tap.Gbs_Seq.replace(0, Tap.Gbs_Seq.capacity(), RUTGEN.Blancos(3 ));
          Tap.Gbs_Itb.replace(0, Tap.Gbs_Itb.capacity(), RUTGEN.Blancos(2 ));
          Tap.Ghd_Sqd.replace(0, Tap.Ghd_Sqd.capacity(), RUTGEN.Blancos(3 ));
          Tap.Ghd_Ith.replace(0, Tap.Ghd_Ith.capacity(), RUTGEN.Blancos(2 ));
          Tap.Ghd_Dsh.replace(0, Tap.Ghd_Dsh.capacity(), RUTGEN.Blancos(30));
          Tap.Ghd_Pet.replace(0, Tap.Ghd_Pet.capacity(), RUTGEN.Blancos(5 ));
          l_MsgGT436.GT436_Rgs.add(Tap);
        }
    return l_MsgGT436;
  }
  //---------------------------------------------------------------------------------------
  static public Buf_MsgGT436 LSet_A_MsgGT436(String pcStr)
  {
    Buf_MsgGT436 l_MsgGT436 = new Buf_MsgGT436();
    MSGGT436 vMsgGT436 = new MSGGT436();
    Vector vDatos = vMsgGT436.LSet_A_vMsgGT436(pcStr);
    l_MsgGT436 = (MSGGT436.Buf_MsgGT436)vDatos.elementAt(0);
    return l_MsgGT436;
  }
  //---------------------------------------------------------------------------------------
  public Vector LSet_A_vMsgGT436(String pcStr)
  {
    Vector vec = new Vector();
    int p = 0;
    Buf_MsgGT436 l_MsgGT436 = new Buf_MsgGT436();                              
    l_MsgGT436.GT436_Idr.replace(0, l_MsgGT436.GT436_Idr.capacity(), pcStr.substring(p, p + l_MsgGT436.GT436_Idr.capacity())); p = p + l_MsgGT436.GT436_Idr.capacity();
    l_MsgGT436.GT436_Gti.replace(0, l_MsgGT436.GT436_Gti.capacity(), pcStr.substring(p, p + l_MsgGT436.GT436_Gti.capacity())); p = p + l_MsgGT436.GT436_Gti.capacity();
    l_MsgGT436.GT436_Nro.replace(0, l_MsgGT436.GT436_Nro.capacity(), pcStr.substring(p, p + l_MsgGT436.GT436_Nro.capacity())); p = p + l_MsgGT436.GT436_Nro.capacity();
    for (int i=0; i<Integer.parseInt(l_MsgGT436.GT436_Nro.toString()); i++)
        {
          Buf_Gbs Tap = new Buf_Gbs();                      
          Tap.Gbs_Seq.replace(0, Tap.Gbs_Seq.capacity(), pcStr.substring(p, p + Tap.Gbs_Seq.capacity())); p = p + Tap.Gbs_Seq.capacity();
          Tap.Gbs_Itb.replace(0, Tap.Gbs_Itb.capacity(), pcStr.substring(p, p + Tap.Gbs_Itb.capacity())); p = p + Tap.Gbs_Itb.capacity();
          Tap.Ghd_Sqd.replace(0, Tap.Ghd_Sqd.capacity(), pcStr.substring(p, p + Tap.Ghd_Sqd.capacity())); p = p + Tap.Ghd_Sqd.capacity();
          Tap.Ghd_Ith.replace(0, Tap.Ghd_Ith.capacity(), pcStr.substring(p, p + Tap.Ghd_Ith.capacity())); p = p + Tap.Ghd_Ith.capacity();
          Tap.Ghd_Dsh.replace(0, Tap.Ghd_Dsh.capacity(), pcStr.substring(p, p + Tap.Ghd_Dsh.capacity())); p = p + Tap.Ghd_Dsh.capacity();
          Tap.Ghd_Pet.replace(0, Tap.Ghd_Pet.capacity(), pcStr.substring(p, p + Tap.Ghd_Pet.capacity())); p = p + Tap.Ghd_Pet.capacity();                      
          l_MsgGT436.GT436_Rgs.add(Tap);
        }
    vec.add (l_MsgGT436);
    return vec;
  }
  //---------------------------------------------------------------------------------------
  static public String LSet_De_MsgGT436(Buf_MsgGT436 p_MsgGT436)
  {
    String pcStr = "";
    pcStr = pcStr + p_MsgGT436.GT436_Idr.toString();
    pcStr = pcStr + p_MsgGT436.GT436_Gti.toString();
    pcStr = pcStr + p_MsgGT436.GT436_Nro.toString();
    for (int i=0; i<p_MsgGT436.GT436_Rgs.size(); i++)
        {
          pcStr = pcStr + ((Buf_Gbs)p_MsgGT436.GT436_Rgs.elementAt(i)).Gbs_Seq.toString();
          pcStr = pcStr + ((Buf_Gbs)p_MsgGT436.GT436_Rgs.elementAt(i)).Gbs_Itb.toString();
          pcStr = pcStr + ((Buf_Gbs)p_MsgGT436.GT436_Rgs.elementAt(i)).Ghd_Sqd.toString();
          pcStr = pcStr + ((Buf_Gbs)p_MsgGT436.GT436_Rgs.elementAt(i)).Ghd_Ith.toString();
          pcStr = pcStr + ((Buf_Gbs)p_MsgGT436.GT436_Rgs.elementAt(i)).Ghd_Dsh.toString();
          pcStr = pcStr + ((Buf_Gbs)p_MsgGT436.GT436_Rgs.elementAt(i)).Ghd_Pet.toString();
        }
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
}