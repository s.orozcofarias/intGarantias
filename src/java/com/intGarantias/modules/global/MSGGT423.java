// Source File Name:   MSGGT423.java

package com.intGarantias.modules.global;

import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.util.Formateo;

import java.util.*;
import java.lang.*;
import org.apache.turbine.util.Log;

public class MSGGT423
{
  //---------------------------------------------------------------------------------------
  public static class vPpv
  { public StringBuffer GT423_Ppv = new StringBuffer(9  );      //9(07)V9(2)     Valor Presupuesto
    public String getPpv()         {return GT423_Ppv.toString();}
  }
  public static class vOep
  { public StringBuffer GT423_Oep = new StringBuffer(5  );      //9(03)V9(2)     % Estado Total
    public String getOep()         {return GT423_Oep.toString();}
  }
  public static class vOev
  { public StringBuffer GT423_Oev = new StringBuffer(9  );      //9(07)V9(2)     Valor Estado Total
    public String getOev()         {return GT423_Oev.toString();}
  }
  public static class vEpp
  { public StringBuffer GT423_Epp = new StringBuffer(5  );      //9(03)V9(2)     % Estado
    public String getEpp()         {return GT423_Epp.toString();}
  }
  public static class vEpv
  { public StringBuffer GT423_Epv = new StringBuffer(9  );      //9(07)V9(2)     Valor Estado
    public String getEpv()         {return GT423_Epv.toString();}
  }
  //---------------------------------------------------------------------------------------
  public static class Bff_MsgGT423
  {
    public StringBuffer GT423_Frv = new StringBuffer(8 );    //9(08)         8 Fecha Estado
    public Vector      vGT423_Epp = new Vector(15);          //X(75)        83 % Estado
    public Vector      vGT423_Epv = new Vector(15);          //X(135)      218 Valor Estado

    public String getFrv()         {return GT423_Frv.toString();}
    public Vector getVecEpp()      {return vGT423_Epp;}
    public Vector getVecEpv()      {return vGT423_Epv;}

    public Formateo f = new Formateo();
    public String fgetFrv()        {return f.FormatDate(getFrv());}
  }
  //---------------------------------------------------------------------------------------
  public static class Buf_MsgGT423
  {
    public StringBuffer GT423_Idr = new StringBuffer(1  );   //X(01)         1 Idr Host
    public StringBuffer GT423_Gti = new StringBuffer(10 );   //X(10)        11 Id.Garantia
    public StringBuffer GT423_Seq = new StringBuffer(3  );   //9(03)        14 Secuencia
    public StringBuffer GT423_Sqd = new StringBuffer(3  );   //9(03)        17 Secuencia Detalle
    public StringBuffer GT423_Fts = new StringBuffer(8  );   //9(08)        25 Fecha ITO
    public StringBuffer GT423_Vuf = new StringBuffer(9  );   //9(07)V9(2)   34 Valor UF
    public StringBuffer GT423_Vtc = new StringBuffer(9  );   //9(07)V9(2)   43 Valor TC
    public StringBuffer GT423_Tuf = new StringBuffer(15 );   //9(11)V9(4)   58 Tasacion UF
    public StringBuffer GT423_Ttc = new StringBuffer(15 );   //9(13)V9(2)   73 Tasacion TC
    public StringBuffer GT423_Tps = new StringBuffer(15 );   //9(13)V9(2)   88 Tasacion $
    public StringBuffer GT423_Dsb = new StringBuffer(30 );   //X(30)       118 Descripcion Bien
    public StringBuffer GT423_Ctr = new StringBuffer(30 );   //X(30)       148 Constructora
    public StringBuffer GT423_Fin = new StringBuffer(8  );   //9(08)       156 Fecha Inicio
    public StringBuffer GT423_Fte = new StringBuffer(8  );   //9(08)       164 Fecha Termino
    public StringBuffer GT423_Ped = new StringBuffer(7  );   //9(07)       171 Permiso Edificacion
    public StringBuffer GT423_Fed = new StringBuffer(8  );   //9(08)       179 Fecha PED
    public StringBuffer GT423_Spc = new StringBuffer(5  );   //9(05)       184 Superficie Const
    public StringBuffer GT423_Sps = new StringBuffer(5  );   //9(05)       189 Superficie Saldo
    public StringBuffer GT423_Rem = new StringBuffer(7  );   //9(07)       196 Recepcion Municipal
    public StringBuffer GT423_Vsg = new StringBuffer(15 );   //9(11)V9(4)  211 Valor Asegurable
    public StringBuffer GT423_Dpy = new StringBuffer(184);   //X(184)      395 Comment 1
    public StringBuffer GT423_Opp = new StringBuffer(184);   //X(184)      579 Comment 2
    public StringBuffer GT423_Occ = new StringBuffer(184);   //X(184)      763 Comment 3
    public StringBuffer GT423_Nro = new StringBuffer(2  );   //9(02)       765 Nro Elementos
    public Vector      vGT423_Ppv = new Vector(15);          //X(135)      900 Valor Presupuesto
    public Vector      vGT423_Oep = new Vector(15);          //X(75)       975 % Estado Total
    public Vector      vGT423_Oev = new Vector(15);          //X(135)     1110 Valor Estado Total
    public Vector       GT423_Tab = new Vector(11);          //X(2398)    3508 Detalle Consulta
    public StringBuffer GT423_Idx = new StringBuffer(2  );   //9(02)      3510 Nro Activo
    public StringBuffer GT423_Fp1 = new StringBuffer(8  );   //9(08)      3518 Fecha EP1
    public StringBuffer GT423_Fp2 = new StringBuffer(8  );   //9(08)      3526 Fecha EP2

    public String getIdr()         {return GT423_Idr.toString();}
    public String getGti()         {return GT423_Gti.toString();}
    public String getSeq()         {return GT423_Seq.toString();}
    public String getSqd()         {return GT423_Sqd.toString();}
    public String getFts()         {return GT423_Fts.toString();}
    public String getVuf()         {return GT423_Vuf.toString();}
    public String getVtc()         {return GT423_Vtc.toString();}
    public String getTuf()         {return GT423_Tuf.toString();}
    public String getTtc()         {return GT423_Ttc.toString();}
    public String getTps()         {return GT423_Tps.toString();}
    public String getDsb()         {return GT423_Dsb.toString();}
    public String getCtr()         {return GT423_Ctr.toString();}
    public String getFin()         {return GT423_Fin.toString();}
    public String getFte()         {return GT423_Fte.toString();}
    public String getPed()         {return GT423_Ped.toString();}
    public String getFed()         {return GT423_Fed.toString();}
    public String getSpc()         {return GT423_Spc.toString();}
    public String getSps()         {return GT423_Sps.toString();}
    public String getRem()         {return GT423_Rem.toString();}
    public String getVsg()         {return GT423_Vsg.toString();}
    public String getDpy()         {return GT423_Dpy.toString();}
    public String getOpp()         {return GT423_Opp.toString();}
    public String getOcc()         {return GT423_Occ.toString();}
    public String getNro()         {return GT423_Nro.toString();}
    public Vector getVecPpv()      {return vGT423_Ppv;}
    public Vector getVecOep()      {return vGT423_Oep;}
    public Vector getVecOev()      {return vGT423_Oev;}
    public Vector getTab()         {return GT423_Tab;}
    public String getIdx()         {return GT423_Idx.toString();}
    public String getFp1()         {return GT423_Fp1.toString();}
    public String getFp2()         {return GT423_Fp2.toString();}

    public Formateo f = new Formateo();
    public String fgetFts()        {return f.FormatDate(getFts());}
    public String fgetVuf()        {return f.Formato(getVuf(),"9.999.999",2,2,',');}
    public String fgetVtc()        {return f.Formato(getVtc(),"9.999.999",2,2,',');}
    public String fgetTuf()        {return f.Formato(getTuf(),"99.999.999.999",4,4,',');}
    public String fgetTtc()        {return f.Formato(getTtc(),"99.999.999.999",2,2,',');}
    public String fgetTps()        {return f.Formato(getTps(),"99.999.999.999",2,0,',');}
    public String fgetFin()        {return f.FormatDate(getFin());}
    public String fgetFte()        {return f.FormatDate(getFte());}
    public String fgetVsg()        {return f.Formato(getVsg(),"99.999.999.999",4,4,',');}
    public int    ngetNro()        {return Integer.parseInt(GT423_Nro.toString());}
    public int    ngetIdx()        {return Integer.parseInt(GT423_Idx.toString());}
    public String fgetFp1()        {return f.FormatDate(getFp1());}
    public String fgetFp2()        {return f.FormatDate(getFp2());}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_MsgGT423 Inicia_MsgGT423 ()
  {
    Buf_MsgGT423 l_MsgGT423 = new Buf_MsgGT423();
    l_MsgGT423.GT423_Idr.replace(0, l_MsgGT423.GT423_Idr.capacity(), RUTGEN.Blancos(1  ));
    l_MsgGT423.GT423_Gti.replace(0, l_MsgGT423.GT423_Gti.capacity(), RUTGEN.Blancos(10 ));
    l_MsgGT423.GT423_Seq.replace(0, l_MsgGT423.GT423_Seq.capacity(), RUTGEN.Blancos(3  ));
    l_MsgGT423.GT423_Sqd.replace(0, l_MsgGT423.GT423_Sqd.capacity(), RUTGEN.Blancos(3  ));
    l_MsgGT423.GT423_Fts.replace(0, l_MsgGT423.GT423_Fts.capacity(), RUTGEN.Blancos(8  ));
    l_MsgGT423.GT423_Vuf.replace(0, l_MsgGT423.GT423_Vuf.capacity(), RUTGEN.Blancos(9  ));
    l_MsgGT423.GT423_Vtc.replace(0, l_MsgGT423.GT423_Vtc.capacity(), RUTGEN.Blancos(9  ));
    l_MsgGT423.GT423_Tuf.replace(0, l_MsgGT423.GT423_Tuf.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT423.GT423_Ttc.replace(0, l_MsgGT423.GT423_Ttc.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT423.GT423_Tps.replace(0, l_MsgGT423.GT423_Tps.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT423.GT423_Dsb.replace(0, l_MsgGT423.GT423_Dsb.capacity(), RUTGEN.Blancos(30 ));
    l_MsgGT423.GT423_Ctr.replace(0, l_MsgGT423.GT423_Ctr.capacity(), RUTGEN.Blancos(30 ));
    l_MsgGT423.GT423_Fin.replace(0, l_MsgGT423.GT423_Fin.capacity(), RUTGEN.Blancos(8  ));
    l_MsgGT423.GT423_Fte.replace(0, l_MsgGT423.GT423_Fte.capacity(), RUTGEN.Blancos(8  ));
    l_MsgGT423.GT423_Ped.replace(0, l_MsgGT423.GT423_Ped.capacity(), RUTGEN.Blancos(7  ));
    l_MsgGT423.GT423_Fed.replace(0, l_MsgGT423.GT423_Fed.capacity(), RUTGEN.Blancos(8  ));
    l_MsgGT423.GT423_Spc.replace(0, l_MsgGT423.GT423_Spc.capacity(), RUTGEN.Blancos(5  ));
    l_MsgGT423.GT423_Sps.replace(0, l_MsgGT423.GT423_Sps.capacity(), RUTGEN.Blancos(5  ));
    l_MsgGT423.GT423_Rem.replace(0, l_MsgGT423.GT423_Rem.capacity(), RUTGEN.Blancos(7  ));
    l_MsgGT423.GT423_Vsg.replace(0, l_MsgGT423.GT423_Vsg.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT423.GT423_Dpy.replace(0, l_MsgGT423.GT423_Dpy.capacity(), RUTGEN.Blancos(184));
    l_MsgGT423.GT423_Opp.replace(0, l_MsgGT423.GT423_Opp.capacity(), RUTGEN.Blancos(184));
    l_MsgGT423.GT423_Occ.replace(0, l_MsgGT423.GT423_Occ.capacity(), RUTGEN.Blancos(184));
    l_MsgGT423.GT423_Nro.replace(0, l_MsgGT423.GT423_Nro.capacity(), RUTGEN.Blancos(2  ));
    for (int i=0; i<15; i++)
        {
          vPpv Tap = new vPpv();          
          Tap.GT423_Ppv.replace(0, Tap.GT423_Ppv.capacity(), RUTGEN.Blancos(9));
          l_MsgGT423.vGT423_Ppv.add(Tap);
        }               
    for (int i=0; i<15; i++)
        {
          vOep Tap = new vOep();          
          Tap.GT423_Oep.replace(0, Tap.GT423_Oep.capacity(), RUTGEN.Blancos(5));
          l_MsgGT423.vGT423_Oep.add(Tap);
        }
    for (int i=0; i<15; i++)
        {
          vOev Tap = new vOev();          
          Tap.GT423_Oev.replace(0, Tap.GT423_Oev.capacity(), RUTGEN.Blancos(9));
          l_MsgGT423.vGT423_Oev.add(Tap);
        }
    for (int i=0; i<11; i++)
        {
          Bff_MsgGT423 Tar = new Bff_MsgGT423();
          Tar.GT423_Frv.replace(0, Tar.GT423_Frv.capacity(), RUTGEN.Blancos(8 ));
          for (int j=0; j<15; j++)
              {
                vEpp Tap = new vEpp();          
                Tap.GT423_Epp.replace(0, Tap.GT423_Epp.capacity(), RUTGEN.Blancos(5));
                Tar.vGT423_Epp.add(Tap);
              }
          for (int j=0; j<15; j++)
              {
                vEpv Tap = new vEpv();          
                Tap.GT423_Epv.replace(0, Tap.GT423_Epv.capacity(), RUTGEN.Blancos(9));
                Tar.vGT423_Epv.add(Tap);
              }
          l_MsgGT423.GT423_Tab.add(Tar);        
        }
    l_MsgGT423.GT423_Idx.replace(0, l_MsgGT423.GT423_Idx.capacity(), RUTGEN.Blancos(2  ));
    l_MsgGT423.GT423_Fp1.replace(0, l_MsgGT423.GT423_Fp1.capacity(), RUTGEN.Blancos(8  ));
    l_MsgGT423.GT423_Fp2.replace(0, l_MsgGT423.GT423_Fp2.capacity(), RUTGEN.Blancos(8  ));
    return l_MsgGT423;
  }
  //---------------------------------------------------------------------------------------
  public static Buf_MsgGT423 LSet_A_MsgGT423(String pcStr)
  {
    Buf_MsgGT423 l_MsgGT423 = new Buf_MsgGT423();
    Vector vDatos = LSet_A_vMsgGT423(pcStr);
    l_MsgGT423 = (Buf_MsgGT423)vDatos.elementAt(0);
    return l_MsgGT423;
  }
  //---------------------------------------------------------------------------------------
  public static Vector LSet_A_vMsgGT423(String pcStr)
  {
    Vector vec = new Vector();
    int p = 0;
    String lcData = "";
    Buf_MsgGT423 l_MsgGT423 = new Buf_MsgGT423();
    l_MsgGT423.GT423_Idr.replace(0, l_MsgGT423.GT423_Idr.capacity(), pcStr.substring(p, p + l_MsgGT423.GT423_Idr.capacity())); p = p + l_MsgGT423.GT423_Idr.capacity();
    l_MsgGT423.GT423_Gti.replace(0, l_MsgGT423.GT423_Gti.capacity(), pcStr.substring(p, p + l_MsgGT423.GT423_Gti.capacity())); p = p + l_MsgGT423.GT423_Gti.capacity();
    l_MsgGT423.GT423_Seq.replace(0, l_MsgGT423.GT423_Seq.capacity(), pcStr.substring(p, p + l_MsgGT423.GT423_Seq.capacity())); p = p + l_MsgGT423.GT423_Seq.capacity();
    l_MsgGT423.GT423_Sqd.replace(0, l_MsgGT423.GT423_Sqd.capacity(), pcStr.substring(p, p + l_MsgGT423.GT423_Sqd.capacity())); p = p + l_MsgGT423.GT423_Sqd.capacity();
    l_MsgGT423.GT423_Fts.replace(0, l_MsgGT423.GT423_Fts.capacity(), pcStr.substring(p, p + l_MsgGT423.GT423_Fts.capacity())); p = p + l_MsgGT423.GT423_Fts.capacity();
    l_MsgGT423.GT423_Vuf.replace(0, l_MsgGT423.GT423_Vuf.capacity(), pcStr.substring(p, p + l_MsgGT423.GT423_Vuf.capacity())); p = p + l_MsgGT423.GT423_Vuf.capacity();
    l_MsgGT423.GT423_Vtc.replace(0, l_MsgGT423.GT423_Vtc.capacity(), pcStr.substring(p, p + l_MsgGT423.GT423_Vtc.capacity())); p = p + l_MsgGT423.GT423_Vtc.capacity();
    l_MsgGT423.GT423_Tuf.replace(0, l_MsgGT423.GT423_Tuf.capacity(), pcStr.substring(p, p + l_MsgGT423.GT423_Tuf.capacity())); p = p + l_MsgGT423.GT423_Tuf.capacity();
    l_MsgGT423.GT423_Ttc.replace(0, l_MsgGT423.GT423_Ttc.capacity(), pcStr.substring(p, p + l_MsgGT423.GT423_Ttc.capacity())); p = p + l_MsgGT423.GT423_Ttc.capacity();
    l_MsgGT423.GT423_Tps.replace(0, l_MsgGT423.GT423_Tps.capacity(), pcStr.substring(p, p + l_MsgGT423.GT423_Tps.capacity())); p = p + l_MsgGT423.GT423_Tps.capacity();
    l_MsgGT423.GT423_Dsb.replace(0, l_MsgGT423.GT423_Dsb.capacity(), pcStr.substring(p, p + l_MsgGT423.GT423_Dsb.capacity())); p = p + l_MsgGT423.GT423_Dsb.capacity();
    l_MsgGT423.GT423_Ctr.replace(0, l_MsgGT423.GT423_Ctr.capacity(), pcStr.substring(p, p + l_MsgGT423.GT423_Ctr.capacity())); p = p + l_MsgGT423.GT423_Ctr.capacity();
    l_MsgGT423.GT423_Fin.replace(0, l_MsgGT423.GT423_Fin.capacity(), pcStr.substring(p, p + l_MsgGT423.GT423_Fin.capacity())); p = p + l_MsgGT423.GT423_Fin.capacity();
    l_MsgGT423.GT423_Fte.replace(0, l_MsgGT423.GT423_Fte.capacity(), pcStr.substring(p, p + l_MsgGT423.GT423_Fte.capacity())); p = p + l_MsgGT423.GT423_Fte.capacity();
    l_MsgGT423.GT423_Ped.replace(0, l_MsgGT423.GT423_Ped.capacity(), pcStr.substring(p, p + l_MsgGT423.GT423_Ped.capacity())); p = p + l_MsgGT423.GT423_Ped.capacity();
    l_MsgGT423.GT423_Fed.replace(0, l_MsgGT423.GT423_Fed.capacity(), pcStr.substring(p, p + l_MsgGT423.GT423_Fed.capacity())); p = p + l_MsgGT423.GT423_Fed.capacity();
    l_MsgGT423.GT423_Spc.replace(0, l_MsgGT423.GT423_Spc.capacity(), pcStr.substring(p, p + l_MsgGT423.GT423_Spc.capacity())); p = p + l_MsgGT423.GT423_Spc.capacity();
    l_MsgGT423.GT423_Sps.replace(0, l_MsgGT423.GT423_Sps.capacity(), pcStr.substring(p, p + l_MsgGT423.GT423_Sps.capacity())); p = p + l_MsgGT423.GT423_Sps.capacity();
    l_MsgGT423.GT423_Rem.replace(0, l_MsgGT423.GT423_Rem.capacity(), pcStr.substring(p, p + l_MsgGT423.GT423_Rem.capacity())); p = p + l_MsgGT423.GT423_Rem.capacity();
    l_MsgGT423.GT423_Vsg.replace(0, l_MsgGT423.GT423_Vsg.capacity(), pcStr.substring(p, p + l_MsgGT423.GT423_Vsg.capacity())); p = p + l_MsgGT423.GT423_Vsg.capacity();
    l_MsgGT423.GT423_Dpy.replace(0, l_MsgGT423.GT423_Dpy.capacity(), pcStr.substring(p, p + l_MsgGT423.GT423_Dpy.capacity())); p = p + l_MsgGT423.GT423_Dpy.capacity();
    l_MsgGT423.GT423_Opp.replace(0, l_MsgGT423.GT423_Opp.capacity(), pcStr.substring(p, p + l_MsgGT423.GT423_Opp.capacity())); p = p + l_MsgGT423.GT423_Opp.capacity();
    l_MsgGT423.GT423_Occ.replace(0, l_MsgGT423.GT423_Occ.capacity(), pcStr.substring(p, p + l_MsgGT423.GT423_Occ.capacity())); p = p + l_MsgGT423.GT423_Occ.capacity();
    l_MsgGT423.GT423_Nro.replace(0, l_MsgGT423.GT423_Nro.capacity(), pcStr.substring(p, p + l_MsgGT423.GT423_Nro.capacity())); p = p + l_MsgGT423.GT423_Nro.capacity();
    for (int i=0; i<15; i++)
        {
          vPpv Tap = new vPpv();          
          Tap.GT423_Ppv.replace(0, Tap.GT423_Ppv.capacity(), pcStr.substring(p, p + Tap.GT423_Ppv.capacity())); p = p + Tap.GT423_Ppv.capacity();
          l_MsgGT423.vGT423_Ppv.add(Tap);
        }               
    for (int i=0; i<15; i++)
        {
          vOep Tap = new vOep();          
          Tap.GT423_Oep.replace(0, Tap.GT423_Oep.capacity(), pcStr.substring(p, p + Tap.GT423_Oep.capacity())); p = p + Tap.GT423_Oep.capacity();
          l_MsgGT423.vGT423_Oep.add(Tap);
        }
    for (int i=0; i<15; i++)
        {
          vOev Tap = new vOev();          
          Tap.GT423_Oev.replace(0, Tap.GT423_Oev.capacity(), pcStr.substring(p, p + Tap.GT423_Oev.capacity())); p = p + Tap.GT423_Oev.capacity();
          l_MsgGT423.vGT423_Oev.add(Tap);
        }
    for (int i=0; i<11; i++)
        {
          Bff_MsgGT423 Tar = new Bff_MsgGT423();
          Tar.GT423_Frv.append(pcStr.substring(p, p + Tar.GT423_Frv.capacity())); p = p + Tar.GT423_Frv.capacity();
          for (int j=0; j<15; j++)
              {
                vEpp Tap = new vEpp();          
                Tap.GT423_Epp.replace(0, Tap.GT423_Epp.capacity(), pcStr.substring(p, p + Tap.GT423_Epp.capacity())); p = p + Tap.GT423_Epp.capacity();
                Tar.vGT423_Epp.add(Tap);
              }
          for (int j=0; j<15; j++)
              {
                vEpv Tap = new vEpv();          
                Tap.GT423_Epv.replace(0, Tap.GT423_Epv.capacity(), pcStr.substring(p, p + Tap.GT423_Epv.capacity())); p = p + Tap.GT423_Epv.capacity();
                Tar.vGT423_Epv.add(Tap);
              }
          l_MsgGT423.GT423_Tab.add(Tar);
        }
    l_MsgGT423.GT423_Idx.replace(0, l_MsgGT423.GT423_Idx.capacity(), pcStr.substring(p, p + l_MsgGT423.GT423_Idx.capacity())); p = p + l_MsgGT423.GT423_Idx.capacity();
    l_MsgGT423.GT423_Fp1.replace(0, l_MsgGT423.GT423_Fp1.capacity(), pcStr.substring(p, p + l_MsgGT423.GT423_Fp1.capacity())); p = p + l_MsgGT423.GT423_Fp1.capacity();
    l_MsgGT423.GT423_Fp2.replace(0, l_MsgGT423.GT423_Fp2.capacity(), pcStr.substring(p, p + l_MsgGT423.GT423_Fp2.capacity())); p = p + l_MsgGT423.GT423_Fp2.capacity();
    vec.add(l_MsgGT423);
    return vec;
  }
  //---------------------------------------------------------------------------------------
  public static String LSet_De_MsgGT423 (Buf_MsgGT423 p_MsgGT423)
  {
    String pcStr = "";
    pcStr = pcStr + p_MsgGT423.GT423_Idr.toString();
    pcStr = pcStr + p_MsgGT423.GT423_Gti.toString();
    pcStr = pcStr + p_MsgGT423.GT423_Seq.toString();
    pcStr = pcStr + p_MsgGT423.GT423_Sqd.toString();
    pcStr = pcStr + p_MsgGT423.GT423_Fts.toString();
    pcStr = pcStr + p_MsgGT423.GT423_Vuf.toString();
    pcStr = pcStr + p_MsgGT423.GT423_Vtc.toString();
    pcStr = pcStr + p_MsgGT423.GT423_Tuf.toString();
    pcStr = pcStr + p_MsgGT423.GT423_Ttc.toString();
    pcStr = pcStr + p_MsgGT423.GT423_Tps.toString();
    pcStr = pcStr + p_MsgGT423.GT423_Dsb.toString();
    pcStr = pcStr + p_MsgGT423.GT423_Ctr.toString();
    pcStr = pcStr + p_MsgGT423.GT423_Fin.toString();
    pcStr = pcStr + p_MsgGT423.GT423_Fte.toString();
    pcStr = pcStr + p_MsgGT423.GT423_Ped.toString();
    pcStr = pcStr + p_MsgGT423.GT423_Fed.toString();
    pcStr = pcStr + p_MsgGT423.GT423_Spc.toString();
    pcStr = pcStr + p_MsgGT423.GT423_Sps.toString();
    pcStr = pcStr + p_MsgGT423.GT423_Rem.toString();
    pcStr = pcStr + p_MsgGT423.GT423_Vsg.toString();
    pcStr = pcStr + p_MsgGT423.GT423_Dpy.toString();
    pcStr = pcStr + p_MsgGT423.GT423_Opp.toString();
    pcStr = pcStr + p_MsgGT423.GT423_Occ.toString();
    pcStr = pcStr + p_MsgGT423.GT423_Nro.toString();
    for (int i=0; i<15; i++)
        { pcStr = pcStr + ((vPpv)p_MsgGT423.vGT423_Ppv.elementAt(i)).GT423_Ppv.toString(); }               
    for (int i=0; i<15; i++)
        { pcStr = pcStr + ((vOep)p_MsgGT423.vGT423_Oep.elementAt(i)).GT423_Oep.toString(); }               
    for (int i=0; i<15; i++)
        { pcStr = pcStr + ((vOev)p_MsgGT423.vGT423_Oev.elementAt(i)).GT423_Oev.toString(); }               
    for (int i=0; i<11; i++)
        {
          pcStr = pcStr + ((Bff_MsgGT423)p_MsgGT423.GT423_Tab.elementAt(i)).GT423_Frv.toString();
          for (int j=0; j<15; j++)
              { pcStr = pcStr + ((vEpp)((Bff_MsgGT423)p_MsgGT423.GT423_Tab.elementAt(i)).vGT423_Epp.elementAt(j)).GT423_Epp.toString(); }               
          for (int j=0; j<15; j++)
              { pcStr = pcStr + ((vEpv)((Bff_MsgGT423)p_MsgGT423.GT423_Tab.elementAt(i)).vGT423_Epv.elementAt(j)).GT423_Epv.toString(); }               
        }
    pcStr = pcStr + p_MsgGT423.GT423_Idx.toString();
    pcStr = pcStr + p_MsgGT423.GT423_Fp1.toString();
    pcStr = pcStr + p_MsgGT423.GT423_Fp2.toString();
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
  public static class Buf_vEao
  {
    public StringBuffer vEao_Tit = new StringBuffer(25);   //X(25)          Titulo        
    public StringBuffer vEao_Ppv = new StringBuffer(9 );   //9(07)V9(2)     VPresupuesto        
    public StringBuffer vEao_Oep = new StringBuffer(5 );   //9(03)V9(2)     %AvFecha            
    public StringBuffer vEao_Oev = new StringBuffer(9 );   //9(07)V9(2)     VAvFecha            
    public StringBuffer vEao_Ep1 = new StringBuffer(5 );   //9(03)V9(2)     %AvFecha EP1
    public StringBuffer vEao_Ev1 = new StringBuffer(9 );   //9(07)V9(2)     VAvFecha EP1
    public StringBuffer vEao_Ep2 = new StringBuffer(5 );   //9(03)V9(2)     %AvFecha EP2
    public StringBuffer vEao_Ev2 = new StringBuffer(9 );   //9(07)V9(2)     VAvFecha EP2

    public Formateo f = new Formateo();
    public String getTit()        {return vEao_Tit.toString();}
    public String fgetPpv()       {return f.Formato(vEao_Ppv.toString(),"99.999.999.999",2,2,',');}
    public String fgetOep()       {return f.Formato(vEao_Oep.toString(),"999",2,2,',');}
    public String fgetOev()       {return f.Formato(vEao_Oev.toString(),"99.999.999.999",2,2,',');}
    public String fgetEp1()       {return f.Formato(vEao_Ep1.toString(),"999",2,2,',');}
    public String fgetEv1()       {return f.Formato(vEao_Ev1.toString(),"99.999.999.999",2,2,',');}
    public String fgetEp2()       {return f.Formato(vEao_Ep2.toString(),"999",2,2,',');}
    public String fgetEv2()       {return f.Formato(vEao_Ev2.toString(),"99.999.999.999",2,2,',');}
  }
  //---------------------------------------------------------------------------------------
}