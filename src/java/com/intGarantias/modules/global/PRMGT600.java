// Source File Name:   PRMGT600.java

package com.intGarantias.modules.global;

import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.util.Formateo;

import java.util.*;
import java.lang.*;
import org.apache.turbine.util.Log;

public class PRMGT600
{
  //---------------------------------------------------------------------------------------
  public static class Buf_PrmGT600
  {
    public StringBuffer GT600_Ano = new StringBuffer(4 );  //'9(04)        4 Ano      
    public StringBuffer GT600_Mes = new StringBuffer(2 );  //'9(02)        6 Mes                 
    public StringBuffer GT600_Nms = new StringBuffer(10);  //'X(10)       16 Nombre Mes                 
    public StringBuffer GT600_Tpr = new StringBuffer(3 );  //'X(03)       19 Tipo Consulta               

    public String getAno()         {return GT600_Ano.toString();}
    public String getMes()         {return GT600_Mes.toString();}
    public String getNms()         {return GT600_Nms.toString();}
    public String getTpr()         {return GT600_Tpr.toString();}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_PrmGT600 Inicia_PrmGT600 ()
  {
    Buf_PrmGT600 l_PrmGT600 = new Buf_PrmGT600();
    l_PrmGT600.GT600_Ano.replace(0, l_PrmGT600.GT600_Ano.capacity(), RUTGEN.Blancos(4 ));
    l_PrmGT600.GT600_Mes.replace(0, l_PrmGT600.GT600_Mes.capacity(), RUTGEN.Blancos(2 ));
    l_PrmGT600.GT600_Nms.replace(0, l_PrmGT600.GT600_Nms.capacity(), RUTGEN.Blancos(10));
    l_PrmGT600.GT600_Tpr.replace(0, l_PrmGT600.GT600_Tpr.capacity(), RUTGEN.Blancos(3 ));
    return l_PrmGT600;
  }
  //---------------------------------------------------------------------------------------
  public static Buf_PrmGT600 LSet_A_PrmGT600(String pcStr)
  {
    Buf_PrmGT600 l_PrmGT600 = new Buf_PrmGT600();
    Vector vDatos = LSet_A_vPrmGT600(pcStr);
    l_PrmGT600 = (Buf_PrmGT600)vDatos.elementAt(0);
    return l_PrmGT600;
  }
  //---------------------------------------------------------------------------------------
  public static Vector LSet_A_vPrmGT600(String pcStr)
  {
    Vector vec = new Vector();
    int p = 0;
    Buf_PrmGT600 l_PrmGT600 = new Buf_PrmGT600();
    l_PrmGT600.GT600_Ano.replace(0, l_PrmGT600.GT600_Ano.capacity(), pcStr.substring(p, p + l_PrmGT600.GT600_Ano.capacity())); p = p + l_PrmGT600.GT600_Ano.capacity();
    l_PrmGT600.GT600_Mes.replace(0, l_PrmGT600.GT600_Mes.capacity(), pcStr.substring(p, p + l_PrmGT600.GT600_Mes.capacity())); p = p + l_PrmGT600.GT600_Mes.capacity();
    l_PrmGT600.GT600_Nms.replace(0, l_PrmGT600.GT600_Nms.capacity(), pcStr.substring(p, p + l_PrmGT600.GT600_Nms.capacity())); p = p + l_PrmGT600.GT600_Nms.capacity();
    l_PrmGT600.GT600_Tpr.replace(0, l_PrmGT600.GT600_Tpr.capacity(), pcStr.substring(p, p + l_PrmGT600.GT600_Tpr.capacity())); p = p + l_PrmGT600.GT600_Tpr.capacity();
    vec.add (l_PrmGT600);                          
    return vec;
  }
  //---------------------------------------------------------------------------------------
  public static String LSet_De_PrmGT600 (Buf_PrmGT600 p_PrmGT600)
  {
    String pcStr = "";
    pcStr = pcStr + p_PrmGT600.GT600_Ano.toString();
    pcStr = pcStr + p_PrmGT600.GT600_Mes.toString();
    pcStr = pcStr + p_PrmGT600.GT600_Nms.toString();
    pcStr = pcStr + p_PrmGT600.GT600_Tpr.toString();
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
}