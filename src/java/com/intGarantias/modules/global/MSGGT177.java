// Source File Name:   MSGGT177.java

package com.intGarantias.modules.global;

import java.util.Vector;

import com.FHTServlet.modules.global.RUTGEN;

public class MSGGT177 {
	public static class Buf_MsgGT177 {

		public String getIdr() {
			return GT177_Idr.toString();
		}

		public String getCli() {
			return GT177_Cli.toString();
		}

		public String getNro() {
			return GT177_Nro.toString();
		}

		public Vector getTab() {
			return GT177_Tab;
		}

		public StringBuffer GT177_Idr;
		public StringBuffer GT177_Cli;
		public StringBuffer GT177_Nro;
		public Vector GT177_Tab;

		public Buf_MsgGT177() {
			GT177_Idr = new StringBuffer(1);
			GT177_Cli = new StringBuffer(10);
			GT177_Nro = new StringBuffer(3);
			GT177_Tab = new Vector();
		}
	}

	public static class Bff_MsgGT177 {

		public String getGbs() {
			return Mod_Gbs.toString();
		}

		public String getDsg() {
			return Mod_Dsg.toString();
		}

		public String getDsb() {
			return Mod_Dsb.toString();
		}

		public String getUmt() {
			return Mod_Umt.toString();
		}

		public String getUdc() {
			return Mod_Udc.toString();
		}

		public String getUmo() {
			return Mod_Umo.toString();
		}

		public StringBuffer Mod_Gbs;
		public StringBuffer Mod_Dsg;
		public StringBuffer Mod_Dsb;
		public StringBuffer Mod_Umt;
		public StringBuffer Mod_Udc;
		public StringBuffer Mod_Umo;

		public Bff_MsgGT177() {
			Mod_Gbs = new StringBuffer(13);
			Mod_Dsg = new StringBuffer(100);
			Mod_Dsb = new StringBuffer(100);
			Mod_Umt = new StringBuffer(3);
			Mod_Udc = new StringBuffer(1);
			Mod_Umo = new StringBuffer(15);
		}
	}

	public MSGGT177() {
	}

	public static Buf_MsgGT177 Inicia_MsgGT177() {
		Buf_MsgGT177 l_MsgGT177 = new Buf_MsgGT177();
		l_MsgGT177.GT177_Idr.replace(0, l_MsgGT177.GT177_Idr.capacity(),
				RUTGEN.Blancos(1));
		l_MsgGT177.GT177_Cli.replace(0, l_MsgGT177.GT177_Cli.capacity(),
				RUTGEN.Blancos(10));
		l_MsgGT177.GT177_Nro.replace(0, l_MsgGT177.GT177_Nro.capacity(),
				RUTGEN.Blancos(3));
		for (int i = 0; i < g_Max_GT177; i++) {
			Bff_MsgGT177 Tap = new Bff_MsgGT177();
			Tap.Mod_Gbs.replace(0, Tap.Mod_Gbs.capacity(), RUTGEN.Blancos(13));
			Tap.Mod_Dsg.replace(0, Tap.Mod_Dsg.capacity(), RUTGEN.Blancos(100));
			Tap.Mod_Dsb.replace(0, Tap.Mod_Dsb.capacity(), RUTGEN.Blancos(100));
			Tap.Mod_Umt.replace(0, Tap.Mod_Umt.capacity(), RUTGEN.Blancos(3));
			Tap.Mod_Udc.replace(0, Tap.Mod_Udc.capacity(), RUTGEN.Blancos(1));
			Tap.Mod_Umo.replace(0, Tap.Mod_Umo.capacity(), RUTGEN.Blancos(15));
			l_MsgGT177.GT177_Tab.add(Tap);
		}

		return l_MsgGT177;
	}

	public static Buf_MsgGT177 LSet_A_MsgGT177(String pcStr) {
		Buf_MsgGT177 l_MsgGT177 = new Buf_MsgGT177();
		MSGGT177 vMsgGT177 = new MSGGT177();
		Vector vDatos = vMsgGT177.LSet_A_vMsgGT177(pcStr);
		l_MsgGT177 = (Buf_MsgGT177) vDatos.elementAt(0);
		return l_MsgGT177;
	}

	public Vector LSet_A_vMsgGT177(String pcStr) {
		Vector vec = new Vector();
		int p = 0;
		Buf_MsgGT177 l_MsgGT177 = new Buf_MsgGT177();
		l_MsgGT177.GT177_Idr.append(pcStr.substring(p,
				p + l_MsgGT177.GT177_Idr.capacity()));
		p += l_MsgGT177.GT177_Idr.capacity();
		l_MsgGT177.GT177_Cli.append(pcStr.substring(p,
				p + l_MsgGT177.GT177_Cli.capacity()));
		p += l_MsgGT177.GT177_Cli.capacity();
		l_MsgGT177.GT177_Nro.append(pcStr.substring(p,
				p + l_MsgGT177.GT177_Nro.capacity()));
		p += l_MsgGT177.GT177_Nro.capacity();
		for (int i = 0; i < Integer.parseInt(l_MsgGT177.GT177_Nro.toString()); i++) {
			Bff_MsgGT177 Tap = new Bff_MsgGT177();
			Tap.Mod_Gbs.append(pcStr.substring(p, p + Tap.Mod_Gbs.capacity()));
			p += Tap.Mod_Gbs.capacity();
			Tap.Mod_Dsg.append(pcStr.substring(p, p + Tap.Mod_Dsg.capacity()));
			p += Tap.Mod_Dsg.capacity();
			Tap.Mod_Dsb.append(pcStr.substring(p, p + Tap.Mod_Dsb.capacity()));
			p += Tap.Mod_Dsb.capacity();
			Tap.Mod_Umt.append(pcStr.substring(p, p + Tap.Mod_Umt.capacity()));
			p += Tap.Mod_Umt.capacity();
			Tap.Mod_Udc.append(pcStr.substring(p, p + Tap.Mod_Udc.capacity()));
			p += Tap.Mod_Udc.capacity();
			Tap.Mod_Umo.append(pcStr.substring(p, p + Tap.Mod_Umo.capacity()));
			p += Tap.Mod_Umo.capacity();
			l_MsgGT177.GT177_Tab.add(Tap);
		}

		vec.add(l_MsgGT177);
		return vec;
	}

	public static String LSet_De_MsgGT177(Buf_MsgGT177 p_MsgGT177) {
		String pcStr = "";
		pcStr = pcStr + p_MsgGT177.GT177_Idr.toString();
		pcStr = pcStr + p_MsgGT177.GT177_Cli.toString();
		pcStr = pcStr + p_MsgGT177.GT177_Nro.toString();
		for (int i = 0; i < p_MsgGT177.GT177_Tab.size(); i++) {
			pcStr = pcStr
					+ ((Bff_MsgGT177) p_MsgGT177.GT177_Tab.elementAt(i)).Mod_Gbs
							.toString();
			pcStr = pcStr
					+ ((Bff_MsgGT177) p_MsgGT177.GT177_Tab.elementAt(i)).Mod_Dsg
							.toString();
			pcStr = pcStr
					+ ((Bff_MsgGT177) p_MsgGT177.GT177_Tab.elementAt(i)).Mod_Dsb
							.toString();
			pcStr = pcStr
					+ ((Bff_MsgGT177) p_MsgGT177.GT177_Tab.elementAt(i)).Mod_Umt
							.toString();
			pcStr = pcStr
					+ ((Bff_MsgGT177) p_MsgGT177.GT177_Tab.elementAt(i)).Mod_Udc
							.toString();
			pcStr = pcStr
					+ ((Bff_MsgGT177) p_MsgGT177.GT177_Tab.elementAt(i)).Mod_Umo
							.toString();
		}

		return pcStr;
	}

	public static int g_Max_GT177 = 15;

}
