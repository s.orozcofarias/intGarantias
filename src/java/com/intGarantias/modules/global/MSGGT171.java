// Source File Name:   MSGGT171.java

package com.intGarantias.modules.global;

import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.util.Formateo;

import java.util.*;
import java.lang.*;
import org.apache.turbine.util.Log;

public class MSGGT171
{
  public static int g_Max_GT171 = 39;
  //---------------------------------------------------------------------------------------
  public static class Bff_MsgGT171
  {
    public StringBuffer GtAlz_Seq = new StringBuffer(3 );       //9(03)        3 Secuencia Detalle
    public StringBuffer GtAlz_Itb = new StringBuffer(2 );       //X(02)        5 Tipo Bien (UV)
    public StringBuffer GtAlz_Dsb = new StringBuffer(30);       //X(30)       35 Descripcion Garantia 
    public StringBuffer GtAlz_Vts = new StringBuffer(13);       //9(11)V9(2)  48 Valor Tasacion       
    public StringBuffer GtAlz_Vcn = new StringBuffer(13);       //9(11)V9(2)  61 Valor Garantia       
    public StringBuffer GtAlz_Est = new StringBuffer(5 );       //X(05)       66 Estado Garantia
    public StringBuffer GtAlz_Sqb = new StringBuffer(3 );       //9(03)       69 Secuencia Bien

    public String getSeq()      {return GtAlz_Seq.toString();}
    public String getItb()      {return GtAlz_Itb.toString();}
    public String getDsb()      {return GtAlz_Dsb.toString();}
    public String getVts()      {return GtAlz_Vts.toString();}
    public String getVcn()      {return GtAlz_Vcn.toString();}
    public String getEst()      {return GtAlz_Est.toString();}
    public String getSqb()      {return GtAlz_Sqb.toString();}
  }
  //---------------------------------------------------------------------------------------
  public static class Buf_MsgGT171
  {
    public StringBuffer GT171_Idr     = new StringBuffer(1  );
    public StringBuffer GT171_Img     = new StringBuffer(827);
    public StringBuffer GT171_Swt_Dcn = new StringBuffer(1  );
    public StringBuffer GT171_Swt_Cnr = new StringBuffer(1  );
    public StringBuffer GT171_Swt_Gti = new StringBuffer(1  );
    public StringBuffer GT171_Swt_Evt = new StringBuffer(1  );
    public StringBuffer GT171_Swt_Cli = new StringBuffer(1  );
    public StringBuffer GT171_Idt     = new StringBuffer(1  );
    public StringBuffer GT171_Sqb     = new StringBuffer(3  );
    public StringBuffer GT171_Nro     = new StringBuffer(2  );
    public Vector       GT171_Alz     = new Vector();

    public String getIdr()           {return GT171_Idr.toString();}
    public String getImg()           {return GT171_Img.toString();}
    public String getSwt_Dcn()       {return GT171_Swt_Dcn.toString();}
    public String getSwt_Cnr()       {return GT171_Swt_Cnr.toString();}
    public String getSwt_Gti()       {return GT171_Swt_Gti.toString();}
    public String getSwt_Evt()       {return GT171_Swt_Evt.toString();}
    public String getSwt_Cli()       {return GT171_Swt_Cli.toString();}
    public String getIdt()           {return GT171_Idt.toString();}
    public String getSqb()           {return GT171_Sqb.toString();}
    public String getNro()           {return GT171_Nro.toString();}
    public Vector getAlz()           {return GT171_Alz;}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_MsgGT171 Inicia_MsgGT171()
  {
    Buf_MsgGT171 l_MsgGT171 = new Buf_MsgGT171();
    l_MsgGT171.GT171_Idr.replace    (0,l_MsgGT171.GT171_Idr.capacity(),     RUTGEN.Blancos(1  ));
    l_MsgGT171.GT171_Img.replace    (0,l_MsgGT171.GT171_Img.capacity(),     RUTGEN.Blancos(827));
    l_MsgGT171.GT171_Swt_Dcn.replace(0,l_MsgGT171.GT171_Swt_Dcn.capacity(), RUTGEN.Blancos(1  ));
    l_MsgGT171.GT171_Swt_Cnr.replace(0,l_MsgGT171.GT171_Swt_Cnr.capacity(), RUTGEN.Blancos(1  ));
    l_MsgGT171.GT171_Swt_Gti.replace(0,l_MsgGT171.GT171_Swt_Gti.capacity(), RUTGEN.Blancos(1  ));
    l_MsgGT171.GT171_Swt_Evt.replace(0,l_MsgGT171.GT171_Swt_Evt.capacity(), RUTGEN.Blancos(1  ));
    l_MsgGT171.GT171_Swt_Cli.replace(0,l_MsgGT171.GT171_Swt_Cli.capacity(), RUTGEN.Blancos(1  ));
    l_MsgGT171.GT171_Idt.replace    (0,l_MsgGT171.GT171_Idt.capacity(),     RUTGEN.Blancos(1  ));
    l_MsgGT171.GT171_Sqb.replace    (0,l_MsgGT171.GT171_Sqb.capacity(),     RUTGEN.Blancos(3  ));
    l_MsgGT171.GT171_Nro.replace    (0,l_MsgGT171.GT171_Nro.capacity(),     RUTGEN.Blancos(2  ));
    int p = 0;
    for (int i=0; i<g_Max_GT171; i++)
        {
          Bff_MsgGT171 Tap = new Bff_MsgGT171();
          Tap.GtAlz_Seq.replace(0, Tap.GtAlz_Seq.capacity(), RUTGEN.Blancos(3 ));
          Tap.GtAlz_Itb.replace(0, Tap.GtAlz_Itb.capacity(), RUTGEN.Blancos(2 ));
          Tap.GtAlz_Dsb.replace(0, Tap.GtAlz_Dsb.capacity(), RUTGEN.Blancos(30));
          Tap.GtAlz_Vts.replace(0, Tap.GtAlz_Vts.capacity(), RUTGEN.Blancos(13));
          Tap.GtAlz_Vcn.replace(0, Tap.GtAlz_Vcn.capacity(), RUTGEN.Blancos(13));
          Tap.GtAlz_Est.replace(0, Tap.GtAlz_Est.capacity(), RUTGEN.Blancos(5 ));
          Tap.GtAlz_Sqb.replace(0, Tap.GtAlz_Sqb.capacity(), RUTGEN.Blancos(3 ));
          l_MsgGT171.GT171_Alz.add(Tap);        
        }
    return l_MsgGT171;
  }
  //---------------------------------------------------------------------------------------
  static public Buf_MsgGT171 LSet_A_MsgGT171(String pcStr)
  {
    Buf_MsgGT171 l_MsgGT171 = new Buf_MsgGT171();
    MSGGT171 vMsgGT171 = new MSGGT171();
    Vector vDatos = vMsgGT171.LSet_A_vMsgGT171(pcStr);
    l_MsgGT171 = (MSGGT171.Buf_MsgGT171)vDatos.elementAt(0);
    return l_MsgGT171;
  }
  //---------------------------------------------------------------------------------------
  public Vector LSet_A_vMsgGT171(String pcStr)
  {
    Vector vec = new Vector();
    int p = 0;
    Buf_MsgGT171 l_MsgGT171 = new Buf_MsgGT171();                            
    l_MsgGT171.GT171_Idr.append     (pcStr.substring(p, p + l_MsgGT171.GT171_Idr.capacity()));     p = p + l_MsgGT171.GT171_Idr.capacity();
    l_MsgGT171.GT171_Img.append     (pcStr.substring(p, p + l_MsgGT171.GT171_Img.capacity()));     p = p + l_MsgGT171.GT171_Img.capacity();
    l_MsgGT171.GT171_Swt_Dcn.append (pcStr.substring(p, p + l_MsgGT171.GT171_Swt_Dcn.capacity())); p = p + l_MsgGT171.GT171_Swt_Dcn.capacity();
    l_MsgGT171.GT171_Swt_Cnr.append (pcStr.substring(p, p + l_MsgGT171.GT171_Swt_Cnr.capacity())); p = p + l_MsgGT171.GT171_Swt_Cnr.capacity();
    l_MsgGT171.GT171_Swt_Gti.append (pcStr.substring(p, p + l_MsgGT171.GT171_Swt_Gti.capacity())); p = p + l_MsgGT171.GT171_Swt_Gti.capacity();
    l_MsgGT171.GT171_Swt_Evt.append (pcStr.substring(p, p + l_MsgGT171.GT171_Swt_Evt.capacity())); p = p + l_MsgGT171.GT171_Swt_Evt.capacity();
    l_MsgGT171.GT171_Swt_Cli.append (pcStr.substring(p, p + l_MsgGT171.GT171_Swt_Cli.capacity())); p = p + l_MsgGT171.GT171_Swt_Cli.capacity();
    l_MsgGT171.GT171_Idt.append     (pcStr.substring(p, p + l_MsgGT171.GT171_Idt.capacity()));     p = p + l_MsgGT171.GT171_Idt.capacity();
    l_MsgGT171.GT171_Sqb.append     (pcStr.substring(p, p + l_MsgGT171.GT171_Sqb.capacity()));     p = p + l_MsgGT171.GT171_Sqb.capacity();
    l_MsgGT171.GT171_Nro.append     (pcStr.substring(p, p + l_MsgGT171.GT171_Nro.capacity()));     p = p + l_MsgGT171.GT171_Nro.capacity();
//    for (int i=0; i<Integer.parseInt(l_MsgGT171.GT171_Nro.toString()); i++)
    for (int i=0; i<g_Max_GT171; i++)
        {
          Bff_MsgGT171 Tap = new Bff_MsgGT171();
          Tap.GtAlz_Seq.append(pcStr.substring(p, p + Tap.GtAlz_Seq.capacity())); p = p + Tap.GtAlz_Seq.capacity();
          Tap.GtAlz_Itb.append(pcStr.substring(p, p + Tap.GtAlz_Itb.capacity())); p = p + Tap.GtAlz_Itb.capacity();
          Tap.GtAlz_Dsb.append(pcStr.substring(p, p + Tap.GtAlz_Dsb.capacity())); p = p + Tap.GtAlz_Dsb.capacity();
          Tap.GtAlz_Vts.append(pcStr.substring(p, p + Tap.GtAlz_Vts.capacity())); p = p + Tap.GtAlz_Vts.capacity();
          Tap.GtAlz_Vcn.append(pcStr.substring(p, p + Tap.GtAlz_Vcn.capacity())); p = p + Tap.GtAlz_Vcn.capacity();
          Tap.GtAlz_Est.append(pcStr.substring(p, p + Tap.GtAlz_Est.capacity())); p = p + Tap.GtAlz_Est.capacity();
          Tap.GtAlz_Sqb.append(pcStr.substring(p, p + Tap.GtAlz_Sqb.capacity())); p = p + Tap.GtAlz_Sqb.capacity();
          l_MsgGT171.GT171_Alz.add(Tap);
        }
    vec.add (l_MsgGT171);
    return vec;
  }
  //---------------------------------------------------------------------------------------
  static public String LSet_De_MsgGT171(Buf_MsgGT171 p_MsgGT171)
  {
    String pcStr = "";
    pcStr = pcStr + p_MsgGT171.GT171_Idr.toString();
    pcStr = pcStr + p_MsgGT171.GT171_Img.toString();
    pcStr = pcStr + p_MsgGT171.GT171_Swt_Dcn.toString();
    pcStr = pcStr + p_MsgGT171.GT171_Swt_Cnr.toString();
    pcStr = pcStr + p_MsgGT171.GT171_Swt_Gti.toString();
    pcStr = pcStr + p_MsgGT171.GT171_Swt_Evt.toString();
    pcStr = pcStr + p_MsgGT171.GT171_Swt_Cli.toString();
    pcStr = pcStr + p_MsgGT171.GT171_Idt.toString();
    pcStr = pcStr + p_MsgGT171.GT171_Sqb.toString();
    pcStr = pcStr + p_MsgGT171.GT171_Nro.toString();
    for (int i=0; i<p_MsgGT171.GT171_Alz.size(); i++)
        {
          pcStr = pcStr + ((Bff_MsgGT171)p_MsgGT171.GT171_Alz.elementAt(i)).GtAlz_Seq.toString();
          pcStr = pcStr + ((Bff_MsgGT171)p_MsgGT171.GT171_Alz.elementAt(i)).GtAlz_Itb.toString();
          pcStr = pcStr + ((Bff_MsgGT171)p_MsgGT171.GT171_Alz.elementAt(i)).GtAlz_Dsb.toString();
          pcStr = pcStr + ((Bff_MsgGT171)p_MsgGT171.GT171_Alz.elementAt(i)).GtAlz_Vts.toString();
          pcStr = pcStr + ((Bff_MsgGT171)p_MsgGT171.GT171_Alz.elementAt(i)).GtAlz_Vcn.toString();
          pcStr = pcStr + ((Bff_MsgGT171)p_MsgGT171.GT171_Alz.elementAt(i)).GtAlz_Est.toString();
          pcStr = pcStr + ((Bff_MsgGT171)p_MsgGT171.GT171_Alz.elementAt(i)).GtAlz_Sqb.toString();
        }
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
}