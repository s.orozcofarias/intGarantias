// Source File Name:   MSGGT902.java

package com.intGarantias.modules.global;

import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.util.Formateo;

import java.util.*;
import java.lang.*;
import org.apache.turbine.util.Log;

public class MSGGT902
{
  public static int g_Max_GT902 = 23;
  //---------------------------------------------------------------------------------------
  public static class Bff_MsgGT902
  {
    public StringBuffer GT902_Itb = new StringBuffer(2  );      //X(02)        2 Tipo Bien             
    public StringBuffer GT902_Dpc = new StringBuffer(10 );      //X(10)       12 PContable
    public StringBuffer GT902_Dsb = new StringBuffer(100);      //X(100)     112 Descripcion Bien      
    public StringBuffer GT902_Vcn = new StringBuffer(15 );      //9(13)V9(2) 127 Valor Contab ORDPPAL  
    public StringBuffer GT902_Mrj = new StringBuffer(15 );      //9(11)V9(4) 142 Valor MO ORDPPAL      
    public StringBuffer GT902_Fvb = new StringBuffer(8  );      //9(08)      150 FVencto Bien          

    public String getItb()         {return GT902_Itb.toString();}
    public String getDpc()         {return GT902_Dpc.toString();}
    public String getDsb()         {return GT902_Dsb.toString();}
    public String getVcn()         {return GT902_Vcn.toString();}
    public String getMrj()         {return GT902_Mrj.toString();}
    public String getFvb()         {return GT902_Fvb.toString();}
  }
  //---------------------------------------------------------------------------------------
  public static class Buf_MsgGT902
  {
    public StringBuffer GT902_Idr = new StringBuffer(1);
    public StringBuffer GT902_Ntr = new StringBuffer(7);
    public StringBuffer GT902_Dsd = new StringBuffer(3);
    public StringBuffer GT902_Nro = new StringBuffer(2);
    public Vector       GT902_Tab = new Vector();

    public String getIdr()         {return GT902_Idr.toString();}
    public String getNtr()         {return GT902_Ntr.toString();}
    public String getDsd()         {return GT902_Dsd.toString();}
    public String getNro()         {return GT902_Nro.toString();}
    public Vector getTab()         {return GT902_Tab;}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_MsgGT902 Inicia_MsgGT902()
  {
    Buf_MsgGT902 l_MsgGT902 = new Buf_MsgGT902();
    l_MsgGT902.GT902_Idr.replace(0, l_MsgGT902.GT902_Idr.capacity(), RUTGEN.Blancos(1));
    l_MsgGT902.GT902_Ntr.replace(0, l_MsgGT902.GT902_Ntr.capacity(), RUTGEN.Blancos(7));
    l_MsgGT902.GT902_Dsd.replace(0, l_MsgGT902.GT902_Dsd.capacity(), RUTGEN.Blancos(3));
    l_MsgGT902.GT902_Nro.replace(0, l_MsgGT902.GT902_Nro.capacity(), RUTGEN.Blancos(2));
    int p = 0;
    for (int i=0; i<g_Max_GT902; i++)
        {
          Bff_MsgGT902 Tap = new Bff_MsgGT902();
          Tap.GT902_Itb.replace(0, Tap.GT902_Itb.capacity(), RUTGEN.Blancos(2  ));
          Tap.GT902_Dpc.replace(0, Tap.GT902_Dpc.capacity(), RUTGEN.Blancos(10 ));
          Tap.GT902_Dsb.replace(0, Tap.GT902_Dsb.capacity(), RUTGEN.Blancos(100));
          Tap.GT902_Vcn.replace(0, Tap.GT902_Vcn.capacity(), RUTGEN.Blancos(15 ));
          Tap.GT902_Mrj.replace(0, Tap.GT902_Mrj.capacity(), RUTGEN.Blancos(15 ));
          Tap.GT902_Fvb.replace(0, Tap.GT902_Fvb.capacity(), RUTGEN.Blancos(8  ));
          l_MsgGT902.GT902_Tab.add(Tap);
        }
    return l_MsgGT902;
  }
  //---------------------------------------------------------------------------------------
  static public Buf_MsgGT902 LSet_A_MsgGT902(String pcStr)
  {
    Buf_MsgGT902 l_MsgGT902 = new Buf_MsgGT902();
    MSGGT902 vMsgGT902 = new MSGGT902();
    Vector vDatos = vMsgGT902.LSet_A_vMsgGT902(pcStr);
    l_MsgGT902 = (MSGGT902.Buf_MsgGT902)vDatos.elementAt(0);
    return l_MsgGT902;
  }
  //---------------------------------------------------------------------------------------
  public Vector LSet_A_vMsgGT902(String pcStr)
  {
    Vector vec = new Vector();
    int p = 0;
    Buf_MsgGT902 l_MsgGT902 = new Buf_MsgGT902();                            
    l_MsgGT902.GT902_Idr.append(pcStr.substring(p, p + l_MsgGT902.GT902_Idr.capacity())); p = p + l_MsgGT902.GT902_Idr.capacity();
    l_MsgGT902.GT902_Ntr.append(pcStr.substring(p, p + l_MsgGT902.GT902_Ntr.capacity())); p = p + l_MsgGT902.GT902_Ntr.capacity();
    l_MsgGT902.GT902_Dsd.append(pcStr.substring(p, p + l_MsgGT902.GT902_Dsd.capacity())); p = p + l_MsgGT902.GT902_Dsd.capacity();
    l_MsgGT902.GT902_Nro.append(pcStr.substring(p, p + l_MsgGT902.GT902_Nro.capacity())); p = p + l_MsgGT902.GT902_Nro.capacity();
    //for (int i=0; i<Integer.parseInt(l_MsgGT902.GT902_Nro.toString()); i++)
    for (int i=0; i<g_Max_GT902; i++)
        {
          Bff_MsgGT902 Tap = new Bff_MsgGT902();
          Tap.GT902_Itb.append(pcStr.substring(p, p + Tap.GT902_Itb.capacity())); p = p + Tap.GT902_Itb.capacity();
          Tap.GT902_Dpc.append(pcStr.substring(p, p + Tap.GT902_Dpc.capacity())); p = p + Tap.GT902_Dpc.capacity();
          Tap.GT902_Dsb.append(pcStr.substring(p, p + Tap.GT902_Dsb.capacity())); p = p + Tap.GT902_Dsb.capacity();
          Tap.GT902_Vcn.append(pcStr.substring(p, p + Tap.GT902_Vcn.capacity())); p = p + Tap.GT902_Vcn.capacity();
          Tap.GT902_Mrj.append(pcStr.substring(p, p + Tap.GT902_Mrj.capacity())); p = p + Tap.GT902_Mrj.capacity();
          Tap.GT902_Fvb.append(pcStr.substring(p, p + Tap.GT902_Fvb.capacity())); p = p + Tap.GT902_Fvb.capacity();
          l_MsgGT902.GT902_Tab.add(Tap);
        }
    vec.add (l_MsgGT902);
    return vec;
  }
  //---------------------------------------------------------------------------------------
  static public String LSet_De_MsgGT902(Buf_MsgGT902 p_MsgGT902)
  {
    String pcStr = "";
    pcStr = pcStr + p_MsgGT902.GT902_Idr.toString();
    pcStr = pcStr + p_MsgGT902.GT902_Ntr.toString();
    pcStr = pcStr + p_MsgGT902.GT902_Dsd.toString();
    pcStr = pcStr + p_MsgGT902.GT902_Nro.toString();
    for (int i=0; i<p_MsgGT902.GT902_Tab.size(); i++)
        {
          pcStr = pcStr + ((Bff_MsgGT902)p_MsgGT902.GT902_Tab.elementAt(i)).GT902_Itb.toString();
          pcStr = pcStr + ((Bff_MsgGT902)p_MsgGT902.GT902_Tab.elementAt(i)).GT902_Dpc.toString();
          pcStr = pcStr + ((Bff_MsgGT902)p_MsgGT902.GT902_Tab.elementAt(i)).GT902_Dsb.toString();
          pcStr = pcStr + ((Bff_MsgGT902)p_MsgGT902.GT902_Tab.elementAt(i)).GT902_Vcn.toString();
          pcStr = pcStr + ((Bff_MsgGT902)p_MsgGT902.GT902_Tab.elementAt(i)).GT902_Mrj.toString();
          pcStr = pcStr + ((Bff_MsgGT902)p_MsgGT902.GT902_Tab.elementAt(i)).GT902_Fvb.toString();
        }
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
}