// Source File Name:   MSGGT176.java

package com.intGarantias.modules.global;

import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.util.Formateo;

import java.util.*;
import java.lang.*;
import org.apache.turbine.util.Log;

public class MSGGT176
{
  public static int g_Max_GT176 = 50;
  //---------------------------------------------------------------------------------------
  public static class Bff_MsgGT176
  {
    public StringBuffer Mod_Cli = new StringBuffer(10);       //X(10)       10 Id. Garantia           
    public StringBuffer Mod_Rco = new StringBuffer(5 );       //X(05)       15 Rel. Cliente/Credito   
    public StringBuffer Mod_Rel = new StringBuffer(5 );       //X(05)       20 Rel. Cliente/Garantia  
    public StringBuffer Mod_Cre = new StringBuffer(22);       //X(22)       42 Id. Credito            
    public StringBuffer Mod_Ncl = new StringBuffer(25);       //X(25)       67 Nombre Cliente         

    public String getCli()       {return Mod_Cli.toString();}
    public String getRco()       {return Mod_Rco.toString();}
    public String getRel()       {return Mod_Rel.toString();}
    public String getCre()       {return Mod_Cre.toString();}
    public String getNcl()       {return Mod_Ncl.toString();}
  }
  //---------------------------------------------------------------------------------------
  public static class Buf_MsgGT176
  {
    public StringBuffer GT176_Idr = new StringBuffer(1 );
    public StringBuffer GT176_Gti = new StringBuffer(10);
    public StringBuffer GT176_Nro = new StringBuffer(3 );
    public Vector       GT176_Tab = new Vector();

    public String getIdr()         {return GT176_Idr.toString();}
    public String getTpr()         {return GT176_Gti.toString();}
    public String getNro()         {return GT176_Nro.toString();}
    public Vector getTab()         {return GT176_Tab;}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_MsgGT176 Inicia_MsgGT176()
  {
    Buf_MsgGT176 l_MsgGT176 = new Buf_MsgGT176();
    l_MsgGT176.GT176_Idr.replace(0, l_MsgGT176.GT176_Idr.capacity(), RUTGEN.Blancos(1 ));
    l_MsgGT176.GT176_Gti.replace(0, l_MsgGT176.GT176_Gti.capacity(), RUTGEN.Blancos(10));
    l_MsgGT176.GT176_Nro.replace(0, l_MsgGT176.GT176_Nro.capacity(), RUTGEN.Blancos(3 ));
    int p = 0;
    for (int i=0; i<g_Max_GT176; i++)
        {
          Bff_MsgGT176 Tap = new Bff_MsgGT176();
          Tap.Mod_Cli.replace(0, Tap.Mod_Cli.capacity(), RUTGEN.Blancos(10));
          Tap.Mod_Rco.replace(0, Tap.Mod_Rco.capacity(), RUTGEN.Blancos(5 ));
          Tap.Mod_Rel.replace(0, Tap.Mod_Rel.capacity(), RUTGEN.Blancos(5 ));
          Tap.Mod_Cre.replace(0, Tap.Mod_Cre.capacity(), RUTGEN.Blancos(22));
          Tap.Mod_Ncl.replace(0, Tap.Mod_Ncl.capacity(), RUTGEN.Blancos(25));
          l_MsgGT176.GT176_Tab.add(Tap);        
        }
    return l_MsgGT176;
  }
  //---------------------------------------------------------------------------------------
  static public Buf_MsgGT176 LSet_A_MsgGT176(String pcStr)
  {
    Buf_MsgGT176 l_MsgGT176 = new Buf_MsgGT176();
    MSGGT176 vMsgGT176 = new MSGGT176();
    Vector vDatos = vMsgGT176.LSet_A_vMsgGT176(pcStr);
    l_MsgGT176 = (MSGGT176.Buf_MsgGT176)vDatos.elementAt(0);
    return l_MsgGT176;
  }
  //---------------------------------------------------------------------------------------
  public Vector LSet_A_vMsgGT176(String pcStr)
  {
    Vector vec = new Vector();
    int p = 0;
    Buf_MsgGT176 l_MsgGT176 = new Buf_MsgGT176();                            
    l_MsgGT176.GT176_Idr.append(pcStr.substring(p, p + l_MsgGT176.GT176_Idr.capacity())); p = p + l_MsgGT176.GT176_Idr.capacity();
    l_MsgGT176.GT176_Gti.append(pcStr.substring(p, p + l_MsgGT176.GT176_Gti.capacity())); p = p + l_MsgGT176.GT176_Gti.capacity();
    l_MsgGT176.GT176_Nro.append(pcStr.substring(p, p + l_MsgGT176.GT176_Nro.capacity())); p = p + l_MsgGT176.GT176_Nro.capacity();
    for (int i=0; i<Integer.parseInt(l_MsgGT176.GT176_Nro.toString()); i++)
        {
          Bff_MsgGT176 Tap = new Bff_MsgGT176();
          Tap.Mod_Cli.append(pcStr.substring(p, p + Tap.Mod_Cli.capacity())); p = p + Tap.Mod_Cli.capacity();
          Tap.Mod_Rco.append(pcStr.substring(p, p + Tap.Mod_Rco.capacity())); p = p + Tap.Mod_Rco.capacity();
          Tap.Mod_Rel.append(pcStr.substring(p, p + Tap.Mod_Rel.capacity())); p = p + Tap.Mod_Rel.capacity();
          Tap.Mod_Cre.append(pcStr.substring(p, p + Tap.Mod_Cre.capacity())); p = p + Tap.Mod_Cre.capacity();
          Tap.Mod_Ncl.append(pcStr.substring(p, p + Tap.Mod_Ncl.capacity())); p = p + Tap.Mod_Ncl.capacity();
          l_MsgGT176.GT176_Tab.add(Tap);
        }
    vec.add (l_MsgGT176);
    return vec;
  }
  //---------------------------------------------------------------------------------------
  static public String LSet_De_MsgGT176(Buf_MsgGT176 p_MsgGT176)
  {
    String pcStr = "";
    pcStr = pcStr + p_MsgGT176.GT176_Idr.toString();
    pcStr = pcStr + p_MsgGT176.GT176_Gti.toString();
    pcStr = pcStr + p_MsgGT176.GT176_Nro.toString();
    for (int i=0; i<p_MsgGT176.GT176_Tab.size(); i++)
        {
          pcStr = pcStr + ((Bff_MsgGT176)p_MsgGT176.GT176_Tab.elementAt(i)).Mod_Cli.toString();
          pcStr = pcStr + ((Bff_MsgGT176)p_MsgGT176.GT176_Tab.elementAt(i)).Mod_Rco.toString();
          pcStr = pcStr + ((Bff_MsgGT176)p_MsgGT176.GT176_Tab.elementAt(i)).Mod_Rel.toString();
          pcStr = pcStr + ((Bff_MsgGT176)p_MsgGT176.GT176_Tab.elementAt(i)).Mod_Cre.toString();
          pcStr = pcStr + ((Bff_MsgGT176)p_MsgGT176.GT176_Tab.elementAt(i)).Mod_Ncl.toString();
        }
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
}