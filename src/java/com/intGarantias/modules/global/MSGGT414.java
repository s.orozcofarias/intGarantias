// Source File Name:   MSGGT414.java

package com.intGarantias.modules.global;

import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.util.Formateo;

import java.util.*;
import java.lang.*;
import org.apache.turbine.util.Log;

public class MSGGT414
{
  //---------------------------------------------------------------------------------------
  public static class Grt_Gbs_Det
  {
    public StringBuffer GT414_Cbg = new StringBuffer(4  );      //9(04)        4 Codigo Bodega
    public StringBuffer GT414_Nbg = new StringBuffer(30 );      //X(30)       34 Nombre Bodega
    public StringBuffer GT414_Nct = new StringBuffer(10 );      //9(10)       44 Numero Certificado
    public StringBuffer GT414_Fct = new StringBuffer(8  );      //9(08)       52 Fecha Certificado
    public StringBuffer GT414_Fvt = new StringBuffer(8  );      //9(08)       60 Fecha Vencimiento
    public StringBuffer GT414_Wum = new StringBuffer(5  );      //X(05)       65 UMedida
    public StringBuffer GT414_Wcn = new StringBuffer(9  );      //9(07)V9(2)  74 Cantidad
    public StringBuffer GT414_Wpu = new StringBuffer(15 );      //9(13)V9(2)  89 Precio Unitario
    public StringBuffer GT414_Wvp = new StringBuffer(15 );      //9(13)V9(2) 104 Valor Vale Prenda
    public StringBuffer GT414_Wvl = new StringBuffer(15 );      //9(13)V9(2) 119 Valor Vale Prenda
    public StringBuffer GT414_Wcm = new StringBuffer(15 );      //X(15)      134 Codigo Mercaderia
    public StringBuffer GT414_Wmk = new StringBuffer(15 );      //X(15)      149 Marca Mercaderia
    public StringBuffer GT414_Wmd = new StringBuffer(15 );      //X(15)      164 Modelo Mercaderia
    public StringBuffer GT414_Wan = new StringBuffer(4  );      //9(04)      168 A�o Mercaderia
    public StringBuffer GT414_Wns = new StringBuffer(20 );      //X(20)      188 NSerie Mercaderia
    public StringBuffer GT414_Itb = new StringBuffer(2  );      //X(02)      190 Id Tipo Bien
    public StringBuffer GT414_Itg = new StringBuffer(5  );      //X(05)      195 Id Tipo Gtia
    public StringBuffer GT414_Dpc = new StringBuffer(10 );      //X(10)      205 PContable
    public StringBuffer GT414_Csv = new StringBuffer(3  );      //X(03)      208 Conservador
    public StringBuffer GT414_Lcl = new StringBuffer(15 );      //X(15)      223 Localidad
    public StringBuffer GT414_Rpo = new StringBuffer(4  );      //9(04)      227 Repertorio
    public StringBuffer GT414_Foj = new StringBuffer(7  );      //9(07)      234 Fojas
    public StringBuffer GT414_Nin = new StringBuffer(7  );      //9(07)      241 Numero
    public StringBuffer GT414_Vsg = new StringBuffer(15 );      //9(11)V9(4) 256 VAsegurable       
    public StringBuffer GT414_Csg = new StringBuffer(3  );      //9(03)      259 Aseguradora       
    public StringBuffer GT414_Spz = new StringBuffer(10 );      //X(10)      269 Poliza            
    public StringBuffer GT414_Sfv = new StringBuffer(8  );      //9(08)      277 FVcto Seguro      
    public StringBuffer GT414_Sum = new StringBuffer(1  );      //X(01)      278 UMon Seguro       
    public StringBuffer GT414_Svl = new StringBuffer(15 );      //9(11)V9(4) 293 VSeguro <UF>      

    public String getCbg()         {return GT414_Cbg.toString();}
    public String getNbg()         {return GT414_Nbg.toString();}
    public String getNct()         {return GT414_Nct.toString();}
    public String getFct()         {return GT414_Fct.toString();}
    public String getFvt()         {return GT414_Fvt.toString();}
    public String getWum()         {return GT414_Wum.toString();}
    public String getWcn()         {return GT414_Wcn.toString();}
    public String getWpu()         {return GT414_Wpu.toString();}
    public String getWvp()         {return GT414_Wvp.toString();}
    public String getWvl()         {return GT414_Wvl.toString();}
    public String getWcm()         {return GT414_Wcm.toString();}
    public String getWmk()         {return GT414_Wmk.toString();}
    public String getWmd()         {return GT414_Wmd.toString();}
    public String getWan()         {return GT414_Wan.toString();}
    public String getWns()         {return GT414_Wns.toString();}
    public String getItb()         {return GT414_Itb.toString();}
    public String getItg()         {return GT414_Itg.toString();}
    public String getDpc()         {return GT414_Dpc.toString();}
    public String getCsv()         {return GT414_Csv.toString();}
    public String getLcl()         {return GT414_Lcl.toString();}
    public String getRpo()         {return GT414_Rpo.toString();}
    public String getFoj()         {return GT414_Foj.toString();}
    public String getNin()         {return GT414_Nin.toString();}
    public String getVsg()         {return GT414_Vsg.toString();}
    public String getCsg()         {return GT414_Csg.toString();}
    public String getSpz()         {return GT414_Spz.toString();}
    public String getSfv()         {return GT414_Sfv.toString();}
    public String getSum()         {return GT414_Sum.toString();}
    public String getSvl()         {return GT414_Svl.toString();}

    public Formateo f = new Formateo();
    public String fgetNct()        {return f.Formato(GT414_Nct.toString(),"9.999.999.999",0,0,',');}
    public String fgetFct()        {return f.FormatDate(GT414_Fct.toString());}
    public String fgetFvt()        {return f.FormatDate(GT414_Fvt.toString());}
    public String fgetWcn()        {return f.Formato(GT414_Wcn.toString(),"99.999.999",2,2,',');}
    public String fgetWpu()        {return f.Formato(GT414_Wpu.toString(),"9.999.999.999.999",2,2,',');}
    public String fgetWvp()        {return f.Formato(GT414_Wvp.toString(),"9.999.999.999.999",2,2,',');}
    public String fgetWvl()        {return f.Formato(GT414_Wvl.toString(),"9.999.999.999.999",2,2,',');}
    public String fgetSfv()        {return f.FormatDate(GT414_Sfv.toString());}
    public String fgetVsg()        {return f.Formato(GT414_Vsg.toString(),"9.999.999.999.999",4,4,',');}
    public String fgetSvl()        {return f.Formato(GT414_Svl.toString(),"9.999.999.999.999",4,4,',');}

    public String f2getWvp()       {return f.Formato(GT414_Wvp.toString(),"9.999.999.999.999",2,2,',');}
    public String f0getWvp()       {return f.Formato(GT414_Wvp.toString(),"9.999.999.999.999",2,0,',');}
    public String f2getWvl()       {return f.Formato(GT414_Wvl.toString(),"9.999.999.999.999",2,2,',');}
    public String f0getWvl()       {return f.Formato(GT414_Wvl.toString(),"9.999.999.999.999",2,0,',');}
    public String f4getVsg()       {return f.Formato(GT414_Vsg.toString(),"9.999.999.999.999",4,4,',');}
    public String f2getVsg()       {return f.Formato(GT414_Vsg.toString(),"9.999.999.999.999",4,2,',');}
    public String f0getVsg()       {return f.Formato(GT414_Vsg.toString(),"9.999.999.999.999",4,0,',');}
    public String f4getSvl()       {return f.Formato(GT414_Svl.toString(),"9.999.999.999.999",4,4,',');}
    public String f2getSvl()       {return f.Formato(GT414_Svl.toString(),"9.999.999.999.999",4,2,',');}
    public String f0getSvl()       {return f.Formato(GT414_Svl.toString(),"9.999.999.999.999",4,0,',');}
  }
  //---------------------------------------------------------------------------------------
  public static class Buf_MsgGT414
  {
    public StringBuffer GT414_Idr = new StringBuffer(1) ;       //X(01)        1 Idr Host
    public StringBuffer GT414_Gti = new StringBuffer(10);       //X(10)       11 Id. Garantia
    public StringBuffer GT414_Seq = new StringBuffer(3) ;       //9(03)       14 Total Indices
    public Grt_Gbs_Det  GT414_Gbs = new Grt_Gbs_Det();          //X(277)     291 data Detalle

    public String getIdr()         {return GT414_Idr.toString();}
    public String getGti()         {return GT414_Gti.toString();}
    public String getSeq()         {return GT414_Seq.toString();}
    public Grt_Gbs_Det getGbs()    {return GT414_Gbs;}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_MsgGT414 Inicia_MsgGT414()
  {
    Buf_MsgGT414 l_MsgGT414 = new Buf_MsgGT414();
    l_MsgGT414.GT414_Idr.replace(0, l_MsgGT414.GT414_Idr.capacity(), RUTGEN.Blancos(1));
    l_MsgGT414.GT414_Gti.replace(0, l_MsgGT414.GT414_Gti.capacity(), RUTGEN.Blancos(10));
    l_MsgGT414.GT414_Seq.replace(0, l_MsgGT414.GT414_Seq.capacity(), RUTGEN.Blancos(3));
    Grt_Gbs_Det Gbs = new Grt_Gbs_Det();
    Gbs.GT414_Cbg.replace(0, Gbs.GT414_Cbg.capacity(), RUTGEN.Blancos(4  ));
    Gbs.GT414_Nbg.replace(0, Gbs.GT414_Nbg.capacity(), RUTGEN.Blancos(30 ));
    Gbs.GT414_Nct.replace(0, Gbs.GT414_Nct.capacity(), RUTGEN.Blancos(10 ));
    Gbs.GT414_Fct.replace(0, Gbs.GT414_Fct.capacity(), RUTGEN.Blancos(8  ));
    Gbs.GT414_Fvt.replace(0, Gbs.GT414_Fvt.capacity(), RUTGEN.Blancos(8  ));
    Gbs.GT414_Wum.replace(0, Gbs.GT414_Wum.capacity(), RUTGEN.Blancos(5  ));
    Gbs.GT414_Wcn.replace(0, Gbs.GT414_Wcn.capacity(), RUTGEN.Blancos(9  ));
    Gbs.GT414_Wpu.replace(0, Gbs.GT414_Wpu.capacity(), RUTGEN.Blancos(15 ));
    Gbs.GT414_Wvp.replace(0, Gbs.GT414_Wvp.capacity(), RUTGEN.Blancos(15 ));
    Gbs.GT414_Wvl.replace(0, Gbs.GT414_Wvl.capacity(), RUTGEN.Blancos(15 ));
    Gbs.GT414_Wcm.replace(0, Gbs.GT414_Wcm.capacity(), RUTGEN.Blancos(15 ));
    Gbs.GT414_Wmk.replace(0, Gbs.GT414_Wmk.capacity(), RUTGEN.Blancos(15 ));
    Gbs.GT414_Wmd.replace(0, Gbs.GT414_Wmd.capacity(), RUTGEN.Blancos(15 ));
    Gbs.GT414_Wan.replace(0, Gbs.GT414_Wan.capacity(), RUTGEN.Blancos(4  ));
    Gbs.GT414_Wns.replace(0, Gbs.GT414_Wns.capacity(), RUTGEN.Blancos(20 ));
    Gbs.GT414_Itb.replace(0, Gbs.GT414_Itb.capacity(), RUTGEN.Blancos(2  ));
    Gbs.GT414_Itg.replace(0, Gbs.GT414_Itg.capacity(), RUTGEN.Blancos(5  ));
    Gbs.GT414_Dpc.replace(0, Gbs.GT414_Dpc.capacity(), RUTGEN.Blancos(10 ));
    Gbs.GT414_Csv.replace(0, Gbs.GT414_Csv.capacity(), RUTGEN.Blancos(3  ));
    Gbs.GT414_Lcl.replace(0, Gbs.GT414_Lcl.capacity(), RUTGEN.Blancos(15 ));
    Gbs.GT414_Rpo.replace(0, Gbs.GT414_Rpo.capacity(), RUTGEN.Blancos(4  ));
    Gbs.GT414_Foj.replace(0, Gbs.GT414_Foj.capacity(), RUTGEN.Blancos(7  ));
    Gbs.GT414_Nin.replace(0, Gbs.GT414_Nin.capacity(), RUTGEN.Blancos(7  ));
    Gbs.GT414_Vsg.replace(0, Gbs.GT414_Vsg.capacity(), RUTGEN.Blancos(15 ));
    Gbs.GT414_Csg.replace(0, Gbs.GT414_Csg.capacity(), RUTGEN.Blancos(3  ));
    Gbs.GT414_Spz.replace(0, Gbs.GT414_Spz.capacity(), RUTGEN.Blancos(10 ));
    Gbs.GT414_Sfv.replace(0, Gbs.GT414_Sfv.capacity(), RUTGEN.Blancos(8  ));
    Gbs.GT414_Sum.replace(0, Gbs.GT414_Sum.capacity(), RUTGEN.Blancos(1  ));
    Gbs.GT414_Svl.replace(0, Gbs.GT414_Svl.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT414.GT414_Gbs = Gbs;
    return l_MsgGT414;
  }
  //---------------------------------------------------------------------------------------
  static public Buf_MsgGT414 LSet_A_MsgGT414(String pcStr)
  {
    Buf_MsgGT414 l_MsgGT414 = new Buf_MsgGT414();
    MSGGT414 vMsgGT414 = new MSGGT414();
    Vector vDatos = vMsgGT414.LSet_A_vMsgGT414(pcStr);
    l_MsgGT414 = (MSGGT414.Buf_MsgGT414)vDatos.elementAt(0);
    return l_MsgGT414;
  }
  //---------------------------------------------------------------------------------------
  public Vector LSet_A_vMsgGT414(String pcStr)
  {
    int p = 0;
    Vector vec = new Vector();
    Buf_MsgGT414 l_MsgGT414 = new Buf_MsgGT414();
    l_MsgGT414.GT414_Idr.append(pcStr.substring(p, p + l_MsgGT414.GT414_Idr.capacity())); p = p + l_MsgGT414.GT414_Idr.capacity();
    l_MsgGT414.GT414_Gti.append(pcStr.substring(p, p + l_MsgGT414.GT414_Gti.capacity())); p = p + l_MsgGT414.GT414_Gti.capacity();
    l_MsgGT414.GT414_Seq.append(pcStr.substring(p, p + l_MsgGT414.GT414_Seq.capacity())); p = p + l_MsgGT414.GT414_Seq.capacity();
    Grt_Gbs_Det Gbs = new Grt_Gbs_Det();
    Gbs.GT414_Cbg.append(pcStr.substring(p, p + Gbs.GT414_Cbg.capacity())); p = p + Gbs.GT414_Cbg.capacity();
    Gbs.GT414_Nbg.append(pcStr.substring(p, p + Gbs.GT414_Nbg.capacity())); p = p + Gbs.GT414_Nbg.capacity();
    Gbs.GT414_Nct.append(pcStr.substring(p, p + Gbs.GT414_Nct.capacity())); p = p + Gbs.GT414_Nct.capacity();
    Gbs.GT414_Fct.append(pcStr.substring(p, p + Gbs.GT414_Fct.capacity())); p = p + Gbs.GT414_Fct.capacity();
    Gbs.GT414_Fvt.append(pcStr.substring(p, p + Gbs.GT414_Fvt.capacity())); p = p + Gbs.GT414_Fvt.capacity();
    Gbs.GT414_Wum.append(pcStr.substring(p, p + Gbs.GT414_Wum.capacity())); p = p + Gbs.GT414_Wum.capacity();
    Gbs.GT414_Wcn.append(pcStr.substring(p, p + Gbs.GT414_Wcn.capacity())); p = p + Gbs.GT414_Wcn.capacity();
    Gbs.GT414_Wpu.append(pcStr.substring(p, p + Gbs.GT414_Wpu.capacity())); p = p + Gbs.GT414_Wpu.capacity();
    Gbs.GT414_Wvp.append(pcStr.substring(p, p + Gbs.GT414_Wvp.capacity())); p = p + Gbs.GT414_Wvp.capacity();
    Gbs.GT414_Wvl.append(pcStr.substring(p, p + Gbs.GT414_Wvl.capacity())); p = p + Gbs.GT414_Wvl.capacity();
    Gbs.GT414_Wcm.append(pcStr.substring(p, p + Gbs.GT414_Wcm.capacity())); p = p + Gbs.GT414_Wcm.capacity();
    Gbs.GT414_Wmk.append(pcStr.substring(p, p + Gbs.GT414_Wmk.capacity())); p = p + Gbs.GT414_Wmk.capacity();
    Gbs.GT414_Wmd.append(pcStr.substring(p, p + Gbs.GT414_Wmd.capacity())); p = p + Gbs.GT414_Wmd.capacity();
    Gbs.GT414_Wan.append(pcStr.substring(p, p + Gbs.GT414_Wan.capacity())); p = p + Gbs.GT414_Wan.capacity();
    Gbs.GT414_Wns.append(pcStr.substring(p, p + Gbs.GT414_Wns.capacity())); p = p + Gbs.GT414_Wns.capacity();
    Gbs.GT414_Itb.append(pcStr.substring(p, p + Gbs.GT414_Itb.capacity())); p = p + Gbs.GT414_Itb.capacity();
    Gbs.GT414_Itg.append(pcStr.substring(p, p + Gbs.GT414_Itg.capacity())); p = p + Gbs.GT414_Itg.capacity();
    Gbs.GT414_Dpc.append(pcStr.substring(p, p + Gbs.GT414_Dpc.capacity())); p = p + Gbs.GT414_Dpc.capacity();
    Gbs.GT414_Csv.append(pcStr.substring(p, p + Gbs.GT414_Csv.capacity())); p = p + Gbs.GT414_Csv.capacity();
    Gbs.GT414_Lcl.append(pcStr.substring(p, p + Gbs.GT414_Lcl.capacity())); p = p + Gbs.GT414_Lcl.capacity();
    Gbs.GT414_Rpo.append(pcStr.substring(p, p + Gbs.GT414_Rpo.capacity())); p = p + Gbs.GT414_Rpo.capacity();
    Gbs.GT414_Foj.append(pcStr.substring(p, p + Gbs.GT414_Foj.capacity())); p = p + Gbs.GT414_Foj.capacity();
    Gbs.GT414_Nin.append(pcStr.substring(p, p + Gbs.GT414_Nin.capacity())); p = p + Gbs.GT414_Nin.capacity();
    Gbs.GT414_Vsg.append(pcStr.substring(p, p + Gbs.GT414_Vsg.capacity())); p = p + Gbs.GT414_Vsg.capacity();
    Gbs.GT414_Csg.append(pcStr.substring(p, p + Gbs.GT414_Csg.capacity())); p = p + Gbs.GT414_Csg.capacity();
    Gbs.GT414_Spz.append(pcStr.substring(p, p + Gbs.GT414_Spz.capacity())); p = p + Gbs.GT414_Spz.capacity();
    Gbs.GT414_Sfv.append(pcStr.substring(p, p + Gbs.GT414_Sfv.capacity())); p = p + Gbs.GT414_Sfv.capacity();
    Gbs.GT414_Sum.append(pcStr.substring(p, p + Gbs.GT414_Sum.capacity())); p = p + Gbs.GT414_Sum.capacity();
    Gbs.GT414_Svl.append(pcStr.substring(p, p + Gbs.GT414_Svl.capacity())); p = p + Gbs.GT414_Svl.capacity();
    l_MsgGT414.GT414_Gbs = Gbs;
    vec.add(l_MsgGT414);
    return vec;
  }
  //---------------------------------------------------------------------------------------
  static public String LSet_De_MsgGT414(Buf_MsgGT414 p_MsgGT414)
  {
    String pcStr = "";
    pcStr = pcStr + p_MsgGT414.GT414_Idr.toString();
    pcStr = pcStr + p_MsgGT414.GT414_Gti.toString();
    pcStr = pcStr + p_MsgGT414.GT414_Seq.toString();
    Grt_Gbs_Det Gbs = new Grt_Gbs_Det();
    Gbs = p_MsgGT414.GT414_Gbs;
    pcStr = pcStr + Gbs.GT414_Cbg.toString();
    pcStr = pcStr + Gbs.GT414_Nbg.toString();
    pcStr = pcStr + Gbs.GT414_Nct.toString();
    pcStr = pcStr + Gbs.GT414_Fct.toString();
    pcStr = pcStr + Gbs.GT414_Fvt.toString();
    pcStr = pcStr + Gbs.GT414_Wum.toString();
    pcStr = pcStr + Gbs.GT414_Wcn.toString();
    pcStr = pcStr + Gbs.GT414_Wpu.toString();
    pcStr = pcStr + Gbs.GT414_Wvp.toString();
    pcStr = pcStr + Gbs.GT414_Wvl.toString();
    pcStr = pcStr + Gbs.GT414_Wcm.toString();
    pcStr = pcStr + Gbs.GT414_Wmk.toString();
    pcStr = pcStr + Gbs.GT414_Wmd.toString();
    pcStr = pcStr + Gbs.GT414_Wan.toString();
    pcStr = pcStr + Gbs.GT414_Wns.toString();
    pcStr = pcStr + Gbs.GT414_Itb.toString();
    pcStr = pcStr + Gbs.GT414_Itg.toString();
    pcStr = pcStr + Gbs.GT414_Dpc.toString();
    pcStr = pcStr + Gbs.GT414_Csv.toString();
    pcStr = pcStr + Gbs.GT414_Lcl.toString();
    pcStr = pcStr + Gbs.GT414_Rpo.toString();
    pcStr = pcStr + Gbs.GT414_Foj.toString();
    pcStr = pcStr + Gbs.GT414_Nin.toString();
    pcStr = pcStr + Gbs.GT414_Vsg.toString();
    pcStr = pcStr + Gbs.GT414_Csg.toString();
    pcStr = pcStr + Gbs.GT414_Spz.toString();
    pcStr = pcStr + Gbs.GT414_Sfv.toString();
    pcStr = pcStr + Gbs.GT414_Sum.toString();
    pcStr = pcStr + Gbs.GT414_Svl.toString();
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
}