// Source File Name:   MSGGT201.java

package com.intGarantias.modules.global;

import java.util.Vector;

import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.util.Formateo;

public class MSGGT201 {
	public static class Buf_MsgGT201 {

		public String getIdr() {
			return GT201_Idr.toString();
		}

		public String getTpr() {
			return GT201_Tpr.toString();
		}

		public String getTsd() {
			return GT201_Tsd.toString();
		}

		public String getSuc() {
			return GT201_Suc.toString();
		}

		public String getEje() {
			return GT201_Eje.toString();
		}

		public String getNcn() {
			return GT201_Ncn.toString();
		}

		public String getCli() {
			return GT201_Cli.toString();
		}

		public String getNro() {
			return GT201_Nro.toString();
		}

		public Vector getTab() {
			return GT201_Tab;
		}

		public StringBuffer GT201_Idr;
		public StringBuffer GT201_Tpr;
		public StringBuffer GT201_Tsd;
		public StringBuffer GT201_Suc;
		public StringBuffer GT201_Eje;
		public StringBuffer GT201_Ncn;
		public StringBuffer GT201_Cli;
		public StringBuffer GT201_Nro;
		public Vector GT201_Tab;

		public Buf_MsgGT201() {
			GT201_Idr = new StringBuffer(1);
			GT201_Tpr = new StringBuffer(1);
			GT201_Tsd = new StringBuffer(3);
			GT201_Suc = new StringBuffer(3);
			GT201_Eje = new StringBuffer(4);
			GT201_Ncn = new StringBuffer(7);
			GT201_Cli = new StringBuffer(10);
			GT201_Nro = new StringBuffer(3);
			GT201_Tab = new Vector();
		}
	}

	public static class Bff_MsgGT201 {

		public String getTEst() {
			return Tmt_Est.toString();
		}

		public String getTSuc() {
			return Tmt_Suc.toString();
		}

		public String getTNev() {
			return Tmt_Nev.toString();
		}

		public String getTTrt() {
			return Tmt_Trt.toString();
		}

		public String getEEje() {
			return Evt_Eje.toString();
		}

		public String getTFen() {
			return Tmt_Fen.toString();
		}

		public String getTHra() {
			return Tmt_Hra.toString();
		}

		public String getECli() {
			return Evt_Cli.toString();
		}

		public String getENcl() {
			return Evt_Ncl.toString();
		}

		public String getESis() {
			return Evt_Sis.toString();
		}

		public String getENcn() {
			return Evt_Ncn.toString();
		}

		public String getEDcn() {
			return Evt_Dcn.toString();
		}

		public String getEGgr() {
			return Evt_Ggr.toString();
		}

		public String getEDsg() {
			return Evt_Dsg.toString();
		}

		public String getEMnd() {
			return Evt_Mnd.toString();
		}

		public String getETrj() {
			return Evt_Trj.toString();
		}

		public String getEVle() {
			return Evt_Vle.toString();
		}

		public String getEDcm() {
			return Evt_Dcm.toString();
		}

		public String getTPgm() {
			return Tmt_Pgm.toString();
		}

		public String getETtr() {
			return Evt_Ttr.toString();
		}

		public String getTTsd() {
			return Tmt_Tsd.toString();
		}

		public String getENej() {
			return Evt_Nej.toString();
		}

		public String fgetEst() {
			return Tmt_Est.toString();
		}

		public String fgetSuc() {
			return Tmt_Suc.toString();
		}

		public String fgetNev() {
			return Tmt_Nev.toString();
		}

		public String fgetTrt() {
			return Tmt_Trt.toString();
		}

		public String fgetEje() {
			return Evt_Eje.toString();
		}

		public String fgetFen() {
			return f.FormatDate(Tmt_Fen.toString());
		}

		public String fgetHra() {
			String dato = (new StringBuffer())
					.append(Tmt_Hra.toString().substring(0, 2)).append(":")
					.append(Tmt_Hra.toString().substring(2, 4)).append(":")
					.append(Tmt_Hra.toString().substring(4)).toString();
			return dato;
		}

		public String fgetCli() {
			return f.fmtRut(Evt_Cli.toString());
		}

		public String fgetNcl() {
			return Evt_Ncl.toString();
		}

		public String fgetOpe() {
			String dato = (new StringBuffer()).append(Evt_Sis.toString())
					.append("-").append(Evt_Dcn.toString()).append("-")
					.append(Evt_Ncn.toString()).toString();
			return dato;
		}

		public String fgetGgr() {
			return Evt_Ggr.toString();
		}

		public String fgetDsg() {
			return Evt_Dsg.toString();
		}

		public String fgetTrj() {
			return (new StringBuffer()).append(Evt_Trj.toString().trim())
					.append("-").append(Evt_Mnd.toString()).toString();
		}

		public String fgetVle() {
			return f.Formato(Evt_Vle.toString(), "99.999.999.999", 4, 2, ',');
		}

		public String fgetDcm() {
			return Evt_Dcm.toString();
		}

		public String fgetPgm() {
			return Tmt_Pgm.toString();
		}

		public String fgetTtr() {
			return Evt_Ttr.toString();
		}

		public String fgetTsd() {
			if (Tmt_Tsd.toString().equals("000"))
				return "N/A";
			else
				return Tmt_Tsd.toString();
		}

		public String egetVle() {
			return f.Formato(Evt_Vle.toString(), "99.999.999.999", 4, 0, ',');
		}

		public StringBuffer Tmt_Est;
		public StringBuffer Tmt_Suc;
		public StringBuffer Tmt_Nev;
		public StringBuffer Tmt_Trt;
		public StringBuffer Evt_Eje;
		public StringBuffer Tmt_Fen;
		public StringBuffer Tmt_Hra;
		public StringBuffer Evt_Cli;
		public StringBuffer Evt_Ncl;
		public StringBuffer Evt_Sis;
		public StringBuffer Evt_Ncn;
		public StringBuffer Evt_Dcn;
		public StringBuffer Evt_Ggr;
		public StringBuffer Evt_Dsg;
		public StringBuffer Evt_Mnd;
		public StringBuffer Evt_Trj;
		public StringBuffer Evt_Vle;
		public StringBuffer Evt_Dcm;
		public StringBuffer Tmt_Pgm;
		public StringBuffer Evt_Ttr;
		public StringBuffer Tmt_Tsd;
		public StringBuffer Evt_Nej;
		public Formateo f;

		public Bff_MsgGT201() {
			Tmt_Est = new StringBuffer(5);
			Tmt_Suc = new StringBuffer(3);
			Tmt_Nev = new StringBuffer(7);
			Tmt_Trt = new StringBuffer(5);
			Evt_Eje = new StringBuffer(4);
			Tmt_Fen = new StringBuffer(8);
			Tmt_Hra = new StringBuffer(6);
			Evt_Cli = new StringBuffer(10);
			Evt_Ncl = new StringBuffer(40);
			Evt_Sis = new StringBuffer(3);
			Evt_Ncn = new StringBuffer(7);
			Evt_Dcn = new StringBuffer(5);
			Evt_Ggr = new StringBuffer(3);
			Evt_Dsg = new StringBuffer(30);
			Evt_Mnd = new StringBuffer(3);
			Evt_Trj = new StringBuffer(3);
			Evt_Vle = new StringBuffer(15);
			Evt_Dcm = new StringBuffer(1);
			Tmt_Pgm = new StringBuffer(8);
			Evt_Ttr = new StringBuffer(5);
			Tmt_Tsd = new StringBuffer(3);
			Evt_Nej = new StringBuffer(25);
			f = new Formateo();
		}
	}

	public MSGGT201() {
	}

	public static Buf_MsgGT201 Inicia_MsgGT201() {
		Buf_MsgGT201 l_MsgGT201 = new Buf_MsgGT201();
		l_MsgGT201.GT201_Idr.replace(0, l_MsgGT201.GT201_Idr.capacity(),
				RUTGEN.Blancos(1));
		l_MsgGT201.GT201_Tpr.replace(0, l_MsgGT201.GT201_Tpr.capacity(),
				RUTGEN.Blancos(1));
		l_MsgGT201.GT201_Tsd.replace(0, l_MsgGT201.GT201_Tsd.capacity(),
				RUTGEN.Blancos(3));
		l_MsgGT201.GT201_Suc.replace(0, l_MsgGT201.GT201_Suc.capacity(),
				RUTGEN.Blancos(3));
		l_MsgGT201.GT201_Eje.replace(0, l_MsgGT201.GT201_Eje.capacity(),
				RUTGEN.Blancos(4));
		l_MsgGT201.GT201_Ncn.replace(0, l_MsgGT201.GT201_Ncn.capacity(),
				RUTGEN.Blancos(7));
		l_MsgGT201.GT201_Cli.replace(0, l_MsgGT201.GT201_Cli.capacity(),
				RUTGEN.Blancos(10));
		l_MsgGT201.GT201_Nro.replace(0, l_MsgGT201.GT201_Nro.capacity(),
				RUTGEN.Blancos(3));
		for (int i = 0; i < g_Max_GT201; i++) {
			Bff_MsgGT201 Tap = new Bff_MsgGT201();
			Tap.Tmt_Est.replace(0, Tap.Tmt_Est.capacity(), RUTGEN.Blancos(5));
			Tap.Tmt_Suc.replace(0, Tap.Tmt_Suc.capacity(), RUTGEN.Blancos(3));
			Tap.Tmt_Nev.replace(0, Tap.Tmt_Nev.capacity(), RUTGEN.Blancos(7));
			Tap.Tmt_Trt.replace(0, Tap.Tmt_Trt.capacity(), RUTGEN.Blancos(5));
			Tap.Evt_Eje.replace(0, Tap.Evt_Eje.capacity(), RUTGEN.Blancos(4));
			Tap.Tmt_Fen.replace(0, Tap.Tmt_Fen.capacity(), RUTGEN.Blancos(8));
			Tap.Tmt_Hra.replace(0, Tap.Tmt_Hra.capacity(), RUTGEN.Blancos(6));
			Tap.Evt_Cli.replace(0, Tap.Evt_Cli.capacity(), RUTGEN.Blancos(10));
			Tap.Evt_Ncl.replace(0, Tap.Evt_Ncl.capacity(), RUTGEN.Blancos(40));
			Tap.Evt_Sis.replace(0, Tap.Evt_Sis.capacity(), RUTGEN.Blancos(3));
			Tap.Evt_Ncn.replace(0, Tap.Evt_Ncn.capacity(), RUTGEN.Blancos(7));
			Tap.Evt_Dcn.replace(0, Tap.Evt_Dcn.capacity(), RUTGEN.Blancos(5));
			Tap.Evt_Ggr.replace(0, Tap.Evt_Ggr.capacity(), RUTGEN.Blancos(3));
			Tap.Evt_Dsg.replace(0, Tap.Evt_Dsg.capacity(), RUTGEN.Blancos(30));
			Tap.Evt_Mnd.replace(0, Tap.Evt_Mnd.capacity(), RUTGEN.Blancos(3));
			Tap.Evt_Trj.replace(0, Tap.Evt_Trj.capacity(), RUTGEN.Blancos(3));
			Tap.Evt_Vle.replace(0, Tap.Evt_Vle.capacity(), RUTGEN.Blancos(15));
			Tap.Evt_Dcm.replace(0, Tap.Evt_Dcm.capacity(), RUTGEN.Blancos(1));
			Tap.Tmt_Pgm.replace(0, Tap.Tmt_Pgm.capacity(), RUTGEN.Blancos(8));
			Tap.Evt_Ttr.replace(0, Tap.Evt_Ttr.capacity(), RUTGEN.Blancos(5));
			Tap.Tmt_Tsd.replace(0, Tap.Tmt_Tsd.capacity(), RUTGEN.Blancos(3));
			Tap.Evt_Nej.replace(0, Tap.Evt_Nej.capacity(), RUTGEN.Blancos(25));
			l_MsgGT201.GT201_Tab.add(Tap);
		}

		return l_MsgGT201;
	}

	public static Buf_MsgGT201 LSet_A_MsgGT201(String pcStr) {
		Buf_MsgGT201 l_MsgGT201 = new Buf_MsgGT201();
		MSGGT201 vMsgGT201 = new MSGGT201();
		Vector vDatos = vMsgGT201.LSet_A_vMsgGT201(pcStr);
		l_MsgGT201 = (Buf_MsgGT201) vDatos.elementAt(0);
		return l_MsgGT201;
	}

	public Vector LSet_A_vMsgGT201(String pcStr) {
		Vector vec = new Vector();
		int p = 0;
		Buf_MsgGT201 l_MsgGT201 = new Buf_MsgGT201();
		l_MsgGT201.GT201_Idr.append(pcStr.substring(p,
				p + l_MsgGT201.GT201_Idr.capacity()));
		p += l_MsgGT201.GT201_Idr.capacity();
		l_MsgGT201.GT201_Tpr.append(pcStr.substring(p,
				p + l_MsgGT201.GT201_Tpr.capacity()));
		p += l_MsgGT201.GT201_Tpr.capacity();
		l_MsgGT201.GT201_Tsd.append(pcStr.substring(p,
				p + l_MsgGT201.GT201_Tsd.capacity()));
		p += l_MsgGT201.GT201_Tsd.capacity();
		l_MsgGT201.GT201_Suc.append(pcStr.substring(p,
				p + l_MsgGT201.GT201_Suc.capacity()));
		p += l_MsgGT201.GT201_Suc.capacity();
		l_MsgGT201.GT201_Eje.append(pcStr.substring(p,
				p + l_MsgGT201.GT201_Eje.capacity()));
		p += l_MsgGT201.GT201_Eje.capacity();
		l_MsgGT201.GT201_Ncn.append(pcStr.substring(p,
				p + l_MsgGT201.GT201_Ncn.capacity()));
		p += l_MsgGT201.GT201_Ncn.capacity();
		l_MsgGT201.GT201_Cli.append(pcStr.substring(p,
				p + l_MsgGT201.GT201_Cli.capacity()));
		p += l_MsgGT201.GT201_Cli.capacity();
		l_MsgGT201.GT201_Nro.append(pcStr.substring(p,
				p + l_MsgGT201.GT201_Nro.capacity()));
		p += l_MsgGT201.GT201_Nro.capacity();
		for (int i = 0; i < Integer.parseInt(l_MsgGT201.GT201_Nro.toString()); i++) {
			Bff_MsgGT201 Tap = new Bff_MsgGT201();
			Tap.Tmt_Est.append(pcStr.substring(p, p + Tap.Tmt_Est.capacity()));
			p += Tap.Tmt_Est.capacity();
			Tap.Tmt_Suc.append(pcStr.substring(p, p + Tap.Tmt_Suc.capacity()));
			p += Tap.Tmt_Suc.capacity();
			Tap.Tmt_Nev.append(pcStr.substring(p, p + Tap.Tmt_Nev.capacity()));
			p += Tap.Tmt_Nev.capacity();
			Tap.Tmt_Trt.append(pcStr.substring(p, p + Tap.Tmt_Trt.capacity()));
			p += Tap.Tmt_Trt.capacity();
			Tap.Evt_Eje.append(pcStr.substring(p, p + Tap.Evt_Eje.capacity()));
			p += Tap.Evt_Eje.capacity();
			Tap.Tmt_Fen.append(pcStr.substring(p, p + Tap.Tmt_Fen.capacity()));
			p += Tap.Tmt_Fen.capacity();
			Tap.Tmt_Hra.append(pcStr.substring(p, p + Tap.Tmt_Hra.capacity()));
			p += Tap.Tmt_Hra.capacity();
			Tap.Evt_Cli.append(pcStr.substring(p, p + Tap.Evt_Cli.capacity()));
			p += Tap.Evt_Cli.capacity();
			Tap.Evt_Ncl.append(pcStr.substring(p, p + Tap.Evt_Ncl.capacity()));
			p += Tap.Evt_Ncl.capacity();
			Tap.Evt_Sis.append(pcStr.substring(p, p + Tap.Evt_Sis.capacity()));
			p += Tap.Evt_Sis.capacity();
			Tap.Evt_Ncn.append(pcStr.substring(p, p + Tap.Evt_Ncn.capacity()));
			p += Tap.Evt_Ncn.capacity();
			Tap.Evt_Dcn.append(pcStr.substring(p, p + Tap.Evt_Dcn.capacity()));
			p += Tap.Evt_Dcn.capacity();
			Tap.Evt_Ggr.append(pcStr.substring(p, p + Tap.Evt_Ggr.capacity()));
			p += Tap.Evt_Ggr.capacity();
			Tap.Evt_Dsg.append(pcStr.substring(p, p + Tap.Evt_Dsg.capacity()));
			p += Tap.Evt_Dsg.capacity();
			Tap.Evt_Mnd.append(pcStr.substring(p, p + Tap.Evt_Mnd.capacity()));
			p += Tap.Evt_Mnd.capacity();
			Tap.Evt_Trj.append(pcStr.substring(p, p + Tap.Evt_Trj.capacity()));
			p += Tap.Evt_Trj.capacity();
			Tap.Evt_Vle.append(pcStr.substring(p, p + Tap.Evt_Vle.capacity()));
			p += Tap.Evt_Vle.capacity();
			Tap.Evt_Dcm.append(pcStr.substring(p, p + Tap.Evt_Dcm.capacity()));
			p += Tap.Evt_Dcm.capacity();
			Tap.Tmt_Pgm.append(pcStr.substring(p, p + Tap.Tmt_Pgm.capacity()));
			p += Tap.Tmt_Pgm.capacity();
			Tap.Evt_Ttr.append(pcStr.substring(p, p + Tap.Evt_Ttr.capacity()));
			p += Tap.Evt_Ttr.capacity();
			Tap.Tmt_Tsd.append(pcStr.substring(p, p + Tap.Tmt_Tsd.capacity()));
			p += Tap.Tmt_Tsd.capacity();
			Tap.Evt_Nej.append(pcStr.substring(p, p + Tap.Evt_Nej.capacity()));
			p += Tap.Evt_Nej.capacity();
			l_MsgGT201.GT201_Tab.add(Tap);
		}

		vec.add(l_MsgGT201);
		return vec;
	}

	public static String LSet_De_MsgGT201(Buf_MsgGT201 p_MsgGT201) {
		String pcStr = "";
		pcStr = (new StringBuffer()).append(pcStr)
				.append(p_MsgGT201.GT201_Idr.toString()).toString();
		pcStr = (new StringBuffer()).append(pcStr)
				.append(p_MsgGT201.GT201_Tpr.toString()).toString();
		pcStr = (new StringBuffer()).append(pcStr)
				.append(p_MsgGT201.GT201_Tsd.toString()).toString();
		pcStr = (new StringBuffer()).append(pcStr)
				.append(p_MsgGT201.GT201_Suc.toString()).toString();
		pcStr = (new StringBuffer()).append(pcStr)
				.append(p_MsgGT201.GT201_Eje.toString()).toString();
		pcStr = (new StringBuffer()).append(pcStr)
				.append(p_MsgGT201.GT201_Ncn.toString()).toString();
		pcStr = (new StringBuffer()).append(pcStr)
				.append(p_MsgGT201.GT201_Cli.toString()).toString();
		pcStr = (new StringBuffer()).append(pcStr)
				.append(p_MsgGT201.GT201_Nro.toString()).toString();
		for (int i = 0; i < p_MsgGT201.GT201_Tab.size(); i++) {
			pcStr = (new StringBuffer())
					.append(pcStr)
					.append(((Bff_MsgGT201) p_MsgGT201.GT201_Tab.elementAt(i)).Tmt_Est
							.toString()).toString();
			pcStr = (new StringBuffer())
					.append(pcStr)
					.append(((Bff_MsgGT201) p_MsgGT201.GT201_Tab.elementAt(i)).Tmt_Suc
							.toString()).toString();
			pcStr = (new StringBuffer())
					.append(pcStr)
					.append(((Bff_MsgGT201) p_MsgGT201.GT201_Tab.elementAt(i)).Tmt_Nev
							.toString()).toString();
			pcStr = (new StringBuffer())
					.append(pcStr)
					.append(((Bff_MsgGT201) p_MsgGT201.GT201_Tab.elementAt(i)).Tmt_Trt
							.toString()).toString();
			pcStr = (new StringBuffer())
					.append(pcStr)
					.append(((Bff_MsgGT201) p_MsgGT201.GT201_Tab.elementAt(i)).Evt_Eje
							.toString()).toString();
			pcStr = (new StringBuffer())
					.append(pcStr)
					.append(((Bff_MsgGT201) p_MsgGT201.GT201_Tab.elementAt(i)).Tmt_Fen
							.toString()).toString();
			pcStr = (new StringBuffer())
					.append(pcStr)
					.append(((Bff_MsgGT201) p_MsgGT201.GT201_Tab.elementAt(i)).Tmt_Hra
							.toString()).toString();
			pcStr = (new StringBuffer())
					.append(pcStr)
					.append(((Bff_MsgGT201) p_MsgGT201.GT201_Tab.elementAt(i)).Evt_Cli
							.toString()).toString();
			pcStr = (new StringBuffer())
					.append(pcStr)
					.append(((Bff_MsgGT201) p_MsgGT201.GT201_Tab.elementAt(i)).Evt_Ncl
							.toString()).toString();
			pcStr = (new StringBuffer())
					.append(pcStr)
					.append(((Bff_MsgGT201) p_MsgGT201.GT201_Tab.elementAt(i)).Evt_Sis
							.toString()).toString();
			pcStr = (new StringBuffer())
					.append(pcStr)
					.append(((Bff_MsgGT201) p_MsgGT201.GT201_Tab.elementAt(i)).Evt_Ncn
							.toString()).toString();
			pcStr = (new StringBuffer())
					.append(pcStr)
					.append(((Bff_MsgGT201) p_MsgGT201.GT201_Tab.elementAt(i)).Evt_Dcn
							.toString()).toString();
			pcStr = (new StringBuffer())
					.append(pcStr)
					.append(((Bff_MsgGT201) p_MsgGT201.GT201_Tab.elementAt(i)).Evt_Ggr
							.toString()).toString();
			pcStr = (new StringBuffer())
					.append(pcStr)
					.append(((Bff_MsgGT201) p_MsgGT201.GT201_Tab.elementAt(i)).Evt_Dsg
							.toString()).toString();
			pcStr = (new StringBuffer())
					.append(pcStr)
					.append(((Bff_MsgGT201) p_MsgGT201.GT201_Tab.elementAt(i)).Evt_Mnd
							.toString()).toString();
			pcStr = (new StringBuffer())
					.append(pcStr)
					.append(((Bff_MsgGT201) p_MsgGT201.GT201_Tab.elementAt(i)).Evt_Trj
							.toString()).toString();
			pcStr = (new StringBuffer())
					.append(pcStr)
					.append(((Bff_MsgGT201) p_MsgGT201.GT201_Tab.elementAt(i)).Evt_Vle
							.toString()).toString();
			pcStr = (new StringBuffer())
					.append(pcStr)
					.append(((Bff_MsgGT201) p_MsgGT201.GT201_Tab.elementAt(i)).Evt_Dcm
							.toString()).toString();
			pcStr = (new StringBuffer())
					.append(pcStr)
					.append(((Bff_MsgGT201) p_MsgGT201.GT201_Tab.elementAt(i)).Tmt_Pgm
							.toString()).toString();
			pcStr = (new StringBuffer())
					.append(pcStr)
					.append(((Bff_MsgGT201) p_MsgGT201.GT201_Tab.elementAt(i)).Evt_Ttr
							.toString()).toString();
			pcStr = (new StringBuffer())
					.append(pcStr)
					.append(((Bff_MsgGT201) p_MsgGT201.GT201_Tab.elementAt(i)).Tmt_Tsd
							.toString()).toString();
			pcStr = (new StringBuffer())
					.append(pcStr)
					.append(((Bff_MsgGT201) p_MsgGT201.GT201_Tab.elementAt(i)).Evt_Nej
							.toString()).toString();
		}

		return pcStr;
	}
	static public String GtiTraStmt(String nev, String ncn)
	  {
	    String sqlText ="SELECT GTI_NEV "
					    +"FROM GRT_ADMIN.TT_GTI_TRA "
					    +"WHERE GTI_NEV ="+nev+" "
					    +"AND GTI_NCN="+ncn;
	    return sqlText;
	  }

	public static int g_Max_GT201 = 17;

}
