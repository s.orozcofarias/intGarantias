// Source File Name:   PRMGT093.java

package com.intGarantias.modules.global;

import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.util.Formateo;

import java.util.*;
import java.lang.*;
import org.apache.turbine.util.Log;

public class PRMGT093
{
  //---------------------------------------------------------------------------------------
  public static class Buf_PrmGT093
  {
    public StringBuffer GT093_Idr = new StringBuffer(1 );   //X(01)        Idr.Accion Q
    public StringBuffer GT093_Rtn = new StringBuffer(2 );   //X(02)        Retorno OK/NK
    public StringBuffer GT093_Sis = new StringBuffer(3 );   //X(03)        Sistema (Garantia)
    public StringBuffer GT093_Dcn = new StringBuffer(5 );   //X(05)        Desc.Contrato/Garantia
    public StringBuffer GT093_Itb = new StringBuffer(1 );   //X(01)        Tipo Bien (H/P)
    public StringBuffer GT093_Dpc = new StringBuffer(10);   //X(10)        Producto Contable
    public StringBuffer GT093_Pct = new StringBuffer(3 );   //9(03)        %Castigo SBIF

    public String getIdr()         {return GT093_Idr.toString();}
    public String getRtn()         {return GT093_Rtn.toString();}
    public String getSis()         {return GT093_Sis.toString();}
    public String getDcn()         {return GT093_Dcn.toString();}
    public String getItb()         {return GT093_Itb.toString();}
    public String getDpc()         {return GT093_Dpc.toString();}
    public String getPct()         {return GT093_Pct.toString();}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_PrmGT093 Inicia_PrmGT093 ()
  {
    Buf_PrmGT093 l_PrmGT093 = new Buf_PrmGT093();
    l_PrmGT093.GT093_Idr.replace(0, l_PrmGT093.GT093_Idr.capacity(), RUTGEN.Blancos(1 ));
    l_PrmGT093.GT093_Rtn.replace(0, l_PrmGT093.GT093_Rtn.capacity(), RUTGEN.Blancos(2 ));
    l_PrmGT093.GT093_Sis.replace(0, l_PrmGT093.GT093_Sis.capacity(), RUTGEN.Blancos(3 ));
    l_PrmGT093.GT093_Dcn.replace(0, l_PrmGT093.GT093_Dcn.capacity(), RUTGEN.Blancos(5 ));
    l_PrmGT093.GT093_Itb.replace(0, l_PrmGT093.GT093_Itb.capacity(), RUTGEN.Blancos(1 ));
    l_PrmGT093.GT093_Dpc.replace(0, l_PrmGT093.GT093_Dpc.capacity(), RUTGEN.Blancos(10));
    l_PrmGT093.GT093_Pct.replace(0, l_PrmGT093.GT093_Pct.capacity(), RUTGEN.Blancos(3 ));
    return l_PrmGT093;
  }
  //---------------------------------------------------------------------------------------
  public static Buf_PrmGT093 LSet_A_PrmGT093(String pcStr)
  {
    Buf_PrmGT093 l_PrmGT093 = new Buf_PrmGT093();
    Vector vDatos = LSet_A_vPrmGT093(pcStr);
    l_PrmGT093 = (Buf_PrmGT093)vDatos.elementAt(0);
    return l_PrmGT093;
  }
  //---------------------------------------------------------------------------------------
  public static Vector LSet_A_vPrmGT093(String pcStr)
  {
    Vector vec = new Vector();
    int p = 0;
    Buf_PrmGT093 l_PrmGT093 = new Buf_PrmGT093();
    l_PrmGT093.GT093_Idr.replace(0, l_PrmGT093.GT093_Idr.capacity(), pcStr.substring(p, p + l_PrmGT093.GT093_Idr.capacity())); p = p + l_PrmGT093.GT093_Idr.capacity();
    l_PrmGT093.GT093_Rtn.replace(0, l_PrmGT093.GT093_Rtn.capacity(), pcStr.substring(p, p + l_PrmGT093.GT093_Rtn.capacity())); p = p + l_PrmGT093.GT093_Rtn.capacity();
    l_PrmGT093.GT093_Sis.replace(0, l_PrmGT093.GT093_Sis.capacity(), pcStr.substring(p, p + l_PrmGT093.GT093_Sis.capacity())); p = p + l_PrmGT093.GT093_Sis.capacity();
    l_PrmGT093.GT093_Dcn.replace(0, l_PrmGT093.GT093_Dcn.capacity(), pcStr.substring(p, p + l_PrmGT093.GT093_Dcn.capacity())); p = p + l_PrmGT093.GT093_Dcn.capacity();
    l_PrmGT093.GT093_Itb.replace(0, l_PrmGT093.GT093_Itb.capacity(), pcStr.substring(p, p + l_PrmGT093.GT093_Itb.capacity())); p = p + l_PrmGT093.GT093_Itb.capacity();
    l_PrmGT093.GT093_Dpc.replace(0, l_PrmGT093.GT093_Dpc.capacity(), pcStr.substring(p, p + l_PrmGT093.GT093_Dpc.capacity())); p = p + l_PrmGT093.GT093_Dpc.capacity();
    l_PrmGT093.GT093_Pct.replace(0, l_PrmGT093.GT093_Pct.capacity(), pcStr.substring(p, p + l_PrmGT093.GT093_Pct.capacity())); p = p + l_PrmGT093.GT093_Pct.capacity();
    vec.add (l_PrmGT093);
    return vec;
  }
  //---------------------------------------------------------------------------------------
  public static String LSet_De_PrmGT093 (Buf_PrmGT093 p_PrmGT093)
  {
    String pcStr = "";
    pcStr = pcStr + p_PrmGT093.GT093_Idr.toString();
    pcStr = pcStr + p_PrmGT093.GT093_Rtn.toString();
    pcStr = pcStr + p_PrmGT093.GT093_Sis.toString();
    pcStr = pcStr + p_PrmGT093.GT093_Dcn.toString();
    pcStr = pcStr + p_PrmGT093.GT093_Itb.toString();
    pcStr = pcStr + p_PrmGT093.GT093_Dpc.toString();
    pcStr = pcStr + p_PrmGT093.GT093_Pct.toString();
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
}