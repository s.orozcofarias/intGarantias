// Source File Name:   PRMGT421.java

package com.intGarantias.modules.global;

import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.util.Formateo;

import java.util.*;
import java.lang.*;
import org.apache.turbine.util.Log;

public class PRMGT421
{
  //---------------------------------------------------------------------------------------
  public static class Buf_PrmGT421
  {
    public StringBuffer GT421_Acc = new StringBuffer(1);   //'X(01)        1 Idr Host                  
    public StringBuffer GT421_Gti = new StringBuffer(10);  //'X(10)       11 Id.Garantia a buscar      
    public StringBuffer GT421_Seq = new StringBuffer(3);   //'9(03)        3 Secuencia                 
    public StringBuffer GT421_Sqd = new StringBuffer(3);   //'9(03)        3 Secuencia                 
    public StringBuffer GT421_Cli = new StringBuffer(10);  //'X(10)       79 Id. Cliente               
    public StringBuffer GT421_Ncl = new StringBuffer(40);  //'X(40)      119 Nombre Cliente            
    public StringBuffer GT421_Dcn = new StringBuffer(5);   //'X(05)      263 Tipo Contrato/Garantia    
    public StringBuffer GT421_Ndc = new StringBuffer(30);  //'X(30)      293 Desc.Contrato/Garantia    

    public String getAcc()         {return GT421_Acc.toString();}
    public String getGti()         {return GT421_Gti.toString();}
    public String getSeq()         {return GT421_Seq.toString();}
    public String getSqd()         {return GT421_Sqd.toString();}
    public String getCli()         {return GT421_Cli.toString();}
    public String getNcl()         {return GT421_Ncl.toString();}
    public String getDcn()         {return GT421_Dcn.toString();}
    public String getNdc()         {return GT421_Ndc.toString();}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_PrmGT421 Inicia_PrmGT421 ()
  {
    Buf_PrmGT421 l_PrmGT421 = new Buf_PrmGT421();
    l_PrmGT421.GT421_Acc.replace(0, l_PrmGT421.GT421_Acc.capacity(), RUTGEN.Blancos(1));
    l_PrmGT421.GT421_Gti.replace(0, l_PrmGT421.GT421_Gti.capacity(), RUTGEN.Blancos(10));
    l_PrmGT421.GT421_Seq.replace(0, l_PrmGT421.GT421_Seq.capacity(), RUTGEN.Blancos(3));
    l_PrmGT421.GT421_Sqd.replace(0, l_PrmGT421.GT421_Sqd.capacity(), RUTGEN.Blancos(3));
    l_PrmGT421.GT421_Cli.replace(0, l_PrmGT421.GT421_Cli.capacity(), RUTGEN.Blancos(10));
    l_PrmGT421.GT421_Ncl.replace(0, l_PrmGT421.GT421_Ncl.capacity(), RUTGEN.Blancos(40));
    l_PrmGT421.GT421_Dcn.replace(0, l_PrmGT421.GT421_Dcn.capacity(), RUTGEN.Blancos(5));
    l_PrmGT421.GT421_Ndc.replace(0, l_PrmGT421.GT421_Ndc.capacity(), RUTGEN.Blancos(30));
    return l_PrmGT421;
  }
  //---------------------------------------------------------------------------------------
  public static Buf_PrmGT421 LSet_A_PrmGT421(String pcStr)
  {
    Buf_PrmGT421 l_PrmGT421 = new Buf_PrmGT421();
    Vector vDatos = LSet_A_vPrmGT421(pcStr);
    l_PrmGT421 = (Buf_PrmGT421)vDatos.elementAt(0);
    return l_PrmGT421;
  }
  //---------------------------------------------------------------------------------------
  public static Vector LSet_A_vPrmGT421(String pcStr)
  {
    Vector vec = new Vector();
    int p = 0;
    Buf_PrmGT421 l_PrmGT421 = new Buf_PrmGT421();
    l_PrmGT421.GT421_Acc.replace(0, l_PrmGT421.GT421_Acc.capacity(), pcStr.substring(p, p + l_PrmGT421.GT421_Acc.capacity())); p = p + l_PrmGT421.GT421_Acc.capacity();
    l_PrmGT421.GT421_Gti.replace(0, l_PrmGT421.GT421_Gti.capacity(), pcStr.substring(p, p + l_PrmGT421.GT421_Gti.capacity())); p = p + l_PrmGT421.GT421_Gti.capacity();
    l_PrmGT421.GT421_Seq.replace(0, l_PrmGT421.GT421_Seq.capacity(), pcStr.substring(p, p + l_PrmGT421.GT421_Seq.capacity())); p = p + l_PrmGT421.GT421_Seq.capacity();
    l_PrmGT421.GT421_Sqd.replace(0, l_PrmGT421.GT421_Sqd.capacity(), pcStr.substring(p, p + l_PrmGT421.GT421_Sqd.capacity())); p = p + l_PrmGT421.GT421_Sqd.capacity();
    l_PrmGT421.GT421_Cli.replace(0, l_PrmGT421.GT421_Cli.capacity(), pcStr.substring(p, p + l_PrmGT421.GT421_Cli.capacity())); p = p + l_PrmGT421.GT421_Cli.capacity();
    l_PrmGT421.GT421_Ncl.replace(0, l_PrmGT421.GT421_Ncl.capacity(), pcStr.substring(p, p + l_PrmGT421.GT421_Ncl.capacity())); p = p + l_PrmGT421.GT421_Ncl.capacity();
    l_PrmGT421.GT421_Dcn.replace(0, l_PrmGT421.GT421_Dcn.capacity(), pcStr.substring(p, p + l_PrmGT421.GT421_Dcn.capacity())); p = p + l_PrmGT421.GT421_Dcn.capacity();
    l_PrmGT421.GT421_Ndc.replace(0, l_PrmGT421.GT421_Ndc.capacity(), pcStr.substring(p, p + l_PrmGT421.GT421_Ndc.capacity())); p = p + l_PrmGT421.GT421_Ndc.capacity();
    vec.add (l_PrmGT421);                          
    return vec;
  }
  //---------------------------------------------------------------------------------------
  public static String LSet_De_PrmGT421 (Buf_PrmGT421 p_PrmGT421)
  {
    String pcStr = "";
    pcStr = pcStr + p_PrmGT421.GT421_Acc.toString();
    pcStr = pcStr + p_PrmGT421.GT421_Gti.toString();
    pcStr = pcStr + p_PrmGT421.GT421_Seq.toString();
    pcStr = pcStr + p_PrmGT421.GT421_Sqd.toString();
    pcStr = pcStr + p_PrmGT421.GT421_Cli.toString();
    pcStr = pcStr + p_PrmGT421.GT421_Ncl.toString();
    pcStr = pcStr + p_PrmGT421.GT421_Dcn.toString();
    pcStr = pcStr + p_PrmGT421.GT421_Ndc.toString();
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
}