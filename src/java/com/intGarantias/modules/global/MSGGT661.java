// Source File Name:   MSGGT661.java

package com.intGarantias.modules.global;

import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.util.Formateo;

import java.util.*;
import java.lang.*;
import org.apache.turbine.util.Log;

public class MSGGT661
{
  //---------------------------------------------------------------------------------------
  public static class vPpv
  { public StringBuffer GT661_Ppv = new StringBuffer(9  );      //9(07)V9(2)  135 Valor Presupuesto
    public String getPpv()         {return GT661_Ppv.toString();}
  }
  public static class vEap
  { public StringBuffer GT661_Eap = new StringBuffer(5  );      //9(03)V9(2)   75 % Estado Anterior
    public String getEap()         {return GT661_Eap.toString();}
  }
  public static class vEav
  { public StringBuffer GT661_Eav = new StringBuffer(9  );      //9(07)V9(2)  135 Valor Estado Anterior
    public String getEav()         {return GT661_Eav.toString();}
  }
  //---------------------------------------------------------------------------------------
  public static class Buf_MsgGT661
  {
    public StringBuffer GT661_Idr = new StringBuffer(1  );   //X(01)         1 Idr Host
    public StringBuffer GT661_Gti = new StringBuffer(10 );   //X(10)        11 Id.Garantia a buscar
    public StringBuffer GT661_Seq = new StringBuffer(3  );   //9(03)        14 Secuencia
    public StringBuffer GT661_Dsb = new StringBuffer(30 );   //X(30)        44 Descripcion PI
    public StringBuffer GT661_Ctr = new StringBuffer(30 );   //X(30)        74 Constructora
    public StringBuffer GT661_Fin = new StringBuffer(8  );   //9(08)        82 Fecha Inicio
    public StringBuffer GT661_Fte = new StringBuffer(8  );   //9(08)        90 Fecha EstTermino
    public StringBuffer GT661_Ped = new StringBuffer(7  );   //9(07)        97 PermEdificacion
    public StringBuffer GT661_Fed = new StringBuffer(8  );   //9(08)       105 Fecha PED
    public StringBuffer GT661_Spc = new StringBuffer(5  );   //9(05)       110 Sup a Construir
    public StringBuffer GT661_Sps = new StringBuffer(5  );   //9(05)       115 Sup Saldo
    public StringBuffer GT661_Rem = new StringBuffer(7  );   //9(07)       122 RecepMunicipal
    public StringBuffer GT661_Vsg = new StringBuffer(15 );   //9(11)V9(4)  137 VSeguro <Trj>
    public StringBuffer GT661_Dpy = new StringBuffer(184);   //X(184)      321 46*4 DescProyecto
    public StringBuffer GT661_Occ = new StringBuffer(184);   //X(184)      505 46*4 ObsContrato
    public StringBuffer GT661_Opp = new StringBuffer(184);   //X(184)      689 46*4 ObsEstado Pago
    public StringBuffer GT661_Sqd = new StringBuffer(3  );   //9(03)       692 Nro Bienes
    public StringBuffer GT661_Swq = new StringBuffer(1  );   //X(01)       693 Idr Sqd
    public StringBuffer GT661_Itg = new StringBuffer(5  );   //X(05)       698 Idr TipoGtia
    public StringBuffer GT661_Dpc = new StringBuffer(10 );   //X(10)       708 PContable
    public Vector      vGT661_Ppv = new Vector(15);          //X(135)      843 Valor Presupuesto
    public Vector      vGT661_Eap = new Vector(15);          //X(75)       918 % Estado Anterior
    public Vector      vGT661_Eav = new Vector(15);          //X(135)     1053 Valor Estado Anterior

    public String getIdr()         {return GT661_Idr.toString();}
    public String getGti()         {return GT661_Gti.toString();}
    public String getSeq()         {return GT661_Seq.toString();}
    public String getDsb()         {return GT661_Dsb.toString();}
    public String getCtr()         {return GT661_Ctr.toString();}
    public String getFin()         {return GT661_Fin.toString();}
    public String getFte()         {return GT661_Fte.toString();}
    public String getPed()         {return GT661_Ped.toString();}
    public String getFed()         {return GT661_Fed.toString();}
    public String getSpc()         {return GT661_Spc.toString();}
    public String getSps()         {return GT661_Sps.toString();}
    public String getRem()         {return GT661_Rem.toString();}
    public String getVsg()         {return GT661_Vsg.toString();}
    public String getDpy()         {return GT661_Dpy.toString();}
    public String getOcc()         {return GT661_Occ.toString();}
    public String getOpp()         {return GT661_Opp.toString();}
    public String getSqd()         {return GT661_Sqd.toString();}
    public String getSwq()         {return GT661_Swq.toString();}
    public String getItg()         {return GT661_Itg.toString();}
    public String getDpc()         {return GT661_Dpc.toString();}
    public Vector getVecPpv()      {return vGT661_Ppv;}
    public Vector getVecEap()      {return vGT661_Eap;}
    public Vector getVecEav()      {return vGT661_Eav;}

    public Formateo f = new Formateo();
  //public String fgetFts()        {return f.FormatDate(getFts());}
  //public String fgetVuf()        {return f.Formato(getVuf(),"9.999.999",2,2,',');}
  //public String fgetVtc()        {return f.Formato(getVtc(),"9.999.999",2,2,',');}
  //public String fgetUdd()        {return f.Formato(getUdd(),"9.999.999",2,2,',');}
  //public String fgetVsg()        {return f.Formato(getVsg(),"99.999.999.999",4,4,',');}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_MsgGT661 Inicia_MsgGT661 ()
  {
    Buf_MsgGT661 l_MsgGT661 = new Buf_MsgGT661();
    l_MsgGT661.GT661_Idr.replace(0, l_MsgGT661.GT661_Idr.capacity(), RUTGEN.Blancos(1  ));
    l_MsgGT661.GT661_Gti.replace(0, l_MsgGT661.GT661_Gti.capacity(), RUTGEN.Blancos(10 ));
    l_MsgGT661.GT661_Seq.replace(0, l_MsgGT661.GT661_Seq.capacity(), RUTGEN.Blancos(3  ));
    l_MsgGT661.GT661_Dsb.replace(0, l_MsgGT661.GT661_Dsb.capacity(), RUTGEN.Blancos(30 ));
    l_MsgGT661.GT661_Ctr.replace(0, l_MsgGT661.GT661_Ctr.capacity(), RUTGEN.Blancos(30 ));
    l_MsgGT661.GT661_Fin.replace(0, l_MsgGT661.GT661_Fin.capacity(), RUTGEN.Blancos(8  ));
    l_MsgGT661.GT661_Fte.replace(0, l_MsgGT661.GT661_Fte.capacity(), RUTGEN.Blancos(8  ));
    l_MsgGT661.GT661_Ped.replace(0, l_MsgGT661.GT661_Ped.capacity(), RUTGEN.Blancos(7  ));
    l_MsgGT661.GT661_Fed.replace(0, l_MsgGT661.GT661_Fed.capacity(), RUTGEN.Blancos(8  ));
    l_MsgGT661.GT661_Spc.replace(0, l_MsgGT661.GT661_Spc.capacity(), RUTGEN.Blancos(5  ));
    l_MsgGT661.GT661_Sps.replace(0, l_MsgGT661.GT661_Sps.capacity(), RUTGEN.Blancos(5  ));
    l_MsgGT661.GT661_Rem.replace(0, l_MsgGT661.GT661_Rem.capacity(), RUTGEN.Blancos(7  ));
    l_MsgGT661.GT661_Vsg.replace(0, l_MsgGT661.GT661_Vsg.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT661.GT661_Dpy.replace(0, l_MsgGT661.GT661_Dpy.capacity(), RUTGEN.Blancos(184));
    l_MsgGT661.GT661_Occ.replace(0, l_MsgGT661.GT661_Occ.capacity(), RUTGEN.Blancos(184));
    l_MsgGT661.GT661_Opp.replace(0, l_MsgGT661.GT661_Opp.capacity(), RUTGEN.Blancos(184));
    l_MsgGT661.GT661_Sqd.replace(0, l_MsgGT661.GT661_Sqd.capacity(), RUTGEN.Blancos(3  ));
    l_MsgGT661.GT661_Swq.replace(0, l_MsgGT661.GT661_Swq.capacity(), RUTGEN.Blancos(1  ));
    l_MsgGT661.GT661_Itg.replace(0, l_MsgGT661.GT661_Itg.capacity(), RUTGEN.Blancos(5  ));
    l_MsgGT661.GT661_Dpc.replace(0, l_MsgGT661.GT661_Dpc.capacity(), RUTGEN.Blancos(10 ));
    for (int i=0; i<15; i++)
        {
          vPpv Tap = new vPpv();
          Tap.GT661_Ppv.replace(0, Tap.GT661_Ppv.capacity(), RUTGEN.Blancos(9));
          l_MsgGT661.vGT661_Ppv.add(Tap);
        }
    for (int i=0; i<15; i++)
        {
          vEap Tap = new vEap();
          Tap.GT661_Eap.replace(0, Tap.GT661_Eap.capacity(), RUTGEN.Blancos(5));
          l_MsgGT661.vGT661_Eap.add(Tap);
        }
    for (int i=0; i<15; i++)
        {
          vEav Tap = new vEav();
          Tap.GT661_Eav.replace(0, Tap.GT661_Eav.capacity(), RUTGEN.Blancos(9));
          l_MsgGT661.vGT661_Eav.add(Tap);
        }
    return l_MsgGT661;
  }
  //---------------------------------------------------------------------------------------
  public static Buf_MsgGT661 LSet_A_MsgGT661(String pcStr)
  {
    Buf_MsgGT661 l_MsgGT661 = new Buf_MsgGT661();
    Vector vDatos = LSet_A_vMsgGT661(pcStr);
    l_MsgGT661 = (Buf_MsgGT661)vDatos.elementAt(0);
    return l_MsgGT661;
  }
  //---------------------------------------------------------------------------------------
  public static Vector LSet_A_vMsgGT661(String pcStr)
  {
    Vector vec = new Vector();
    int p = 0;
    String lcData = "";
    Buf_MsgGT661 l_MsgGT661 = new Buf_MsgGT661();
    l_MsgGT661.GT661_Idr.replace(0, l_MsgGT661.GT661_Idr.capacity(), pcStr.substring(p, p + l_MsgGT661.GT661_Idr.capacity())); p = p + l_MsgGT661.GT661_Idr.capacity();
    l_MsgGT661.GT661_Gti.replace(0, l_MsgGT661.GT661_Gti.capacity(), pcStr.substring(p, p + l_MsgGT661.GT661_Gti.capacity())); p = p + l_MsgGT661.GT661_Gti.capacity();
    l_MsgGT661.GT661_Seq.replace(0, l_MsgGT661.GT661_Seq.capacity(), pcStr.substring(p, p + l_MsgGT661.GT661_Seq.capacity())); p = p + l_MsgGT661.GT661_Seq.capacity();
    l_MsgGT661.GT661_Dsb.replace(0, l_MsgGT661.GT661_Dsb.capacity(), pcStr.substring(p, p + l_MsgGT661.GT661_Dsb.capacity())); p = p + l_MsgGT661.GT661_Dsb.capacity();
    l_MsgGT661.GT661_Ctr.replace(0, l_MsgGT661.GT661_Ctr.capacity(), pcStr.substring(p, p + l_MsgGT661.GT661_Ctr.capacity())); p = p + l_MsgGT661.GT661_Ctr.capacity();
    l_MsgGT661.GT661_Fin.replace(0, l_MsgGT661.GT661_Fin.capacity(), pcStr.substring(p, p + l_MsgGT661.GT661_Fin.capacity())); p = p + l_MsgGT661.GT661_Fin.capacity();
    l_MsgGT661.GT661_Fte.replace(0, l_MsgGT661.GT661_Fte.capacity(), pcStr.substring(p, p + l_MsgGT661.GT661_Fte.capacity())); p = p + l_MsgGT661.GT661_Fte.capacity();
    l_MsgGT661.GT661_Ped.replace(0, l_MsgGT661.GT661_Ped.capacity(), pcStr.substring(p, p + l_MsgGT661.GT661_Ped.capacity())); p = p + l_MsgGT661.GT661_Ped.capacity();
    l_MsgGT661.GT661_Fed.replace(0, l_MsgGT661.GT661_Fed.capacity(), pcStr.substring(p, p + l_MsgGT661.GT661_Fed.capacity())); p = p + l_MsgGT661.GT661_Fed.capacity();
    l_MsgGT661.GT661_Spc.replace(0, l_MsgGT661.GT661_Spc.capacity(), pcStr.substring(p, p + l_MsgGT661.GT661_Spc.capacity())); p = p + l_MsgGT661.GT661_Spc.capacity();
    l_MsgGT661.GT661_Sps.replace(0, l_MsgGT661.GT661_Sps.capacity(), pcStr.substring(p, p + l_MsgGT661.GT661_Sps.capacity())); p = p + l_MsgGT661.GT661_Sps.capacity();
    l_MsgGT661.GT661_Rem.replace(0, l_MsgGT661.GT661_Rem.capacity(), pcStr.substring(p, p + l_MsgGT661.GT661_Rem.capacity())); p = p + l_MsgGT661.GT661_Rem.capacity();
    l_MsgGT661.GT661_Vsg.replace(0, l_MsgGT661.GT661_Vsg.capacity(), pcStr.substring(p, p + l_MsgGT661.GT661_Vsg.capacity())); p = p + l_MsgGT661.GT661_Vsg.capacity();
    l_MsgGT661.GT661_Dpy.replace(0, l_MsgGT661.GT661_Dpy.capacity(), pcStr.substring(p, p + l_MsgGT661.GT661_Dpy.capacity())); p = p + l_MsgGT661.GT661_Dpy.capacity();
    l_MsgGT661.GT661_Occ.replace(0, l_MsgGT661.GT661_Occ.capacity(), pcStr.substring(p, p + l_MsgGT661.GT661_Occ.capacity())); p = p + l_MsgGT661.GT661_Occ.capacity();
    l_MsgGT661.GT661_Opp.replace(0, l_MsgGT661.GT661_Opp.capacity(), pcStr.substring(p, p + l_MsgGT661.GT661_Opp.capacity())); p = p + l_MsgGT661.GT661_Opp.capacity();
    l_MsgGT661.GT661_Sqd.replace(0, l_MsgGT661.GT661_Sqd.capacity(), pcStr.substring(p, p + l_MsgGT661.GT661_Sqd.capacity())); p = p + l_MsgGT661.GT661_Sqd.capacity();
    l_MsgGT661.GT661_Swq.replace(0, l_MsgGT661.GT661_Swq.capacity(), pcStr.substring(p, p + l_MsgGT661.GT661_Swq.capacity())); p = p + l_MsgGT661.GT661_Swq.capacity();
    l_MsgGT661.GT661_Itg.replace(0, l_MsgGT661.GT661_Itg.capacity(), pcStr.substring(p, p + l_MsgGT661.GT661_Itg.capacity())); p = p + l_MsgGT661.GT661_Itg.capacity();
    l_MsgGT661.GT661_Dpc.replace(0, l_MsgGT661.GT661_Dpc.capacity(), pcStr.substring(p, p + l_MsgGT661.GT661_Dpc.capacity())); p = p + l_MsgGT661.GT661_Dpc.capacity();
    for (int i=0; i<15; i++)
        {
          vPpv Tap = new vPpv();
          Tap.GT661_Ppv.replace(0, Tap.GT661_Ppv.capacity(), pcStr.substring(p, p + Tap.GT661_Ppv.capacity())); p = p + Tap.GT661_Ppv.capacity();
          l_MsgGT661.vGT661_Ppv.add(Tap);
        }
    for (int i=0; i<15; i++)
        {
          vEap Tap = new vEap();
          Tap.GT661_Eap.replace(0, Tap.GT661_Eap.capacity(), pcStr.substring(p, p + Tap.GT661_Eap.capacity())); p = p + Tap.GT661_Eap.capacity();
          l_MsgGT661.vGT661_Eap.add(Tap);
        }
    for (int i=0; i<15; i++)
        {
          vEav Tap = new vEav();
          Tap.GT661_Eav.replace(0, Tap.GT661_Eav.capacity(), pcStr.substring(p, p + Tap.GT661_Eav.capacity())); p = p + Tap.GT661_Eav.capacity();
          l_MsgGT661.vGT661_Eav.add(Tap);
        }
    vec.add(l_MsgGT661);
    return vec;
  }
  //---------------------------------------------------------------------------------------
  public static String LSet_De_MsgGT661 (Buf_MsgGT661 p_MsgGT661)
  {
    String pcStr = "";
    pcStr = pcStr + p_MsgGT661.GT661_Idr.toString();
    pcStr = pcStr + p_MsgGT661.GT661_Gti.toString();
    pcStr = pcStr + p_MsgGT661.GT661_Seq.toString();
    pcStr = pcStr + p_MsgGT661.GT661_Dsb.toString();
    pcStr = pcStr + p_MsgGT661.GT661_Ctr.toString();
    pcStr = pcStr + p_MsgGT661.GT661_Fin.toString();
    pcStr = pcStr + p_MsgGT661.GT661_Fte.toString();
    pcStr = pcStr + p_MsgGT661.GT661_Ped.toString();
    pcStr = pcStr + p_MsgGT661.GT661_Fed.toString();
    pcStr = pcStr + p_MsgGT661.GT661_Spc.toString();
    pcStr = pcStr + p_MsgGT661.GT661_Sps.toString();
    pcStr = pcStr + p_MsgGT661.GT661_Rem.toString();
    pcStr = pcStr + p_MsgGT661.GT661_Vsg.toString();
    pcStr = pcStr + p_MsgGT661.GT661_Dpy.toString();
    pcStr = pcStr + p_MsgGT661.GT661_Occ.toString();
    pcStr = pcStr + p_MsgGT661.GT661_Opp.toString();
    pcStr = pcStr + p_MsgGT661.GT661_Sqd.toString();
    pcStr = pcStr + p_MsgGT661.GT661_Swq.toString();
    pcStr = pcStr + p_MsgGT661.GT661_Itg.toString();
    pcStr = pcStr + p_MsgGT661.GT661_Dpc.toString();
    for (int i=0; i<15; i++)
        { pcStr = pcStr + ((vPpv)p_MsgGT661.vGT661_Ppv.elementAt(i)).GT661_Ppv.toString(); }
    for (int i=0; i<15; i++)
        { pcStr = pcStr + ((vEap)p_MsgGT661.vGT661_Eap.elementAt(i)).GT661_Eap.toString(); }
    for (int i=0; i<15; i++)
        { pcStr = pcStr + ((vEav)p_MsgGT661.vGT661_Eav.elementAt(i)).GT661_Eav.toString(); }
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
}