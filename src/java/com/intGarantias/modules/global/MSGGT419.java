// Source File Name:   MSGGT419.java

package com.intGarantias.modules.global;

import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.util.Formateo;
import com.FHTServlet.modules.util.Constants;

import java.util.*;
import java.lang.*;
import com.workingdogs.village.Record;
import org.apache.turbine.om.peer.BasePeer;
import org.apache.turbine.util.Log;

public class MSGGT419
{
  //===============================================================================================================================
  public static class Buf_MsgGT419
  {
    public StringBuffer GT419_Idr = new StringBuffer(1  );      //X(01)          Idr Host
    public StringBuffer GT419_Gti = new StringBuffer(10 );      //X(10)          Id. Garantia
    public StringBuffer GT419_Seq = new StringBuffer(3  );      //9(03)          Id. Bien
    public StringBuffer GT419_Itb = new StringBuffer(2  );      //X(02)          Idr Tipo Bien
    public StringBuffer GT419_Itg = new StringBuffer(5  );      //X(05)          Idr Tipo Garantia
    public StringBuffer GT419_Dpc = new StringBuffer(10 );      //X(10)          PContable
    public StringBuffer GT419_Dsg = new StringBuffer(100);      //X(100)         Descripcion Garantia
    public StringBuffer GT419_Fui = new StringBuffer(19 );      //X(19)          Folio Unico Instrumento
    public StringBuffer GT419_Nmi = new StringBuffer(30 );      //X(30)          Nemotecnico Instrumento
    public StringBuffer GT419_Mnd = new StringBuffer(3  );      //X(03)          Moneda Instrumento
    public StringBuffer GT419_Nml = new StringBuffer(15 );      //9(13)V9(2)     Nominal Instrumento
    public StringBuffer GT419_Lin = new StringBuffer(19 );      //X(19)          Linea CMA Instrumento
    public StringBuffer GT419_Dvf = new StringBuffer(1  );      //X(01)          Idr Diversificacion
    public StringBuffer GT419_Fev = new StringBuffer(8  );      //9(08)          Fecha Valor
    public StringBuffer GT419_Cnt = new StringBuffer(11 );      //9(11)          Cantidad
    public StringBuffer GT419_Vcn = new StringBuffer(15 );      //9(13)V9(2)     Valor Contable

    public String getIdr()         {return GT419_Idr.toString();}
    public String getGti()         {return GT419_Gti.toString();}
    public String getSeq()         {return GT419_Seq.toString();}
    public String getItb()         {return GT419_Itb.toString();}
    public String getItg()         {return GT419_Itg.toString();}
    public String getDpc()         {return GT419_Dpc.toString();}
    public String getDsg()         {return GT419_Dsg.toString();}
    public String getFui()         {return GT419_Fui.toString();}
    public String getNmi()         {return GT419_Nmi.toString();}
    public String getMnd()         {return GT419_Mnd.toString();}
    public String getNml()         {return GT419_Nml.toString();}
    public String getLin()         {return GT419_Lin.toString();}
    public String getDvf()         {return GT419_Dvf.toString();}
    public String getFev()         {return GT419_Fev.toString();}
    public String getCnt()         {return GT419_Cnt.toString();}
    public String getVcn()         {return GT419_Vcn.toString();}

    public Formateo f = new Formateo();
    public String fgetFev()        {return f.FormatDate(getFev());}
    public String fgetNml()        {return f.Formato(getNml(),"9.999.999.999.999",2,2,',');}
    public String fgetCnt()        {return f.Formato(getCnt(),"9.999.999.999.999",0,0,',');}
    public String fgetVcn()        {return f.Formato(getVcn(),"9.999.999.999.999",2,2,',');}
    public String fgetDvf()        {if (getDvf().equals("S")) return "SI"; else return "NO";}
  }
  //===============================================================================================================================
  public static Buf_MsgGT419 Inicia_MsgGT419()
  {
    Buf_MsgGT419 l_MsgGT419 = new Buf_MsgGT419();
    l_MsgGT419.GT419_Idr.replace(0, l_MsgGT419.GT419_Idr.capacity(), RUTGEN.Blancos(l_MsgGT419.GT419_Idr));
    l_MsgGT419.GT419_Gti.replace(0, l_MsgGT419.GT419_Gti.capacity(), RUTGEN.Blancos(l_MsgGT419.GT419_Gti));
    l_MsgGT419.GT419_Seq.replace(0, l_MsgGT419.GT419_Seq.capacity(), RUTGEN.Blancos(l_MsgGT419.GT419_Seq));
    l_MsgGT419.GT419_Itb.replace(0, l_MsgGT419.GT419_Itb.capacity(), RUTGEN.Blancos(l_MsgGT419.GT419_Itb));
    l_MsgGT419.GT419_Itg.replace(0, l_MsgGT419.GT419_Itg.capacity(), RUTGEN.Blancos(l_MsgGT419.GT419_Itg));
    l_MsgGT419.GT419_Dpc.replace(0, l_MsgGT419.GT419_Dpc.capacity(), RUTGEN.Blancos(l_MsgGT419.GT419_Dpc));
    l_MsgGT419.GT419_Dsg.replace(0, l_MsgGT419.GT419_Dsg.capacity(), RUTGEN.Blancos(l_MsgGT419.GT419_Dsg));
    l_MsgGT419.GT419_Fui.replace(0, l_MsgGT419.GT419_Fui.capacity(), RUTGEN.Blancos(l_MsgGT419.GT419_Fui));
    l_MsgGT419.GT419_Nmi.replace(0, l_MsgGT419.GT419_Nmi.capacity(), RUTGEN.Blancos(l_MsgGT419.GT419_Nmi));
    l_MsgGT419.GT419_Mnd.replace(0, l_MsgGT419.GT419_Mnd.capacity(), RUTGEN.Blancos(l_MsgGT419.GT419_Mnd));
    l_MsgGT419.GT419_Nml.replace(0, l_MsgGT419.GT419_Nml.capacity(), RUTGEN.Blancos(l_MsgGT419.GT419_Nml));
    l_MsgGT419.GT419_Lin.replace(0, l_MsgGT419.GT419_Lin.capacity(), RUTGEN.Blancos(l_MsgGT419.GT419_Lin));
    l_MsgGT419.GT419_Dvf.replace(0, l_MsgGT419.GT419_Dvf.capacity(), RUTGEN.Blancos(l_MsgGT419.GT419_Dvf));
    l_MsgGT419.GT419_Fev.replace(0, l_MsgGT419.GT419_Fev.capacity(), RUTGEN.Blancos(l_MsgGT419.GT419_Fev));
    l_MsgGT419.GT419_Cnt.replace(0, l_MsgGT419.GT419_Cnt.capacity(), RUTGEN.Blancos(l_MsgGT419.GT419_Cnt));
    l_MsgGT419.GT419_Vcn.replace(0, l_MsgGT419.GT419_Vcn.capacity(), RUTGEN.Blancos(l_MsgGT419.GT419_Vcn));
    return l_MsgGT419;
  }
  //===============================================================================================================================
  public static void Consulta_MsgGT419(Buf_MsgGT419 pMsgGT419) throws Exception
  {
    String gbsKEY  = pMsgGT419.getGti()+pMsgGT419.getSeq();
    String sqlTEXT = elStmt(gbsKEY);
    try
       {
         Vector vQuery = BasePeer.executeQuery(sqlTEXT, Constants.BASE_GAR);
         if (vQuery.size()==0)
            {
              RUTGEN.MoverA(pMsgGT419.GT419_Idr, "1");
              RUTGEN.MoverA(pMsgGT419.GT419_Dsg, "No Existe Contrato");
              return;
            }
         if (vQuery.size()!=1)
            {
              RUTGEN.MoverA(pMsgGT419.GT419_Idr, "2");
              RUTGEN.MoverA(pMsgGT419.GT419_Dsg, "Inconsistencia de Datos (Muchos)");
              return;
            }
         Record Reg = (Record)vQuery.elementAt(0);
         RUTGEN.MoverA(pMsgGT419.GT419_Idr, "");
         RUTGEN.MoverA(pMsgGT419.GT419_Gti, gbsKEY.substring(0,10));
         RUTGEN.MoverA(pMsgGT419.GT419_Seq, gbsKEY.substring(10));
         RUTGEN.MoverA(pMsgGT419.GT419_Itb, Reg.getValue("GBS_ITB").asString());
         RUTGEN.MoverA(pMsgGT419.GT419_Itg, Reg.getValue("GBS_ITG").asString());
         RUTGEN.MoverA(pMsgGT419.GT419_Dpc, Reg.getValue("GBS_DPC").asString());
         RUTGEN.MoverA(pMsgGT419.GT419_Dsg, Reg.getValue("GBS_DSG").asString());
         RUTGEN.MoverA(pMsgGT419.GT419_Fui, Reg.getValue("GBS_FUI").asString());
         RUTGEN.MoverA(pMsgGT419.GT419_Nmi, Reg.getValue("GBS_NMI").asString());
         RUTGEN.MoverA(pMsgGT419.GT419_Mnd, Reg.getValue("GBS_MND").asInt());
         RUTGEN.MoverA(pMsgGT419.GT419_Nml, Reg.getValue("GBS_NML").asBigDecimal(),15,2);
         RUTGEN.MoverA(pMsgGT419.GT419_Lin, Reg.getValue("GBS_LIN").asString());
         RUTGEN.MoverA(pMsgGT419.GT419_Dvf, Reg.getValue("GBS_DVF").asString());
         RUTGEN.MoverA(pMsgGT419.GT419_Fev, Reg.getValue("GBS_FEV").asUtilDate());
         RUTGEN.MoverA(pMsgGT419.GT419_Cnt, Reg.getValue("GBS_CNT").asBigDecimal(),11,0);
         RUTGEN.MoverA(pMsgGT419.GT419_Vcn, Reg.getValue("GBS_VCN").asBigDecimal(),15,2);
       }
    catch(Exception e)
       {
         Log.debug("MSGGT419(Consulta_MsgGT419): ERROR: SQL fallo: [" + e.getMessage() + "]");
         e.printStackTrace();
       }
  }
  //-------------------------------------------------------------------------------------------
  static public String elStmt(String pcGBS)
  {
    String sqlText = "Select                                                                                                 "
                   + "                                                                                             GBS_ITB,  "
                   + "                                                                                             GBS_ITG,  "
                   + "                                                                                             GBS_DPC,  "
                   + "    (Select GTI_DSG From TB_GTI Where GTI_SIS = GBS_SIS And GTI_NCN = GBS_NCN)               GBS_DSG,  "
                   + "     SUBSTR(GBS_DSB,1,19)                                                                    GBS_FUI,  "
                   + "     SUBSTR(GBS_DSB,21,30)                                                                   GBS_NMI,  "
                   + "    (Select GTI_MND From TB_GTI Where GTI_SIS = GBS_SIS And GTI_NCN = GBS_NCN)               GBS_MND,  "
                   + "     TO_NUMBER(SUBSTR(GBS_DSB,52,15))/100                                                    GBS_NML,  "
                   + "     SUBSTR(GBS_DSB,68,12)                                                                   GBS_LIN,  "
                   + "     SUBSTR(GBS_DSB,81,1)                                                                    GBS_DVF,  "
                   + "     GBS_FRV                                                                                 GBS_FEV,  "
                   + "     GBS_VTT                                                                                 GBS_CNT,  "
                   + "                                                                                             GBS_VCN   "
                   + "  From TB_GBS                                                                                          "
                   + " Where                                                                                                 "
                   + "       GBS_SIS  = '" + pcGBS.substring(0,3) + "'                                                       "
                   + "   And GBS_NCN  = '" + pcGBS.substring(3,10) + "'                                                      "
                   + "   And GBS_SEQ  = '" + pcGBS.substring(10) + "'                                                        ";
    return sqlText;
  }
  //===============================================================================================================================
}