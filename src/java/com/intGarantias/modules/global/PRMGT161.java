// Source File Name:   PRMGT161.java

package com.intGarantias.modules.global;

import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.util.Formateo;

import java.util.*;
import java.lang.*;
import org.apache.turbine.util.Log;

public class PRMGT161
{
  public static int g_Max_GT161 = 500;
  //---------------------------------------------------------------------------------------
  public static class Bff_PrmGT161
  {
    public StringBuffer GT161_Cti = new StringBuffer(2  );  //9(02)       35 Tipo Inscripcion     
    public StringBuffer GT161_Nti = new StringBuffer(22 );  //X(22)       57 Nombre TInscripcion  
    public StringBuffer GT161_Csv = new StringBuffer(3  );  //X(03)       60 Conservador          
    public StringBuffer GT161_Lcl = new StringBuffer(15 );  //X(15)       75 Nombre Conservador   
    public StringBuffer GT161_Rpo = new StringBuffer(4  );  //9(04)       79 Repertorio (A�o)     
    public StringBuffer GT161_Foj = new StringBuffer(7  );  //9(07)       86 Fojas                
    public StringBuffer GT161_Nin = new StringBuffer(7  );  //9(07)       93 Numero               
    public StringBuffer GT161_Grd = new StringBuffer(1  );  //9(01)       94 Grado (1/2/3/...)    

    public String getCti()         {return GT161_Cti.toString();}
    public String getNti()         {return GT161_Nti.toString();}
    public String getCsv()         {return GT161_Csv.toString();}
    public String getLcl()         {return GT161_Lcl.toString();}
    public String getRpo()         {return GT161_Rpo.toString();}
    public String getFoj()         {return GT161_Foj.toString();}
    public String getNin()         {return GT161_Nin.toString();}
    public String getGrd()         {return GT161_Grd.toString();}

    public Formateo f = new Formateo();
  }
  //---------------------------------------------------------------------------------------
  public static class Buf_PrmGT161
  {
    public StringBuffer GT161_Idr = new StringBuffer(1 );   //X(01)        1 Idr Inscripciones/Prohibiciones
    public StringBuffer GT161_Nrv = new StringBuffer(3 );   //9(03)        4 Nro Items                                 
    public Vector       GT161_Tap = new Vector();           //X(2444)   2448 Tabla Items                               
    public StringBuffer GT161_Psw = new StringBuffer(1 );
    public StringBuffer GT161_Idx = new StringBuffer(2 );
    public StringBuffer GT161_Cti = new StringBuffer(2 );
    public StringBuffer GT161_Nti = new StringBuffer(22);
    public StringBuffer GT161_Csv = new StringBuffer(3 );
    public StringBuffer GT161_Lcl = new StringBuffer(15);
    public StringBuffer GT161_Rpo = new StringBuffer(4 );
    public StringBuffer GT161_Foj = new StringBuffer(7 );
    public StringBuffer GT161_Nin = new StringBuffer(7 );
    public StringBuffer GT161_Grd = new StringBuffer(1 );

    public String getIdr()  {return GT161_Idr.toString();}
    public String getNrv()  {return GT161_Nrv.toString();}
    public Vector getTap()  {return GT161_Tap;}
    public String getPsw()  {return GT161_Psw.toString();}
    public String getIdx()  {return GT161_Idx.toString();}
    public String getCti()  {return GT161_Cti.toString();}
    public String getNti()  {return GT161_Nti.toString();}
    public String getCsv()  {return GT161_Csv.toString();}
    public String getLcl()  {return GT161_Lcl.toString();}
    public String getRpo()  {return GT161_Rpo.toString();}
    public String getFoj()  {return GT161_Foj.toString();}
    public String getNin()  {return GT161_Nin.toString();}
    public String getGrd()  {return GT161_Grd.toString();}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_PrmGT161 Inicia_PrmGT161()
  {
    Buf_PrmGT161 l_PrmGT161 = new Buf_PrmGT161();
    l_PrmGT161.GT161_Idr.replace(0,l_PrmGT161.GT161_Idr.capacity(), RUTGEN.Blancos(1 ));
    l_PrmGT161.GT161_Nrv.replace(0,l_PrmGT161.GT161_Nrv.capacity(), RUTGEN.Blancos(3 ));
    int p=0;
    for(int i=0; i<g_Max_GT161; i++)
    {
      Bff_PrmGT161 Tap = new Bff_PrmGT161();
      Tap.GT161_Cti.replace(0,Tap.GT161_Cti.capacity(), RUTGEN.Blancos(2  ));
      Tap.GT161_Nti.replace(0,Tap.GT161_Nti.capacity(), RUTGEN.Blancos(22 ));
      Tap.GT161_Csv.replace(0,Tap.GT161_Csv.capacity(), RUTGEN.Blancos(3  ));
      Tap.GT161_Lcl.replace(0,Tap.GT161_Lcl.capacity(), RUTGEN.Blancos(15 ));
      Tap.GT161_Rpo.replace(0,Tap.GT161_Rpo.capacity(), RUTGEN.Blancos(4  ));
      Tap.GT161_Foj.replace(0,Tap.GT161_Foj.capacity(), RUTGEN.Blancos(7  ));
      Tap.GT161_Nin.replace(0,Tap.GT161_Nin.capacity(), RUTGEN.Blancos(7  ));
      Tap.GT161_Grd.replace(0,Tap.GT161_Grd.capacity(), RUTGEN.Blancos(1  ));
      l_PrmGT161.GT161_Tap.add(Tap);
    }
    l_PrmGT161.GT161_Psw.replace(0,l_PrmGT161.GT161_Psw.capacity(), RUTGEN.Blancos(1 ));
    l_PrmGT161.GT161_Idx.replace(0,l_PrmGT161.GT161_Idx.capacity(), RUTGEN.Blancos(2 ));
    l_PrmGT161.GT161_Cti.replace(0,l_PrmGT161.GT161_Cti.capacity(), RUTGEN.Blancos(2 ));
    l_PrmGT161.GT161_Nti.replace(0,l_PrmGT161.GT161_Nti.capacity(), RUTGEN.Blancos(22));
    l_PrmGT161.GT161_Csv.replace(0,l_PrmGT161.GT161_Csv.capacity(), RUTGEN.Blancos(3 ));
    l_PrmGT161.GT161_Lcl.replace(0,l_PrmGT161.GT161_Lcl.capacity(), RUTGEN.Blancos(15));
    l_PrmGT161.GT161_Rpo.replace(0,l_PrmGT161.GT161_Rpo.capacity(), RUTGEN.Blancos(4 ));
    l_PrmGT161.GT161_Foj.replace(0,l_PrmGT161.GT161_Foj.capacity(), RUTGEN.Blancos(7 ));
    l_PrmGT161.GT161_Nin.replace(0,l_PrmGT161.GT161_Nin.capacity(), RUTGEN.Blancos(7 ));
    l_PrmGT161.GT161_Grd.replace(0,l_PrmGT161.GT161_Grd.capacity(), RUTGEN.Blancos(1 ));
    return l_PrmGT161;
  }
  //---------------------------------------------------------------------------------------
  static public Buf_PrmGT161 LSet_A_PrmGT161(String pcStr)
  {
    Buf_PrmGT161 l_PrmGT161 = new Buf_PrmGT161();
    PRMGT161 vPrmGT161 = new PRMGT161();
    Vector vDatos = vPrmGT161.LSet_A_vPrmGT161(pcStr);
    l_PrmGT161 = (PRMGT161.Buf_PrmGT161)vDatos.elementAt(0);
    return l_PrmGT161;
  }
  //---------------------------------------------------------------------------------------
  public Vector LSet_A_vPrmGT161(String pcStr)
  {
    Vector vec = new Vector();
    int p = 0;
    Buf_PrmGT161 l_PrmGT161 = new Buf_PrmGT161();
    l_PrmGT161.GT161_Idr.append(pcStr.substring(p, p + l_PrmGT161.GT161_Idr.capacity())); p = p + l_PrmGT161.GT161_Idr.capacity();
    l_PrmGT161.GT161_Nrv.append(pcStr.substring(p, p + l_PrmGT161.GT161_Nrv.capacity())); p = p + l_PrmGT161.GT161_Nrv.capacity();
    for (int i=0;i< g_Max_GT161;i++)
        {
          Bff_PrmGT161 Tap = new Bff_PrmGT161();
          Tap.GT161_Cti.append(pcStr.substring(p, p + Tap.GT161_Cti.capacity())); p = p + Tap.GT161_Cti.capacity();
          Tap.GT161_Nti.append(pcStr.substring(p, p + Tap.GT161_Nti.capacity())); p = p + Tap.GT161_Nti.capacity();
          Tap.GT161_Csv.append(pcStr.substring(p, p + Tap.GT161_Csv.capacity())); p = p + Tap.GT161_Csv.capacity();
          Tap.GT161_Lcl.append(pcStr.substring(p, p + Tap.GT161_Lcl.capacity())); p = p + Tap.GT161_Lcl.capacity();	        	
          Tap.GT161_Rpo.append(pcStr.substring(p, p + Tap.GT161_Rpo.capacity())); p = p + Tap.GT161_Rpo.capacity();
          Tap.GT161_Foj.append(pcStr.substring(p, p + Tap.GT161_Foj.capacity())); p = p + Tap.GT161_Foj.capacity();
          Tap.GT161_Nin.append(pcStr.substring(p, p + Tap.GT161_Nin.capacity())); p = p + Tap.GT161_Nin.capacity();
          Tap.GT161_Grd.append(pcStr.substring(p, p + Tap.GT161_Grd.capacity())); p = p + Tap.GT161_Grd.capacity();	        	
          l_PrmGT161.GT161_Tap.add(Tap);
        }
    l_PrmGT161.GT161_Psw.append(pcStr.substring(p, p + l_PrmGT161.GT161_Psw.capacity())); p = p + l_PrmGT161.GT161_Psw.capacity();
    l_PrmGT161.GT161_Idx.append(pcStr.substring(p, p + l_PrmGT161.GT161_Idx.capacity())); p = p + l_PrmGT161.GT161_Idx.capacity();
    l_PrmGT161.GT161_Cti.append(pcStr.substring(p, p + l_PrmGT161.GT161_Cti.capacity())); p = p + l_PrmGT161.GT161_Cti.capacity();
    l_PrmGT161.GT161_Nti.append(pcStr.substring(p, p + l_PrmGT161.GT161_Nti.capacity())); p = p + l_PrmGT161.GT161_Nti.capacity();
    l_PrmGT161.GT161_Csv.append(pcStr.substring(p, p + l_PrmGT161.GT161_Csv.capacity())); p = p + l_PrmGT161.GT161_Csv.capacity();
    l_PrmGT161.GT161_Lcl.append(pcStr.substring(p, p + l_PrmGT161.GT161_Lcl.capacity())); p = p + l_PrmGT161.GT161_Lcl.capacity();
    l_PrmGT161.GT161_Rpo.append(pcStr.substring(p, p + l_PrmGT161.GT161_Rpo.capacity())); p = p + l_PrmGT161.GT161_Rpo.capacity();
    l_PrmGT161.GT161_Foj.append(pcStr.substring(p, p + l_PrmGT161.GT161_Foj.capacity())); p = p + l_PrmGT161.GT161_Foj.capacity();
    l_PrmGT161.GT161_Nin.append(pcStr.substring(p, p + l_PrmGT161.GT161_Nin.capacity())); p = p + l_PrmGT161.GT161_Nin.capacity();
    l_PrmGT161.GT161_Grd.append(pcStr.substring(p, p + l_PrmGT161.GT161_Grd.capacity())); p = p + l_PrmGT161.GT161_Grd.capacity();
    vec.add (l_PrmGT161);
    return vec;
  }
  //---------------------------------------------------------------------------------------
  static public String LSet_De_PrmGT161(Buf_PrmGT161 p_PrmGT161)
  {
    String pcStr = "";
    pcStr = pcStr + p_PrmGT161.GT161_Idr.toString();
    pcStr = pcStr + p_PrmGT161.GT161_Nrv.toString();
    for(int i=0; i<p_PrmGT161.GT161_Tap.size(); i++)
    {
      pcStr = pcStr + ((Bff_PrmGT161)p_PrmGT161.GT161_Tap.elementAt(i)).GT161_Cti.toString();
      pcStr = pcStr + ((Bff_PrmGT161)p_PrmGT161.GT161_Tap.elementAt(i)).GT161_Nti.toString();
      pcStr = pcStr + ((Bff_PrmGT161)p_PrmGT161.GT161_Tap.elementAt(i)).GT161_Csv.toString();
      pcStr = pcStr + ((Bff_PrmGT161)p_PrmGT161.GT161_Tap.elementAt(i)).GT161_Lcl.toString();
      pcStr = pcStr + ((Bff_PrmGT161)p_PrmGT161.GT161_Tap.elementAt(i)).GT161_Rpo.toString();
      pcStr = pcStr + ((Bff_PrmGT161)p_PrmGT161.GT161_Tap.elementAt(i)).GT161_Foj.toString();
      pcStr = pcStr + ((Bff_PrmGT161)p_PrmGT161.GT161_Tap.elementAt(i)).GT161_Nin.toString();
      pcStr = pcStr + ((Bff_PrmGT161)p_PrmGT161.GT161_Tap.elementAt(i)).GT161_Grd.toString();
    }
    pcStr = pcStr + p_PrmGT161.GT161_Psw.toString();
    pcStr = pcStr + p_PrmGT161.GT161_Idx.toString();
    pcStr = pcStr + p_PrmGT161.GT161_Cti.toString();
    pcStr = pcStr + p_PrmGT161.GT161_Nti.toString();
    pcStr = pcStr + p_PrmGT161.GT161_Csv.toString();
    pcStr = pcStr + p_PrmGT161.GT161_Lcl.toString();
    pcStr = pcStr + p_PrmGT161.GT161_Rpo.toString();
    pcStr = pcStr + p_PrmGT161.GT161_Foj.toString();
    pcStr = pcStr + p_PrmGT161.GT161_Nin.toString();
    pcStr = pcStr + p_PrmGT161.GT161_Grd.toString();
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
}