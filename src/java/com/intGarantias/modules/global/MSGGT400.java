// Source File Name:   MSGGT400.java

package com.intGarantias.modules.global;

import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.util.Formateo;

import java.util.*;
import java.lang.*;
import org.apache.turbine.util.Log;

public class MSGGT400
{
  public static int g_Max_GT400 = 43;
  //---------------------------------------------------------------------------------------
  public static class Bff_MsgGT400
  {
    public StringBuffer GT400_Cnr = new StringBuffer(10); // As String * 10  'X(10)       10 Id. Contrato (KEY)
    public StringBuffer GT400_Rel = new StringBuffer(5 ); // As String * 5   'X(05)       15 Relacion Cli-Cnr (KEY)
    public StringBuffer GT400_Tpg = new StringBuffer(3 ); // As String * 3   'X(03)       18 Tipo Garantia
    public StringBuffer GT400_Suc = new StringBuffer(3 ); // As String * 3   '9(03)       21 Sucursal
    public StringBuffer GT400_Dcn = new StringBuffer(5 ); // As String * 5   'X(05)       26 Producto
    public StringBuffer GT400_Ndc = new StringBuffer(30); // As String * 30  'X(30)       56 Descripcion Producto
    public StringBuffer GT400_Mnd = new StringBuffer(3 ); // As String * 3   '9(03)       59 Moneda
    public StringBuffer GT400_Vlc = new StringBuffer(15); // As String * 15  '9(13)V9(2)  74 Valor Contable
    public StringBuffer GT400_Fts = new StringBuffer(8 ); // As String * 8   '9(08)       82 Fecha Tasacion
    public StringBuffer GT400_Cmp = new StringBuffer(1 ); // As String * 1   'X(01)       83 Compartida (S/N)

    public String getCnr()         {return GT400_Cnr.toString();}
    public String getRel()         {return GT400_Rel.toString();}
    public String getTpg()         {return GT400_Tpg.toString();}
    public String getSuc()         {return GT400_Suc.toString();}
    public String getDcn()         {return GT400_Dcn.toString();}
    public String getNdc()         {return GT400_Ndc.toString();}
    public String getMnd()         {return GT400_Mnd.toString();}
    public String getVlc()         {return GT400_Vlc.toString();}
    public String getFts()         {return GT400_Fts.toString();}
    public String getCmp()         {return GT400_Cmp.toString();}

    public Formateo f = new Formateo();
    public String fgetCnr()        {
                                     String Cnr = GT400_Cnr.toString();              
                                     return Cnr.substring(0,3) + "-" + Cnr.substring(3,10);
                                   }
    public String fgetRel()        {return GT400_Rel.toString();}
    public String fgetTpg()        {return GT400_Tpg.toString();}
    public String fgetSuc()        {return GT400_Suc.toString();}
    public String fgetDcn()        {return GT400_Dcn.toString();}
    public String fgetNdc()        {return GT400_Ndc.toString();}
    public String fgetMnd()        {return GT400_Mnd.toString();}
    public String fgetVlc()        {return f.Formato(GT400_Vlc.toString(),"9.999.999.999.999",2,2,',');}
    public String fgetFts()        {return f.FormatDate(GT400_Fts.toString());}
    public String fgetCmp()        {
                                     String Cmp = GT400_Cmp.toString();              
                                     if (Cmp.equals("N") )  { return "&nbsp;"; }
                                     return "S";
                                   }            
  }
  //---------------------------------------------------------------------------------------
  public static class Buf_MsgGT400
  {
    public StringBuffer GT400_Idr = new StringBuffer(1);
    public StringBuffer GT400_Rut = new StringBuffer(10);
    public StringBuffer GT400_Nro = new StringBuffer(3);
    public Vector       GT400_Tap = new Vector();

    public String getIdr()         {return GT400_Idr.toString();}
    public String getRut()         {return GT400_Rut.toString();}
    public String getNro()         {return GT400_Nro.toString();}
    public Vector getTab()         {return GT400_Tap;}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_MsgGT400 Inicia_MsgGT400()
  {
    Buf_MsgGT400 l_MsgGT400 = new Buf_MsgGT400();
    l_MsgGT400.GT400_Idr.replace(0,l_MsgGT400.GT400_Idr.capacity(), RUTGEN.Blancos(1));
    l_MsgGT400.GT400_Rut.replace(0,l_MsgGT400.GT400_Rut.capacity(), RUTGEN.Blancos(10));
    l_MsgGT400.GT400_Nro.replace(0,l_MsgGT400.GT400_Nro.capacity(), RUTGEN.Blancos(3));
    int p=0;
    for(int i=0; i<g_Max_GT400; i++)
    {
      Bff_MsgGT400 Tap = new Bff_MsgGT400();
      Tap.GT400_Cnr.replace(0,Tap.GT400_Cnr.capacity(), RUTGEN.Blancos(10));
      Tap.GT400_Rel.replace(0,Tap.GT400_Rel.capacity(), RUTGEN.Blancos(5 ));
      Tap.GT400_Tpg.replace(0,Tap.GT400_Tpg.capacity(), RUTGEN.Blancos(3 ));
      Tap.GT400_Suc.replace(0,Tap.GT400_Suc.capacity(), RUTGEN.Blancos(3 ));
      Tap.GT400_Dcn.replace(0,Tap.GT400_Dcn.capacity(), RUTGEN.Blancos(5 ));
      Tap.GT400_Ndc.replace(0,Tap.GT400_Ndc.capacity(), RUTGEN.Blancos(30));
      Tap.GT400_Mnd.replace(0,Tap.GT400_Mnd.capacity(), RUTGEN.Blancos(3 ));
      Tap.GT400_Vlc.replace(0,Tap.GT400_Vlc.capacity(), RUTGEN.Blancos(15));
      Tap.GT400_Fts.replace(0,Tap.GT400_Fts.capacity(), RUTGEN.Blancos(8 ));
      Tap.GT400_Cmp.replace(0,Tap.GT400_Cmp.capacity(), RUTGEN.Blancos(1 ));
      l_MsgGT400.GT400_Tap.add(Tap);
    }
    return l_MsgGT400;
  }
  //---------------------------------------------------------------------------------------
  static public Buf_MsgGT400 LSet_A_MsgGT400(String pcStr)
  {
    Buf_MsgGT400 l_MsgGT400 = new Buf_MsgGT400();
    MSGGT400 vMsgGT400 = new MSGGT400();
    Vector vDatos = vMsgGT400.LSet_A_vMsgGT400(pcStr);
    l_MsgGT400 = (MSGGT400.Buf_MsgGT400)vDatos.elementAt(0);
    return l_MsgGT400;
  }
  //---------------------------------------------------------------------------------------
  public Vector LSet_A_vMsgGT400(String pcStr)
  {
    Vector vec = new Vector();
    int p = 0;
    Buf_MsgGT400 l_MsgGT400 = new Buf_MsgGT400();                            
    l_MsgGT400.GT400_Idr.append(pcStr.substring(p, p + l_MsgGT400.GT400_Idr.capacity())); p = p + l_MsgGT400.GT400_Idr.capacity();
    l_MsgGT400.GT400_Rut.append(pcStr.substring(p, p + l_MsgGT400.GT400_Rut.capacity())); p = p + l_MsgGT400.GT400_Rut.capacity();
    l_MsgGT400.GT400_Nro.append(pcStr.substring(p, p + l_MsgGT400.GT400_Nro.capacity())); p = p + l_MsgGT400.GT400_Nro.capacity();
    for (int i=0; i<Integer.parseInt(l_MsgGT400.GT400_Nro.toString()); i++)
        {
          Bff_MsgGT400 Tap = new Bff_MsgGT400();
          Tap.GT400_Cnr.append(pcStr.substring(p, p + Tap.GT400_Cnr.capacity())); p = p + Tap.GT400_Cnr.capacity();
          Tap.GT400_Rel.append(pcStr.substring(p, p + Tap.GT400_Rel.capacity())); p = p + Tap.GT400_Rel.capacity();
          Tap.GT400_Tpg.append(pcStr.substring(p, p + Tap.GT400_Tpg.capacity())); p = p + Tap.GT400_Tpg.capacity();
          Tap.GT400_Suc.append(pcStr.substring(p, p + Tap.GT400_Suc.capacity())); p = p + Tap.GT400_Suc.capacity();
          Tap.GT400_Dcn.append(pcStr.substring(p, p + Tap.GT400_Dcn.capacity())); p = p + Tap.GT400_Dcn.capacity();
          Tap.GT400_Ndc.append(pcStr.substring(p, p + Tap.GT400_Ndc.capacity())); p = p + Tap.GT400_Ndc.capacity();
          Tap.GT400_Mnd.append(pcStr.substring(p, p + Tap.GT400_Mnd.capacity())); p = p + Tap.GT400_Mnd.capacity();
          Tap.GT400_Vlc.append(pcStr.substring(p, p + Tap.GT400_Vlc.capacity())); p = p + Tap.GT400_Vlc.capacity();
          Tap.GT400_Fts.append(pcStr.substring(p, p + Tap.GT400_Fts.capacity())); p = p + Tap.GT400_Fts.capacity();
          Tap.GT400_Cmp.append(pcStr.substring(p, p + Tap.GT400_Cmp.capacity())); p = p + Tap.GT400_Cmp.capacity();
          if (Tap.GT400_Cnr.toString().trim().equals("")) { break; }
          l_MsgGT400.GT400_Tap.add(Tap);
        }
    vec.add (l_MsgGT400);
    return vec;
  }
  //---------------------------------------------------------------------------------------
  static public String LSet_De_MsgGT400(Buf_MsgGT400 p_MsgGT400)
  {
    String pcStr = "";
    pcStr = pcStr + p_MsgGT400.GT400_Idr.toString();
    pcStr = pcStr + p_MsgGT400.GT400_Rut.toString();
    pcStr = pcStr + p_MsgGT400.GT400_Nro.toString();
    for(int i=0; i<p_MsgGT400.GT400_Tap.size(); i++)
    {
      pcStr = pcStr + ((Bff_MsgGT400)p_MsgGT400.GT400_Tap.elementAt(i)).GT400_Cnr.toString();
      pcStr = pcStr + ((Bff_MsgGT400)p_MsgGT400.GT400_Tap.elementAt(i)).GT400_Rel.toString();
      pcStr = pcStr + ((Bff_MsgGT400)p_MsgGT400.GT400_Tap.elementAt(i)).GT400_Tpg.toString();
      pcStr = pcStr + ((Bff_MsgGT400)p_MsgGT400.GT400_Tap.elementAt(i)).GT400_Suc.toString();
      pcStr = pcStr + ((Bff_MsgGT400)p_MsgGT400.GT400_Tap.elementAt(i)).GT400_Dcn.toString();
      pcStr = pcStr + ((Bff_MsgGT400)p_MsgGT400.GT400_Tap.elementAt(i)).GT400_Ndc.toString();
      pcStr = pcStr + ((Bff_MsgGT400)p_MsgGT400.GT400_Tap.elementAt(i)).GT400_Mnd.toString();
      pcStr = pcStr + ((Bff_MsgGT400)p_MsgGT400.GT400_Tap.elementAt(i)).GT400_Vlc.toString();
      pcStr = pcStr + ((Bff_MsgGT400)p_MsgGT400.GT400_Tap.elementAt(i)).GT400_Fts.toString();
      pcStr = pcStr + ((Bff_MsgGT400)p_MsgGT400.GT400_Tap.elementAt(i)).GT400_Cmp.toString();
    }
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
}