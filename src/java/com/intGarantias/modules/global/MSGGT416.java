// Source File Name:   MSGGT416.java

package com.intGarantias.modules.global;

import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.util.Formateo;

import java.util.*;
import java.lang.*;
import org.apache.turbine.util.Log;

public class MSGGT416
{
  //---------------------------------------------------------------------------------------
  public static class Grt_Gbs_Det
  {
    public StringBuffer GT416_Gta = new StringBuffer(36);       //X(36)       14 Codigo Accion    
    public StringBuffer GT416_Nse = new StringBuffer(40);       //X(40)       54 Nombre SAEmisora  
    public StringBuffer GT416_Can = new StringBuffer(12);       //9(12)       66 Cantidad Acciones 
    public StringBuffer GT416_Mnd = new StringBuffer(3 );       //9(03)       69 Moneda Accion    
    public StringBuffer GT416_Vlc = new StringBuffer(15);       //9(13)V9(2)  84 VContable        
    public StringBuffer GT416_Fut = new StringBuffer(8 );       //9(08)       92 Moneda Accion    
    public StringBuffer GT416_Uvb = new StringBuffer(15);       //9(13)V9(2) 107 VContable        
    public StringBuffer GT416_Nbe = new StringBuffer(40);       //X(40)      147 Nombre SAEmisora 
    public StringBuffer GT416_Itb = new StringBuffer(2  );      //X(02)      149 Id Tipo Bien
    public StringBuffer GT416_Itg = new StringBuffer(5  );      //X(05)      154 Id Tipo Gtia
    public StringBuffer GT416_Dpc = new StringBuffer(10 );      //X(10)      164 PContable
    public StringBuffer GT416_Csv = new StringBuffer(3  );      //X(03)      167 Conservador
    public StringBuffer GT416_Lcl = new StringBuffer(15 );      //X(15)      182 Localidad
    public StringBuffer GT416_Rpo = new StringBuffer(4  );      //9(04)      186 Repertorio
    public StringBuffer GT416_Foj = new StringBuffer(7  );      //9(07)      193 Fojas
    public StringBuffer GT416_Nin = new StringBuffer(7  );      //9(07)      200 Numero
    public StringBuffer GT416_Vsg = new StringBuffer(15 );      //9(11)V9(4) 215 VAsegurable       
    public StringBuffer GT416_Csg = new StringBuffer(3  );      //9(03)      218 Aseguradora       
    public StringBuffer GT416_Spz = new StringBuffer(10 );      //X(10)      228 Poliza            
    public StringBuffer GT416_Sfv = new StringBuffer(8  );      //9(08)      236 FVcto Seguro      
    public StringBuffer GT416_Sum = new StringBuffer(1  );      //X(01)      237 UMon Seguro       
    public StringBuffer GT416_Svl = new StringBuffer(15 );      //9(11)V9(4) 252 VSeguro <UF>      
    public StringBuffer GT416_Ben = new StringBuffer(40);       //X(40)      147 Nombre SAEmisora 
    public StringBuffer GT416_Fvc = new StringBuffer(8  );      //9(08)      236 FVcto Seguro      

    public String getGta()         {return GT416_Gta.toString();}
    public String getNse()         {return GT416_Nse.toString();}
    public String getCan()         {return GT416_Can.toString();}
    public String getMnd()         {return GT416_Mnd.toString();}
    public String getVlc()         {return GT416_Vlc.toString();}
    public String getFut()         {return GT416_Fut.toString();}
    public String getUvb()         {return GT416_Uvb.toString();}
    public String getNbe()         {return GT416_Nbe.toString();}
    public String getItb()         {return GT416_Itb.toString();}
    public String getItg()         {return GT416_Itg.toString();}
    public String getDpc()         {return GT416_Dpc.toString();}
    public String getCsv()         {return GT416_Csv.toString();}
    public String getLcl()         {return GT416_Lcl.toString();}
    public String getRpo()         {return GT416_Rpo.toString();}
    public String getFoj()         {return GT416_Foj.toString();}
    public String getNin()         {return GT416_Nin.toString();}
    public String getVsg()         {return GT416_Vsg.toString();}
    public String getCsg()         {return GT416_Csg.toString();}
    public String getSpz()         {return GT416_Spz.toString();}
    public String getSfv()         {return GT416_Sfv.toString();}
    public String getSum()         {return GT416_Sum.toString();}
    public String getSvl()         {return GT416_Svl.toString();}
    public String getBen()         {return GT416_Ben.toString();}
    public String getFvc()         {return GT416_Fvc.toString();}

    public Formateo f = new Formateo();
    public String fgetGta()
                                   {
                                     String Cnr = GT416_Gta.toString();              
                                     return Cnr.substring(0,10) + "-" + Cnr.substring(10,12) + "-" + Cnr.substring(12,14);                   
                                   }
    public String fgetCan()        {return f.Formato(GT416_Can.toString(),"999.999.999.999",0,0,',');}
    public String fgetVlc()        {return f.Formato(GT416_Vlc.toString(),"9.999.999.999.999",2,2,',');}
    public String fgetFut()        {return f.FormatDate(GT416_Fut.toString());}
    public String fgetUvb()        {return f.Formato(GT416_Uvb.toString(),"9.999.999.999.999",2,2,',');}
    public String fgetSfv()        {return f.FormatDate(GT416_Sfv.toString());}
    public String fgetVsg()        {return f.Formato(GT416_Vsg.toString(),"9.999.999.999.999",4,4,',');}
    public String fgetSvl()        {return f.Formato(GT416_Svl.toString(),"9.999.999.999.999",4,4,',');}
    public String fgetFvc()        {return f.FormatDate(GT416_Fvc.toString());}
  }
  //---------------------------------------------------------------------------------------
  public static class Buf_MsgGT416
  {
    public StringBuffer GT416_Idr = new StringBuffer(1) ;       //X(01)        1 Idr Host              
    public StringBuffer GT416_Gti = new StringBuffer(10);       //X(10)       11 Id. Garantia (SIS/NCN)
    public StringBuffer GT416_Seq = new StringBuffer(3) ;       //9(03)       14 Total Indices         
    public Grt_Gbs_Det  GT416_Gbs = new Grt_Gbs_Det();          //X(252)     266 Data Detalle

    public String getIdr()         {return GT416_Idr.toString();}
    public String getGti()         {return GT416_Gti.toString();}
    public String getSeq()         {return GT416_Seq.toString();}
    public Grt_Gbs_Det getGbs()    {return GT416_Gbs;}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_MsgGT416 Inicia_MsgGT416()
  {
    Buf_MsgGT416 l_MsgGT416 = new Buf_MsgGT416();
    l_MsgGT416.GT416_Idr.replace(0, l_MsgGT416.GT416_Idr.capacity(), RUTGEN.Blancos(1));
    l_MsgGT416.GT416_Gti.replace(0, l_MsgGT416.GT416_Gti.capacity(), RUTGEN.Blancos(10));
    l_MsgGT416.GT416_Seq.replace(0, l_MsgGT416.GT416_Seq.capacity(), RUTGEN.Blancos(3));
    Grt_Gbs_Det Gbs = new Grt_Gbs_Det();          
    Gbs.GT416_Gta.replace(0, Gbs.GT416_Gta.capacity(), RUTGEN.Blancos(36 ));
    Gbs.GT416_Nse.replace(0, Gbs.GT416_Nse.capacity(), RUTGEN.Blancos(40 ));
    Gbs.GT416_Can.replace(0, Gbs.GT416_Can.capacity(), RUTGEN.Blancos(12 ));
    Gbs.GT416_Mnd.replace(0, Gbs.GT416_Mnd.capacity(), RUTGEN.Blancos(3  ));
    Gbs.GT416_Vlc.replace(0, Gbs.GT416_Vlc.capacity(), RUTGEN.Blancos(15 ));
    Gbs.GT416_Fut.replace(0, Gbs.GT416_Fut.capacity(), RUTGEN.Blancos(8  ));
    Gbs.GT416_Uvb.replace(0, Gbs.GT416_Uvb.capacity(), RUTGEN.Blancos(15 ));
    Gbs.GT416_Nbe.replace(0, Gbs.GT416_Nbe.capacity(), RUTGEN.Blancos(40 ));
    Gbs.GT416_Itb.replace(0, Gbs.GT416_Itb.capacity(), RUTGEN.Blancos(2  ));
    Gbs.GT416_Itg.replace(0, Gbs.GT416_Itg.capacity(), RUTGEN.Blancos(5  ));
    Gbs.GT416_Dpc.replace(0, Gbs.GT416_Dpc.capacity(), RUTGEN.Blancos(10 ));
    Gbs.GT416_Csv.replace(0, Gbs.GT416_Csv.capacity(), RUTGEN.Blancos(3  ));
    Gbs.GT416_Lcl.replace(0, Gbs.GT416_Lcl.capacity(), RUTGEN.Blancos(15 ));
    Gbs.GT416_Rpo.replace(0, Gbs.GT416_Rpo.capacity(), RUTGEN.Blancos(4  ));
    Gbs.GT416_Foj.replace(0, Gbs.GT416_Foj.capacity(), RUTGEN.Blancos(7  ));
    Gbs.GT416_Nin.replace(0, Gbs.GT416_Nin.capacity(), RUTGEN.Blancos(7  ));
    Gbs.GT416_Vsg.replace(0, Gbs.GT416_Vsg.capacity(), RUTGEN.Blancos(15 ));
    Gbs.GT416_Csg.replace(0, Gbs.GT416_Csg.capacity(), RUTGEN.Blancos(3  ));
    Gbs.GT416_Spz.replace(0, Gbs.GT416_Spz.capacity(), RUTGEN.Blancos(10 ));
    Gbs.GT416_Sfv.replace(0, Gbs.GT416_Sfv.capacity(), RUTGEN.Blancos(8  ));
    Gbs.GT416_Sum.replace(0, Gbs.GT416_Sum.capacity(), RUTGEN.Blancos(1  ));
    Gbs.GT416_Svl.replace(0, Gbs.GT416_Svl.capacity(), RUTGEN.Blancos(15 ));
    Gbs.GT416_Ben.replace(0, Gbs.GT416_Ben.capacity(), RUTGEN.Blancos(40 ));
    Gbs.GT416_Fvc.replace(0, Gbs.GT416_Fvc.capacity(), RUTGEN.Blancos(8  ));
    l_MsgGT416.GT416_Gbs = Gbs;
    return l_MsgGT416;
  }
  //---------------------------------------------------------------------------------------
  static public Buf_MsgGT416 LSet_A_MsgGT416(String pcStr)
  {
    Buf_MsgGT416 l_MsgGT416 = new Buf_MsgGT416();
    MSGGT416 vMsgGT416 = new MSGGT416();
    Vector vDatos = vMsgGT416.LSet_A_vMsgGT416(pcStr);
    l_MsgGT416 = (MSGGT416.Buf_MsgGT416)vDatos.elementAt(0);
    return l_MsgGT416;
  }
  //---------------------------------------------------------------------------------------
  public Vector LSet_A_vMsgGT416(String pcStr)
  {
    int p = 0;
    Vector vec = new Vector();
    Buf_MsgGT416 l_MsgGT416 = new Buf_MsgGT416();                              
    l_MsgGT416.GT416_Idr.append(pcStr.substring(p, p + l_MsgGT416.GT416_Idr.capacity())); p = p + l_MsgGT416.GT416_Idr.capacity();
    l_MsgGT416.GT416_Gti.append(pcStr.substring(p, p + l_MsgGT416.GT416_Gti.capacity())); p = p + l_MsgGT416.GT416_Gti.capacity();
    l_MsgGT416.GT416_Seq.append(pcStr.substring(p, p + l_MsgGT416.GT416_Seq.capacity())); p = p + l_MsgGT416.GT416_Seq.capacity();
    Grt_Gbs_Det Gbs = new Grt_Gbs_Det();          
    Gbs.GT416_Gta.append(pcStr.substring(p, p + Gbs.GT416_Gta.capacity())); p = p + Gbs.GT416_Gta.capacity();
    Gbs.GT416_Nse.append(pcStr.substring(p, p + Gbs.GT416_Nse.capacity())); p = p + Gbs.GT416_Nse.capacity();
    Gbs.GT416_Can.append(pcStr.substring(p, p + Gbs.GT416_Can.capacity())); p = p + Gbs.GT416_Can.capacity();
    Gbs.GT416_Mnd.append(pcStr.substring(p, p + Gbs.GT416_Mnd.capacity())); p = p + Gbs.GT416_Mnd.capacity();
    Gbs.GT416_Vlc.append(pcStr.substring(p, p + Gbs.GT416_Vlc.capacity())); p = p + Gbs.GT416_Vlc.capacity();
    Gbs.GT416_Fut.append(pcStr.substring(p, p + Gbs.GT416_Fut.capacity())); p = p + Gbs.GT416_Fut.capacity();
    Gbs.GT416_Uvb.append(pcStr.substring(p, p + Gbs.GT416_Uvb.capacity())); p = p + Gbs.GT416_Uvb.capacity();
    Gbs.GT416_Nbe.append(pcStr.substring(p, p + Gbs.GT416_Nbe.capacity())); p = p + Gbs.GT416_Nbe.capacity();
    Gbs.GT416_Itb.append(pcStr.substring(p, p + Gbs.GT416_Itb.capacity())); p = p + Gbs.GT416_Itb.capacity();
    Gbs.GT416_Itg.append(pcStr.substring(p, p + Gbs.GT416_Itg.capacity())); p = p + Gbs.GT416_Itg.capacity();
    Gbs.GT416_Dpc.append(pcStr.substring(p, p + Gbs.GT416_Dpc.capacity())); p = p + Gbs.GT416_Dpc.capacity();
    Gbs.GT416_Csv.append(pcStr.substring(p, p + Gbs.GT416_Csv.capacity())); p = p + Gbs.GT416_Csv.capacity();
    Gbs.GT416_Lcl.append(pcStr.substring(p, p + Gbs.GT416_Lcl.capacity())); p = p + Gbs.GT416_Lcl.capacity();
    Gbs.GT416_Rpo.append(pcStr.substring(p, p + Gbs.GT416_Rpo.capacity())); p = p + Gbs.GT416_Rpo.capacity();
    Gbs.GT416_Foj.append(pcStr.substring(p, p + Gbs.GT416_Foj.capacity())); p = p + Gbs.GT416_Foj.capacity();
    Gbs.GT416_Nin.append(pcStr.substring(p, p + Gbs.GT416_Nin.capacity())); p = p + Gbs.GT416_Nin.capacity();
    Gbs.GT416_Vsg.append(pcStr.substring(p, p + Gbs.GT416_Vsg.capacity())); p = p + Gbs.GT416_Vsg.capacity();
    Gbs.GT416_Csg.append(pcStr.substring(p, p + Gbs.GT416_Csg.capacity())); p = p + Gbs.GT416_Csg.capacity();
    Gbs.GT416_Spz.append(pcStr.substring(p, p + Gbs.GT416_Spz.capacity())); p = p + Gbs.GT416_Spz.capacity();
    Gbs.GT416_Sfv.append(pcStr.substring(p, p + Gbs.GT416_Sfv.capacity())); p = p + Gbs.GT416_Sfv.capacity();
    Gbs.GT416_Sum.append(pcStr.substring(p, p + Gbs.GT416_Sum.capacity())); p = p + Gbs.GT416_Sum.capacity();
    Gbs.GT416_Svl.append(pcStr.substring(p, p + Gbs.GT416_Svl.capacity())); p = p + Gbs.GT416_Svl.capacity();
    Gbs.GT416_Ben.append(pcStr.substring(p, p + Gbs.GT416_Ben.capacity())); p = p + Gbs.GT416_Ben.capacity();
    Gbs.GT416_Fvc.append(pcStr.substring(p, p + Gbs.GT416_Fvc.capacity())); p = p + Gbs.GT416_Fvc.capacity();
    l_MsgGT416.GT416_Gbs = Gbs;
    vec.add(l_MsgGT416);            
    return vec;
  }  
  //---------------------------------------------------------------------------------------
  static public String LSet_De_MsgGT416(Buf_MsgGT416 p_MsgGT416)
  {
    String pcStr = "";
    pcStr = pcStr + p_MsgGT416.GT416_Idr.toString();
    pcStr = pcStr + p_MsgGT416.GT416_Gti.toString();
    pcStr = pcStr + p_MsgGT416.GT416_Seq.toString();
    Grt_Gbs_Det Gbs = new Grt_Gbs_Det();          
    Gbs = p_MsgGT416.GT416_Gbs;
    pcStr = pcStr + Gbs.GT416_Gta.toString();
    pcStr = pcStr + Gbs.GT416_Nse.toString();
    pcStr = pcStr + Gbs.GT416_Can.toString();
    pcStr = pcStr + Gbs.GT416_Mnd.toString();
    pcStr = pcStr + Gbs.GT416_Vlc.toString();
    pcStr = pcStr + Gbs.GT416_Fut.toString();
    pcStr = pcStr + Gbs.GT416_Uvb.toString();
    pcStr = pcStr + Gbs.GT416_Nbe.toString();
    pcStr = pcStr + Gbs.GT416_Itb.toString();
    pcStr = pcStr + Gbs.GT416_Itg.toString();
    pcStr = pcStr + Gbs.GT416_Dpc.toString();
    pcStr = pcStr + Gbs.GT416_Csv.toString();
    pcStr = pcStr + Gbs.GT416_Lcl.toString();
    pcStr = pcStr + Gbs.GT416_Rpo.toString();
    pcStr = pcStr + Gbs.GT416_Foj.toString();
    pcStr = pcStr + Gbs.GT416_Nin.toString();
    pcStr = pcStr + Gbs.GT416_Vsg.toString();
    pcStr = pcStr + Gbs.GT416_Csg.toString();
    pcStr = pcStr + Gbs.GT416_Spz.toString();
    pcStr = pcStr + Gbs.GT416_Sfv.toString();
    pcStr = pcStr + Gbs.GT416_Sum.toString();
    pcStr = pcStr + Gbs.GT416_Svl.toString();
    pcStr = pcStr + Gbs.GT416_Ben.toString();
    pcStr = pcStr + Gbs.GT416_Fvc.toString();
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
}