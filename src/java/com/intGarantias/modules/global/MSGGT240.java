// Source File Name:   MSGGT240.java

package com.intGarantias.modules.global;

import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.util.Formateo;

import java.util.*;
import java.lang.*;
import org.apache.turbine.util.Log;

public class MSGGT240
{
  //---------------------------------------------------------------------------------------
  public static class Buf_MsgGT240
  {
    public StringBuffer GT240_Idr     = new StringBuffer(1  );  //X(01)        1 Idr Host                 
    public StringBuffer GT240_Img     = new StringBuffer(827);  //X(827)     828 Img ANULA
    public StringBuffer GT240_Swt_Dcn = new StringBuffer(1  );  //X(01)      829 Switches de Error
    public StringBuffer GT240_Swt_Cnr = new StringBuffer(1  );  //X(01)      830 Switches de Error
    public StringBuffer GT240_Swt_Gti = new StringBuffer(1  );  //X(01)      831 Switches de Error
    public StringBuffer GT240_Swt_Evt = new StringBuffer(1  );  //X(01)      832 Switches de Error
    public StringBuffer GT240_Swt_Cli = new StringBuffer(1  );  //X(01)      833 Switches de Error

    public String getIdr()             {return GT240_Idr.toString();}
    public String getGti()             {return GT240_Img.toString();}
    public String getSwt_Dcn()         {return GT240_Swt_Dcn.toString();}
    public String getSwt_Cnr()         {return GT240_Swt_Cnr.toString();}
    public String getSwt_Gti()         {return GT240_Swt_Gti.toString();}
    public String getSwt_Evt()         {return GT240_Swt_Evt.toString();}
    public String getSwt_Cli()         {return GT240_Swt_Cli.toString();}
  }
  //---------------------------------------------------------------------------------------
  static public Buf_MsgGT240 Inicia_MsgGT240()
  {
    Buf_MsgGT240 l_MsgGT240 = new Buf_MsgGT240();
    l_MsgGT240.GT240_Idr.replace    (0,l_MsgGT240.GT240_Idr.capacity(),     RUTGEN.Blancos(1  ));
    l_MsgGT240.GT240_Img.replace    (0,l_MsgGT240.GT240_Img.capacity(),     RUTGEN.Blancos(827));
    l_MsgGT240.GT240_Swt_Dcn.replace(0,l_MsgGT240.GT240_Swt_Dcn.capacity(), RUTGEN.Blancos(1  ));
    l_MsgGT240.GT240_Swt_Cnr.replace(0,l_MsgGT240.GT240_Swt_Cnr.capacity(), RUTGEN.Blancos(1  ));
    l_MsgGT240.GT240_Swt_Gti.replace(0,l_MsgGT240.GT240_Swt_Gti.capacity(), RUTGEN.Blancos(1  ));
    l_MsgGT240.GT240_Swt_Evt.replace(0,l_MsgGT240.GT240_Swt_Evt.capacity(), RUTGEN.Blancos(1  ));
    l_MsgGT240.GT240_Swt_Cli.replace(0,l_MsgGT240.GT240_Swt_Cli.capacity(), RUTGEN.Blancos(1  ));
    return l_MsgGT240;
  }
  //---------------------------------------------------------------------------------------
  static public Buf_MsgGT240 LSet_A_MsgGT240 (String pcStr)
  {
    Buf_MsgGT240 l_MsgGT240 = new Buf_MsgGT240();
    MSGGT240 vMsgGT240 = new MSGGT240();
    Vector vDatos = vMsgGT240.LSet_A_vMsgGT240(pcStr);
    l_MsgGT240 = (MSGGT240.Buf_MsgGT240)vDatos.elementAt(0);
    return l_MsgGT240;
  }
  //---------------------------------------------------------------------------------------
  public Vector LSet_A_vMsgGT240(String pcStr)
  {
    Vector vec = new Vector();
    int p = 0;
    Buf_MsgGT240 l_MsgGT240 = new Buf_MsgGT240();
    l_MsgGT240.GT240_Idr.append     (pcStr.substring(p , p + l_MsgGT240.GT240_Idr.capacity()));     p = p + l_MsgGT240.GT240_Idr.capacity();
    l_MsgGT240.GT240_Img.append     (pcStr.substring(p , p + l_MsgGT240.GT240_Img.capacity()));     p = p + l_MsgGT240.GT240_Img.capacity();
    l_MsgGT240.GT240_Swt_Dcn.append (pcStr.substring(p , p + l_MsgGT240.GT240_Swt_Dcn.capacity())); p = p + l_MsgGT240.GT240_Swt_Dcn.capacity();
    l_MsgGT240.GT240_Swt_Cnr.append (pcStr.substring(p , p + l_MsgGT240.GT240_Swt_Cnr.capacity())); p = p + l_MsgGT240.GT240_Swt_Cnr.capacity();
    l_MsgGT240.GT240_Swt_Gti.append (pcStr.substring(p , p + l_MsgGT240.GT240_Swt_Gti.capacity())); p = p + l_MsgGT240.GT240_Swt_Gti.capacity();
    l_MsgGT240.GT240_Swt_Evt.append (pcStr.substring(p , p + l_MsgGT240.GT240_Swt_Evt.capacity())); p = p + l_MsgGT240.GT240_Swt_Evt.capacity();
    l_MsgGT240.GT240_Swt_Cli.append (pcStr.substring(p , p + l_MsgGT240.GT240_Swt_Cli.capacity())); p = p + l_MsgGT240.GT240_Swt_Cli.capacity();
    vec.add (l_MsgGT240);
    return vec;
  }
  //---------------------------------------------------------------------------------------
  static public String LSet_De_MsgGT240 (Buf_MsgGT240 p_MsgGT240)
  {
    String pcStr = "";
    pcStr = pcStr + p_MsgGT240.GT240_Idr.toString();
    pcStr = pcStr + p_MsgGT240.GT240_Img.toString();
    pcStr = pcStr + p_MsgGT240.GT240_Swt_Dcn.toString();
    pcStr = pcStr + p_MsgGT240.GT240_Swt_Cnr.toString();
    pcStr = pcStr + p_MsgGT240.GT240_Swt_Gti.toString();
    pcStr = pcStr + p_MsgGT240.GT240_Swt_Evt.toString();
    pcStr = pcStr + p_MsgGT240.GT240_Swt_Cli.toString();
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
}