// Source File Name:   MSGGT455.java

package com.intGarantias.modules.global;

import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.util.Formateo;
import com.FHTServlet.modules.global.BF_LOB;

import java.util.*;
import java.lang.*;
import org.apache.turbine.util.Log;

public class MSGGT455
{
  public static int g_Max_GT455 = 148;
  //---------------------------------------------------------------------------------------
  public static class Bff_MsgGT455
  {
    public StringBuffer GT455_Iln = new StringBuffer(7 );   //9(07)        7 Id. InfLegal
    public StringBuffer GT455_Ilf = new StringBuffer(8 );   //9(08)       15 Fecha InfLegal
    public StringBuffer GT455_Dsl = new StringBuffer(9 );   //9(09)       24 @Observaciones

    public String getIln()         {return GT455_Iln.toString();}
    public String getIlf()         {return GT455_Ilf.toString();}
    public String getDsl()         {return GT455_Dsl.toString();}

    public Formateo f = new Formateo();
    public BF_LOB lob = new BF_LOB();
    public int    igetDsl()        {if (getDsl().trim().equals("")) return 0; else return Integer.parseInt(getDsl());}
    public String fgetIlf()        {return f.FormatDate(GT455_Ilf.toString());}
    public String fgetDsl()  throws Exception        {if (igetDsl()==0) return ""; else return lob.leeCLOB(getDsl());}
  }
  //---------------------------------------------------------------------------------------
  public static class Buf_MsgGT455
  {
    public StringBuffer GT455_Idr = new StringBuffer(1);
    public StringBuffer GT455_Sis = new StringBuffer(3);
    public StringBuffer GT455_Ncn = new StringBuffer(7);
    public StringBuffer GT455_Nro = new StringBuffer(3);
    public Vector       GT455_Tap = new Vector();

    public String getIdr()         {return GT455_Idr.toString();}
    public String getSis()         {return GT455_Sis.toString();}
    public String getNcn()         {return GT455_Ncn.toString();}
    public String getNro()         {return GT455_Nro.toString();}
    public Vector getTab()         {return GT455_Tap;}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_MsgGT455 Inicia_MsgGT455()
  {
    Buf_MsgGT455 l_MsgGT455 = new Buf_MsgGT455();
    l_MsgGT455.GT455_Idr.replace(0,l_MsgGT455.GT455_Idr.capacity(), RUTGEN.Blancos(1));
    l_MsgGT455.GT455_Sis.replace(0,l_MsgGT455.GT455_Sis.capacity(), RUTGEN.Blancos(3));
    l_MsgGT455.GT455_Ncn.replace(0,l_MsgGT455.GT455_Ncn.capacity(), RUTGEN.Blancos(7));
    l_MsgGT455.GT455_Nro.replace(0,l_MsgGT455.GT455_Nro.capacity(), RUTGEN.Blancos(3));
    int p=0;
    for (int i=0; i<g_Max_GT455; i++)
       {
         Bff_MsgGT455 Tap = new Bff_MsgGT455();
         Tap.GT455_Iln.replace(0,Tap.GT455_Iln.capacity(), RUTGEN.Blancos(7 ));
         Tap.GT455_Ilf.replace(0,Tap.GT455_Ilf.capacity(), RUTGEN.Blancos(8 ));
         Tap.GT455_Dsl.replace(0,Tap.GT455_Dsl.capacity(), RUTGEN.Blancos(9 ));
         l_MsgGT455.GT455_Tap.add(Tap);
       }
    return l_MsgGT455;
  }
  //---------------------------------------------------------------------------------------
  static public Buf_MsgGT455 LSet_A_MsgGT455(String pcStr)
  {
    Buf_MsgGT455 l_MsgGT455 = new Buf_MsgGT455();
    MSGGT455 vMsgGT455 = new MSGGT455();
    Vector vDatos = vMsgGT455.LSet_A_vMsgGT455(pcStr);
    l_MsgGT455 = (MSGGT455.Buf_MsgGT455)vDatos.elementAt(0);
    return l_MsgGT455;
  }
  //---------------------------------------------------------------------------------------
  public Vector LSet_A_vMsgGT455(String pcStr)
  {
    Vector vec = new Vector();
    int p = 0;
    Buf_MsgGT455 l_MsgGT455 = new Buf_MsgGT455();
    l_MsgGT455.GT455_Idr.append(pcStr.substring(p, p + l_MsgGT455.GT455_Idr.capacity())); p = p + l_MsgGT455.GT455_Idr.capacity();
    l_MsgGT455.GT455_Sis.append(pcStr.substring(p, p + l_MsgGT455.GT455_Sis.capacity())); p = p + l_MsgGT455.GT455_Sis.capacity();
    l_MsgGT455.GT455_Ncn.append(pcStr.substring(p, p + l_MsgGT455.GT455_Ncn.capacity())); p = p + l_MsgGT455.GT455_Ncn.capacity();
    l_MsgGT455.GT455_Nro.append(pcStr.substring(p, p + l_MsgGT455.GT455_Nro.capacity())); p = p + l_MsgGT455.GT455_Nro.capacity();
    for (int i=0; i<g_Max_GT455; i++)
       {
         Bff_MsgGT455 Tap = new Bff_MsgGT455();
         Tap.GT455_Iln.append(pcStr.substring(p, p + Tap.GT455_Iln.capacity())); p = p + Tap.GT455_Iln.capacity();
         Tap.GT455_Ilf.append(pcStr.substring(p, p + Tap.GT455_Ilf.capacity())); p = p + Tap.GT455_Ilf.capacity();
         Tap.GT455_Dsl.append(pcStr.substring(p, p + Tap.GT455_Dsl.capacity())); p = p + Tap.GT455_Dsl.capacity();
         if (Tap.GT455_Iln.toString().trim().equals("")) { break; }
         l_MsgGT455.GT455_Tap.add(Tap);
       }
    vec.add (l_MsgGT455);
    return vec;
  }
  //---------------------------------------------------------------------------------------
  static public String LSet_De_MsgGT455(Buf_MsgGT455 p_MsgGT455)
  {
    String pcStr = "";
    pcStr = pcStr + p_MsgGT455.GT455_Idr.toString();
    pcStr = pcStr + p_MsgGT455.GT455_Sis.toString();
    pcStr = pcStr + p_MsgGT455.GT455_Ncn.toString();
    pcStr = pcStr + p_MsgGT455.GT455_Nro.toString();
    for (int i=0; i<p_MsgGT455.GT455_Tap.size(); i++)
       {
         pcStr = pcStr + ((Bff_MsgGT455)p_MsgGT455.GT455_Tap.elementAt(i)).GT455_Iln.toString();
         pcStr = pcStr + ((Bff_MsgGT455)p_MsgGT455.GT455_Tap.elementAt(i)).GT455_Ilf.toString();
         pcStr = pcStr + ((Bff_MsgGT455)p_MsgGT455.GT455_Tap.elementAt(i)).GT455_Dsl.toString();
       }
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
}