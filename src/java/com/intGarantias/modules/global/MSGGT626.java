// Source File Name:   MSGGT626.java

package com.intGarantias.modules.global;

import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.util.Formateo;

import java.util.*;
import java.lang.*;
import org.apache.turbine.util.Log;

public class MSGGT626
{
  public static int g_Max_GT626 = 15;
  //---------------------------------------------------------------------------------------
  public static class Bff_MsgGT626
  {
    public StringBuffer GtRvd_Seq = new StringBuffer(3 );       //9(03)        3 Cuota           
    public StringBuffer GtRvd_Itb = new StringBuffer(2 );       //X(02)        5 Tipo Bien       
    public StringBuffer GtRvd_Ide = new StringBuffer(36);       //X(36)       41 Id. Bien        
    public StringBuffer GtRvd_Dsb = new StringBuffer(30);       //X(30)       71 Desc. Bien      
    public StringBuffer GtRvd_Umd = new StringBuffer(2 );       //X(02)       73 UMedida   
    public StringBuffer GtRvd_Pum = new StringBuffer(15);       //9(11)V9(4)  88 PUnitario 
    public StringBuffer GtRvd_Udd = new StringBuffer(9 );       //9(07)V9(2)  97 Cantidad     
    public StringBuffer GtRvd_Vtt = new StringBuffer(15);       //9(11)V9(4) 112 VNominal/Prenda 
    public StringBuffer GtRvd_Frv = new StringBuffer(8 );       //9(08)      120 FRevAnterior 
    public StringBuffer GtRvd_Vur = new StringBuffer(9 );       //9(07)V9(2) 129 VuoAnterior    
    public StringBuffer GtRvd_Vts = new StringBuffer(15);       //9(13)V9(2) 144 VTasacion/Banco  
    public StringBuffer GtRvd_Fvt = new StringBuffer(8 );       //9(08)      152 FVctoAnterior 
    public StringBuffer GtRvd_Fre = new StringBuffer(8 );       //9(08)      160 FRevNueva 
    public StringBuffer GtRvd_Vue = new StringBuffer(9 );       //9(07)V9(2) 169 VuoNueva 
    public StringBuffer GtRvd_Nvt = new StringBuffer(8 );       //9(11)V9(4) 177 FVctoNueva 
    public StringBuffer GtRvd_Pct = new StringBuffer(5 );       //9(03)V9(2) 182 %SBIF

    public String getSeq()       {return GtRvd_Seq.toString();}
    public String getItb()       {return GtRvd_Itb.toString();}
    public String getIde()       {return GtRvd_Ide.toString();}
    public String getDsb()       {return GtRvd_Dsb.toString();}
    public String getUmd()       {return GtRvd_Umd.toString();}
    public String getPum()       {return GtRvd_Pum.toString();}
    public String getUdd()       {return GtRvd_Udd.toString();}
    public String getVtt()       {return GtRvd_Vtt.toString();}
    public String getFrv()       {return GtRvd_Frv.toString();}
    public String getVur()       {return GtRvd_Vur.toString();}
    public String getVts()       {return GtRvd_Vts.toString();}
    public String getFvt()       {return GtRvd_Fvt.toString();}
    public String getFre()       {return GtRvd_Fre.toString();}
    public String getVue()       {return GtRvd_Vue.toString();}
    public String getNvt()       {return GtRvd_Nvt.toString();}
    public String getPct()       {return GtRvd_Pct.toString();}

    public Formateo f = new Formateo();
    public String fgetPum()      {return f.Formato(GtRvd_Pum.toString(),"99.999.999.999",4,4,',');}
    public String fgetUdd()      {return f.Formato(GtRvd_Udd.toString(),"99.999.999.999",2,2,',');}
    public String fgetVtt()      {return f.Formato(GtRvd_Vtt.toString(),"99.999.999.999",4,4,',');}
    public String fgetFrv()      {return f.FormatDate(GtRvd_Frv.toString());}
    public String fgetVur()      {return f.Formato(GtRvd_Vur.toString(),"99.999.999.999",2,2,',');}
    public String fgetVts()      {return f.Formato(GtRvd_Vts.toString(),"99.999.999.999",2,2,',');}
    public String fgetFvt()      {return f.FormatDate(GtRvd_Fvt.toString());}
    public String fgetFre()      {return f.FormatDate(GtRvd_Fre.toString());}
    public String fgetVue()      {return f.Formato(GtRvd_Vue.toString(),"99.999.999.999",2,2,',');}
    public String fgetNvt()      {return f.FormatDate(GtRvd_Nvt.toString());}
  }
  //---------------------------------------------------------------------------------------
  public static class Buf_MsgGT626
  {
    public StringBuffer GT626_Idr     = new StringBuffer(1  );
    public StringBuffer GT626_Img     = new StringBuffer(827);
    public StringBuffer GT626_Swt_Dcn = new StringBuffer(1  );
    public StringBuffer GT626_Swt_Gti = new StringBuffer(1  );
    public StringBuffer GT626_Swt_Cnr = new StringBuffer(1  );
    public StringBuffer GT626_Swt_Evt = new StringBuffer(1  );
    public StringBuffer GT626_Swt_Cli = new StringBuffer(1  );
    public StringBuffer GT626_Mxt     = new StringBuffer(2  );
    public Vector       GT626_Rvl     = new Vector();

    public String getIdr()           {return GT626_Idr.toString();}
    public String getImg()           {return GT626_Img.toString();}
    public String getSwt_Dcn()       {return GT626_Swt_Dcn.toString();}
    public String getSwt_Gti()       {return GT626_Swt_Gti.toString();}
    public String getSwt_Cnr()       {return GT626_Swt_Cnr.toString();}
    public String getSwt_Evt()       {return GT626_Swt_Evt.toString();}
    public String getSwt_Cli()       {return GT626_Swt_Cli.toString();}
    public String getMxt()           {return GT626_Mxt.toString();}
    public Vector getRvl()           {return GT626_Rvl;}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_MsgGT626 Inicia_MsgGT626()
  {
    Buf_MsgGT626 l_MsgGT626 = new Buf_MsgGT626();
    l_MsgGT626.GT626_Idr.replace    (0,l_MsgGT626.GT626_Idr.capacity(),     RUTGEN.Blancos(1  ));
    l_MsgGT626.GT626_Img.replace    (0,l_MsgGT626.GT626_Img.capacity(),     RUTGEN.Blancos(827));
    l_MsgGT626.GT626_Swt_Dcn.replace(0,l_MsgGT626.GT626_Swt_Dcn.capacity(), RUTGEN.Blancos(1  ));
    l_MsgGT626.GT626_Swt_Gti.replace(0,l_MsgGT626.GT626_Swt_Gti.capacity(), RUTGEN.Blancos(1  ));
    l_MsgGT626.GT626_Swt_Cnr.replace(0,l_MsgGT626.GT626_Swt_Cnr.capacity(), RUTGEN.Blancos(1  ));
    l_MsgGT626.GT626_Swt_Evt.replace(0,l_MsgGT626.GT626_Swt_Evt.capacity(), RUTGEN.Blancos(1  ));
    l_MsgGT626.GT626_Swt_Cli.replace(0,l_MsgGT626.GT626_Swt_Cli.capacity(), RUTGEN.Blancos(1  ));
    l_MsgGT626.GT626_Mxt.replace    (0,l_MsgGT626.GT626_Mxt.capacity(),     RUTGEN.Blancos(2  ));
    int p = 0;
    for (int i=0; i<g_Max_GT626; i++)
        {
          Bff_MsgGT626 Tap = new Bff_MsgGT626();
          Tap.GtRvd_Seq.replace(0, Tap.GtRvd_Seq.capacity(), RUTGEN.Blancos(3 ));
          Tap.GtRvd_Itb.replace(0, Tap.GtRvd_Itb.capacity(), RUTGEN.Blancos(2 ));
          Tap.GtRvd_Ide.replace(0, Tap.GtRvd_Ide.capacity(), RUTGEN.Blancos(36));
          Tap.GtRvd_Dsb.replace(0, Tap.GtRvd_Dsb.capacity(), RUTGEN.Blancos(30));
          Tap.GtRvd_Umd.replace(0, Tap.GtRvd_Umd.capacity(), RUTGEN.Blancos(2 ));
          Tap.GtRvd_Pum.replace(0, Tap.GtRvd_Pum.capacity(), RUTGEN.Blancos(15));
          Tap.GtRvd_Udd.replace(0, Tap.GtRvd_Udd.capacity(), RUTGEN.Blancos(9 ));
          Tap.GtRvd_Vtt.replace(0, Tap.GtRvd_Vtt.capacity(), RUTGEN.Blancos(15));
          Tap.GtRvd_Frv.replace(0, Tap.GtRvd_Frv.capacity(), RUTGEN.Blancos(8 ));
          Tap.GtRvd_Vur.replace(0, Tap.GtRvd_Vur.capacity(), RUTGEN.Blancos(9 ));
          Tap.GtRvd_Vts.replace(0, Tap.GtRvd_Vts.capacity(), RUTGEN.Blancos(15));
          Tap.GtRvd_Fvt.replace(0, Tap.GtRvd_Fvt.capacity(), RUTGEN.Blancos(8 ));
          Tap.GtRvd_Fre.replace(0, Tap.GtRvd_Fre.capacity(), RUTGEN.Blancos(8 ));
          Tap.GtRvd_Vue.replace(0, Tap.GtRvd_Vue.capacity(), RUTGEN.Blancos(9 ));
          Tap.GtRvd_Nvt.replace(0, Tap.GtRvd_Nvt.capacity(), RUTGEN.Blancos(8 ));
          Tap.GtRvd_Pct.replace(0, Tap.GtRvd_Pct.capacity(), RUTGEN.Blancos(5 ));
          l_MsgGT626.GT626_Rvl.add(Tap);        
        }
    return l_MsgGT626;
  }
  //---------------------------------------------------------------------------------------
  static public Buf_MsgGT626 LSet_A_MsgGT626(String pcStr)
  {
    Buf_MsgGT626 l_MsgGT626 = new Buf_MsgGT626();
    MSGGT626 vMsgGT626 = new MSGGT626();
    Vector vDatos = vMsgGT626.LSet_A_vMsgGT626(pcStr);
    l_MsgGT626 = (MSGGT626.Buf_MsgGT626)vDatos.elementAt(0);
    return l_MsgGT626;
  }
  //---------------------------------------------------------------------------------------
  public Vector LSet_A_vMsgGT626(String pcStr)
  {
    Vector vec = new Vector();
    int p = 0;
    Buf_MsgGT626 l_MsgGT626 = new Buf_MsgGT626();                            
    l_MsgGT626.GT626_Idr.append     (pcStr.substring(p, p + l_MsgGT626.GT626_Idr.capacity()));     p = p + l_MsgGT626.GT626_Idr.capacity();
    l_MsgGT626.GT626_Img.append     (pcStr.substring(p, p + l_MsgGT626.GT626_Img.capacity()));     p = p + l_MsgGT626.GT626_Img.capacity();
    l_MsgGT626.GT626_Swt_Dcn.append (pcStr.substring(p, p + l_MsgGT626.GT626_Swt_Dcn.capacity())); p = p + l_MsgGT626.GT626_Swt_Dcn.capacity();
    l_MsgGT626.GT626_Swt_Gti.append (pcStr.substring(p, p + l_MsgGT626.GT626_Swt_Gti.capacity())); p = p + l_MsgGT626.GT626_Swt_Gti.capacity();
    l_MsgGT626.GT626_Swt_Cnr.append (pcStr.substring(p, p + l_MsgGT626.GT626_Swt_Cnr.capacity())); p = p + l_MsgGT626.GT626_Swt_Cnr.capacity();
    l_MsgGT626.GT626_Swt_Evt.append (pcStr.substring(p, p + l_MsgGT626.GT626_Swt_Evt.capacity())); p = p + l_MsgGT626.GT626_Swt_Evt.capacity();
    l_MsgGT626.GT626_Swt_Cli.append (pcStr.substring(p, p + l_MsgGT626.GT626_Swt_Cli.capacity())); p = p + l_MsgGT626.GT626_Swt_Cli.capacity();
    l_MsgGT626.GT626_Mxt.append     (pcStr.substring(p, p + l_MsgGT626.GT626_Mxt.capacity()));     p = p + l_MsgGT626.GT626_Mxt.capacity();
//    for (int i=0; i<Integer.parseInt(l_MsgGT626.GT626_Mxt.toString()); i++)
    for (int i=0; i<g_Max_GT626; i++)
        {
          Bff_MsgGT626 Tap = new Bff_MsgGT626();
          Tap.GtRvd_Seq.append(pcStr.substring(p, p + Tap.GtRvd_Seq.capacity())); p = p + Tap.GtRvd_Seq.capacity();
          Tap.GtRvd_Itb.append(pcStr.substring(p, p + Tap.GtRvd_Itb.capacity())); p = p + Tap.GtRvd_Itb.capacity();
          Tap.GtRvd_Ide.append(pcStr.substring(p, p + Tap.GtRvd_Ide.capacity())); p = p + Tap.GtRvd_Ide.capacity();
          Tap.GtRvd_Dsb.append(pcStr.substring(p, p + Tap.GtRvd_Dsb.capacity())); p = p + Tap.GtRvd_Dsb.capacity();
          Tap.GtRvd_Umd.append(pcStr.substring(p, p + Tap.GtRvd_Umd.capacity())); p = p + Tap.GtRvd_Umd.capacity();
          Tap.GtRvd_Pum.append(pcStr.substring(p, p + Tap.GtRvd_Pum.capacity())); p = p + Tap.GtRvd_Pum.capacity();
          Tap.GtRvd_Udd.append(pcStr.substring(p, p + Tap.GtRvd_Udd.capacity())); p = p + Tap.GtRvd_Udd.capacity();
          Tap.GtRvd_Vtt.append(pcStr.substring(p, p + Tap.GtRvd_Vtt.capacity())); p = p + Tap.GtRvd_Vtt.capacity();
          Tap.GtRvd_Frv.append(pcStr.substring(p, p + Tap.GtRvd_Frv.capacity())); p = p + Tap.GtRvd_Frv.capacity();
          Tap.GtRvd_Vur.append(pcStr.substring(p, p + Tap.GtRvd_Vur.capacity())); p = p + Tap.GtRvd_Vur.capacity();
          Tap.GtRvd_Vts.append(pcStr.substring(p, p + Tap.GtRvd_Vts.capacity())); p = p + Tap.GtRvd_Vts.capacity();
          Tap.GtRvd_Fvt.append(pcStr.substring(p, p + Tap.GtRvd_Fvt.capacity())); p = p + Tap.GtRvd_Fvt.capacity();
          Tap.GtRvd_Fre.append(pcStr.substring(p, p + Tap.GtRvd_Fre.capacity())); p = p + Tap.GtRvd_Fre.capacity();
          Tap.GtRvd_Vue.append(pcStr.substring(p, p + Tap.GtRvd_Vue.capacity())); p = p + Tap.GtRvd_Vue.capacity();
          Tap.GtRvd_Nvt.append(pcStr.substring(p, p + Tap.GtRvd_Nvt.capacity())); p = p + Tap.GtRvd_Nvt.capacity();
          Tap.GtRvd_Pct.append(pcStr.substring(p, p + Tap.GtRvd_Pct.capacity())); p = p + Tap.GtRvd_Pct.capacity();
          l_MsgGT626.GT626_Rvl.add(Tap);
        }
    vec.add (l_MsgGT626);
    return vec;
  }
  //---------------------------------------------------------------------------------------
  static public String LSet_De_MsgGT626(Buf_MsgGT626 p_MsgGT626)
  {
    String pcStr = "";
    pcStr = pcStr + p_MsgGT626.GT626_Idr.toString();
    pcStr = pcStr + p_MsgGT626.GT626_Img.toString();
    pcStr = pcStr + p_MsgGT626.GT626_Swt_Dcn.toString();
    pcStr = pcStr + p_MsgGT626.GT626_Swt_Gti.toString();
    pcStr = pcStr + p_MsgGT626.GT626_Swt_Cnr.toString();
    pcStr = pcStr + p_MsgGT626.GT626_Swt_Evt.toString();
    pcStr = pcStr + p_MsgGT626.GT626_Swt_Cli.toString();
    pcStr = pcStr + p_MsgGT626.GT626_Mxt.toString();
    for (int i=0; i<p_MsgGT626.GT626_Rvl.size(); i++)
        {
          pcStr = pcStr + ((Bff_MsgGT626)p_MsgGT626.GT626_Rvl.elementAt(i)).GtRvd_Seq.toString();
          pcStr = pcStr + ((Bff_MsgGT626)p_MsgGT626.GT626_Rvl.elementAt(i)).GtRvd_Itb.toString();
          pcStr = pcStr + ((Bff_MsgGT626)p_MsgGT626.GT626_Rvl.elementAt(i)).GtRvd_Ide.toString();
          pcStr = pcStr + ((Bff_MsgGT626)p_MsgGT626.GT626_Rvl.elementAt(i)).GtRvd_Dsb.toString();
          pcStr = pcStr + ((Bff_MsgGT626)p_MsgGT626.GT626_Rvl.elementAt(i)).GtRvd_Umd.toString();
          pcStr = pcStr + ((Bff_MsgGT626)p_MsgGT626.GT626_Rvl.elementAt(i)).GtRvd_Pum.toString();
          pcStr = pcStr + ((Bff_MsgGT626)p_MsgGT626.GT626_Rvl.elementAt(i)).GtRvd_Udd.toString();
          pcStr = pcStr + ((Bff_MsgGT626)p_MsgGT626.GT626_Rvl.elementAt(i)).GtRvd_Vtt.toString();
          pcStr = pcStr + ((Bff_MsgGT626)p_MsgGT626.GT626_Rvl.elementAt(i)).GtRvd_Frv.toString();
          pcStr = pcStr + ((Bff_MsgGT626)p_MsgGT626.GT626_Rvl.elementAt(i)).GtRvd_Vur.toString();
          pcStr = pcStr + ((Bff_MsgGT626)p_MsgGT626.GT626_Rvl.elementAt(i)).GtRvd_Vts.toString();
          pcStr = pcStr + ((Bff_MsgGT626)p_MsgGT626.GT626_Rvl.elementAt(i)).GtRvd_Fvt.toString();
          pcStr = pcStr + ((Bff_MsgGT626)p_MsgGT626.GT626_Rvl.elementAt(i)).GtRvd_Fre.toString();
          pcStr = pcStr + ((Bff_MsgGT626)p_MsgGT626.GT626_Rvl.elementAt(i)).GtRvd_Vue.toString();
          pcStr = pcStr + ((Bff_MsgGT626)p_MsgGT626.GT626_Rvl.elementAt(i)).GtRvd_Nvt.toString();
          pcStr = pcStr + ((Bff_MsgGT626)p_MsgGT626.GT626_Rvl.elementAt(i)).GtRvd_Pct.toString();
        }
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
}