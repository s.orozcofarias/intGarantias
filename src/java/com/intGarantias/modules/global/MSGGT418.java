// Source File Name:   MSGGT418.java

package com.intGarantias.modules.global;

import java.util.Vector;

import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.util.Formateo;

public class MSGGT418 {
	public static class Buf_MsgGT418 {

		public String getIdr() {
			return GT418_Idr.toString();
		}

		public String getGti() {
			return GT418_Gti.toString();
		}

		public String getSeq() {
			return GT418_Seq.toString();
		}

		public Grt_Gbs_Det getGbs() {
			return GT418_Gbs;
		}

		public StringBuffer GT418_Idr;
		public StringBuffer GT418_Gti;
		public StringBuffer GT418_Seq;
		public Grt_Gbs_Det GT418_Gbs;

		public Buf_MsgGT418() {
			GT418_Idr = new StringBuffer(1);
			GT418_Gti = new StringBuffer(10);
			GT418_Seq = new StringBuffer(3);
			GT418_Gbs = new Grt_Gbs_Det();
		}
	}

	public static class Grt_Gbs_Det {

		public String getItb() {
			return GT418_Itb.toString();
		}

		public String getDcn() {
			return GT418_Dcn.toString();
		}

		public String getDpc() {
			return GT418_Dpc.toString();
		}

		public String getDsb() {
			return GT418_Dsb.toString();
		}

		public String getFem() {
			return GT418_Fem.toString();
		}

		public String getPct() {
			return GT418_Pct.toString();
		}

		public String getFvt() {
			return GT418_Fvt.toString();
		}

		public String getUmt() {
			return GT418_Umt.toString();
		}

		public String getUdc() {
			return GT418_Udc.toString();
		}

		public String getVum() {
			return GT418_Vum.toString();
		}

		public String getVln() {
			return GT418_Vln.toString();
		}

		public String getVlc() {
			return GT418_Vlc.toString();
		}

		public String getObb() {
			return GT418_Obb.toString();
		}

		public String fgetFem() {
			return f.FormatDate(getFem());
		}

		public String fgetFvt() {
			return f.FormatDate(getFvt());
		}

		public String fgetUmt() {
			String Umt = "";
			if (getUmt().trim().equals("P"))
				Umt = "$";
			if (getUmt().trim().equals("U"))
				Umt = "UF";
			if (getUmt().trim().equals("D"))
				Umt = "US$";
			return Umt;
		}

		public int igetUdc() {
			int Udc = 0;
			if (getUmt().trim().equals("P"))
				Udc = 0;
			if (getUmt().trim().equals("U"))
				Udc = 4;
			if (getUmt().trim().equals("D"))
				Udc = 2;
			return Udc;
		}

		public String fgetPct() {
			return f.Formato(getPct(), "999", 2, 2, ',');
		}

		public String fgetVum() {
			return f.Formato(getVum(), "9.999.999", 2, 2, ',');
		}

		public String fgetVln(int Dcu) {
			return f.Formato(getVln(), "99.999.999.999", 4, Dcu, ',');
		}

		public String fgetVlc(int Dcm) {
			return f.Formato(getVlc(), "99.999.999.999", 2, Dcm, ',');
		}

		public StringBuffer GT418_Itb;
		public StringBuffer GT418_Dcn;
		public StringBuffer GT418_Dpc;
		public StringBuffer GT418_Dsb;
		public StringBuffer GT418_Fem;
		public StringBuffer GT418_Pct;
		public StringBuffer GT418_Fvt;
		public StringBuffer GT418_Umt;
		public StringBuffer GT418_Udc;
		public StringBuffer GT418_Vum;
		public StringBuffer GT418_Vln;
		public StringBuffer GT418_Vlc;
		public StringBuffer GT418_Obb;
		public Formateo f;

		public Grt_Gbs_Det() {
			GT418_Itb = new StringBuffer(2);
			GT418_Dcn = new StringBuffer(5);
			GT418_Dpc = new StringBuffer(10);
			GT418_Dsb = new StringBuffer(100);
			GT418_Fem = new StringBuffer(8);
			GT418_Pct = new StringBuffer(5);
			GT418_Fvt = new StringBuffer(8);
			GT418_Umt = new StringBuffer(3);
			GT418_Udc = new StringBuffer(1);
			GT418_Vum = new StringBuffer(9);
			GT418_Vln = new StringBuffer(15);
			GT418_Vlc = new StringBuffer(15);
			GT418_Obb = new StringBuffer(9);
			f = new Formateo();
		}
	}

	public MSGGT418() {
	}

	public static Buf_MsgGT418 Inicia_MsgGT418() {
		Buf_MsgGT418 l_MsgGT418 = new Buf_MsgGT418();
		l_MsgGT418.GT418_Idr.replace(0, l_MsgGT418.GT418_Idr.capacity(),
				RUTGEN.Blancos(1));
		l_MsgGT418.GT418_Gti.replace(0, l_MsgGT418.GT418_Gti.capacity(),
				RUTGEN.Blancos(10));
		l_MsgGT418.GT418_Seq.replace(0, l_MsgGT418.GT418_Seq.capacity(),
				RUTGEN.Blancos(3));
		Grt_Gbs_Det Gbs = new Grt_Gbs_Det();
		Gbs.GT418_Itb.replace(0, Gbs.GT418_Itb.capacity(), RUTGEN.Blancos(2));
		Gbs.GT418_Dcn.replace(0, Gbs.GT418_Dcn.capacity(), RUTGEN.Blancos(5));
		Gbs.GT418_Dpc.replace(0, Gbs.GT418_Dpc.capacity(), RUTGEN.Blancos(10));
		Gbs.GT418_Dsb.replace(0, Gbs.GT418_Dsb.capacity(), RUTGEN.Blancos(100));
		Gbs.GT418_Fem.replace(0, Gbs.GT418_Fem.capacity(), RUTGEN.Blancos(8));
		Gbs.GT418_Pct.replace(0, Gbs.GT418_Pct.capacity(), RUTGEN.Blancos(5));
		Gbs.GT418_Fvt.replace(0, Gbs.GT418_Fvt.capacity(), RUTGEN.Blancos(8));
		Gbs.GT418_Umt.replace(0, Gbs.GT418_Umt.capacity(), RUTGEN.Blancos(3));
		Gbs.GT418_Udc.replace(0, Gbs.GT418_Udc.capacity(), RUTGEN.Blancos(1));
		Gbs.GT418_Vum.replace(0, Gbs.GT418_Vum.capacity(), RUTGEN.Blancos(9));
		Gbs.GT418_Vln.replace(0, Gbs.GT418_Vln.capacity(), RUTGEN.Blancos(15));
		Gbs.GT418_Vlc.replace(0, Gbs.GT418_Vlc.capacity(), RUTGEN.Blancos(15));
		Gbs.GT418_Obb.replace(0, Gbs.GT418_Obb.capacity(), RUTGEN.Blancos(9));
		l_MsgGT418.GT418_Gbs = Gbs;
		return l_MsgGT418;
	}

	public static Buf_MsgGT418 LSet_A_MsgGT418(String pcStr) {
		Buf_MsgGT418 l_MsgGT418 = new Buf_MsgGT418();
		MSGGT418 vMsgGT418 = new MSGGT418();
		Vector vDatos = vMsgGT418.LSet_A_vMsgGT418(pcStr);
		l_MsgGT418 = (Buf_MsgGT418) vDatos.elementAt(0);
		return l_MsgGT418;
	}

	public Vector LSet_A_vMsgGT418(String pcStr) {
		int p = 0;
		Vector vec = new Vector();
		Buf_MsgGT418 l_MsgGT418 = new Buf_MsgGT418();
		l_MsgGT418.GT418_Idr.append(pcStr.substring(p,
				p + l_MsgGT418.GT418_Idr.capacity()));
		p += l_MsgGT418.GT418_Idr.capacity();
		l_MsgGT418.GT418_Gti.append(pcStr.substring(p,
				p + l_MsgGT418.GT418_Gti.capacity()));
		p += l_MsgGT418.GT418_Gti.capacity();
		l_MsgGT418.GT418_Seq.append(pcStr.substring(p,
				p + l_MsgGT418.GT418_Seq.capacity()));
		p += l_MsgGT418.GT418_Seq.capacity();
		Grt_Gbs_Det Gbs = new Grt_Gbs_Det();
		Gbs.GT418_Itb.append(pcStr.substring(p, p + Gbs.GT418_Itb.capacity()));
		p += Gbs.GT418_Itb.capacity();
		Gbs.GT418_Dcn.append(pcStr.substring(p, p + Gbs.GT418_Dcn.capacity()));
		p += Gbs.GT418_Dcn.capacity();
		Gbs.GT418_Dpc.append(pcStr.substring(p, p + Gbs.GT418_Dpc.capacity()));
		p += Gbs.GT418_Dpc.capacity();
		Gbs.GT418_Dsb.append(pcStr.substring(p, p + Gbs.GT418_Dsb.capacity()));
		p += Gbs.GT418_Dsb.capacity();
		Gbs.GT418_Fem.append(pcStr.substring(p, p + Gbs.GT418_Fem.capacity()));
		p += Gbs.GT418_Fem.capacity();
		Gbs.GT418_Pct.append(pcStr.substring(p, p + Gbs.GT418_Pct.capacity()));
		p += Gbs.GT418_Pct.capacity();
		Gbs.GT418_Fvt.append(pcStr.substring(p, p + Gbs.GT418_Fvt.capacity()));
		p += Gbs.GT418_Fvt.capacity();
		Gbs.GT418_Umt.append(pcStr.substring(p, p + Gbs.GT418_Umt.capacity()));
		p += Gbs.GT418_Umt.capacity();
		Gbs.GT418_Udc.append(pcStr.substring(p, p + Gbs.GT418_Udc.capacity()));
		p += Gbs.GT418_Udc.capacity();
		Gbs.GT418_Vum.append(pcStr.substring(p, p + Gbs.GT418_Vum.capacity()));
		p += Gbs.GT418_Vum.capacity();
		Gbs.GT418_Vln.append(pcStr.substring(p, p + Gbs.GT418_Vln.capacity()));
		p += Gbs.GT418_Vln.capacity();
		Gbs.GT418_Vlc.append(pcStr.substring(p, p + Gbs.GT418_Vlc.capacity()));
		p += Gbs.GT418_Vlc.capacity();
		Gbs.GT418_Obb.append(pcStr.substring(p, p + Gbs.GT418_Obb.capacity()));
		p += Gbs.GT418_Obb.capacity();
		l_MsgGT418.GT418_Gbs = Gbs;
		vec.add(l_MsgGT418);
		return vec;
	}

	public static String LSet_De_MsgGT418(Buf_MsgGT418 p_MsgGT418) {
		String pcStr = "";
		pcStr = pcStr + p_MsgGT418.GT418_Idr.toString();
		pcStr = pcStr + p_MsgGT418.GT418_Gti.toString();
		pcStr = pcStr + p_MsgGT418.GT418_Seq.toString();
		Grt_Gbs_Det Gbs = new Grt_Gbs_Det();
		Gbs = p_MsgGT418.GT418_Gbs;
		pcStr = pcStr + Gbs.GT418_Itb.toString();
		pcStr = pcStr + Gbs.GT418_Dcn.toString();
		pcStr = pcStr + Gbs.GT418_Dpc.toString();
		pcStr = pcStr + Gbs.GT418_Dsb.toString();
		pcStr = pcStr + Gbs.GT418_Fem.toString();
		pcStr = pcStr + Gbs.GT418_Pct.toString();
		pcStr = pcStr + Gbs.GT418_Fvt.toString();
		pcStr = pcStr + Gbs.GT418_Umt.toString();
		pcStr = pcStr + Gbs.GT418_Udc.toString();
		pcStr = pcStr + Gbs.GT418_Vum.toString();
		pcStr = pcStr + Gbs.GT418_Vln.toString();
		pcStr = pcStr + Gbs.GT418_Vlc.toString();
		pcStr = pcStr + Gbs.GT418_Obb.toString();
		return pcStr;
	}
}
