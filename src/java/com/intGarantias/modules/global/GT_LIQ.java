// Source File Name:   GT_LIQ.java

package com.intGarantias.modules.global;

import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.util.Formateo;

import java.util.*;
import java.lang.*;
import org.apache.turbine.util.Log;

public class GT_LIQ
{
  //---------------------------------------------------------------------------------------
  public GT_LIQ()
  {
  }
  //=======================================================================================
  //Generica: Mensaje para Impresion
  //---------------------------------------------------------------------------------------
  public static class Buf_vBns
  {
    public StringBuffer vBns_Itb  = new StringBuffer(2  );  //X(02)       Tipo Bien
    public StringBuffer vBns_Dpc  = new StringBuffer(10 );  //X(10)       PContable
    public StringBuffer vBns_Dsb  = new StringBuffer(100);  //X(100)      Descripcion Bien
    public StringBuffer vBns_Vcn  = new StringBuffer(15 );  //9(13)V9(2)  Valor Contab ORDPPAL
    public StringBuffer vBns_Mrj  = new StringBuffer(15 );  //9(11)V9(4)  Valor MO ORDPPAL
    public StringBuffer vBns_Fvb  = new StringBuffer(8  );  //9(08)       FVencto Bien

    public String getItb()         {return vBns_Itb.toString();}
    public String getDpc()         {return vBns_Dpc.toString();}
    public String getDsb()         {return vBns_Dsb.toString();}
    public String getVcn()         {return vBns_Vcn.toString();}
    public String getMrj()         {return vBns_Mrj.toString();}
    public String getFvb()         {return vBns_Fvb.toString();}

    public Formateo f = new Formateo();
    public String fgetFvb()        {
                                    if (getFvb().trim().equals("10020101"))
                                       { return "SinFecha"; }
                                    else
                                       { return f.FormatDate(getFvb()); }
                                   }
    public String fgetVcn()        {return f.Formato(getVcn(),"99.999.999.999",2,2,',');}
    public String fgetMrj()        {return f.Formato(getMrj(),"99.999.999.999",4,4,',');}
  }
  //---------------------------------------------------------------------------------------
  public static class Buf_vIpt
  {
    public StringBuffer vIpt_Ipt  = new StringBuffer(30 );  //X(30)       Descripcion Imputacion
    public StringBuffer vIpt_Vcn  = new StringBuffer(15 );  //9(13)V9(2)  Valor Contab Imputacion

    public String getIpt()         {return vIpt_Ipt.toString();}
    public String getVcn()         {return vIpt_Vcn.toString();}

    public Formateo f = new Formateo();
    public String fgetVcn()      {return f.Formato(getVcn(),"99.999.999.999",2,2,',');}
  }
  //---------------------------------------------------------------------------------------
  public static class Buf_TxGT900
  {
    public StringBuffer GT900_Ntr = new StringBuffer(7  );  //9(07)       Id. Transaccion
    public StringBuffer GT900_Ttr = new StringBuffer(5  );  //X(05)       Transaccion
    public StringBuffer GT900_Ntt = new StringBuffer(30 );  //X(30)       Nombre Transaccion
    public StringBuffer GT900_Fla = new StringBuffer(7  );  //9(07)       Folio Anulacion
    public StringBuffer GT900_Fem = new StringBuffer(8  );  //9(08)       Fecha
    public StringBuffer GT900_Hrm = new StringBuffer(6  );  //9(06)       Hora
    public StringBuffer GT900_Suc = new StringBuffer(3  );  //9(03)       Sucursal
    public StringBuffer GT900_Nsu = new StringBuffer(15 );  //X(15)       Nombre Sucursal
    public StringBuffer GT900_Eje = new StringBuffer(4  );  //9(04)       Ejecutivo
    public StringBuffer GT900_Nej = new StringBuffer(25 );  //X(25)       Nombre Ejecutivo
    public StringBuffer GT900_Tej = new StringBuffer(10 );  //X(10)       Telefono Ejecutivo
    public StringBuffer GT900_Cli = new StringBuffer(10 );  //X(10)       Id. Cliente (RUT)
    public StringBuffer GT900_Ncl = new StringBuffer(40 );  //X(40)       Nombre Cliente
    public StringBuffer GT900_Dir = new StringBuffer(107);  //X(107)      Direccion Cliente
    public StringBuffer GT900_Cnr = new StringBuffer(10 );  //X(10)       Sistema-Contrato
    public StringBuffer GT900_Dcn = new StringBuffer(5  );  //X(05)       Producto
    public StringBuffer GT900_Ndc = new StringBuffer(30 );  //X(30)       Nombre Producto
    public StringBuffer GT900_Tmn = new StringBuffer(3  );  //X(03)       TpMoneda
    public StringBuffer GT900_Trj = new StringBuffer(3  );  //X(03)       TpReajustabilidad            
    public StringBuffer GT900_Mnd = new StringBuffer(3  );  //9(03)       Moneda                     
    public StringBuffer GT900_Nmn = new StringBuffer(15 );  //X(15)       Nombre Moneda
    public StringBuffer GT900_Ggr = new StringBuffer(3  );  //X(03)       Grupo Garantia (HYP/CRH/AYF/CTW/DMN/DMX/PMN/PMX)
    public StringBuffer GT900_Dsg = new StringBuffer(100);  //X(100)      Descripcion Garantia
    public StringBuffer GT900_Fec = new StringBuffer(8  );  //9(08)       FEvento Garantia
    public StringBuffer GT900_Vuc = new StringBuffer(9  );  //9(07)V9(2)  Valor Conversion FEC
    public Vector      vGT900_Ipt = new Vector();           //--          Tabla Imps (0 To 14)
    public Vector      vGT900_Bns = new Vector();           //--          Tabla Bienes (0 To 49)

    public String getNtr()         {return GT900_Ntr.toString();}
    public String getTtr()         {return GT900_Ttr.toString();}
    public String getNtt()         {return GT900_Ntt.toString();}
    public String getFla()         {return GT900_Fla.toString();}
    public String getFem()         {return GT900_Fem.toString();}
    public String getHrm()         {return GT900_Hrm.toString();}
    public String getSuc()         {return GT900_Suc.toString();}
    public String getNsu()         {return GT900_Nsu.toString();}
    public String getEje()         {return GT900_Eje.toString();}
    public String getNej()         {return GT900_Nej.toString();}
    public String getTej()         {return GT900_Tej.toString();}
    public String getCli()         {return GT900_Cli.toString();}
    public String getNcl()         {return GT900_Ncl.toString();}
    public String getDir()         {return GT900_Dir.toString();}
    public String getCnr()         {return GT900_Cnr.toString();}
    public String getDcn()         {return GT900_Dcn.toString();}
    public String getNdc()         {return GT900_Ndc.toString();}
    public String getTmn()         {return GT900_Tmn.toString();}
    public String getTrj()         {return GT900_Trj.toString();}
    public String getMnd()         {return GT900_Mnd.toString();}
    public String getNmn()         {return GT900_Nmn.toString();}
    public String getGgr()         {return GT900_Ggr.toString();}
    public String getDsg()         {return GT900_Dsg.toString();}
    public String getFec()         {return GT900_Fec.toString();}
    public String getVuc()         {return GT900_Vuc.toString();}
    public Vector getvIpt()        {return vGT900_Ipt;}
    public Vector getvBns()        {return vGT900_Bns;}

    public Formateo f = new Formateo();
    public String fgetFem()        {return f.FormatDate(getFem());}
    public String fgetFec()        {return f.FormatDate(getFec());}
    public String fgetHrm()        {return f.EdtHra(getHrm());}
    public String fgetCli()        {return f.fmtRut(getCli());}
    public String fgetDir1()
                                   {
                                     String lcTemp = getDir().substring(0,34).trim() + " ";
                                     lcTemp += getDir().substring(34,40).trim() + " ";
                                     if ( getDir().substring(40,42).trim().equals("CA")){ lcTemp += "CASA ";    }
                                     if ( getDir().substring(40,42).trim().equals("DE")){ lcTemp += "DEPTO ";   }
                                     if ( getDir().substring(40,42).trim().equals("OF")){ lcTemp += "OFICINA "; }
                                     if ( getDir().substring(40,42).trim().equals("LO")){ lcTemp += "LOCAL ";   }
                                     if ( getDir().substring(40,42).trim().equals("PA")){ lcTemp += "PARCELA "; }
                                     lcTemp += getDir().substring(42,48).trim() + " ";
                                     if ( getDir().substring(48,50).trim().equals("PO")){ lcTemp += "POBLACION "; }
                                     if ( getDir().substring(48,50).trim().equals("CO")){ lcTemp += "CONDOMINIO ";}
                                     if ( getDir().substring(48,50).trim().equals("ED")){ lcTemp += "EDIFICIO ";  }
                                     if ( getDir().substring(48,50).trim().equals("VI")){ lcTemp += "VILLA ";     }
                                     if ( getDir().substring(48,50).trim().equals("BL")){ lcTemp += "BLOCK ";     }
                                     if ( getDir().substring(48,50).trim().equals("TO")){ lcTemp += "TORRE ";     }
                                     if ( getDir().substring(48,50).trim().equals("FU")){ lcTemp += "FUNDO ";     }
                                     if ( getDir().substring(48,50).trim().equals("CA")){ lcTemp += "CAMPO ";     }
                                     lcTemp += getDir().substring(50,70).trim();
                                     return lcTemp;
                                   }
    public String fgetDir2()
                                   {
                                     String lcTemp = getDir().substring(70,90).trim() + " / ";
                                     lcTemp += getDir().substring(90,107).trim();
                                     return lcTemp;
                                   }
    public String fgetCnr()
                                   {
                                     String lcTemp = getCnr().substring(0,3).trim() + "-";
                                     lcTemp += getCnr().substring(3,10).trim();
                                     return lcTemp;
                                   }
    public String fgetVuc()        {return f.Formato(getVuc(),"9.999.999",2,2,',');}
  }
  //=======================================================================================
  //Generica: GLOBAL
  public static class Buf_Liq_GtGbl
  {
    public StringBuffer GtGbl_Tmn = new StringBuffer(3  );  //X(03)        3 TpMoneda
    public StringBuffer GtGbl_Trj = new StringBuffer(3  );  //X(03)        6 TpReajustabilidad            
    public StringBuffer GtGbl_Mnd = new StringBuffer(3  );  //9(03)        9 Moneda                     
    public StringBuffer GtGbl_Nmn = new StringBuffer(15 );  //X(15)       24 Nombre Moneda
    public StringBuffer GtGbl_Ggr = new StringBuffer(3  );  //X(03)       27 Grupo Garantia (HYP/CRH/AYF/CTW/DMN/DMX/PMN/PMX)
    public StringBuffer GtGbl_Dsg = new StringBuffer(100);  //X(100)     127 Descripcion Garantia
    public StringBuffer GtGbl_Fem = new StringBuffer(8  );  //9(08)      135 FMovimiento Garantia
    public StringBuffer GtGbl_Fec = new StringBuffer(8  );  //9(08)      143 FIngreso Garantia
    public StringBuffer GtGbl_Vuc = new StringBuffer(9  );  //9(07)V9(2) 152 Valor Conversion FEC
    public StringBuffer GtGbl_Fvc = new StringBuffer(8  );  //9(08)      160 FVcto Garantia            
    public StringBuffer GtGbl_Vl1 = new StringBuffer(15 );  //9(13)V9(2) 175 Valor Contab 1
    public StringBuffer GtGbl_Vl2 = new StringBuffer(15 );  //9(13)V9(2) 190 Valor Contab 2
    public StringBuffer GtGbl_Vl3 = new StringBuffer(15 );  //9(13)V9(2) 205 Valor Contab 3
    public StringBuffer GtGbl_Vl4 = new StringBuffer(15 );  //9(13)V9(2) 220 Valor Contab 4
    public StringBuffer GtGbl_Fll = new StringBuffer(428);  //X(428)     648 Disponible                 

    public String getTmn()       {return GtGbl_Tmn.toString();}
    public String getTrj()       {return GtGbl_Trj.toString();}
    public String getMnd()       {return GtGbl_Mnd.toString();}
    public String getNmn()       {return GtGbl_Nmn.toString();}
    public String getGgr()       {return GtGbl_Ggr.toString();}
    public String getDsg()       {return GtGbl_Dsg.toString();}
    public String getFem()       {return GtGbl_Fem.toString();}
    public String getFec()       {return GtGbl_Fec.toString();}
    public String getVuc()       {return GtGbl_Vuc.toString();}
    public String getFvc()       {return GtGbl_Fvc.toString();}
    public String getVl1()       {return GtGbl_Vl1.toString();}
    public String getVl2()       {return GtGbl_Vl2.toString();}
    public String getVl3()       {return GtGbl_Vl3.toString();}
    public String getVl4()       {return GtGbl_Vl4.toString();}
    public String getFll()       {return GtGbl_Fll.toString();}

    public Formateo f = new Formateo();
    public String fgetFem()      {return f.FormatDate(getFem());}
    public String fgetFec()      {return f.FormatDate(getFec());}
    public String fgetFvc()      {return f.FormatDate(getFvc());}
    public String fgetVuc()      {return f.Formato(getVuc(),"9.999.999",2,2,',');}
    public String fgetVl1()      {return f.Formato(getVl1(),"99.999.999.999",2,2,',');}
    public String fgetVl2()      {return f.Formato(getVl2(),"99.999.999.999",2,2,',');}
    public String fgetVl3()      {return f.Formato(getVl3(),"99.999.999.999",2,2,',');}
    public String fgetVl4()      {return f.Formato(getVl4(),"99.999.999.999",2,2,',');}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_Liq_GtGbl LSet_A_LiqGtGbl(String pcStr)
  {
    Buf_Liq_GtGbl l_Liq = new Buf_Liq_GtGbl();
    Vector vDatos = LSet_A_vLiqGtGbl(pcStr);
    l_Liq = (Buf_Liq_GtGbl)vDatos.elementAt(0);
    return l_Liq;
  }
  //---------------------------------------------------------------------------------------
  public static Vector LSet_A_vLiqGtGbl(String pcStr)
  {
    Vector vec = new Vector();
    int p = 0;
    Buf_Liq_GtGbl l_GtGbl = new Buf_Liq_GtGbl();
    l_GtGbl.GtGbl_Tmn.append(pcStr.substring(p, p + l_GtGbl.GtGbl_Tmn.capacity())); p = p + l_GtGbl.GtGbl_Tmn.capacity();
    l_GtGbl.GtGbl_Trj.append(pcStr.substring(p, p + l_GtGbl.GtGbl_Trj.capacity())); p = p + l_GtGbl.GtGbl_Trj.capacity();
    l_GtGbl.GtGbl_Mnd.append(pcStr.substring(p, p + l_GtGbl.GtGbl_Mnd.capacity())); p = p + l_GtGbl.GtGbl_Mnd.capacity();
    l_GtGbl.GtGbl_Nmn.append(pcStr.substring(p, p + l_GtGbl.GtGbl_Nmn.capacity())); p = p + l_GtGbl.GtGbl_Nmn.capacity();
    l_GtGbl.GtGbl_Ggr.append(pcStr.substring(p, p + l_GtGbl.GtGbl_Ggr.capacity())); p = p + l_GtGbl.GtGbl_Ggr.capacity();
    l_GtGbl.GtGbl_Dsg.append(pcStr.substring(p, p + l_GtGbl.GtGbl_Dsg.capacity())); p = p + l_GtGbl.GtGbl_Dsg.capacity();
    l_GtGbl.GtGbl_Fem.append(pcStr.substring(p, p + l_GtGbl.GtGbl_Fem.capacity())); p = p + l_GtGbl.GtGbl_Fem.capacity();
    l_GtGbl.GtGbl_Fec.append(pcStr.substring(p, p + l_GtGbl.GtGbl_Fec.capacity())); p = p + l_GtGbl.GtGbl_Fec.capacity();
    l_GtGbl.GtGbl_Vuc.append(pcStr.substring(p, p + l_GtGbl.GtGbl_Vuc.capacity())); p = p + l_GtGbl.GtGbl_Vuc.capacity();
    l_GtGbl.GtGbl_Fvc.append(pcStr.substring(p, p + l_GtGbl.GtGbl_Fvc.capacity())); p = p + l_GtGbl.GtGbl_Fvc.capacity();
    l_GtGbl.GtGbl_Vl1.append(pcStr.substring(p, p + l_GtGbl.GtGbl_Vl1.capacity())); p = p + l_GtGbl.GtGbl_Vl1.capacity();
    l_GtGbl.GtGbl_Vl2.append(pcStr.substring(p, p + l_GtGbl.GtGbl_Vl2.capacity())); p = p + l_GtGbl.GtGbl_Vl2.capacity();
    l_GtGbl.GtGbl_Vl3.append(pcStr.substring(p, p + l_GtGbl.GtGbl_Vl3.capacity())); p = p + l_GtGbl.GtGbl_Vl3.capacity();
    l_GtGbl.GtGbl_Vl4.append(pcStr.substring(p, p + l_GtGbl.GtGbl_Vl4.capacity())); p = p + l_GtGbl.GtGbl_Vl4.capacity();
    l_GtGbl.GtGbl_Fll.append(pcStr.substring(p, p + l_GtGbl.GtGbl_Fll.capacity())); p = p + l_GtGbl.GtGbl_Fll.capacity();
    vec.add (l_GtGbl);                          
    return vec;
  }
  //=======================================================================================
  //Generica: PAGO A TASADORES
  public static class Buf_Liq_GtPts
  {
    public StringBuffer GtPts_Tmn  = new StringBuffer(3  );  //X(03)        3 TpMoneda
    public StringBuffer GtPts_Trj  = new StringBuffer(3  );  //X(03)        6 TpReajustabilidad            
    public StringBuffer GtPts_Mnd  = new StringBuffer(3  );  //X(03)        9 Moneda                     
    public StringBuffer GtPts_Nmn  = new StringBuffer(15 );  //X(15)       24 Nombre Moneda
    public StringBuffer GtPts_Ggr  = new StringBuffer(3  );  //X(03)       27 Grupo Garantia (HYP/CRH/AYF/CTW/DMN/DMX/PMN/PMX)
    public StringBuffer GtPts_Dsg  = new StringBuffer(100);  //X(100)     127 Descripcion Garantia
    public StringBuffer GtPts_Fem  = new StringBuffer(8  );  //9(08)      135 FMovimiento Garantia
    public StringBuffer GtPts_Fec  = new StringBuffer(8  );  //9(08)      143 FIngreso Garantia
    public StringBuffer GtPts_Vuc  = new StringBuffer(9  );  //9(07)V9(2) 152 Valor Conversion FEC
    public StringBuffer GtPts_Fvc  = new StringBuffer(8  );  //9(08)      160 FVcto Garantia
    public StringBuffer GtPts_Gst  = new StringBuffer(15 );  //9(13)V9(2) 175 Valor Gasto
    public StringBuffer GtPts_Hon  = new StringBuffer(15 );  //9(13)V9(2) 190 Valor Honorarios
    public StringBuffer GtPts_Rti  = new StringBuffer(15 );  //9(13)V9(2) 205 Valor Impto Honorarios
    public StringBuffer GtPts_Com  = new StringBuffer(15 );  //9(13)V9(2) 220 Valor Comision
    public StringBuffer GtPts_Iva  = new StringBuffer(15 );  //9(13)V9(2) 235 Valor IVA Comision
    public StringBuffer GtPts_Tot1 = new StringBuffer(15 );  //9(13)V9(2) 250 Valor Total
    public StringBuffer GtPts_Cbp1 = new StringBuffer(5  );  //X(05)      255 Codigo Cuenta Cargo
    public StringBuffer GtPts_Dbp1 = new StringBuffer(12 );  //9(12)      267 Id Cuenta Cargo
    public StringBuffer GtPts_Tot2 = new StringBuffer(15 );  //9(13)V9(2) 282 Valor Total
    public StringBuffer GtPts_Cbp2 = new StringBuffer(5  );  //X(05)      287 Codigo Cuenta Cargo
    public StringBuffer GtPts_Dbp2 = new StringBuffer(12 );  //9(12)      299 Id Cuenta Cargo
    public StringBuffer GtPts_Tot3 = new StringBuffer(15 );  //9(13)V9(2) 314 Valor Total
    public StringBuffer GtPts_Cbp3 = new StringBuffer(5  );  //X(05)      319 Codigo Cuenta Cargo
    public StringBuffer GtPts_Dbp3 = new StringBuffer(12 );  //9(12)      331 Id Cuenta Cargo
    public StringBuffer GtPts_Tot4 = new StringBuffer(15 );  //9(13)V9(2) 346 Valor Total
    public StringBuffer GtPts_Cbp4 = new StringBuffer(5  );  //X(05)      351 Codigo Cuenta Cargo
    public StringBuffer GtPts_Dbp4 = new StringBuffer(12 );  //9(12)      363 Id Cuenta Cargo
    public StringBuffer GtPts_Tot5 = new StringBuffer(15 );  //9(13)V9(2) 378 Valor Total
    public StringBuffer GtPts_Cbp5 = new StringBuffer(5  );  //X(05)      383 Codigo Cuenta Cargo
    public StringBuffer GtPts_Dbp5 = new StringBuffer(12 );  //9(12)      395 Id Cuenta Cargo
    public StringBuffer GtPts_Fll  = new StringBuffer(253);  //X(253)     648 Disponible                 

    public String getTmn()       {return GtPts_Tmn.toString();}
    public String getTrj()       {return GtPts_Trj.toString();}
    public String getMnd()       {return GtPts_Mnd.toString();}
    public String getNmn()       {return GtPts_Nmn.toString();}
    public String getGgr()       {return GtPts_Ggr.toString();}
    public String getDsg()       {return GtPts_Dsg.toString();}
    public String getFem()       {return GtPts_Fem.toString();}
    public String getFec()       {return GtPts_Fec.toString();}
    public String getVuc()       {return GtPts_Vuc.toString();}
    public String getFvc()       {return GtPts_Fvc.toString();}
    public String getGst()       {return GtPts_Gst.toString();}
    public String getHon()       {return GtPts_Hon.toString();}
    public String getRti()       {return GtPts_Rti.toString();}
    public String getCom()       {return GtPts_Com.toString();}
    public String getIva()       {return GtPts_Iva.toString();}
    public String getTot1()      {return GtPts_Tot1.toString();}
    public String getCbp1()      {return GtPts_Cbp1.toString();}
    public String getDbp1()      {return GtPts_Dbp1.toString();}
    public String getTot2()      {return GtPts_Tot2.toString();}
    public String getCbp2()      {return GtPts_Cbp2.toString();}
    public String getDbp2()      {return GtPts_Dbp2.toString();}
    public String getTot3()      {return GtPts_Tot3.toString();}
    public String getCbp3()      {return GtPts_Cbp3.toString();}
    public String getDbp3()      {return GtPts_Dbp3.toString();}
    public String getTot4()      {return GtPts_Tot4.toString();}
    public String getCbp4()      {return GtPts_Cbp4.toString();}
    public String getDbp4()      {return GtPts_Dbp4.toString();}
    public String getTot5()      {return GtPts_Tot5.toString();}
    public String getCbp5()      {return GtPts_Cbp5.toString();}
    public String getDbp5()      {return GtPts_Dbp5.toString();}
    public String getFll()       {return GtPts_Fll.toString();}

    public Formateo f = new Formateo();
    public String fgetFem()      {return f.FormatDate(getFem());}
    public String fgetFec()      {return f.FormatDate(getFec());}
    public String fgetFvc()      {return f.FormatDate(getFvc());}
    public String fgetVuc()      {return f.Formato(getVuc(),"9.999.999",2,2,',');}
    public String fgetGst()      {return f.Formato(getGst(),"99.999.999.999",2,2,',');}
    public String fgetHon()      {return f.Formato(getHon(),"99.999.999.999",2,2,',');}
    public String fgetRti()      {return f.Formato(getRti(),"99.999.999.999",2,2,',');}
    public String fgetCom()      {return f.Formato(getCom(),"99.999.999.999",2,2,',');}
    public String fgetIva()      {return f.Formato(getIva(),"99.999.999.999",2,2,',');}
    public String fgetTot1()     {return f.Formato(getTot1(),"99.999.999.999",2,2,',');}
    public String fgetDbp1()     {return f.EditCta(GtPts_Dbp1.toString());}
    public String fgetTot2()     {return f.Formato(getTot2(),"99.999.999.999",2,2,',');}
    public String fgetDbp2()     {return f.EditCta(GtPts_Dbp2.toString());}
    public String fgetTot3()     {return f.Formato(getTot3(),"99.999.999.999",2,2,',');}
    public String fgetDbp3()     {return f.EditCta(GtPts_Dbp3.toString());}
    public String fgetTot4()     {return f.Formato(getTot4(),"99.999.999.999",2,2,',');}
    public String fgetDbp4()     {return f.EditCta(GtPts_Dbp4.toString());}
    public String fgetTot5()     {return f.Formato(getTot5(),"99.999.999.999",2,2,',');}
    public String fgetDbp5()     {return f.EditCta(GtPts_Dbp5.toString());}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_Liq_GtPts LSet_A_LiqGtPts(String pcStr)
  {
    Buf_Liq_GtPts l_Liq = new Buf_Liq_GtPts();
    Vector vDatos = LSet_A_vLiqGtPts(pcStr);
    l_Liq = (Buf_Liq_GtPts)vDatos.elementAt(0);
    return l_Liq;
  }
  //---------------------------------------------------------------------------------------
  public static Vector LSet_A_vLiqGtPts(String pcStr)
  {
    Vector vec = new Vector();
    int p = 0;
    Buf_Liq_GtPts l_GtPts = new Buf_Liq_GtPts();
    l_GtPts.GtPts_Tmn.append(pcStr.substring(p, p + l_GtPts.GtPts_Tmn.capacity())); p = p + l_GtPts.GtPts_Tmn.capacity();
    l_GtPts.GtPts_Trj.append(pcStr.substring(p, p + l_GtPts.GtPts_Trj.capacity())); p = p + l_GtPts.GtPts_Trj.capacity();
    l_GtPts.GtPts_Mnd.append(pcStr.substring(p, p + l_GtPts.GtPts_Mnd.capacity())); p = p + l_GtPts.GtPts_Mnd.capacity();
    l_GtPts.GtPts_Nmn.append(pcStr.substring(p, p + l_GtPts.GtPts_Nmn.capacity())); p = p + l_GtPts.GtPts_Nmn.capacity();
    l_GtPts.GtPts_Ggr.append(pcStr.substring(p, p + l_GtPts.GtPts_Ggr.capacity())); p = p + l_GtPts.GtPts_Ggr.capacity();
    l_GtPts.GtPts_Dsg.append(pcStr.substring(p, p + l_GtPts.GtPts_Dsg.capacity())); p = p + l_GtPts.GtPts_Dsg.capacity();
    l_GtPts.GtPts_Fem.append(pcStr.substring(p, p + l_GtPts.GtPts_Fem.capacity())); p = p + l_GtPts.GtPts_Fem.capacity();
    l_GtPts.GtPts_Fec.append(pcStr.substring(p, p + l_GtPts.GtPts_Fec.capacity())); p = p + l_GtPts.GtPts_Fec.capacity();
    l_GtPts.GtPts_Vuc.append(pcStr.substring(p, p + l_GtPts.GtPts_Vuc.capacity())); p = p + l_GtPts.GtPts_Vuc.capacity();
    l_GtPts.GtPts_Fvc.append(pcStr.substring(p, p + l_GtPts.GtPts_Fvc.capacity())); p = p + l_GtPts.GtPts_Fvc.capacity();
    l_GtPts.GtPts_Gst.append(pcStr.substring(p, p + l_GtPts.GtPts_Gst.capacity())); p = p + l_GtPts.GtPts_Gst.capacity();
    l_GtPts.GtPts_Hon.append(pcStr.substring(p, p + l_GtPts.GtPts_Hon.capacity())); p = p + l_GtPts.GtPts_Hon.capacity();
    l_GtPts.GtPts_Rti.append(pcStr.substring(p, p + l_GtPts.GtPts_Rti.capacity())); p = p + l_GtPts.GtPts_Rti.capacity();
    l_GtPts.GtPts_Com.append(pcStr.substring(p, p + l_GtPts.GtPts_Com.capacity())); p = p + l_GtPts.GtPts_Com.capacity();
    l_GtPts.GtPts_Iva.append(pcStr.substring(p, p + l_GtPts.GtPts_Iva.capacity())); p = p + l_GtPts.GtPts_Iva.capacity();
    l_GtPts.GtPts_Tot1.append(pcStr.substring(p, p + l_GtPts.GtPts_Tot1.capacity())); p = p + l_GtPts.GtPts_Tot1.capacity();
    l_GtPts.GtPts_Cbp1.append(pcStr.substring(p, p + l_GtPts.GtPts_Cbp1.capacity())); p = p + l_GtPts.GtPts_Cbp1.capacity();
    l_GtPts.GtPts_Dbp1.append(pcStr.substring(p, p + l_GtPts.GtPts_Dbp1.capacity())); p = p + l_GtPts.GtPts_Dbp1.capacity();
    l_GtPts.GtPts_Tot2.append(pcStr.substring(p, p + l_GtPts.GtPts_Tot2.capacity())); p = p + l_GtPts.GtPts_Tot2.capacity();
    l_GtPts.GtPts_Cbp2.append(pcStr.substring(p, p + l_GtPts.GtPts_Cbp2.capacity())); p = p + l_GtPts.GtPts_Cbp2.capacity();
    l_GtPts.GtPts_Dbp2.append(pcStr.substring(p, p + l_GtPts.GtPts_Dbp2.capacity())); p = p + l_GtPts.GtPts_Dbp2.capacity();
    l_GtPts.GtPts_Tot3.append(pcStr.substring(p, p + l_GtPts.GtPts_Tot3.capacity())); p = p + l_GtPts.GtPts_Tot3.capacity();
    l_GtPts.GtPts_Cbp3.append(pcStr.substring(p, p + l_GtPts.GtPts_Cbp3.capacity())); p = p + l_GtPts.GtPts_Cbp3.capacity();
    l_GtPts.GtPts_Dbp3.append(pcStr.substring(p, p + l_GtPts.GtPts_Dbp3.capacity())); p = p + l_GtPts.GtPts_Dbp3.capacity();
    l_GtPts.GtPts_Tot4.append(pcStr.substring(p, p + l_GtPts.GtPts_Tot4.capacity())); p = p + l_GtPts.GtPts_Tot4.capacity();
    l_GtPts.GtPts_Cbp4.append(pcStr.substring(p, p + l_GtPts.GtPts_Cbp4.capacity())); p = p + l_GtPts.GtPts_Cbp4.capacity();
    l_GtPts.GtPts_Dbp4.append(pcStr.substring(p, p + l_GtPts.GtPts_Dbp4.capacity())); p = p + l_GtPts.GtPts_Dbp4.capacity();
    l_GtPts.GtPts_Tot5.append(pcStr.substring(p, p + l_GtPts.GtPts_Tot5.capacity())); p = p + l_GtPts.GtPts_Tot5.capacity();
    l_GtPts.GtPts_Cbp5.append(pcStr.substring(p, p + l_GtPts.GtPts_Cbp5.capacity())); p = p + l_GtPts.GtPts_Cbp5.capacity();
    l_GtPts.GtPts_Dbp5.append(pcStr.substring(p, p + l_GtPts.GtPts_Dbp5.capacity())); p = p + l_GtPts.GtPts_Dbp5.capacity();
    l_GtPts.GtPts_Fll.append(pcStr.substring(p, p + l_GtPts.GtPts_Fll.capacity())); p = p + l_GtPts.GtPts_Fll.capacity();
    vec.add (l_GtPts);                          
    return vec;
  }
  //=======================================================================================
}