// Source File Name:   MSGGT422.java

package com.intGarantias.modules.global;

import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.util.Formateo;

import java.util.*;
import java.lang.*;
import org.apache.turbine.util.Log;

public class MSGGT422
{
  //---------------------------------------------------------------------------------------
  public static class vPgm
  { public StringBuffer GT422_Pgm = new StringBuffer(2  );      //'9(02)      575 X(34) Programa
    public String getPgm()         {return GT422_Pgm.toString();}
  }
  public static class vEst
  { public StringBuffer GT422_Est = new StringBuffer(1  );      //'X(01)      586 X(11) Estructura
    public String getEst()         {return GT422_Est.toString();}
  }
  public static class vTer
  { public StringBuffer GT422_Ter = new StringBuffer(1  );      //'X(01)      597 X(11) Terminaciones
    public String getTer()         {return GT422_Ter.toString();}
  }
  public static class vPav
  { public StringBuffer GT422_Pav = new StringBuffer(1  );      //'X(01)      608 X(11) Pavimentos
    public String getPav()         {return GT422_Pav.toString();}
  }
  public static class vCub
  { public StringBuffer GT422_Cub = new StringBuffer(1  );      //'X(01)      619 X(11) Cubiertas
    public String getCub()         {return GT422_Cub.toString();}
  }
  public static class vInt
  { public StringBuffer GT422_Int = new StringBuffer(1  );      //'X(01)      630 X(11) Instalaciones
    public String getInt()         {return GT422_Int.toString();}
  }
  //---------------------------------------------------------------------------------------
  public static class Buf_MsgGT422
  {
    public StringBuffer GT422_Idr = new StringBuffer(1  );   //X(01)
    public StringBuffer GT422_Gti = new StringBuffer(10 );   //X(10)
    public StringBuffer GT422_Seq = new StringBuffer(3  );   //9(03)
    public StringBuffer GT422_Sqd = new StringBuffer(3  );   //9(03)
    public StringBuffer GT422_Fts = new StringBuffer(8  );   //9(08)
    public StringBuffer GT422_Vuf = new StringBuffer(9  );   //9(07)V9(2)
    public StringBuffer GT422_Vtc = new StringBuffer(9  );   //9(07)V9(2)
    public StringBuffer GT422_Tuf = new StringBuffer(15 );   //9(11)V9(4)
    public StringBuffer GT422_Ttc = new StringBuffer(15 );   //9(13)V9(2)
    public StringBuffer GT422_Tps = new StringBuffer(15 );   //9(13)V9(2)
    public StringBuffer GT422_Ith = new StringBuffer(2  );   //X(02)
    public StringBuffer GT422_Dsh = new StringBuffer(30 );   //X(30)
    public StringBuffer GT422_Rs1 = new StringBuffer(5  );   //9(05)
    public StringBuffer GT422_Rs2 = new StringBuffer(3  );   //9(03)
    public StringBuffer GT422_Rds = new StringBuffer(20 );   //X(20)
    public StringBuffer GT422_Faf = new StringBuffer(8  );   //9(08)
    public StringBuffer GT422_Vaf = new StringBuffer(15 );   //9(13)V9(2)
    public StringBuffer GT422_Udd = new StringBuffer(9  );   //9(07)V9(2)
    public StringBuffer GT422_Umd = new StringBuffer(2  );   //X(02)
    public StringBuffer GT422_Umt = new StringBuffer(1  );   //X(01)
    public StringBuffer GT422_Pum = new StringBuffer(15 );   //9(11)V9(4)
    public StringBuffer GT422_Vtu = new StringBuffer(15 );   //9(11)V9(4)
    public StringBuffer GT422_Vbu = new StringBuffer(15 );   //9(11)V9(4)
    public StringBuffer GT422_Plq = new StringBuffer(5  );   //9(03)V9(2)
    public StringBuffer GT422_Vlu = new StringBuffer(15 );   //9(11)V9(4)
    public StringBuffer GT422_Vsu = new StringBuffer(15 );   //9(11)V9(4)
    public StringBuffer GT422_Ano = new StringBuffer(4  );   //9(04)
    public StringBuffer GT422_Nam = new StringBuffer(1  );   //X(01)
    public StringBuffer GT422_Ams = new StringBuffer(1  );   //X(01)
    public StringBuffer GT422_Amn = new StringBuffer(1  );   //X(01)
    public StringBuffer GT422_Ped = new StringBuffer(7  );   //9(07)
    public StringBuffer GT422_Fed = new StringBuffer(8  );   //9(08)
    public StringBuffer GT422_Rem = new StringBuffer(7  );   //9(07)
    public Vector      vGT422_Pgm = new Vector(17);
    public Vector      vGT422_Est = new Vector(11);
    public Vector      vGT422_Ter = new Vector(11);
    public Vector      vGT422_Pav = new Vector(11);
    public Vector      vGT422_Cub = new Vector(11);
    public Vector      vGT422_Int = new Vector(11);
    public StringBuffer GT422_Obt = new StringBuffer(9  );   //9(09)
    public StringBuffer GT422_Ovb = new StringBuffer(9  );   //9(09)
    public StringBuffer GT422_Uso = new StringBuffer(30 );   //X(30)
    public StringBuffer GT422_Agp = new StringBuffer(30 );   //X(30)
    public StringBuffer GT422_Tct = new StringBuffer(30 );   //X(30)
    public StringBuffer GT422_Cld = new StringBuffer(30 );   //X(30)

    public String getIdr()         {return GT422_Idr.toString();}
    public String getGti()         {return GT422_Gti.toString();}
    public String getSeq()         {return GT422_Seq.toString();}
    public String getSqd()         {return GT422_Sqd.toString();}
    public String getFts()         {return GT422_Fts.toString();}
    public String getVuf()         {return GT422_Vuf.toString();}
    public String getVtc()         {return GT422_Vtc.toString();}
    public String getTuf()         {return GT422_Tuf.toString();}
    public String getTtc()         {return GT422_Ttc.toString();}
    public String getTps()         {return GT422_Tps.toString();}
    public String getIth()         {return GT422_Ith.toString();}
    public String getDsh()         {return GT422_Dsh.toString();}
    public String getRs1()         {return GT422_Rs1.toString();}
    public String getRs2()         {return GT422_Rs2.toString();}
    public String getRds()         {return GT422_Rds.toString();}
    public String getFaf()         {return GT422_Faf.toString();}
    public String getVaf()         {return GT422_Vaf.toString();}
    public String getUdd()         {return GT422_Udd.toString();}
    public String getUmd()         {return GT422_Umd.toString();}
    public String getUmt()         {return GT422_Umt.toString();}
    public String getPum()         {return GT422_Pum.toString();}
    public String getVtu()         {return GT422_Vtu.toString();}
    public String getVbu()         {return GT422_Vbu.toString();}
    public String getPlq()         {return GT422_Plq.toString();}
    public String getVlu()         {return GT422_Vlu.toString();}
    public String getVsu()         {return GT422_Vsu.toString();}
    public String getAno()         {return GT422_Ano.toString();}
    public String getNam()         {return GT422_Nam.toString();}
    public String getAms()         {return GT422_Ams.toString();}
    public String getAmn()         {return GT422_Amn.toString();}
    public String getPed()         {return GT422_Ped.toString();}
    public String getFed()         {return GT422_Fed.toString();}
    public String getRem()         {return GT422_Rem.toString();}
    public Vector getVecPgm()      {return vGT422_Pgm;}
    public Vector getVecEst()      {return vGT422_Est;}
    public Vector getVecTer()      {return vGT422_Ter;}
    public Vector getVecPav()      {return vGT422_Pav;}
    public Vector getVecCub()      {return vGT422_Cub;}
    public Vector getVecInt()      {return vGT422_Int;}
    public String getObt()         {return GT422_Obt.toString();}
    public String getOvb()         {return GT422_Ovb.toString();}
    public String getUso()         {return GT422_Uso.toString();}
    public String getAgp()         {return GT422_Agp.toString();}
    public String getTct()         {return GT422_Tct.toString();}
    public String getCld()         {return GT422_Cld.toString();}

    public Formateo f = new Formateo();
    public String fgetFts()        {return f.FormatDate(getFts());}
    public String fgetVuf()        {return f.Formato(getVuf(),"9.999.999",2,2,',');}
    public String fgetVtc()        {return f.Formato(getVtc(),"9.999.999",2,2,',');}
    public String fgetTuf()        {return f.Formato(getTuf(),"99.999.999.999",4,4,',');}
    public String fgetTtc()        {return f.Formato(getTtc(),"99.999.999.999",2,2,',');}
    public String fgetTps()        {return f.Formato(getTps(),"99.999.999.999",2,0,',');}
    public String fgetFaf()        {return f.FormatDate(getFaf());}
    public String fgetVaf()        {return f.Formato(getVaf(),"9.999.999.999.999",2,0,',');}
    public String fgetUdd()        {return f.Formato(getUdd(),"9.999.999",2,2,',');}
    public String fgetPum()
                                   {
                                     int Dec = 0;
                                     if (getUmt().equals("P"))  { Dec = 0; }
                                     if (getUmt().equals("U"))  { Dec = 4; }
                                     if (getUmt().equals("D"))  { Dec = 2; }
                                     return f.Formato(getPum(),"99.999.999.999",4,Dec,',');
                                   }
    public String fgetVtu()
                                   {
                                     int Dec = 0;
                                     if (getUmt().equals("P"))  { Dec = 0; }
                                     if (getUmt().equals("U"))  { Dec = 4; }
                                     if (getUmt().equals("D"))  { Dec = 2; }
                                     return f.Formato(getVtu(),"99.999.999.999",4,Dec,',');
                                   }
    public String fgetVbu()
                                   {
                                     int Dec = 0;
                                     if (getUmt().equals("P"))  { Dec = 0; }
                                     if (getUmt().equals("U"))  { Dec = 4; }
                                     if (getUmt().equals("D"))  { Dec = 2; }
                                     return f.Formato(getVbu(),"99.999.999.999",4,Dec,',');
                                   }
    public String fgetPlq()        {return f.Formato(getPlq(),"999",2,2,',');}
    public String fgetVlu()
                                   {
                                     int Dec = 0;
                                     if (getUmt().equals("P"))  { Dec = 0; }
                                     if (getUmt().equals("U"))  { Dec = 4; }
                                     if (getUmt().equals("D"))  { Dec = 2; }
                                     return f.Formato(getVlu(),"99.999.999.999",4,Dec,',');
                                   }
    public String fgetVsu()
                                   {
                                     int Dec = 0;
                                     if (getUmt().equals("P"))  { Dec = 0; }
                                     if (getUmt().equals("U"))  { Dec = 4; }
                                     if (getUmt().equals("D"))  { Dec = 2; }
                                     return f.Formato(getVsu(),"99.999.999.999",4,Dec,',');
                                   }
  }
  //---------------------------------------------------------------------------------------
  public static Buf_MsgGT422 Inicia_MsgGT422 ()
  {
    Buf_MsgGT422 l_MsgGT422 = new Buf_MsgGT422();
    l_MsgGT422.GT422_Idr.replace(0, l_MsgGT422.GT422_Idr.capacity(), RUTGEN.Blancos(1  ));
    l_MsgGT422.GT422_Gti.replace(0, l_MsgGT422.GT422_Gti.capacity(), RUTGEN.Blancos(10 ));
    l_MsgGT422.GT422_Seq.replace(0, l_MsgGT422.GT422_Seq.capacity(), RUTGEN.Blancos(3  ));
    l_MsgGT422.GT422_Sqd.replace(0, l_MsgGT422.GT422_Sqd.capacity(), RUTGEN.Blancos(3  ));
    l_MsgGT422.GT422_Fts.replace(0, l_MsgGT422.GT422_Fts.capacity(), RUTGEN.Blancos(8  ));
    l_MsgGT422.GT422_Vuf.replace(0, l_MsgGT422.GT422_Vuf.capacity(), RUTGEN.Blancos(9  ));
    l_MsgGT422.GT422_Vtc.replace(0, l_MsgGT422.GT422_Vtc.capacity(), RUTGEN.Blancos(9  ));
    l_MsgGT422.GT422_Tuf.replace(0, l_MsgGT422.GT422_Tuf.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT422.GT422_Ttc.replace(0, l_MsgGT422.GT422_Ttc.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT422.GT422_Tps.replace(0, l_MsgGT422.GT422_Tps.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT422.GT422_Ith.replace(0, l_MsgGT422.GT422_Ith.capacity(), RUTGEN.Blancos(2  ));
    l_MsgGT422.GT422_Dsh.replace(0, l_MsgGT422.GT422_Dsh.capacity(), RUTGEN.Blancos(30 ));
    l_MsgGT422.GT422_Rs1.replace(0, l_MsgGT422.GT422_Rs1.capacity(), RUTGEN.Blancos(5  ));
    l_MsgGT422.GT422_Rs2.replace(0, l_MsgGT422.GT422_Rs2.capacity(), RUTGEN.Blancos(3  ));
    l_MsgGT422.GT422_Rds.replace(0, l_MsgGT422.GT422_Rds.capacity(), RUTGEN.Blancos(20 ));
    l_MsgGT422.GT422_Faf.replace(0, l_MsgGT422.GT422_Faf.capacity(), RUTGEN.Blancos(8  ));
    l_MsgGT422.GT422_Vaf.replace(0, l_MsgGT422.GT422_Vaf.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT422.GT422_Udd.replace(0, l_MsgGT422.GT422_Udd.capacity(), RUTGEN.Blancos(9  ));
    l_MsgGT422.GT422_Umd.replace(0, l_MsgGT422.GT422_Umd.capacity(), RUTGEN.Blancos(2  ));
    l_MsgGT422.GT422_Umt.replace(0, l_MsgGT422.GT422_Umt.capacity(), RUTGEN.Blancos(1  ));
    l_MsgGT422.GT422_Pum.replace(0, l_MsgGT422.GT422_Pum.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT422.GT422_Vtu.replace(0, l_MsgGT422.GT422_Vtu.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT422.GT422_Vbu.replace(0, l_MsgGT422.GT422_Vbu.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT422.GT422_Plq.replace(0, l_MsgGT422.GT422_Plq.capacity(), RUTGEN.Blancos(5  ));
    l_MsgGT422.GT422_Vlu.replace(0, l_MsgGT422.GT422_Vlu.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT422.GT422_Vsu.replace(0, l_MsgGT422.GT422_Vsu.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT422.GT422_Ano.replace(0, l_MsgGT422.GT422_Ano.capacity(), RUTGEN.Blancos(4  ));
    l_MsgGT422.GT422_Nam.replace(0, l_MsgGT422.GT422_Nam.capacity(), RUTGEN.Blancos(1  ));
    l_MsgGT422.GT422_Ams.replace(0, l_MsgGT422.GT422_Ams.capacity(), RUTGEN.Blancos(1  ));
    l_MsgGT422.GT422_Amn.replace(0, l_MsgGT422.GT422_Amn.capacity(), RUTGEN.Blancos(1  ));
    l_MsgGT422.GT422_Ped.replace(0, l_MsgGT422.GT422_Ped.capacity(), RUTGEN.Blancos(7  ));
    l_MsgGT422.GT422_Fed.replace(0, l_MsgGT422.GT422_Fed.capacity(), RUTGEN.Blancos(8  ));
    l_MsgGT422.GT422_Rem.replace(0, l_MsgGT422.GT422_Rem.capacity(), RUTGEN.Blancos(7  ));
    for (int i=0; i<17; i++)
        {
          vPgm Tap = new vPgm();
          Tap.GT422_Pgm.replace(0, Tap.GT422_Pgm.capacity(), RUTGEN.Blancos(2));
          l_MsgGT422.vGT422_Pgm.add(Tap);
        }
    for (int i=0; i<11; i++)
        {
          vEst Tap = new vEst();
          Tap.GT422_Est.replace(0, Tap.GT422_Est.capacity(), RUTGEN.Blancos(1));
          l_MsgGT422.vGT422_Est.add(Tap);
        }
    for (int i=0; i<11; i++)
        {
          vTer Tap = new vTer();
          Tap.GT422_Ter.replace(0, Tap.GT422_Ter.capacity(), RUTGEN.Blancos(1));
          l_MsgGT422.vGT422_Ter.add(Tap);
        }
    for (int i=0; i<11; i++)
        {
          vPav Tap = new vPav();
          Tap.GT422_Pav.replace(0, Tap.GT422_Pav.capacity(), RUTGEN.Blancos(1));
          l_MsgGT422.vGT422_Pav.add(Tap);
        }
    for (int i=0; i<11; i++)
        {
          vCub Tap = new vCub();
          Tap.GT422_Cub.replace(0, Tap.GT422_Cub.capacity(), RUTGEN.Blancos(1));
          l_MsgGT422.vGT422_Cub.add(Tap);
        }
    for (int i=0; i<11; i++)
        {
          vInt Tap = new vInt();
          Tap.GT422_Int.replace(0, Tap.GT422_Int.capacity(), RUTGEN.Blancos(1));
          l_MsgGT422.vGT422_Int.add(Tap);
        }
    l_MsgGT422.GT422_Obt.replace(0, l_MsgGT422.GT422_Obt.capacity(), RUTGEN.Blancos(9  ));
    l_MsgGT422.GT422_Ovb.replace(0, l_MsgGT422.GT422_Ovb.capacity(), RUTGEN.Blancos(9  ));
    l_MsgGT422.GT422_Uso.replace(0, l_MsgGT422.GT422_Uso.capacity(), RUTGEN.Blancos(30 ));
    l_MsgGT422.GT422_Agp.replace(0, l_MsgGT422.GT422_Agp.capacity(), RUTGEN.Blancos(30 ));
    l_MsgGT422.GT422_Tct.replace(0, l_MsgGT422.GT422_Tct.capacity(), RUTGEN.Blancos(30 ));
    l_MsgGT422.GT422_Cld.replace(0, l_MsgGT422.GT422_Cld.capacity(), RUTGEN.Blancos(30 ));
    return l_MsgGT422;
  }
  //---------------------------------------------------------------------------------------
  public static Buf_MsgGT422 LSet_A_MsgGT422(String pcStr)
  {
    Buf_MsgGT422 l_MsgGT422 = new Buf_MsgGT422();
    Vector vDatos = LSet_A_vMsgGT422(pcStr);
    l_MsgGT422 = (Buf_MsgGT422)vDatos.elementAt(0);
    return l_MsgGT422;
  }
  //---------------------------------------------------------------------------------------
  public static Vector LSet_A_vMsgGT422(String pcStr)
  {
    Vector vec = new Vector();
    int p = 0;
    String lcData = "";
    Buf_MsgGT422 l_MsgGT422 = new Buf_MsgGT422();
    l_MsgGT422.GT422_Idr.replace(0, l_MsgGT422.GT422_Idr.capacity(), pcStr.substring(p, p + l_MsgGT422.GT422_Idr.capacity())); p = p + l_MsgGT422.GT422_Idr.capacity();
    l_MsgGT422.GT422_Gti.replace(0, l_MsgGT422.GT422_Gti.capacity(), pcStr.substring(p, p + l_MsgGT422.GT422_Gti.capacity())); p = p + l_MsgGT422.GT422_Gti.capacity();
    l_MsgGT422.GT422_Seq.replace(0, l_MsgGT422.GT422_Seq.capacity(), pcStr.substring(p, p + l_MsgGT422.GT422_Seq.capacity())); p = p + l_MsgGT422.GT422_Seq.capacity();
    l_MsgGT422.GT422_Sqd.replace(0, l_MsgGT422.GT422_Sqd.capacity(), pcStr.substring(p, p + l_MsgGT422.GT422_Sqd.capacity())); p = p + l_MsgGT422.GT422_Sqd.capacity();
    l_MsgGT422.GT422_Fts.replace(0, l_MsgGT422.GT422_Fts.capacity(), pcStr.substring(p, p + l_MsgGT422.GT422_Fts.capacity())); p = p + l_MsgGT422.GT422_Fts.capacity();
    l_MsgGT422.GT422_Vuf.replace(0, l_MsgGT422.GT422_Vuf.capacity(), pcStr.substring(p, p + l_MsgGT422.GT422_Vuf.capacity())); p = p + l_MsgGT422.GT422_Vuf.capacity();
    l_MsgGT422.GT422_Vtc.replace(0, l_MsgGT422.GT422_Vtc.capacity(), pcStr.substring(p, p + l_MsgGT422.GT422_Vtc.capacity())); p = p + l_MsgGT422.GT422_Vtc.capacity();
    l_MsgGT422.GT422_Tuf.replace(0, l_MsgGT422.GT422_Tuf.capacity(), pcStr.substring(p, p + l_MsgGT422.GT422_Tuf.capacity())); p = p + l_MsgGT422.GT422_Tuf.capacity();
    l_MsgGT422.GT422_Ttc.replace(0, l_MsgGT422.GT422_Ttc.capacity(), pcStr.substring(p, p + l_MsgGT422.GT422_Ttc.capacity())); p = p + l_MsgGT422.GT422_Ttc.capacity();
    l_MsgGT422.GT422_Tps.replace(0, l_MsgGT422.GT422_Tps.capacity(), pcStr.substring(p, p + l_MsgGT422.GT422_Tps.capacity())); p = p + l_MsgGT422.GT422_Tps.capacity();
    l_MsgGT422.GT422_Ith.replace(0, l_MsgGT422.GT422_Ith.capacity(), pcStr.substring(p, p + l_MsgGT422.GT422_Ith.capacity())); p = p + l_MsgGT422.GT422_Ith.capacity();
    l_MsgGT422.GT422_Dsh.replace(0, l_MsgGT422.GT422_Dsh.capacity(), pcStr.substring(p, p + l_MsgGT422.GT422_Dsh.capacity())); p = p + l_MsgGT422.GT422_Dsh.capacity();
    l_MsgGT422.GT422_Rs1.replace(0, l_MsgGT422.GT422_Rs1.capacity(), pcStr.substring(p, p + l_MsgGT422.GT422_Rs1.capacity())); p = p + l_MsgGT422.GT422_Rs1.capacity();
    l_MsgGT422.GT422_Rs2.replace(0, l_MsgGT422.GT422_Rs2.capacity(), pcStr.substring(p, p + l_MsgGT422.GT422_Rs2.capacity())); p = p + l_MsgGT422.GT422_Rs2.capacity();
    l_MsgGT422.GT422_Rds.replace(0, l_MsgGT422.GT422_Rds.capacity(), pcStr.substring(p, p + l_MsgGT422.GT422_Rds.capacity())); p = p + l_MsgGT422.GT422_Rds.capacity();
    l_MsgGT422.GT422_Faf.replace(0, l_MsgGT422.GT422_Faf.capacity(), pcStr.substring(p, p + l_MsgGT422.GT422_Faf.capacity())); p = p + l_MsgGT422.GT422_Faf.capacity();
    l_MsgGT422.GT422_Vaf.replace(0, l_MsgGT422.GT422_Vaf.capacity(), pcStr.substring(p, p + l_MsgGT422.GT422_Vaf.capacity())); p = p + l_MsgGT422.GT422_Vaf.capacity();
    l_MsgGT422.GT422_Udd.replace(0, l_MsgGT422.GT422_Udd.capacity(), pcStr.substring(p, p + l_MsgGT422.GT422_Udd.capacity())); p = p + l_MsgGT422.GT422_Udd.capacity();
    l_MsgGT422.GT422_Umd.replace(0, l_MsgGT422.GT422_Umd.capacity(), pcStr.substring(p, p + l_MsgGT422.GT422_Umd.capacity())); p = p + l_MsgGT422.GT422_Umd.capacity();
    l_MsgGT422.GT422_Umt.replace(0, l_MsgGT422.GT422_Umt.capacity(), pcStr.substring(p, p + l_MsgGT422.GT422_Umt.capacity())); p = p + l_MsgGT422.GT422_Umt.capacity();
    l_MsgGT422.GT422_Pum.replace(0, l_MsgGT422.GT422_Pum.capacity(), pcStr.substring(p, p + l_MsgGT422.GT422_Pum.capacity())); p = p + l_MsgGT422.GT422_Pum.capacity();
    l_MsgGT422.GT422_Vtu.replace(0, l_MsgGT422.GT422_Vtu.capacity(), pcStr.substring(p, p + l_MsgGT422.GT422_Vtu.capacity())); p = p + l_MsgGT422.GT422_Vtu.capacity();
    l_MsgGT422.GT422_Vbu.replace(0, l_MsgGT422.GT422_Vbu.capacity(), pcStr.substring(p, p + l_MsgGT422.GT422_Vbu.capacity())); p = p + l_MsgGT422.GT422_Vbu.capacity();
    l_MsgGT422.GT422_Plq.replace(0, l_MsgGT422.GT422_Plq.capacity(), pcStr.substring(p, p + l_MsgGT422.GT422_Plq.capacity())); p = p + l_MsgGT422.GT422_Plq.capacity();
    l_MsgGT422.GT422_Vlu.replace(0, l_MsgGT422.GT422_Vlu.capacity(), pcStr.substring(p, p + l_MsgGT422.GT422_Vlu.capacity())); p = p + l_MsgGT422.GT422_Vlu.capacity();
    l_MsgGT422.GT422_Vsu.replace(0, l_MsgGT422.GT422_Vsu.capacity(), pcStr.substring(p, p + l_MsgGT422.GT422_Vsu.capacity())); p = p + l_MsgGT422.GT422_Vsu.capacity();
    l_MsgGT422.GT422_Ano.replace(0, l_MsgGT422.GT422_Ano.capacity(), pcStr.substring(p, p + l_MsgGT422.GT422_Ano.capacity())); p = p + l_MsgGT422.GT422_Ano.capacity();
    l_MsgGT422.GT422_Nam.replace(0, l_MsgGT422.GT422_Nam.capacity(), pcStr.substring(p, p + l_MsgGT422.GT422_Nam.capacity())); p = p + l_MsgGT422.GT422_Nam.capacity();
    l_MsgGT422.GT422_Ams.replace(0, l_MsgGT422.GT422_Ams.capacity(), pcStr.substring(p, p + l_MsgGT422.GT422_Ams.capacity())); p = p + l_MsgGT422.GT422_Ams.capacity();
    l_MsgGT422.GT422_Amn.replace(0, l_MsgGT422.GT422_Amn.capacity(), pcStr.substring(p, p + l_MsgGT422.GT422_Amn.capacity())); p = p + l_MsgGT422.GT422_Amn.capacity();
    l_MsgGT422.GT422_Ped.replace(0, l_MsgGT422.GT422_Ped.capacity(), pcStr.substring(p, p + l_MsgGT422.GT422_Ped.capacity())); p = p + l_MsgGT422.GT422_Ped.capacity();
    l_MsgGT422.GT422_Fed.replace(0, l_MsgGT422.GT422_Fed.capacity(), pcStr.substring(p, p + l_MsgGT422.GT422_Fed.capacity())); p = p + l_MsgGT422.GT422_Fed.capacity();
    l_MsgGT422.GT422_Rem.replace(0, l_MsgGT422.GT422_Rem.capacity(), pcStr.substring(p, p + l_MsgGT422.GT422_Rem.capacity())); p = p + l_MsgGT422.GT422_Rem.capacity();
    for (int i=0; i<17; i++)
        {
          vPgm Tap = new vPgm();
          Tap.GT422_Pgm.replace(0, Tap.GT422_Pgm.capacity(), pcStr.substring(p, p + Tap.GT422_Pgm.capacity())); p = p + Tap.GT422_Pgm.capacity();
          l_MsgGT422.vGT422_Pgm.add(Tap);
        }
    for (int i=0; i<11; i++)
        {
          vEst Tap = new vEst();
          Tap.GT422_Est.replace(0, Tap.GT422_Est.capacity(), pcStr.substring(p, p + Tap.GT422_Est.capacity())); p = p + Tap.GT422_Est.capacity();
          l_MsgGT422.vGT422_Est.add(Tap);
        }
    for (int i=0; i<11; i++)
        {
          vTer Tap = new vTer();
          Tap.GT422_Ter.replace(0, Tap.GT422_Ter.capacity(), pcStr.substring(p, p + Tap.GT422_Ter.capacity())); p = p + Tap.GT422_Ter.capacity();
          l_MsgGT422.vGT422_Ter.add(Tap);
        }
    for (int i=0; i<11; i++)
        {
          vPav Tap = new vPav();
          Tap.GT422_Pav.replace(0, Tap.GT422_Pav.capacity(), pcStr.substring(p, p + Tap.GT422_Pav.capacity())); p = p + Tap.GT422_Pav.capacity();
          l_MsgGT422.vGT422_Pav.add(Tap);
        }
    for (int i=0; i<11; i++)
        {
          vCub Tap = new vCub();
          Tap.GT422_Cub.replace(0, Tap.GT422_Cub.capacity(), pcStr.substring(p, p + Tap.GT422_Cub.capacity())); p = p + Tap.GT422_Cub.capacity();
          l_MsgGT422.vGT422_Cub.add(Tap);
        }
    for (int i=0; i<11; i++)
        {
          vInt Tap = new vInt();
          Tap.GT422_Int.replace(0, Tap.GT422_Int.capacity(), pcStr.substring(p, p + Tap.GT422_Int.capacity())); p = p + Tap.GT422_Int.capacity();
          l_MsgGT422.vGT422_Int.add(Tap);
        }
    l_MsgGT422.GT422_Obt.replace(0, l_MsgGT422.GT422_Obt.capacity(), pcStr.substring(p, p + l_MsgGT422.GT422_Obt.capacity())); p = p + l_MsgGT422.GT422_Obt.capacity();
    l_MsgGT422.GT422_Ovb.replace(0, l_MsgGT422.GT422_Ovb.capacity(), pcStr.substring(p, p + l_MsgGT422.GT422_Ovb.capacity())); p = p + l_MsgGT422.GT422_Ovb.capacity();
    l_MsgGT422.GT422_Uso.replace(0, l_MsgGT422.GT422_Uso.capacity(), pcStr.substring(p, p + l_MsgGT422.GT422_Uso.capacity())); p = p + l_MsgGT422.GT422_Uso.capacity();
    l_MsgGT422.GT422_Agp.replace(0, l_MsgGT422.GT422_Agp.capacity(), pcStr.substring(p, p + l_MsgGT422.GT422_Agp.capacity())); p = p + l_MsgGT422.GT422_Agp.capacity();
    l_MsgGT422.GT422_Tct.replace(0, l_MsgGT422.GT422_Tct.capacity(), pcStr.substring(p, p + l_MsgGT422.GT422_Tct.capacity())); p = p + l_MsgGT422.GT422_Tct.capacity();
    l_MsgGT422.GT422_Cld.replace(0, l_MsgGT422.GT422_Cld.capacity(), pcStr.substring(p, p + l_MsgGT422.GT422_Cld.capacity())); p = p + l_MsgGT422.GT422_Cld.capacity();
    vec.add(l_MsgGT422);
    return vec;
  }
  //---------------------------------------------------------------------------------------
  public static String LSet_De_MsgGT422 (Buf_MsgGT422 p_MsgGT422)
  {
    String pcStr = "";
    pcStr = pcStr + p_MsgGT422.GT422_Idr.toString();
    pcStr = pcStr + p_MsgGT422.GT422_Gti.toString();
    pcStr = pcStr + p_MsgGT422.GT422_Seq.toString();
    pcStr = pcStr + p_MsgGT422.GT422_Sqd.toString();
    pcStr = pcStr + p_MsgGT422.GT422_Fts.toString();
    pcStr = pcStr + p_MsgGT422.GT422_Vuf.toString();
    pcStr = pcStr + p_MsgGT422.GT422_Vtc.toString();
    pcStr = pcStr + p_MsgGT422.GT422_Tuf.toString();
    pcStr = pcStr + p_MsgGT422.GT422_Ttc.toString();
    pcStr = pcStr + p_MsgGT422.GT422_Tps.toString();
    pcStr = pcStr + p_MsgGT422.GT422_Ith.toString();
    pcStr = pcStr + p_MsgGT422.GT422_Dsh.toString();
    pcStr = pcStr + p_MsgGT422.GT422_Rs1.toString();
    pcStr = pcStr + p_MsgGT422.GT422_Rs2.toString();
    pcStr = pcStr + p_MsgGT422.GT422_Rds.toString();
    pcStr = pcStr + p_MsgGT422.GT422_Faf.toString();
    pcStr = pcStr + p_MsgGT422.GT422_Vaf.toString();
    pcStr = pcStr + p_MsgGT422.GT422_Udd.toString();
    pcStr = pcStr + p_MsgGT422.GT422_Umd.toString();
    pcStr = pcStr + p_MsgGT422.GT422_Umt.toString();
    pcStr = pcStr + p_MsgGT422.GT422_Pum.toString();
    pcStr = pcStr + p_MsgGT422.GT422_Vtu.toString();
    pcStr = pcStr + p_MsgGT422.GT422_Vbu.toString();
    pcStr = pcStr + p_MsgGT422.GT422_Plq.toString();
    pcStr = pcStr + p_MsgGT422.GT422_Vlu.toString();
    pcStr = pcStr + p_MsgGT422.GT422_Vsu.toString();
    pcStr = pcStr + p_MsgGT422.GT422_Ano.toString();
    pcStr = pcStr + p_MsgGT422.GT422_Nam.toString();
    pcStr = pcStr + p_MsgGT422.GT422_Ams.toString();
    pcStr = pcStr + p_MsgGT422.GT422_Amn.toString();
    pcStr = pcStr + p_MsgGT422.GT422_Ped.toString();
    pcStr = pcStr + p_MsgGT422.GT422_Fed.toString();
    pcStr = pcStr + p_MsgGT422.GT422_Rem.toString();
    for (int i=0; i<17; i++)
        { pcStr = pcStr + ((vPgm)p_MsgGT422.vGT422_Pgm.elementAt(i)).GT422_Pgm.toString(); }
    for (int i=0; i<11; i++)
        { pcStr = pcStr + ((vEst)p_MsgGT422.vGT422_Est.elementAt(i)).GT422_Est.toString(); }
    for (int i=0; i<11; i++)
        { pcStr = pcStr + ((vTer)p_MsgGT422.vGT422_Ter.elementAt(i)).GT422_Ter.toString(); }
    for (int i=0; i<11; i++)
        { pcStr = pcStr + ((vPav)p_MsgGT422.vGT422_Pav.elementAt(i)).GT422_Pav.toString(); }
    for (int i=0; i<11; i++)
        { pcStr = pcStr + ((vCub)p_MsgGT422.vGT422_Cub.elementAt(i)).GT422_Cub.toString(); }
    for (int i=0; i<11; i++)
        { pcStr = pcStr + ((vInt)p_MsgGT422.vGT422_Int.elementAt(i)).GT422_Int.toString(); }
    pcStr = pcStr + p_MsgGT422.GT422_Obt.toString();
    pcStr = pcStr + p_MsgGT422.GT422_Ovb.toString();
    pcStr = pcStr + p_MsgGT422.GT422_Uso.toString();
    pcStr = pcStr + p_MsgGT422.GT422_Agp.toString();
    pcStr = pcStr + p_MsgGT422.GT422_Tct.toString();
    pcStr = pcStr + p_MsgGT422.GT422_Cld.toString();
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
}