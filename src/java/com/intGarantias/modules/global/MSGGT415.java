// Source File Name:   MSGGT415.java

package com.intGarantias.modules.global;

import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.util.Formateo;

import java.util.*;
import java.lang.*;
import org.apache.turbine.util.Log;

public class MSGGT415
{
  //---------------------------------------------------------------------------------------
  public static class Grt_Gbs_Det
  {
    public StringBuffer GT415_Tpv = new StringBuffer(1 );       //X(01)        1 Tipo Valor (F/O)
    public StringBuffer GT415_Ntd = new StringBuffer(15);       //X(15)       16 Nombre Tipo Documento
    public StringBuffer GT415_Tdo = new StringBuffer(2 );       //9(02)       18 Tipo Documento
    public StringBuffer GT415_Nbc = new StringBuffer(15);       //X(15)       33 Nombre Banco
    public StringBuffer GT415_Bco = new StringBuffer(3 );       //9(03)       36 Codigo Banco
    public StringBuffer GT415_Ser = new StringBuffer(8 );       //X(08)       44 Serie Documento
    public StringBuffer GT415_Nro = new StringBuffer(12);       //X(12)       56 Numero Documento
    public StringBuffer GT415_Fem = new StringBuffer(8 );       //9(08)       64 Fecha Emision
    public StringBuffer GT415_Fvt = new StringBuffer(8 );       //9(08)       72 Fecha Vencimiento
    public StringBuffer GT415_Mnd = new StringBuffer(3 );       //9(03)       75 Moneda
    public StringBuffer GT415_Trj = new StringBuffer(3 );       //X(03)       78 Reajustabilidad
    public StringBuffer GT415_Vln = new StringBuffer(15);       //9(11)V9(4)  93 Valor Nominal
    public StringBuffer GT415_Vlc = new StringBuffer(15);       //9(13)V9(2) 108 Valor Contable
    public StringBuffer GT415_Dcm = new StringBuffer(1 );       //9(01)      109 Decimales
    public StringBuffer GT415_Nbe = new StringBuffer(40);       //X(40)      149 Beneficiario
    public StringBuffer GT415_Itb = new StringBuffer(2  );      //X(02)      151 Id Tipo Bien
    public StringBuffer GT415_Itg = new StringBuffer(5  );      //X(05)      156 Id Tipo Gtia
    public StringBuffer GT415_Dpc = new StringBuffer(10 );      //X(10)      166 PContable
    public StringBuffer GT415_Csv = new StringBuffer(3  );      //X(03)      169 Conservador
    public StringBuffer GT415_Lcl = new StringBuffer(15 );      //X(15)      184 Localidad
    public StringBuffer GT415_Rpo = new StringBuffer(4  );      //9(04)      188 Repertorio
    public StringBuffer GT415_Foj = new StringBuffer(7  );      //9(07)      195 Fojas
    public StringBuffer GT415_Nin = new StringBuffer(7  );      //9(07)      202 Numero
    public StringBuffer GT415_Vsg = new StringBuffer(15 );      //9(11)V9(4) 217 VAsegurable       
    public StringBuffer GT415_Csg = new StringBuffer(3  );      //9(03)      220 Aseguradora       
    public StringBuffer GT415_Spz = new StringBuffer(10 );      //X(10)      230 Poliza            
    public StringBuffer GT415_Sfv = new StringBuffer(8  );      //9(08)      238 FVcto Seguro      
    public StringBuffer GT415_Sum = new StringBuffer(1  );      //X(01)      239 UMon Seguro       
    public StringBuffer GT415_Svl = new StringBuffer(15 );      //9(11)V9(4) 254 VSeguro <UF>      

    public String getTpv()         {return GT415_Tpv.toString();}
    public String getNtd()         {return GT415_Ntd.toString();}
    public String getTdo()         {return GT415_Tdo.toString();}
    public String getNbc()         {return GT415_Nbc.toString();}
    public String getBco()         {return GT415_Bco.toString();}
    public String getSer()         {return GT415_Ser.toString();}
    public String getNro()         {return GT415_Nro.toString();}
    public String getFem()         {return GT415_Fem.toString();}
    public String getFvt()         {return GT415_Fvt.toString();}
    public String getMnd()         {return GT415_Mnd.toString();}
    public String getTrj()         {return GT415_Trj.toString();}
    public String getVln()         {return GT415_Vln.toString();}
    public String getVlc()         {return GT415_Vlc.toString();}
    public String getDcm()         {return GT415_Dcm.toString();}
    public String getNbe()         {return GT415_Nbe.toString();}
    public String getItb()         {return GT415_Itb.toString();}
    public String getItg()         {return GT415_Itg.toString();}
    public String getDpc()         {return GT415_Dpc.toString();}
    public String getCsv()         {return GT415_Csv.toString();}
    public String getLcl()         {return GT415_Lcl.toString();}
    public String getRpo()         {return GT415_Rpo.toString();}
    public String getFoj()         {return GT415_Foj.toString();}
    public String getNin()         {return GT415_Nin.toString();}
    public String getVsg()         {return GT415_Vsg.toString();}
    public String getCsg()         {return GT415_Csg.toString();}
    public String getSpz()         {return GT415_Spz.toString();}
    public String getSfv()         {return GT415_Sfv.toString();}
    public String getSum()         {return GT415_Sum.toString();}
    public String getSvl()         {return GT415_Svl.toString();}

    public Formateo f = new Formateo();
    public String fgetFem()        {return f.FormatDate(GT415_Fem.toString());}
    public String fgetFvt()        {return f.FormatDate(GT415_Fvt.toString());}
    public String fgetVln()        {return f.Formato(GT415_Vln.toString(),"99.999.999.999",4,Integer.parseInt(GT415_Dcm.toString()),',');}
    public String fgetVlc()        {return f.Formato(GT415_Vlc.toString(),"9.999.999.999.999",2,2,',');}
    public String fgetSfv()        {return f.FormatDate(GT415_Sfv.toString());}
    public String fgetVsg()        {return f.Formato(GT415_Vsg.toString(),"9.999.999.999.999",4,4,',');}
    public String fgetSvl()        {return f.Formato(GT415_Svl.toString(),"9.999.999.999.999",4,4,',');}
  }
  //---------------------------------------------------------------------------------------
  public static class Buf_MsgGT415
  {
    public StringBuffer GT415_Idr = new StringBuffer(1) ;       //X(01)        1 Idr Host
    public StringBuffer GT415_Gti = new StringBuffer(10);       //X(10)       11 Id. Garantia
    public StringBuffer GT415_Seq = new StringBuffer(3) ;       //9(03)       14 Total Indices
    public Grt_Gbs_Det  GT415_Gbs = new Grt_Gbs_Det();          //X(254)     268 Data Detalle

    public String getIdr()         {return GT415_Idr.toString();}
    public String getGti()         {return GT415_Gti.toString();}
    public String getSeq()         {return GT415_Seq.toString();}
    public Grt_Gbs_Det getGbs()    {return GT415_Gbs;}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_MsgGT415 Inicia_MsgGT415()
  {
    Buf_MsgGT415 l_MsgGT415 = new Buf_MsgGT415();
    l_MsgGT415.GT415_Idr.replace(0, l_MsgGT415.GT415_Idr.capacity(), RUTGEN.Blancos(1));
    l_MsgGT415.GT415_Gti.replace(0, l_MsgGT415.GT415_Gti.capacity(), RUTGEN.Blancos(10));
    l_MsgGT415.GT415_Seq.replace(0, l_MsgGT415.GT415_Seq.capacity(), RUTGEN.Blancos(3));
    Grt_Gbs_Det Gbs = new Grt_Gbs_Det();
    Gbs.GT415_Tpv.replace(0, Gbs.GT415_Tpv.capacity(), RUTGEN.Blancos(1  ));
    Gbs.GT415_Ntd.replace(0, Gbs.GT415_Ntd.capacity(), RUTGEN.Blancos(15 ));
    Gbs.GT415_Tdo.replace(0, Gbs.GT415_Tdo.capacity(), RUTGEN.Blancos(2  ));
    Gbs.GT415_Nbc.replace(0, Gbs.GT415_Nbc.capacity(), RUTGEN.Blancos(15 ));
    Gbs.GT415_Bco.replace(0, Gbs.GT415_Bco.capacity(), RUTGEN.Blancos(3  ));
    Gbs.GT415_Ser.replace(0, Gbs.GT415_Ser.capacity(), RUTGEN.Blancos(8  ));
    Gbs.GT415_Nro.replace(0, Gbs.GT415_Nro.capacity(), RUTGEN.Blancos(12 ));
    Gbs.GT415_Fem.replace(0, Gbs.GT415_Fem.capacity(), RUTGEN.Blancos(8  ));
    Gbs.GT415_Fvt.replace(0, Gbs.GT415_Fvt.capacity(), RUTGEN.Blancos(8  ));
    Gbs.GT415_Mnd.replace(0, Gbs.GT415_Mnd.capacity(), RUTGEN.Blancos(3  ));
    Gbs.GT415_Trj.replace(0, Gbs.GT415_Trj.capacity(), RUTGEN.Blancos(3  ));
    Gbs.GT415_Vln.replace(0, Gbs.GT415_Vln.capacity(), RUTGEN.Blancos(15 ));
    Gbs.GT415_Vlc.replace(0, Gbs.GT415_Vlc.capacity(), RUTGEN.Blancos(15 ));
    Gbs.GT415_Dcm.replace(0, Gbs.GT415_Dcm.capacity(), RUTGEN.Blancos(1  ));
    Gbs.GT415_Nbe.replace(0, Gbs.GT415_Nbe.capacity(), RUTGEN.Blancos(40 ));
    Gbs.GT415_Itb.replace(0, Gbs.GT415_Itb.capacity(), RUTGEN.Blancos(2  ));
    Gbs.GT415_Itg.replace(0, Gbs.GT415_Itg.capacity(), RUTGEN.Blancos(5  ));
    Gbs.GT415_Dpc.replace(0, Gbs.GT415_Dpc.capacity(), RUTGEN.Blancos(10 ));
    Gbs.GT415_Csv.replace(0, Gbs.GT415_Csv.capacity(), RUTGEN.Blancos(3  ));
    Gbs.GT415_Lcl.replace(0, Gbs.GT415_Lcl.capacity(), RUTGEN.Blancos(15 ));
    Gbs.GT415_Rpo.replace(0, Gbs.GT415_Rpo.capacity(), RUTGEN.Blancos(4  ));
    Gbs.GT415_Foj.replace(0, Gbs.GT415_Foj.capacity(), RUTGEN.Blancos(7  ));
    Gbs.GT415_Nin.replace(0, Gbs.GT415_Nin.capacity(), RUTGEN.Blancos(7  ));
    Gbs.GT415_Vsg.replace(0, Gbs.GT415_Vsg.capacity(), RUTGEN.Blancos(15 ));
    Gbs.GT415_Csg.replace(0, Gbs.GT415_Csg.capacity(), RUTGEN.Blancos(3  ));
    Gbs.GT415_Spz.replace(0, Gbs.GT415_Spz.capacity(), RUTGEN.Blancos(10 ));
    Gbs.GT415_Sfv.replace(0, Gbs.GT415_Sfv.capacity(), RUTGEN.Blancos(8  ));
    Gbs.GT415_Sum.replace(0, Gbs.GT415_Sum.capacity(), RUTGEN.Blancos(1  ));
    Gbs.GT415_Svl.replace(0, Gbs.GT415_Svl.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT415.GT415_Gbs = Gbs;
    return l_MsgGT415;
  }
  //---------------------------------------------------------------------------------------
  static public Buf_MsgGT415 LSet_A_MsgGT415(String pcStr)
  {
    Buf_MsgGT415 l_MsgGT415 = new Buf_MsgGT415();
    MSGGT415 vMsgGT415 = new MSGGT415();
    Vector vDatos = vMsgGT415.LSet_A_vMsgGT415(pcStr);
    l_MsgGT415 = (MSGGT415.Buf_MsgGT415)vDatos.elementAt(0);
    return l_MsgGT415;
  }
  //---------------------------------------------------------------------------------------
  public Vector LSet_A_vMsgGT415(String pcStr)
  {
    int p = 0;
    Vector vec = new Vector();
    Buf_MsgGT415 l_MsgGT415 = new Buf_MsgGT415();
    l_MsgGT415.GT415_Idr.append(pcStr.substring(p, p + l_MsgGT415.GT415_Idr.capacity())); p = p + l_MsgGT415.GT415_Idr.capacity();
    l_MsgGT415.GT415_Gti.append(pcStr.substring(p, p + l_MsgGT415.GT415_Gti.capacity())); p = p + l_MsgGT415.GT415_Gti.capacity();
    l_MsgGT415.GT415_Seq.append(pcStr.substring(p, p + l_MsgGT415.GT415_Seq.capacity())); p = p + l_MsgGT415.GT415_Seq.capacity();
    Grt_Gbs_Det Gbs = new Grt_Gbs_Det();
    Gbs.GT415_Tpv.append(pcStr.substring(p, p + Gbs.GT415_Tpv.capacity())); p = p + Gbs.GT415_Tpv.capacity();
    Gbs.GT415_Ntd.append(pcStr.substring(p, p + Gbs.GT415_Ntd.capacity())); p = p + Gbs.GT415_Ntd.capacity();
    Gbs.GT415_Tdo.append(pcStr.substring(p, p + Gbs.GT415_Tdo.capacity())); p = p + Gbs.GT415_Tdo.capacity();
    Gbs.GT415_Nbc.append(pcStr.substring(p, p + Gbs.GT415_Nbc.capacity())); p = p + Gbs.GT415_Nbc.capacity();
    Gbs.GT415_Bco.append(pcStr.substring(p, p + Gbs.GT415_Bco.capacity())); p = p + Gbs.GT415_Bco.capacity();
    Gbs.GT415_Ser.append(pcStr.substring(p, p + Gbs.GT415_Ser.capacity())); p = p + Gbs.GT415_Ser.capacity();
    Gbs.GT415_Nro.append(pcStr.substring(p, p + Gbs.GT415_Nro.capacity())); p = p + Gbs.GT415_Nro.capacity();
    Gbs.GT415_Fem.append(pcStr.substring(p, p + Gbs.GT415_Fem.capacity())); p = p + Gbs.GT415_Fem.capacity();
    Gbs.GT415_Fvt.append(pcStr.substring(p, p + Gbs.GT415_Fvt.capacity())); p = p + Gbs.GT415_Fvt.capacity();
    Gbs.GT415_Mnd.append(pcStr.substring(p, p + Gbs.GT415_Mnd.capacity())); p = p + Gbs.GT415_Mnd.capacity();
    Gbs.GT415_Trj.append(pcStr.substring(p, p + Gbs.GT415_Trj.capacity())); p = p + Gbs.GT415_Trj.capacity();
    Gbs.GT415_Vln.append(pcStr.substring(p, p + Gbs.GT415_Vln.capacity())); p = p + Gbs.GT415_Vln.capacity();
    Gbs.GT415_Vlc.append(pcStr.substring(p, p + Gbs.GT415_Vlc.capacity())); p = p + Gbs.GT415_Vlc.capacity();
    Gbs.GT415_Dcm.append(pcStr.substring(p, p + Gbs.GT415_Dcm.capacity())); p = p + Gbs.GT415_Dcm.capacity();
    Gbs.GT415_Nbe.append(pcStr.substring(p, p + Gbs.GT415_Nbe.capacity())); p = p + Gbs.GT415_Nbe.capacity();
    Gbs.GT415_Itb.append(pcStr.substring(p, p + Gbs.GT415_Itb.capacity())); p = p + Gbs.GT415_Itb.capacity();
    Gbs.GT415_Itg.append(pcStr.substring(p, p + Gbs.GT415_Itg.capacity())); p = p + Gbs.GT415_Itg.capacity();
    Gbs.GT415_Dpc.append(pcStr.substring(p, p + Gbs.GT415_Dpc.capacity())); p = p + Gbs.GT415_Dpc.capacity();
    Gbs.GT415_Csv.append(pcStr.substring(p, p + Gbs.GT415_Csv.capacity())); p = p + Gbs.GT415_Csv.capacity();
    Gbs.GT415_Lcl.append(pcStr.substring(p, p + Gbs.GT415_Lcl.capacity())); p = p + Gbs.GT415_Lcl.capacity();
    Gbs.GT415_Rpo.append(pcStr.substring(p, p + Gbs.GT415_Rpo.capacity())); p = p + Gbs.GT415_Rpo.capacity();
    Gbs.GT415_Foj.append(pcStr.substring(p, p + Gbs.GT415_Foj.capacity())); p = p + Gbs.GT415_Foj.capacity();
    Gbs.GT415_Nin.append(pcStr.substring(p, p + Gbs.GT415_Nin.capacity())); p = p + Gbs.GT415_Nin.capacity();
    Gbs.GT415_Vsg.append(pcStr.substring(p, p + Gbs.GT415_Vsg.capacity())); p = p + Gbs.GT415_Vsg.capacity();
    Gbs.GT415_Csg.append(pcStr.substring(p, p + Gbs.GT415_Csg.capacity())); p = p + Gbs.GT415_Csg.capacity();
    Gbs.GT415_Spz.append(pcStr.substring(p, p + Gbs.GT415_Spz.capacity())); p = p + Gbs.GT415_Spz.capacity();
    Gbs.GT415_Sfv.append(pcStr.substring(p, p + Gbs.GT415_Sfv.capacity())); p = p + Gbs.GT415_Sfv.capacity();
    Gbs.GT415_Sum.append(pcStr.substring(p, p + Gbs.GT415_Sum.capacity())); p = p + Gbs.GT415_Sum.capacity();
    Gbs.GT415_Svl.append(pcStr.substring(p, p + Gbs.GT415_Svl.capacity())); p = p + Gbs.GT415_Svl.capacity();
    l_MsgGT415.GT415_Gbs = Gbs;
    vec.add(l_MsgGT415);
    return vec;
  }
  //---------------------------------------------------------------------------------------
  static public String LSet_De_MsgGT415(Buf_MsgGT415 p_MsgGT415)
  {
    String pcStr = "";
    pcStr = pcStr + p_MsgGT415.GT415_Idr.toString();
    pcStr = pcStr + p_MsgGT415.GT415_Gti.toString();
    pcStr = pcStr + p_MsgGT415.GT415_Seq.toString();
    Grt_Gbs_Det Gbs = new Grt_Gbs_Det();
    Gbs = p_MsgGT415.GT415_Gbs;
    pcStr = pcStr + Gbs.GT415_Tpv.toString();
    pcStr = pcStr + Gbs.GT415_Ntd.toString();
    pcStr = pcStr + Gbs.GT415_Tdo.toString();
    pcStr = pcStr + Gbs.GT415_Nbc.toString();
    pcStr = pcStr + Gbs.GT415_Bco.toString();
    pcStr = pcStr + Gbs.GT415_Ser.toString();
    pcStr = pcStr + Gbs.GT415_Nro.toString();
    pcStr = pcStr + Gbs.GT415_Fem.toString();
    pcStr = pcStr + Gbs.GT415_Fvt.toString();
    pcStr = pcStr + Gbs.GT415_Mnd.toString();
    pcStr = pcStr + Gbs.GT415_Trj.toString();
    pcStr = pcStr + Gbs.GT415_Vln.toString();
    pcStr = pcStr + Gbs.GT415_Vlc.toString();
    pcStr = pcStr + Gbs.GT415_Dcm.toString();
    pcStr = pcStr + Gbs.GT415_Nbe.toString();
    pcStr = pcStr + Gbs.GT415_Itb.toString();
    pcStr = pcStr + Gbs.GT415_Itg.toString();
    pcStr = pcStr + Gbs.GT415_Dpc.toString();
    pcStr = pcStr + Gbs.GT415_Csv.toString();
    pcStr = pcStr + Gbs.GT415_Lcl.toString();
    pcStr = pcStr + Gbs.GT415_Rpo.toString();
    pcStr = pcStr + Gbs.GT415_Foj.toString();
    pcStr = pcStr + Gbs.GT415_Nin.toString();
    pcStr = pcStr + Gbs.GT415_Vsg.toString();
    pcStr = pcStr + Gbs.GT415_Csg.toString();
    pcStr = pcStr + Gbs.GT415_Spz.toString();
    pcStr = pcStr + Gbs.GT415_Sfv.toString();
    pcStr = pcStr + Gbs.GT415_Sum.toString();
    pcStr = pcStr + Gbs.GT415_Svl.toString();
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
}