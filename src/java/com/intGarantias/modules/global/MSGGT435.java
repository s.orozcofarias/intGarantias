// Source File Name:   MSGGT435.java

package com.intGarantias.modules.global;

import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.util.Formateo;

import java.util.*;
import java.lang.*;
import org.apache.turbine.util.Log;

public class MSGGT435
{
  //---------------------------------------------------------------------------------------
  public static class Buf_MsgGT435
  {
    public StringBuffer GT435_Idr = new StringBuffer(1  );      //X(01)        1 Inicio/Especifico/Avanza/Retrocede
    public StringBuffer GT435_Gti = new StringBuffer(10 );      //X(10)       11 Operacion a Buscar
    public StringBuffer GT435_Swt = new StringBuffer(1  );      //9(01)       12 Retorno
    public StringBuffer GT435_Men = new StringBuffer(8  );      //X(08)       20 MensajeError
    public StringBuffer GT435_Cli = new StringBuffer(10 );      //X(10)       30 Id.Cliente
    public StringBuffer GT435_Ncl = new StringBuffer(40 );      //X(40)       70 Nom.Cliente
    public StringBuffer GT435_Dcn = new StringBuffer(5  );      //X(05)       75 Producto
    public StringBuffer GT435_Ndc = new StringBuffer(30 );      //X(30)      105 Desc.Producto
    public StringBuffer GT435_Suc = new StringBuffer(3  );      //9(03)      108 Sucursal
    public StringBuffer GT435_Nsu = new StringBuffer(15 );      //X(15)      123 Nom.Sucursal
    public StringBuffer GT435_Eje = new StringBuffer(4  );      //9(04)      127 Ejecutivo
    public StringBuffer GT435_Nej = new StringBuffer(25 );      //X(25)      152 Nom.Ejecutivo
    public StringBuffer GT435_Tej = new StringBuffer(10 );      //X(10)      162 Fono.Ejecutivo
    public StringBuffer GT435_Tmn = new StringBuffer(3  );      //X(03)      165 Tipo Moneda
    public StringBuffer GT435_Trj = new StringBuffer(3  );      //X(03)      168 Reajustabilidad
    public StringBuffer GT435_Dcm = new StringBuffer(1  );      //9(01)      169 Decimales
    public StringBuffer GT435_Fco = new StringBuffer(8  );      //9(08)      177 Fecha Apertura
    public StringBuffer GT435_Mnd = new StringBuffer(3  );      //9(03)      180 Moneda
    public StringBuffer GT435_Nmn = new StringBuffer(15 );      //X(15)      195 Nom.Moneda
    public StringBuffer GT435_Vco = new StringBuffer(15 );      //9(11)V9(4) 210 Valor
    public StringBuffer GT435_Vcn = new StringBuffer(15 );      //9(13)V9(2) 225 Valor Actualizado
    public StringBuffer GT435_Fcn = new StringBuffer(8  );      //9(08)      233 Fecha Actualiza
    public StringBuffer GT435_Cbt = new StringBuffer(1  );      //X(01)      234 Cobertura
    public StringBuffer GT435_Est = new StringBuffer(5  );      //X(05)      239 Estado
    public StringBuffer GT435_Dpc = new StringBuffer(10 );      //X(10)      249 PContable
    public StringBuffer GT435_Dir = new StringBuffer(107);      //X(107)     356 Direccion Cliente
    public StringBuffer GT435_Tfc = new StringBuffer(10 );      //X(10)      366 Telefono Cliente
    public StringBuffer GT435_Cmn = new StringBuffer(11 );      //9(11)      377 Comuna Cliente
    public StringBuffer GT435_Ggr = new StringBuffer(3  );      //X(03)      380 Grupo Garantias
    public StringBuffer GT435_Itg = new StringBuffer(2  );      //X(02)      382 Idr.Tipo Garantia
    public StringBuffer GT435_Tpr = new StringBuffer(1  );      //X(01)      383 Cobertura
    public StringBuffer GT435_Ttr = new StringBuffer(5  );      //X(05)      388 CodEvento
    public StringBuffer GT435_Fte = new StringBuffer(3  );      //X(03)      391 Fuente (TSD/otro)
    public StringBuffer GT435_Tsd = new StringBuffer(3  );      //9(03)      394 Tasador
    public StringBuffer GT435_Dsg = new StringBuffer(100);      //X(100)     494 Descripcion Garantia

    public String getIdr()         {return GT435_Idr.toString();}
    public String getGti()         {return GT435_Gti.toString();}
    public String getSwt()         {return GT435_Swt.toString();}
    public String getMen()         {return GT435_Men.toString();}
    public String getCli()         {return GT435_Cli.toString();}
    public String getNcl()         {return GT435_Ncl.toString();}
    public String getDcn()         {return GT435_Dcn.toString();}
    public String getNdc()         {return GT435_Ndc.toString();}
    public String getSuc()         {return GT435_Suc.toString();}
    public String getNsu()         {return GT435_Nsu.toString();}
    public String getEje()         {return GT435_Eje.toString();}
    public String getNej()         {return GT435_Nej.toString();}
    public String getTej()         {return GT435_Tej.toString();}
    public String getTmn()         {return GT435_Tmn.toString();}
    public String getTrj()         {return GT435_Trj.toString();}
    public String getDcm()         {return GT435_Dcm.toString();}
    public String getFco()         {return GT435_Fco.toString();}
    public String getMnd()         {return GT435_Mnd.toString();}
    public String getNmn()         {return GT435_Nmn.toString();}
    public String getVco()         {return GT435_Vco.toString();}
    public String getVcn()         {return GT435_Vcn.toString();}
    public String getFcn()         {return GT435_Fcn.toString();}
    public String getCbt()         {return GT435_Cbt.toString();}
    public String getEst()         {return GT435_Est.toString();}
    public String getDpc()         {return GT435_Dpc.toString();}
    public String getDir()         {return GT435_Dir.toString();}
    public String getTfc()         {return GT435_Tfc.toString();}
    public String getCmn()         {return GT435_Cmn.toString();}
    public String getGgr()         {return GT435_Ggr.toString();}
    public String getItg()         {return GT435_Itg.toString();}
    public String getTpr()         {return GT435_Tpr.toString();}
    public String getTtr()         {return GT435_Ttr.toString();}
    public String getFte()         {return GT435_Fte.toString();}
    public String getTsd()         {return GT435_Tsd.toString();}
    public String getDsg()         {return GT435_Dsg.toString();}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_MsgGT435 Inicia_MsgGT435()
  {
    Buf_MsgGT435 l_MsgGT435 = new Buf_MsgGT435();
    l_MsgGT435.GT435_Idr.replace(0,l_MsgGT435.GT435_Idr.capacity(), RUTGEN.Blancos(1  ));
    l_MsgGT435.GT435_Gti.replace(0,l_MsgGT435.GT435_Gti.capacity(), RUTGEN.Blancos(10 ));
    l_MsgGT435.GT435_Swt.replace(0,l_MsgGT435.GT435_Swt.capacity(), RUTGEN.Blancos(1  ));
    l_MsgGT435.GT435_Men.replace(0,l_MsgGT435.GT435_Men.capacity(), RUTGEN.Blancos(8  ));
    l_MsgGT435.GT435_Cli.replace(0,l_MsgGT435.GT435_Cli.capacity(), RUTGEN.Blancos(10 ));
    l_MsgGT435.GT435_Ncl.replace(0,l_MsgGT435.GT435_Ncl.capacity(), RUTGEN.Blancos(40 ));
    l_MsgGT435.GT435_Dcn.replace(0,l_MsgGT435.GT435_Dcn.capacity(), RUTGEN.Blancos(5  ));
    l_MsgGT435.GT435_Ndc.replace(0,l_MsgGT435.GT435_Ndc.capacity(), RUTGEN.Blancos(30 ));
    l_MsgGT435.GT435_Suc.replace(0,l_MsgGT435.GT435_Suc.capacity(), RUTGEN.Blancos(3  ));
    l_MsgGT435.GT435_Nsu.replace(0,l_MsgGT435.GT435_Nsu.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT435.GT435_Eje.replace(0,l_MsgGT435.GT435_Eje.capacity(), RUTGEN.Blancos(4  ));
    l_MsgGT435.GT435_Nej.replace(0,l_MsgGT435.GT435_Nej.capacity(), RUTGEN.Blancos(25 ));
    l_MsgGT435.GT435_Tej.replace(0,l_MsgGT435.GT435_Tej.capacity(), RUTGEN.Blancos(10 ));
    l_MsgGT435.GT435_Tmn.replace(0,l_MsgGT435.GT435_Tmn.capacity(), RUTGEN.Blancos(3  ));
    l_MsgGT435.GT435_Trj.replace(0,l_MsgGT435.GT435_Trj.capacity(), RUTGEN.Blancos(3  ));
    l_MsgGT435.GT435_Dcm.replace(0,l_MsgGT435.GT435_Dcm.capacity(), RUTGEN.Blancos(1  ));
    l_MsgGT435.GT435_Fco.replace(0,l_MsgGT435.GT435_Fco.capacity(), RUTGEN.Blancos(8  ));
    l_MsgGT435.GT435_Mnd.replace(0,l_MsgGT435.GT435_Mnd.capacity(), RUTGEN.Blancos(3  ));
    l_MsgGT435.GT435_Nmn.replace(0,l_MsgGT435.GT435_Nmn.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT435.GT435_Vco.replace(0,l_MsgGT435.GT435_Vco.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT435.GT435_Vcn.replace(0,l_MsgGT435.GT435_Vcn.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT435.GT435_Fcn.replace(0,l_MsgGT435.GT435_Fcn.capacity(), RUTGEN.Blancos(8  ));
    l_MsgGT435.GT435_Cbt.replace(0,l_MsgGT435.GT435_Cbt.capacity(), RUTGEN.Blancos(1  ));
    l_MsgGT435.GT435_Est.replace(0,l_MsgGT435.GT435_Est.capacity(), RUTGEN.Blancos(5  ));
    l_MsgGT435.GT435_Dpc.replace(0,l_MsgGT435.GT435_Dpc.capacity(), RUTGEN.Blancos(10 ));
    l_MsgGT435.GT435_Dir.replace(0,l_MsgGT435.GT435_Dir.capacity(), RUTGEN.Blancos(107));
    l_MsgGT435.GT435_Tfc.replace(0,l_MsgGT435.GT435_Tfc.capacity(), RUTGEN.Blancos(10 ));
    l_MsgGT435.GT435_Cmn.replace(0,l_MsgGT435.GT435_Cmn.capacity(), RUTGEN.Blancos(11 ));
    l_MsgGT435.GT435_Ggr.replace(0,l_MsgGT435.GT435_Ggr.capacity(), RUTGEN.Blancos(3  ));
    l_MsgGT435.GT435_Itg.replace(0,l_MsgGT435.GT435_Itg.capacity(), RUTGEN.Blancos(2  ));
    l_MsgGT435.GT435_Tpr.replace(0,l_MsgGT435.GT435_Tpr.capacity(), RUTGEN.Blancos(1  ));
    l_MsgGT435.GT435_Ttr.replace(0,l_MsgGT435.GT435_Ttr.capacity(), RUTGEN.Blancos(5  ));
    l_MsgGT435.GT435_Fte.replace(0,l_MsgGT435.GT435_Fte.capacity(), RUTGEN.Blancos(3  ));
    l_MsgGT435.GT435_Tsd.replace(0,l_MsgGT435.GT435_Tsd.capacity(), RUTGEN.Blancos(3  ));
    l_MsgGT435.GT435_Dsg.replace(0,l_MsgGT435.GT435_Dsg.capacity(), RUTGEN.Blancos(100));
    return l_MsgGT435;
  }
  //---------------------------------------------------------------------------------------
  static public Buf_MsgGT435 LSet_A_MsgGT435(String pcStr)
  {
    Buf_MsgGT435 l_MsgGT435 = new Buf_MsgGT435();
    MSGGT435 vMsgGT435 = new MSGGT435();
    Vector vDatos = vMsgGT435.LSet_A_vMsgGT435(pcStr);
    l_MsgGT435 = (MSGGT435.Buf_MsgGT435)vDatos.elementAt(0);
    return l_MsgGT435;
  }
  //---------------------------------------------------------------------------------------
  public Vector LSet_A_vMsgGT435(String pcStr)
  {
    Vector vec = new Vector();
    int p = 0;
    Buf_MsgGT435 l_MsgGT435 = new Buf_MsgGT435();
    l_MsgGT435.GT435_Idr.replace(0, l_MsgGT435.GT435_Idr.capacity(), pcStr.substring(p, p + l_MsgGT435.GT435_Idr.capacity())); p = p + l_MsgGT435.GT435_Idr.capacity();
    l_MsgGT435.GT435_Gti.replace(0, l_MsgGT435.GT435_Gti.capacity(), pcStr.substring(p, p + l_MsgGT435.GT435_Gti.capacity())); p = p + l_MsgGT435.GT435_Gti.capacity();
    l_MsgGT435.GT435_Swt.replace(0, l_MsgGT435.GT435_Swt.capacity(), pcStr.substring(p, p + l_MsgGT435.GT435_Swt.capacity())); p = p + l_MsgGT435.GT435_Swt.capacity();
    l_MsgGT435.GT435_Men.replace(0, l_MsgGT435.GT435_Men.capacity(), pcStr.substring(p, p + l_MsgGT435.GT435_Men.capacity())); p = p + l_MsgGT435.GT435_Men.capacity();
    l_MsgGT435.GT435_Cli.replace(0, l_MsgGT435.GT435_Cli.capacity(), pcStr.substring(p, p + l_MsgGT435.GT435_Cli.capacity())); p = p + l_MsgGT435.GT435_Cli.capacity();
    l_MsgGT435.GT435_Ncl.replace(0, l_MsgGT435.GT435_Ncl.capacity(), pcStr.substring(p, p + l_MsgGT435.GT435_Ncl.capacity())); p = p + l_MsgGT435.GT435_Ncl.capacity();
    l_MsgGT435.GT435_Dcn.replace(0, l_MsgGT435.GT435_Dcn.capacity(), pcStr.substring(p, p + l_MsgGT435.GT435_Dcn.capacity())); p = p + l_MsgGT435.GT435_Dcn.capacity();
    l_MsgGT435.GT435_Ndc.replace(0, l_MsgGT435.GT435_Ndc.capacity(), pcStr.substring(p, p + l_MsgGT435.GT435_Ndc.capacity())); p = p + l_MsgGT435.GT435_Ndc.capacity();
    l_MsgGT435.GT435_Suc.replace(0, l_MsgGT435.GT435_Suc.capacity(), pcStr.substring(p, p + l_MsgGT435.GT435_Suc.capacity())); p = p + l_MsgGT435.GT435_Suc.capacity();
    l_MsgGT435.GT435_Nsu.replace(0, l_MsgGT435.GT435_Nsu.capacity(), pcStr.substring(p, p + l_MsgGT435.GT435_Nsu.capacity())); p = p + l_MsgGT435.GT435_Nsu.capacity();
    l_MsgGT435.GT435_Eje.replace(0, l_MsgGT435.GT435_Eje.capacity(), pcStr.substring(p, p + l_MsgGT435.GT435_Eje.capacity())); p = p + l_MsgGT435.GT435_Eje.capacity();
    l_MsgGT435.GT435_Nej.replace(0, l_MsgGT435.GT435_Nej.capacity(), pcStr.substring(p, p + l_MsgGT435.GT435_Nej.capacity())); p = p + l_MsgGT435.GT435_Nej.capacity();
    l_MsgGT435.GT435_Tej.replace(0, l_MsgGT435.GT435_Tej.capacity(), pcStr.substring(p, p + l_MsgGT435.GT435_Tej.capacity())); p = p + l_MsgGT435.GT435_Tej.capacity();
    l_MsgGT435.GT435_Tmn.replace(0, l_MsgGT435.GT435_Tmn.capacity(), pcStr.substring(p, p + l_MsgGT435.GT435_Tmn.capacity())); p = p + l_MsgGT435.GT435_Tmn.capacity();
    l_MsgGT435.GT435_Trj.replace(0, l_MsgGT435.GT435_Trj.capacity(), pcStr.substring(p, p + l_MsgGT435.GT435_Trj.capacity())); p = p + l_MsgGT435.GT435_Trj.capacity();
    l_MsgGT435.GT435_Dcm.replace(0, l_MsgGT435.GT435_Dcm.capacity(), pcStr.substring(p, p + l_MsgGT435.GT435_Dcm.capacity())); p = p + l_MsgGT435.GT435_Dcm.capacity();
    l_MsgGT435.GT435_Fco.replace(0, l_MsgGT435.GT435_Fco.capacity(), pcStr.substring(p, p + l_MsgGT435.GT435_Fco.capacity())); p = p + l_MsgGT435.GT435_Fco.capacity();
    l_MsgGT435.GT435_Mnd.replace(0, l_MsgGT435.GT435_Mnd.capacity(), pcStr.substring(p, p + l_MsgGT435.GT435_Mnd.capacity())); p = p + l_MsgGT435.GT435_Mnd.capacity();
    l_MsgGT435.GT435_Nmn.replace(0, l_MsgGT435.GT435_Nmn.capacity(), pcStr.substring(p, p + l_MsgGT435.GT435_Nmn.capacity())); p = p + l_MsgGT435.GT435_Nmn.capacity();
    l_MsgGT435.GT435_Vco.replace(0, l_MsgGT435.GT435_Vco.capacity(), pcStr.substring(p, p + l_MsgGT435.GT435_Vco.capacity())); p = p + l_MsgGT435.GT435_Vco.capacity();
    l_MsgGT435.GT435_Vcn.replace(0, l_MsgGT435.GT435_Vcn.capacity(), pcStr.substring(p, p + l_MsgGT435.GT435_Vcn.capacity())); p = p + l_MsgGT435.GT435_Vcn.capacity();
    l_MsgGT435.GT435_Fcn.replace(0, l_MsgGT435.GT435_Fcn.capacity(), pcStr.substring(p, p + l_MsgGT435.GT435_Fcn.capacity())); p = p + l_MsgGT435.GT435_Fcn.capacity();
    l_MsgGT435.GT435_Cbt.replace(0, l_MsgGT435.GT435_Cbt.capacity(), pcStr.substring(p, p + l_MsgGT435.GT435_Cbt.capacity())); p = p + l_MsgGT435.GT435_Cbt.capacity();
    l_MsgGT435.GT435_Est.replace(0, l_MsgGT435.GT435_Est.capacity(), pcStr.substring(p, p + l_MsgGT435.GT435_Est.capacity())); p = p + l_MsgGT435.GT435_Est.capacity();
    l_MsgGT435.GT435_Dpc.replace(0, l_MsgGT435.GT435_Dpc.capacity(), pcStr.substring(p, p + l_MsgGT435.GT435_Dpc.capacity())); p = p + l_MsgGT435.GT435_Dpc.capacity();
    l_MsgGT435.GT435_Dir.replace(0, l_MsgGT435.GT435_Dir.capacity(), pcStr.substring(p, p + l_MsgGT435.GT435_Dir.capacity())); p = p + l_MsgGT435.GT435_Dir.capacity();
    l_MsgGT435.GT435_Tfc.replace(0, l_MsgGT435.GT435_Tfc.capacity(), pcStr.substring(p, p + l_MsgGT435.GT435_Tfc.capacity())); p = p + l_MsgGT435.GT435_Tfc.capacity();
    l_MsgGT435.GT435_Cmn.replace(0, l_MsgGT435.GT435_Cmn.capacity(), pcStr.substring(p, p + l_MsgGT435.GT435_Cmn.capacity())); p = p + l_MsgGT435.GT435_Cmn.capacity();
    l_MsgGT435.GT435_Ggr.replace(0, l_MsgGT435.GT435_Ggr.capacity(), pcStr.substring(p, p + l_MsgGT435.GT435_Ggr.capacity())); p = p + l_MsgGT435.GT435_Ggr.capacity();
    l_MsgGT435.GT435_Itg.replace(0, l_MsgGT435.GT435_Itg.capacity(), pcStr.substring(p, p + l_MsgGT435.GT435_Itg.capacity())); p = p + l_MsgGT435.GT435_Itg.capacity();
    l_MsgGT435.GT435_Tpr.replace(0, l_MsgGT435.GT435_Tpr.capacity(), pcStr.substring(p, p + l_MsgGT435.GT435_Tpr.capacity())); p = p + l_MsgGT435.GT435_Tpr.capacity();
    l_MsgGT435.GT435_Ttr.replace(0, l_MsgGT435.GT435_Ttr.capacity(), pcStr.substring(p, p + l_MsgGT435.GT435_Ttr.capacity())); p = p + l_MsgGT435.GT435_Ttr.capacity();
    l_MsgGT435.GT435_Fte.replace(0, l_MsgGT435.GT435_Fte.capacity(), pcStr.substring(p, p + l_MsgGT435.GT435_Fte.capacity())); p = p + l_MsgGT435.GT435_Fte.capacity();
    l_MsgGT435.GT435_Tsd.replace(0, l_MsgGT435.GT435_Tsd.capacity(), pcStr.substring(p, p + l_MsgGT435.GT435_Tsd.capacity())); p = p + l_MsgGT435.GT435_Tsd.capacity();
    l_MsgGT435.GT435_Dsg.replace(0, l_MsgGT435.GT435_Dsg.capacity(), pcStr.substring(p, p + l_MsgGT435.GT435_Dsg.capacity())); p = p + l_MsgGT435.GT435_Dsg.capacity();
    vec.add (l_MsgGT435);
    return vec;
  }
  //---------------------------------------------------------------------------------------
  static public String LSet_De_MsgGT435(Buf_MsgGT435 p_MsgGT435)
  {
    String pcStr = "";
    pcStr = pcStr + p_MsgGT435.GT435_Idr.toString();
    pcStr = pcStr + p_MsgGT435.GT435_Gti.toString();
    pcStr = pcStr + p_MsgGT435.GT435_Swt.toString();
    pcStr = pcStr + p_MsgGT435.GT435_Men.toString();
    pcStr = pcStr + p_MsgGT435.GT435_Cli.toString();
    pcStr = pcStr + p_MsgGT435.GT435_Ncl.toString();
    pcStr = pcStr + p_MsgGT435.GT435_Dcn.toString();
    pcStr = pcStr + p_MsgGT435.GT435_Ndc.toString();
    pcStr = pcStr + p_MsgGT435.GT435_Suc.toString();
    pcStr = pcStr + p_MsgGT435.GT435_Nsu.toString();
    pcStr = pcStr + p_MsgGT435.GT435_Eje.toString();
    pcStr = pcStr + p_MsgGT435.GT435_Nej.toString();
    pcStr = pcStr + p_MsgGT435.GT435_Tej.toString();
    pcStr = pcStr + p_MsgGT435.GT435_Tmn.toString();
    pcStr = pcStr + p_MsgGT435.GT435_Trj.toString();
    pcStr = pcStr + p_MsgGT435.GT435_Dcm.toString();
    pcStr = pcStr + p_MsgGT435.GT435_Fco.toString();
    pcStr = pcStr + p_MsgGT435.GT435_Mnd.toString();
    pcStr = pcStr + p_MsgGT435.GT435_Nmn.toString();
    pcStr = pcStr + p_MsgGT435.GT435_Vco.toString();
    pcStr = pcStr + p_MsgGT435.GT435_Vcn.toString();
    pcStr = pcStr + p_MsgGT435.GT435_Fcn.toString();
    pcStr = pcStr + p_MsgGT435.GT435_Cbt.toString();
    pcStr = pcStr + p_MsgGT435.GT435_Est.toString();
    pcStr = pcStr + p_MsgGT435.GT435_Dpc.toString();
    pcStr = pcStr + p_MsgGT435.GT435_Dir.toString();
    pcStr = pcStr + p_MsgGT435.GT435_Tfc.toString();
    pcStr = pcStr + p_MsgGT435.GT435_Cmn.toString();
    pcStr = pcStr + p_MsgGT435.GT435_Ggr.toString();
    pcStr = pcStr + p_MsgGT435.GT435_Itg.toString();
    pcStr = pcStr + p_MsgGT435.GT435_Tpr.toString();
    pcStr = pcStr + p_MsgGT435.GT435_Ttr.toString();
    pcStr = pcStr + p_MsgGT435.GT435_Fte.toString();
    pcStr = pcStr + p_MsgGT435.GT435_Tsd.toString();
    pcStr = pcStr + p_MsgGT435.GT435_Dsg.toString();
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
}