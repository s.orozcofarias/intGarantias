// Source File Name:   MSGGT412.java

package com.intGarantias.modules.global;

import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.util.Formateo;

import java.util.*;
import java.lang.*;
import org.apache.turbine.util.Log;

public class MSGGT412
{
  //---------------------------------------------------------------------------------------
  public static class Grt_Gbs_Det
  {
    public StringBuffer GT412_Itb = new StringBuffer(2  );  //X(02)         2 Id Tipo Bien                                     
    public StringBuffer GT412_Itg = new StringBuffer(5  );  //X(05)         7 Id Tipo Gtia                                     
    public StringBuffer GT412_Csv = new StringBuffer(3  );  //X(03)        10 Conservador                                      
    public StringBuffer GT412_Lcl = new StringBuffer(15 );  //X(15)        25 Localidad                                        
    public StringBuffer GT412_Rpo = new StringBuffer(4  );  //9(04)        29 Repertorio                                       
    public StringBuffer GT412_Foj = new StringBuffer(7  );  //9(07)        36 Fojas                                            
    public StringBuffer GT412_Nin = new StringBuffer(7  );  //9(07)        43 Numero                                           
    public StringBuffer GT412_Dsb = new StringBuffer(100);  //X(100)       73 Descripcion Bien                                 
    public StringBuffer GT412_Vtt = new StringBuffer(15 );  //9(11)V9(4)   88 VTasacion <TRJ>                                  
    public StringBuffer GT412_Vts = new StringBuffer(15 );  //9(13)V9(2)  103 VTasacion <MND>                                  
    public StringBuffer GT412_Frv = new StringBuffer(8  );  //9(08)       111 FRevalorizacion                                  
    public StringBuffer GT412_Vur = new StringBuffer(9  );  //9(07)V9(2)  120 Valor UReajustable                               
    public StringBuffer GT412_Dpc = new StringBuffer(10 );  //X(10)       130 PContable                                        
    public StringBuffer GT412_Pct = new StringBuffer(5  );  //9(03)V9(2)  135 %Castigo SBIF                                    
    public StringBuffer GT412_Vcn = new StringBuffer(15 );  //9(13)V9(2)  150 VContable <MND>                                  
    public StringBuffer GT412_Vlq = new StringBuffer(15 );  //9(13)V9(2)  165 VLiquidacion <MND>                               
    public StringBuffer GT412_Vtb = new StringBuffer(15 );  //9(13)V9(2)  180 VRemate <MND>                                    
    public StringBuffer GT412_Fvt = new StringBuffer(8  );  //9(08)       188 FVcto Tasacion                                   
    public StringBuffer GT412_Obs = new StringBuffer(9  );  //9(09)       372 @Observaciones       
    public StringBuffer GT412_Vsg = new StringBuffer(15 );  //9(11)V9(4)  387 VReposicion                                      
    public StringBuffer GT412_Csg = new StringBuffer(3  );  //9(03)       390 Aseguradora                                      
    public StringBuffer GT412_Spz = new StringBuffer(10 );  //X(10)       400 Poliza                                           
    public StringBuffer GT412_Sfv = new StringBuffer(8  );  //9(08)       408 FVcto Seguro                                     
    public StringBuffer GT412_Sum = new StringBuffer(1  );  //X(01)       409 UMon Seguro                                      
    public StringBuffer GT412_Svl = new StringBuffer(15 );  //9(11)V9(4)  424 VSeguro <UF>                                     
    public StringBuffer GT412_Drb = new StringBuffer(107);  //X(107)      531 Direccion Bien                                   
    public StringBuffer GT412_Cmn = new StringBuffer(11 );  //9(11)       542 CodComuna                                        
    public StringBuffer GT412_Coo = new StringBuffer(10 );  //X(10)       552 Coordenadas                                      
    public StringBuffer GT412_Udd = new StringBuffer(9  );  //9(07)V9(2)  561 Cantidad                                         
    public StringBuffer GT412_Umd = new StringBuffer(2  );  //X(02)       563 UMedida                                          
    public StringBuffer GT412_Umt = new StringBuffer(1  );  //X(01)       564 UMonetaria                                       
    public StringBuffer GT412_Pum = new StringBuffer(15 );  //9(11)V9(4)  579 PUnitario <UMT>                                  
    public StringBuffer GT412_Vtu = new StringBuffer(15 );  //9(11)V9(4)  594 VTasacion <UMT>                                  
    public StringBuffer GT412_Vum = new StringBuffer(9  );  //9(07)V9(2)  603 Valor Pesos <UMT>                                
    public StringBuffer GT412_Mka = new StringBuffer(20 );  //X(20)       623 Marca                                            
    public StringBuffer GT412_Mdl = new StringBuffer(20 );  //X(20)       643 Modelo                                           
    public StringBuffer GT412_Ano = new StringBuffer(4  );  //9(04)       647 A�o Fabricacion                                  
    public StringBuffer GT412_Nmt = new StringBuffer(20 );  //X(20)       667 Numero Motor                                     
    public StringBuffer GT412_Nse = new StringBuffer(20 );  //X(20)       687 Numero Serie                                     
    public StringBuffer GT412_Nhu = new StringBuffer(5  );  //9(05)       692 Numero Horas Uso                                 
    public StringBuffer GT412_Avu = new StringBuffer(5  );  //9(05)       697 A�os Vida Util                                   
    public StringBuffer GT412_Faf = new StringBuffer(8  );  //9(08)       705 FAvaluo Fiscal                                   
    public StringBuffer GT412_Vaf = new StringBuffer(15 );  //9(13)V9(2)  720 VAvaluo Fiscal                                   
    public StringBuffer GT412_Nfa = new StringBuffer(7  );  //9(07)       727 Numero Factura                                   
    public StringBuffer GT412_Mfa = new StringBuffer(15 );  //9(13)V9(2)  742 Monto Factura                                    
    public StringBuffer GT412_Trj = new StringBuffer(3  );  //X(03)       745 Tipo Reajustabilidad                             
    public StringBuffer GT412_Mnd = new StringBuffer(3  );  //9(03)       748 Moneda                                           
    public StringBuffer GT412_Dcm = new StringBuffer(1  );  //9(01)       749 Decimales <MND>                                  
    public StringBuffer GT412_Uso = new StringBuffer(30 );   //X(30)
    public StringBuffer GT412_Agp = new StringBuffer(30 );   //X(30)
    public StringBuffer GT412_Tct = new StringBuffer(30 );   //X(30)
    public StringBuffer GT412_Cld = new StringBuffer(30 );   //X(30)

    public String getItb()         {return GT412_Itb.toString();}
    public String getItg()         {return GT412_Itg.toString();}
    public String getCsv()         {return GT412_Csv.toString();}
    public String getLcl()         {return GT412_Lcl.toString();}
    public String getRpo()         {return GT412_Rpo.toString();}
    public String getFoj()         {return GT412_Foj.toString();}
    public String getNin()         {return GT412_Nin.toString();}
    public String getDsb()         {return GT412_Dsb.toString();}
    public String getVtt()         {return GT412_Vtt.toString();}
    public String getVts()         {return GT412_Vts.toString();}
    public String getFrv()         {return GT412_Frv.toString();}
    public String getVur()         {return GT412_Vur.toString();}
    public String getDpc()         {return GT412_Dpc.toString();}
    public String getPct()         {return GT412_Pct.toString();}
    public String getVcn()         {return GT412_Vcn.toString();}
    public String getVlq()         {return GT412_Vlq.toString();}
    public String getVtb()         {return GT412_Vtb.toString();}
    public String getFvt()         {return GT412_Fvt.toString();}
    public String getObs()         {return GT412_Obs.toString();}
    public String getVsg()         {return GT412_Vsg.toString();}
    public String getCsg()         {return GT412_Csg.toString();}
    public String getSpz()         {return GT412_Spz.toString();}
    public String getSfv()         {return GT412_Sfv.toString();}
    public String getSum()         {return GT412_Sum.toString();}
    public String getSvl()         {return GT412_Svl.toString();}
    public String getDrb()         {return GT412_Drb.toString();}
    public String getCmn()         {return GT412_Cmn.toString();}
    public String getCoo()         {return GT412_Coo.toString();}
    public String getUdd()         {return GT412_Udd.toString();}
    public String getUmd()         {return GT412_Umd.toString();}
    public String getUmt()         {return GT412_Umt.toString();}
    public String getPum()         {return GT412_Pum.toString();}
    public String getVtu()         {return GT412_Vtu.toString();}
    public String getVum()         {return GT412_Vum.toString();}
    public String getMka()         {return GT412_Mka.toString();}
    public String getMdl()         {return GT412_Mdl.toString();}
    public String getAno()         {return GT412_Ano.toString();}
    public String getNmt()         {return GT412_Nmt.toString();}
    public String getNse()         {return GT412_Nse.toString();}
    public String getNhu()         {return GT412_Nhu.toString();}
    public String getAvu()         {return GT412_Avu.toString();}
    public String getFaf()         {return GT412_Faf.toString();}
    public String getVaf()         {return GT412_Vaf.toString();}
    public String getNfa()         {return GT412_Nfa.toString();}
    public String getMfa()         {return GT412_Mfa.toString();}
    public String getTrj()         {return GT412_Trj.toString();}
    public String getMnd()         {return GT412_Mnd.toString();}
    public String getDcm()         {return GT412_Dcm.toString();}
    public String getUso()         {return GT412_Uso.toString();}
    public String getAgp()         {return GT412_Agp.toString();}
    public String getTct()         {return GT412_Tct.toString();}
    public String getCld()         {return GT412_Cld.toString();}

    public Formateo f = new Formateo();
    public String fgetVtt()        {return f.Formato(getVtt(),"99.999.999.999",4,0,',');}
    public String fgetVts()        {return f.Formato(getVts(),"9.999.999.999.999",2,0,',');}
    public String fgetFrv()        {return f.FormatDate(getFrv());}
    public String fgetVur()        {return f.Formato(getVur(),"9.999.999",2,2,',');}
    public String fgetPct()        {return f.Formato(getPct(),"999",2,2,',');}
    public String fgetVcn()        {return f.Formato(getVcn(),"9.999.999.999.999",2,0,',');}
    public String fgetVlq()        {return f.Formato(getVlq(),"9.999.999.999.999",2,0,',');}
    public String fgetVtb()        {return f.Formato(getVtb(),"9.999.999.999.999",2,0,',');}
    public String fgetFvt()        {return f.FormatDate(getFvt());}
    public String fgetVsg()        {return f.Formato(getVsg(),"99.999.999.999",4,4,',');}
    public String fgetSfv()        {return f.FormatDate(getSfv());}
    public String fgetSvl()        {return f.Formato(getSvl(),"99.999.999.999",4,4,',');}
    public String fgetUdd()        {return f.Formato(getUdd(),"9.999.999",2,2,',');}
    public String fgetPum()        {return f.Formato(getPum(),"99.999.999.999",4,4,',');}
    public String fgetVtu()        {return f.Formato(getVtu(),"99.999.999.999",4,4,',');}
    public String fgetVum()        {return f.Formato(getVum(),"9.999.999",2,2,',');}
    public String fgetNhu()        {return f.Formato(getNhu(),"99.999",0,0,',');}
    public String fgetAvu()        {return f.Formato(getAvu(),"99.999",0,0,',');}
    public String fgetFaf()        {return f.FormatDate(getFaf());}
    public String fgetVaf()        {return f.Formato(getVaf(),"9.999.999.999.999",2,0,',');}
    public String fgetMfa()        {return f.Formato(getMfa(),"9.999.999.999.999",2,0,',');}
    public String fgetTrj()        {return getTrj().trim();}
  }
  //---------------------------------------------------------------------------------------
  public static class Buf_MsgGT412
  {
    public StringBuffer GT412_Idr = new StringBuffer(1); //X(01)        1 Idr Host     
    public StringBuffer GT412_Gti = new StringBuffer(10);//X(10)       11 Id. Garantia 
    public StringBuffer GT412_Seq = new StringBuffer(3); //9(03)       14 Total Indices
    public Grt_Gbs_Det  GT412_Gbs = new Grt_Gbs_Det();   //X(749)     763 Tabla Detalle

    public String getIdr()         {return GT412_Idr.toString();}
    public String getGti()         {return GT412_Gti.toString();}
    public String getSeq()         {return GT412_Seq.toString();}
    public Grt_Gbs_Det getGbs()    {return GT412_Gbs;}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_MsgGT412 Inicia_MsgGT412()
  {
    Buf_MsgGT412 l_MsgGT412 = new Buf_MsgGT412();
    l_MsgGT412.GT412_Idr.replace(0, l_MsgGT412.GT412_Idr.capacity(), RUTGEN.Blancos(1 ));
    l_MsgGT412.GT412_Gti.replace(0, l_MsgGT412.GT412_Gti.capacity(), RUTGEN.Blancos(10));
    l_MsgGT412.GT412_Seq.replace(0, l_MsgGT412.GT412_Seq.capacity(), RUTGEN.Blancos(3 ));
    Grt_Gbs_Det Gbs = new Grt_Gbs_Det();          
    Gbs.GT412_Itb.replace(0, Gbs.GT412_Itb.capacity(), RUTGEN.Blancos(2  ));
    Gbs.GT412_Itg.replace(0, Gbs.GT412_Itg.capacity(), RUTGEN.Blancos(5  ));
    Gbs.GT412_Csv.replace(0, Gbs.GT412_Csv.capacity(), RUTGEN.Blancos(3  ));
    Gbs.GT412_Lcl.replace(0, Gbs.GT412_Lcl.capacity(), RUTGEN.Blancos(15 ));
    Gbs.GT412_Rpo.replace(0, Gbs.GT412_Rpo.capacity(), RUTGEN.Blancos(4  ));
    Gbs.GT412_Foj.replace(0, Gbs.GT412_Foj.capacity(), RUTGEN.Blancos(7  ));
    Gbs.GT412_Nin.replace(0, Gbs.GT412_Nin.capacity(), RUTGEN.Blancos(7  ));
    Gbs.GT412_Dsb.replace(0, Gbs.GT412_Dsb.capacity(), RUTGEN.Blancos(100));
    Gbs.GT412_Vtt.replace(0, Gbs.GT412_Vtt.capacity(), RUTGEN.Blancos(15 ));
    Gbs.GT412_Vts.replace(0, Gbs.GT412_Vts.capacity(), RUTGEN.Blancos(15 ));
    Gbs.GT412_Frv.replace(0, Gbs.GT412_Frv.capacity(), RUTGEN.Blancos(8  ));
    Gbs.GT412_Vur.replace(0, Gbs.GT412_Vur.capacity(), RUTGEN.Blancos(9  ));
    Gbs.GT412_Dpc.replace(0, Gbs.GT412_Dpc.capacity(), RUTGEN.Blancos(10 ));
    Gbs.GT412_Pct.replace(0, Gbs.GT412_Pct.capacity(), RUTGEN.Blancos(5  ));
    Gbs.GT412_Vcn.replace(0, Gbs.GT412_Vcn.capacity(), RUTGEN.Blancos(15 ));
    Gbs.GT412_Vlq.replace(0, Gbs.GT412_Vlq.capacity(), RUTGEN.Blancos(15 ));
    Gbs.GT412_Vtb.replace(0, Gbs.GT412_Vtb.capacity(), RUTGEN.Blancos(15 ));
    Gbs.GT412_Fvt.replace(0, Gbs.GT412_Fvt.capacity(), RUTGEN.Blancos(8  ));
    Gbs.GT412_Obs.replace(0, Gbs.GT412_Obs.capacity(), RUTGEN.Blancos(9  ));
    Gbs.GT412_Vsg.replace(0, Gbs.GT412_Vsg.capacity(), RUTGEN.Blancos(15 ));
    Gbs.GT412_Csg.replace(0, Gbs.GT412_Csg.capacity(), RUTGEN.Blancos(3  ));
    Gbs.GT412_Spz.replace(0, Gbs.GT412_Spz.capacity(), RUTGEN.Blancos(10 ));
    Gbs.GT412_Sfv.replace(0, Gbs.GT412_Sfv.capacity(), RUTGEN.Blancos(8  ));
    Gbs.GT412_Sum.replace(0, Gbs.GT412_Sum.capacity(), RUTGEN.Blancos(1  ));
    Gbs.GT412_Svl.replace(0, Gbs.GT412_Svl.capacity(), RUTGEN.Blancos(15 ));
    Gbs.GT412_Drb.replace(0, Gbs.GT412_Drb.capacity(), RUTGEN.Blancos(107));
    Gbs.GT412_Cmn.replace(0, Gbs.GT412_Cmn.capacity(), RUTGEN.Blancos(11 ));
    Gbs.GT412_Coo.replace(0, Gbs.GT412_Coo.capacity(), RUTGEN.Blancos(10 ));
    Gbs.GT412_Udd.replace(0, Gbs.GT412_Udd.capacity(), RUTGEN.Blancos(9  ));
    Gbs.GT412_Umd.replace(0, Gbs.GT412_Umd.capacity(), RUTGEN.Blancos(2  ));
    Gbs.GT412_Umt.replace(0, Gbs.GT412_Umt.capacity(), RUTGEN.Blancos(1  ));
    Gbs.GT412_Pum.replace(0, Gbs.GT412_Pum.capacity(), RUTGEN.Blancos(15 ));
    Gbs.GT412_Vtu.replace(0, Gbs.GT412_Vtu.capacity(), RUTGEN.Blancos(15 ));
    Gbs.GT412_Vum.replace(0, Gbs.GT412_Vum.capacity(), RUTGEN.Blancos(9  ));
    Gbs.GT412_Mka.replace(0, Gbs.GT412_Mka.capacity(), RUTGEN.Blancos(20 ));
    Gbs.GT412_Mdl.replace(0, Gbs.GT412_Mdl.capacity(), RUTGEN.Blancos(20 ));
    Gbs.GT412_Ano.replace(0, Gbs.GT412_Ano.capacity(), RUTGEN.Blancos(4  ));
    Gbs.GT412_Nmt.replace(0, Gbs.GT412_Nmt.capacity(), RUTGEN.Blancos(20 ));
    Gbs.GT412_Nse.replace(0, Gbs.GT412_Nse.capacity(), RUTGEN.Blancos(20 ));
    Gbs.GT412_Nhu.replace(0, Gbs.GT412_Nhu.capacity(), RUTGEN.Blancos(5  ));
    Gbs.GT412_Avu.replace(0, Gbs.GT412_Avu.capacity(), RUTGEN.Blancos(5  ));
    Gbs.GT412_Faf.replace(0, Gbs.GT412_Faf.capacity(), RUTGEN.Blancos(8  ));
    Gbs.GT412_Vaf.replace(0, Gbs.GT412_Vaf.capacity(), RUTGEN.Blancos(15 ));
    Gbs.GT412_Nfa.replace(0, Gbs.GT412_Nfa.capacity(), RUTGEN.Blancos(7  ));
    Gbs.GT412_Mfa.replace(0, Gbs.GT412_Mfa.capacity(), RUTGEN.Blancos(15 ));
    Gbs.GT412_Trj.replace(0, Gbs.GT412_Trj.capacity(), RUTGEN.Blancos(3  ));
    Gbs.GT412_Mnd.replace(0, Gbs.GT412_Mnd.capacity(), RUTGEN.Blancos(3  ));
    Gbs.GT412_Dcm.replace(0, Gbs.GT412_Dcm.capacity(), RUTGEN.Blancos(1  ));
    Gbs.GT412_Uso.replace(0, Gbs.GT412_Uso.capacity(), RUTGEN.Blancos(30 ));
    Gbs.GT412_Agp.replace(0, Gbs.GT412_Agp.capacity(), RUTGEN.Blancos(30 ));
    Gbs.GT412_Tct.replace(0, Gbs.GT412_Tct.capacity(), RUTGEN.Blancos(30 ));
    Gbs.GT412_Cld.replace(0, Gbs.GT412_Cld.capacity(), RUTGEN.Blancos(30 ));
    l_MsgGT412.GT412_Gbs = Gbs;
    return l_MsgGT412;
  }
  //---------------------------------------------------------------------------------------
  static public Buf_MsgGT412 LSet_A_MsgGT412(String pcStr)
  {
    Buf_MsgGT412 l_MsgGT412 = new Buf_MsgGT412();
    MSGGT412 vMsgGT412 = new MSGGT412();
    Vector vDatos = vMsgGT412.LSet_A_vMsgGT412(pcStr);
    l_MsgGT412 = (MSGGT412.Buf_MsgGT412)vDatos.elementAt(0);
    return l_MsgGT412;
  }
  //---------------------------------------------------------------------------------------
  public Vector LSet_A_vMsgGT412(String pcStr)
  {
    int p = 0;
    Vector vec = new Vector();
    Buf_MsgGT412 l_MsgGT412 = new Buf_MsgGT412();                              
    l_MsgGT412.GT412_Idr.append(pcStr.substring(p, p + l_MsgGT412.GT412_Idr.capacity())); p = p + l_MsgGT412.GT412_Idr.capacity();
    l_MsgGT412.GT412_Gti.append(pcStr.substring(p, p + l_MsgGT412.GT412_Gti.capacity())); p = p + l_MsgGT412.GT412_Gti.capacity();
    l_MsgGT412.GT412_Seq.append(pcStr.substring(p, p + l_MsgGT412.GT412_Seq.capacity())); p = p + l_MsgGT412.GT412_Seq.capacity();
    Grt_Gbs_Det Gbs = new Grt_Gbs_Det();          
    Gbs.GT412_Itb.append(pcStr.substring(p, p + Gbs.GT412_Itb.capacity())); p = p + Gbs.GT412_Itb.capacity();
    Gbs.GT412_Itg.append(pcStr.substring(p, p + Gbs.GT412_Itg.capacity())); p = p + Gbs.GT412_Itg.capacity();
    Gbs.GT412_Csv.append(pcStr.substring(p, p + Gbs.GT412_Csv.capacity())); p = p + Gbs.GT412_Csv.capacity();
    Gbs.GT412_Lcl.append(pcStr.substring(p, p + Gbs.GT412_Lcl.capacity())); p = p + Gbs.GT412_Lcl.capacity();
    Gbs.GT412_Rpo.append(pcStr.substring(p, p + Gbs.GT412_Rpo.capacity())); p = p + Gbs.GT412_Rpo.capacity();
    Gbs.GT412_Foj.append(pcStr.substring(p, p + Gbs.GT412_Foj.capacity())); p = p + Gbs.GT412_Foj.capacity();
    Gbs.GT412_Nin.append(pcStr.substring(p, p + Gbs.GT412_Nin.capacity())); p = p + Gbs.GT412_Nin.capacity();
    Gbs.GT412_Dsb.append(pcStr.substring(p, p + Gbs.GT412_Dsb.capacity())); p = p + Gbs.GT412_Dsb.capacity();
    Gbs.GT412_Vtt.append(pcStr.substring(p, p + Gbs.GT412_Vtt.capacity())); p = p + Gbs.GT412_Vtt.capacity();
    Gbs.GT412_Vts.append(pcStr.substring(p, p + Gbs.GT412_Vts.capacity())); p = p + Gbs.GT412_Vts.capacity();
    Gbs.GT412_Frv.append(pcStr.substring(p, p + Gbs.GT412_Frv.capacity())); p = p + Gbs.GT412_Frv.capacity();
    Gbs.GT412_Vur.append(pcStr.substring(p, p + Gbs.GT412_Vur.capacity())); p = p + Gbs.GT412_Vur.capacity();
    Gbs.GT412_Dpc.append(pcStr.substring(p, p + Gbs.GT412_Dpc.capacity())); p = p + Gbs.GT412_Dpc.capacity();
    Gbs.GT412_Pct.append(pcStr.substring(p, p + Gbs.GT412_Pct.capacity())); p = p + Gbs.GT412_Pct.capacity();
    Gbs.GT412_Vcn.append(pcStr.substring(p, p + Gbs.GT412_Vcn.capacity())); p = p + Gbs.GT412_Vcn.capacity();
    Gbs.GT412_Vlq.append(pcStr.substring(p, p + Gbs.GT412_Vlq.capacity())); p = p + Gbs.GT412_Vlq.capacity();
    Gbs.GT412_Vtb.append(pcStr.substring(p, p + Gbs.GT412_Vtb.capacity())); p = p + Gbs.GT412_Vtb.capacity();
    Gbs.GT412_Fvt.append(pcStr.substring(p, p + Gbs.GT412_Fvt.capacity())); p = p + Gbs.GT412_Fvt.capacity();
    Gbs.GT412_Obs.append(pcStr.substring(p, p + Gbs.GT412_Obs.capacity())); p = p + Gbs.GT412_Obs.capacity();
    Gbs.GT412_Vsg.append(pcStr.substring(p, p + Gbs.GT412_Vsg.capacity())); p = p + Gbs.GT412_Vsg.capacity();
    Gbs.GT412_Csg.append(pcStr.substring(p, p + Gbs.GT412_Csg.capacity())); p = p + Gbs.GT412_Csg.capacity();
    Gbs.GT412_Spz.append(pcStr.substring(p, p + Gbs.GT412_Spz.capacity())); p = p + Gbs.GT412_Spz.capacity();
    Gbs.GT412_Sfv.append(pcStr.substring(p, p + Gbs.GT412_Sfv.capacity())); p = p + Gbs.GT412_Sfv.capacity();
    Gbs.GT412_Sum.append(pcStr.substring(p, p + Gbs.GT412_Sum.capacity())); p = p + Gbs.GT412_Sum.capacity();
    Gbs.GT412_Svl.append(pcStr.substring(p, p + Gbs.GT412_Svl.capacity())); p = p + Gbs.GT412_Svl.capacity();
    Gbs.GT412_Drb.append(pcStr.substring(p, p + Gbs.GT412_Drb.capacity())); p = p + Gbs.GT412_Drb.capacity();
    Gbs.GT412_Cmn.append(pcStr.substring(p, p + Gbs.GT412_Cmn.capacity())); p = p + Gbs.GT412_Cmn.capacity();
    Gbs.GT412_Coo.append(pcStr.substring(p, p + Gbs.GT412_Coo.capacity())); p = p + Gbs.GT412_Coo.capacity();
    Gbs.GT412_Udd.append(pcStr.substring(p, p + Gbs.GT412_Udd.capacity())); p = p + Gbs.GT412_Udd.capacity();
    Gbs.GT412_Umd.append(pcStr.substring(p, p + Gbs.GT412_Umd.capacity())); p = p + Gbs.GT412_Umd.capacity();
    Gbs.GT412_Umt.append(pcStr.substring(p, p + Gbs.GT412_Umt.capacity())); p = p + Gbs.GT412_Umt.capacity();
    Gbs.GT412_Pum.append(pcStr.substring(p, p + Gbs.GT412_Pum.capacity())); p = p + Gbs.GT412_Pum.capacity();
    Gbs.GT412_Vtu.append(pcStr.substring(p, p + Gbs.GT412_Vtu.capacity())); p = p + Gbs.GT412_Vtu.capacity();
    Gbs.GT412_Vum.append(pcStr.substring(p, p + Gbs.GT412_Vum.capacity())); p = p + Gbs.GT412_Vum.capacity();
    Gbs.GT412_Mka.append(pcStr.substring(p, p + Gbs.GT412_Mka.capacity())); p = p + Gbs.GT412_Mka.capacity();
    Gbs.GT412_Mdl.append(pcStr.substring(p, p + Gbs.GT412_Mdl.capacity())); p = p + Gbs.GT412_Mdl.capacity();
    Gbs.GT412_Ano.append(pcStr.substring(p, p + Gbs.GT412_Ano.capacity())); p = p + Gbs.GT412_Ano.capacity();
    Gbs.GT412_Nmt.append(pcStr.substring(p, p + Gbs.GT412_Nmt.capacity())); p = p + Gbs.GT412_Nmt.capacity();
    Gbs.GT412_Nse.append(pcStr.substring(p, p + Gbs.GT412_Nse.capacity())); p = p + Gbs.GT412_Nse.capacity();
    Gbs.GT412_Nhu.append(pcStr.substring(p, p + Gbs.GT412_Nhu.capacity())); p = p + Gbs.GT412_Nhu.capacity();
    Gbs.GT412_Avu.append(pcStr.substring(p, p + Gbs.GT412_Avu.capacity())); p = p + Gbs.GT412_Avu.capacity();
    Gbs.GT412_Faf.append(pcStr.substring(p, p + Gbs.GT412_Faf.capacity())); p = p + Gbs.GT412_Faf.capacity();
    Gbs.GT412_Vaf.append(pcStr.substring(p, p + Gbs.GT412_Vaf.capacity())); p = p + Gbs.GT412_Vaf.capacity();
    Gbs.GT412_Nfa.append(pcStr.substring(p, p + Gbs.GT412_Nfa.capacity())); p = p + Gbs.GT412_Nfa.capacity();
    Gbs.GT412_Mfa.append(pcStr.substring(p, p + Gbs.GT412_Mfa.capacity())); p = p + Gbs.GT412_Mfa.capacity();
    Gbs.GT412_Trj.append(pcStr.substring(p, p + Gbs.GT412_Trj.capacity())); p = p + Gbs.GT412_Trj.capacity();
    Gbs.GT412_Mnd.append(pcStr.substring(p, p + Gbs.GT412_Mnd.capacity())); p = p + Gbs.GT412_Mnd.capacity();
    Gbs.GT412_Dcm.append(pcStr.substring(p, p + Gbs.GT412_Dcm.capacity())); p = p + Gbs.GT412_Dcm.capacity();
    Gbs.GT412_Uso.append(pcStr.substring(p, p + Gbs.GT412_Uso.capacity())); p = p + Gbs.GT412_Uso.capacity();
    Gbs.GT412_Agp.append(pcStr.substring(p, p + Gbs.GT412_Agp.capacity())); p = p + Gbs.GT412_Agp.capacity();
    Gbs.GT412_Tct.append(pcStr.substring(p, p + Gbs.GT412_Tct.capacity())); p = p + Gbs.GT412_Tct.capacity();
    Gbs.GT412_Cld.append(pcStr.substring(p, p + Gbs.GT412_Cld.capacity())); p = p + Gbs.GT412_Cld.capacity();
    l_MsgGT412.GT412_Gbs = Gbs;
    vec.add(l_MsgGT412);            
    return vec;
  }
  //---------------------------------------------------------------------------------------
  static public String LSet_De_MsgGT412(Buf_MsgGT412 p_MsgGT412)
  {
    String pcStr = "";
    pcStr = pcStr + p_MsgGT412.GT412_Idr.toString();
    pcStr = pcStr + p_MsgGT412.GT412_Gti.toString();
    pcStr = pcStr + p_MsgGT412.GT412_Seq.toString();
    Grt_Gbs_Det Gbs = new Grt_Gbs_Det();          
    Gbs = p_MsgGT412.GT412_Gbs;
    pcStr = pcStr + Gbs.GT412_Itb.toString();
    pcStr = pcStr + Gbs.GT412_Itg.toString();
    pcStr = pcStr + Gbs.GT412_Csv.toString();
    pcStr = pcStr + Gbs.GT412_Lcl.toString();
    pcStr = pcStr + Gbs.GT412_Rpo.toString();
    pcStr = pcStr + Gbs.GT412_Foj.toString();
    pcStr = pcStr + Gbs.GT412_Nin.toString();
    pcStr = pcStr + Gbs.GT412_Dsb.toString();
    pcStr = pcStr + Gbs.GT412_Vtt.toString();
    pcStr = pcStr + Gbs.GT412_Vts.toString();
    pcStr = pcStr + Gbs.GT412_Frv.toString();
    pcStr = pcStr + Gbs.GT412_Vur.toString();
    pcStr = pcStr + Gbs.GT412_Dpc.toString();
    pcStr = pcStr + Gbs.GT412_Pct.toString();
    pcStr = pcStr + Gbs.GT412_Vcn.toString();
    pcStr = pcStr + Gbs.GT412_Vlq.toString();
    pcStr = pcStr + Gbs.GT412_Vtb.toString();
    pcStr = pcStr + Gbs.GT412_Fvt.toString();
    pcStr = pcStr + Gbs.GT412_Obs.toString();
    pcStr = pcStr + Gbs.GT412_Vsg.toString();
    pcStr = pcStr + Gbs.GT412_Csg.toString();
    pcStr = pcStr + Gbs.GT412_Spz.toString();
    pcStr = pcStr + Gbs.GT412_Sfv.toString();
    pcStr = pcStr + Gbs.GT412_Sum.toString();
    pcStr = pcStr + Gbs.GT412_Svl.toString();
    pcStr = pcStr + Gbs.GT412_Drb.toString();
    pcStr = pcStr + Gbs.GT412_Cmn.toString();
    pcStr = pcStr + Gbs.GT412_Coo.toString();
    pcStr = pcStr + Gbs.GT412_Udd.toString();
    pcStr = pcStr + Gbs.GT412_Umd.toString();
    pcStr = pcStr + Gbs.GT412_Umt.toString();
    pcStr = pcStr + Gbs.GT412_Pum.toString();
    pcStr = pcStr + Gbs.GT412_Vtu.toString();
    pcStr = pcStr + Gbs.GT412_Vum.toString();
    pcStr = pcStr + Gbs.GT412_Mka.toString();
    pcStr = pcStr + Gbs.GT412_Mdl.toString();
    pcStr = pcStr + Gbs.GT412_Ano.toString();
    pcStr = pcStr + Gbs.GT412_Nmt.toString();
    pcStr = pcStr + Gbs.GT412_Nse.toString();
    pcStr = pcStr + Gbs.GT412_Nhu.toString();
    pcStr = pcStr + Gbs.GT412_Avu.toString();
    pcStr = pcStr + Gbs.GT412_Faf.toString();
    pcStr = pcStr + Gbs.GT412_Vaf.toString();
    pcStr = pcStr + Gbs.GT412_Nfa.toString();
    pcStr = pcStr + Gbs.GT412_Mfa.toString();
    pcStr = pcStr + Gbs.GT412_Trj.toString();
    pcStr = pcStr + Gbs.GT412_Mnd.toString();
    pcStr = pcStr + Gbs.GT412_Dcm.toString();
    pcStr = pcStr + Gbs.GT412_Uso.toString();
    pcStr = pcStr + Gbs.GT412_Agp.toString();
    pcStr = pcStr + Gbs.GT412_Tct.toString();
    pcStr = pcStr + Gbs.GT412_Cld.toString();
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
}