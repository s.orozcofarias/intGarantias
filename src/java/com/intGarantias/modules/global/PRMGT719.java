// Source File Name:   PRMGT719.java

package com.intGarantias.modules.global;

import com.FHTServlet.modules.global.fhtException;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.util.Formateo;
import com.FHTServlet.modules.global.BF_PRA;
import com.FHTServlet.modules.global.BF_IXI;
import com.FHTServlet.modules.global.BF_LOB;
import com.FHTServlet.modules.util.Constants;

import org.apache.turbine.util.upload.FileItem;
import org.apache.turbine.om.peer.BasePeer;
import org.apache.turbine.util.db.pool.DBConnection;
import com.workingdogs.village.Record;

import java.util.*;
import java.lang.*;
import org.apache.turbine.util.Log;

public class PRMGT719
{
  //===============================================================================================================================
  public static class Buf_PrmGT719
  {
    public StringBuffer GT719_Idr = new StringBuffer(1 );   //X(01)        Idr.Accion
    public StringBuffer GT719_Psw = new StringBuffer(1 );   //X(01)        Control Ingreso
    public StringBuffer GT719_Sis = new StringBuffer(3 );   //X(03)        Sistema (Garantia)
    public StringBuffer GT719_Ncn = new StringBuffer(7 );   //9(07)        Nro.Contrato/Garantia
    public StringBuffer GT719_Seq = new StringBuffer(3 );   //9(03)        Item Contrato
    public StringBuffer GT719_Fol = new StringBuffer(9 );   //9(09)        Folio Imagen (Llave BLB)
    public StringBuffer GT719_Fch = new StringBuffer(8 );   //9(08)        Fecha Imagen
    public StringBuffer GT719_Dsg = new StringBuffer(90);   //X(90)        Titulo Imagen
    public StringBuffer GT719_Tpi = new StringBuffer(3 );   //X(03)        Tipo Imagen (extension)
    public StringBuffer GT719_Fte = new StringBuffer(3 );   //X(03)        Rol Ingresador

    public String getIdr()         {return GT719_Idr.toString();}
    public String getPsw()         {return GT719_Psw.toString();}
    public String getSis()         {return GT719_Sis.toString();}
    public String getNcn()         {return GT719_Ncn.toString();}
    public String getSeq()         {return GT719_Seq.toString();}
    public String getFol()         {return GT719_Fol.toString();}
    public String getFch()         {return GT719_Fch.toString();}
    public String getDsg()         {return GT719_Dsg.toString();}
    public String getTpi()         {return GT719_Tpi.toString();}
    public String getFte()         {return GT719_Fte.toString();}

    public int    igetSeq()        {if (getSeq().trim().equals("")) return 0; else return Integer.parseInt(getSeq()); }
    public int    igetFol()        {if (getFol().trim().equals("")) return 0; else return Integer.parseInt(getFol()); }
  }
  //===============================================================================================================================
  public static Buf_PrmGT719 Inicia_PrmGT719 ()
  {
    Buf_PrmGT719 l_PrmGT719 = new Buf_PrmGT719();
    l_PrmGT719.GT719_Idr.replace(0, l_PrmGT719.GT719_Idr.capacity(), RUTGEN.Blancos(l_PrmGT719.GT719_Idr));
    l_PrmGT719.GT719_Psw.replace(0, l_PrmGT719.GT719_Psw.capacity(), RUTGEN.Blancos(l_PrmGT719.GT719_Psw));
    l_PrmGT719.GT719_Sis.replace(0, l_PrmGT719.GT719_Sis.capacity(), RUTGEN.Blancos(l_PrmGT719.GT719_Sis));
    l_PrmGT719.GT719_Ncn.replace(0, l_PrmGT719.GT719_Ncn.capacity(), RUTGEN.Blancos(l_PrmGT719.GT719_Ncn));
    l_PrmGT719.GT719_Seq.replace(0, l_PrmGT719.GT719_Seq.capacity(), RUTGEN.Blancos(l_PrmGT719.GT719_Seq));
    l_PrmGT719.GT719_Fol.replace(0, l_PrmGT719.GT719_Fol.capacity(), RUTGEN.Blancos(l_PrmGT719.GT719_Fol));
    l_PrmGT719.GT719_Fch.replace(0, l_PrmGT719.GT719_Fch.capacity(), RUTGEN.Blancos(l_PrmGT719.GT719_Fch));
    l_PrmGT719.GT719_Dsg.replace(0, l_PrmGT719.GT719_Dsg.capacity(), RUTGEN.Blancos(l_PrmGT719.GT719_Dsg));
    l_PrmGT719.GT719_Tpi.replace(0, l_PrmGT719.GT719_Tpi.capacity(), RUTGEN.Blancos(l_PrmGT719.GT719_Tpi));
    l_PrmGT719.GT719_Fte.replace(0, l_PrmGT719.GT719_Fte.capacity(), RUTGEN.Blancos(l_PrmGT719.GT719_Fte));
    return l_PrmGT719;
  }
  //===============================================================================================================================
  public static Buf_PrmGT719 LSet_A_PrmGT719(String pcStr)
  {
    int p = 0;
    Buf_PrmGT719 l_PrmGT719 = new Buf_PrmGT719();
    l_PrmGT719.GT719_Idr.replace(0, l_PrmGT719.GT719_Idr.capacity(), pcStr.substring(p, p + l_PrmGT719.GT719_Idr.capacity())); p = p + l_PrmGT719.GT719_Idr.capacity();
    l_PrmGT719.GT719_Psw.replace(0, l_PrmGT719.GT719_Psw.capacity(), pcStr.substring(p, p + l_PrmGT719.GT719_Psw.capacity())); p = p + l_PrmGT719.GT719_Psw.capacity();
    l_PrmGT719.GT719_Sis.replace(0, l_PrmGT719.GT719_Sis.capacity(), pcStr.substring(p, p + l_PrmGT719.GT719_Sis.capacity())); p = p + l_PrmGT719.GT719_Sis.capacity();
    l_PrmGT719.GT719_Ncn.replace(0, l_PrmGT719.GT719_Ncn.capacity(), pcStr.substring(p, p + l_PrmGT719.GT719_Ncn.capacity())); p = p + l_PrmGT719.GT719_Ncn.capacity();
    l_PrmGT719.GT719_Seq.replace(0, l_PrmGT719.GT719_Seq.capacity(), pcStr.substring(p, p + l_PrmGT719.GT719_Seq.capacity())); p = p + l_PrmGT719.GT719_Seq.capacity();
    l_PrmGT719.GT719_Fol.replace(0, l_PrmGT719.GT719_Fol.capacity(), pcStr.substring(p, p + l_PrmGT719.GT719_Fol.capacity())); p = p + l_PrmGT719.GT719_Fol.capacity();
    l_PrmGT719.GT719_Fch.replace(0, l_PrmGT719.GT719_Fch.capacity(), pcStr.substring(p, p + l_PrmGT719.GT719_Fch.capacity())); p = p + l_PrmGT719.GT719_Fch.capacity();
    l_PrmGT719.GT719_Dsg.replace(0, l_PrmGT719.GT719_Dsg.capacity(), pcStr.substring(p, p + l_PrmGT719.GT719_Dsg.capacity())); p = p + l_PrmGT719.GT719_Dsg.capacity();
    l_PrmGT719.GT719_Tpi.replace(0, l_PrmGT719.GT719_Tpi.capacity(), pcStr.substring(p, p + l_PrmGT719.GT719_Tpi.capacity())); p = p + l_PrmGT719.GT719_Tpi.capacity();
    l_PrmGT719.GT719_Fte.replace(0, l_PrmGT719.GT719_Fte.capacity(), pcStr.substring(p, p + l_PrmGT719.GT719_Fte.capacity())); p = p + l_PrmGT719.GT719_Fte.capacity();
    return l_PrmGT719;
  }
  //-------------------------------------------------------------------------------------------
  public static String LSet_De_PrmGT719 (Buf_PrmGT719 p_PrmGT719)
  {
    String pcStr = "";
    pcStr = pcStr + p_PrmGT719.GT719_Idr.toString();
    pcStr = pcStr + p_PrmGT719.GT719_Psw.toString();
    pcStr = pcStr + p_PrmGT719.GT719_Sis.toString();
    pcStr = pcStr + p_PrmGT719.GT719_Ncn.toString();
    pcStr = pcStr + p_PrmGT719.GT719_Seq.toString();
    pcStr = pcStr + p_PrmGT719.GT719_Fol.toString();
    pcStr = pcStr + p_PrmGT719.GT719_Fch.toString();
    pcStr = pcStr + p_PrmGT719.GT719_Dsg.toString();
    pcStr = pcStr + p_PrmGT719.GT719_Tpi.toString();
    pcStr = pcStr + p_PrmGT719.GT719_Fte.toString();
    return pcStr;
  }
  //===============================================================================================================================
  public static String Actualiza_IXI(String pcIDR, BF_IXI.Bff_Ixi pIxi, FileItem pcFile) throws Exception
  {
    BF_LOB lob = new BF_LOB();
    int rCode      = -1;
    String lcKEY   = pIxi.getKey();
    String lcFolio = pIxi.getFol().trim();
    DBConnection dbConn = null;
    Log.debug("PRMGT719.Actualiza_IXI[IDR]:["+pcIDR+"] [ixiKEY]:[" + lcKEY + "]");
    try
       {
         dbConn = BasePeer.beginTransaction(Constants.BASE_GAR);
         dbConn.setAutoCommit(false);
         if (pcIDR.equals("E") || pcIDR.equals("a"))
            {
              lob.mantieneBLOB("D", dbConn, pIxi.getFol(), pIxi.getTpi(), pcFile);
              rCode = BF_IXI.Delete_IXI(dbConn, pIxi);
            }
         if (pcIDR.equals("A"))
            { lcFolio = BF_PRA.Asigna_Folio(dbConn, "FOLIO-BLOBS", 9); }
         if (pcIDR.equals("A") || pcIDR.equals("a"))
            {
              lob.mantieneBLOB("A", dbConn, lcFolio, pIxi.getTpi(), pcFile);
              RUTGEN.MoverA(pIxi.Ixi_Fol, lcFolio);
              rCode = BF_IXI.Graba_IXI(dbConn, pIxi);
            }
         if (pcIDR.equals("U"))
            { rCode = BF_IXI.Update_IXI(dbConn, pIxi); }
         BasePeer.commitTransaction(dbConn);
       }
    catch (Exception e)
       {
         BasePeer.rollBackTransaction(dbConn);
         e.printStackTrace();
         String MsgErr = "PRMGT719.Actualiza_IXI Key:[" + lcKEY + "]  Error: [" + e.toString() + "]";
         Log.error(MsgErr);
         throw new fhtException(MsgErr);
      }
    Log.debug("PRMGT719.Actualiza_IXI: [" + lcKEY + "]  [OK]");
    return lcFolio;
  }
  //===============================================================================================================================
}