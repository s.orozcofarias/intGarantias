// Source File Name:   MSGGT459.java

package com.intGarantias.modules.global;

import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.util.Formateo;

import java.util.*;
import java.lang.*;
import org.apache.turbine.util.Log;

public class MSGGT459
{
  public static int g_Max_GT459 = 25;
  //---------------------------------------------------------------------------------------
  public static class Bff_MsgGT459
  {
    public StringBuffer Gif_Fol = new StringBuffer(7 );       //9(07)         7  Folio                      
    public StringBuffer Gif_Dsg = new StringBuffer(30);       //X(30)        37  Descripcion                    
    public StringBuffer Gif_Fch = new StringBuffer(8 );       //9(08)        45  Fecha               
    public StringBuffer Gif_Tpi = new StringBuffer(3 );       //X(03)        48  Tipo Imagen

    public String getFol()       {return Gif_Fol.toString();}
    public String getDsg()       {return Gif_Dsg.toString();}
    public String getFch()       {return Gif_Fch.toString();}
    public String getTpi()       {return Gif_Tpi.toString();}

    public Formateo f = new Formateo();
    public String fgetFch()      {return f.FormatDate(Gif_Fch.toString());}
  }
  //---------------------------------------------------------------------------------------
  public static class Buf_MsgGT459
  {
    public StringBuffer GT459_Idr = new StringBuffer(1 );
    public StringBuffer GT459_Sis = new StringBuffer(3 );
    public StringBuffer GT459_Ncn = new StringBuffer(7 );
    public StringBuffer GT459_Seq = new StringBuffer(3 );
    public StringBuffer GT459_Nro = new StringBuffer(3 );
    public Vector       GT459_Tab = new Vector();
    public StringBuffer GT459_Fol = new StringBuffer(7 );
    public StringBuffer GT459_Dsg = new StringBuffer(30);
    public StringBuffer GT459_Fch = new StringBuffer(8 );
    public StringBuffer GT459_Tpi = new StringBuffer(3 );
    public StringBuffer GT459_Psw = new StringBuffer(1 );
    public StringBuffer GT459_Idx = new StringBuffer(3 );

    public String getIdr()         {return GT459_Idr.toString();}
    public String getSis()         {return GT459_Sis.toString();}
    public String getNcn()         {return GT459_Ncn.toString();}
    public String getSeq()         {return GT459_Seq.toString();}
    public String getNro()         {return GT459_Nro.toString();}
    public Vector getTab()         {return GT459_Tab;}
    public String getFol()         {return GT459_Fol.toString();}
    public String getDsg()         {return GT459_Dsg.toString();}
    public String getFch()         {return GT459_Fch.toString();}
    public String getTpi()         {return GT459_Tpi.toString();}
    public String getPsw()         {return GT459_Psw.toString();}
    public String getIdx()         {return GT459_Idx.toString();}

    public Formateo f = new Formateo();
    public String fgetFch()      {return f.FormatDate(GT459_Fch.toString());}
    public int    igetIdx()        {
    	                            if (getIdx().trim().equals(""))
    	                               { return Integer.parseInt("1"); }
    	                            else
    	                               { return Integer.parseInt(GT459_Idx.toString()); }
    	                           }
  }
  //---------------------------------------------------------------------------------------
  public static Buf_MsgGT459 Inicia_MsgGT459()
  {
    Buf_MsgGT459 l_MsgGT459 = new Buf_MsgGT459();
    l_MsgGT459.GT459_Idr.replace(0, l_MsgGT459.GT459_Idr.capacity(), RUTGEN.Blancos(1 ));
    l_MsgGT459.GT459_Sis.replace(0, l_MsgGT459.GT459_Sis.capacity(), RUTGEN.Blancos(3 ));
    l_MsgGT459.GT459_Ncn.replace(0, l_MsgGT459.GT459_Ncn.capacity(), RUTGEN.Blancos(7 ));
    l_MsgGT459.GT459_Seq.replace(0, l_MsgGT459.GT459_Seq.capacity(), RUTGEN.Blancos(3 ));
    l_MsgGT459.GT459_Nro.replace(0, l_MsgGT459.GT459_Nro.capacity(), RUTGEN.Blancos(3 ));
    int p = 0;
    for (int i=0; i<g_Max_GT459; i++)
        {
          Bff_MsgGT459 Tap = new Bff_MsgGT459();
          Tap.Gif_Fol.replace(0, Tap.Gif_Fol.capacity(), RUTGEN.Blancos(7 ));
          Tap.Gif_Dsg.replace(0, Tap.Gif_Dsg.capacity(), RUTGEN.Blancos(30));
          Tap.Gif_Fch.replace(0, Tap.Gif_Fch.capacity(), RUTGEN.Blancos(8 ));
          Tap.Gif_Tpi.replace(0, Tap.Gif_Tpi.capacity(), RUTGEN.Blancos(3 ));
          l_MsgGT459.GT459_Tab.add(Tap);        
        }
    l_MsgGT459.GT459_Fol.replace(0, l_MsgGT459.GT459_Fol.capacity(), RUTGEN.Blancos(7 ));
    l_MsgGT459.GT459_Dsg.replace(0, l_MsgGT459.GT459_Dsg.capacity(), RUTGEN.Blancos(30));
    l_MsgGT459.GT459_Fch.replace(0, l_MsgGT459.GT459_Fch.capacity(), RUTGEN.Blancos(8 ));
    l_MsgGT459.GT459_Tpi.replace(0, l_MsgGT459.GT459_Tpi.capacity(), RUTGEN.Blancos(3 ));
    l_MsgGT459.GT459_Psw.replace(0, l_MsgGT459.GT459_Psw.capacity(), RUTGEN.Blancos(1 ));
    l_MsgGT459.GT459_Idx.replace(0, l_MsgGT459.GT459_Idx.capacity(), RUTGEN.Blancos(3 ));
    return l_MsgGT459;
  }
  //---------------------------------------------------------------------------------------
  static public Buf_MsgGT459 LSet_A_MsgGT459(String pcStr)
  {
    Buf_MsgGT459 l_MsgGT459 = new Buf_MsgGT459();
    MSGGT459 vMsgGT459 = new MSGGT459();
    Vector vDatos = vMsgGT459.LSet_A_vMsgGT459(pcStr);
    l_MsgGT459 = (MSGGT459.Buf_MsgGT459)vDatos.elementAt(0);
    return l_MsgGT459;
  }
  //---------------------------------------------------------------------------------------
  public Vector LSet_A_vMsgGT459(String pcStr)
  {
    Vector vec = new Vector();
    int p = 0;
    Buf_MsgGT459 l_MsgGT459 = new Buf_MsgGT459();                            
    l_MsgGT459.GT459_Idr.append(pcStr.substring(p, p + l_MsgGT459.GT459_Idr.capacity())); p = p + l_MsgGT459.GT459_Idr.capacity();
    l_MsgGT459.GT459_Sis.append(pcStr.substring(p, p + l_MsgGT459.GT459_Sis.capacity())); p = p + l_MsgGT459.GT459_Sis.capacity();
    l_MsgGT459.GT459_Ncn.append(pcStr.substring(p, p + l_MsgGT459.GT459_Ncn.capacity())); p = p + l_MsgGT459.GT459_Ncn.capacity();
    l_MsgGT459.GT459_Seq.append(pcStr.substring(p, p + l_MsgGT459.GT459_Seq.capacity())); p = p + l_MsgGT459.GT459_Seq.capacity();
    l_MsgGT459.GT459_Nro.append(pcStr.substring(p, p + l_MsgGT459.GT459_Nro.capacity())); p = p + l_MsgGT459.GT459_Nro.capacity();
    for (int i=0; i<g_Max_GT459; i++)
//    for (int i=0; i<Integer.parseInt(l_MsgGT459.GT459_Nro.toString()); i++)
        {
          Bff_MsgGT459 Tap = new Bff_MsgGT459();
          Tap.Gif_Fol.append(pcStr.substring(p, p + Tap.Gif_Fol.capacity())); p = p + Tap.Gif_Fol.capacity();
          Tap.Gif_Dsg.append(pcStr.substring(p, p + Tap.Gif_Dsg.capacity())); p = p + Tap.Gif_Dsg.capacity();
          Tap.Gif_Fch.append(pcStr.substring(p, p + Tap.Gif_Fch.capacity())); p = p + Tap.Gif_Fch.capacity();
          Tap.Gif_Tpi.append(pcStr.substring(p, p + Tap.Gif_Tpi.capacity())); p = p + Tap.Gif_Tpi.capacity();
          l_MsgGT459.GT459_Tab.add(Tap);
        }
    l_MsgGT459.GT459_Fol.append(pcStr.substring(p, p + l_MsgGT459.GT459_Fol.capacity())); p = p + l_MsgGT459.GT459_Fol.capacity();
    l_MsgGT459.GT459_Dsg.append(pcStr.substring(p, p + l_MsgGT459.GT459_Dsg.capacity())); p = p + l_MsgGT459.GT459_Dsg.capacity();
    l_MsgGT459.GT459_Fch.append(pcStr.substring(p, p + l_MsgGT459.GT459_Fch.capacity())); p = p + l_MsgGT459.GT459_Fch.capacity();
    l_MsgGT459.GT459_Tpi.append(pcStr.substring(p, p + l_MsgGT459.GT459_Tpi.capacity())); p = p + l_MsgGT459.GT459_Tpi.capacity();
    l_MsgGT459.GT459_Psw.append(pcStr.substring(p, p + l_MsgGT459.GT459_Psw.capacity())); p = p + l_MsgGT459.GT459_Psw.capacity();
    l_MsgGT459.GT459_Idx.append(pcStr.substring(p, p + l_MsgGT459.GT459_Idx.capacity())); p = p + l_MsgGT459.GT459_Idx.capacity();
    vec.add (l_MsgGT459);
    return vec;
  }
  //---------------------------------------------------------------------------------------
  static public String LSet_De_MsgGT459(Buf_MsgGT459 p_MsgGT459)
  {
    String pcStr = "";
    pcStr = pcStr + p_MsgGT459.GT459_Idr.toString();
    pcStr = pcStr + p_MsgGT459.GT459_Sis.toString();
    pcStr = pcStr + p_MsgGT459.GT459_Ncn.toString();
    pcStr = pcStr + p_MsgGT459.GT459_Seq.toString();
    pcStr = pcStr + p_MsgGT459.GT459_Nro.toString();
    for (int i=0; i<g_Max_GT459; i++)
//    for (int i=0; i<p_MsgGT459.GT459_Tab.size(); i++)
        {
          pcStr = pcStr + ((Bff_MsgGT459)p_MsgGT459.GT459_Tab.elementAt(i)).Gif_Fol.toString();
          pcStr = pcStr + ((Bff_MsgGT459)p_MsgGT459.GT459_Tab.elementAt(i)).Gif_Dsg.toString();
          pcStr = pcStr + ((Bff_MsgGT459)p_MsgGT459.GT459_Tab.elementAt(i)).Gif_Fch.toString();
          pcStr = pcStr + ((Bff_MsgGT459)p_MsgGT459.GT459_Tab.elementAt(i)).Gif_Tpi.toString();
        }
    pcStr = pcStr + p_MsgGT459.GT459_Fol.toString();
    pcStr = pcStr + p_MsgGT459.GT459_Dsg.toString();
    pcStr = pcStr + p_MsgGT459.GT459_Fch.toString();
    pcStr = pcStr + p_MsgGT459.GT459_Tpi.toString();
    pcStr = pcStr + p_MsgGT459.GT459_Psw.toString();
    pcStr = pcStr + p_MsgGT459.GT459_Idx.toString();
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
}