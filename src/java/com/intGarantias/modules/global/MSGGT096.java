// Source File Name:   MSGGT096.java

package com.intGarantias.modules.global;

import java.util.Vector;

import com.FHTServlet.modules.global.RUTGEN;

public class MSGGT096 {
	public static class Buf_MsgGT096 {

		public String getIdr() {
			return GT096_Idr.toString();
		}

		public String getCli() {
			return GT096_Cli.toString();
		}

		public String getRtn() {
			return GT096_Rtn.toString();
		}

		public String getMse() {
			return GT096_Mse.toString();
		}

		public String getNcl() {
			return GT096_Ncl.toString();
		}

		public String getTpr() {
			return GT096_Tpr.toString();
		}

		public String getDir() {
			return GT096_Dir.toString();
		}

		public String getCmn() {
			return GT096_Cmn.toString();
		}

		public String getTfc() {
			return GT096_Tfc.toString();
		}

		public String getNtr() {
			return GT096_Ntr.toString();
		}

		public String getNcn() {
			return GT096_Ncn.toString();
		}

		public String getFec() {
			return GT096_Fec.toString();
		}

		public String getVuf() {
			return GT096_Vuf.toString();
		}

		public String getViv() {
			return GT096_Viv.toString();
		}

		public String getVtc() {
			return GT096_Vtc.toString();
		}

		/***********************************/
		public String getGfu() {
			return GT096_Gfu.toString();
		}

		/***********************************/
		public StringBuffer GT096_Idr;
		public StringBuffer GT096_Cli;
		public StringBuffer GT096_Rtn;
		public StringBuffer GT096_Mse;
		public StringBuffer GT096_Ncl;
		public StringBuffer GT096_Tpr;
		public StringBuffer GT096_Dir;
		public StringBuffer GT096_Cmn;
		public StringBuffer GT096_Tfc;
		public StringBuffer GT096_Ntr;
		public StringBuffer GT096_Ncn;
		public StringBuffer GT096_Fec;
		public StringBuffer GT096_Vuf;
		public StringBuffer GT096_Viv;
		public StringBuffer GT096_Vtc;
		/***********************************/
		public StringBuffer GT096_Gfu;

		/***********************************/
		public Buf_MsgGT096() {
			GT096_Idr = new StringBuffer(1);
			GT096_Cli = new StringBuffer(10);
			GT096_Rtn = new StringBuffer(1);
			GT096_Mse = new StringBuffer(25);
			GT096_Ncl = new StringBuffer(40);
			GT096_Tpr = new StringBuffer(1);
			GT096_Dir = new StringBuffer(107);
			GT096_Cmn = new StringBuffer(11);
			GT096_Tfc = new StringBuffer(10);
			GT096_Ntr = new StringBuffer(7);
			GT096_Ncn = new StringBuffer(7);
			GT096_Fec = new StringBuffer(8);
			GT096_Vuf = new StringBuffer(11);
			GT096_Viv = new StringBuffer(11);
			GT096_Vtc = new StringBuffer(11);
			/***********************************/
			GT096_Gfu = new StringBuffer(1);
			/***********************************/
		}
	}

	public MSGGT096() {
	}

	public static Buf_MsgGT096 Inicia_MsgGT096() {
		Buf_MsgGT096 l_MsgGT096 = new Buf_MsgGT096();
		l_MsgGT096.GT096_Idr.replace(0, l_MsgGT096.GT096_Idr.capacity(),
				RUTGEN.Blancos(1));
		l_MsgGT096.GT096_Cli.replace(0, l_MsgGT096.GT096_Cli.capacity(),
				RUTGEN.Blancos(10));
		l_MsgGT096.GT096_Rtn.replace(0, l_MsgGT096.GT096_Rtn.capacity(),
				RUTGEN.Blancos(1));
		l_MsgGT096.GT096_Mse.replace(0, l_MsgGT096.GT096_Mse.capacity(),
				RUTGEN.Blancos(25));
		l_MsgGT096.GT096_Ncl.replace(0, l_MsgGT096.GT096_Ncl.capacity(),
				RUTGEN.Blancos(40));
		l_MsgGT096.GT096_Tpr.replace(0, l_MsgGT096.GT096_Tpr.capacity(),
				RUTGEN.Blancos(1));
		l_MsgGT096.GT096_Dir.replace(0, l_MsgGT096.GT096_Dir.capacity(),
				RUTGEN.Blancos(107));
		l_MsgGT096.GT096_Cmn.replace(0, l_MsgGT096.GT096_Cmn.capacity(),
				RUTGEN.Blancos(11));
		l_MsgGT096.GT096_Tfc.replace(0, l_MsgGT096.GT096_Tfc.capacity(),
				RUTGEN.Blancos(10));
		l_MsgGT096.GT096_Ntr.replace(0, l_MsgGT096.GT096_Ntr.capacity(),
				RUTGEN.Blancos(7));
		l_MsgGT096.GT096_Ncn.replace(0, l_MsgGT096.GT096_Ncn.capacity(),
				RUTGEN.Blancos(7));
		l_MsgGT096.GT096_Fec.replace(0, l_MsgGT096.GT096_Fec.capacity(),
				RUTGEN.Blancos(8));
		l_MsgGT096.GT096_Vuf.replace(0, l_MsgGT096.GT096_Vuf.capacity(),
				RUTGEN.Blancos(11));
		l_MsgGT096.GT096_Viv.replace(0, l_MsgGT096.GT096_Viv.capacity(),
				RUTGEN.Blancos(11));
		l_MsgGT096.GT096_Vtc.replace(0, l_MsgGT096.GT096_Vtc.capacity(),
				RUTGEN.Blancos(11));
		/***********************************/
		l_MsgGT096.GT096_Gfu.replace(0, l_MsgGT096.GT096_Gfu.capacity(),
				RUTGEN.Blancos(1));
		/***********************************/
		return l_MsgGT096;
	}

	public static Buf_MsgGT096 LSet_A_MsgGT096(String pcStr) {
		Buf_MsgGT096 l_MsgGT096 = new Buf_MsgGT096();
		MSGGT096 vMsgGT096 = new MSGGT096();
		Vector vDatos = vMsgGT096.LSet_A_vMsgGT096(pcStr);
		l_MsgGT096 = (Buf_MsgGT096) vDatos.elementAt(0);
		return l_MsgGT096;
	}

	public Vector LSet_A_vMsgGT096(String pcStr) {
		Vector vec = new Vector();
		int p = 0;
		Buf_MsgGT096 l_MsgGT096 = new Buf_MsgGT096();
		l_MsgGT096.GT096_Idr.append(pcStr.substring(p,
				p + l_MsgGT096.GT096_Idr.capacity()));
		p += l_MsgGT096.GT096_Idr.capacity();
		l_MsgGT096.GT096_Cli.append(pcStr.substring(p,
				p + l_MsgGT096.GT096_Cli.capacity()));
		p += l_MsgGT096.GT096_Cli.capacity();
		l_MsgGT096.GT096_Rtn.append(pcStr.substring(p,
				p + l_MsgGT096.GT096_Rtn.capacity()));
		p += l_MsgGT096.GT096_Rtn.capacity();
		l_MsgGT096.GT096_Mse.append(pcStr.substring(p,
				p + l_MsgGT096.GT096_Mse.capacity()));
		p += l_MsgGT096.GT096_Mse.capacity();
		l_MsgGT096.GT096_Ncl.append(pcStr.substring(p,
				p + l_MsgGT096.GT096_Ncl.capacity()));
		p += l_MsgGT096.GT096_Ncl.capacity();
		l_MsgGT096.GT096_Tpr.append(pcStr.substring(p,
				p + l_MsgGT096.GT096_Tpr.capacity()));
		p += l_MsgGT096.GT096_Tpr.capacity();
		l_MsgGT096.GT096_Dir.append(pcStr.substring(p,
				p + l_MsgGT096.GT096_Dir.capacity()));
		p += l_MsgGT096.GT096_Dir.capacity();
		l_MsgGT096.GT096_Cmn.append(pcStr.substring(p,
				p + l_MsgGT096.GT096_Cmn.capacity()));
		p += l_MsgGT096.GT096_Cmn.capacity();
		l_MsgGT096.GT096_Tfc.append(pcStr.substring(p,
				p + l_MsgGT096.GT096_Tfc.capacity()));
		p += l_MsgGT096.GT096_Tfc.capacity();
		l_MsgGT096.GT096_Ntr.append(pcStr.substring(p,
				p + l_MsgGT096.GT096_Ntr.capacity()));
		p += l_MsgGT096.GT096_Ntr.capacity();
		l_MsgGT096.GT096_Ncn.append(pcStr.substring(p,
				p + l_MsgGT096.GT096_Ncn.capacity()));
		p += l_MsgGT096.GT096_Ncn.capacity();
		l_MsgGT096.GT096_Fec.append(pcStr.substring(p,
				p + l_MsgGT096.GT096_Fec.capacity()));
		p += l_MsgGT096.GT096_Fec.capacity();
		l_MsgGT096.GT096_Vuf.append(pcStr.substring(p,
				p + l_MsgGT096.GT096_Vuf.capacity()));
		p += l_MsgGT096.GT096_Vuf.capacity();
		l_MsgGT096.GT096_Viv.append(pcStr.substring(p,
				p + l_MsgGT096.GT096_Viv.capacity()));
		p += l_MsgGT096.GT096_Viv.capacity();
		l_MsgGT096.GT096_Vtc.append(pcStr.substring(p,
				p + l_MsgGT096.GT096_Vtc.capacity()));
		p += l_MsgGT096.GT096_Vtc.capacity();
		/***********************************/
		l_MsgGT096.GT096_Gfu.append(pcStr.substring(p,
				p + l_MsgGT096.GT096_Gfu.capacity()));
		p += l_MsgGT096.GT096_Gfu.capacity();
		/***********************************/
		vec.add(l_MsgGT096);
		return vec;
	}

	public static String LSet_De_MsgGT096(Buf_MsgGT096 p_MsgGT096) {
		String pcStr = "";
		pcStr = pcStr + p_MsgGT096.GT096_Idr.toString();
		pcStr = pcStr + p_MsgGT096.GT096_Cli.toString();
		pcStr = pcStr + p_MsgGT096.GT096_Rtn.toString();
		pcStr = pcStr + p_MsgGT096.GT096_Mse.toString();
		pcStr = pcStr + p_MsgGT096.GT096_Ncl.toString();
		pcStr = pcStr + p_MsgGT096.GT096_Tpr.toString();
		pcStr = pcStr + p_MsgGT096.GT096_Dir.toString();
		pcStr = pcStr + p_MsgGT096.GT096_Cmn.toString();
		pcStr = pcStr + p_MsgGT096.GT096_Tfc.toString();
		pcStr = pcStr + p_MsgGT096.GT096_Ntr.toString();
		pcStr = pcStr + p_MsgGT096.GT096_Ncn.toString();
		pcStr = pcStr + p_MsgGT096.GT096_Fec.toString();
		pcStr = pcStr + p_MsgGT096.GT096_Vuf.toString();
		pcStr = pcStr + p_MsgGT096.GT096_Viv.toString();
		pcStr = pcStr + p_MsgGT096.GT096_Vtc.toString();
		/***********************************/
		pcStr = pcStr + p_MsgGT096.GT096_Gfu.toString();
		/***********************************/
		return pcStr;
	}
}
