// Source File Name:   MSGGT734.java

package com.intGarantias.modules.global;

import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.util.Formateo;

import java.util.*;
import java.lang.*;
import org.apache.turbine.util.Log;

public class MSGGT734
{
  //---------------------------------------------------------------------------------------
  public static class Buf_MsgGT734
  {
    public StringBuffer GT734_Idr = new StringBuffer(1  );  //X(01)        1 Idr Host
    public StringBuffer GT734_Gti = new StringBuffer(10 );  //X(10)       11 Id.Garantia a buscar
    public StringBuffer GT734_Seq = new StringBuffer(3  );  //9(03)       14 Secuencia
    public StringBuffer GT734_Itb = new StringBuffer(2  );  //X(02)       16 Tipo Bien (HP/PD)
    public StringBuffer GT734_Itg = new StringBuffer(5  );  //X(05)       21 Tipo Gtia CodGen
    public StringBuffer GT734_Dpc = new StringBuffer(10 );  //X(10)       31 Producto Contable
    public StringBuffer GT734_Dsb = new StringBuffer(30 );  //X(30)       61 Descripcion Bien
    public StringBuffer GT734_Sqd = new StringBuffer(3  );  //9(03)       64 Secuencia
    public StringBuffer GT734_Frv = new StringBuffer(8  );  //9(08)       72 FRevalorizacion
    public StringBuffer GT734_Vur = new StringBuffer(9  );  //9(07)V9(2)  81 VConv <Trj> a FRV
    public StringBuffer GT734_Udd = new StringBuffer(9  );  //9(07)V9(2)  90 Unidades (Superficie)
    public StringBuffer GT734_Umd = new StringBuffer(2  );  //X(02)       92 UMedida (M2/HA)
    public StringBuffer GT734_Umt = new StringBuffer(1  );  //X(01)       93 UMonetaria (P/U/D)
    public StringBuffer GT734_Pum = new StringBuffer(15 );  //9(11)V9(4) 108 PUnitario UMedida
    public StringBuffer GT734_Vtu = new StringBuffer(15 );  //9(11)V9(4) 123 VTasacion UMedida
    public StringBuffer GT734_Vum = new StringBuffer(9  );  //9(07)V9(2) 132 VConversion UMonetaria
    public StringBuffer GT734_Vtt = new StringBuffer(15 );  //9(11)V9(4) 147 Valor Tasacion <Trj>
    public StringBuffer GT734_Vts = new StringBuffer(15 );  //9(13)V9(2) 162 Valor Tasacion <Mnd>
    public StringBuffer GT734_Tuf = new StringBuffer(15 );  //9(11)V9(4) 177 Tasacion UF
    public StringBuffer GT734_Ttc = new StringBuffer(15 );  //9(13)V9(2) 192 Tasacion TC
    public StringBuffer GT734_Tps = new StringBuffer(15 );  //9(13)V9(2) 207 Tasacion $
    public StringBuffer GT734_Pct = new StringBuffer(5  );  //9(03)V9(2) 212 Tasacion $
    public StringBuffer GT734_Vcn = new StringBuffer(15 );  //9(13)V9(2) 227 VContable Moneda
    public StringBuffer GT734_Vlq = new StringBuffer(15 );  //9(13)V9(2) 242 VLiquidacion Moneda
    public StringBuffer GT734_Vtb = new StringBuffer(15 );  //9(13)V9(2) 257 VRemate Moneda
    public StringBuffer GT734_Vsg = new StringBuffer(15 );  //9(11)V9(4) 272 VSeguro <Trj>
    public StringBuffer GT734_Fvt = new StringBuffer(8  );  //9(08)      280 FVcto Tasacion

    public String getIdr()         {return GT734_Idr.toString();}
    public String getGti()         {return GT734_Gti.toString();}
    public String getSeq()         {return GT734_Seq.toString();}
    public String getItb()         {return GT734_Itb.toString();}
    public String getItg()         {return GT734_Itg.toString();}
    public String getDpc()         {return GT734_Dpc.toString();}
    public String getDsb()         {return GT734_Dsb.toString();}
    public String getSqd()         {return GT734_Sqd.toString();}
    public String getFrv()         {return GT734_Frv.toString();}
    public String getVur()         {return GT734_Vur.toString();}
    public String getUdd()         {return GT734_Udd.toString();}
    public String getUmd()         {return GT734_Umd.toString();}
    public String getUmt()         {return GT734_Umt.toString();}
    public String getPum()         {return GT734_Pum.toString();}
    public String getVtu()         {return GT734_Vtu.toString();}
    public String getVum()         {return GT734_Vum.toString();}
    public String getVtt()         {return GT734_Vtt.toString();}
    public String getVts()         {return GT734_Vts.toString();}
    public String getTuf()         {return GT734_Tuf.toString();}
    public String getTtc()         {return GT734_Ttc.toString();}
    public String getTps()         {return GT734_Tps.toString();}
    public String getPct()         {return GT734_Pct.toString();}
    public String getVcn()         {return GT734_Vcn.toString();}
    public String getVlq()         {return GT734_Vlq.toString();}
    public String getVtb()         {return GT734_Vtb.toString();}
    public String getVsg()         {return GT734_Vsg.toString();}
    public String getFvt()         {return GT734_Fvt.toString();}

    public Formateo f = new Formateo();
  //public String fgetFts()        {return f.FormatDate(getFts());}
  //public String fgetVuf()        {return f.Formato(getVuf(),"9.999.999",2,2,',');}
  //public String fgetVtc()        {return f.Formato(getVtc(),"9.999.999",2,2,',');}
  //public String fgetUdd()        {return f.Formato(getUdd(),"9.999.999",2,2,',');}
  //public String fgetVsg()        {return f.Formato(getVsg(),"99.999.999.999",4,4,',');}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_MsgGT734 Inicia_MsgGT734 ()
  {
    Buf_MsgGT734 l_MsgGT734 = new Buf_MsgGT734();
    l_MsgGT734.GT734_Idr.replace(0, l_MsgGT734.GT734_Idr.capacity(), RUTGEN.Blancos(1  ));
    l_MsgGT734.GT734_Gti.replace(0, l_MsgGT734.GT734_Gti.capacity(), RUTGEN.Blancos(10 ));
    l_MsgGT734.GT734_Seq.replace(0, l_MsgGT734.GT734_Seq.capacity(), RUTGEN.Blancos(3  ));
    l_MsgGT734.GT734_Itb.replace(0, l_MsgGT734.GT734_Itb.capacity(), RUTGEN.Blancos(2  ));
    l_MsgGT734.GT734_Itg.replace(0, l_MsgGT734.GT734_Itg.capacity(), RUTGEN.Blancos(5  ));
    l_MsgGT734.GT734_Dpc.replace(0, l_MsgGT734.GT734_Dpc.capacity(), RUTGEN.Blancos(10 ));
    l_MsgGT734.GT734_Dsb.replace(0, l_MsgGT734.GT734_Dsb.capacity(), RUTGEN.Blancos(30 ));
    l_MsgGT734.GT734_Sqd.replace(0, l_MsgGT734.GT734_Sqd.capacity(), RUTGEN.Blancos(3  ));
    l_MsgGT734.GT734_Frv.replace(0, l_MsgGT734.GT734_Frv.capacity(), RUTGEN.Blancos(8  ));
    l_MsgGT734.GT734_Vur.replace(0, l_MsgGT734.GT734_Vur.capacity(), RUTGEN.Blancos(9  ));
    l_MsgGT734.GT734_Udd.replace(0, l_MsgGT734.GT734_Udd.capacity(), RUTGEN.Blancos(9  ));
    l_MsgGT734.GT734_Umd.replace(0, l_MsgGT734.GT734_Umd.capacity(), RUTGEN.Blancos(2  ));
    l_MsgGT734.GT734_Umt.replace(0, l_MsgGT734.GT734_Umt.capacity(), RUTGEN.Blancos(1  ));
    l_MsgGT734.GT734_Pum.replace(0, l_MsgGT734.GT734_Pum.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT734.GT734_Vtu.replace(0, l_MsgGT734.GT734_Vtu.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT734.GT734_Vum.replace(0, l_MsgGT734.GT734_Vum.capacity(), RUTGEN.Blancos(9  ));
    l_MsgGT734.GT734_Vtt.replace(0, l_MsgGT734.GT734_Vtt.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT734.GT734_Vts.replace(0, l_MsgGT734.GT734_Vts.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT734.GT734_Tuf.replace(0, l_MsgGT734.GT734_Tuf.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT734.GT734_Ttc.replace(0, l_MsgGT734.GT734_Ttc.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT734.GT734_Tps.replace(0, l_MsgGT734.GT734_Tps.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT734.GT734_Pct.replace(0, l_MsgGT734.GT734_Pct.capacity(), RUTGEN.Blancos(5  ));
    l_MsgGT734.GT734_Vcn.replace(0, l_MsgGT734.GT734_Vcn.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT734.GT734_Vlq.replace(0, l_MsgGT734.GT734_Vlq.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT734.GT734_Vtb.replace(0, l_MsgGT734.GT734_Vtb.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT734.GT734_Vsg.replace(0, l_MsgGT734.GT734_Vsg.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT734.GT734_Fvt.replace(0, l_MsgGT734.GT734_Fvt.capacity(), RUTGEN.Blancos(8  ));
    return l_MsgGT734;
  }
  //---------------------------------------------------------------------------------------
  public static Buf_MsgGT734 LSet_A_MsgGT734(String pcStr)
  {
    Buf_MsgGT734 l_MsgGT734 = new Buf_MsgGT734();
    Vector vDatos = LSet_A_vMsgGT734(pcStr);
    l_MsgGT734 = (Buf_MsgGT734)vDatos.elementAt(0);
    return l_MsgGT734;
  }
  //---------------------------------------------------------------------------------------
  public static Vector LSet_A_vMsgGT734(String pcStr)
  {
    Vector vec = new Vector();
    int p = 0;
    String lcData = "";
    Buf_MsgGT734 l_MsgGT734 = new Buf_MsgGT734();
    l_MsgGT734.GT734_Idr.replace(0, l_MsgGT734.GT734_Idr.capacity(), pcStr.substring(p, p + l_MsgGT734.GT734_Idr.capacity())); p = p + l_MsgGT734.GT734_Idr.capacity();
    l_MsgGT734.GT734_Gti.replace(0, l_MsgGT734.GT734_Gti.capacity(), pcStr.substring(p, p + l_MsgGT734.GT734_Gti.capacity())); p = p + l_MsgGT734.GT734_Gti.capacity();
    l_MsgGT734.GT734_Seq.replace(0, l_MsgGT734.GT734_Seq.capacity(), pcStr.substring(p, p + l_MsgGT734.GT734_Seq.capacity())); p = p + l_MsgGT734.GT734_Seq.capacity();
    l_MsgGT734.GT734_Itb.replace(0, l_MsgGT734.GT734_Itb.capacity(), pcStr.substring(p, p + l_MsgGT734.GT734_Itb.capacity())); p = p + l_MsgGT734.GT734_Itb.capacity();
    l_MsgGT734.GT734_Itg.replace(0, l_MsgGT734.GT734_Itg.capacity(), pcStr.substring(p, p + l_MsgGT734.GT734_Itg.capacity())); p = p + l_MsgGT734.GT734_Itg.capacity();
    l_MsgGT734.GT734_Dpc.replace(0, l_MsgGT734.GT734_Dpc.capacity(), pcStr.substring(p, p + l_MsgGT734.GT734_Dpc.capacity())); p = p + l_MsgGT734.GT734_Dpc.capacity();
    l_MsgGT734.GT734_Dsb.replace(0, l_MsgGT734.GT734_Dsb.capacity(), pcStr.substring(p, p + l_MsgGT734.GT734_Dsb.capacity())); p = p + l_MsgGT734.GT734_Dsb.capacity();
    l_MsgGT734.GT734_Sqd.replace(0, l_MsgGT734.GT734_Sqd.capacity(), pcStr.substring(p, p + l_MsgGT734.GT734_Sqd.capacity())); p = p + l_MsgGT734.GT734_Sqd.capacity();
    l_MsgGT734.GT734_Frv.replace(0, l_MsgGT734.GT734_Frv.capacity(), pcStr.substring(p, p + l_MsgGT734.GT734_Frv.capacity())); p = p + l_MsgGT734.GT734_Frv.capacity();
    l_MsgGT734.GT734_Vur.replace(0, l_MsgGT734.GT734_Vur.capacity(), pcStr.substring(p, p + l_MsgGT734.GT734_Vur.capacity())); p = p + l_MsgGT734.GT734_Vur.capacity();
    l_MsgGT734.GT734_Udd.replace(0, l_MsgGT734.GT734_Udd.capacity(), pcStr.substring(p, p + l_MsgGT734.GT734_Udd.capacity())); p = p + l_MsgGT734.GT734_Udd.capacity();
    l_MsgGT734.GT734_Umd.replace(0, l_MsgGT734.GT734_Umd.capacity(), pcStr.substring(p, p + l_MsgGT734.GT734_Umd.capacity())); p = p + l_MsgGT734.GT734_Umd.capacity();
    l_MsgGT734.GT734_Umt.replace(0, l_MsgGT734.GT734_Umt.capacity(), pcStr.substring(p, p + l_MsgGT734.GT734_Umt.capacity())); p = p + l_MsgGT734.GT734_Umt.capacity();
    l_MsgGT734.GT734_Pum.replace(0, l_MsgGT734.GT734_Pum.capacity(), pcStr.substring(p, p + l_MsgGT734.GT734_Pum.capacity())); p = p + l_MsgGT734.GT734_Pum.capacity();
    l_MsgGT734.GT734_Vtu.replace(0, l_MsgGT734.GT734_Vtu.capacity(), pcStr.substring(p, p + l_MsgGT734.GT734_Vtu.capacity())); p = p + l_MsgGT734.GT734_Vtu.capacity();
    l_MsgGT734.GT734_Vum.replace(0, l_MsgGT734.GT734_Vum.capacity(), pcStr.substring(p, p + l_MsgGT734.GT734_Vum.capacity())); p = p + l_MsgGT734.GT734_Vum.capacity();
    l_MsgGT734.GT734_Vtt.replace(0, l_MsgGT734.GT734_Vtt.capacity(), pcStr.substring(p, p + l_MsgGT734.GT734_Vtt.capacity())); p = p + l_MsgGT734.GT734_Vtt.capacity();
    l_MsgGT734.GT734_Vts.replace(0, l_MsgGT734.GT734_Vts.capacity(), pcStr.substring(p, p + l_MsgGT734.GT734_Vts.capacity())); p = p + l_MsgGT734.GT734_Vts.capacity();
    l_MsgGT734.GT734_Tuf.replace(0, l_MsgGT734.GT734_Tuf.capacity(), pcStr.substring(p, p + l_MsgGT734.GT734_Tuf.capacity())); p = p + l_MsgGT734.GT734_Tuf.capacity();
    l_MsgGT734.GT734_Ttc.replace(0, l_MsgGT734.GT734_Ttc.capacity(), pcStr.substring(p, p + l_MsgGT734.GT734_Ttc.capacity())); p = p + l_MsgGT734.GT734_Ttc.capacity();
    l_MsgGT734.GT734_Tps.replace(0, l_MsgGT734.GT734_Tps.capacity(), pcStr.substring(p, p + l_MsgGT734.GT734_Tps.capacity())); p = p + l_MsgGT734.GT734_Tps.capacity();
    l_MsgGT734.GT734_Pct.replace(0, l_MsgGT734.GT734_Pct.capacity(), pcStr.substring(p, p + l_MsgGT734.GT734_Pct.capacity())); p = p + l_MsgGT734.GT734_Pct.capacity();
    l_MsgGT734.GT734_Vcn.replace(0, l_MsgGT734.GT734_Vcn.capacity(), pcStr.substring(p, p + l_MsgGT734.GT734_Vcn.capacity())); p = p + l_MsgGT734.GT734_Vcn.capacity();
    l_MsgGT734.GT734_Vlq.replace(0, l_MsgGT734.GT734_Vlq.capacity(), pcStr.substring(p, p + l_MsgGT734.GT734_Vlq.capacity())); p = p + l_MsgGT734.GT734_Vlq.capacity();
    l_MsgGT734.GT734_Vtb.replace(0, l_MsgGT734.GT734_Vtb.capacity(), pcStr.substring(p, p + l_MsgGT734.GT734_Vtb.capacity())); p = p + l_MsgGT734.GT734_Vtb.capacity();
    l_MsgGT734.GT734_Vsg.replace(0, l_MsgGT734.GT734_Vsg.capacity(), pcStr.substring(p, p + l_MsgGT734.GT734_Vsg.capacity())); p = p + l_MsgGT734.GT734_Vsg.capacity();
    l_MsgGT734.GT734_Fvt.replace(0, l_MsgGT734.GT734_Fvt.capacity(), pcStr.substring(p, p + l_MsgGT734.GT734_Fvt.capacity())); p = p + l_MsgGT734.GT734_Fvt.capacity();
    vec.add(l_MsgGT734);
    return vec;
  }
  //---------------------------------------------------------------------------------------
  public static String LSet_De_MsgGT734 (Buf_MsgGT734 p_MsgGT734)
  {
    String pcStr = "";
    pcStr = pcStr + p_MsgGT734.GT734_Idr.toString();
    pcStr = pcStr + p_MsgGT734.GT734_Gti.toString();
    pcStr = pcStr + p_MsgGT734.GT734_Seq.toString();
    pcStr = pcStr + p_MsgGT734.GT734_Itb.toString();
    pcStr = pcStr + p_MsgGT734.GT734_Itg.toString();
    pcStr = pcStr + p_MsgGT734.GT734_Dpc.toString();
    pcStr = pcStr + p_MsgGT734.GT734_Dsb.toString();
    pcStr = pcStr + p_MsgGT734.GT734_Sqd.toString();
    pcStr = pcStr + p_MsgGT734.GT734_Frv.toString();
    pcStr = pcStr + p_MsgGT734.GT734_Vur.toString();
    pcStr = pcStr + p_MsgGT734.GT734_Udd.toString();
    pcStr = pcStr + p_MsgGT734.GT734_Umd.toString();
    pcStr = pcStr + p_MsgGT734.GT734_Umt.toString();
    pcStr = pcStr + p_MsgGT734.GT734_Pum.toString();
    pcStr = pcStr + p_MsgGT734.GT734_Vtu.toString();
    pcStr = pcStr + p_MsgGT734.GT734_Vum.toString();
    pcStr = pcStr + p_MsgGT734.GT734_Vtt.toString();
    pcStr = pcStr + p_MsgGT734.GT734_Vts.toString();
    pcStr = pcStr + p_MsgGT734.GT734_Tuf.toString();
    pcStr = pcStr + p_MsgGT734.GT734_Ttc.toString();
    pcStr = pcStr + p_MsgGT734.GT734_Tps.toString();
    pcStr = pcStr + p_MsgGT734.GT734_Pct.toString();
    pcStr = pcStr + p_MsgGT734.GT734_Vcn.toString();
    pcStr = pcStr + p_MsgGT734.GT734_Vlq.toString();
    pcStr = pcStr + p_MsgGT734.GT734_Vtb.toString();
    pcStr = pcStr + p_MsgGT734.GT734_Vsg.toString();
    pcStr = pcStr + p_MsgGT734.GT734_Fvt.toString();
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
}