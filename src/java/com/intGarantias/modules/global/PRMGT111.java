// Source File Name:   PRMGT111.java

package com.intGarantias.modules.global;

import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.util.Formateo;

import java.util.*;
import java.lang.*;
import org.apache.turbine.util.Log;

public class PRMGT111
{
  public static class Buf_PrmGT111
  {
    public StringBuffer GT111_Idr     = new StringBuffer(1);    //X(01)        1 Idr.Accion Q
    public StringBuffer GT111_Rtn     = new StringBuffer(2);    //X(02)        3 Retorno OK/NK
    public StringBuffer GT111_Dcn     = new StringBuffer(5);    //X(05)        8 Tipo Contrato/Garantia
    public StringBuffer GT111_Ndc     = new StringBuffer(30);   //X(30)       38 Desc.Contrato/Garantia
    public StringBuffer GT111_Tmn     = new StringBuffer(3);    //X(03)       41 Tipo Moneda
    public StringBuffer GT111_Trj     = new StringBuffer(3);    //X(03)       44 Tipo Reajustabilidad
    public StringBuffer GT111_Dcm     = new StringBuffer(1);    //9(01)       45 Decimales
    public StringBuffer GT111_Prp     = new StringBuffer(5);    //X(05)       50 Propietario Contrato
    public StringBuffer GT111_Grt_Ggr = new StringBuffer(3);    //X(03)       53 Grupo Garantia (HIP/PRD/FZA/WAR/VFI/ACC/OTR)
    public StringBuffer GT111_Grt_Itg = new StringBuffer(2);    //X(02)       55 Idr Tipo Garantia (NA/PI/PA/LV/LF/MV/MF)
    public StringBuffer GT111_Grt_Tob = new StringBuffer(1);    //X(01)       56 Tipo Obligacion (G/E/Seleccionable)
    public StringBuffer GT111_Grt_Plm = new StringBuffer(1);    //X(01)       57 Permite Limitacion (S/N)
    public StringBuffer GT111_Grt_Usg = new StringBuffer(5);    //X(05)       62 Codigo Uso Gtia
    public StringBuffer GT111_Grt_Nug = new StringBuffer(30);   //X(30)       92 Nombre Uso Gtia         
    public StringBuffer GT111_Grt_Pct = new StringBuffer(5);    //9(03)V9(2)  97 %Castigo SBIF
    public String getIdr()             {return GT111_Idr.toString();}
    public String getRtn()             {return GT111_Rtn.toString();}
    public String getDcn()             {return GT111_Dcn.toString();}
    public String getNdc()             {return GT111_Ndc.toString();}
    public String getTmn()             {return GT111_Tmn.toString();}
    public String getTrj()             {return GT111_Trj.toString();}
    public String getDcm()             {return GT111_Dcm.toString();}
    public String getPrp()             {return GT111_Prp.toString();}
    public String getGrt_Ggr()         {return GT111_Grt_Ggr.toString();}
    public String getGrt_Itg()         {return GT111_Grt_Itg.toString();}
    public String getGrt_Tob()         {return GT111_Grt_Tob.toString();}
    public String getGrt_Plm()         {return GT111_Grt_Plm.toString();}
    public String getGrt_Usg()         {return GT111_Grt_Usg.toString();}
    public String getGrt_Nug()         {return GT111_Grt_Nug.toString();}
    public String getGrt_Pct()         {return GT111_Grt_Pct.toString();}
  }
}