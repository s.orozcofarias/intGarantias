// Source File Name:   MSGGT451.java

package com.intGarantias.modules.global;

import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.util.Formateo;

import java.util.*;
import java.lang.*;
import org.apache.turbine.util.Log;

public class MSGGT451
{
  public static int g_Max_GT451 = 26;
  //---------------------------------------------------------------------------------------
  public static class Bff_MsgGT451
  {
    public StringBuffer GT451_Cli = new StringBuffer(10);  //'X(10)       10 Cli Avl/Grd Mge     
    public StringBuffer GT451_Rel = new StringBuffer(5);   //'X(05)       15 Relacion Avl/Grd    
    public StringBuffer GT451_Ncl = new StringBuffer(40);  //'X(40)       55 Nombre Avl/Grd      
    public StringBuffer GT451_Prc = new StringBuffer(5);   //'9(03)V9(2)  60 Porcentaje Avl/Grd  
    public StringBuffer GT451_Isg = new StringBuffer(1);   //'X(01)       61 Idr.Asegurado       

    public String getCli(){return GT451_Cli.toString();}
    public String getRel(){return GT451_Rel.toString();}
    public String getNcl(){return GT451_Ncl.toString();}
    public String getPrc(){return GT451_Prc.toString();}
    public String getIsg(){return GT451_Isg.toString();}

    public Formateo f = new Formateo();
    public String fgetCli451(){return f.fmtRut(GT451_Cli.toString());}
  }
  //---------------------------------------------------------------------------------------
  public static class Buf_MsgGT451
  {
    public StringBuffer GT451_Idr = new StringBuffer(1);   //'X(01)        1 Inicio/Especifico/Avanza/Retrocede  
    public StringBuffer GT451_Gti = new StringBuffer(10);  //'X(10)       11 Id.Credito a buscar                 
    public StringBuffer GT451_Nro = new StringBuffer(2);   //'9(02)       13 Nro Avales                          
    public Vector       GT451_Tag = new Vector();          //'X(1586)   1599 Avales Involucrados                
    
    public String getIdr(){return GT451_Idr.toString();}
    public String getGti(){return GT451_Gti.toString();}
    public String getNro(){return GT451_Nro.toString();}
    public Vector getTag(){return GT451_Tag;}
      
  }
  //---------------------------------------------------------------------------------------
  public static Buf_MsgGT451 Inicia_MsgGT451()
  {
    Buf_MsgGT451 l_MsgGT451 = new Buf_MsgGT451();
    l_MsgGT451.GT451_Idr.replace(0,l_MsgGT451.GT451_Idr.capacity(), RUTGEN.Blancos(1));
    l_MsgGT451.GT451_Gti.replace(0,l_MsgGT451.GT451_Gti.capacity(), RUTGEN.Blancos(10));
    l_MsgGT451.GT451_Nro.replace(0,l_MsgGT451.GT451_Nro.capacity(), RUTGEN.Blancos(2));
    int p=0;
    for(int i=0; i<g_Max_GT451; i++)
    {
      Bff_MsgGT451 Tap = new Bff_MsgGT451();
      Tap.GT451_Cli.replace(0,Tap.GT451_Cli.capacity(), RUTGEN.Blancos(10));
      Tap.GT451_Rel.replace(0,Tap.GT451_Rel.capacity(), RUTGEN.Blancos(5));
      Tap.GT451_Ncl.replace(0,Tap.GT451_Ncl.capacity(), RUTGEN.Blancos(40));
      Tap.GT451_Prc.replace(0,Tap.GT451_Prc.capacity(), RUTGEN.Blancos(5));
      Tap.GT451_Isg.replace(0,Tap.GT451_Isg.capacity(), RUTGEN.Blancos(1));
      l_MsgGT451.GT451_Tag.add(Tap);
    }
    return l_MsgGT451;
  }
  //---------------------------------------------------------------------------------------
  static public Buf_MsgGT451 LSet_A_MsgGT451(String pcStr)
  {
    Buf_MsgGT451 l_MsgGT451 = new Buf_MsgGT451();
    MSGGT451 vMsgGT451 = new MSGGT451();
    Vector vDatos = vMsgGT451.LSet_A_vMsgGT451(pcStr);
    l_MsgGT451 = (MSGGT451.Buf_MsgGT451)vDatos.elementAt(0);
    return l_MsgGT451;
  }
  //---------------------------------------------------------------------------------------
  public Vector LSet_A_vMsgGT451(String pcStr)
  {
    Vector vec = new Vector();
    int p = 0;
    Buf_MsgGT451 l_MsgGT451 = new Buf_MsgGT451();                            
    l_MsgGT451.GT451_Idr.append(pcStr.substring(p, p + l_MsgGT451.GT451_Idr.capacity())); p = p + l_MsgGT451.GT451_Idr.capacity();
    l_MsgGT451.GT451_Gti.append(pcStr.substring(p, p + l_MsgGT451.GT451_Gti.capacity())); p = p + l_MsgGT451.GT451_Gti.capacity();
    l_MsgGT451.GT451_Nro.append(pcStr.substring(p, p + l_MsgGT451.GT451_Nro.capacity())); p = p + l_MsgGT451.GT451_Nro.capacity();
    for (int i=0; i<Integer.parseInt(l_MsgGT451.GT451_Nro.toString()); i++)
        {
          Bff_MsgGT451 Tap = new Bff_MsgGT451();
          Tap.GT451_Cli.append(pcStr.substring(p, p + Tap.GT451_Cli.capacity())); p = p + Tap.GT451_Cli.capacity();
          Tap.GT451_Rel.append(pcStr.substring(p, p + Tap.GT451_Rel.capacity())); p = p + Tap.GT451_Rel.capacity();
          Tap.GT451_Ncl.append(pcStr.substring(p, p + Tap.GT451_Ncl.capacity())); p = p + Tap.GT451_Ncl.capacity();
          Tap.GT451_Prc.append(pcStr.substring(p, p + Tap.GT451_Prc.capacity())); p = p + Tap.GT451_Prc.capacity();
          Tap.GT451_Isg.append(pcStr.substring(p, p + Tap.GT451_Isg.capacity())); p = p + Tap.GT451_Isg.capacity();
          l_MsgGT451.GT451_Tag.add(Tap);
        }
    vec.add (l_MsgGT451);            
    return vec;
  }
  //---------------------------------------------------------------------------------------
  static public String LSet_De_MsgGT451(Buf_MsgGT451 p_MsgGT451)
  {
    String pcStr = "";
    pcStr = pcStr + p_MsgGT451.GT451_Idr.toString();
    pcStr = pcStr + p_MsgGT451.GT451_Gti.toString();
    pcStr = pcStr + p_MsgGT451.GT451_Nro.toString();
    for(int i=0; i<p_MsgGT451.GT451_Tag.size(); i++)
    {
      pcStr = pcStr + ((Bff_MsgGT451)p_MsgGT451.GT451_Tag.elementAt(i)).GT451_Cli.toString();
      pcStr = pcStr + ((Bff_MsgGT451)p_MsgGT451.GT451_Tag.elementAt(i)).GT451_Rel.toString();
      pcStr = pcStr + ((Bff_MsgGT451)p_MsgGT451.GT451_Tag.elementAt(i)).GT451_Ncl.toString();
      pcStr = pcStr + ((Bff_MsgGT451)p_MsgGT451.GT451_Tag.elementAt(i)).GT451_Prc.toString();
      pcStr = pcStr + ((Bff_MsgGT451)p_MsgGT451.GT451_Tag.elementAt(i)).GT451_Isg.toString();
    }
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
}