// Source File Name:   MSGGT677.java

package com.intGarantias.modules.global;

public class MSGGT677 {
	public static class Buf_MsgGT677 {

		public String getIdr() {
			return GT677_Idr.toString();
		}

		public String getGfu() {
			return GT677_Gfu.toString();
		}

		public String getGbs() {
			return GT677_Gbs.toString();
		}

		public String getEst() {
			return GT677_Est.toString();
		}

		public StringBuffer GT677_Idr;
		public StringBuffer GT677_Gfu;
		public StringBuffer GT677_Gbs;
		public StringBuffer GT677_Est;

		public Buf_MsgGT677() {
			GT677_Idr = new StringBuffer(1);
			GT677_Gfu = new StringBuffer(10);
			GT677_Gbs = new StringBuffer(13);
			GT677_Est = new StringBuffer(5);
		}
	}

	public MSGGT677() {
	}

	public static String LSet_De_MsgGT677(Buf_MsgGT677 p_MsgGT677) {
		String pcStr = "";
		pcStr = pcStr + p_MsgGT677.GT677_Idr.toString();
		pcStr = pcStr + p_MsgGT677.GT677_Gfu.toString();
		pcStr = pcStr + p_MsgGT677.GT677_Gbs.toString();
		pcStr = pcStr + p_MsgGT677.GT677_Est.toString();
		return pcStr;
	}
}
