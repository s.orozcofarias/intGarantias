// Source File Name:   MSGGT405.java

package com.intGarantias.modules.global;

import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.util.Formateo;

import java.util.*;
import java.lang.*;
import org.apache.turbine.util.Log;

public class MSGGT405
{
  public static int g_Max_GT405 = 64;
  //---------------------------------------------------------------------------------------
  public static class Bff_MsgGT405
  {
    public StringBuffer GT405_Rel = new StringBuffer(5 );   //X(05)        5 Relacion Cli-Cnr (KEY)
    public StringBuffer GT405_Cli = new StringBuffer(10);   //X(10)       15 Id. Cliente (KEY)
    public StringBuffer GT405_Ncl = new StringBuffer(40);   //X(40)       55 Nombre Cliente

    public String getRel()         {return GT405_Rel.toString();}
    public String getCli()         {return GT405_Cli.toString();}
    public String getNcl()         {return GT405_Ncl.toString();}
  }
  //---------------------------------------------------------------------------------------
  public static class Buf_MsgGT405
  {
    public StringBuffer GT405_Idr = new StringBuffer(1 );
    public StringBuffer GT405_Gti = new StringBuffer(10);
    public StringBuffer GT405_Nro = new StringBuffer(3 );
    public Vector       GT405_Tap = new Vector();

    public String getIdr()         {return GT405_Idr.toString();}
    public String getGti()         {return GT405_Gti.toString();}
    public String getNro()         {return GT405_Nro.toString();}
    public Vector getTab()         {return GT405_Tap;}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_MsgGT405 Inicia_MsgGT405()
  {
    Buf_MsgGT405 l_MsgGT405 = new Buf_MsgGT405();
    l_MsgGT405.GT405_Idr.replace(0,l_MsgGT405.GT405_Idr.capacity(), RUTGEN.Blancos(1 ));
    l_MsgGT405.GT405_Gti.replace(0,l_MsgGT405.GT405_Gti.capacity(), RUTGEN.Blancos(10));
    l_MsgGT405.GT405_Nro.replace(0,l_MsgGT405.GT405_Nro.capacity(), RUTGEN.Blancos(3 ));
    int p=0;
    for(int i=0; i<g_Max_GT405; i++)
    {
      Bff_MsgGT405 Tap = new Bff_MsgGT405();
      Tap.GT405_Rel.replace(0,Tap.GT405_Rel.capacity(), RUTGEN.Blancos(5 ));
      Tap.GT405_Cli.replace(0,Tap.GT405_Cli.capacity(), RUTGEN.Blancos(10));
      Tap.GT405_Ncl.replace(0,Tap.GT405_Ncl.capacity(), RUTGEN.Blancos(40));
      l_MsgGT405.GT405_Tap.add(Tap);
    }
    return l_MsgGT405;
  }
  //---------------------------------------------------------------------------------------
  static public Buf_MsgGT405 LSet_A_MsgGT405(String pcStr)
  {
    Buf_MsgGT405 l_MsgGT405 = new Buf_MsgGT405();
    MSGGT405 vMsgGT405 = new MSGGT405();
    Vector vDatos = vMsgGT405.LSet_A_vMsgGT405(pcStr);
    l_MsgGT405 = (MSGGT405.Buf_MsgGT405)vDatos.elementAt(0);
    return l_MsgGT405;
  }
  //---------------------------------------------------------------------------------------
  public Vector LSet_A_vMsgGT405(String pcStr)
  {
    Vector vec = new Vector();
    int p = 0;
    Buf_MsgGT405 l_MsgGT405 = new Buf_MsgGT405();                            
    l_MsgGT405.GT405_Idr.append(pcStr.substring(p, p + l_MsgGT405.GT405_Idr.capacity())); p = p + l_MsgGT405.GT405_Idr.capacity();
    l_MsgGT405.GT405_Gti.append(pcStr.substring(p, p + l_MsgGT405.GT405_Gti.capacity())); p = p + l_MsgGT405.GT405_Gti.capacity();
    l_MsgGT405.GT405_Nro.append(pcStr.substring(p, p + l_MsgGT405.GT405_Nro.capacity())); p = p + l_MsgGT405.GT405_Nro.capacity();
    for (int i=0; i<Integer.parseInt(l_MsgGT405.GT405_Nro.toString()); i++)
        {
          Bff_MsgGT405 Tap = new Bff_MsgGT405();
          Tap.GT405_Rel.append(pcStr.substring(p, p + Tap.GT405_Rel.capacity())); p = p + Tap.GT405_Rel.capacity();
          Tap.GT405_Cli.append(pcStr.substring(p, p + Tap.GT405_Cli.capacity())); p = p + Tap.GT405_Cli.capacity();
          Tap.GT405_Ncl.append(pcStr.substring(p, p + Tap.GT405_Ncl.capacity())); p = p + Tap.GT405_Ncl.capacity();
          if (Tap.GT405_Cli.toString().trim().equals("")) { break; }
          l_MsgGT405.GT405_Tap.add(Tap);
        }
    vec.add (l_MsgGT405);
    return vec;
  }
  //---------------------------------------------------------------------------------------
  static public String LSet_De_MsgGT405(Buf_MsgGT405 p_MsgGT405)
  {
    String pcStr = "";
    pcStr = pcStr + p_MsgGT405.GT405_Idr.toString();
    pcStr = pcStr + p_MsgGT405.GT405_Gti.toString();
    pcStr = pcStr + p_MsgGT405.GT405_Nro.toString();
    for(int i=0; i<p_MsgGT405.GT405_Tap.size(); i++)
    {
      pcStr = pcStr + ((Bff_MsgGT405)p_MsgGT405.GT405_Tap.elementAt(i)).GT405_Rel.toString();
      pcStr = pcStr + ((Bff_MsgGT405)p_MsgGT405.GT405_Tap.elementAt(i)).GT405_Cli.toString();
      pcStr = pcStr + ((Bff_MsgGT405)p_MsgGT405.GT405_Tap.elementAt(i)).GT405_Ncl.toString();
    }
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
}