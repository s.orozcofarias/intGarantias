// Source File Name:   MSGGT093.java

package com.intGarantias.modules.global;

import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.util.Formateo;

import java.util.*;
import java.lang.*;
import org.apache.turbine.util.Log;

public class MSGGT093
{
  public static int g_Max_GT093 = 38;
  //---------------------------------------------------------------------------------------
  public static class Bff_MsgGT093
  {
    public StringBuffer GT093_Dpc = new StringBuffer(10);       //X(10)        10  Producto Contable
    public StringBuffer GT093_Dsc = new StringBuffer(30);       //X(30)        40  Descripcion
    public StringBuffer GT093_Pct = new StringBuffer(3 );       //9(03)        43  %Castigo SBIF
    public StringBuffer GT093_Dpp = new StringBuffer(1 );       //X(01)        44  Codigo Tipo Valor

    public String getDpc()         {return GT093_Dpc.toString();}
    public String getDsc()         {return GT093_Dsc.toString();}
    public String getPct()         {return GT093_Pct.toString();}
    public String getDpp()         {return GT093_Dpp.toString();}
  }
  //---------------------------------------------------------------------------------------
  public static class Buf_MsgGT093
  {
    public StringBuffer GT093_Idr = new StringBuffer(1);    //X(01)         1  Idr.Host
    public StringBuffer GT093_Sis = new StringBuffer(3);    //X(03)         4  Codigo Sistema
    public StringBuffer GT093_Dcn = new StringBuffer(5);    //X(05)         9  Codigo Producto
    public StringBuffer GT093_Itb = new StringBuffer(1);    //X(01)        10  Codigo Tipo Gtia
    public StringBuffer GT093_Nro = new StringBuffer(3);    //9(03)        13  Nro.Items
    public Vector       GT093_Tab = new Vector();           //X(1672)    1685  Tabla Items

    public String getIdr()         {return GT093_Idr.toString();}
    public String getSis()         {return GT093_Sis.toString();}
    public String getDcn()         {return GT093_Dcn.toString();}
    public String getItb()         {return GT093_Itb.toString();}
    public String getNro()         {return GT093_Nro.toString();}
    public Vector getTab()         {return GT093_Tab;}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_MsgGT093 Inicia_MsgGT093()
  {
    Buf_MsgGT093 l_MsgGT093 = new Buf_MsgGT093();
    l_MsgGT093.GT093_Idr.replace(0, l_MsgGT093.GT093_Idr.capacity(), RUTGEN.Blancos(1));
    l_MsgGT093.GT093_Sis.replace(0, l_MsgGT093.GT093_Sis.capacity(), RUTGEN.Blancos(3));
    l_MsgGT093.GT093_Dcn.replace(0, l_MsgGT093.GT093_Dcn.capacity(), RUTGEN.Blancos(5));
    l_MsgGT093.GT093_Itb.replace(0, l_MsgGT093.GT093_Itb.capacity(), RUTGEN.Blancos(1));
    l_MsgGT093.GT093_Nro.replace(0, l_MsgGT093.GT093_Nro.capacity(), RUTGEN.Blancos(3));
    int p = 0;
    for (int i=0; i<g_Max_GT093; i++)
        {
          Bff_MsgGT093 Tap = new Bff_MsgGT093();
          Tap.GT093_Dpc.replace(0, Tap.GT093_Dpc.capacity(), RUTGEN.Blancos(10));
          Tap.GT093_Dsc.replace(0, Tap.GT093_Dsc.capacity(), RUTGEN.Blancos(30));
          Tap.GT093_Pct.replace(0, Tap.GT093_Pct.capacity(), RUTGEN.Blancos(3 ));
          Tap.GT093_Dpp.replace(0, Tap.GT093_Dpp.capacity(), RUTGEN.Blancos(1 ));
          l_MsgGT093.GT093_Tab.add(Tap);
        }
    return l_MsgGT093;
  }
  //---------------------------------------------------------------------------------------
  static public Buf_MsgGT093 LSet_A_MsgGT093(String pcStr)
  {
    Buf_MsgGT093 l_MsgGT093 = new Buf_MsgGT093();
    MSGGT093 vMsgGT093 = new MSGGT093();
    Vector vDatos = vMsgGT093.LSet_A_vMsgGT093(pcStr);
    l_MsgGT093 = (MSGGT093.Buf_MsgGT093)vDatos.elementAt(0);
    return l_MsgGT093;
  }
  //---------------------------------------------------------------------------------------
  public Vector LSet_A_vMsgGT093(String pcStr)
  {
    Vector vec = new Vector();
    int p = 0;
    Buf_MsgGT093 l_MsgGT093 = new Buf_MsgGT093();
    l_MsgGT093.GT093_Idr.append(pcStr.substring(p, p + l_MsgGT093.GT093_Idr.capacity())); p = p + l_MsgGT093.GT093_Idr.capacity();
    l_MsgGT093.GT093_Sis.append(pcStr.substring(p, p + l_MsgGT093.GT093_Sis.capacity())); p = p + l_MsgGT093.GT093_Sis.capacity();
    l_MsgGT093.GT093_Dcn.append(pcStr.substring(p, p + l_MsgGT093.GT093_Dcn.capacity())); p = p + l_MsgGT093.GT093_Dcn.capacity();
    l_MsgGT093.GT093_Itb.append(pcStr.substring(p, p + l_MsgGT093.GT093_Itb.capacity())); p = p + l_MsgGT093.GT093_Itb.capacity();
    l_MsgGT093.GT093_Nro.append(pcStr.substring(p, p + l_MsgGT093.GT093_Nro.capacity())); p = p + l_MsgGT093.GT093_Nro.capacity();
    for (int i=0; i<Integer.parseInt(l_MsgGT093.GT093_Nro.toString()); i++)
        {
          Bff_MsgGT093 Tap = new Bff_MsgGT093();
          Tap.GT093_Dpc.append(pcStr.substring(p, p + Tap.GT093_Dpc.capacity())); p = p + Tap.GT093_Dpc.capacity();
          Tap.GT093_Dsc.append(pcStr.substring(p, p + Tap.GT093_Dsc.capacity())); p = p + Tap.GT093_Dsc.capacity();
          Tap.GT093_Pct.append(pcStr.substring(p, p + Tap.GT093_Pct.capacity())); p = p + Tap.GT093_Pct.capacity();
          Tap.GT093_Dpp.append(pcStr.substring(p, p + Tap.GT093_Dpp.capacity())); p = p + Tap.GT093_Dpp.capacity();
          l_MsgGT093.GT093_Tab.add(Tap);
        }
    vec.add (l_MsgGT093);
    return vec;
  }
  //---------------------------------------------------------------------------------------
  static public String LSet_De_MsgGT093(Buf_MsgGT093 p_MsgGT093)
  {
    String pcStr = "";
    pcStr = pcStr + p_MsgGT093.GT093_Idr.toString();
    pcStr = pcStr + p_MsgGT093.GT093_Sis.toString();
    pcStr = pcStr + p_MsgGT093.GT093_Dcn.toString();
    pcStr = pcStr + p_MsgGT093.GT093_Itb.toString();
    pcStr = pcStr + p_MsgGT093.GT093_Nro.toString();
    for (int i=0; i<p_MsgGT093.GT093_Tab.size(); i++)
        {
          pcStr = pcStr + ((Bff_MsgGT093)p_MsgGT093.GT093_Tab.elementAt(i)).GT093_Dpc.toString();
          pcStr = pcStr + ((Bff_MsgGT093)p_MsgGT093.GT093_Tab.elementAt(i)).GT093_Dsc.toString();
          pcStr = pcStr + ((Bff_MsgGT093)p_MsgGT093.GT093_Tab.elementAt(i)).GT093_Pct.toString();
          pcStr = pcStr + ((Bff_MsgGT093)p_MsgGT093.GT093_Tab.elementAt(i)).GT093_Dpp.toString();
        }
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
}