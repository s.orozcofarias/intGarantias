// Source File Name:   MSGGT425.java

package com.intGarantias.modules.global;

import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.util.Formateo;

import java.util.*;
import java.lang.*;
import org.apache.turbine.util.Log;

public class MSGGT425
{
  //---------------------------------------------------------------------------------------
  public static class Buf_MsgGT425
  {
    public StringBuffer GT425_Idr = new StringBuffer(1  );   //X(01)
    public StringBuffer GT425_Gti = new StringBuffer(10 );   //X(10)
    public StringBuffer GT425_Seq = new StringBuffer(3  );   //9(03)
    public StringBuffer GT425_Sqd = new StringBuffer(3  );   //9(03)
    public StringBuffer GT425_Fts = new StringBuffer(8  );   //9(08)
    public StringBuffer GT425_Vuf = new StringBuffer(9  );   //9(07)V9(2)
    public StringBuffer GT425_Vtc = new StringBuffer(9  );   //9(07)V9(2)
    public StringBuffer GT425_Tuf = new StringBuffer(15 );   //9(11)V9(4)
    public StringBuffer GT425_Ttc = new StringBuffer(15 );   //9(13)V9(2)
    public StringBuffer GT425_Tps = new StringBuffer(15 );   //9(13)V9(2)
    public StringBuffer GT425_Ith = new StringBuffer(2  );   //X(02)
    public StringBuffer GT425_Dsh = new StringBuffer(30 );   //X(30)
    public StringBuffer GT425_Rs1 = new StringBuffer(5  );   //9(05)
    public StringBuffer GT425_Rs2 = new StringBuffer(3  );   //9(03)
    public StringBuffer GT425_Rds = new StringBuffer(20 );   //X(20)
    public StringBuffer GT425_Faf = new StringBuffer(8  );   //9(08)
    public StringBuffer GT425_Vaf = new StringBuffer(15 );   //9(13)V9(2)
    public StringBuffer GT425_Udd = new StringBuffer(9  );   //9(07)V9(2)
    public StringBuffer GT425_Umd = new StringBuffer(2  );   //X(02)
    public StringBuffer GT425_Umt = new StringBuffer(1  );   //X(01)
    public StringBuffer GT425_Pum = new StringBuffer(15 );   //9(11)V9(4)
    public StringBuffer GT425_Vtu = new StringBuffer(15 );   //9(11)V9(4)
    public StringBuffer GT425_Vbu = new StringBuffer(15 );   //9(11)V9(4)
    public StringBuffer GT425_Plq = new StringBuffer(5  );   //9(03)V9(2)
    public StringBuffer GT425_Vlu = new StringBuffer(15 );   //9(11)V9(4)
    public StringBuffer GT425_Vsu = new StringBuffer(15 );   //9(11)V9(4)
    public StringBuffer GT425_Dsc = new StringBuffer(20 );   //X(20)
    public StringBuffer GT425_Obt = new StringBuffer(9  );   //9(09)
    public StringBuffer GT425_Ovb = new StringBuffer(9  );   //9(09)
    public StringBuffer GT425_Uso = new StringBuffer(30 );   //X(30)
    public StringBuffer GT425_Agp = new StringBuffer(30 );   //X(30)
    public StringBuffer GT425_Tct = new StringBuffer(30 );   //X(30)
    public StringBuffer GT425_Cld = new StringBuffer(30 );   //X(30)

    public String getIdr()         {return GT425_Idr.toString();}
    public String getGti()         {return GT425_Gti.toString();}
    public String getSeq()         {return GT425_Seq.toString();}
    public String getSqd()         {return GT425_Sqd.toString();}
    public String getFts()         {return GT425_Fts.toString();}
    public String getVuf()         {return GT425_Vuf.toString();}
    public String getVtc()         {return GT425_Vtc.toString();}
    public String getTuf()         {return GT425_Tuf.toString();}
    public String getTtc()         {return GT425_Ttc.toString();}
    public String getTps()         {return GT425_Tps.toString();}
    public String getIth()         {return GT425_Ith.toString();}
    public String getDsh()         {return GT425_Dsh.toString();}
    public String getRs1()         {return GT425_Rs1.toString();}
    public String getRs2()         {return GT425_Rs2.toString();}
    public String getRds()         {return GT425_Rds.toString();}
    public String getFaf()         {return GT425_Faf.toString();}
    public String getVaf()         {return GT425_Vaf.toString();}
    public String getUdd()         {return GT425_Udd.toString();}
    public String getUmd()         {return GT425_Umd.toString();}
    public String getUmt()         {return GT425_Umt.toString();}
    public String getPum()         {return GT425_Pum.toString();}
    public String getVtu()         {return GT425_Vtu.toString();}
    public String getVbu()         {return GT425_Vbu.toString();}
    public String getPlq()         {return GT425_Plq.toString();}
    public String getVlu()         {return GT425_Vlu.toString();}
    public String getVsu()         {return GT425_Vsu.toString();}
    public String getDsc()         {return GT425_Dsc.toString();}
    public String getObt()         {return GT425_Obt.toString();}
    public String getOvb()         {return GT425_Ovb.toString();}
    public String getUso()         {return GT425_Uso.toString();}
    public String getAgp()         {return GT425_Agp.toString();}
    public String getTct()         {return GT425_Tct.toString();}
    public String getCld()         {return GT425_Cld.toString();}

    public Formateo f = new Formateo();
    public String fgetFts()        {return f.FormatDate(getFts());}
    public String fgetVuf()        {return f.Formato(getVuf(),"9.999.999",2,2,',');}
    public String fgetVtc()        {return f.Formato(getVtc(),"9.999.999",2,2,',');}
    public String fgetTuf()        {return f.Formato(getTuf(),"99.999.999.999",4,4,',');}
    public String fgetTtc()        {return f.Formato(getTtc(),"99.999.999.999",2,2,',');}
    public String fgetTps()        {return f.Formato(getTps(),"99.999.999.999",2,0,',');}
    public String fgetFaf()        {return f.FormatDate(getFaf());}
    public String fgetVaf()        {return f.Formato(getVaf(),"9.999.999.999.999",2,0,',');}
    public String fgetUdd()        {return f.Formato(getUdd(),"9.999.999",2,2,',');}
    public String fgetPum()
                                   {
                                     int Dec = 0;
                                     if (getUmt().equals("P"))  { Dec = 0; }
                                     if (getUmt().equals("U"))  { Dec = 4; }
                                     if (getUmt().equals("D"))  { Dec = 2; }
                                     return f.Formato(getPum(),"99.999.999.999",4,Dec,',');
                                   }
    public String fgetVtu()
                                   {
                                     int Dec = 0;
                                     if (getUmt().equals("P"))  { Dec = 0; }
                                     if (getUmt().equals("U"))  { Dec = 4; }
                                     if (getUmt().equals("D"))  { Dec = 2; }
                                     return f.Formato(getVtu(),"99.999.999.999",4,Dec,',');
                                   }
    public String fgetVbu()
                                   {
                                     int Dec = 0;
                                     if (getUmt().equals("P"))  { Dec = 0; }
                                     if (getUmt().equals("U"))  { Dec = 4; }
                                     if (getUmt().equals("D"))  { Dec = 2; }
                                     return f.Formato(getVbu(),"99.999.999.999",4,Dec,',');
                                   }
    public String fgetPlq()        {return f.Formato(getPlq(),"999",2,2,',');}
    public String fgetVlu()
                                   {
                                     int Dec = 0;
                                     if (getUmt().equals("P"))  { Dec = 0; }
                                     if (getUmt().equals("U"))  { Dec = 4; }
                                     if (getUmt().equals("D"))  { Dec = 2; }
                                     return f.Formato(getVlu(),"99.999.999.999",4,Dec,',');
                                   }
    public String fgetVsu()
                                   {
                                     int Dec = 0;
                                     if (getUmt().equals("P"))  { Dec = 0; }
                                     if (getUmt().equals("U"))  { Dec = 4; }
                                     if (getUmt().equals("D"))  { Dec = 2; }
                                     return f.Formato(getVsu(),"99.999.999.999",4,Dec,',');
                                   }
  }
  //---------------------------------------------------------------------------------------
  public static Buf_MsgGT425 Inicia_MsgGT425 ()
  {
    Buf_MsgGT425 l_MsgGT425 = new Buf_MsgGT425();
    l_MsgGT425.GT425_Idr.replace(0, l_MsgGT425.GT425_Idr.capacity(), RUTGEN.Blancos(1  ));
    l_MsgGT425.GT425_Gti.replace(0, l_MsgGT425.GT425_Gti.capacity(), RUTGEN.Blancos(10 ));
    l_MsgGT425.GT425_Seq.replace(0, l_MsgGT425.GT425_Seq.capacity(), RUTGEN.Blancos(3  ));
    l_MsgGT425.GT425_Sqd.replace(0, l_MsgGT425.GT425_Sqd.capacity(), RUTGEN.Blancos(3  ));
    l_MsgGT425.GT425_Fts.replace(0, l_MsgGT425.GT425_Fts.capacity(), RUTGEN.Blancos(8  ));
    l_MsgGT425.GT425_Vuf.replace(0, l_MsgGT425.GT425_Vuf.capacity(), RUTGEN.Blancos(9  ));
    l_MsgGT425.GT425_Vtc.replace(0, l_MsgGT425.GT425_Vtc.capacity(), RUTGEN.Blancos(9  ));
    l_MsgGT425.GT425_Tuf.replace(0, l_MsgGT425.GT425_Tuf.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT425.GT425_Ttc.replace(0, l_MsgGT425.GT425_Ttc.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT425.GT425_Tps.replace(0, l_MsgGT425.GT425_Tps.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT425.GT425_Ith.replace(0, l_MsgGT425.GT425_Ith.capacity(), RUTGEN.Blancos(2  ));
    l_MsgGT425.GT425_Dsh.replace(0, l_MsgGT425.GT425_Dsh.capacity(), RUTGEN.Blancos(30 ));
    l_MsgGT425.GT425_Rs1.replace(0, l_MsgGT425.GT425_Rs1.capacity(), RUTGEN.Blancos(5  ));
    l_MsgGT425.GT425_Rs2.replace(0, l_MsgGT425.GT425_Rs2.capacity(), RUTGEN.Blancos(3  ));
    l_MsgGT425.GT425_Rds.replace(0, l_MsgGT425.GT425_Rds.capacity(), RUTGEN.Blancos(20 ));
    l_MsgGT425.GT425_Faf.replace(0, l_MsgGT425.GT425_Faf.capacity(), RUTGEN.Blancos(8  ));
    l_MsgGT425.GT425_Vaf.replace(0, l_MsgGT425.GT425_Vaf.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT425.GT425_Udd.replace(0, l_MsgGT425.GT425_Udd.capacity(), RUTGEN.Blancos(9  ));
    l_MsgGT425.GT425_Umd.replace(0, l_MsgGT425.GT425_Umd.capacity(), RUTGEN.Blancos(2  ));
    l_MsgGT425.GT425_Umt.replace(0, l_MsgGT425.GT425_Umt.capacity(), RUTGEN.Blancos(1  ));
    l_MsgGT425.GT425_Pum.replace(0, l_MsgGT425.GT425_Pum.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT425.GT425_Vtu.replace(0, l_MsgGT425.GT425_Vtu.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT425.GT425_Vbu.replace(0, l_MsgGT425.GT425_Vbu.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT425.GT425_Plq.replace(0, l_MsgGT425.GT425_Plq.capacity(), RUTGEN.Blancos(5  ));
    l_MsgGT425.GT425_Vlu.replace(0, l_MsgGT425.GT425_Vlu.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT425.GT425_Vsu.replace(0, l_MsgGT425.GT425_Vsu.capacity(), RUTGEN.Blancos(15 ));
    l_MsgGT425.GT425_Dsc.replace(0, l_MsgGT425.GT425_Dsc.capacity(), RUTGEN.Blancos(20 ));
    l_MsgGT425.GT425_Obt.replace(0, l_MsgGT425.GT425_Obt.capacity(), RUTGEN.Blancos(9  ));
    l_MsgGT425.GT425_Ovb.replace(0, l_MsgGT425.GT425_Ovb.capacity(), RUTGEN.Blancos(9  ));
    l_MsgGT425.GT425_Uso.replace(0, l_MsgGT425.GT425_Uso.capacity(), RUTGEN.Blancos(30 ));
    l_MsgGT425.GT425_Agp.replace(0, l_MsgGT425.GT425_Agp.capacity(), RUTGEN.Blancos(30 ));
    l_MsgGT425.GT425_Tct.replace(0, l_MsgGT425.GT425_Tct.capacity(), RUTGEN.Blancos(30 ));
    l_MsgGT425.GT425_Cld.replace(0, l_MsgGT425.GT425_Cld.capacity(), RUTGEN.Blancos(30 ));
    return l_MsgGT425;
  }
  //---------------------------------------------------------------------------------------
  public static Buf_MsgGT425 LSet_A_MsgGT425(String pcStr)
  {
    Buf_MsgGT425 l_MsgGT425 = new Buf_MsgGT425();
    Vector vDatos = LSet_A_vMsgGT425(pcStr);
    l_MsgGT425 = (Buf_MsgGT425)vDatos.elementAt(0);
    return l_MsgGT425;
  }
  //---------------------------------------------------------------------------------------
  public static Vector LSet_A_vMsgGT425(String pcStr)
  {
    int p = 0;
    Vector vec = new Vector();
    Buf_MsgGT425 l_MsgGT425 = new Buf_MsgGT425();
    l_MsgGT425.GT425_Idr.replace(0, l_MsgGT425.GT425_Idr.capacity(), pcStr.substring(p, p + l_MsgGT425.GT425_Idr.capacity())); p = p + l_MsgGT425.GT425_Idr.capacity();
    l_MsgGT425.GT425_Gti.replace(0, l_MsgGT425.GT425_Gti.capacity(), pcStr.substring(p, p + l_MsgGT425.GT425_Gti.capacity())); p = p + l_MsgGT425.GT425_Gti.capacity();
    l_MsgGT425.GT425_Seq.replace(0, l_MsgGT425.GT425_Seq.capacity(), pcStr.substring(p, p + l_MsgGT425.GT425_Seq.capacity())); p = p + l_MsgGT425.GT425_Seq.capacity();
    l_MsgGT425.GT425_Sqd.replace(0, l_MsgGT425.GT425_Sqd.capacity(), pcStr.substring(p, p + l_MsgGT425.GT425_Sqd.capacity())); p = p + l_MsgGT425.GT425_Sqd.capacity();
    l_MsgGT425.GT425_Fts.replace(0, l_MsgGT425.GT425_Fts.capacity(), pcStr.substring(p, p + l_MsgGT425.GT425_Fts.capacity())); p = p + l_MsgGT425.GT425_Fts.capacity();
    l_MsgGT425.GT425_Vuf.replace(0, l_MsgGT425.GT425_Vuf.capacity(), pcStr.substring(p, p + l_MsgGT425.GT425_Vuf.capacity())); p = p + l_MsgGT425.GT425_Vuf.capacity();
    l_MsgGT425.GT425_Vtc.replace(0, l_MsgGT425.GT425_Vtc.capacity(), pcStr.substring(p, p + l_MsgGT425.GT425_Vtc.capacity())); p = p + l_MsgGT425.GT425_Vtc.capacity();
    l_MsgGT425.GT425_Tuf.replace(0, l_MsgGT425.GT425_Tuf.capacity(), pcStr.substring(p, p + l_MsgGT425.GT425_Tuf.capacity())); p = p + l_MsgGT425.GT425_Tuf.capacity();
    l_MsgGT425.GT425_Ttc.replace(0, l_MsgGT425.GT425_Ttc.capacity(), pcStr.substring(p, p + l_MsgGT425.GT425_Ttc.capacity())); p = p + l_MsgGT425.GT425_Ttc.capacity();
    l_MsgGT425.GT425_Tps.replace(0, l_MsgGT425.GT425_Tps.capacity(), pcStr.substring(p, p + l_MsgGT425.GT425_Tps.capacity())); p = p + l_MsgGT425.GT425_Tps.capacity();
    l_MsgGT425.GT425_Ith.replace(0, l_MsgGT425.GT425_Ith.capacity(), pcStr.substring(p, p + l_MsgGT425.GT425_Ith.capacity())); p = p + l_MsgGT425.GT425_Ith.capacity();
    l_MsgGT425.GT425_Dsh.replace(0, l_MsgGT425.GT425_Dsh.capacity(), pcStr.substring(p, p + l_MsgGT425.GT425_Dsh.capacity())); p = p + l_MsgGT425.GT425_Dsh.capacity();
    l_MsgGT425.GT425_Rs1.replace(0, l_MsgGT425.GT425_Rs1.capacity(), pcStr.substring(p, p + l_MsgGT425.GT425_Rs1.capacity())); p = p + l_MsgGT425.GT425_Rs1.capacity();
    l_MsgGT425.GT425_Rs2.replace(0, l_MsgGT425.GT425_Rs2.capacity(), pcStr.substring(p, p + l_MsgGT425.GT425_Rs2.capacity())); p = p + l_MsgGT425.GT425_Rs2.capacity();
    l_MsgGT425.GT425_Rds.replace(0, l_MsgGT425.GT425_Rds.capacity(), pcStr.substring(p, p + l_MsgGT425.GT425_Rds.capacity())); p = p + l_MsgGT425.GT425_Rds.capacity();
    l_MsgGT425.GT425_Faf.replace(0, l_MsgGT425.GT425_Faf.capacity(), pcStr.substring(p, p + l_MsgGT425.GT425_Faf.capacity())); p = p + l_MsgGT425.GT425_Faf.capacity();
    l_MsgGT425.GT425_Vaf.replace(0, l_MsgGT425.GT425_Vaf.capacity(), pcStr.substring(p, p + l_MsgGT425.GT425_Vaf.capacity())); p = p + l_MsgGT425.GT425_Vaf.capacity();
    l_MsgGT425.GT425_Udd.replace(0, l_MsgGT425.GT425_Udd.capacity(), pcStr.substring(p, p + l_MsgGT425.GT425_Udd.capacity())); p = p + l_MsgGT425.GT425_Udd.capacity();
    l_MsgGT425.GT425_Umd.replace(0, l_MsgGT425.GT425_Umd.capacity(), pcStr.substring(p, p + l_MsgGT425.GT425_Umd.capacity())); p = p + l_MsgGT425.GT425_Umd.capacity();
    l_MsgGT425.GT425_Umt.replace(0, l_MsgGT425.GT425_Umt.capacity(), pcStr.substring(p, p + l_MsgGT425.GT425_Umt.capacity())); p = p + l_MsgGT425.GT425_Umt.capacity();
    l_MsgGT425.GT425_Pum.replace(0, l_MsgGT425.GT425_Pum.capacity(), pcStr.substring(p, p + l_MsgGT425.GT425_Pum.capacity())); p = p + l_MsgGT425.GT425_Pum.capacity();
    l_MsgGT425.GT425_Vtu.replace(0, l_MsgGT425.GT425_Vtu.capacity(), pcStr.substring(p, p + l_MsgGT425.GT425_Vtu.capacity())); p = p + l_MsgGT425.GT425_Vtu.capacity();
    l_MsgGT425.GT425_Vbu.replace(0, l_MsgGT425.GT425_Vbu.capacity(), pcStr.substring(p, p + l_MsgGT425.GT425_Vbu.capacity())); p = p + l_MsgGT425.GT425_Vbu.capacity();
    l_MsgGT425.GT425_Plq.replace(0, l_MsgGT425.GT425_Plq.capacity(), pcStr.substring(p, p + l_MsgGT425.GT425_Plq.capacity())); p = p + l_MsgGT425.GT425_Plq.capacity();
    l_MsgGT425.GT425_Vlu.replace(0, l_MsgGT425.GT425_Vlu.capacity(), pcStr.substring(p, p + l_MsgGT425.GT425_Vlu.capacity())); p = p + l_MsgGT425.GT425_Vlu.capacity();
    l_MsgGT425.GT425_Vsu.replace(0, l_MsgGT425.GT425_Vsu.capacity(), pcStr.substring(p, p + l_MsgGT425.GT425_Vsu.capacity())); p = p + l_MsgGT425.GT425_Vsu.capacity();
    l_MsgGT425.GT425_Dsc.replace(0, l_MsgGT425.GT425_Dsc.capacity(), pcStr.substring(p, p + l_MsgGT425.GT425_Dsc.capacity())); p = p + l_MsgGT425.GT425_Dsc.capacity();
    l_MsgGT425.GT425_Obt.replace(0, l_MsgGT425.GT425_Obt.capacity(), pcStr.substring(p, p + l_MsgGT425.GT425_Obt.capacity())); p = p + l_MsgGT425.GT425_Obt.capacity();
    l_MsgGT425.GT425_Ovb.replace(0, l_MsgGT425.GT425_Ovb.capacity(), pcStr.substring(p, p + l_MsgGT425.GT425_Ovb.capacity())); p = p + l_MsgGT425.GT425_Ovb.capacity();
    l_MsgGT425.GT425_Uso.replace(0, l_MsgGT425.GT425_Uso.capacity(), pcStr.substring(p, p + l_MsgGT425.GT425_Uso.capacity())); p = p + l_MsgGT425.GT425_Uso.capacity();
    l_MsgGT425.GT425_Agp.replace(0, l_MsgGT425.GT425_Agp.capacity(), pcStr.substring(p, p + l_MsgGT425.GT425_Agp.capacity())); p = p + l_MsgGT425.GT425_Agp.capacity();
    l_MsgGT425.GT425_Tct.replace(0, l_MsgGT425.GT425_Tct.capacity(), pcStr.substring(p, p + l_MsgGT425.GT425_Tct.capacity())); p = p + l_MsgGT425.GT425_Tct.capacity();
    l_MsgGT425.GT425_Cld.replace(0, l_MsgGT425.GT425_Cld.capacity(), pcStr.substring(p, p + l_MsgGT425.GT425_Cld.capacity())); p = p + l_MsgGT425.GT425_Cld.capacity();
    vec.add(l_MsgGT425);
    return vec;
  }
  //---------------------------------------------------------------------------------------
  public static String LSet_De_MsgGT425 (Buf_MsgGT425 p_MsgGT425)
  {
    String pcStr = "";
    pcStr = pcStr + p_MsgGT425.GT425_Idr.toString();
    pcStr = pcStr + p_MsgGT425.GT425_Gti.toString();
    pcStr = pcStr + p_MsgGT425.GT425_Seq.toString();
    pcStr = pcStr + p_MsgGT425.GT425_Sqd.toString();
    pcStr = pcStr + p_MsgGT425.GT425_Fts.toString();
    pcStr = pcStr + p_MsgGT425.GT425_Vuf.toString();
    pcStr = pcStr + p_MsgGT425.GT425_Vtc.toString();
    pcStr = pcStr + p_MsgGT425.GT425_Tuf.toString();
    pcStr = pcStr + p_MsgGT425.GT425_Ttc.toString();
    pcStr = pcStr + p_MsgGT425.GT425_Tps.toString();
    pcStr = pcStr + p_MsgGT425.GT425_Ith.toString();
    pcStr = pcStr + p_MsgGT425.GT425_Dsh.toString();
    pcStr = pcStr + p_MsgGT425.GT425_Rs1.toString();
    pcStr = pcStr + p_MsgGT425.GT425_Rs2.toString();
    pcStr = pcStr + p_MsgGT425.GT425_Rds.toString();
    pcStr = pcStr + p_MsgGT425.GT425_Faf.toString();
    pcStr = pcStr + p_MsgGT425.GT425_Vaf.toString();
    pcStr = pcStr + p_MsgGT425.GT425_Udd.toString();
    pcStr = pcStr + p_MsgGT425.GT425_Umd.toString();
    pcStr = pcStr + p_MsgGT425.GT425_Umt.toString();
    pcStr = pcStr + p_MsgGT425.GT425_Pum.toString();
    pcStr = pcStr + p_MsgGT425.GT425_Vtu.toString();
    pcStr = pcStr + p_MsgGT425.GT425_Vbu.toString();
    pcStr = pcStr + p_MsgGT425.GT425_Plq.toString();
    pcStr = pcStr + p_MsgGT425.GT425_Vlu.toString();
    pcStr = pcStr + p_MsgGT425.GT425_Vsu.toString();
    pcStr = pcStr + p_MsgGT425.GT425_Dsc.toString();
    pcStr = pcStr + p_MsgGT425.GT425_Obt.toString();
    pcStr = pcStr + p_MsgGT425.GT425_Ovb.toString();
    pcStr = pcStr + p_MsgGT425.GT425_Uso.toString();
    pcStr = pcStr + p_MsgGT425.GT425_Agp.toString();
    pcStr = pcStr + p_MsgGT425.GT425_Tct.toString();
    pcStr = pcStr + p_MsgGT425.GT425_Cld.toString();
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
}