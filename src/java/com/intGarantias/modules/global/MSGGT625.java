// Source File Name:   MSGGT625.java

package com.intGarantias.modules.global;

import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.util.Formateo;

import java.util.*;
import java.lang.*;
import org.apache.turbine.util.Log;

public class MSGGT625
{
  public static int g_Max_GT625 = 18;
  //---------------------------------------------------------------------------------------
  public static class Bff_MsgGT625
  {
    public StringBuffer GtSgr_Seq = new StringBuffer(3 );       //9(03)        3 Bien
    public StringBuffer GtSgr_Itb = new StringBuffer(2 );       //X(02)        5 Tipo Bien
    public StringBuffer GtSgr_Dsb = new StringBuffer(30);       //X(30)       35 Desc. Bien
    public StringBuffer GtSgr_Frv = new StringBuffer(8 );       //9(08)       43 FValorizacion
    public StringBuffer GtSgr_Vcn = new StringBuffer(15);       //9(13)V9(2)  58 Valor Contable
    public StringBuffer GtSgr_Umt = new StringBuffer(1 );       //X(01)       59 UMonetaria
    public StringBuffer GtSgr_Vum = new StringBuffer(9 );       //9(07)V9(2)  68 Valor UMonetaria
    public StringBuffer GtSgr_Vsu = new StringBuffer(15);       //9(11)V9(4)  83 Valor Asegurable
    public StringBuffer GtSgr_Vsg = new StringBuffer(15);       //9(13)V9(2)  98 Valor Asegurable $
    public StringBuffer GtSgr_Isg = new StringBuffer(1 );       //X(01)       99 Idr. Seguro
    public StringBuffer GtSgr_Csg = new StringBuffer(3 );       //9(03)      102 Cod.Cia Seguros
    public StringBuffer GtSgr_Nsg = new StringBuffer(15);       //X(15)      117 Nom.Cia Seguros
    public StringBuffer GtSgr_Spz = new StringBuffer(10);       //X(10)      127 Id. Poliza
    public StringBuffer GtSgr_Sfv = new StringBuffer(8 );       //9(08)      135 FVencto Poliza
    public StringBuffer GtSgr_Sum = new StringBuffer(1 );       //X(01)      136 UMonetaria Seguro
    public StringBuffer GtSgr_Svl = new StringBuffer(15);       //9(11)V9(4) 151 Valor Poliza MO

    public String getSeq()      {return GtSgr_Seq.toString();}
    public String getItb()      {return GtSgr_Itb.toString();}
    public String getDsb()      {return GtSgr_Dsb.toString();}
    public String getFrv()      {return GtSgr_Frv.toString();}
    public String getVcn()      {return GtSgr_Vcn.toString();}
    public String getUmt()      {return GtSgr_Umt.toString();}
    public String getVum()      {return GtSgr_Vum.toString();}
    public String getVsu()      {return GtSgr_Vsu.toString();}
    public String getVsg()      {return GtSgr_Vsg.toString();}
    public String getIsg()      {return GtSgr_Isg.toString();}
    public String getCsg()      {return GtSgr_Csg.toString();}
    public String getNsg()      {return GtSgr_Nsg.toString();}
    public String getSpz()      {return GtSgr_Spz.toString();}
    public String getSfv()      {return GtSgr_Sfv.toString();}
    public String getSum()      {return GtSgr_Sum.toString();}
    public String getSvl()      {return GtSgr_Svl.toString();}

    public Formateo f = new Formateo();
    public String fgetFrv()     {return f.FormatDate(GtSgr_Frv.toString());}
    public String fgetVcn()     {return f.Formato(GtSgr_Vcn.toString(),"99.999.999.999",2,2,',');}
    public String fgetVum()     {return f.Formato(GtSgr_Vum.toString(),"99.999.999.999",2,2,',');}
    public String fgetVsu()     {return f.Formato(GtSgr_Vsu.toString(),"99.999.999.999",4,4,',');}
    public String fgetVsg()     {return f.Formato(GtSgr_Vsg.toString(),"99.999.999.999",2,2,',');}
    public String fgetSfv()     {return f.FormatDate(GtSgr_Sfv.toString());}
    public String fgetSvl()     {return f.Formato(GtSgr_Svl.toString(),"99.999.999.999",4,4,',');}
  }
  //---------------------------------------------------------------------------------------
  public static class Buf_MsgGT625
  {
    public StringBuffer GT625_Idr     = new StringBuffer(1  );
    public StringBuffer GT625_Img     = new StringBuffer(827);
    public StringBuffer GT625_Swt_Dcn = new StringBuffer(1  );
    public StringBuffer GT625_Swt_Gti = new StringBuffer(1  );
    public StringBuffer GT625_Swt_Cnr = new StringBuffer(1  );
    public StringBuffer GT625_Swt_Evt = new StringBuffer(1  );
    public StringBuffer GT625_Swt_Cli = new StringBuffer(1  );
    public StringBuffer GT625_Mxt     = new StringBuffer(2  );
    public Vector       GT625_Sgr     = new Vector();

    public String getIdr()           {return GT625_Idr.toString();}
    public String getImg()           {return GT625_Img.toString();}
    public String getSwt_Dcn()       {return GT625_Swt_Dcn.toString();}
    public String getSwt_Gti()       {return GT625_Swt_Gti.toString();}
    public String getSwt_Cnr()       {return GT625_Swt_Cnr.toString();}
    public String getSwt_Evt()       {return GT625_Swt_Evt.toString();}
    public String getSwt_Cli()       {return GT625_Swt_Cli.toString();}
    public String getMxt()           {return GT625_Mxt.toString();}
    public Vector getSgr()           {return GT625_Sgr;}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_MsgGT625 Inicia_MsgGT625()
  {
    Buf_MsgGT625 l_MsgGT625 = new Buf_MsgGT625();
    l_MsgGT625.GT625_Idr.replace    (0,l_MsgGT625.GT625_Idr.capacity(),     RUTGEN.Blancos(1  ));
    l_MsgGT625.GT625_Img.replace    (0,l_MsgGT625.GT625_Img.capacity(),     RUTGEN.Blancos(827));
    l_MsgGT625.GT625_Swt_Dcn.replace(0,l_MsgGT625.GT625_Swt_Dcn.capacity(), RUTGEN.Blancos(1  ));
    l_MsgGT625.GT625_Swt_Gti.replace(0,l_MsgGT625.GT625_Swt_Gti.capacity(), RUTGEN.Blancos(1  ));
    l_MsgGT625.GT625_Swt_Cnr.replace(0,l_MsgGT625.GT625_Swt_Cnr.capacity(), RUTGEN.Blancos(1  ));
    l_MsgGT625.GT625_Swt_Evt.replace(0,l_MsgGT625.GT625_Swt_Evt.capacity(), RUTGEN.Blancos(1  ));
    l_MsgGT625.GT625_Swt_Cli.replace(0,l_MsgGT625.GT625_Swt_Cli.capacity(), RUTGEN.Blancos(1  ));
    l_MsgGT625.GT625_Mxt.replace    (0,l_MsgGT625.GT625_Mxt.capacity(),     RUTGEN.Blancos(2  ));
    int p = 0;
    for (int i=0; i<g_Max_GT625; i++)
        {
          Bff_MsgGT625 Tap = new Bff_MsgGT625();
          Tap.GtSgr_Seq.replace(0, Tap.GtSgr_Seq.capacity(), RUTGEN.Blancos(3 ));
          Tap.GtSgr_Itb.replace(0, Tap.GtSgr_Itb.capacity(), RUTGEN.Blancos(2 ));
          Tap.GtSgr_Dsb.replace(0, Tap.GtSgr_Dsb.capacity(), RUTGEN.Blancos(30));
          Tap.GtSgr_Frv.replace(0, Tap.GtSgr_Frv.capacity(), RUTGEN.Blancos(8 ));
          Tap.GtSgr_Vcn.replace(0, Tap.GtSgr_Vcn.capacity(), RUTGEN.Blancos(15));
          Tap.GtSgr_Umt.replace(0, Tap.GtSgr_Umt.capacity(), RUTGEN.Blancos(1 ));
          Tap.GtSgr_Vum.replace(0, Tap.GtSgr_Vum.capacity(), RUTGEN.Blancos(9 ));
          Tap.GtSgr_Vsu.replace(0, Tap.GtSgr_Vsu.capacity(), RUTGEN.Blancos(15));
          Tap.GtSgr_Vsg.replace(0, Tap.GtSgr_Vsg.capacity(), RUTGEN.Blancos(15));
          Tap.GtSgr_Isg.replace(0, Tap.GtSgr_Isg.capacity(), RUTGEN.Blancos(1 ));
          Tap.GtSgr_Csg.replace(0, Tap.GtSgr_Csg.capacity(), RUTGEN.Blancos(3 ));
          Tap.GtSgr_Nsg.replace(0, Tap.GtSgr_Nsg.capacity(), RUTGEN.Blancos(15));
          Tap.GtSgr_Spz.replace(0, Tap.GtSgr_Spz.capacity(), RUTGEN.Blancos(10));
          Tap.GtSgr_Sfv.replace(0, Tap.GtSgr_Sfv.capacity(), RUTGEN.Blancos(8 ));
          Tap.GtSgr_Sum.replace(0, Tap.GtSgr_Sum.capacity(), RUTGEN.Blancos(1 ));
          Tap.GtSgr_Svl.replace(0, Tap.GtSgr_Svl.capacity(), RUTGEN.Blancos(15));
          l_MsgGT625.GT625_Sgr.add(Tap);
        }
    return l_MsgGT625;
  }
  //---------------------------------------------------------------------------------------
  static public Buf_MsgGT625 LSet_A_MsgGT625(String pcStr)
  {
    Buf_MsgGT625 l_MsgGT625 = new Buf_MsgGT625();
    MSGGT625 vMsgGT625 = new MSGGT625();
    Vector vDatos = vMsgGT625.LSet_A_vMsgGT625(pcStr);
    l_MsgGT625 = (MSGGT625.Buf_MsgGT625)vDatos.elementAt(0);
    return l_MsgGT625;
  }
  //---------------------------------------------------------------------------------------
  public Vector LSet_A_vMsgGT625(String pcStr)
  {
    Vector vec = new Vector();
    int p = 0;
    Buf_MsgGT625 l_MsgGT625 = new Buf_MsgGT625();
    l_MsgGT625.GT625_Idr.append     (pcStr.substring(p, p + l_MsgGT625.GT625_Idr.capacity()));     p = p + l_MsgGT625.GT625_Idr.capacity();
    l_MsgGT625.GT625_Img.append     (pcStr.substring(p, p + l_MsgGT625.GT625_Img.capacity()));     p = p + l_MsgGT625.GT625_Img.capacity();
    l_MsgGT625.GT625_Swt_Dcn.append (pcStr.substring(p, p + l_MsgGT625.GT625_Swt_Dcn.capacity())); p = p + l_MsgGT625.GT625_Swt_Dcn.capacity();
    l_MsgGT625.GT625_Swt_Gti.append (pcStr.substring(p, p + l_MsgGT625.GT625_Swt_Gti.capacity())); p = p + l_MsgGT625.GT625_Swt_Gti.capacity();
    l_MsgGT625.GT625_Swt_Cnr.append (pcStr.substring(p, p + l_MsgGT625.GT625_Swt_Cnr.capacity())); p = p + l_MsgGT625.GT625_Swt_Cnr.capacity();
    l_MsgGT625.GT625_Swt_Evt.append (pcStr.substring(p, p + l_MsgGT625.GT625_Swt_Evt.capacity())); p = p + l_MsgGT625.GT625_Swt_Evt.capacity();
    l_MsgGT625.GT625_Swt_Cli.append (pcStr.substring(p, p + l_MsgGT625.GT625_Swt_Cli.capacity())); p = p + l_MsgGT625.GT625_Swt_Cli.capacity();
    l_MsgGT625.GT625_Mxt.append     (pcStr.substring(p, p + l_MsgGT625.GT625_Mxt.capacity()));     p = p + l_MsgGT625.GT625_Mxt.capacity();
//    for (int i=0; i<Integer.parseInt(l_MsgGT625.GT625_Mxt.toString()); i++)
    for (int i=0; i<g_Max_GT625; i++)
        {
          Bff_MsgGT625 Tap = new Bff_MsgGT625();
          Tap.GtSgr_Seq.append(pcStr.substring(p, p + Tap.GtSgr_Seq.capacity())); p = p + Tap.GtSgr_Seq.capacity();
          Tap.GtSgr_Itb.append(pcStr.substring(p, p + Tap.GtSgr_Itb.capacity())); p = p + Tap.GtSgr_Itb.capacity();
          Tap.GtSgr_Dsb.append(pcStr.substring(p, p + Tap.GtSgr_Dsb.capacity())); p = p + Tap.GtSgr_Dsb.capacity();
          Tap.GtSgr_Frv.append(pcStr.substring(p, p + Tap.GtSgr_Frv.capacity())); p = p + Tap.GtSgr_Frv.capacity();
          Tap.GtSgr_Vcn.append(pcStr.substring(p, p + Tap.GtSgr_Vcn.capacity())); p = p + Tap.GtSgr_Vcn.capacity();
          Tap.GtSgr_Umt.append(pcStr.substring(p, p + Tap.GtSgr_Umt.capacity())); p = p + Tap.GtSgr_Umt.capacity();
          Tap.GtSgr_Vum.append(pcStr.substring(p, p + Tap.GtSgr_Vum.capacity())); p = p + Tap.GtSgr_Vum.capacity();
          Tap.GtSgr_Vsu.append(pcStr.substring(p, p + Tap.GtSgr_Vsu.capacity())); p = p + Tap.GtSgr_Vsu.capacity();
          Tap.GtSgr_Vsg.append(pcStr.substring(p, p + Tap.GtSgr_Vsg.capacity())); p = p + Tap.GtSgr_Vsg.capacity();
          Tap.GtSgr_Isg.append(pcStr.substring(p, p + Tap.GtSgr_Isg.capacity())); p = p + Tap.GtSgr_Isg.capacity();
          Tap.GtSgr_Csg.append(pcStr.substring(p, p + Tap.GtSgr_Csg.capacity())); p = p + Tap.GtSgr_Csg.capacity();
          Tap.GtSgr_Nsg.append(pcStr.substring(p, p + Tap.GtSgr_Nsg.capacity())); p = p + Tap.GtSgr_Nsg.capacity();
          Tap.GtSgr_Spz.append(pcStr.substring(p, p + Tap.GtSgr_Spz.capacity())); p = p + Tap.GtSgr_Spz.capacity();
          Tap.GtSgr_Sfv.append(pcStr.substring(p, p + Tap.GtSgr_Sfv.capacity())); p = p + Tap.GtSgr_Sfv.capacity();
          Tap.GtSgr_Sum.append(pcStr.substring(p, p + Tap.GtSgr_Sum.capacity())); p = p + Tap.GtSgr_Sum.capacity();
          Tap.GtSgr_Svl.append(pcStr.substring(p, p + Tap.GtSgr_Svl.capacity())); p = p + Tap.GtSgr_Svl.capacity();
          l_MsgGT625.GT625_Sgr.add(Tap);
        }
    vec.add (l_MsgGT625);
    return vec;
  }
  //---------------------------------------------------------------------------------------
  static public String LSet_De_MsgGT625(Buf_MsgGT625 p_MsgGT625)
  {
    String pcStr = "";
    pcStr = pcStr + p_MsgGT625.GT625_Idr.toString();
    pcStr = pcStr + p_MsgGT625.GT625_Img.toString();
    pcStr = pcStr + p_MsgGT625.GT625_Swt_Dcn.toString();
    pcStr = pcStr + p_MsgGT625.GT625_Swt_Gti.toString();
    pcStr = pcStr + p_MsgGT625.GT625_Swt_Cnr.toString();
    pcStr = pcStr + p_MsgGT625.GT625_Swt_Evt.toString();
    pcStr = pcStr + p_MsgGT625.GT625_Swt_Cli.toString();
    pcStr = pcStr + p_MsgGT625.GT625_Mxt.toString();
    for (int i=0; i<p_MsgGT625.GT625_Sgr.size(); i++)
        {
          pcStr = pcStr + ((Bff_MsgGT625)p_MsgGT625.GT625_Sgr.elementAt(i)).GtSgr_Seq.toString();
          pcStr = pcStr + ((Bff_MsgGT625)p_MsgGT625.GT625_Sgr.elementAt(i)).GtSgr_Itb.toString();
          pcStr = pcStr + ((Bff_MsgGT625)p_MsgGT625.GT625_Sgr.elementAt(i)).GtSgr_Dsb.toString();
          pcStr = pcStr + ((Bff_MsgGT625)p_MsgGT625.GT625_Sgr.elementAt(i)).GtSgr_Frv.toString();
          pcStr = pcStr + ((Bff_MsgGT625)p_MsgGT625.GT625_Sgr.elementAt(i)).GtSgr_Vcn.toString();
          pcStr = pcStr + ((Bff_MsgGT625)p_MsgGT625.GT625_Sgr.elementAt(i)).GtSgr_Umt.toString();
          pcStr = pcStr + ((Bff_MsgGT625)p_MsgGT625.GT625_Sgr.elementAt(i)).GtSgr_Vum.toString();
          pcStr = pcStr + ((Bff_MsgGT625)p_MsgGT625.GT625_Sgr.elementAt(i)).GtSgr_Vsu.toString();
          pcStr = pcStr + ((Bff_MsgGT625)p_MsgGT625.GT625_Sgr.elementAt(i)).GtSgr_Vsg.toString();
          pcStr = pcStr + ((Bff_MsgGT625)p_MsgGT625.GT625_Sgr.elementAt(i)).GtSgr_Isg.toString();
          pcStr = pcStr + ((Bff_MsgGT625)p_MsgGT625.GT625_Sgr.elementAt(i)).GtSgr_Csg.toString();
          pcStr = pcStr + ((Bff_MsgGT625)p_MsgGT625.GT625_Sgr.elementAt(i)).GtSgr_Nsg.toString();
          pcStr = pcStr + ((Bff_MsgGT625)p_MsgGT625.GT625_Sgr.elementAt(i)).GtSgr_Spz.toString();
          pcStr = pcStr + ((Bff_MsgGT625)p_MsgGT625.GT625_Sgr.elementAt(i)).GtSgr_Sfv.toString();
          pcStr = pcStr + ((Bff_MsgGT625)p_MsgGT625.GT625_Sgr.elementAt(i)).GtSgr_Sum.toString();
          pcStr = pcStr + ((Bff_MsgGT625)p_MsgGT625.GT625_Sgr.elementAt(i)).GtSgr_Svl.toString();
        }
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
}