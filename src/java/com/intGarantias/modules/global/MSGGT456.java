// Source File Name:   MSGGT456.java

package com.intGarantias.modules.global;

import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.util.Formateo;

import java.util.*;
import java.lang.*;
import org.apache.turbine.util.Log;

public class MSGGT456
{
  public static int g_Max_GT456 = 25;
  //---------------------------------------------------------------------------------------
  public static class Bff_MsgGT456
  {
    public StringBuffer GT456_Iln = new StringBuffer(7 );   //9(07)        7 Id. InfLegal
    public StringBuffer GT456_Ilf = new StringBuffer(8 );   //9(08)       15 Fecha InfLegal
    public StringBuffer GT456_Abg = new StringBuffer(4 );   //9(04)       19 Abogado
    public StringBuffer GT456_Efc = new StringBuffer(8 );   //9(08)       27 Fecha Contrato
    public StringBuffer GT456_Nnt = new StringBuffer(7 );   //X(07)       34 Notaria
    public StringBuffer GT456_Ent = new StringBuffer(25);   //X(25)       59 Notario
    public StringBuffer GT456_Pcm = new StringBuffer(11);   //9(11)       70 Canton
    public StringBuffer GT456_Nag = new StringBuffer(25);   //X(25)       95 Nombre Abogado
    public StringBuffer GT456_Ncm = new StringBuffer(20);   //X(20)      115 Nombre Canton
    public StringBuffer GT456_Npv = new StringBuffer(17);   //X(17)      132 Nombre Provincia
    public StringBuffer GT456_Dsl = new StringBuffer(9 );   //9(09)      141 @Observaciones

    public String getIln()         {return GT456_Iln.toString();}
    public String getIlf()         {return GT456_Ilf.toString();}
    public String getAbg()         {return GT456_Abg.toString();}
    public String getEfc()         {return GT456_Efc.toString();}
    public String getNnt()         {return GT456_Nnt.toString();}
    public String getEnt()         {return GT456_Ent.toString();}
    public String getPcm()         {return GT456_Pcm.toString();}
    public String getNag()         {return GT456_Nag.toString();}
    public String getNcm()         {return GT456_Ncm.toString();}
    public String getNpv()         {return GT456_Npv.toString();}
    public String getDsl()         {return GT456_Dsl.toString();}

    public Formateo f = new Formateo();
    public String fgetIlf()        {return f.FormatDate(GT456_Ilf.toString());}
    public String fgetEfc()        {return f.FormatDate(GT456_Efc.toString());}
  }
  //---------------------------------------------------------------------------------------
  public static class Buf_MsgGT456
  {
    public StringBuffer GT456_Idr = new StringBuffer(1);
    public StringBuffer GT456_Sis = new StringBuffer(3);
    public StringBuffer GT456_Ncn = new StringBuffer(7);
    public StringBuffer GT456_Nro = new StringBuffer(3);
    public Vector       GT456_Tap = new Vector();

    public String getIdr()         {return GT456_Idr.toString();}
    public String getSis()         {return GT456_Sis.toString();}
    public String getNcn()         {return GT456_Ncn.toString();}
    public String getNro()         {return GT456_Nro.toString();}
    public Vector getTab()         {return GT456_Tap;}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_MsgGT456 Inicia_MsgGT456()
  {
    Buf_MsgGT456 l_MsgGT456 = new Buf_MsgGT456();
    l_MsgGT456.GT456_Idr.replace(0,l_MsgGT456.GT456_Idr.capacity(), RUTGEN.Blancos(1));
    l_MsgGT456.GT456_Sis.replace(0,l_MsgGT456.GT456_Sis.capacity(), RUTGEN.Blancos(3));
    l_MsgGT456.GT456_Ncn.replace(0,l_MsgGT456.GT456_Ncn.capacity(), RUTGEN.Blancos(7));
    l_MsgGT456.GT456_Nro.replace(0,l_MsgGT456.GT456_Nro.capacity(), RUTGEN.Blancos(3));
    int p=0;
    for(int i=0; i<g_Max_GT456; i++)
    {
      Bff_MsgGT456 Tap = new Bff_MsgGT456();
      Tap.GT456_Iln.replace(0,Tap.GT456_Iln.capacity(), RUTGEN.Blancos(7 ));
      Tap.GT456_Ilf.replace(0,Tap.GT456_Ilf.capacity(), RUTGEN.Blancos(8 ));
      Tap.GT456_Abg.replace(0,Tap.GT456_Abg.capacity(), RUTGEN.Blancos(4 ));
      Tap.GT456_Efc.replace(0,Tap.GT456_Efc.capacity(), RUTGEN.Blancos(8 ));
      Tap.GT456_Nnt.replace(0,Tap.GT456_Nnt.capacity(), RUTGEN.Blancos(7 ));
      Tap.GT456_Ent.replace(0,Tap.GT456_Ent.capacity(), RUTGEN.Blancos(25));
      Tap.GT456_Pcm.replace(0,Tap.GT456_Pcm.capacity(), RUTGEN.Blancos(11));
      Tap.GT456_Nag.replace(0,Tap.GT456_Nag.capacity(), RUTGEN.Blancos(25));
      Tap.GT456_Ncm.replace(0,Tap.GT456_Ncm.capacity(), RUTGEN.Blancos(20));
      Tap.GT456_Npv.replace(0,Tap.GT456_Npv.capacity(), RUTGEN.Blancos(17));
      Tap.GT456_Dsl.replace(0,Tap.GT456_Dsl.capacity(), RUTGEN.Blancos(9 ));
      l_MsgGT456.GT456_Tap.add(Tap);
    }
    return l_MsgGT456;
  }
  //---------------------------------------------------------------------------------------
  static public Buf_MsgGT456 LSet_A_MsgGT456(String pcStr)
  {
    Buf_MsgGT456 l_MsgGT456 = new Buf_MsgGT456();
    MSGGT456 vMsgGT456 = new MSGGT456();
    Vector vDatos = vMsgGT456.LSet_A_vMsgGT456(pcStr);
    l_MsgGT456 = (MSGGT456.Buf_MsgGT456)vDatos.elementAt(0);
    return l_MsgGT456;
  }
  //---------------------------------------------------------------------------------------
  public Vector LSet_A_vMsgGT456(String pcStr)
  {
    Vector vec = new Vector();
    int p = 0;
    Buf_MsgGT456 l_MsgGT456 = new Buf_MsgGT456();
    l_MsgGT456.GT456_Idr.append(pcStr.substring(p, p + l_MsgGT456.GT456_Idr.capacity())); p = p + l_MsgGT456.GT456_Idr.capacity();
    l_MsgGT456.GT456_Sis.append(pcStr.substring(p, p + l_MsgGT456.GT456_Sis.capacity())); p = p + l_MsgGT456.GT456_Sis.capacity();
    l_MsgGT456.GT456_Ncn.append(pcStr.substring(p, p + l_MsgGT456.GT456_Ncn.capacity())); p = p + l_MsgGT456.GT456_Ncn.capacity();
    l_MsgGT456.GT456_Nro.append(pcStr.substring(p, p + l_MsgGT456.GT456_Nro.capacity())); p = p + l_MsgGT456.GT456_Nro.capacity();
    //for (int i=0; i<Integer.parseInt(l_MsgGT456.GT456_Nro.toString()); i++)
    for (int i=0; i<g_Max_GT456; i++)
        {
          Bff_MsgGT456 Tap = new Bff_MsgGT456();
          Tap.GT456_Iln.append(pcStr.substring(p, p + Tap.GT456_Iln.capacity())); p = p + Tap.GT456_Iln.capacity();
          Tap.GT456_Ilf.append(pcStr.substring(p, p + Tap.GT456_Ilf.capacity())); p = p + Tap.GT456_Ilf.capacity();
          Tap.GT456_Abg.append(pcStr.substring(p, p + Tap.GT456_Abg.capacity())); p = p + Tap.GT456_Abg.capacity();
          Tap.GT456_Efc.append(pcStr.substring(p, p + Tap.GT456_Efc.capacity())); p = p + Tap.GT456_Efc.capacity();
          Tap.GT456_Nnt.append(pcStr.substring(p, p + Tap.GT456_Nnt.capacity())); p = p + Tap.GT456_Nnt.capacity();
          Tap.GT456_Ent.append(pcStr.substring(p, p + Tap.GT456_Ent.capacity())); p = p + Tap.GT456_Ent.capacity();
          Tap.GT456_Pcm.append(pcStr.substring(p, p + Tap.GT456_Pcm.capacity())); p = p + Tap.GT456_Pcm.capacity();
          Tap.GT456_Nag.append(pcStr.substring(p, p + Tap.GT456_Nag.capacity())); p = p + Tap.GT456_Nag.capacity();
          Tap.GT456_Ncm.append(pcStr.substring(p, p + Tap.GT456_Ncm.capacity())); p = p + Tap.GT456_Ncm.capacity();
          Tap.GT456_Npv.append(pcStr.substring(p, p + Tap.GT456_Npv.capacity())); p = p + Tap.GT456_Npv.capacity();
          Tap.GT456_Dsl.append(pcStr.substring(p, p + Tap.GT456_Dsl.capacity())); p = p + Tap.GT456_Dsl.capacity();
          if (Tap.GT456_Iln.toString().trim().equals("")) { break; }
          l_MsgGT456.GT456_Tap.add(Tap);
        }
    vec.add (l_MsgGT456);
    return vec;
  }
  //---------------------------------------------------------------------------------------
  static public String LSet_De_MsgGT456(Buf_MsgGT456 p_MsgGT456)
  {
    String pcStr = "";
    pcStr = pcStr + p_MsgGT456.GT456_Idr.toString();
    pcStr = pcStr + p_MsgGT456.GT456_Sis.toString();
    pcStr = pcStr + p_MsgGT456.GT456_Ncn.toString();
    pcStr = pcStr + p_MsgGT456.GT456_Nro.toString();
    for(int i=0; i<p_MsgGT456.GT456_Tap.size(); i++)
    {
      pcStr = pcStr + ((Bff_MsgGT456)p_MsgGT456.GT456_Tap.elementAt(i)).GT456_Iln.toString();
      pcStr = pcStr + ((Bff_MsgGT456)p_MsgGT456.GT456_Tap.elementAt(i)).GT456_Ilf.toString();
      pcStr = pcStr + ((Bff_MsgGT456)p_MsgGT456.GT456_Tap.elementAt(i)).GT456_Abg.toString();
      pcStr = pcStr + ((Bff_MsgGT456)p_MsgGT456.GT456_Tap.elementAt(i)).GT456_Efc.toString();
      pcStr = pcStr + ((Bff_MsgGT456)p_MsgGT456.GT456_Tap.elementAt(i)).GT456_Nnt.toString();
      pcStr = pcStr + ((Bff_MsgGT456)p_MsgGT456.GT456_Tap.elementAt(i)).GT456_Ent.toString();
      pcStr = pcStr + ((Bff_MsgGT456)p_MsgGT456.GT456_Tap.elementAt(i)).GT456_Pcm.toString();
      pcStr = pcStr + ((Bff_MsgGT456)p_MsgGT456.GT456_Tap.elementAt(i)).GT456_Nag.toString();
      pcStr = pcStr + ((Bff_MsgGT456)p_MsgGT456.GT456_Tap.elementAt(i)).GT456_Ncm.toString();
      pcStr = pcStr + ((Bff_MsgGT456)p_MsgGT456.GT456_Tap.elementAt(i)).GT456_Npv.toString();
      pcStr = pcStr + ((Bff_MsgGT456)p_MsgGT456.GT456_Tap.elementAt(i)).GT456_Dsl.toString();
    }
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
}