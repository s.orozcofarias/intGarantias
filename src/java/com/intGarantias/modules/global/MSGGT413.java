// Source File Name:   MSGGT413.java

package com.intGarantias.modules.global;

import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.util.Formateo;

import java.util.*;
import java.lang.*;
import org.apache.turbine.util.Log;

public class MSGGT413
{
	public static class Grt_Fza_Fia
	{
            public StringBuffer GT413_Cli = new StringBuffer(10);  // X(10)       10 RUT Fiador   
            public StringBuffer GT413_Ncl = new StringBuffer(40);  // X(40)       50 Nombre Fiador
            // funciones que retornan los datos
            public String getCli(){return GT413_Cli.toString();}
            public String getNcl(){return GT413_Ncl.toString();}
            // funciones que retornan los datos,formateados
            public Formateo f = new Formateo();
            public String fgetCli413(){return f.fmtRut(GT413_Cli.toString());}

	}
	public static class MsgGT413
	{
            public StringBuffer GT413_Idr = new StringBuffer(1);
            public StringBuffer GT413_Gti = new StringBuffer(10);
            public StringBuffer GT413_Tfz = new StringBuffer(3);
            public Vector       GT413_Fia = new Vector();
            
            public String getIdr(){return GT413_Idr.toString();}
            public String getGti(){return GT413_Gti.toString();}
            public String getTfz(){return GT413_Tfz.toString();}
            public Vector getFia(){return GT413_Fia;}
            
	}
	public Vector LSet_A_GT413(String pcStr)
	{
	    	int p=0;
	    	Vector vec= new Vector();
	    	MsgGT413 p_GT413 = new MsgGT413();	    		    	
	    	p_GT413.GT413_Idr.append( pcStr.substring( p , p + p_GT413.GT413_Idr.capacity() ) );p = p + p_GT413.GT413_Idr.capacity();
	    	p_GT413.GT413_Gti.append( pcStr.substring( p , p + p_GT413.GT413_Gti.capacity() ) );p = p + p_GT413.GT413_Gti.capacity();
	    	p_GT413.GT413_Tfz.append( pcStr.substring( p , p + p_GT413.GT413_Tfz.capacity() ) );p = p + p_GT413.GT413_Tfz.capacity();
	    	for(int i=0;i< Integer.parseInt(p_GT413.GT413_Tfz.toString());i++)
	    	{
	    		Grt_Fza_Fia GT413_Fia = new Grt_Fza_Fia();
	    		
	        	GT413_Fia.GT413_Cli.append( pcStr.substring( p , p + GT413_Fia.GT413_Cli.capacity()) ) ; p = p + GT413_Fia.GT413_Cli.capacity();
	        	GT413_Fia.GT413_Ncl.append( pcStr.substring( p , p + GT413_Fia.GT413_Ncl.capacity()) ) ; p = p + GT413_Fia.GT413_Ncl.capacity();
	        	
	        	p_GT413.GT413_Fia.add(GT413_Fia);
	    	}
	    	vec.add ( p_GT413 );	    	
	    	return vec;
	}
}