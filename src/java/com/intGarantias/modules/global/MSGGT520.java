// Source File Name:   MSGGT520.java

package com.intGarantias.modules.global;

import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.util.Formateo;

import java.util.*;
import java.lang.*;
import org.apache.turbine.util.Log;

public class MSGGT520
{
  public static int g_Max_GT520 = 28;
  //---------------------------------------------------------------------------------------
  public static class Bff_MsgGT520
  {
    public StringBuffer GT520_Sis = new StringBuffer(3 );   //X(03)        3 Sistema         
    public StringBuffer GT520_Ncn = new StringBuffer(7 );   //9(07)       10 Nro Operacion   
    public StringBuffer GT520_Seq = new StringBuffer(3 );   //9(03)       13 Secuencia Bien  
    public StringBuffer GT520_Drb = new StringBuffer(70);   //X(70)       83 Direccion Bien  
    public StringBuffer GT520_Nts = new StringBuffer(20);   //X(20)      103 Nombre Tasador  
    public StringBuffer GT520_Fts = new StringBuffer(8 );   //9(08)      111 Fecha Tasacion  
    public StringBuffer GT520_Vts = new StringBuffer(15);   //9(13)V9(2) 126 Valor Tasacion  

    public String getSis()       {return GT520_Sis.toString();}
    public String getNcn()       {return GT520_Ncn.toString();}
    public String getSeq()       {return GT520_Seq.toString();}
    public String getDrb()       {return GT520_Drb.toString();}
    public String getNts()       {return GT520_Nts.toString();}
    public String getFts()       {return GT520_Fts.toString();}
    public String getVts()       {return GT520_Vts.toString();}

    public Formateo f = new Formateo();
    public String fgetFts()      {return f.FormatDate(GT520_Fts.toString());}
    public String fgetVts()      {return f.Formato(GT520_Vts.toString(),"99.999.999.999",2,2,',');}
  }
  //---------------------------------------------------------------------------------------
  public static class Buf_MsgGT520
  {
    public StringBuffer GT520_Idr = new StringBuffer(1 );   //X(01)        1 Idr.Host         
    public StringBuffer GT520_Itg = new StringBuffer(10);   //X(10)       11 Id Tipo Bien     
    public StringBuffer GT520_Cmn = new StringBuffer(11);   //9(11)       22 Comuna           
    public StringBuffer GT520_Nro = new StringBuffer(3 );   //9(03)       25 Numero Elementos 
    public Vector       GT520_Tab = new Vector();

    public String getIdr()         {return GT520_Idr.toString();}
    public String getItg()         {return GT520_Itg.toString();}
    public String getCmn()         {return GT520_Cmn.toString();}
    public String getNro()         {return GT520_Nro.toString();}
    public Vector getTab()         {return GT520_Tab;}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_MsgGT520 Inicia_MsgGT520()
  {
    Buf_MsgGT520 l_MsgGT520 = new Buf_MsgGT520();
    l_MsgGT520.GT520_Idr.replace(0, l_MsgGT520.GT520_Idr.capacity(), RUTGEN.Blancos(1 ));
    l_MsgGT520.GT520_Itg.replace(0, l_MsgGT520.GT520_Itg.capacity(), RUTGEN.Blancos(10));
    l_MsgGT520.GT520_Cmn.replace(0, l_MsgGT520.GT520_Cmn.capacity(), RUTGEN.Blancos(11));
    l_MsgGT520.GT520_Nro.replace(0, l_MsgGT520.GT520_Nro.capacity(), RUTGEN.Blancos(3 ));
    int p = 0;
    for (int i=0; i<g_Max_GT520; i++)
        {
          Bff_MsgGT520 Tap = new Bff_MsgGT520();
          Tap.GT520_Sis.replace(0, Tap.GT520_Sis.capacity(), RUTGEN.Blancos(3 ));
          Tap.GT520_Ncn.replace(0, Tap.GT520_Ncn.capacity(), RUTGEN.Blancos(7 ));
          Tap.GT520_Seq.replace(0, Tap.GT520_Seq.capacity(), RUTGEN.Blancos(3 ));
          Tap.GT520_Drb.replace(0, Tap.GT520_Drb.capacity(), RUTGEN.Blancos(70));
          Tap.GT520_Nts.replace(0, Tap.GT520_Nts.capacity(), RUTGEN.Blancos(20));
          Tap.GT520_Fts.replace(0, Tap.GT520_Fts.capacity(), RUTGEN.Blancos(8 ));
          Tap.GT520_Vts.replace(0, Tap.GT520_Vts.capacity(), RUTGEN.Blancos(15));
          l_MsgGT520.GT520_Tab.add(Tap);        
        }
    return l_MsgGT520;
  }
  //---------------------------------------------------------------------------------------
  static public Buf_MsgGT520 LSet_A_MsgGT520(String pcStr)
  {
    Buf_MsgGT520 l_MsgGT520 = new Buf_MsgGT520();
    MSGGT520 vMsgGT520 = new MSGGT520();
    Vector vDatos = vMsgGT520.LSet_A_vMsgGT520(pcStr);
    l_MsgGT520 = (MSGGT520.Buf_MsgGT520)vDatos.elementAt(0);
    return l_MsgGT520;
  }
  //---------------------------------------------------------------------------------------
  public Vector LSet_A_vMsgGT520(String pcStr)
  {
    Vector vec = new Vector();
    int p = 0;
    Buf_MsgGT520 l_MsgGT520 = new Buf_MsgGT520();                            
    l_MsgGT520.GT520_Idr.append(pcStr.substring(p, p + l_MsgGT520.GT520_Idr.capacity())); p = p + l_MsgGT520.GT520_Idr.capacity();
    l_MsgGT520.GT520_Itg.append(pcStr.substring(p, p + l_MsgGT520.GT520_Itg.capacity())); p = p + l_MsgGT520.GT520_Itg.capacity();
    l_MsgGT520.GT520_Cmn.append(pcStr.substring(p, p + l_MsgGT520.GT520_Cmn.capacity())); p = p + l_MsgGT520.GT520_Cmn.capacity();
    l_MsgGT520.GT520_Nro.append(pcStr.substring(p, p + l_MsgGT520.GT520_Nro.capacity())); p = p + l_MsgGT520.GT520_Nro.capacity();
    for (int i=0; i<Integer.parseInt(l_MsgGT520.GT520_Nro.toString()); i++)
        {
          Bff_MsgGT520 Tap = new Bff_MsgGT520();
          Tap.GT520_Sis.append(pcStr.substring(p, p + Tap.GT520_Sis.capacity())); p = p + Tap.GT520_Sis.capacity();
          Tap.GT520_Ncn.append(pcStr.substring(p, p + Tap.GT520_Ncn.capacity())); p = p + Tap.GT520_Ncn.capacity();
          Tap.GT520_Seq.append(pcStr.substring(p, p + Tap.GT520_Seq.capacity())); p = p + Tap.GT520_Seq.capacity();
          Tap.GT520_Drb.append(pcStr.substring(p, p + Tap.GT520_Drb.capacity())); p = p + Tap.GT520_Drb.capacity();
          Tap.GT520_Nts.append(pcStr.substring(p, p + Tap.GT520_Nts.capacity())); p = p + Tap.GT520_Nts.capacity();
          Tap.GT520_Fts.append(pcStr.substring(p, p + Tap.GT520_Fts.capacity())); p = p + Tap.GT520_Fts.capacity();
          Tap.GT520_Vts.append(pcStr.substring(p, p + Tap.GT520_Vts.capacity())); p = p + Tap.GT520_Vts.capacity();
          l_MsgGT520.GT520_Tab.add(Tap);
        }
    vec.add (l_MsgGT520);
    return vec;
  }
  //---------------------------------------------------------------------------------------
  static public String LSet_De_MsgGT520(Buf_MsgGT520 p_MsgGT520)
  {
    String pcStr = "";
    pcStr = pcStr + p_MsgGT520.GT520_Idr.toString();
    pcStr = pcStr + p_MsgGT520.GT520_Itg.toString();
    pcStr = pcStr + p_MsgGT520.GT520_Cmn.toString();
    pcStr = pcStr + p_MsgGT520.GT520_Nro.toString();
    for (int i=0; i<p_MsgGT520.GT520_Tab.size(); i++)
        {
          pcStr = pcStr + ((Bff_MsgGT520)p_MsgGT520.GT520_Tab.elementAt(i)).GT520_Sis.toString();
          pcStr = pcStr + ((Bff_MsgGT520)p_MsgGT520.GT520_Tab.elementAt(i)).GT520_Ncn.toString();
          pcStr = pcStr + ((Bff_MsgGT520)p_MsgGT520.GT520_Tab.elementAt(i)).GT520_Seq.toString();
          pcStr = pcStr + ((Bff_MsgGT520)p_MsgGT520.GT520_Tab.elementAt(i)).GT520_Drb.toString();
          pcStr = pcStr + ((Bff_MsgGT520)p_MsgGT520.GT520_Tab.elementAt(i)).GT520_Nts.toString();
          pcStr = pcStr + ((Bff_MsgGT520)p_MsgGT520.GT520_Tab.elementAt(i)).GT520_Fts.toString();
          pcStr = pcStr + ((Bff_MsgGT520)p_MsgGT520.GT520_Tab.elementAt(i)).GT520_Vts.toString();
        }
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
}