// Source File Name:   MSGGT619.java

package com.intGarantias.modules.global;

import com.FHTServlet.modules.global.BF_LOB;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.util.Formateo;

import java.util.*;
import java.lang.*;
import org.apache.turbine.util.Log;

public class MSGGT619
{
  //===============================================================================================================================
  public static int g_Max_GT619 = 100;
  //-------------------------------------------------------------------------------------------
  public static class Bff_MsgGT619
  {
    public StringBuffer GtMmc_Iln = new StringBuffer(7 );       //9(07)        7 Numero Informe
    public StringBuffer GtMmc_Ilf = new StringBuffer(8 );       //9(08)       15 Fecha Informe
    public StringBuffer GtMmc_Dsl = new StringBuffer(9 );       //9(09)       24 @Descripcion Informe

    public String getIln()       {return GtMmc_Iln.toString();}
    public String getIlf()       {return GtMmc_Ilf.toString();}
    public String getDsl()       {return GtMmc_Dsl.toString();}

    public int    igetDsl()      {if (getDsl().trim().equals("")) return 0; else return Integer.parseInt(getDsl());}

    public Formateo f = new Formateo();
    public String fgetIlf()                   {return f.FormatDate(getIlf());}
    public String fgetDsl() throws Exception  {BF_LOB lob = new BF_LOB(); if (igetDsl()==0) return ""; else return lob.leeCLOB(getDsl().trim());}
  //public BF_LOB lob = new BF_LOB();
  //public String fgetDsl() throws Exception  {if (igetDsl()==0) return ""; else return lob.leeCLOB(getDsl().trim());}
  }
  //===============================================================================================================================
  public static class Buf_MsgGT619
  {
    public StringBuffer GT619_Idr     = new StringBuffer(1  );      //X(001)      1 Idr Host
    public StringBuffer GT619_Img     = new StringBuffer(827);      //X(827)    828 Img Base
    public StringBuffer GT619_Swt_Dcn = new StringBuffer(1  );      //9(001)    829 Swt Dcn
    public StringBuffer GT619_Swt_Gti = new StringBuffer(1  );      //9(001)    830 Swt Gti
    public StringBuffer GT619_Swt_Evt = new StringBuffer(1  );      //9(001)    831 Swt Evt
    public StringBuffer GT619_Swt_Cli = new StringBuffer(1  );      //9(001)    832 Swt Cli
    public StringBuffer GT619_Mxt     = new StringBuffer(2  );      //9(002)    834 Cantidad MMC
    public Vector       GT619_Mmc     = new Vector();               //X(2400)  3234 Tabla MMC

    public String getIdr()           {return GT619_Idr.toString();}
    public String getImg()           {return GT619_Img.toString();}
    public String getSwt_Dcn()       {return GT619_Swt_Dcn.toString();}
    public String getSwt_Gti()       {return GT619_Swt_Gti.toString();}
    public String getSwt_Evt()       {return GT619_Swt_Evt.toString();}
    public String getSwt_Cli()       {return GT619_Swt_Cli.toString();}
    public String getMxt()           {return GT619_Mxt.toString();}
    public Vector getMmc()           {return GT619_Mmc;}

    public int    igetMxt()          {if (getMxt().trim().equals("")) return 0; else return Integer.parseInt(getMxt());}
  }
  //-------------------------------------------------------------------------------------------
  public static Buf_MsgGT619 Inicia_MsgGT619()
  {
    Buf_MsgGT619 l_MsgGT619 = new Buf_MsgGT619();
    l_MsgGT619.GT619_Idr.replace    (0,l_MsgGT619.GT619_Idr.capacity(),     RUTGEN.Blancos(1  ));
    l_MsgGT619.GT619_Img.replace    (0,l_MsgGT619.GT619_Img.capacity(),     RUTGEN.Blancos(827));
    l_MsgGT619.GT619_Swt_Dcn.replace(0,l_MsgGT619.GT619_Swt_Dcn.capacity(), RUTGEN.Blancos(1  ));
    l_MsgGT619.GT619_Swt_Gti.replace(0,l_MsgGT619.GT619_Swt_Gti.capacity(), RUTGEN.Blancos(1  ));
    l_MsgGT619.GT619_Swt_Evt.replace(0,l_MsgGT619.GT619_Swt_Evt.capacity(), RUTGEN.Blancos(1  ));
    l_MsgGT619.GT619_Swt_Cli.replace(0,l_MsgGT619.GT619_Swt_Cli.capacity(), RUTGEN.Blancos(1  ));
    l_MsgGT619.GT619_Mxt.replace    (0,l_MsgGT619.GT619_Mxt.capacity(),     RUTGEN.Blancos(2  ));
    l_MsgGT619.GT619_Mmc.clear();
    return l_MsgGT619;
  }
  //===============================================================================================================================
  static public Buf_MsgGT619 LSet_A_MsgGT619(String pcStr)
  {
    int p = 0;
    Buf_MsgGT619 l_MsgGT619 = new Buf_MsgGT619();                            
    l_MsgGT619.GT619_Idr.append     (pcStr.substring(p, p + l_MsgGT619.GT619_Idr.capacity()));     p = p + l_MsgGT619.GT619_Idr.capacity();
    l_MsgGT619.GT619_Img.append     (pcStr.substring(p, p + l_MsgGT619.GT619_Img.capacity()));     p = p + l_MsgGT619.GT619_Img.capacity();
    l_MsgGT619.GT619_Swt_Dcn.append (pcStr.substring(p, p + l_MsgGT619.GT619_Swt_Dcn.capacity())); p = p + l_MsgGT619.GT619_Swt_Dcn.capacity();
    l_MsgGT619.GT619_Swt_Gti.append (pcStr.substring(p, p + l_MsgGT619.GT619_Swt_Gti.capacity())); p = p + l_MsgGT619.GT619_Swt_Gti.capacity();
    l_MsgGT619.GT619_Swt_Evt.append (pcStr.substring(p, p + l_MsgGT619.GT619_Swt_Evt.capacity())); p = p + l_MsgGT619.GT619_Swt_Evt.capacity();
    l_MsgGT619.GT619_Swt_Cli.append (pcStr.substring(p, p + l_MsgGT619.GT619_Swt_Cli.capacity())); p = p + l_MsgGT619.GT619_Swt_Cli.capacity();
    l_MsgGT619.GT619_Mxt.append     (pcStr.substring(p, p + l_MsgGT619.GT619_Mxt.capacity()));     p = p + l_MsgGT619.GT619_Mxt.capacity();
    for (int i=0; i<l_MsgGT619.igetMxt(); i++)
        {
          Bff_MsgGT619 Tap = new Bff_MsgGT619();
          Tap.GtMmc_Iln.append(pcStr.substring(p, p + Tap.GtMmc_Iln.capacity())); p = p + Tap.GtMmc_Iln.capacity();
          Tap.GtMmc_Ilf.append(pcStr.substring(p, p + Tap.GtMmc_Ilf.capacity())); p = p + Tap.GtMmc_Ilf.capacity();
          Tap.GtMmc_Dsl.append(pcStr.substring(p, p + Tap.GtMmc_Dsl.capacity())); p = p + Tap.GtMmc_Dsl.capacity();
          l_MsgGT619.GT619_Mmc.add(Tap);
        }
    return l_MsgGT619;
  }
  //-------------------------------------------------------------------------------------------
  static public String LSet_De_MsgGT619(Buf_MsgGT619 p_MsgGT619)
  {
    String pcStr = "";
    pcStr = pcStr + p_MsgGT619.GT619_Idr.toString();
    pcStr = pcStr + p_MsgGT619.GT619_Img.toString();
    pcStr = pcStr + p_MsgGT619.GT619_Swt_Dcn.toString();
    pcStr = pcStr + p_MsgGT619.GT619_Swt_Gti.toString();
    pcStr = pcStr + p_MsgGT619.GT619_Swt_Evt.toString();
    pcStr = pcStr + p_MsgGT619.GT619_Swt_Cli.toString();
    pcStr = pcStr + p_MsgGT619.GT619_Mxt.toString();
    for (int i=0; i<p_MsgGT619.GT619_Mmc.size(); i++)
        {
          pcStr = pcStr + ((Bff_MsgGT619)p_MsgGT619.GT619_Mmc.elementAt(i)).GtMmc_Iln.toString();
          pcStr = pcStr + ((Bff_MsgGT619)p_MsgGT619.GT619_Mmc.elementAt(i)).GtMmc_Ilf.toString();
          pcStr = pcStr + ((Bff_MsgGT619)p_MsgGT619.GT619_Mmc.elementAt(i)).GtMmc_Dsl.toString();
        }
    return pcStr;
  }
  //===============================================================================================================================
}