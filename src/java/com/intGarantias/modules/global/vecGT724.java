// Source File Name:   vecGT724.java

package com.intGarantias.modules.global;

import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.util.Formateo;

import java.util.*;
import java.lang.*;
import org.apache.turbine.util.Log;

public class vecGT724
{
  //---------------------------------------------------------------------------------------
  public static class Bff_Hpd
  {
    public StringBuffer Hpd_Hpd = new StringBuffer(827);  //X(827)         Img_GtHpd

    public String getHpd()       {return Hpd_Hpd.toString();}
  }
  //---------------------------------------------------------------------------------------
  public static class Bff_Hyp
  {
    public StringBuffer Hyp_Idx = new StringBuffer(3 );   //9(03)          Indice Activo
    public StringBuffer Hyp_Hyp = new StringBuffer(827);  //X(827)         Img_GtHyp
    public Vector       Hyp_Hpd = new Vector();

    public String getHyp()       {return Hyp_Hyp.toString();}
    public Vector getHpd()       {return Hyp_Hpd;}
  }
  //---------------------------------------------------------------------------------------
  public static class Buf_vecGT724
  {
    public StringBuffer GT724_Idx = new StringBuffer(3 );   //9(03)        Indice Activo
    public StringBuffer GT724_Ape = new StringBuffer(827);  //X(827)       Img_GtApe
    public StringBuffer GT724_Tsd = new StringBuffer(827);  //X(827)       Img_GtTsd
    public Vector       GT724_Hyp = new Vector();

    public String getIdx()         {return GT724_Idx.toString();}
    public String getApe()         {return GT724_Ape.toString();}
    public String getTsd()         {return GT724_Tsd.toString();}
    public Vector getHyp()         {return GT724_Hyp;}
  }
  //---------------------------------------------------------------------------------------
}