// Source File Name:   MSGGT411.java

package com.intGarantias.modules.global;

import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.util.Formateo;

import java.util.*;
import java.lang.*;
import org.apache.turbine.util.Log;

public class MSGGT411
{
  public static int g_Max_GT411 = 25;
  //---------------------------------------------------------------------------------------
  public static class Grt_Gbs_Det
  {
    public StringBuffer Gbs_Itb = new StringBuffer(2  ); // X(02)         2 Id Tipo Bien
    public StringBuffer Gbs_Itg = new StringBuffer(5  ); // X(05)         7 Id Tipo Gtia
    public StringBuffer Gbs_Csv = new StringBuffer(3  ); // X(03)        10 Conservador
    public StringBuffer Gbs_Lcl = new StringBuffer(15 ); // X(15)        25 Localidad
    public StringBuffer Gbs_Rpo = new StringBuffer(4  ); // 9(04)        29 Repertorio
    public StringBuffer Gbs_Foj = new StringBuffer(7  ); // 9(07)        36 Fojas
    public StringBuffer Gbs_Nin = new StringBuffer(7  ); // 9(07)        43 Numero
    public StringBuffer Gbs_Dsb = new StringBuffer(100); // X(100)      143 Descripcion Bien
    public StringBuffer Gbs_Vtt = new StringBuffer(15 ); // 9(11)V9(4)  158 VTasacion <TRJ>
    public StringBuffer Gbs_Vts = new StringBuffer(15 ); // 9(13)V9(2)  173 VTasacion <MND>
    public StringBuffer Gbs_Frv = new StringBuffer(8  ); // 9(08)       181 FRevalorizacion
    public StringBuffer Gbs_Vur = new StringBuffer(9  ); // 9(07)V9(2)  190 Valor UReajustable
    public StringBuffer Gbs_Dpc = new StringBuffer(10 ); // X(10)       200 PContable
    public StringBuffer Gbs_Pct = new StringBuffer(5  ); // 9(03)V9(2)  205 %Castigo SBIF
    public StringBuffer Gbs_Vcn = new StringBuffer(15 ); // 9(13)V9(2)  220 VContable <MND>
    public StringBuffer Gbs_Vlq = new StringBuffer(15 ); // 9(13)V9(2)  235 VLiquidacion <MND>
    public StringBuffer Gbs_Vtb = new StringBuffer(15 ); // 9(13)V9(2)  250 VTasacion Banco <MND>
    public StringBuffer Gbs_Fvt = new StringBuffer(8  ); // 9(08)       258 FVcto Tasacion
    public StringBuffer Gbs_Obs = new StringBuffer(9  ); // 9(09)       267 @Observaciones
    public StringBuffer Gbs_Vsg = new StringBuffer(15 ); // 9(11)V9(4)  282 VAsegurable <MND>
    public StringBuffer Gbs_Csg = new StringBuffer(3  ); // 9(03)       285 Aseguradora
    public StringBuffer Gbs_Spz = new StringBuffer(10 ); // X(10)       295 Poliza
    public StringBuffer Gbs_Sfv = new StringBuffer(8  ); // 9(08)       303 FVcto Seguro
    public StringBuffer Gbs_Sum = new StringBuffer(1  ); // X(01)       304 UMon Seguro
    public StringBuffer Gbs_Svl = new StringBuffer(15 ); // 9(11)V9(4)  319 VSeguro <UF>
    public StringBuffer Gah_Drb = new StringBuffer(107); // X(107)      426 Direccion Bien
    public StringBuffer Gah_Cmn = new StringBuffer(11 ); // 9(11)       437 CodComuna
    public StringBuffer Gah_Coo = new StringBuffer(10 ); // X(10)       447 Coordenadas
    public StringBuffer Gti_Trj = new StringBuffer(3  ); // X(03)       450 TReajustabilidad
    public StringBuffer Gti_Mnd = new StringBuffer(3  ); // 9(03)       453 Codigo Moneda
    public StringBuffer Gti_Dcm = new StringBuffer(1  ); // 9(01)       454 Decimales

    public String getItb()       {return Gbs_Itb.toString();}
    public String getItg()       {return Gbs_Itg.toString();}
    public String getCsv()       {return Gbs_Csv.toString();}
    public String getLcl()       {return Gbs_Lcl.toString();}
    public String getRpo()       {return Gbs_Rpo.toString();}
    public String getFoj()       {return Gbs_Foj.toString();}
    public String getNin()       {return Gbs_Nin.toString();}
    public String getDsb()       {return Gbs_Dsb.toString();}
    public String getVtt()       {return Gbs_Vtt.toString();}
    public String getVts()       {return Gbs_Vts.toString();}
    public String getFrv()       {return Gbs_Frv.toString();}
    public String getVur()       {return Gbs_Vur.toString();}
    public String getDpc()       {return Gbs_Dpc.toString();}
    public String getPct()       {return Gbs_Pct.toString();}
    public String getVcn()       {return Gbs_Vcn.toString();}
    public String getVlq()       {return Gbs_Vlq.toString();}
    public String getVtb()       {return Gbs_Vtb.toString();}
    public String getFvt()       {return Gbs_Fvt.toString();}
    public String getObs()       {return Gbs_Obs.toString();}
    public String getVsg()       {return Gbs_Vsg.toString();}
    public String getCsg()       {return Gbs_Csg.toString();}
    public String getSpz()       {return Gbs_Spz.toString();}
    public String getSfv()       {return Gbs_Sfv.toString();}
    public String getSum()       {return Gbs_Sum.toString();}
    public String getSvl()       {return Gbs_Svl.toString();}
    public String getDrb()       {return Gah_Drb.toString();}
    public String getCmn()       {return Gah_Cmn.toString();}
    public String getCoo()       {return Gah_Coo.toString();}
    public String getTrj()       {return Gti_Trj.toString();}
    public String getMnd()       {return Gti_Mnd.toString();}
    public String getDcm()       {return Gti_Dcm.toString();}

    public Formateo f = new Formateo();
    public String fgetVtt()      {return f.Formato(Gbs_Vtt.toString(),"99.999.999.999",4,Integer.parseInt(getDcm()),',');}
    public String fgetVts()      {return f.Formato(Gbs_Vts.toString(),"9.999.999.999.999",2,0,',');}
    public String fgetRpo()      {return f.Formato(Gbs_Rpo.toString(),"",0,20,',');}
    public String fgetFoj()      {return f.Formato(Gbs_Foj.toString(),"",0,20,',');}
    public String fgetNin()      {return f.Formato(Gbs_Nin.toString(),"",0,20,',');}
    public String fgetFrv()      {return f.FormatDate(Gbs_Frv.toString());}
    public String fgetVur()      {return f.Formato(Gbs_Vur.toString(),"9.999.999.999.999",2,2,',');}
    public String fgetPct()      {return f.Formato(Gbs_Pct.toString(),"999",2,2,',');}
    public String fgetVcn()      {return f.Formato(Gbs_Vcn.toString(),"9.999.999.999.999",2,0,',');}
    public String fgetVlq()      {return f.Formato(Gbs_Vlq.toString(),"9.999.999.999.999",2,0,',');}
    public String fgetVtb()      {return f.Formato(Gbs_Vtb.toString(),"9.999.999.999.999",2,0,',');}
    public String fgetFvt()      {return f.FormatDate(Gbs_Fvt.toString());}
    public String fgetVsg()      {return f.Formato(Gbs_Vsg.toString(),"999.999.999.999",4,4,',');}
    public String fgetSfv()      {return f.FormatDate(Gbs_Sfv.toString());}
    public String fgetSvl()      {return f.Formato(Gbs_Svl.toString(),"99.999.999.999",4,4,',');}
    public String fgetTrj()      {return getTrj().trim();}
  }
  //---------------------------------------------------------------------------------------
  public static class Bfv_Ghd
  {
    public Vector       Ghd_Rgs = new Vector();

    public Vector getRgs()       {return Ghd_Rgs;}
  }
  //---------------------------------------------------------------------------------------
  public static class Grt_Ghd_Det
  {
    public StringBuffer Ghd_Sqd = new StringBuffer(3) ; // 9(03)         3 Sequencia
    public StringBuffer Ghd_Ith = new StringBuffer(2) ; // X(02)         5 Sequencia
    public StringBuffer Ghd_Dsh = new StringBuffer(30); // X(30)        35 Sequencia
    public StringBuffer Ghd_Udd = new StringBuffer(9) ; // 9(07)V9(2)   44.
    public StringBuffer Ghd_Umd = new StringBuffer(2) ; // X(02)        46.
    public StringBuffer Ghd_Umt = new StringBuffer(1) ; // X(01)        47.
    public StringBuffer Ghd_Pum = new StringBuffer(15); // 9(11)V9(4)   62.
    public StringBuffer Ghd_Vtu = new StringBuffer(15); // 9(11)V9(4)   77.
    public StringBuffer Ghd_Vum = new StringBuffer(9) ; // 9(07)V9(2)   86.
    public StringBuffer Ghd_Vtt = new StringBuffer(15); // 9(11)V9(4)  101.
    public StringBuffer Ghd_Vts = new StringBuffer(15); // 9(13)V9(2)  116.

    public String getSqd()       {return Ghd_Sqd.toString();}
    public String getIth()       {return Ghd_Ith.toString();}
    public String getDsh()       {return Ghd_Dsh.toString();}
    public String getUdd()       {return Ghd_Udd.toString();}
    public String getUmd()       {return Ghd_Umd.toString();}
    public String getUmt()       {return Ghd_Umt.toString();}
    public String getPum()       {return Ghd_Pum.toString();}
    public String getVtu()       {return Ghd_Vtu.toString();}
    public String getVum()       {return Ghd_Vum.toString();}
    public String getVtt()       {return Ghd_Vtt.toString();}
    public String getVts()       {return Ghd_Vts.toString();}

    public Formateo f = new Formateo();
    public String fgetVtt(String Dcm){return f.Formato(Ghd_Vtt.toString(),"99.999.999.999",4,Integer.parseInt(Dcm),',');}
    public String fgetVts()      {return f.Formato(Ghd_Vts.toString(),"9.999.999.999.999",2,0,',');}
    public String fgetUdd()      {return f.Formato(Ghd_Udd.toString(),"9.999.999",2,2,',');}
    public String fgetPum()      {return f.Formato(Ghd_Pum.toString(),"99.999.999.999",4,4,',');}
    public String fgetVtu()      {return f.Formato(Ghd_Vtu.toString(),"99.999.999.999",4,4,',');}
    public String fgetVum()      {return f.Formato(Ghd_Vum.toString(),"9.999.999",2,2,',');}
    public String fgetVtt2()     {return f.Formato(Ghd_Vtt.toString(),"99.999.999.999",4,0,',');}
  }
  //---------------------------------------------------------------------------------------
  public static class Buf_MsgGT411
  {
    public StringBuffer GT411_Idr = new StringBuffer(01);  //X(01)         1 Idr Host
    public StringBuffer GT411_Gti = new StringBuffer(10);  //X(10)        11 Id. Garantia
    public StringBuffer GT411_Seq = new StringBuffer(03);  //9(03)        14 Total Indices
    public Grt_Gbs_Det  GT411_Gbs = new Grt_Gbs_Det();     //X(424)      438 Tabla Detalle
    public StringBuffer GT411_Nro = new StringBuffer(02);  //9(02)       440 Total Indices
    public Vector       GT411_Ghd = new Vector();          //X(3132)    3572 Avales Involucrados

    public String      getIdr()    {return GT411_Idr.toString();}
    public String      getGti()    {return GT411_Gti.toString();}
    public String      getSeq()    {return GT411_Seq.toString();}
    public Grt_Gbs_Det getGbs()    {return GT411_Gbs;}
    public String      getNro()    {return GT411_Nro.toString();}
    public Vector      getGhd()    {return GT411_Ghd;}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_MsgGT411 Inicia_MsgGT411()
  {
    Buf_MsgGT411 l_MsgGT411 = new Buf_MsgGT411();
    l_MsgGT411.GT411_Idr.replace(0, l_MsgGT411.GT411_Idr.capacity(), RUTGEN.Blancos(1 ));
    l_MsgGT411.GT411_Gti.replace(0, l_MsgGT411.GT411_Gti.capacity(), RUTGEN.Blancos(10));
    l_MsgGT411.GT411_Seq.replace(0, l_MsgGT411.GT411_Seq.capacity(), RUTGEN.Blancos(3 ));
    Grt_Gbs_Det Gbs = new Grt_Gbs_Det();
    Gbs.Gbs_Itb.replace(0, Gbs.Gbs_Itb.capacity(), RUTGEN.Blancos(2  ));
    Gbs.Gbs_Itg.replace(0, Gbs.Gbs_Itg.capacity(), RUTGEN.Blancos(5  ));
    Gbs.Gbs_Csv.replace(0, Gbs.Gbs_Csv.capacity(), RUTGEN.Blancos(3  ));
    Gbs.Gbs_Lcl.replace(0, Gbs.Gbs_Lcl.capacity(), RUTGEN.Blancos(15 ));
    Gbs.Gbs_Rpo.replace(0, Gbs.Gbs_Rpo.capacity(), RUTGEN.Blancos(4  ));
    Gbs.Gbs_Foj.replace(0, Gbs.Gbs_Foj.capacity(), RUTGEN.Blancos(7  ));
    Gbs.Gbs_Nin.replace(0, Gbs.Gbs_Nin.capacity(), RUTGEN.Blancos(7  ));
    Gbs.Gbs_Dsb.replace(0, Gbs.Gbs_Dsb.capacity(), RUTGEN.Blancos(100));
    Gbs.Gbs_Vtt.replace(0, Gbs.Gbs_Vtt.capacity(), RUTGEN.Blancos(15 ));
    Gbs.Gbs_Vts.replace(0, Gbs.Gbs_Vts.capacity(), RUTGEN.Blancos(15 ));
    Gbs.Gbs_Frv.replace(0, Gbs.Gbs_Frv.capacity(), RUTGEN.Blancos(8  ));
    Gbs.Gbs_Vur.replace(0, Gbs.Gbs_Vur.capacity(), RUTGEN.Blancos(9  ));
    Gbs.Gbs_Dpc.replace(0, Gbs.Gbs_Dpc.capacity(), RUTGEN.Blancos(10 ));
    Gbs.Gbs_Pct.replace(0, Gbs.Gbs_Pct.capacity(), RUTGEN.Blancos(5  ));
    Gbs.Gbs_Vcn.replace(0, Gbs.Gbs_Vcn.capacity(), RUTGEN.Blancos(15 ));
    Gbs.Gbs_Vlq.replace(0, Gbs.Gbs_Vlq.capacity(), RUTGEN.Blancos(15 ));
    Gbs.Gbs_Vtb.replace(0, Gbs.Gbs_Vtb.capacity(), RUTGEN.Blancos(15 ));
    Gbs.Gbs_Fvt.replace(0, Gbs.Gbs_Fvt.capacity(), RUTGEN.Blancos(8  ));
    Gbs.Gbs_Obs.replace(0, Gbs.Gbs_Obs.capacity(), RUTGEN.Blancos(9  ));
    Gbs.Gbs_Vsg.replace(0, Gbs.Gbs_Vsg.capacity(), RUTGEN.Blancos(15 ));
    Gbs.Gbs_Csg.replace(0, Gbs.Gbs_Csg.capacity(), RUTGEN.Blancos(3  ));
    Gbs.Gbs_Spz.replace(0, Gbs.Gbs_Spz.capacity(), RUTGEN.Blancos(10 ));
    Gbs.Gbs_Sfv.replace(0, Gbs.Gbs_Sfv.capacity(), RUTGEN.Blancos(8  ));
    Gbs.Gbs_Sum.replace(0, Gbs.Gbs_Sum.capacity(), RUTGEN.Blancos(1  ));
    Gbs.Gbs_Svl.replace(0, Gbs.Gbs_Svl.capacity(), RUTGEN.Blancos(15 ));
    Gbs.Gah_Drb.replace(0, Gbs.Gah_Drb.capacity(), RUTGEN.Blancos(107));
    Gbs.Gah_Cmn.replace(0, Gbs.Gah_Cmn.capacity(), RUTGEN.Blancos(11 ));
    Gbs.Gah_Coo.replace(0, Gbs.Gah_Coo.capacity(), RUTGEN.Blancos(10 ));
    Gbs.Gti_Trj.replace(0, Gbs.Gti_Trj.capacity(), RUTGEN.Blancos(3  ));
    Gbs.Gti_Mnd.replace(0, Gbs.Gti_Mnd.capacity(), RUTGEN.Blancos(3  ));
    Gbs.Gti_Dcm.replace(0, Gbs.Gti_Dcm.capacity(), RUTGEN.Blancos(1  ));
    l_MsgGT411.GT411_Gbs = Gbs;
    l_MsgGT411.GT411_Nro.replace(0, l_MsgGT411.GT411_Nro.capacity(), RUTGEN.Blancos(2 ));
    int p=0;
    for (int i=0; i<g_Max_GT411; i++)
        {
          Grt_Ghd_Det Tap = new Grt_Ghd_Det();
          Tap.Ghd_Sqd.replace(0, Tap.Ghd_Sqd.capacity(), RUTGEN.Blancos(3 ));
          Tap.Ghd_Ith.replace(0, Tap.Ghd_Ith.capacity(), RUTGEN.Blancos(2 ));
          Tap.Ghd_Dsh.replace(0, Tap.Ghd_Dsh.capacity(), RUTGEN.Blancos(30));
          Tap.Ghd_Udd.replace(0, Tap.Ghd_Udd.capacity(), RUTGEN.Blancos(9 ));
          Tap.Ghd_Umd.replace(0, Tap.Ghd_Umd.capacity(), RUTGEN.Blancos(2 ));
          Tap.Ghd_Umt.replace(0, Tap.Ghd_Umt.capacity(), RUTGEN.Blancos(1 ));
          Tap.Ghd_Pum.replace(0, Tap.Ghd_Pum.capacity(), RUTGEN.Blancos(15));
          Tap.Ghd_Vtu.replace(0, Tap.Ghd_Vtu.capacity(), RUTGEN.Blancos(15));
          Tap.Ghd_Vum.replace(0, Tap.Ghd_Vum.capacity(), RUTGEN.Blancos(9 ));
          Tap.Ghd_Vtt.replace(0, Tap.Ghd_Vtt.capacity(), RUTGEN.Blancos(15));
          Tap.Ghd_Vts.replace(0, Tap.Ghd_Vts.capacity(), RUTGEN.Blancos(15));
          l_MsgGT411.GT411_Ghd.add(Tap);
        }
    return l_MsgGT411;
  }
  //---------------------------------------------------------------------------------------
  static public Buf_MsgGT411 LSet_A_MsgGT411(String pcStr)
  {
    Buf_MsgGT411 l_MsgGT411 = new Buf_MsgGT411();
    MSGGT411 vMsgGT411 = new MSGGT411();
    Vector vDatos = vMsgGT411.LSet_A_vMsgGT411(pcStr);
    l_MsgGT411 = (MSGGT411.Buf_MsgGT411)vDatos.elementAt(0);
    return l_MsgGT411;
  }
  //---------------------------------------------------------------------------------------
  public Vector LSet_A_vMsgGT411(String pcStr)
  {
    Vector vec = new Vector();
    int p = 0;
    Buf_MsgGT411 l_MsgGT411 = new Buf_MsgGT411();
    l_MsgGT411.GT411_Idr.append( pcStr.substring(p, p + l_MsgGT411.GT411_Idr.capacity())); p = p + l_MsgGT411.GT411_Idr.capacity();
    l_MsgGT411.GT411_Gti.append( pcStr.substring(p, p + l_MsgGT411.GT411_Gti.capacity())); p = p + l_MsgGT411.GT411_Gti.capacity();
    l_MsgGT411.GT411_Seq.append( pcStr.substring(p, p + l_MsgGT411.GT411_Seq.capacity())); p = p + l_MsgGT411.GT411_Seq.capacity();
    Grt_Gbs_Det Gbs = new Grt_Gbs_Det();
    Gbs.Gbs_Itb.replace(0, Gbs.Gbs_Itb.capacity(), pcStr.substring(p, p + Gbs.Gbs_Itb.capacity())); p = p + Gbs.Gbs_Itb.capacity();
    Gbs.Gbs_Itg.replace(0, Gbs.Gbs_Itg.capacity(), pcStr.substring(p, p + Gbs.Gbs_Itg.capacity())); p = p + Gbs.Gbs_Itg.capacity();
    Gbs.Gbs_Csv.replace(0, Gbs.Gbs_Csv.capacity(), pcStr.substring(p, p + Gbs.Gbs_Csv.capacity())); p = p + Gbs.Gbs_Csv.capacity();
    Gbs.Gbs_Lcl.replace(0, Gbs.Gbs_Lcl.capacity(), pcStr.substring(p, p + Gbs.Gbs_Lcl.capacity())); p = p + Gbs.Gbs_Lcl.capacity();
    Gbs.Gbs_Rpo.replace(0, Gbs.Gbs_Rpo.capacity(), pcStr.substring(p, p + Gbs.Gbs_Rpo.capacity())); p = p + Gbs.Gbs_Rpo.capacity();
    Gbs.Gbs_Foj.replace(0, Gbs.Gbs_Foj.capacity(), pcStr.substring(p, p + Gbs.Gbs_Foj.capacity())); p = p + Gbs.Gbs_Foj.capacity();
    Gbs.Gbs_Nin.replace(0, Gbs.Gbs_Nin.capacity(), pcStr.substring(p, p + Gbs.Gbs_Nin.capacity())); p = p + Gbs.Gbs_Nin.capacity();
    Gbs.Gbs_Dsb.replace(0, Gbs.Gbs_Dsb.capacity(), pcStr.substring(p, p + Gbs.Gbs_Dsb.capacity())); p = p + Gbs.Gbs_Dsb.capacity();
    Gbs.Gbs_Vtt.replace(0, Gbs.Gbs_Vtt.capacity(), pcStr.substring(p, p + Gbs.Gbs_Vtt.capacity())); p = p + Gbs.Gbs_Vtt.capacity();
    Gbs.Gbs_Vts.replace(0, Gbs.Gbs_Vts.capacity(), pcStr.substring(p, p + Gbs.Gbs_Vts.capacity())); p = p + Gbs.Gbs_Vts.capacity();
    Gbs.Gbs_Frv.replace(0, Gbs.Gbs_Frv.capacity(), pcStr.substring(p, p + Gbs.Gbs_Frv.capacity())); p = p + Gbs.Gbs_Frv.capacity();
    Gbs.Gbs_Vur.replace(0, Gbs.Gbs_Vur.capacity(), pcStr.substring(p, p + Gbs.Gbs_Vur.capacity())); p = p + Gbs.Gbs_Vur.capacity();
    Gbs.Gbs_Dpc.replace(0, Gbs.Gbs_Dpc.capacity(), pcStr.substring(p, p + Gbs.Gbs_Dpc.capacity())); p = p + Gbs.Gbs_Dpc.capacity();
    Gbs.Gbs_Pct.replace(0, Gbs.Gbs_Pct.capacity(), pcStr.substring(p, p + Gbs.Gbs_Pct.capacity())); p = p + Gbs.Gbs_Pct.capacity();
    Gbs.Gbs_Vcn.replace(0, Gbs.Gbs_Vcn.capacity(), pcStr.substring(p, p + Gbs.Gbs_Vcn.capacity())); p = p + Gbs.Gbs_Vcn.capacity();
    Gbs.Gbs_Vlq.replace(0, Gbs.Gbs_Vlq.capacity(), pcStr.substring(p, p + Gbs.Gbs_Vlq.capacity())); p = p + Gbs.Gbs_Vlq.capacity();
    Gbs.Gbs_Vtb.replace(0, Gbs.Gbs_Vtb.capacity(), pcStr.substring(p, p + Gbs.Gbs_Vtb.capacity())); p = p + Gbs.Gbs_Vtb.capacity();
    Gbs.Gbs_Fvt.replace(0, Gbs.Gbs_Fvt.capacity(), pcStr.substring(p, p + Gbs.Gbs_Fvt.capacity())); p = p + Gbs.Gbs_Fvt.capacity();
    Gbs.Gbs_Obs.replace(0, Gbs.Gbs_Obs.capacity(), pcStr.substring(p, p + Gbs.Gbs_Obs.capacity())); p = p + Gbs.Gbs_Obs.capacity();
    Gbs.Gbs_Vsg.replace(0, Gbs.Gbs_Vsg.capacity(), pcStr.substring(p, p + Gbs.Gbs_Vsg.capacity())); p = p + Gbs.Gbs_Vsg.capacity();
    Gbs.Gbs_Csg.replace(0, Gbs.Gbs_Csg.capacity(), pcStr.substring(p, p + Gbs.Gbs_Csg.capacity())); p = p + Gbs.Gbs_Csg.capacity();
    Gbs.Gbs_Spz.replace(0, Gbs.Gbs_Spz.capacity(), pcStr.substring(p, p + Gbs.Gbs_Spz.capacity())); p = p + Gbs.Gbs_Spz.capacity();
    Gbs.Gbs_Sfv.replace(0, Gbs.Gbs_Sfv.capacity(), pcStr.substring(p, p + Gbs.Gbs_Sfv.capacity())); p = p + Gbs.Gbs_Sfv.capacity();
    Gbs.Gbs_Sum.replace(0, Gbs.Gbs_Sum.capacity(), pcStr.substring(p, p + Gbs.Gbs_Sum.capacity())); p = p + Gbs.Gbs_Sum.capacity();
    Gbs.Gbs_Svl.replace(0, Gbs.Gbs_Svl.capacity(), pcStr.substring(p, p + Gbs.Gbs_Svl.capacity())); p = p + Gbs.Gbs_Svl.capacity();
    Gbs.Gah_Drb.replace(0, Gbs.Gah_Drb.capacity(), pcStr.substring(p, p + Gbs.Gah_Drb.capacity())); p = p + Gbs.Gah_Drb.capacity();
    Gbs.Gah_Cmn.replace(0, Gbs.Gah_Cmn.capacity(), pcStr.substring(p, p + Gbs.Gah_Cmn.capacity())); p = p + Gbs.Gah_Cmn.capacity();
    Gbs.Gah_Coo.replace(0, Gbs.Gah_Coo.capacity(), pcStr.substring(p, p + Gbs.Gah_Coo.capacity())); p = p + Gbs.Gah_Coo.capacity();
    Gbs.Gti_Trj.replace(0, Gbs.Gti_Trj.capacity(), pcStr.substring(p, p + Gbs.Gti_Trj.capacity())); p = p + Gbs.Gti_Trj.capacity();
    Gbs.Gti_Mnd.replace(0, Gbs.Gti_Mnd.capacity(), pcStr.substring(p, p + Gbs.Gti_Mnd.capacity())); p = p + Gbs.Gti_Mnd.capacity();
    Gbs.Gti_Dcm.replace(0, Gbs.Gti_Dcm.capacity(), pcStr.substring(p, p + Gbs.Gti_Dcm.capacity())); p = p + Gbs.Gti_Dcm.capacity();
    l_MsgGT411.GT411_Gbs = Gbs;
    l_MsgGT411.GT411_Nro.replace(0, l_MsgGT411.GT411_Nro.capacity(), pcStr.substring(p, p + l_MsgGT411.GT411_Nro.capacity())); p = p + l_MsgGT411.GT411_Nro.capacity();
    for (int i=0;i< Integer.parseInt(l_MsgGT411.GT411_Nro.toString());i++)
        {
          Grt_Ghd_Det Tap = new Grt_Ghd_Det();
          Tap.Ghd_Sqd.replace(0, Tap.Ghd_Sqd.capacity(), pcStr.substring(p, p + Tap.Ghd_Sqd.capacity())); p = p + Tap.Ghd_Sqd.capacity();
          Tap.Ghd_Ith.replace(0, Tap.Ghd_Ith.capacity(), pcStr.substring(p, p + Tap.Ghd_Ith.capacity())); p = p + Tap.Ghd_Ith.capacity();
          Tap.Ghd_Dsh.replace(0, Tap.Ghd_Dsh.capacity(), pcStr.substring(p, p + Tap.Ghd_Dsh.capacity())); p = p + Tap.Ghd_Dsh.capacity();
          Tap.Ghd_Udd.replace(0, Tap.Ghd_Udd.capacity(), pcStr.substring(p, p + Tap.Ghd_Udd.capacity())); p = p + Tap.Ghd_Udd.capacity();
          Tap.Ghd_Umd.replace(0, Tap.Ghd_Umd.capacity(), pcStr.substring(p, p + Tap.Ghd_Umd.capacity())); p = p + Tap.Ghd_Umd.capacity();
          Tap.Ghd_Umt.replace(0, Tap.Ghd_Umt.capacity(), pcStr.substring(p, p + Tap.Ghd_Umt.capacity())); p = p + Tap.Ghd_Umt.capacity();
          Tap.Ghd_Pum.replace(0, Tap.Ghd_Pum.capacity(), pcStr.substring(p, p + Tap.Ghd_Pum.capacity())); p = p + Tap.Ghd_Pum.capacity();
          Tap.Ghd_Vtu.replace(0, Tap.Ghd_Vtu.capacity(), pcStr.substring(p, p + Tap.Ghd_Vtu.capacity())); p = p + Tap.Ghd_Vtu.capacity();
          Tap.Ghd_Vum.replace(0, Tap.Ghd_Vum.capacity(), pcStr.substring(p, p + Tap.Ghd_Vum.capacity())); p = p + Tap.Ghd_Vum.capacity();
          Tap.Ghd_Vtt.replace(0, Tap.Ghd_Vtt.capacity(), pcStr.substring(p, p + Tap.Ghd_Vtt.capacity())); p = p + Tap.Ghd_Vtt.capacity();
          Tap.Ghd_Vts.replace(0, Tap.Ghd_Vts.capacity(), pcStr.substring(p, p + Tap.Ghd_Vts.capacity())); p = p + Tap.Ghd_Vts.capacity();
          l_MsgGT411.GT411_Ghd.add(Tap);
        }
    vec.add (l_MsgGT411);
    return vec;
  }
  //---------------------------------------------------------------------------------------
  static public String LSet_De_MsgGT411(Buf_MsgGT411 p_MsgGT411)
  {
    String pcStr = "";
    pcStr = pcStr + p_MsgGT411.GT411_Idr.toString();
    pcStr = pcStr + p_MsgGT411.GT411_Gti.toString();
    pcStr = pcStr + p_MsgGT411.GT411_Seq.toString();
    Grt_Gbs_Det Gbs = new Grt_Gbs_Det();
    Gbs = p_MsgGT411.GT411_Gbs;
    pcStr = pcStr + Gbs.Gbs_Itb.toString();
    pcStr = pcStr + Gbs.Gbs_Itg.toString();
    pcStr = pcStr + Gbs.Gbs_Csv.toString();
    pcStr = pcStr + Gbs.Gbs_Lcl.toString();
    pcStr = pcStr + Gbs.Gbs_Rpo.toString();
    pcStr = pcStr + Gbs.Gbs_Foj.toString();
    pcStr = pcStr + Gbs.Gbs_Nin.toString();
    pcStr = pcStr + Gbs.Gbs_Dsb.toString();
    pcStr = pcStr + Gbs.Gbs_Vtt.toString();
    pcStr = pcStr + Gbs.Gbs_Vts.toString();
    pcStr = pcStr + Gbs.Gbs_Frv.toString();
    pcStr = pcStr + Gbs.Gbs_Vur.toString();
    pcStr = pcStr + Gbs.Gbs_Dpc.toString();
    pcStr = pcStr + Gbs.Gbs_Pct.toString();
    pcStr = pcStr + Gbs.Gbs_Vcn.toString();
    pcStr = pcStr + Gbs.Gbs_Vlq.toString();
    pcStr = pcStr + Gbs.Gbs_Vtb.toString();
    pcStr = pcStr + Gbs.Gbs_Fvt.toString();
    pcStr = pcStr + Gbs.Gbs_Obs.toString();
    pcStr = pcStr + Gbs.Gbs_Vsg.toString();
    pcStr = pcStr + Gbs.Gbs_Csg.toString();
    pcStr = pcStr + Gbs.Gbs_Spz.toString();
    pcStr = pcStr + Gbs.Gbs_Sfv.toString();
    pcStr = pcStr + Gbs.Gbs_Sum.toString();
    pcStr = pcStr + Gbs.Gbs_Svl.toString();
    pcStr = pcStr + Gbs.Gah_Drb.toString();
    pcStr = pcStr + Gbs.Gah_Cmn.toString();
    pcStr = pcStr + Gbs.Gah_Coo.toString();
    pcStr = pcStr + Gbs.Gti_Trj.toString();
    pcStr = pcStr + Gbs.Gti_Mnd.toString();
    pcStr = pcStr + Gbs.Gti_Dcm.toString();
    pcStr = pcStr + p_MsgGT411.GT411_Nro.toString();
    for (int i=0; i<p_MsgGT411.GT411_Ghd.size(); i++)
        {
          pcStr = pcStr + ((Grt_Ghd_Det)p_MsgGT411.GT411_Ghd.elementAt(i)).Ghd_Sqd.toString();
          pcStr = pcStr + ((Grt_Ghd_Det)p_MsgGT411.GT411_Ghd.elementAt(i)).Ghd_Ith.toString();
          pcStr = pcStr + ((Grt_Ghd_Det)p_MsgGT411.GT411_Ghd.elementAt(i)).Ghd_Dsh.toString();
          pcStr = pcStr + ((Grt_Ghd_Det)p_MsgGT411.GT411_Ghd.elementAt(i)).Ghd_Udd.toString();
          pcStr = pcStr + ((Grt_Ghd_Det)p_MsgGT411.GT411_Ghd.elementAt(i)).Ghd_Umd.toString();
          pcStr = pcStr + ((Grt_Ghd_Det)p_MsgGT411.GT411_Ghd.elementAt(i)).Ghd_Umt.toString();
          pcStr = pcStr + ((Grt_Ghd_Det)p_MsgGT411.GT411_Ghd.elementAt(i)).Ghd_Pum.toString();
          pcStr = pcStr + ((Grt_Ghd_Det)p_MsgGT411.GT411_Ghd.elementAt(i)).Ghd_Vtu.toString();
          pcStr = pcStr + ((Grt_Ghd_Det)p_MsgGT411.GT411_Ghd.elementAt(i)).Ghd_Vum.toString();
          pcStr = pcStr + ((Grt_Ghd_Det)p_MsgGT411.GT411_Ghd.elementAt(i)).Ghd_Vtt.toString();
          pcStr = pcStr + ((Grt_Ghd_Det)p_MsgGT411.GT411_Ghd.elementAt(i)).Ghd_Vts.toString();
        }
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
}