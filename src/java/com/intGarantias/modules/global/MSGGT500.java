// Source File Name:   MSGGT500.java

package com.intGarantias.modules.global;

import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.util.Formateo;
import com.FHTServlet.modules.util.Constants;

import org.apache.turbine.om.peer.BasePeer;
import com.workingdogs.village.Record;

import java.util.*;
import java.lang.*;
import java.text.*;
import org.apache.turbine.util.Log;

public class MSGGT500
{
  //===============================================================================================================================
  public static int g_Max_GT500 = 960;
  //-------------------------------------------------------------------------------------------
  public static class Bff_MsgGT500
  {
    public StringBuffer GT500_Reg = new StringBuffer(3 );    //9(03)      Numero Registro
    public StringBuffer GT500_Rvt = new StringBuffer(1 );    //9(01)      Rango (1/2/3/4)
    public StringBuffer GT500_Fvc = new StringBuffer(8 );    //9(08)      Fecha Vencto
    public StringBuffer GT500_Gti = new StringBuffer(10);    //X(10)      Id.Operacion
    public StringBuffer GT500_Sqb = new StringBuffer(3 );    //9(03)      Id.Bien
    public StringBuffer GT500_Tpv = new StringBuffer(1 );    //X(01)      Tipo Vencto (G/S/T)
    public StringBuffer GT500_Dcn = new StringBuffer(5 );    //X(05)      Codigo Producto
    public StringBuffer GT500_Ndc = new StringBuffer(30);    //X(30)      Nombre Producto
    public StringBuffer GT500_Dsg = new StringBuffer(100);   //X(100)     Descripcion Garantia

    public String getReg()         {return GT500_Reg.toString();}
    public String getRvt()         {return GT500_Rvt.toString();}
    public String getFvc()         {return GT500_Fvc.toString();}
    public String getGti()         {return GT500_Gti.toString();}
    public String getSqb()         {return GT500_Sqb.toString();}
    public String getTpv()         {return GT500_Tpv.toString();}
    public String getDcn()         {return GT500_Dcn.toString();}
    public String getNdc()         {return GT500_Ndc.toString();}
    public String getDsg()         {return GT500_Dsg.toString();}

    public Formateo f = new Formateo();
    public String fgetGti()        {return getGti().substring(0,3) + "-" + getGti().substring(3,10) + "." + getSqb();}
    public String fgetFvc()        {return f.FormatDate(getFvc());}
    public int    getDiaDif(String FHasta)
                                   {return RUTGEN.DateDiff(FHasta, getFvc()); }
  }
  //===============================================================================================================================
  public static class Buf_MsgGT500
  {
    public StringBuffer GT500_Idr = new StringBuffer(1); //X(01)      Id.Requerimiento
    public StringBuffer GT500_Idx = new StringBuffer(3); //9(03)      Indice (inp=INI/out=SEL)
    public StringBuffer GT500_Eje = new StringBuffer(4); //9(04)      Ejecutivo
    public StringBuffer GT500_Nro = new StringBuffer(3); //9(03)      Numero Elementos
    public Vector       GT500_Rgs = new Vector();

    public String getIdr()         {return GT500_Idr.toString();}
    public String getIdx()         {return GT500_Idx.toString();}
    public String getEje()         {return GT500_Eje.toString();}
    public String getNro()         {return GT500_Nro.toString();}
    public Vector getRgs()         {return GT500_Rgs;}

    public int    igetMax()        {return g_Max_GT500;}
    public int    igetIdx()        {if (getIdx().trim().equals("")) return 0; else return Integer.parseInt(getIdx());}
    public int    igetNro()        {if (getNro().trim().equals("")) return 0; else return Integer.parseInt(getNro());}
  }
  //-------------------------------------------------------------------------------------------
  public static Buf_MsgGT500 Inicia_MsgGT500()
  {
    Buf_MsgGT500 l_MsgGT500 = new Buf_MsgGT500();
    l_MsgGT500.GT500_Idr.replace(0,l_MsgGT500.GT500_Idr.capacity(), RUTGEN.Blancos(l_MsgGT500.GT500_Idr));
    l_MsgGT500.GT500_Idx.replace(0,l_MsgGT500.GT500_Idx.capacity(), RUTGEN.Blancos(l_MsgGT500.GT500_Idx));
    l_MsgGT500.GT500_Eje.replace(0,l_MsgGT500.GT500_Eje.capacity(), RUTGEN.Blancos(l_MsgGT500.GT500_Eje));
    l_MsgGT500.GT500_Nro.replace(0,l_MsgGT500.GT500_Nro.capacity(), RUTGEN.Blancos(l_MsgGT500.GT500_Nro));
    l_MsgGT500.GT500_Rgs.clear();
    return l_MsgGT500;
  }
  //===============================================================================================================================
  public static void Consulta_MsgGT500(Buf_MsgGT500 pMsgGT500) throws Exception
  {
    String lcSNT = pMsgGT500.getIdr();
    String lcINI = pMsgGT500.getIdx();
    String lcEJE = pMsgGT500.getEje().trim();
Log.debug("[Consulta_MsgGT500[parms]:[" + lcSNT + "][" + lcINI + "][" + lcEJE + "]");
    //--------------------------------------------------------------
    int liIdx = -1;
    Vector vGT500  = new Vector();
    String ultKEY = elQuery(lcSNT, lcINI, lcEJE, vGT500);
Log.debug("[Consulta_MsgGT500[ultKEY]:[" + ultKEY + "]");
    RUTGEN.MoverA(pMsgGT500.GT500_Idr, "");
    if (vGT500.size()==0)
       { RUTGEN.MoverA(pMsgGT500.GT500_Idx, 0); }
    else
       {
         if (vGT500.size()<g_Max_GT500)
            {
              liIdx = g_Max_GT500 - vGT500.size() + 1;
              vGT500.clear();
              if (lcSNT.trim().equals("P"))
                 {
                   ultKEY  = elQuery("S", ultKEY, lcEJE, vGT500);
                   RUTGEN.MoverA(pMsgGT500.GT500_Idr, "F");
                 }
              else
                 {
                   ultKEY  = elQuery("P", ultKEY, lcEJE, vGT500);
                   RUTGEN.MoverA(pMsgGT500.GT500_Idr, "L");
                 }
            }
         else
            {
              if (lcSNT.trim().equals("I")
               || Integer.parseInt(ultKEY)==1)
                 { RUTGEN.MoverA(pMsgGT500.GT500_Idr, "F"); }
            }
         if (liIdx==-1)
            { RUTGEN.MoverA(pMsgGT500.GT500_Idx, 1); }
         else
            { RUTGEN.MoverA(pMsgGT500.GT500_Idx, liIdx); }
       }
    RUTGEN.MoverA(pMsgGT500.GT500_Nro, vGT500.size());
Log.debug("[Consulta_MsgGT500[salida]:[" + pMsgGT500.getIdr() + "][" + pMsgGT500.getIdx() + "][" + pMsgGT500.getNro() + "]");
    pMsgGT500.GT500_Rgs = vGT500;
  }
  //-------------------------------------------------------------------------------------------
  static public String elQuery(String pcSNT, String pcINI, String pcEJE, Vector vGT500) throws Exception
  {
    String lcKEY_P = "";
    String lcKEY_U = "";
    int    liINI   = Integer.parseInt(pcINI);
    if (pcSNT.trim().equals("P"))
       {
         liINI = liINI - g_Max_GT500 + 1;
         if (liINI < 1)
            { liINI = 1; }
       }
    Vector vQuery  = new Vector();
    String sqlText = elStmt(pcEJE, liINI);
    vQuery  = BasePeer.executeQuery(sqlText, Constants.BASE_GAR);
    for (int i=0; i<vQuery.size(); i++)
        {
          Record Reg = (Record)vQuery.elementAt(i);
          Bff_MsgGT500 dMsgGT500 = new Bff_MsgGT500();
          RUTGEN.MoverA(dMsgGT500.GT500_Reg, Reg.getValue("NUM_REG").asInt());
          RUTGEN.MoverA(dMsgGT500.GT500_Rvt, Reg.getValue("VTG_RVT").asInt());
          RUTGEN.MoverA(dMsgGT500.GT500_Fvc, Reg.getValue("VTG_FVC").asUtilDate());
          RUTGEN.MoverA(dMsgGT500.GT500_Gti, Reg.getValue("VTG_GTI").asString());
          RUTGEN.MoverA(dMsgGT500.GT500_Sqb, Reg.getValue("VTG_SQB").asInt());
          RUTGEN.MoverA(dMsgGT500.GT500_Tpv, Reg.getValue("VTG_TPV").asString());
          RUTGEN.MoverA(dMsgGT500.GT500_Dcn, Reg.getValue("VTG_DCN").asString());
          RUTGEN.MoverA(dMsgGT500.GT500_Ndc, Reg.getValue("VTG_NDC").asString());
          RUTGEN.MoverA(dMsgGT500.GT500_Dsg, Reg.getValue("VTG_DSG").asString());
          if (i==0)
             { lcKEY_P = dMsgGT500.getReg(); }
          lcKEY_U = dMsgGT500.getReg();
          vGT500.add(dMsgGT500);
        }
    if (pcSNT.trim().equals("P"))
       { return lcKEY_P; }
    return lcKEY_U;
  }
  //-------------------------------------------------------------------------------------------
  static public String elStmt(String pcEJE, int piINI)
  {
    String lcSIS   = "GAR";
    int    liFIN   = piINI + g_Max_GT500 - 1;
    String sqlDATA = "Select                                                                                    "
                   + "       VTG_RVT,                                                                           "
                   + "       VTG_FVC,                                                                           "
                   + "       VTG_GTI,                                                                           "
                   + "       VTG_SQB,                                                                           "
                   + "       VTG_TPV,                                                                           "
                   + "       VTG_DCN,                                                                           "
                   + "       (Select DCN_DSC From TT_DCN Where DCN_SIS = 'GAR' And DCN_COD = VTG_DCN)  VTG_NDC, "
                   + "       (Select GTI_DSG From TT_GTI Where                     GTI_KYV = VTG_GTI)  VTG_DSG, "
                   + "       Row_Number() Over(Order By VTG_KYV Asc)                                   NUM_REG  "
                   + "  From TT_VTG                                                                             "
                   + " Where                                                                                    "
                   + "       VTG_EJE = '" + pcEJE + "'                                                          ";
    String sqlText = "Select *                                               "
                   + "  From (" + sqlDATA + ")                               "
                   + " Where NUM_REG Between " + piINI + " And " + liFIN + " "
                   + " Order By NUM_REG Asc                                  ";
    return sqlText;
  }
  //===============================================================================================================================
}