// Source File Name:   MSGGT140.java

package com.intGarantias.modules.global;

import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.util.Formateo;

import java.util.*;
import java.lang.*;
import org.apache.turbine.util.Log;

public class MSGGT140
{
  //---------------------------------------------------------------------------------------
  public static class Buf_MsgGT140
  {
    public StringBuffer GT140_Idr = new StringBuffer(1  );  //X(01)        1 Idr Host
    public StringBuffer GT140_Gti = new StringBuffer(10 );  //X(10)       11 Id.Garantia a buscar
    public StringBuffer GT140_Cll = new StringBuffer(34 );  //X(34)       45 Calle
    public StringBuffer GT140_Cnm = new StringBuffer(6  );  //X(06)       51 Calle Numero
    public StringBuffer GT140_Lug = new StringBuffer(2  );  //X(02)       53 Lugar
    public StringBuffer GT140_Lnm = new StringBuffer(6  );  //X(06)       59 Lugar Numero
    public StringBuffer GT140_Sec = new StringBuffer(2  );  //X(02)       61 Sector
    public StringBuffer GT140_Snb = new StringBuffer(20 );  //X(20)       81 Sector Nombre
    public StringBuffer GT140_Cmn = new StringBuffer(11 );  //9(11)       92 Comuna
    public StringBuffer GT140_Ncm = new StringBuffer(20 );  //X(20)      112 Nombre Comuna
    public StringBuffer GT140_Npv = new StringBuffer(17 );  //X(17)      129 Nombre Provincia
    public StringBuffer GT140_Nc1 = new StringBuffer(40 );  //X(40)      169 Nombre Contacto 1
    public StringBuffer GT140_T11 = new StringBuffer(10 );  //X(10)      179 Fono 1 Contacto 1

    public String getIdr()  {return GT140_Idr.toString();}
    public String getGti()  {return GT140_Gti.toString();}
    public String getCll()  {return GT140_Cll.toString();}
    public String getCnm()  {return GT140_Cnm.toString();}
    public String getLug()  {return GT140_Lug.toString();}
    public String getLnm()  {return GT140_Lnm.toString();}
    public String getSec()  {return GT140_Sec.toString();}
    public String getSnb()  {return GT140_Snb.toString();}
    public String getCmn()  {return GT140_Cmn.toString();}
    public String getNcm()  {return GT140_Ncm.toString();}
    public String getNpv()  {return GT140_Npv.toString();}
    public String getNc1()  {return GT140_Nc1.toString();}
    public String getT11()  {return GT140_T11.toString();}
  }
  //---------------------------------------------------------------------------------------
  static public Buf_MsgGT140 Inicia_MsgGT140()
  {
    Buf_MsgGT140 l_MsgGT140 = new Buf_MsgGT140();
    l_MsgGT140.GT140_Idr.replace(0,l_MsgGT140.GT140_Idr.capacity(), RUTGEN.Blancos(1  ));
    l_MsgGT140.GT140_Gti.replace(0,l_MsgGT140.GT140_Gti.capacity(), RUTGEN.Blancos(10 ));
    l_MsgGT140.GT140_Cll.replace(0,l_MsgGT140.GT140_Cll.capacity(), RUTGEN.Blancos(34 ));
    l_MsgGT140.GT140_Cnm.replace(0,l_MsgGT140.GT140_Cnm.capacity(), RUTGEN.Blancos(6  ));
    l_MsgGT140.GT140_Lug.replace(0,l_MsgGT140.GT140_Lug.capacity(), RUTGEN.Blancos(2  ));
    l_MsgGT140.GT140_Lnm.replace(0,l_MsgGT140.GT140_Lnm.capacity(), RUTGEN.Blancos(6  ));
    l_MsgGT140.GT140_Sec.replace(0,l_MsgGT140.GT140_Sec.capacity(), RUTGEN.Blancos(2  ));
    l_MsgGT140.GT140_Snb.replace(0,l_MsgGT140.GT140_Snb.capacity(), RUTGEN.Blancos(20 ));
    l_MsgGT140.GT140_Cmn.replace(0,l_MsgGT140.GT140_Cmn.capacity(), RUTGEN.Blancos(11 ));
    l_MsgGT140.GT140_Ncm.replace(0,l_MsgGT140.GT140_Ncm.capacity(), RUTGEN.Blancos(20 ));
    l_MsgGT140.GT140_Npv.replace(0,l_MsgGT140.GT140_Npv.capacity(), RUTGEN.Blancos(17 ));
    l_MsgGT140.GT140_Nc1.replace(0,l_MsgGT140.GT140_Nc1.capacity(), RUTGEN.Blancos(40 ));
    l_MsgGT140.GT140_T11.replace(0,l_MsgGT140.GT140_T11.capacity(), RUTGEN.Blancos(10 ));
    return l_MsgGT140;
  }
  //---------------------------------------------------------------------------------------
  static public Buf_MsgGT140 LSet_A_MsgGT140 (String pcStr)
  {
    Buf_MsgGT140 l_MsgGT140 = new Buf_MsgGT140();
    MSGGT140 vMsgGT140 = new MSGGT140();
    Vector vDatos = vMsgGT140.LSet_A_vMsgGT140(pcStr);
    l_MsgGT140 = (MSGGT140.Buf_MsgGT140)vDatos.elementAt(0);
    return l_MsgGT140;
  }
  //---------------------------------------------------------------------------------------
  public Vector LSet_A_vMsgGT140(String pcStr)
  {
    Vector vec = new Vector();
    int p = 0;
    Buf_MsgGT140 l_MsgGT140 = new Buf_MsgGT140();
    l_MsgGT140.GT140_Idr.append (pcStr.substring(p , p + l_MsgGT140.GT140_Idr.capacity())); p = p + l_MsgGT140.GT140_Idr.capacity();
    l_MsgGT140.GT140_Gti.append (pcStr.substring(p , p + l_MsgGT140.GT140_Gti.capacity())); p = p + l_MsgGT140.GT140_Gti.capacity();
    l_MsgGT140.GT140_Cll.append (pcStr.substring(p , p + l_MsgGT140.GT140_Cll.capacity())); p = p + l_MsgGT140.GT140_Cll.capacity();
    l_MsgGT140.GT140_Cnm.append (pcStr.substring(p , p + l_MsgGT140.GT140_Cnm.capacity())); p = p + l_MsgGT140.GT140_Cnm.capacity();
    l_MsgGT140.GT140_Lug.append (pcStr.substring(p , p + l_MsgGT140.GT140_Lug.capacity())); p = p + l_MsgGT140.GT140_Lug.capacity();
    l_MsgGT140.GT140_Lnm.append (pcStr.substring(p , p + l_MsgGT140.GT140_Lnm.capacity())); p = p + l_MsgGT140.GT140_Lnm.capacity();
    l_MsgGT140.GT140_Sec.append (pcStr.substring(p , p + l_MsgGT140.GT140_Sec.capacity())); p = p + l_MsgGT140.GT140_Sec.capacity();
    l_MsgGT140.GT140_Snb.append (pcStr.substring(p , p + l_MsgGT140.GT140_Snb.capacity())); p = p + l_MsgGT140.GT140_Snb.capacity();
    l_MsgGT140.GT140_Cmn.append (pcStr.substring(p , p + l_MsgGT140.GT140_Cmn.capacity())); p = p + l_MsgGT140.GT140_Cmn.capacity();
    l_MsgGT140.GT140_Ncm.append (pcStr.substring(p , p + l_MsgGT140.GT140_Ncm.capacity())); p = p + l_MsgGT140.GT140_Ncm.capacity();
    l_MsgGT140.GT140_Npv.append (pcStr.substring(p , p + l_MsgGT140.GT140_Npv.capacity())); p = p + l_MsgGT140.GT140_Npv.capacity();
    l_MsgGT140.GT140_Nc1.append (pcStr.substring(p , p + l_MsgGT140.GT140_Nc1.capacity())); p = p + l_MsgGT140.GT140_Nc1.capacity();
    l_MsgGT140.GT140_T11.append (pcStr.substring(p , p + l_MsgGT140.GT140_T11.capacity())); p = p + l_MsgGT140.GT140_T11.capacity();
    vec.add (l_MsgGT140);
    return vec;
  }
  //---------------------------------------------------------------------------------------
  static public String LSet_De_MsgGT140 (Buf_MsgGT140 p_MsgGT140)
  {
    String pcStr = "";
    pcStr = pcStr + p_MsgGT140.GT140_Idr.toString();
    pcStr = pcStr + p_MsgGT140.GT140_Gti.toString();
    pcStr = pcStr + p_MsgGT140.GT140_Cll.toString();
    pcStr = pcStr + p_MsgGT140.GT140_Cnm.toString();
    pcStr = pcStr + p_MsgGT140.GT140_Lug.toString();
    pcStr = pcStr + p_MsgGT140.GT140_Lnm.toString();
    pcStr = pcStr + p_MsgGT140.GT140_Sec.toString();
    pcStr = pcStr + p_MsgGT140.GT140_Snb.toString();
    pcStr = pcStr + p_MsgGT140.GT140_Cmn.toString();
    pcStr = pcStr + p_MsgGT140.GT140_Ncm.toString();
    pcStr = pcStr + p_MsgGT140.GT140_Npv.toString();
    pcStr = pcStr + p_MsgGT140.GT140_Nc1.toString();
    pcStr = pcStr + p_MsgGT140.GT140_T11.toString();
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
}