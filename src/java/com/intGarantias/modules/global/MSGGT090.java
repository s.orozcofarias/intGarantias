// Source File Name:   MSGGT090.java

package com.intGarantias.modules.global;

import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.util.Formateo;

import java.util.*;
import java.lang.*;
import org.apache.turbine.util.Log;

public class MSGGT090
{
  public static int g_Max_GT090 = 38;
  //---------------------------------------------------------------------------------------
  public static class Bff_MsgGT090
  {
    public StringBuffer GT090_Dcn = new StringBuffer(5 );       //X(05)         5  Tipo Garantia
    public StringBuffer GT090_Dsc = new StringBuffer(30);       //X(30)        35  Descripcion
    public StringBuffer GT090_Tmn = new StringBuffer(3 );       //X(03)        38  Tipo Moneda
    public StringBuffer GT090_Trj = new StringBuffer(3 );       //X(03)        41  Tipo Reajustabilidad
    public StringBuffer GT090_Dcm = new StringBuffer(1 );       //9(01)        42  Decimales
    public StringBuffer GT090_Prp = new StringBuffer(5 );       //X(05)        47  Propietario Contrato
    public StringBuffer GT090_Ggr = new StringBuffer(3 );       //X(03)        50  Grupo Garantias
    public StringBuffer GT090_Sgr = new StringBuffer(2 );       //X(02)        52  SubGrupo Garantias
    public StringBuffer GT090_Tob = new StringBuffer(2 );       //X(02)        54  Tipo Obligacion
    public StringBuffer GT090_Nmt = new StringBuffer(3 );       //9(03)        57  Meses p/Tasacion
    public StringBuffer GT090_Mma = new StringBuffer(7 );       //9(07)        64  Mto.Min.Asegurab(UF)
    public StringBuffer GT090_Vpr = new StringBuffer(5 );       //9(03)V9(2)   69  %Prima Seguro
    public StringBuffer GT090_Cfu = new StringBuffer(11);       //9(07)V9(4)   80  Costo Fijo UF
    public StringBuffer GT090_Coc = new StringBuffer(5 );       //9(03)V9(2)   85  %Comision Corredora
    public StringBuffer GT090_Cms = new StringBuffer(5 );       //9(02)V9(2)   89  %Margen SBIF
    public StringBuffer GT090_Plm = new StringBuffer(2 );       //X(02)        91  Permite Limitacion

    public String getDcn()         {return GT090_Dcn.toString();}
    public String getDsc()         {return GT090_Dsc.toString();}
    public String getTmn()         {return GT090_Tmn.toString();}
    public String getTrj()         {return GT090_Trj.toString();}
    public String getDcm()         {return GT090_Dcm.toString();}
    public String getPrp()         {return GT090_Prp.toString();}
    public String getGgr()         {return GT090_Ggr.toString();}
    public String getSgr()         {return GT090_Sgr.toString();}
    public String getTob()         {return GT090_Tob.toString();}
    public String getNmt()         {return GT090_Nmt.toString();}
    public String getMma()         {return GT090_Mma.toString();}
    public String getVpr()         {return GT090_Vpr.toString();}
    public String getCfu()         {return GT090_Cfu.toString();}
    public String getCoc()         {return GT090_Coc.toString();}
    public String getCms()         {return GT090_Cms.toString();}
    public String getPlm()         {return GT090_Plm.toString();}
  }
  //---------------------------------------------------------------------------------------
  public static class Buf_MsgGT090
  {
    public StringBuffer GT090_Idr = new StringBuffer(1);
    public StringBuffer GT090_Sis = new StringBuffer(3);
    public StringBuffer GT090_Nro = new StringBuffer(3);
    public StringBuffer GT090_Ggr = new StringBuffer(3);
    public Vector       GT090_Tab = new Vector();

    public String getIdr()         {return GT090_Idr.toString();}
    public String getSis()         {return GT090_Sis.toString();}
    public String getNro()         {return GT090_Nro.toString();}
    public String getGgr()         {return GT090_Ggr.toString();}
    public Vector getTab()         {return GT090_Tab;}
  }
  //---------------------------------------------------------------------------------------
  public static Buf_MsgGT090 Inicia_MsgGT090()
  {
    Buf_MsgGT090 l_MsgGT090 = new Buf_MsgGT090();
    l_MsgGT090.GT090_Idr.replace(0, l_MsgGT090.GT090_Idr.capacity(), RUTGEN.Blancos(1));
    l_MsgGT090.GT090_Sis.replace(0, l_MsgGT090.GT090_Sis.capacity(), RUTGEN.Blancos(3));
    l_MsgGT090.GT090_Nro.replace(0, l_MsgGT090.GT090_Nro.capacity(), RUTGEN.Blancos(3));
    l_MsgGT090.GT090_Ggr.replace(0, l_MsgGT090.GT090_Ggr.capacity(), RUTGEN.Blancos(3));
    int p = 0;
    for (int i=0; i<g_Max_GT090; i++)
        {
          Bff_MsgGT090 Tap = new Bff_MsgGT090();
          Tap.GT090_Dcn.replace(0, Tap.GT090_Dcn.capacity(), RUTGEN.Blancos(5 ));
          Tap.GT090_Dsc.replace(0, Tap.GT090_Dsc.capacity(), RUTGEN.Blancos(30));
          Tap.GT090_Tmn.replace(0, Tap.GT090_Tmn.capacity(), RUTGEN.Blancos(3 ));
          Tap.GT090_Trj.replace(0, Tap.GT090_Trj.capacity(), RUTGEN.Blancos(3 ));
          Tap.GT090_Dcm.replace(0, Tap.GT090_Dcm.capacity(), RUTGEN.Blancos(1 ));
          Tap.GT090_Prp.replace(0, Tap.GT090_Prp.capacity(), RUTGEN.Blancos(5 ));
          Tap.GT090_Ggr.replace(0, Tap.GT090_Ggr.capacity(), RUTGEN.Blancos(3 ));
          Tap.GT090_Sgr.replace(0, Tap.GT090_Sgr.capacity(), RUTGEN.Blancos(2 ));
          Tap.GT090_Tob.replace(0, Tap.GT090_Tob.capacity(), RUTGEN.Blancos(2 ));
          Tap.GT090_Nmt.replace(0, Tap.GT090_Nmt.capacity(), RUTGEN.Blancos(3 ));
          Tap.GT090_Mma.replace(0, Tap.GT090_Mma.capacity(), RUTGEN.Blancos(7 ));
          Tap.GT090_Vpr.replace(0, Tap.GT090_Vpr.capacity(), RUTGEN.Blancos(5 ));
          Tap.GT090_Cfu.replace(0, Tap.GT090_Cfu.capacity(), RUTGEN.Blancos(11));
          Tap.GT090_Coc.replace(0, Tap.GT090_Coc.capacity(), RUTGEN.Blancos(5 ));
          Tap.GT090_Cms.replace(0, Tap.GT090_Cms.capacity(), RUTGEN.Blancos(5 ));
          Tap.GT090_Plm.replace(0, Tap.GT090_Plm.capacity(), RUTGEN.Blancos(2 ));
          l_MsgGT090.GT090_Tab.add(Tap);
        }
    return l_MsgGT090;
  }
  //---------------------------------------------------------------------------------------
  static public Buf_MsgGT090 LSet_A_MsgGT090(String pcStr)
  {
    Buf_MsgGT090 l_MsgGT090 = new Buf_MsgGT090();
    MSGGT090 vMsgGT090 = new MSGGT090();
    Vector vDatos = vMsgGT090.LSet_A_vMsgGT090(pcStr);
    l_MsgGT090 = (MSGGT090.Buf_MsgGT090)vDatos.elementAt(0);
    return l_MsgGT090;
  }
  //---------------------------------------------------------------------------------------
  public Vector LSet_A_vMsgGT090(String pcStr)
  {
    Vector vec = new Vector();
    int p = 0;
    Buf_MsgGT090 l_MsgGT090 = new Buf_MsgGT090();
    l_MsgGT090.GT090_Idr.append(pcStr.substring(p, p + l_MsgGT090.GT090_Idr.capacity())); p = p + l_MsgGT090.GT090_Idr.capacity();
    l_MsgGT090.GT090_Sis.append(pcStr.substring(p, p + l_MsgGT090.GT090_Sis.capacity())); p = p + l_MsgGT090.GT090_Sis.capacity();
    l_MsgGT090.GT090_Nro.append(pcStr.substring(p, p + l_MsgGT090.GT090_Nro.capacity())); p = p + l_MsgGT090.GT090_Nro.capacity();
    l_MsgGT090.GT090_Ggr.append(pcStr.substring(p, p + l_MsgGT090.GT090_Ggr.capacity())); p = p + l_MsgGT090.GT090_Ggr.capacity();
    for (int i=0; i<Integer.parseInt(l_MsgGT090.GT090_Nro.toString()); i++)
        {
          Bff_MsgGT090 Tap = new Bff_MsgGT090();
          Tap.GT090_Dcn.append(pcStr.substring(p, p + Tap.GT090_Dcn.capacity())); p = p + Tap.GT090_Dcn.capacity();
          Tap.GT090_Dsc.append(pcStr.substring(p, p + Tap.GT090_Dsc.capacity())); p = p + Tap.GT090_Dsc.capacity();
          Tap.GT090_Tmn.append(pcStr.substring(p, p + Tap.GT090_Tmn.capacity())); p = p + Tap.GT090_Tmn.capacity();
          Tap.GT090_Trj.append(pcStr.substring(p, p + Tap.GT090_Trj.capacity())); p = p + Tap.GT090_Trj.capacity();
          Tap.GT090_Dcm.append(pcStr.substring(p, p + Tap.GT090_Dcm.capacity())); p = p + Tap.GT090_Dcm.capacity();
          Tap.GT090_Prp.append(pcStr.substring(p, p + Tap.GT090_Prp.capacity())); p = p + Tap.GT090_Prp.capacity();
          Tap.GT090_Ggr.append(pcStr.substring(p, p + Tap.GT090_Ggr.capacity())); p = p + Tap.GT090_Ggr.capacity();
          Tap.GT090_Sgr.append(pcStr.substring(p, p + Tap.GT090_Sgr.capacity())); p = p + Tap.GT090_Sgr.capacity();
          Tap.GT090_Tob.append(pcStr.substring(p, p + Tap.GT090_Tob.capacity())); p = p + Tap.GT090_Tob.capacity();
          Tap.GT090_Nmt.append(pcStr.substring(p, p + Tap.GT090_Nmt.capacity())); p = p + Tap.GT090_Nmt.capacity();
          Tap.GT090_Mma.append(pcStr.substring(p, p + Tap.GT090_Mma.capacity())); p = p + Tap.GT090_Mma.capacity();
          Tap.GT090_Vpr.append(pcStr.substring(p, p + Tap.GT090_Vpr.capacity())); p = p + Tap.GT090_Vpr.capacity();
          Tap.GT090_Cfu.append(pcStr.substring(p, p + Tap.GT090_Cfu.capacity())); p = p + Tap.GT090_Cfu.capacity();
          Tap.GT090_Coc.append(pcStr.substring(p, p + Tap.GT090_Coc.capacity())); p = p + Tap.GT090_Coc.capacity();
          Tap.GT090_Cms.append(pcStr.substring(p, p + Tap.GT090_Cms.capacity())); p = p + Tap.GT090_Cms.capacity();
          Tap.GT090_Plm.append(pcStr.substring(p, p + Tap.GT090_Plm.capacity())); p = p + Tap.GT090_Plm.capacity();
          l_MsgGT090.GT090_Tab.add(Tap);
        }
    vec.add (l_MsgGT090);
    return vec;
  }
  //---------------------------------------------------------------------------------------
  static public String LSet_De_MsgGT090(Buf_MsgGT090 p_MsgGT090)
  {
    String pcStr = "";
    pcStr = pcStr + p_MsgGT090.GT090_Idr.toString();
    pcStr = pcStr + p_MsgGT090.GT090_Sis.toString();
    pcStr = pcStr + p_MsgGT090.GT090_Nro.toString();
    pcStr = pcStr + p_MsgGT090.GT090_Ggr.toString();
    for (int i=0; i<p_MsgGT090.GT090_Tab.size(); i++)
        {
          pcStr = pcStr + ((Bff_MsgGT090)p_MsgGT090.GT090_Tab.elementAt(i)).GT090_Dcn.toString();
          pcStr = pcStr + ((Bff_MsgGT090)p_MsgGT090.GT090_Tab.elementAt(i)).GT090_Dsc.toString();
          pcStr = pcStr + ((Bff_MsgGT090)p_MsgGT090.GT090_Tab.elementAt(i)).GT090_Tmn.toString();
          pcStr = pcStr + ((Bff_MsgGT090)p_MsgGT090.GT090_Tab.elementAt(i)).GT090_Trj.toString();
          pcStr = pcStr + ((Bff_MsgGT090)p_MsgGT090.GT090_Tab.elementAt(i)).GT090_Dcm.toString();
          pcStr = pcStr + ((Bff_MsgGT090)p_MsgGT090.GT090_Tab.elementAt(i)).GT090_Prp.toString();
          pcStr = pcStr + ((Bff_MsgGT090)p_MsgGT090.GT090_Tab.elementAt(i)).GT090_Ggr.toString();
          pcStr = pcStr + ((Bff_MsgGT090)p_MsgGT090.GT090_Tab.elementAt(i)).GT090_Sgr.toString();
          pcStr = pcStr + ((Bff_MsgGT090)p_MsgGT090.GT090_Tab.elementAt(i)).GT090_Tob.toString();
          pcStr = pcStr + ((Bff_MsgGT090)p_MsgGT090.GT090_Tab.elementAt(i)).GT090_Nmt.toString();
          pcStr = pcStr + ((Bff_MsgGT090)p_MsgGT090.GT090_Tab.elementAt(i)).GT090_Mma.toString();
          pcStr = pcStr + ((Bff_MsgGT090)p_MsgGT090.GT090_Tab.elementAt(i)).GT090_Vpr.toString();
          pcStr = pcStr + ((Bff_MsgGT090)p_MsgGT090.GT090_Tab.elementAt(i)).GT090_Cfu.toString();
          pcStr = pcStr + ((Bff_MsgGT090)p_MsgGT090.GT090_Tab.elementAt(i)).GT090_Coc.toString();
          pcStr = pcStr + ((Bff_MsgGT090)p_MsgGT090.GT090_Tab.elementAt(i)).GT090_Cms.toString();
          pcStr = pcStr + ((Bff_MsgGT090)p_MsgGT090.GT090_Tab.elementAt(i)).GT090_Plm.toString();
        }
    return pcStr;
  }
  //---------------------------------------------------------------------------------------
}