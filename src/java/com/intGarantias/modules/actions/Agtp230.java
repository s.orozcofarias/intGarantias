package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.intGarantias.bean.BoletaBean;
import com.intGarantias.bean.FacturaGtiasBean;
import com.intGarantias.bean.GastosBean;
import com.intGarantias.bean.ValorMonedaBean;
import com.intGarantias.dao.ConsultasBaseDatos;
import com.intGarantias.util.Constantes;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.*;

import javax.servlet.http.HttpServletResponse;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;


public class Agtp230 extends FHTServletAction{

	private List facturasGtiasList = null;
	private List eventosGtiaList = null;

	public Agtp230() {}

	public void doPerform(RunData data, Context context) throws Exception {
		Log.debug("Agtp230.doPerform");
	    setTemplate(data, "Garantias,Agt,Agtp230.vm" );
	    
	    ConsultasBaseDatos consulta = new ConsultasBaseDatos();
	    
	    String opcion = data.getParameters().getString("opcion", "");
		String num_evento = data.getParameters().getString("num_evento", "");
		String num_gtia = data.getParameters().getString("num_gtia", "");
		String paginaRetorno = data.getParameters().getString("paginaRetorno", "");
		
		if("".equals(paginaRetorno)){			
			paginaRetorno = data.getParameters().getString("action");	
		}
		context.put("paginaRetorno", paginaRetorno);
		
		if (opcion.trim().equals("S")){ //BOTON SALIR
			Log.debug("opcion " +opcion);
			setTemplate(data, "garantias,agt,"+paginaRetorno+".vm" );
		} else if(opcion.trim().equals("G")){//LINK EVENTO
			Log.debug("opcion " +opcion);
			Log.debug("num_evento " +num_evento);
			Log.debug("num_gtia " +num_gtia);
			
			String msjSalida = consulta.insertarGastos(data, context);
			
			
			List<BoletaBean> listaBoletas = consulta.consultaFacturaBoleta(data, context);
			
			GastosBean honorarios = consulta.consultaGastoTasador(data, context, Constantes.VCT_NID_HONORARIOS);
			GastosBean peajes = consulta.consultaGastoTasador(data, context, Constantes.VCT_NID_PEAJES);
			GastosBean kilometros = consulta.consultaGastoTasador(data, context, Constantes.VCT_NID_KILOMETROS);
			GastosBean otros = consulta.consultaGastoTasador(data, context, Constantes.VCT_NID_OTROS);
			String fechaTasacion = consulta.getFechaTasacion(num_evento);
			ValorMonedaBean valorUFBean = consulta.valorUF(fechaTasacion);
			  
			context.put("honorarios",honorarios);
			context.put("peajes",peajes);
			context.put("kilometros",kilometros);
			context.put("otros",otros);
			
			String totalMontosUF = sumarMontosUF(otros.getMontoEnUF(),kilometros.getMontoEnUF(),peajes.getMontoEnUF(),honorarios.getMontoEnUF());
			long  totalMontosPesos = sumarMontosPesos(otros.getMonto(),kilometros.getMonto(),peajes.getMonto(),honorarios.getMonto());
				
			
			context.put("fechaTasacion", fechaTasacion);
			context.put("guardoGasto", msjSalida);
			context.put("totalMontosUF", totalMontosUF);
			context.put("totalMontosPesos", totalMontosPesos);
			context.put("listaBoletas", listaBoletas);
			context.put("num_evento", num_evento);
			context.put("num_gtia", num_gtia);
			context.put("valorUF", valorUFBean.getValor());
			
			
			setTemplate(data, "Garantias,Agt,AGTP230.vm" );
			
		}else if(opcion.trim().equals("E")){//LINK EVENTO
			Log.debug("opcion " +opcion);
			Log.debug("num_evento " +num_evento);
			Log.debug("num_gtia " +num_gtia);
			Log.debug("antes eliminar");
			consulta.eliminarFacturaBoleta(data, context);
			Log.debug("despues eliminar");
			List<BoletaBean> listaBoletas = consulta.consultaFacturaBoleta(data, context);
			Log.debug("despues lista");
			GastosBean honorarios = consulta.consultaGastoTasador(data, context, Constantes.VCT_NID_HONORARIOS);
			GastosBean peajes = consulta.consultaGastoTasador(data, context, Constantes.VCT_NID_PEAJES);
			GastosBean kilometros = consulta.consultaGastoTasador(data, context, Constantes.VCT_NID_KILOMETROS);
			GastosBean otros = consulta.consultaGastoTasador(data, context, Constantes.VCT_NID_OTROS);
			String	fechaTasacion = consulta.getFechaTasacion(num_evento);
			ValorMonedaBean valorUFBean = consulta.valorUF(fechaTasacion);  
			
			String totalMontosUF = sumarMontosUF(otros.getMontoEnUF(),kilometros.getMontoEnUF(),peajes.getMontoEnUF(),honorarios.getMontoEnUF());
			long  totalMontosPesos = sumarMontosPesos(otros.getMonto(),kilometros.getMonto(),peajes.getMonto(),honorarios.getMonto());
				
			
			context.put("fechaTasacion", fechaTasacion);
			context.put("totalMontosUF", totalMontosUF);
			context.put("totalMontosPesos", totalMontosPesos);
			
			context.put("honorarios",honorarios);
			context.put("peajes",peajes);
			context.put("kilometros",kilometros);
			context.put("otros",otros);
			
			context.put("listaBoletas", listaBoletas);
			context.put("num_evento", num_evento);
			context.put("num_gtia", num_gtia);
			context.put("valorUF", valorUFBean.getValor());
			
			
			setTemplate(data, "Garantias,Agt,AGTP230.vm" );
			
		}else if(opcion.trim().equals("U")){//subir archivo
			Log.debug("opcion " +opcion);
			Log.debug("num_evento " +num_evento);
			Log.debug("num_gtia " +num_gtia);
			

			String msjSalida = consulta.insertarFacturaBoleta(data, context);
			
			
		    List<BoletaBean> listaBoletas = consulta.consultaFacturaBoleta(data, context);
			GastosBean honorarios = consulta.consultaGastoTasador(data, context, Constantes.VCT_NID_HONORARIOS);
			GastosBean peajes = consulta.consultaGastoTasador(data, context, Constantes.VCT_NID_PEAJES);
			GastosBean kilometros = consulta.consultaGastoTasador(data, context, Constantes.VCT_NID_KILOMETROS);
			GastosBean otros = consulta.consultaGastoTasador(data, context, Constantes.VCT_NID_OTROS);
			String	fechaTasacion = consulta.getFechaTasacion(num_evento);	
			ValorMonedaBean valorUFBean = consulta.valorUF(fechaTasacion);
			
			context.put("honorarios", honorarios);
			context.put("peajes", peajes);
			context.put("kilometros", kilometros);
			context.put("otros", otros);
			  
			String totalMontosUF = sumarMontosUF(otros.getMontoEnUF(),kilometros.getMontoEnUF(),peajes.getMontoEnUF(),honorarios.getMontoEnUF());
			long  totalMontosPesos = sumarMontosPesos(otros.getMonto(),kilometros.getMonto(),peajes.getMonto(),honorarios.getMonto());
			
			
			context.put("fechaTasacion", fechaTasacion);
			context.put("totalMontosUF", totalMontosUF);
			context.put("totalMontosPesos", totalMontosPesos);
			
			context.put("errorArchivo", msjSalida);
			context.put("listaBoletas", listaBoletas);
			context.put("num_evento", num_evento);
			context.put("num_gtia", num_gtia);
			context.put("valorUF", valorUFBean.getValor());
			setTemplate(data, "Garantias,Agt,AGTP230.vm" );
			
		}else if (opcion.trim().equals("D")){ //BOTON DESCARGAR FACTURA
			
			String numFolio = data.getParameters().getString("srcNid", "");
			Log.debug("opcion " +opcion);
			Log.debug("numFolio " +numFolio );
			
			FacturaGtiasBean facturaGtiasBeanResul = new FacturaGtiasBean();
			facturaGtiasBeanResul = consulta.consultaFacturaTasador(Long.parseLong(numFolio));
	        
			//Ejecuta interpretacion del blob
			String tipoFile = facturaGtiasBeanResul.getTipDoc();
			byte[] blob_Facturas = facturaGtiasBeanResul.getBoletaBlob();
			
			String tempDir = System.getProperty("java.io.tmpdir");
 			File archivoTemporal = new File(tempDir + "boleta."+ tipoFile.toLowerCase());
 			archivoTemporal.deleteOnExit();
 			  			
 			InputStream in = new ByteArrayInputStream(blob_Facturas); 
 			FileOutputStream out = new FileOutputStream(archivoTemporal);
 			 
 			Log.debug("OK archivo grabado");
 			 
 			int BUFFER_SIZE = 1024;
 			byte[] buffer = new byte[BUFFER_SIZE];
 			int sizeRead = 0;
 			while ((sizeRead = in.read(buffer)) >= 0) {
 				 out.write(buffer, 0, sizeRead); 
 			}
 			in.close(); 
 			out.close();
 			
 			//Ejecuta descarga archivo a traves de navegador
			HttpServletResponse response = data.getResponse();
			
			InputStream in2 = new  FileInputStream(archivoTemporal); 
			
 			response.setContentType("application/octet-stream");					
            String headerKey = "Content-Disposition";
            String headerValue = "attachment; filename=\""+archivoTemporal.getName()+ "\"";
            response.setHeader(headerKey, headerValue);
            
            OutputStream servletoutputstream = response.getOutputStream();
			int BUFFER_SIZE_2 = 1024;
			byte[] buffer_2 = new byte[BUFFER_SIZE_2];
			int sizeRead_2 = 0;
			while ((sizeRead_2 = in2.read(buffer_2)) >= 0) { //leyendo del host
				servletoutputstream.write(buffer_2, 0, sizeRead_2); //escribiendo para el navegador
			}
			in2.close(); 
			servletoutputstream.close();
			
		}else if(opcion.trim().equals("V")){ //BOTON PAGINA PREVIA
			Log.debug("opcion " +opcion);
			Log.debug("num_gtia " +num_gtia);
			
			List listEventosGtias = consulta.consultaEventosGtia(data);
		 
		    Log.debug("Numero de eventos asociados a la garantia: " + listEventosGtias.size());
		    
		    this.setEventosGtiaList(listEventosGtias);
		    context.put("num_gtia",num_gtia);
			context.put("list_eventos", this.getEventosGtiaList());
			setTemplate(data, "Garantias,Agt,Agtp231.vm" );
		}		
	    		
	}

	private String sumarMontosUF(String monto, String monto2, String monto3,
			String monto4) {
		float suma = 0;
		String stringSuma = "";
		Log.debug("sumarMontosUF INICIO");
		Log.debug("sumarMontosUF"+monto);
		Log.debug("sumarMontosUF"+monto2);
		Log.debug("sumarMontosUF"+monto3);
		Log.debug("sumarMontosUF"+monto4);
		
		suma = Float.parseFloat(monto.replace(",","."))+Float.parseFloat(monto2.replace(",","."))+Float.parseFloat(monto3.replace(",","."))+Float.parseFloat(monto4.replace(",","."));
		
		
		DecimalFormat df = new DecimalFormat("##.####");
		df.setRoundingMode(RoundingMode.UP);			
		Log.debug("valor Suma -> " + df.format(suma));
		stringSuma = df.format(suma);
		
		Log.debug("sumarMontosUF FIN");
		return stringSuma;
	}
	
	private long sumarMontosPesos(String monto, String monto2, String monto3,
			String monto4) {		
		long suma = 0;
		Log.debug("sumarMontosPesos INICIO");
		
		Log.debug("sumarMontosPesos"+monto);
		Log.debug("sumarMontosPesos"+monto2);
		Log.debug("sumarMontosPesos"+monto3);
		Log.debug("sumarMontosPesos"+monto4);
		
		suma = Long.parseLong(monto)+Long.parseLong(monto2)+Long.parseLong(monto3)+Long.parseLong(monto4);
		
		Log.debug("sumarMontosPesos INICIO");
		return suma;
	}
		
	public List getFacturasGtiasList() {
		return facturasGtiasList;
	}

	public void setFacturasGtiasList(List facturasGtiasList) {
		this.facturasGtiasList = facturasGtiasList;
	}

	public List getEventosGtiaList() {
		return eventosGtiaList;
	}

	public void setEventosGtiaList(List eventosGtiaList) {
		this.eventosGtiaList = eventosGtiaList;
	}
	
	
}