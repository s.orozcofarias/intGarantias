package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.intGarantias.bean.BoletaBean;
import com.intGarantias.bean.FacturaGtiasBean;
import com.intGarantias.bean.GastosBean;
import com.intGarantias.bean.ValorMonedaBean;
import com.intGarantias.dao.ConsultasBaseDatos;
import com.intGarantias.util.Constantes;

import java.io.*;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.*;

import javax.servlet.http.HttpServletResponse;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;


public class Agtp231 extends FHTServletAction{

	private List facturasGtiasList = null;
	private List eventosGtiaList = null;

	public Agtp231() {}

	public void doPerform(RunData data, Context context) throws Exception {
		
		Log.debug("Agtp231.doPerform");
	    setTemplate(data, "Garantias,Agt,Agtp231.vm" );
	    
	    ConsultasBaseDatos consulta = new ConsultasBaseDatos();
	    
	    String opcion = data.getParameters().getString("opcion", "");
		String num_evento = data.getParameters().getString("num_evento", "");
		String num_gtia = data.getParameters().getString("num_gtia", "");
		String paginaRetorno = data.getParameters().getString("paginaRetorno", "");
		
		if("".equals(paginaRetorno)){
			Log.debug("data.getParameters().getString(action)"+data.getParameters().getString("action"));
			paginaRetorno = data.getParameters().getString("action");	
		}
		context.put("paginaRetorno", paginaRetorno);
			
		
		if (opcion.trim().equals("A")){ //BOTON ACEPTAR 
			Log.debug("opcion " +opcion);
			Log.debug("num_gtia " +num_gtia);
			
		    List listEventosGtias = consulta.consultaEventosGtia(data);

			if(listEventosGtias.size() == 0){
				 Log.debug("No se encontraron datos");
				 context.put("num_gtia",num_gtia);
				 context.put("msje_pag", "No existen eventos para la garantia ingresada");
				 setTemplate(data, "Garantias,Agt,Agtp231.vm" );
				 return;
			 }
			 
		    Log.debug("Numero de eventos asociados a la garantia: " + listEventosGtias.size());
		    
		    //setea eventos en conexto
		    this.setEventosGtiaList(listEventosGtias);
		    context.put("num_gtia",num_gtia);
			context.put("list_eventos", this.getEventosGtiaList());
			setTemplate(data, "Garantias,Agt,Agtp231.vm" );

		 }else if (opcion.trim().equals("S")){ //BOTON SALIR
			Log.debug("opcion " +opcion);			
					
			setTemplate(data, "garantias,agt,"+paginaRetorno+".vm" );  
			//setTemplate(data, "garantias,agt,AGTP082.vm" );
			
		}else if(opcion.trim().equals("E")){//LINK EVENTO
			Log.debug("opcion " +opcion);
			Log.debug("num_evento " +num_evento);
			Log.debug("num_gtia " +num_gtia);
			
			List<BoletaBean> listaBoletas = consulta.consultaFacturaBoleta(data, context);
			
			GastosBean honorarios = consulta.consultaGastoTasador(data, context, Constantes.VCT_NID_HONORARIOS);
			GastosBean peajes = consulta.consultaGastoTasador(data, context, Constantes.VCT_NID_PEAJES);
			GastosBean kilometros = consulta.consultaGastoTasador(data, context, Constantes.VCT_NID_KILOMETROS);
			GastosBean otros = consulta.consultaGastoTasador(data, context, Constantes.VCT_NID_OTROS);
			String fechaTasacion = consulta.getFechaTasacion(num_evento);
			ValorMonedaBean valorUFBean = consulta.valorUF(fechaTasacion);
			
			String totalMontosUF 	= sumarMontosUF(otros.getMontoEnUF(),kilometros.getMontoEnUF(),peajes.getMontoEnUF(),honorarios.getMontoEnUF());
			long  totalMontosPesos 	= sumarMontosPesos(otros.getMonto(),kilometros.getMonto(),peajes.getMonto(),honorarios.getMonto());
			
			context.put("totalMontosUF", totalMontosUF);
			context.put("totalMontosPesos", totalMontosPesos);
			
			context.put("honorarios",honorarios);
			context.put("peajes",peajes);
			context.put("kilometros",kilometros);
			context.put("otros",otros);
			context.put("fechaTasacion", fechaTasacion);
			  
			context.put("listaBoletas", listaBoletas);
			context.put("num_evento", num_evento);
			context.put("num_gtia", num_gtia);
			context.put("valorUF", valorUFBean.getValor());
			
			setTemplate(data, "Garantias,Agt,AGTP230.vm" );
			
		}else if(opcion.trim().equals("V")){ //BOTON PAGINA PREVIA
			Log.debug("opcion " +opcion);
			Log.debug("num_gtia " +num_gtia);
			
			List listEventosGtias = consulta.consultaEventosGtia(data);
		 
		    Log.debug("Numero de eventos asociados a la garantia: " + listEventosGtias.size());
		    
		    //setea eventos en conexto
		    this.setEventosGtiaList(listEventosGtias);
		    context.put("num_gar",num_gtia);
			context.put("list_eventos", this.getEventosGtiaList());
			setTemplate(data, "Garantias,Agt,Agtp231.vm" );
		}		
	    		
	}

	private String sumarMontosUF(String monto, String monto2, String monto3,
			String monto4) {
		float suma = 0;
		String stringSuma = "";
		Log.debug("sumarMontosUF INICIO");
		Log.debug("sumarMontosUF"+monto);
		Log.debug("sumarMontosUF"+monto2);
		Log.debug("sumarMontosUF"+monto3);
		Log.debug("sumarMontosUF"+monto4);
		
		suma = Float.parseFloat(monto.replace(",","."))+Float.parseFloat(monto2.replace(",","."))+Float.parseFloat(monto3.replace(",","."))+Float.parseFloat(monto4.replace(",","."));
		
		
		DecimalFormat df = new DecimalFormat("##.####");
		df.setRoundingMode(RoundingMode.UP);			
		Log.debug("valor Suma -> " + df.format(suma));
		stringSuma = df.format(suma);
		
		Log.debug("sumarMontosUF FIN");
		return stringSuma;
	}
	
	private long sumarMontosPesos(String monto, String monto2, String monto3,
			String monto4) {		
		long suma = 0;
		Log.debug("sumarMontosPesos INICIO");
		
		Log.debug("sumarMontosPesos"+monto);
		Log.debug("sumarMontosPesos"+monto2);
		Log.debug("sumarMontosPesos"+monto3);
		Log.debug("sumarMontosPesos"+monto4);
		
		suma = Long.parseLong(monto)+Long.parseLong(monto2)+Long.parseLong(monto3)+Long.parseLong(monto4);
		
		Log.debug("sumarMontosPesos INICIO");
		return suma;
	}
	

	public List getFacturasGtiasList() {
		return facturasGtiasList;
	}

	public void setFacturasGtiasList(List facturasGtiasList) {
		this.facturasGtiasList = facturasGtiasList;
	}

	public List getEventosGtiaList() {
		return eventosGtiaList;
	}

	public void setEventosGtiaList(List eventosGtiaList) {
		this.eventosGtiaList = eventosGtiaList;
	}
	
	
}