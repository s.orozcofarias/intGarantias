// Source File Name:   Agtp993.java
// Descripcion     :   Consulta/Seleccion Productos Contables Garantias (GT093) (Todas)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;

import com.intGarantias.modules.global.MSGGT093;
import com.intGarantias.modules.global.PRMGT093;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp993 extends FHTServletAction
{
  //---------------------------------------------------------------------------------------
  public int i = 0;
  public String lcData = "";
  public Vector vDatos;
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  MSGGT093.Buf_MsgGT093 g_MsgGT093 = new MSGGT093.Buf_MsgGT093();
  PRMGT093.Buf_PrmGT093 g_PrmGT093 = new PRMGT093.Buf_PrmGT093();
  //---------------------------------------------------------------------------------------
  public Agtp993()
  {
  }
  //---------------------------------------------------------------------------------------
  public void doPerform(RunData data, Context context)
              throws Exception
  {
  //Log.debug("AGTP993[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp993-DIN", data);
    doAgtp993_Init(data, context);
  }
  //---------------------------------------------------------------------------------------
  public void doCancel(RunData data, Context context)
              throws Exception
  {
  //Log.debug("AGTP993[doCancel.start]", "[CANCELADO]");
    setTemplate(data, "Garantias,Agt,AGTP993.vm");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp993_Init(RunData data, Context context)
              throws Exception
  {
  //Log.debug("AGTP993[doAgtp993_Init.start]", "[" + data.getUser().getUserName() + "]");
//    g_PrmGT093 = PRMGT093.LSet_A_PrmGT093(g_Msg.Msg_Dat.toString());
    g_MsgGT093 = MSGGT093.Inicia_MsgGT093();
    rg.MoverA(g_MsgGT093.GT093_Idr, "I");
    rg.MoverA(g_MsgGT093.GT093_Sis, "GAR");
    rg.MoverA(g_MsgGT093.GT093_Dcn, "HIPRD");
    rg.MoverA(g_MsgGT093.GT093_Itb, "H");
    rg.MoverA(g_Msg.Msg_Dat, MSGGT093.LSet_De_MsgGT093(g_MsgGT093));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GT093");
    g_MsgGT093 = MSGGT093.LSet_A_MsgGT093(g_Msg.Msg_Dat.toString());
    data.getUser().removeTemp("Agtp993_MsgGT093");
    data.getUser().setTemp("Agtp993_MsgGT093", g_MsgGT093);
    setTemplate(data, "Garantias,Agt,AGTP993.vm" );
    return;
  //Log.debug("AGTP993[doAgtp993_Init.end]", "[" + data.getUser().getUserName() + "]");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp993(RunData data, Context context)
              throws Exception
  {
  //Log.debug("AGTP993[doAgtp993.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp993-MAN", data);
    String Opc = data.getParameters().getString("Opc", "");
    if (Opc.trim().equals("C"))
       {
       //Boton Cancelar
         g_PrmGT093 = PRMGT093.Inicia_PrmGT093();
         rg.MoverA(g_Msg.Msg_Dat, PRMGT093.LSet_De_PrmGT093(g_PrmGT093));
         data.getUser().removeTemp("Agtp993_MsgGT093");
         BF_MSG.Return_Data("Agtp993", data, g_Msg);
         return;
       }
    g_MsgGT093 = (MSGGT093.Buf_MsgGT093)data.getUser().getTemp("Agtp993_MsgGT093");
    Vector vTmp = g_MsgGT093.getTab();
    if (Opc.trim().equals("A"))
       {
       //Boton Aceptar
         String Seq = data.getParameters().getString("Seq", "");
         i = Integer.parseInt(Seq);
         MSGGT093.Bff_MsgGT093 d_MsgGT093 = (MSGGT093.Bff_MsgGT093)vTmp.elementAt(i);
         g_PrmGT093 = PRMGT093.Inicia_PrmGT093();
         rg.MoverA(g_PrmGT093.GT093_Dpc, d_MsgGT093.GT093_Dpc);
         rg.MoverA(g_PrmGT093.GT093_Pct, d_MsgGT093.GT093_Pct);
         rg.MoverA(g_PrmGT093.GT093_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMGT093.LSet_De_PrmGT093(g_PrmGT093));
         data.getUser().removeTemp("Agtp993_MsgGT093");
         BF_MSG.Return_Data("Agtp993", data, g_Msg);
         return;
       }
    g_MsgGT093 = MSGGT093.Inicia_MsgGT093();
    if (Opc.trim().equals("P"))
       {
       //Boton Previo
         rg.MoverA(g_MsgGT093.GT093_Idr, "P");
         i = 0;
         MSGGT093.Bff_MsgGT093 d_MsgGT093 = (MSGGT093.Bff_MsgGT093)vTmp.elementAt(i);
         g_MsgGT093.GT093_Tab.set(0, d_MsgGT093);
         rg.MoverA(g_Msg.Msg_Dat, MSGGT093.LSet_De_MsgGT093(g_MsgGT093));
         g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GT093");
         g_MsgGT093 = MSGGT093.LSet_A_MsgGT093(g_Msg.Msg_Dat.toString());
         data.getUser().removeTemp("Agtp993_MsgGT093");
         data.getUser().setTemp("Agtp993_MsgGT093", g_MsgGT093);
         setTemplate(data, "Garantias,Agt,AGTP993.vm" );
         return;
       }
    if (Opc.trim().equals("N"))
       {
       //Boton Siguiente
         rg.MoverA(g_MsgGT093.GT093_Idr, "S");
         i = vTmp.size() - 1;
         MSGGT093.Bff_MsgGT093 d_MsgGT093 = (MSGGT093.Bff_MsgGT093)vTmp.elementAt(i);
         g_MsgGT093.GT093_Tab.set(0, d_MsgGT093);
         rg.MoverA(g_Msg.Msg_Dat, MSGGT093.LSet_De_MsgGT093(g_MsgGT093));
         g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GT093");
         g_MsgGT093 = MSGGT093.LSet_A_MsgGT093(g_Msg.Msg_Dat.toString());
         data.getUser().removeTemp("Agtp993_MsgGT093");
         data.getUser().setTemp("Agtp993_MsgGT093", g_MsgGT093);
         setTemplate(data, "Garantias,Agt,AGTP993.vm" );
         return;
       }
//    if (Opc.trim().equals("B"))
//       {
//       //Boton Buscar
//         String Bsq = data.getParameters().getString("Bsq", "");
//         rg.MoverA(g_MsgGT093.GT093_Idr, "E");
//         MSGGT093.Bff_MsgGT093 d_MsgGT093 = new MSGGT093.Bff_MsgGT093();
//         rg.MoverA(d_MsgGT093.GT093_Prv, Bsq.trim());
//         rg.MoverA(d_MsgGT093.GT093_Npr, "");
//         if (Bsq.length()>6)
//            { rg.MoverA(d_MsgGT093.GT093_Cod, Bsq.substring(6)); }
//         else
//            { rg.MoverA(d_MsgGT093.GT093_Cod, ""); }
//         rg.MoverA(d_MsgGT093.GT093_Dsc, "");
//         g_MsgGT093.GT093_Tab.set(0, d_MsgGT093);
//         rg.MoverA(g_Msg.Msg_Dat, MSGGT093.LSet_De_MsgGT093(g_MsgGT093));
//         g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GT093");
//         g_MsgGT093 = MSGGT093.LSet_A_MsgGT093(g_Msg.Msg_Dat.toString());
//         data.getUser().removeTemp("Agtp993_MsgGT093");
//         data.getUser().setTemp("Agtp993_MsgGT093", g_MsgGT093);
//         setTemplate(data, "Garantias,Agt,AGTP993.vm" );
//         return;
//       }
  //Log.debug("AGTP993[doAgtp993.end]", "[" + data.getUser().getUserName() + "]");
  }
  //---------------------------------------------------------------------------------------
}