// Source File Name:   Agtp850.java
// Descripcion     :   Menu Cursatura Comercial [Fte: SPR] (-)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.BF_PRA;
import com.FHTServlet.modules.global.PRMPC080;
import com.FHTServlet.modules.global.PRMCN080;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp850 extends FHTServletAction
{
  //---------------------------------------------------------------------------------------
  public Vector vDatos;
  public String lcData = "";
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  PRMPC080.PrmPC080 g_PrmPC080     = new PRMPC080.PrmPC080();
  PRMCN080.PrmCN080 g_PrmCN080     = new PRMCN080.PrmCN080();
  //---------------------------------------------------------------------------------------
  public Agtp850()
  {
  }
  //---------------------------------------------------------------------------------------
  public void doPerform(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP850[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    doAgtp850_Init(data, context);
  }
  //---------------------------------------------------------------------------------------
  public void doCancel(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP850[doCancel.start]", "[CANCELADO]");
    setTemplate(data, "Garantias,Agt,AGTP850.vm");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp850_Init(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP850[doAgtp850_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp850-DIN", data);
    g_Msg = BF_MSG.InitIntegra("Agtp850", data, context);
    Nueva_PrmPC080();
    data.getUser().removeTemp("Agtp850_PrmPC080");
    data.getUser().setTemp("Agtp850_PrmPC080", g_PrmPC080);
    BF_MSG.Param_Program(data, g_Msg);
    setTemplate(data, "Garantias,Agt,AGTP850.vm" );
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp850(RunData data, Context context)
              throws Exception
  {
    String lc_MsgTot = (String)data.getUser().getTemp("g_Msg", "");
    g_Msg = BF_MSG.InitIntegra("Agtp850", data, context);
    Nueva_PrmPC080();
    BF_MSG.Param_Program(data, g_Msg);
    String Opc = data.getParameters().getString("Opc", "");
    if (Opc.trim().equals("I"))
       {
       //Iniciar Centralizacion
         doAgtp850_Init(data, context);
       	 return;
       }
    if (Opc.trim().equals("S"))
       {
         //Boton Salir
         BF_MSG.Return_Program(data, g_Msg);
         return;
       }
    if (Opc.equals("1"))
       {
         //Registro y Control de Transacciones
         rg.MoverA(g_PrmPC080.PC080_Cnr.Sis, "GAR");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Link_Program("Agtp850", "Aedp302", data, g_Msg);
         return;
       }
    if (Opc.equals("8"))
       {
         //Contratos y Mandatos en Tramitación
         rg.MoverA(g_PrmPC080.PC080_Cnr.Sis, "MYC");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Link_Program("Agtp850", "Aedp302", data, g_Msg);
         return;
       }
    if (Opc.equals("2"))
       {
         //Consultas Operativas de Garantias
         RUTGEN.MoverA(g_PrmPC080.PC080_Acc, "CONSULTAS");
         RUTGEN.MoverA(g_Msg.Msg_Dat,        PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Link_Program("Agtp850", "Agtp400", data, g_Msg);
         return;
       }
    if (Opc.equals("3"))
       {
         //Consultas Contables de Garantias
         Nueva_PrmCN080();
         BF_MSG.Link_Program("Agtp850", "Acnp420", data, g_Msg);
         return;
       }
    if (Opc.equals("4"))
       {
         //Listado de Transacciones Cursadas
         BF_MSG.Link_Program("Agtp850", "Aedp207", data, g_Msg);
         return;
       }
    if (Opc.equals("5"))
       {
         //Cierre Diario
         BF_MSG.Link_Program("Agtp850", "Appp121", data, g_Msg);
         return;
       }
  }
  //---------------------------------------------------------------------------------------
  public void Nueva_PrmPC080() throws Exception
  {
    g_PrmPC080 = PRMPC080.Inicia_PrmPC080();
    RUTGEN.MoverA(g_PrmPC080.PC080_Fte,     "SPR");
    RUTGEN.MoverA(g_PrmPC080.PC080_Pai,     "CL");
    RUTGEN.MoverA(g_PrmPC080.PC080_Cnr.Sis, "GAR");
    RUTGEN.MoverA(g_PrmPC080.PC080_Slr.Slc, RUTGEN.Zeros(7));
    RUTGEN.MoverA(g_PrmPC080.PC080_Suc,     g_Msg.Msg_Suc);
    RUTGEN.MoverA(g_PrmPC080.PC080_Nsu,     g_Msg.Msg_Nsu);
    RUTGEN.MoverA(g_PrmPC080.PC080_Mnd,     "000");
    RUTGEN.MoverA(g_PrmPC080.PC080_Nmn,     "PESOS CHILE");
    RUTGEN.MoverA(g_PrmPC080.PC080_Eje,     g_Msg.Msg_Cus);
    RUTGEN.MoverA(g_PrmPC080.PC080_Nej,     g_Msg.Msg_Nus);
    RUTGEN.MoverA(g_PrmPC080.PC080_Tej,     g_Msg.Msg_Fus);
    RUTGEN.MoverA(g_PrmPC080.PC080_Idj,     "S");
    if (BF_PRA.getPra("FLAG-CENTRA",1).equals("1"))
       {
         if (!g_PrmPC080.getSuc().equals("001"))
            { RUTGEN.MoverA(g_PrmPC080.PC080_Idj, "N"); }
       }
    RUTGEN.MoverA(g_Msg.Msg_Dat,            PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
  }
  //---------------------------------------------------------------------------------------
  public void Nueva_PrmCN080()
  {
    g_PrmCN080 = PRMCN080.Inicia_PrmCN080();
    RUTGEN.MoverA(g_PrmCN080.CN080_Rtn, "");
    RUTGEN.MoverA(g_PrmCN080.CN080_Fte, "SPR");
    RUTGEN.MoverA(g_PrmCN080.CN080_Acc, "CNSIS");
    RUTGEN.MoverA(g_PrmCN080.CN080_Suc, g_Msg.Msg_Suc);
    RUTGEN.MoverA(g_PrmCN080.CN080_Nsu, g_Msg.Msg_Nsu);
    RUTGEN.MoverA(g_PrmCN080.CN080_Mnd, "");
    RUTGEN.MoverA(g_PrmCN080.CN080_Nmn, "");
    RUTGEN.MoverA(g_PrmCN080.CN080_Sis, "GAR");
    RUTGEN.MoverA(g_Msg.Msg_Dat,        PRMCN080.LSet_De_PrmCN080(g_PrmCN080));
  }
  //---------------------------------------------------------------------------------------
}