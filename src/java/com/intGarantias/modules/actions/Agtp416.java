// Source File Name:   Agtp416.java
// Descripcion     :   Consulta Detalle Acciones (GT416)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.PRMPC080;

import com.intGarantias.modules.global.MSGGT416;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp416 extends FHTServletAction
{
  //---------------------------------------------------------------------------------------
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  PRMPC080.PrmPC080 g_PrmPC080     = new PRMPC080.PrmPC080();
  MSGGT416.Buf_MsgGT416 g_MsgGT416 = new MSGGT416.Buf_MsgGT416();
  //---------------------------------------------------------------------------------------
  public Agtp416()
  {
  }
  //---------------------------------------------------------------------------------------
  public void doPerform(RunData data, Context context)
              throws Exception
  {
  //Log.debug("AGTP416[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp416-DIN", data);
    doAgtp416_Init(data, context);
  }
  //---------------------------------------------------------------------------------------
  public void doCancel(RunData data, Context context)
              throws Exception
  {
  //Log.debug("AGTP416[doCancel.start]", "[CANCELADO]");
    setTemplate(data, "Garantias,Agt,AGTP416.vm");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp416_Init(RunData data, Context context)
              throws Exception
  {
  //Log.debug("AGTP416[doAgtp416_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    g_MsgGT416 = MSGGT416.Inicia_MsgGT416();
    rg.MoverA(g_MsgGT416.GT416_Idr, "I");
    rg.MoverA(g_MsgGT416.GT416_Gti, g_PrmPC080.PC080_Cnr.Sis.toString() + g_PrmPC080.PC080_Cnr.Ncn.toString());
    rg.MoverA(g_MsgGT416.GT416_Seq, g_PrmPC080.PC080_Ntc);
    rg.MoverA(g_Msg.Msg_Dat, MSGGT416.LSet_De_MsgGT416(g_MsgGT416));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GT416");
    g_MsgGT416 = MSGGT416.LSet_A_MsgGT416(g_Msg.Msg_Dat.toString());
    data.getUser().removeTemp("Agtp416_MsgGT416");
    data.getUser().setTemp("Agtp416_MsgGT416", g_MsgGT416);
    data.getUser().removeTemp("Agtp416_PrmPC080");
    data.getUser().setTemp("Agtp416_PrmPC080", g_PrmPC080);					
    setTemplate(data, "Garantias,Agt,AGTP416.vm" );
  //Log.debug("AGTP416[doAgtp416_Init.end]", "[" + data.getUser().getUserName() + "]");
  //---------------------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp416(RunData data, Context context)
              throws Exception
  {
  //Log.debug("AGTP416[doAgtp416.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp416-MAN", data);
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    String Opc = data.getParameters().getString("Opc" ,"");
    if (Opc.trim().equals("S"))
       {
       //Boton Salir
         data.getUser().removeTemp("Agtp416_MsgGT416");
         data.getUser().removeTemp("Agtp416_PrmPC080");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Program(data, g_Msg);
         return;
       }
  }
  //---------------------------------------------------------------------------------------
}