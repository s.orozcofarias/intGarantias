// Source File Name:   Agtp202.java
// Descripcion     :   Eventos en Tramite [Centro Hipotecario] (ED135, GT202)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.PRMPC080;
import com.FHTServlet.modules.global.PRMPP077;

import com.intGlobal.modules.global.MSGED135;
import com.intGlobal.modules.global.PRMED210;

import com.intGarantias.modules.global.MSGGT202;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp202 extends FHTServletAction
{
  //---------------------------------------------------------------------------------------
  public int    i = 0;
  public String lcRetorno = "";
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  PRMPC080.PrmPC080 g_PrmPC080     = new PRMPC080.PrmPC080();
  PRMPP077.Buf_PrmPP077 g_PrmPP077 = new PRMPP077.Buf_PrmPP077();
  MSGED135.Buf_MsgED135 g_MsgED135 = new MSGED135.Buf_MsgED135();
  MSGGT202.Buf_MsgGT202 g_MsgGT202 = new MSGGT202.Buf_MsgGT202();
  PRMED210.Buf_PrmED210 g_PrmED210 = new PRMED210.Buf_PrmED210();
  //---------------------------------------------------------------------------------------
  public Agtp202()
  {
  }
  //---------------------------------------------------------------------------------------
  public void doPerform(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP202[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp202-DIN", data);
    if (g_Msg.Msg_Pgr.toString().equals("RT"))
       { doAgtp202_Continue(data, context); }
    else
       { doAgtp202_Init(data, context); }
  }
  //---------------------------------------------------------------------------------------
  public void doCancel(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP202[doCancel.start]", "[CANCELADO]");
    setTemplate(data, "Garantias,Agt,AGTP202.vm");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp202_Init(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP202[doAgtp202_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    g_PrmPP077 = PRMPP077.Inicia_PrmPP077();
    rg.MoverA(g_PrmPP077.PP077_Swe, "F");
    rg.MoverA(g_PrmPP077.PP077_Sis, "GAR");
    data.getUser().removeTemp("Agtp202_g_PrmPC080");
    data.getUser().setTemp("Agtp202_g_PrmPC080", g_PrmPC080);
    rg.MoverA(g_Msg.Msg_Dat, PRMPP077.LSet_De_PrmPP077(g_PrmPP077));
    BF_MSG.Link_Program("Agtp202", "Appp077", data, g_Msg); 
    //g_MsgGT202 = MSGGT202.Inicia_MsgGT202();
    //rg.MoverA(g_MsgGT202.GT202_Idr, "I");
    //rg.MoverA(g_MsgGT202.GT202_Tpr, "A");
    //Poblar_Panel(data, context, "I");
    //setTemplate(data, "Garantias,Agt,AGTP202.vm" );
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp202(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP202[doAgtp202.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp202-MAN", data);
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    String Opc = data.getParameters().getString("Opc");
    if  (Opc.equals("S"))
        {
          //Salir
          data.getUser().removeTemp("Agtp202_Msg");
          data.getUser().removeTemp("Agtp202_MsgGT202");
          BF_MSG.Return_Program(data, g_Msg);
          return;
        }
    g_MsgGT202 = (MSGGT202.Buf_MsgGT202)data.getUser().getTemp("Agtp202_MsgGT202");
    MSGGT202.Bff_MsgGT202 d_MsgGT202 = new MSGGT202.Bff_MsgGT202();
    if  (Opc.equals("Pasivo"))
        {
          //Activo/Pasivo
          String lcTpr = g_MsgGT202.GT202_Tpr.toString();
          g_MsgGT202 = MSGGT202.Inicia_MsgGT202();
          rg.MoverA(g_MsgGT202.GT202_Idr, "I");
          if (lcTpr.equals("A"))
             { rg.MoverA(g_MsgGT202.GT202_Tpr, "P"); }
          else
             { rg.MoverA(g_MsgGT202.GT202_Tpr, "A"); }
          Poblar_Panel(data, context, "I");
          setTemplate(data, "Garantias,Agt,AGTP202.vm" );
          return;
        }
    if  (Opc.equals("B"))
        {
          //Detalle de Observaciones Pendientes
          String Seq = data.getParameters().getString("Seq", "");
          i = Integer.parseInt(Seq);
          d_MsgGT202 = (MSGGT202.Bff_MsgGT202)g_MsgGT202.GT202_Tab.elementAt(i);
          g_MsgED135 = MSGED135.Inicia_MsgED135();
          rg.MoverA(g_MsgED135.ED135_Idr, "C");
          rg.MoverA(g_MsgED135.ED135_Nev, d_MsgGT202.Tmt_Nev);
          rg.MoverA(g_MsgED135.ED135_Trt, d_MsgGT202.Tmt_Trt);
          rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
          g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
          g_MsgED135 = MSGED135.LSet_A_MsgED135(g_Msg.Msg_Dat.toString());
          g_PrmED210 = PRMED210.Inicia_PrmED210();
          rg.MoverA(g_PrmED210.ED210_Rtn, "RO");
          rg.MoverA(g_PrmED210.ED210_Ldt, g_MsgED135.ED135_Ldv);
          data.getUser().removeTemp("Agtp202_g_PrmPC080");
          data.getUser().setTemp("Agtp202_g_PrmPC080", g_PrmPC080);
          rg.MoverA(g_Msg.Msg_Dat, PRMED210.LSet_De_PrmED210(g_PrmED210));
          BF_MSG.Link_Program("Agtp202", "Aedp210", data, g_Msg);
          return;
        }               
    if  (Opc.equals("A"))
        {
          //Accion
          String lcPrograma = "Agtp222";
          String Seq = data.getParameters().getString("Seq", "");
          i = Integer.parseInt(Seq);
          d_MsgGT202 = (MSGGT202.Bff_MsgGT202)g_MsgGT202.GT202_Tab.elementAt(i);
          if (g_MsgGT202.GT202_Tpr.toString().equals("A"))
             { rg.MoverA(g_PrmPC080.PC080_Rtn, "WR"); }
          else
             { rg.MoverA(g_PrmPC080.PC080_Rtn, "RO"); }
          rg.MoverA(g_PrmPC080.PC080_Cli,     d_MsgGT202.Evt_Cli);
          rg.MoverA(g_PrmPC080.PC080_Ncl,     d_MsgGT202.Evt_Ncl);
          rg.MoverA(g_PrmPC080.PC080_Cnr.Sis, d_MsgGT202.Evt_Sis);
          rg.MoverA(g_PrmPC080.PC080_Cnr.Ncn, d_MsgGT202.Evt_Ncn);
          rg.MoverA(g_PrmPC080.PC080_Dcn,     d_MsgGT202.Evt_Dcn);
          rg.MoverA(g_PrmPC080.PC080_Ndc,     d_MsgGT202.Evt_Dsg);
          rg.MoverA(g_PrmPC080.PC080_Ntr,     d_MsgGT202.Tmt_Nev);
          rg.MoverA(g_PrmPC080.PC080_Ttr,     d_MsgGT202.Evt_Ttr);
          rg.MoverA(g_PrmPC080.PC080_Trt,     d_MsgGT202.Tmt_Trt);
          rg.MoverA(g_PrmPC080.PC080_Mnd,     d_MsgGT202.Evt_Mnd);
          rg.MoverA(g_PrmPC080.PC080_Mnt,     d_MsgGT202.Evt_Vle);
          rg.MoverA(g_PrmPC080.PC080_Trj,     d_MsgGT202.Evt_Trj);
          rg.MoverA(g_PrmPC080.PC080_Dcm,     d_MsgGT202.Evt_Dcm);
          rg.MoverA(g_PrmPC080.PC080_Ggr,     d_MsgGT202.Evt_Ggr);
          rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
          BF_MSG.Link_Program("Agtp202", lcPrograma, data, g_Msg); 
          if (lcRetorno.trim().compareTo("")!=0)
             {
               data.getUser().removeTemp("Agtp202_g_PrmPC080");
               data.getUser().setTemp("Agtp202_g_PrmPC080", g_PrmPC080);
               BF_MSG.MsgInt("Agtp202", data, g_Msg, lcRetorno, "1"); 
      	       return;
             }
       	  return;
        }
    if  (Opc.equals("D"))
        {
          //Devolver
          String Seq = data.getParameters().getString("Seq", "");
          i = Integer.parseInt(Seq);
          d_MsgGT202 = (MSGGT202.Bff_MsgGT202)g_MsgGT202.GT202_Tab.elementAt(i);
          if (d_MsgGT202.Tmt_Est.toString().compareTo("ENCTH")!=0)
             {
               data.getUser().removeTemp("Agtp202_g_PrmPC080");
               data.getUser().setTemp("Agtp202_g_PrmPC080", g_PrmPC080);
               BF_MSG.MsgInt("Agtp202", data, g_Msg, "No Se Puede Devolver a Ejecutivo", "1"); 
      	       return;
             }
          rg.MoverA(g_PrmPC080.PC080_Ntr, d_MsgGT202.Tmt_Nev);
          rg.MoverA(g_PrmPC080.PC080_Trt, d_MsgGT202.Tmt_Trt);
          g_PrmED210 = PRMED210.Inicia_PrmED210();
          rg.MoverA(g_PrmED210.ED210_Rtn, "RW");
          data.getUser().removeTemp("Agtp202_g_PrmPC080");
          data.getUser().setTemp("Agtp202_g_PrmPC080", g_PrmPC080);
          rg.MoverA(g_Msg.Msg_Dat, PRMED210.LSet_De_PrmED210(g_PrmED210));
          BF_MSG.Link_Program("Agtp202", "Aedp210", data, g_Msg); 
          if (lcRetorno.trim().compareTo("")!=0)
             {
               data.getUser().removeTemp("Agtp202_g_PrmPC080");
               data.getUser().setTemp("Agtp202_g_PrmPC080", g_PrmPC080);
               BF_MSG.MsgInt("Agtp202", data, g_Msg, lcRetorno, "1"); 
      	       return;
             }
       	  return;
        }
    if  (Opc.equals("N"))
        {	
          //Siguiente
          Poblar_Panel(data, context, "S");
          setTemplate(data, "Garantias,Agt,AGTP202.vm" );
          return;
        }
    if  (Opc.equals("P"))
        {	
          //Previo
          Poblar_Panel(data, context, "P");
          setTemplate(data, "Garantias,Agt,AGTP202.vm" );
          return;
        }		
  }
  //---------------------------------------------------------------------------------------
  public void Poblar_Panel(RunData data, Context context, String pcIdr)
              throws Exception
  {
    MSGGT202.Bff_MsgGT202 d_MsgGT202 = new MSGGT202.Bff_MsgGT202();
    if (pcIdr.equals("E"))
       {
         g_MsgGT202 = (MSGGT202.Buf_MsgGT202)data.getUser().getTemp("Agtp202_MsgGT202");
         i = 0;
       }
    if (pcIdr.equals("S"))
       { i = Integer.parseInt(g_MsgGT202.GT202_Nro.toString())-1; }
    if (pcIdr.equals("P"))
       { i = 0; }
    if (pcIdr.compareTo("I")!=0)
       {
         String lcTpr = g_MsgGT202.GT202_Tpr.toString();
         String lcScb = g_MsgGT202.GT202_Scb.toString();
         String lcEjb = g_MsgGT202.GT202_Ejb.toString();
         String lcNgb = g_MsgGT202.GT202_Ngb.toString();
         d_MsgGT202 = (MSGGT202.Bff_MsgGT202)g_MsgGT202.GT202_Tab.elementAt(i);
         g_MsgGT202 = MSGGT202.Inicia_MsgGT202();
         rg.MoverA(g_MsgGT202.GT202_Idr, pcIdr);
         rg.MoverA(g_MsgGT202.GT202_Tpr, lcTpr);
         rg.MoverA(g_MsgGT202.GT202_Scb, lcScb);
         rg.MoverA(g_MsgGT202.GT202_Ejb, lcEjb);
         rg.MoverA(g_MsgGT202.GT202_Ngb, lcNgb);
         g_MsgGT202.GT202_Tab.set(0, d_MsgGT202);
       }
    rg.MoverA(g_Msg.Msg_Dat, MSGGT202.LSet_De_MsgGT202(g_MsgGT202));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GT202");
    g_MsgGT202 = MSGGT202.LSet_A_MsgGT202(g_Msg.Msg_Dat.toString());
    data.getUser().removeTemp("Agtp202_MsgGT202");
    data.getUser().setTemp("Agtp202_MsgGT202", g_MsgGT202);
    data.getUser().removeTemp("Agtp202_Msg");
    data.getUser().setTemp("Agtp202_Msg", g_Msg);
  }
//---------------------------------------------------------------------------------------
  public void doAgtp202_Continue(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP202[doAgtp202_Continue.start]", "[" + data.getUser().getUserName() + "]");
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Appp030"))
       {
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp202_g_PrmPC080");
         data.getUser().removeTemp("Agtp202_g_PrmPC080");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP202.vm" );
         return;
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Appp077"))
       {
         g_PrmPP077 = PRMPP077.LSet_A_PrmPP077(g_Msg.Msg_Dat.toString());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp202_g_PrmPC080");
         data.getUser().removeTemp("Agtp202_g_PrmPC080");
         if (g_PrmPP077.PP077_Swe.toString().equals("S"))
            {
              data.getUser().removeTemp("Agtp202_Msg");
              data.getUser().removeTemp("Agtp202_MsgGT202");
              rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
              BF_MSG.Return_Program(data, g_Msg);
              return;
            }
         g_MsgGT202 = MSGGT202.Inicia_MsgGT202();
         rg.MoverA(g_MsgGT202.GT202_Idr, "I");
         rg.MoverA(g_MsgGT202.GT202_Tpr, "A");
         rg.MoverA(g_MsgGT202.GT202_Scb, g_PrmPP077.PP077_Suc);
         rg.MoverA(g_MsgGT202.GT202_Ejb, g_PrmPP077.PP077_Eje);
         rg.MoverA(g_MsgGT202.GT202_Ngb, g_PrmPP077.PP077_Ncn);
         Poblar_Panel(data, context, "I");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP202.vm" );
         return;
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Aedp210"))
       {
         g_PrmED210 = PRMED210.LSet_A_PrmED210(g_Msg.Msg_Dat.toString());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp202_g_PrmPC080");
         data.getUser().removeTemp("Agtp202_g_PrmPC080");
         if (g_PrmED210.ED210_Rtn.toString().equals("OK"))
            {
              g_MsgED135 = MSGED135.Inicia_MsgED135();
              rg.MoverA(g_MsgED135.ED135_Idr, "X");
              rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
              rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
              rg.MoverA(g_MsgED135.ED135_Est, "");
              rg.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
              rg.MoverA(g_MsgED135.ED135_Ldv, g_PrmED210.ED210_Ldt);
              rg.MoverA(g_MsgED135.ED135_Ezt, "DVCTH");
              rg.MoverA(g_MsgED135.ED135_Exi, "");
              rg.MoverA(g_MsgED135.ED135_Exn, rg.Zeros(g_MsgED135.ED135_Exn));
              rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
              g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
              Poblar_Panel(data, context, "E");
            }
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP202.vm" );
         return;
       }
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    if (g_PrmPC080.PC080_Rtn.toString().equals("OK"))
       {
         Poblar_Panel(data, context, "E");
       }
    setTemplate(data, "Garantias,Agt,AGTP202.vm" );
    return;
  }
//---------------------------------------------------------------------------------------
}