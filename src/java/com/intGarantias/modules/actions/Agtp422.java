// Source File Name:   Agtp422.java
// Descripcion     :   Consulta Detalle Hipoteca - Edificacion (GT422)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.BF_LOB;

import com.intGarantias.modules.global.MSGGT422;
import com.intGarantias.modules.global.PRMGT421;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp422 extends FHTServletAction
{
  //---------------------------------------------------------------------------------------
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  BF_LOB lob                       = new BF_LOB();
  MSGGT422.Buf_MsgGT422 g_MsgGT422 = new MSGGT422.Buf_MsgGT422();
  PRMGT421.Buf_PrmGT421 g_PrmGT421 = new PRMGT421.Buf_PrmGT421();
  //---------------------------------------------------------------------------------------
  public Agtp422()
  {
  }
  //---------------------------------------------------------------------------------------
  public void doPerform(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP422[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp422-DIN", data);
    doAgtp422_Init(data, context);
  }
  //---------------------------------------------------------------------------------------
  public void doCancel(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP422[doCancel.start]", "[CANCELADO]");
    setTemplate(data, "Garantias,Agt,AGTP422.vm");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp422_Init(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP422[doAgtp422_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_PrmGT421 = PRMGT421.LSet_A_PrmGT421(g_Msg.Msg_Dat.toString());
    g_MsgGT422 = MSGGT422.Inicia_MsgGT422();
    rg.MoverA(g_MsgGT422.GT422_Idr, "I");
    rg.MoverA(g_MsgGT422.GT422_Gti, g_PrmGT421.GT421_Gti);
    rg.MoverA(g_MsgGT422.GT422_Seq, g_PrmGT421.GT421_Seq);
    rg.MoverA(g_MsgGT422.GT422_Sqd, g_PrmGT421.GT421_Sqd);
    rg.MoverA(g_Msg.Msg_Dat, MSGGT422.LSet_De_MsgGT422(g_MsgGT422));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GT422");
    g_MsgGT422 = MSGGT422.LSet_A_MsgGT422(g_Msg.Msg_Dat.toString());
    data.getUser().removeTemp("Agtp422_MsgGT422");
    data.getUser().setTemp("Agtp422_MsgGT422", g_MsgGT422);
    if (g_MsgGT422.GT422_Obt.toString().trim().equals("")
      ||g_MsgGT422.GT422_Obt.toString().trim().equals("000000000"))
       { data.getUser().setTemp("Agtp422_Obt", ""); }
    else
       { data.getUser().setTemp("Agtp422_Obt", lob.leeCLOB(g_MsgGT422.GT422_Obt.toString())); }
    if (g_MsgGT422.GT422_Ovb.toString().trim().equals("")
      ||g_MsgGT422.GT422_Ovb.toString().trim().equals("000000000"))
       { data.getUser().setTemp("Agtp422_Ovb", ""); }
    else
       { data.getUser().setTemp("Agtp422_Ovb", lob.leeCLOB(g_MsgGT422.GT422_Ovb.toString())); }
    setTemplate(data, "Garantias,Agt,AGTP422.vm" );
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp422(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP422[doAgtp422.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp422-MAN", data);
    String Opc = data.getParameters().getString("Opc" ,"");
    if (Opc.trim().equals("S"))
       {
         //Boton Salir
         data.getUser().removeTemp("Agtp422_MsgGT422");
         BF_MSG.Return_Data("Agtp422", data, g_Msg);
         return;
       }
  }
  //---------------------------------------------------------------------------------------
}