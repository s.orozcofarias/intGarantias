// Source File Name:   Agtp457.java
// Descripcion     :   Consulta Detalle Informe Legal (GT457)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.BF_LOB;
import com.FHTServlet.modules.global.PRMPC080;

//import com.intGarantias.modules.global.MSGGT457;
import com.intGarantias.modules.global.MSGGT406;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp457 extends FHTServletAction
{
  //---------------------------------------------------------------------------------------
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  PRMPC080.PrmPC080 g_PrmPC080     = new PRMPC080.PrmPC080();
  BF_LOB lob                       = new BF_LOB();
//  MSGGT457.Buf_MsgGT457 g_MsgGT457 = new MSGGT457.Buf_MsgGT457();
  MSGGT406.Buf_MsgGT406 g_MsgGT406 = new MSGGT406.Buf_MsgGT406();
  //---------------------------------------------------------------------------------------
  public Agtp457()
  {
  }
  //---------------------------------------------------------------------------------------
  public void doPerform(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP457[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp457-DIN", data);
    doAgtp457_Init(data, context);
  }
  //---------------------------------------------------------------------------------------
  public void doCancel(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP457[doCancel.start]", "[CANCELADO]");
    setTemplate(data, "Garantias,Agt,AGTP457.vm");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp457_Init(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP457[doAgtp457_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
//    g_MsgGT457 = MSGGT457.Inicia_MsgGT457();
//    rg.MoverA(g_MsgGT457.GT457_Idr, "I");
//    rg.MoverA(g_MsgGT457.GT457_Gti, g_PrmGT457.GT457_Gti);
//    rg.MoverA(g_MsgGT457.GT457_Seq, g_PrmGT457.GT457_Seq);
//    rg.MoverA(g_MsgGT457.GT457_Sqd, g_PrmGT457.GT457_Sqd);
//    rg.MoverA(g_Msg.Msg_Dat, MSGGT457.LSet_De_MsgGT457(g_MsgGT457));
//    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GT457");
//    g_MsgGT457 = MSGGT457.LSet_A_MsgGT457(g_Msg.Msg_Dat.toString());
//    data.getUser().removeTemp("Agtp457_MsgGT457");
//    data.getUser().setTemp("Agtp457_MsgGT457", g_MsgGT457);
    g_MsgGT406 = MSGGT406.Inicia_MsgGT406();
    rg.MoverA(g_MsgGT406.GT406_Idr, "I");
    rg.MoverA(g_MsgGT406.GT406_Gti, g_PrmPC080.PC080_Cnr.Sis.toString()+g_PrmPC080.PC080_Cnr.Ncn.toString());
    rg.MoverA(g_MsgGT406.GT406_Iln, g_PrmPC080.PC080_Ntr);
    rg.MoverA(((MSGGT406.Bff_MsgGT406)g_MsgGT406.GT406_Tag.elementAt(1)).GT406_Seq, "000");
    rg.MoverA(g_Msg.Msg_Dat, MSGGT406.LSet_De_MsgGT406(g_MsgGT406));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GT406");
    g_MsgGT406 = MSGGT406.LSet_A_MsgGT406(g_Msg.Msg_Dat.toString());
    data.getUser().removeTemp("Agtp457_MsgGT406");
    data.getUser().setTemp("Agtp457_MsgGT406", g_MsgGT406);
//    if (g_MsgGT457.GT457_Obt.toString().trim().equals(""))
//       { data.getUser().setTemp("Agtp457_Obt", ""); }
//    else
//       { data.getUser().setTemp("Agtp457_Obt", lob.leeCLOB(g_MsgGT457.GT457_Obt.toString())); }
    setTemplate(data, "Garantias,Agt,AGTP457.vm" );
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp457(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP457[doAgtp457.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp457-MAN", data);
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    String Opc = data.getParameters().getString("Opc" ,"");
    if (Opc.trim().equals("S"))
       {
         //Boton Salir
         data.getUser().removeTemp("Agtp457_MsgGT406");
         data.getUser().removeTemp("Agtp457_MsgGT457");
          data.getUser().removeTemp("Agtp456_DetGT456");
          data.getUser().removeTemp("Agtp456_Dsl");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "NK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp457", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("I"))
       {
         //Boton Imagen
         rg.MoverA(g_PrmPC080.PC080_Rtn, "RO");
         rg.MoverA(g_PrmPC080.PC080_Ntc, "000");
         rg.MoverA(g_PrmPC080.PC080_Dtt, "");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         //BF_MSG.Link_Program("Agtp457", "Agtp459", data, g_Msg);
         BF_MSG.Link_Program("Agtp457", "Appp096", data, g_Msg);
         return;
       }
  }
  //---------------------------------------------------------------------------------------
}