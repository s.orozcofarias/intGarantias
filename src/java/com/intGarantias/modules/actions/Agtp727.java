// Source File Name:   Agtp727.java
// Descripcion     :   Ingreso Tasacion Global (-)

package com.intGarantias.modules.actions;

//import com.FhtNucleo.services.configurator.FhtConfigurator;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.BF_LOB;
import com.FHTServlet.modules.global.BF_IMG;
import com.FHTServlet.modules.global.PRMPC080;

import com.intGlobal.modules.global.MSGED090;
import com.intGlobal.modules.global.PRMTG123;
import com.intGlobal.modules.global.PRMTG130;

import com.intGarantias.modules.global.GT_IMG;
import com.intGarantias.modules.global.PRMGT093;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp727 extends FHTServletAction
{
  //---------------------------------------------------------------------------------------
  public int i = 0;
  public int p = 0;
  public String lcData = "";
  public Vector vDatos;
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  BF_LOB lob                       = new BF_LOB();
  PRMPC080.PrmPC080 g_PrmPC080     = new PRMPC080.PrmPC080();
  Vector v_Img_GtHpd               = new Vector();
  BF_IMG img                       = new BF_IMG();
  BF_IMG.Buf_Img g_Img             = new BF_IMG.Buf_Img();
  BF_IMG.Buf_Img_Bse g_Img_Base    = new BF_IMG.Buf_Img_Bse();
  GT_IMG gtimg                     = new GT_IMG();
  GT_IMG.Buf_Img_GtApe g_Img_GtApe = new GT_IMG.Buf_Img_GtApe();
  GT_IMG.Buf_Img_GtHyp g_Img_GtHyp = new GT_IMG.Buf_Img_GtHyp();
  GT_IMG.Buf_Img_GtHpd g_Img_GtHpd = new GT_IMG.Buf_Img_GtHpd();
  MSGED090.Buf_MsgED090 g_MsgED090 = new MSGED090.Buf_MsgED090();
  PRMGT093.Buf_PrmGT093 g_PrmGT093 = new PRMGT093.Buf_PrmGT093();
  PRMTG123.Buf_PrmTG123 g_PrmTG123 = new PRMTG123.Buf_PrmTG123();
  PRMTG130.Buf_PrmTG130 g_PrmTG130 = new PRMTG130.Buf_PrmTG130();
  //---------------------------------------------------------------------------------------
  public Agtp727()
  {
  }
  //---------------------------------------------------------------------------------------
  public void doPerform(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP727[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp727-DIN", data);
    if (g_Msg.Msg_Pgr.toString().equals("RT"))
       { doAgtp727_Continue(data, context); }
    else
       { doAgtp727_Init(data, context); }
  }
  //---------------------------------------------------------------------------------------
  public void doCancel(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP727[doCancel.start]", "[CANCELADO]");
    setTemplate(data, "Garantias,Agt,AGTP727.vm");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp727_Init(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP727[doAgtp727_Init.start]", "[" + data.getUser().getUserName() + "]");
    GT_IMG.Buf_Img_GtHyp l_Img_GtHyp = new GT_IMG.Buf_Img_GtHyp();
    g_PrmPC080  = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp724_ImgGtApe");
    l_Img_GtHyp = (GT_IMG.Buf_Img_GtHyp)data.getUser().getTemp("Agtp724_ImgGtHyp");
    g_Img_GtHyp = gtimg.LSet_A_ImgGtHyp(gtimg.LSet_De_ImgGtHyp(l_Img_GtHyp));
    vDatos      = (Vector)data.getUser().getTemp("Agtp724_vImgGtHpd");
    v_Img_GtHpd.clear();
    for (i=0; i<vDatos.size(); i++)
        {
          lcData = gtimg.LSet_De_ImgGtHpd((GT_IMG.Buf_Img_GtHpd)vDatos.elementAt(i));
          g_Img_GtHpd = gtimg.LSet_A_ImgGtHpd(lcData);
          v_Img_GtHpd.add(g_Img_GtHpd);
        }
    data.getUser().removeTemp("Agtp727_vImgGtHpd");
    data.getUser().setTemp("Agtp727_vImgGtHpd", v_Img_GtHpd);
    data.getUser().removeTemp("Agtp727_ImgGtHyp");
    data.getUser().setTemp("Agtp727_ImgGtHyp", g_Img_GtHyp);
    data.getUser().removeTemp("Agtp727_Obs");
    if (g_Img_GtHyp.GtHyp_Obs.toString().trim().equals("")
      ||g_Img_GtHyp.GtHyp_Obs.toString().trim().equals("000000000"))
       { data.getUser().setTemp("Agtp727_Obs", ""); }
    else
       { data.getUser().setTemp("Agtp727_Obs", lob.leeCLOB(g_Img_GtHyp.GtHyp_Obs.toString())); }
    data.getUser().removeTemp("Agtp727_PrmPC080");
    data.getUser().setTemp("Agtp727_PrmPC080", g_PrmPC080);
    setTemplate(data, "Garantias,Agt,AGTP727.vm" );
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp727(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP727[doAgtp727.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp727-MAN", data);
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    String Opc = data.getParameters().getString("Opc" ,"");
    if (Opc.trim().equals("A"))
       {
         //Boton Agregar
         Actualiza_Datos(data, context);
         g_Img_GtHyp = (GT_IMG.Buf_Img_GtHyp)data.getUser().getTemp("Agtp727_ImgGtHyp");
         v_Img_GtHpd = (Vector)data.getUser().getTemp("Agtp727_vImgGtHpd");
         String Idx = String.valueOf(v_Img_GtHpd.size());
         rg.MoverA(g_Img_GtHyp.GtHyp_Psw, "A");
         rg.MoverA(g_Img_GtHyp.GtHyp_Ibs, Idx);
         rg.MoverA(g_Img_GtHyp.GtHyp_Idx, rg.Suma(Idx,0,"1",0,3,0));
         g_Img_GtHpd = gtimg.LSet_A_ImgGtHpd(rg.Blancos(827)); 
         rg.MoverA(g_Img_GtHpd.GtHpd_Seq, g_Img_GtHyp.GtHyp_Seq);
         rg.MoverA(g_Img_GtHpd.GtHpd_Sqd, rg.Suma(g_Img_GtHyp.GtHyp_Ibs,0,"1",0,3,0));
         String Umt = "D";
//         if (FhtConfigurator.getPai().equals("CL"))
//            { Umt = "U"; }
Umt = "U";
         rg.MoverA(g_Img_GtHpd.GtHpd_Umt, Umt);
         data.getUser().removeTemp("Agtp727_ImgGtHyp");
         data.getUser().setTemp("Agtp727_ImgGtHyp", g_Img_GtHyp);
         data.getUser().removeTemp("Agtp727_ImgGtHpd");
         data.getUser().setTemp("Agtp727_ImgGtHpd", g_Img_GtHpd);
         setTemplate(data, "Garantias,Agt,AGTP727.vm" );
         return;
       }
    if (Opc.trim().equals("E"))
       {
         //Boton Eliminar
         Actualiza_Datos(data, context);
         g_Img_GtHyp = (GT_IMG.Buf_Img_GtHyp)data.getUser().getTemp("Agtp727_ImgGtHyp");
         v_Img_GtHpd = (Vector)data.getUser().getTemp("Agtp727_vImgGtHpd");
         String Seq = data.getParameters().getString("Seq", "");
         i = Integer.parseInt(Seq);
         rg.MoverA(g_Img_GtHyp.GtHyp_Psw, "E");
         rg.MoverA(g_Img_GtHyp.GtHyp_Idx, rg.Suma(Seq,0,"1",0,3,0));
         g_Img_GtHpd = (GT_IMG.Buf_Img_GtHpd)v_Img_GtHpd.elementAt(i);
         data.getUser().removeTemp("Agtp727_ImgGtHyp");
         data.getUser().setTemp("Agtp727_ImgGtHyp", g_Img_GtHyp);
         data.getUser().removeTemp("Agtp727_ImgGtHpd");
         data.getUser().setTemp("Agtp727_ImgGtHpd", g_Img_GtHpd);
         setTemplate(data, "Garantias,Agt,AGTP727.vm" );
         return;
       }
    if (Opc.trim().equals("Y"))
       {
         //Boton Si
         g_Img_GtHpd = (GT_IMG.Buf_Img_GtHpd)data.getUser().getTemp("Agtp727_ImgGtHpd");
         g_Img_GtHyp = (GT_IMG.Buf_Img_GtHyp)data.getUser().getTemp("Agtp727_ImgGtHyp");
         v_Img_GtHpd = (Vector)data.getUser().getTemp("Agtp727_vImgGtHpd");
         rg.MoverA(g_Img_GtHpd.GtHpd_Itp, data.getParameters().getString("GtHpd_Itp", ""));
         if (g_Img_GtHyp.GtHyp_Psw.toString().trim().equals("A"))
            {
              v_Img_GtHpd.add(g_Img_GtHpd);
              rg.MoverA(g_Img_GtHyp.GtHyp_Ibs, rg.Suma(g_Img_GtHyp.GtHyp_Ibs,0,"1",0,3,0));
            }
         else
            {
              if (g_Img_GtHyp.GtHyp_Psw.toString().trim().equals("E"))
                 { 
                   i = Integer.parseInt(g_Img_GtHyp.GtHyp_Idx.toString()) - 1;
                   v_Img_GtHpd.remove(i); 
                   rg.MoverA(g_Img_GtHyp.GtHyp_Ibs, "000");
                   for (p=0; p<v_Img_GtHpd.size(); p++)
                       {
                         rg.MoverA(g_Img_GtHyp.GtHyp_Ibs, rg.Suma(g_Img_GtHyp.GtHyp_Ibs,0,"1",0,3,0));
                         GT_IMG.Buf_Img_GtHpd l_Img_GtHpd = new GT_IMG.Buf_Img_GtHpd();
                         l_Img_GtHpd = (GT_IMG.Buf_Img_GtHpd)v_Img_GtHpd.elementAt(p);
                         rg.MoverA(l_Img_GtHpd.GtHpd_Seq, g_Img_GtHyp.GtHyp_Seq);
                         rg.MoverA(l_Img_GtHpd.GtHpd_Sqd, g_Img_GtHyp.GtHyp_Ibs);
                         v_Img_GtHpd.set(p, l_Img_GtHpd); 
                       }
                 }
            }
         rg.MoverA(g_Img_GtHyp.GtHyp_Psw, " ");
         rg.MoverA(g_Img_GtHyp.GtHyp_Idx, " ");
         data.getUser().removeTemp("Agtp727_ImgGtHpd");
         data.getUser().removeTemp("Agtp727_ImgGtHyp");
         data.getUser().setTemp("Agtp727_ImgGtHyp", g_Img_GtHyp);
         data.getUser().removeTemp("Agtp727_vImgGtHpd");
         data.getUser().setTemp("Agtp727_vImgGtHpd", v_Img_GtHpd);
         setTemplate(data, "Garantias,Agt,AGTP727.vm" );
         return;
       }
    if (Opc.trim().equals("N"))
       {
         //Boton No
         g_Img_GtHyp = (GT_IMG.Buf_Img_GtHyp)data.getUser().getTemp("Agtp727_ImgGtHyp");
         rg.MoverA(g_Img_GtHyp.GtHyp_Psw, " ");
         rg.MoverA(g_Img_GtHyp.GtHyp_Idx, " ");
         data.getUser().removeTemp("Agtp727_ImgGtHpd");
         data.getUser().removeTemp("Agtp727_ImgGtHyp");
         data.getUser().setTemp("Agtp727_ImgGtHyp", g_Img_GtHyp);
         setTemplate(data, "Garantias,Agt,AGTP727.vm" );
         return;
       }
    if (Opc.trim().equals("T"))
       {
         //Boton Detalle
         data.getUser().removeTemp("Agtp727_g_PrmPC080");
         data.getUser().setTemp("Agtp727_g_PrmPC080", g_PrmPC080);
         if (g_PrmPC080.PC080_Rtn.toString().trim().compareTo("RO")!=0)
            { Actualiza_Datos(data, context); }
         g_Img_GtHyp = (GT_IMG.Buf_Img_GtHyp)data.getUser().getTemp("Agtp727_ImgGtHyp");
         String Seq = data.getParameters().getString("Seq", "");
         i = Integer.parseInt(Seq);
         rg.MoverA(g_Img_GtHyp.GtHyp_Idx, rg.Suma(Seq,0,"1",0,3,0));
         data.getUser().removeTemp("Agtp727_ImgGtHyp");
         data.getUser().setTemp("Agtp727_ImgGtHyp", g_Img_GtHyp);
         v_Img_GtHpd = (Vector)data.getUser().getTemp("Agtp727_vImgGtHpd");
         g_Img_GtHpd = (GT_IMG.Buf_Img_GtHpd)v_Img_GtHpd.elementAt(i);
         data.getUser().removeTemp("Agtp727_ImgGtHpd");
         data.getUser().setTemp("Agtp727_ImgGtHpd", g_Img_GtHpd);
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         if (g_Img_GtHpd.GtHpd_Itp.toString().equals("TE"))
            { BF_MSG.Link_Program("Agtp727", "Agtp769", data, g_Msg); }
         if (g_Img_GtHpd.GtHpd_Itp.toString().equals("ED"))
            { BF_MSG.Link_Program("Agtp727", "Agtp763", data, g_Msg); }
         if (g_Img_GtHpd.GtHpd_Itp.toString().equals("OC"))
            { BF_MSG.Link_Program("Agtp727", "Agtp768", data, g_Msg); }
         if (g_Img_GtHpd.GtHpd_Itp.toString().equals("SA"))
            { BF_MSG.Link_Program("Agtp727", "Agtp767", data, g_Msg); }
         return;
       }
    if (Opc.trim().equals("I"))
       {
         //Boton Imagen
         data.getUser().removeTemp("Agtp727_g_PrmPC080");
         data.getUser().setTemp("Agtp727_g_PrmPC080", g_PrmPC080);
         Actualiza_Datos(data, context);
         g_Img_GtHyp = (GT_IMG.Buf_Img_GtHyp)data.getUser().getTemp("Agtp727_ImgGtHyp");
         rg.MoverA(g_PrmPC080.PC080_Ntc, g_Img_GtHyp.GtHyp_Seq);
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         //BF_MSG.Link_Program("Agtp727", "Agtp729", data, g_Msg);
         BF_MSG.Link_Program("Agtp727", "Appp096", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("S"))
       {
         //Boton Salir
         data.getUser().removeTemp("Agtp727_PrmPC080");
         data.getUser().removeTemp("Agtp727_ImgGtHyp");
         data.getUser().removeTemp("Agtp727_vImgGtHpd");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "NK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp727", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("O"))
       {
         //Boton OK (Aceptar)
         int liSwt = 0;
         String lcVtf = rg.Zeros(15);
         String lcVtd = rg.Zeros(15);
         String lcVts = rg.Zeros(15);
         String lcVtb = rg.Zeros(15);
         String lcVlq = rg.Zeros(15);
         String lcVcn = rg.Zeros(15);
         String lcVsg = rg.Zeros(15);
         Actualiza_Datos(data, context);
         if (Validacion(data, context) == false )
            {
              data.getUser().removeTemp("Agtp727_g_PrmPC080");
              data.getUser().setTemp("Agtp727_g_PrmPC080", g_PrmPC080);
              BF_MSG.MsgInt("Agtp727", data, g_Msg, "Ingreso de Datos Erroneo: Existen Detalles con Valores Nulos o en Cero", "1"); 
      	      return;
            }
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp724_ImgGtApe");
         g_Img_GtHyp = (GT_IMG.Buf_Img_GtHyp)data.getUser().getTemp("Agtp727_ImgGtHyp");
         v_Img_GtHpd = (Vector)data.getUser().getTemp("Agtp727_vImgGtHpd");
         for (i=0; i<v_Img_GtHpd.size(); i++)
             {
               lcData = gtimg.LSet_De_ImgGtHpd((GT_IMG.Buf_Img_GtHpd)v_Img_GtHpd.elementAt(i));
               g_Img_GtHpd = gtimg.LSet_A_ImgGtHpd(lcData);
               String Vtf = rg.Divide(g_Img_GtHpd.GtHpd_Vts,2,g_Img_GtApe.GtApe_Vuf,2,15,4,4);
               String Vtd = rg.Divide(g_Img_GtHpd.GtHpd_Vts,2,g_Img_GtApe.GtApe_Vus,2,15,4,2);
               lcVtf = rg.Suma(lcVtf,4,Vtf,4,15,4);
               lcVtd = rg.Suma(lcVtd,4,Vtd,4,15,4);
               lcVts = rg.Suma(lcVts,2,g_Img_GtHpd.GtHpd_Vts,2,15,2);
               lcVtb = rg.Suma(lcVtb,2,g_Img_GtHpd.GtHpd_Vtb,2,15,2);
               lcVlq = rg.Suma(lcVlq,2,g_Img_GtHpd.GtHpd_Vlq,2,15,2);
               lcVcn = rg.Suma(lcVcn,2,g_Img_GtHpd.GtHpd_Vcn,2,15,2);
               lcVsg = rg.Suma(lcVsg,2,g_Img_GtHpd.GtHpd_Vsg,2,15,2);
             }
         rg.MoverA(g_Img_GtHyp.GtHyp_Vtf, lcVtf);
         rg.MoverA(g_Img_GtHyp.GtHyp_Vtd, lcVtd);
         rg.MoverA(g_Img_GtHyp.GtHyp_Vts, lcVts);
         rg.MoverA(g_Img_GtHyp.GtHyp_Vtb, lcVtb);
         rg.MoverA(g_Img_GtHyp.GtHyp_Vlq, lcVlq);
         rg.MoverA(g_Img_GtHyp.GtHyp_Vcn, lcVcn);
         rg.MoverA(g_Img_GtHyp.GtHyp_Vsg, lcVsg);
         data.getUser().removeTemp("Agtp724_vImgGtHpd");
         data.getUser().setTemp("Agtp724_vImgGtHpd", v_Img_GtHpd);
         data.getUser().removeTemp("Agtp724_ImgGtHyp");
         data.getUser().setTemp("Agtp724_ImgGtHyp", g_Img_GtHyp);
         data.getUser().removeTemp("Agtp727_PrmPC080");
         data.getUser().removeTemp("Agtp727_ImgGtHyp");
         data.getUser().removeTemp("Agtp727_vImgGtHpd");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp727", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("1"))
       {
         //Consulta Lugares Disponibles
         Actualiza_Datos(data, context);
         g_PrmTG130 = PRMTG130.Inicia_PrmTG130();
         rg.MoverA(g_PrmTG130.TG130_Cmp, "DMC-LUG");
         rg.MoverA(g_PrmTG130.TG130_Ncm, "Lugares de Domicilios");
         data.getUser().removeTemp("Agtp727_g_PrmPC080");
         data.getUser().setTemp("Agtp727_g_PrmPC080", g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMTG130.LSet_De_PrmTG130(g_PrmTG130));
         BF_MSG.Link_Program("Agtp727", "Atgp130", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("2"))
       {
         //Consulta Sectores Disponibles
         Actualiza_Datos(data, context);
         g_PrmTG130 = PRMTG130.Inicia_PrmTG130();
         rg.MoverA(g_PrmTG130.TG130_Cmp, "DMC-SEC");
         rg.MoverA(g_PrmTG130.TG130_Ncm, "Sectores de Domicilios");
         data.getUser().removeTemp("Agtp727_g_PrmPC080");
         data.getUser().setTemp("Agtp727_g_PrmPC080", g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMTG130.LSet_De_PrmTG130(g_PrmTG130));
         BF_MSG.Link_Program("Agtp727", "Atgp130", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("3"))
       {
         //Consulta Comunas Disponibles
         Actualiza_Datos(data, context);
         g_PrmTG123 = PRMTG123.Inicia_PrmTG123();
         data.getUser().removeTemp("Agtp727_g_PrmPC080");
         data.getUser().setTemp("Agtp727_g_PrmPC080", g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMTG123.LSet_De_PrmTG123(g_PrmTG123));
         BF_MSG.Link_Program("Agtp727", "Atgp124", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("4"))
       {
         //Consulta Especialidades Disponibles
         Actualiza_Datos(data, context);
         g_PrmTG130 = PRMTG130.Inicia_PrmTG130();
         rg.MoverA(g_PrmTG130.TG130_Cmp, "GAR-ITG");
         rg.MoverA(g_PrmTG130.TG130_Ncm, "Especialidades de Garantias");
         data.getUser().removeTemp("Agtp727_g_PrmPC080");
         data.getUser().setTemp("Agtp727_g_PrmPC080", g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMTG130.LSet_De_PrmTG130(g_PrmTG130));
         BF_MSG.Link_Program("Agtp727", "Atgp130", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("5"))
       {
         //Consulta PContables Disponibles
         Actualiza_Datos(data, context);
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp724_ImgGtApe");
         g_Img_Base  = img.LSet_A_ImgBase(g_Img_GtApe.GtApe_Bse.toString());
         g_Img_GtHyp = (GT_IMG.Buf_Img_GtHyp)data.getUser().getTemp("Agtp727_ImgGtHyp");
         g_PrmGT093 = PRMGT093.Inicia_PrmGT093();
         rg.MoverA(g_PrmGT093.GT093_Sis, "GAR");
         rg.MoverA(g_PrmGT093.GT093_Dcn, g_Img_Base.Img_Base_Dcn);
         rg.MoverA(g_PrmGT093.GT093_Itb, g_Img_GtHyp.GtHyp_Itb);
         data.getUser().removeTemp("Agtp727_g_PrmPC080");
         data.getUser().setTemp("Agtp727_g_PrmPC080", g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMGT093.LSet_De_PrmGT093(g_PrmGT093));
         BF_MSG.Link_Program("Agtp727", "Agtp093", data, g_Msg);
         return;
       }
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp727_Continue(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP727[doAgtp727_Continue.start]", "[" + data.getUser().getUserName() + "]");
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Atgp124"))
       {
         g_PrmTG123 = PRMTG123.LSet_A_PrmTG123(g_Msg.Msg_Dat.toString());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp727_g_PrmPC080");
         data.getUser().removeTemp("Agtp727_g_PrmPC080");
         if (g_PrmTG123.TG123_Prv.toString().trim().compareTo("")!=0)
             {
               g_Img_GtHyp = (GT_IMG.Buf_Img_GtHyp)data.getUser().getTemp("Agtp727_ImgGtHyp");
               rg.MoverA(g_Img_GtHyp.GtHyp_Cmn, g_PrmTG123.TG123_Prv.toString() + g_PrmTG123.TG123_Cmn.toString());
               rg.MoverA(g_Img_GtHyp.GtHyp_Ncm, g_PrmTG123.TG123_Ncm);
               rg.MoverA(g_Img_GtHyp.GtHyp_Npv, g_PrmTG123.TG123_Npr);
               data.getUser().removeTemp("Agtp727_ImgGtHyp");
               data.getUser().setTemp("Agtp727_ImgGtHyp", g_Img_GtHyp);
             }
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP727.vm" );
         return;
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Atgp130"))
       {
         g_PrmTG130 = PRMTG130.LSet_A_PrmTG130(g_Msg.Msg_Dat.toString());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp727_g_PrmPC080");
         data.getUser().removeTemp("Agtp727_g_PrmPC080");
         if (g_PrmTG130.TG130_Cmp.toString().trim().equals("DMC-LUG"))
             {
               if (g_PrmTG130.TG130_Cod.toString().trim().compareTo("")!=0)
                  {
                    g_Img_GtHyp = (GT_IMG.Buf_Img_GtHyp)data.getUser().getTemp("Agtp727_ImgGtHyp");
                    rg.MoverA(g_Img_GtHyp.GtHyp_Lug, g_PrmTG130.TG130_Cod);
                    if (g_Img_GtHyp.GtHyp_Lug.toString().trim().equals("<>"))
                       { rg.MoverA(g_Img_GtHyp.GtHyp_Lnm, ""); }
                    data.getUser().removeTemp("Agtp727_ImgGtHyp");
                    data.getUser().setTemp("Agtp727_ImgGtHyp", g_Img_GtHyp);
                  }
             }
         if (g_PrmTG130.TG130_Cmp.toString().trim().equals("DMC-SEC"))
             {
               if (g_PrmTG130.TG130_Cod.toString().trim().compareTo("")!=0)
                  {
                    g_Img_GtHyp = (GT_IMG.Buf_Img_GtHyp)data.getUser().getTemp("Agtp727_ImgGtHyp");
                    rg.MoverA(g_Img_GtHyp.GtHyp_Sec, g_PrmTG130.TG130_Cod);
                    if (g_Img_GtHyp.GtHyp_Sec.toString().trim().equals("<>"))
                       { rg.MoverA(g_Img_GtHyp.GtHyp_Snb, ""); }
                    data.getUser().removeTemp("Agtp727_ImgGtHyp");
                    data.getUser().setTemp("Agtp727_ImgGtHyp", g_Img_GtHyp);
                  }
             }
         if (g_PrmTG130.TG130_Cmp.toString().trim().equals("GAR-ITG"))
             {
               if (g_PrmTG130.TG130_Cod.toString().trim().compareTo("")!=0)
                  {
                    g_Img_GtHyp = (GT_IMG.Buf_Img_GtHyp)data.getUser().getTemp("Agtp727_ImgGtHyp");
                    rg.MoverA(g_Img_GtHyp.GtHyp_Itg, g_PrmTG130.TG130_Cod);
                    data.getUser().removeTemp("Agtp727_ImgGtHyp");
                    data.getUser().setTemp("Agtp727_ImgGtHyp", g_Img_GtHyp);
                  }
             }
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP727.vm" );
         return;
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Agtp093"))
       {
         g_PrmGT093 = PRMGT093.LSet_A_PrmGT093(g_Msg.Msg_Dat.toString());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp727_g_PrmPC080");
         data.getUser().removeTemp("Agtp727_g_PrmPC080");
         if (g_PrmGT093.GT093_Rtn.toString().trim().equals("OK"))
             {
               g_Img_GtHyp = (GT_IMG.Buf_Img_GtHyp)data.getUser().getTemp("Agtp727_ImgGtHyp");
               rg.MoverA(g_Img_GtHyp.GtHyp_Dpc, g_PrmGT093.GT093_Dpc);
               rg.MoverA(g_Img_GtHyp.GtHyp_Pct, rg.FmtValor(g_PrmGT093.GT093_Pct,0,2,5,"+"));
               data.getUser().removeTemp("Agtp727_ImgGtHyp");
               data.getUser().setTemp("Agtp727_ImgGtHyp", g_Img_GtHyp);
             }
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP727.vm" );
         return;
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Agtp763")
     || rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Agtp767")
     || rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Agtp768")
     || rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Agtp769"))
       {
         PRMPC080.PrmPC080 l_PrmPC080 = new PRMPC080.PrmPC080();
         l_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp727_g_PrmPC080");
         data.getUser().removeTemp("Agtp727_g_PrmPC080");
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp724_ImgGtApe");
         g_Img_GtHyp = (GT_IMG.Buf_Img_GtHyp)data.getUser().getTemp("Agtp727_ImgGtHyp");
         g_Img_GtHpd = (GT_IMG.Buf_Img_GtHpd)data.getUser().getTemp("Agtp727_ImgGtHpd");
         v_Img_GtHpd = (Vector)data.getUser().getTemp("Agtp727_vImgGtHpd");
         i = Integer.parseInt(g_Img_GtHyp.GtHyp_Idx.toString()) - 1;
         v_Img_GtHpd.set(i, g_Img_GtHpd); 
         data.getUser().removeTemp("Agtp727_vImgGtHpd");
         data.getUser().setTemp("Agtp727_vImgGtHpd", v_Img_GtHpd);
         rg.MoverA(g_Img_GtHyp.GtHyp_Idx, "");
         if (l_PrmPC080.PC080_Rtn.toString().equals("OK"))
            { Recalcula_GtHyp(data, context); }
         data.getUser().removeTemp("Agtp727_ImgGtHyp");
         data.getUser().setTemp("Agtp727_ImgGtHyp", g_Img_GtHyp);
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP727.vm" );
         return;
       }
    //if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Agtp729"))
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Appp096"))
       {
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp727_g_PrmPC080");
         data.getUser().removeTemp("Agtp727_g_PrmPC080");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP727.vm" );
         return;
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Appp030"))
       {
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp727_g_PrmPC080");
         data.getUser().removeTemp("Agtp727_g_PrmPC080");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP727.vm" );
         return;
       }
  }
  //---------------------------------------------------------------------------------------
  public void Actualiza_Datos(RunData data, Context context)
              throws Exception 
  {
    g_Img_GtHyp = (GT_IMG.Buf_Img_GtHyp)data.getUser().getTemp("Agtp727_ImgGtHyp");
    rg.MoverA(g_Img_GtHyp.GtHyp_Itg, data.getParameters().getString("GtHyp_Itg", ""));
    rg.MoverA(g_Img_GtHyp.GtHyp_Dpc, data.getParameters().getString("GtHyp_Dpc", ""));
    rg.MoverA(g_Img_GtHyp.GtHyp_Dsb, data.getParameters().getString("GtHyp_Dsb", ""));
    rg.MoverA(g_Img_GtHyp.GtHyp_Cll, data.getParameters().getString("GtHyp_Cll", ""));
    rg.MoverA(g_Img_GtHyp.GtHyp_Cnm, data.getParameters().getString("GtHyp_Cnm", ""));
    rg.MoverA(g_Img_GtHyp.GtHyp_Lug, data.getParameters().getString("GtHyp_Lug", ""));
    rg.MoverA(g_Img_GtHyp.GtHyp_Lnm, data.getParameters().getString("GtHyp_Lnm", ""));
    rg.MoverA(g_Img_GtHyp.GtHyp_Sec, data.getParameters().getString("GtHyp_Sec", ""));
    rg.MoverA(g_Img_GtHyp.GtHyp_Snb, data.getParameters().getString("GtHyp_Snb", ""));
    rg.MoverA(g_Img_GtHyp.GtHyp_Cmn, data.getParameters().getString("GtHyp_Cmn", ""));
    rg.MoverA(g_Img_GtHyp.GtHyp_Ncm, data.getParameters().getString("GtHyp_Ncm", ""));
    rg.MoverA(g_Img_GtHyp.GtHyp_Npv, data.getParameters().getString("GtHyp_Npv", ""));
    rg.MoverA(g_Img_GtHyp.GtHyp_Coo, data.getParameters().getString("GtHyp_Coo", ""));
    rg.MoverA(g_Img_GtHyp.GtHyp_Cps, data.getParameters().getString("GtHyp_Cps", ""));
    rg.MoverA(g_Img_GtHyp.GtHyp_Ceb, data.getParameters().getString("GtHyp_Ceb", ""));
    rg.MoverA(g_Img_GtHyp.GtHyp_Cfc, data.getParameters().getString("GtHyp_Cfc", ""));
    rg.MoverA(g_Img_GtHyp.GtHyp_Cee, data.getParameters().getString("GtHyp_Cee", ""));
    lcData = data.getParameters().getString("GtHyp_Obs", "");
    if (!lcData.trim().equals(""))
       { 
         if (g_Img_GtHyp.GtHyp_Obs.toString().trim().equals("")
           ||g_Img_GtHyp.GtHyp_Obs.toString().trim().equals("000000000"))
            { rg.MoverA(g_Img_GtHyp.GtHyp_Obs, Asigna_Folio("FOLIO-CLOBS", data, context)); }
         lob.mantieneCLOB("M", g_Img_GtHyp.GtHyp_Obs.toString(), lcData);
       }
    data.getUser().removeTemp("Agtp727_Obs");
    if (g_Img_GtHyp.GtHyp_Obs.toString().trim().equals("")
      ||g_Img_GtHyp.GtHyp_Obs.toString().trim().equals("000000000"))
       { data.getUser().setTemp("Agtp727_Obs", ""); }
    else
       { data.getUser().setTemp("Agtp727_Obs", lob.leeCLOB(g_Img_GtHyp.GtHyp_Obs.toString())); }
    data.getUser().removeTemp("Agtp727_ImgGtHyp");
    data.getUser().setTemp("Agtp727_ImgGtHyp", g_Img_GtHyp);
  }
  //---------------------------------------------------------------------------------------
  public void Recalcula_GtHyp(RunData data, Context context)
              throws Exception 
  {
    String lcVtf = rg.Zeros(15);
    String lcVtd = rg.Zeros(15);
    String lcVts = rg.Zeros(15);
    String lcVtb = rg.Zeros(15);
    String lcVlq = rg.Zeros(15);
    String lcVcn = rg.Zeros(15);
    String lcVsg = rg.Zeros(15);
    for (i=0; i<v_Img_GtHpd.size(); i++)
        {
          g_Img_GtHpd = (GT_IMG.Buf_Img_GtHpd)v_Img_GtHpd.elementAt(i);
          if (g_Img_GtHpd.GtHpd_Vts.toString().trim().equals(""))
             { rg.MoverA(g_Img_GtHpd.GtHpd_Vts, rg.Zeros(15)); }
          if (g_Img_GtHpd.GtHpd_Vtb.toString().trim().equals(""))
             { rg.MoverA(g_Img_GtHpd.GtHpd_Vtb, rg.Zeros(15)); }
          if (g_Img_GtHpd.GtHpd_Vlq.toString().trim().equals(""))
             { rg.MoverA(g_Img_GtHpd.GtHpd_Vlq, rg.Zeros(15)); }
          if (g_Img_GtHpd.GtHpd_Vcn.toString().trim().equals(""))
             { rg.MoverA(g_Img_GtHpd.GtHpd_Vcn, rg.Zeros(15)); }
          if (g_Img_GtHpd.GtHpd_Vsg.toString().trim().equals(""))
             { rg.MoverA(g_Img_GtHpd.GtHpd_Vsg, rg.Zeros(15)); }
          String Vtf = rg.Divide(g_Img_GtHpd.GtHpd_Vts,2,g_Img_GtApe.GtApe_Vuf,2,15,4,4);
          String Vtd = rg.Divide(g_Img_GtHpd.GtHpd_Vts,2,g_Img_GtApe.GtApe_Vus,2,15,4,2);
          lcVtf = rg.Suma(lcVtf,4,Vtf,4,15,4);
          lcVtd = rg.Suma(lcVtd,4,Vtd,4,15,4);
          lcVts = rg.Suma(lcVts,2,g_Img_GtHpd.GtHpd_Vts,2,15,2);
          lcVtb = rg.Suma(lcVtb,2,g_Img_GtHpd.GtHpd_Vtb,2,15,2);
          lcVcn = rg.Suma(lcVcn,2,g_Img_GtHpd.GtHpd_Vcn,2,15,2);
          lcVlq = rg.Suma(lcVlq,2,g_Img_GtHpd.GtHpd_Vlq,2,15,2);
          lcVsg = rg.Suma(lcVsg,2,g_Img_GtHpd.GtHpd_Vsg,2,15,2);
        }
    rg.MoverA(g_Img_GtHyp.GtHyp_Vtf, lcVtf);
    rg.MoverA(g_Img_GtHyp.GtHyp_Vtd, lcVtd);
    rg.MoverA(g_Img_GtHyp.GtHyp_Vts, lcVts);
    rg.MoverA(g_Img_GtHyp.GtHyp_Vtb, lcVtb);
    rg.MoverA(g_Img_GtHyp.GtHyp_Vlq, lcVlq);
    rg.MoverA(g_Img_GtHyp.GtHyp_Vcn, lcVcn);
    rg.MoverA(g_Img_GtHyp.GtHyp_Vsg, lcVsg);
  }
  //---------------------------------------------------------------------------------------
  public boolean Validacion(RunData data, Context context)
              throws Exception 
  {
    vDatos = (Vector)data.getUser().getTemp("Agtp727_vImgGtHpd");
    for (i=0; i<vDatos.size(); i++)
        {
          lcData = gtimg.LSet_De_ImgGtHpd((GT_IMG.Buf_Img_GtHpd)vDatos.elementAt(i));
          g_Img_GtHpd = gtimg.LSet_A_ImgGtHpd(lcData);
          if (g_Img_GtHpd.GtHpd_Vts.toString().trim().equals("")
           || g_Img_GtHpd.GtHpd_Vts.toString().equals(rg.Zeros(g_Img_GtHpd.GtHpd_Vts)))
             { return false; }
          //if (g_Img_GtHpd.GtHpd_Vtb.toString().trim().equals("")
          // || g_Img_GtHpd.GtHpd_Vtb.toString().equals(rg.Zeros(g_Img_GtHpd.GtHpd_Vtb)))
          //   { return false; }
          if (g_Img_GtHpd.GtHpd_Vlq.toString().trim().equals("")
           || g_Img_GtHpd.GtHpd_Vlq.toString().equals(rg.Zeros(g_Img_GtHpd.GtHpd_Vlq)))
             { return false; }
          if (g_Img_GtHpd.GtHpd_Vcn.toString().trim().equals("")
           || g_Img_GtHpd.GtHpd_Vcn.toString().equals(rg.Zeros(g_Img_GtHpd.GtHpd_Vcn)))
             { return false; }
          //if (g_Img_GtHpd.GtHpd_Vsg.toString().trim().equals("")
          // || g_Img_GtHpd.GtHpd_Vsg.toString().equals(rg.Zeros(g_Img_GtHpd.GtHpd_Vsg)))
          //   { return false; }
        }
    return true;
  }
  //---------------------------------------------------------------------------------------
  public String Asigna_Folio(String pcFolio, RunData data, Context context)
                throws Exception 
  {
    g_MsgED090 = MSGED090.Inicia_MsgED090();
    rg.MoverA(g_MsgED090.ED090_Idr, "F");
    rg.MoverA(g_MsgED090.ED090_Cod, pcFolio);
    rg.MoverA(g_Msg.Msg_Dat, MSGED090.LSet_De_MsgED090(g_MsgED090));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED090");
    g_MsgED090 = MSGED090.LSet_A_MsgED090(g_Msg.Msg_Dat.toString());
    String lcFolio = rg.FmtValor(g_MsgED090.ED090_Fol.toString(),0,0,9,"");
    return lcFolio;
  }
  //---------------------------------------------------------------------------------------
}