// Source File Name:   Agtp626.java
// Descripcion     :   Revalorizacion (ED135, ED500, ED535, GT626)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.BF_IMG;
import com.FHTServlet.modules.global.BF_EVT;
import com.FHTServlet.modules.global.PRMPC080;

import com.intGlobal.modules.global.MSGED100;
import com.intGlobal.modules.global.MSGED135;
import com.intGlobal.modules.global.PRMED210;
import com.intGlobal.modules.global.PRMCL090;
import com.intGlobal.modules.global.PRMTG114;

import com.intGarantias.modules.global.MSGGT626;
import com.intGarantias.modules.global.GT_IMG;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp626 extends FHTServletAction
{
  //---------------------------------------------------------------------------------------
  public int i = 0;
  public String lcData = "";
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  BF_EVT.Buf_Evt g_Evt             = new BF_EVT.Buf_Evt();
  BF_IMG img                       = new BF_IMG();
  BF_IMG.Buf_Img g_Img             = new BF_IMG.Buf_Img();
  BF_IMG.Buf_Img_Bse g_Img_Base    = new BF_IMG.Buf_Img_Bse();
  GT_IMG gtimg                     = new GT_IMG();
  GT_IMG.Buf_Img_GtRvl g_Img_GtRvl = new GT_IMG.Buf_Img_GtRvl();
  GT_IMG.Buf_Img_GtRvd g_Img_GtRvd = new GT_IMG.Buf_Img_GtRvd();
  Vector v_Img_GtRvd               = new Vector();
  PRMPC080.PrmPC080 g_PrmPC080     = new PRMPC080.PrmPC080();
  MSGED100.Buf_MsgED100 g_MsgED100 = new MSGED100.Buf_MsgED100();
  MSGED135.Buf_MsgED135 g_MsgED135 = new MSGED135.Buf_MsgED135();
  PRMED210.Buf_PrmED210 g_PrmED210 = new PRMED210.Buf_PrmED210();
  PRMCL090.Buf_PrmCL090 g_PrmCL090 = new PRMCL090.Buf_PrmCL090();
  MSGGT626.Buf_MsgGT626 g_MsgGT626 = new MSGGT626.Buf_MsgGT626();
  PRMTG114.Buf_PrmTG114 g_PrmTG114 = new PRMTG114.Buf_PrmTG114();
  //---------------------------------------------------------------------------------------
  public Agtp626()
  {
  }
  //---------------------------------------------------------------------------------------
  public void doPerform(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP626[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp626-DIN", data);
    if (g_Msg.Msg_Pgr.toString().equals("RT"))
       { doAgtp626_Continue(data, context); }
    else
       { doAgtp626_Init(data, context); }
  }
  //---------------------------------------------------------------------------------------
  public void doCancel(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP626[doCancel.start]", "[CANCELADO]");
    setTemplate(data, "Garantias,Agt,AGTP626.vm");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp626_Init(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP626[doAgtp626_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (g_PrmPC080.PC080_Cnr.Ncn.toString().trim().equals("")
     && g_PrmPC080.PC080_Cli.toString().trim().equals(""))
       {
         data.getUser().removeTemp("Agtp626_g_PrmPC080");
         data.getUser().setTemp("Agtp626_g_PrmPC080" ,g_PrmPC080);
         g_PrmCL090 = PRMCL090.Inicia_PrmCL090();
         rg.MoverA(g_PrmCL090.CL090_Pcc, "N");
         rg.MoverA(g_Msg.Msg_Dat, PRMCL090.LSet_De_PrmCL090(g_PrmCL090));
         BF_MSG.Link_Program("Agtp626", "Eclp090", data, g_Msg);
         return;
       }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    g_MsgED135 = MSGED135.Inicia_MsgED135();
    if (g_PrmPC080.PC080_Ntr.toString().trim().compareTo("")!=0)
       {
         Recupera_Evt(data, context);
         Carga_de_Host(data, context);
       }
    else
       {
         if (Carga_Inicial(data, context) == 1)
            { return; }
         Init_Evt_GRT(data, context);
       }
    data.getUser().removeTemp("Agtp626_MsgED135");
    data.getUser().setTemp("Agtp626_MsgED135", g_MsgED135);
    data.getUser().removeTemp("Agtp626_Evt");
    data.getUser().setTemp("Agtp626_Evt", g_Evt);
    data.getUser().removeTemp("Agtp626_ImgBase");
    data.getUser().setTemp("Agtp626_ImgBase", g_Img_Base);
    data.getUser().removeTemp("Agtp626_ImgGtRvl");
    data.getUser().setTemp("Agtp626_ImgGtRvl", g_Img_GtRvl);
    data.getUser().removeTemp("Agtp626_vImgGtRvd");
    data.getUser().setTemp("Agtp626_vImgGtRvd", v_Img_GtRvd);
    rg.MoverA(g_PrmPC080.PC080_Cli, g_Evt.Evt_Cli);
    rg.MoverA(g_PrmPC080.PC080_Ncl, g_Evt.Evt_Ncl);
    rg.MoverA(g_PrmPC080.PC080_Dir, g_Evt.Evt_Dir);
    rg.MoverA(g_PrmPC080.PC080_Tfc, g_Evt.Evt_Tfc);
    rg.MoverA(g_PrmPC080.PC080_Dcn, g_Evt.Evt_Dcn);
    rg.MoverA(g_PrmPC080.PC080_Ndc, g_Evt.Evt_Ndc);
    rg.MoverA(g_PrmPC080.PC080_Ntt, g_Evt.Evt_Ntt);
    data.getUser().removeTemp("Agtp626_PrmPC080");
    data.getUser().setTemp("Agtp626_PrmPC080", g_PrmPC080);
    rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
    BF_MSG.Param_Program(data, g_Msg);
    setTemplate(data, "Garantias,Agt,AGTP626.vm" );
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp626(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP626[doAgtp626.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp626-MAN", data);
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    String Opc = data.getParameters().getString("Opc", "");
    if (Opc.trim().equals("S"))
       {
         //Boton Salir
         data.getUser().removeTemp("Agtp626_vImgGtRvd");
         data.getUser().removeTemp("Agtp626_ImgGtRvl");
         data.getUser().removeTemp("Agtp626_ImgBase");
         data.getUser().removeTemp("Agtp626_Evt");
         data.getUser().removeTemp("Agtp626_PrmPC080");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "NK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp626", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("R"))
       {
         //Boton Errores
         g_MsgED135 = (MSGED135.Buf_MsgED135)data.getUser().getTemp("Agtp626_MsgED135");
         g_PrmED210 = PRMED210.Inicia_PrmED210();
         rg.MoverA(g_PrmED210.ED210_Rtn, "RO");
         rg.MoverA(g_PrmED210.ED210_Ldt, g_MsgED135.ED135_Ldv);
         data.getUser().removeTemp("Agtp626_g_PrmPC080");
         data.getUser().setTemp("Agtp626_g_PrmPC080" ,g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMED210.LSet_De_PrmED210(g_PrmED210));
         BF_MSG.Link_Program("Agtp626", "Aedp210", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("A"))
       {
         //Boton Aceptar
         v_Img_GtRvd = (Vector)data.getUser().getTemp("Agtp626_vImgGtRvd");
         g_Img_GtRvl = (GT_IMG.Buf_Img_GtRvl)data.getUser().getTemp("Agtp626_ImgGtRvl");
         g_Img_Base  = img.LSet_A_ImgBase(g_Img_GtRvl.GtRvl_Bse.toString());
         rg.MoverA(g_Img_GtRvl.GtRvl_Fig, g_Msg.Msg_Fch);
         rg.MoverA(g_Img_GtRvl.GtRvl_Psw, " ");
         rg.MoverA(g_Img_GtRvl.GtRvl_Vtt, rg.Zeros(g_Img_GtRvl.GtRvl_Vtt));
         rg.MoverA(g_Img_GtRvl.GtRvl_Vts, rg.Zeros(g_Img_GtRvl.GtRvl_Vts));
         rg.MoverA(g_Img_GtRvl.GtRvl_Vcn, rg.Zeros(g_Img_GtRvl.GtRvl_Vcn));
         boolean swErr = false;
         for (i=0; i<v_Img_GtRvd.size(); i++)
             {
               g_Img_GtRvd = (GT_IMG.Buf_Img_GtRvd)v_Img_GtRvd.elementAt(i);
               if (Long.parseLong(g_Img_GtRvd.GtRvd_Ntt.toString()) == 0
                || Long.parseLong(g_Img_GtRvd.GtRvd_Nts.toString()) == 0)
                  {
                    swErr = true;
                    break;
                  }
               if (g_Img_GtRvd.GtRvd_Idr.toString().equals("S"))
                  {
                    int iDcm = 0;
                    if (g_Img_Base.Img_Base_Tmn.toString().equals("EXT"))
                       { iDcm = 2; }
                    if (g_Img_GtRvd.GtRvd_Itb.toString().equals("IF"))
                       {
                         rg.MoverA(g_Img_GtRvl.GtRvl_Vtt, rg.Suma(g_Img_GtRvl.GtRvl_Vtt,4,g_Img_GtRvd.GtRvd_Vts,2,15,4));
                         rg.MoverA(g_Img_GtRvl.GtRvl_Vts, rg.Suma(g_Img_GtRvl.GtRvl_Vts,2,g_Img_GtRvd.GtRvd_Vts,2,15,2));
                         rg.MoverA(g_Img_GtRvl.GtRvl_Vcn, rg.Suma(g_Img_GtRvl.GtRvl_Vcn,2,g_Img_GtRvd.GtRvd_Vts,2,15,2));
                       }
                    else
                       {
                         String Vts = rg.Multiplica(g_Img_GtRvd.GtRvd_Ntt,4,g_Img_GtRvd.GtRvd_Vue,2,15,2,iDcm);
                         rg.MoverA(g_Img_GtRvl.GtRvl_Vtt, rg.Suma(g_Img_GtRvl.GtRvl_Vtt,4,g_Img_GtRvd.GtRvd_Ntt,4,15,4));
                         rg.MoverA(g_Img_GtRvl.GtRvl_Vts, rg.Suma(g_Img_GtRvl.GtRvl_Vts,2,Vts,2,15,2));
                         rg.MoverA(g_Img_GtRvl.GtRvl_Vcn, rg.Suma(g_Img_GtRvl.GtRvl_Vcn,2,g_Img_GtRvd.GtRvd_Nts,2,15,2));
                       }
                  }
             }
         if (swErr)
            {
              data.getUser().removeTemp("Agtp626_g_PrmPC080");
              data.getUser().setTemp("Agtp626_g_PrmPC080", g_PrmPC080);
              msg.MsgInt("Agtp626", data, g_Msg, "Existen Detalles con Valor Cero", "1"); 
              return;
            }
       //Valida_Host("V", data, context);
         Vector g_Tab_Img = A_Tab_Img(data);
         img.Graba_Img_Host(g_Msg, g_Tab_Img, data, context);
         Cursatura("U", g_Tab_Img, data, context);
         g_MsgED135 = MSGED135.Inicia_MsgED135();
         rg.MoverA(g_MsgED135.ED135_Idr, "M");
         rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
         rg.MoverA(g_MsgED135.ED135_Trt, "UNICO");
         rg.MoverA(g_MsgED135.ED135_Est, "");
         rg.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
         rg.MoverA(g_MsgED135.ED135_Vlc, g_Img_GtRvl.GtRvl_Vcn);
         if (g_Evt.getGgr().equals("CMA"))
            { rg.MoverA(g_MsgED135.ED135_Ezt, "ENSCM"); }
         else
            { rg.MoverA(g_MsgED135.ED135_Ezt, "ENCUR"); }
         rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
         g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
         data.getUser().removeTemp("Agtp626_PrmPC080");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp626", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("M"))
       {
         //Boton Modificar
         String Seq = data.getParameters().getString("Seq", "");
         i = Integer.parseInt(Seq);
         v_Img_GtRvd = (Vector)data.getUser().getTemp("Agtp626_vImgGtRvd");
         g_Img_GtRvl = (GT_IMG.Buf_Img_GtRvl)data.getUser().getTemp("Agtp626_ImgGtRvl");
         g_Img_Base  = img.LSet_A_ImgBase(g_Img_GtRvl.GtRvl_Bse.toString());
         rg.MoverA(g_Img_GtRvl.GtRvl_Idx, rg.Suma(Seq,0,"1",0,3,0));
         g_Img_GtRvd = (GT_IMG.Buf_Img_GtRvd)v_Img_GtRvd.elementAt(i);
         data.getUser().removeTemp("Agtp626_ImgGtRvl");
         data.getUser().setTemp("Agtp626_ImgGtRvl", g_Img_GtRvl);
         data.getUser().removeTemp("Agtp626_ImgGtRvd");
         data.getUser().setTemp("Agtp626_ImgGtRvd", g_Img_GtRvd);
         setTemplate(data, "Garantias,Agt,AGTP626.vm" );
         return;
       }
    if (Opc.trim().equals("Y"))
       {
         //Boton Si
         g_Img_GtRvd = (GT_IMG.Buf_Img_GtRvd)data.getUser().getTemp("Agtp626_ImgGtRvd");
         g_Img_GtRvl = (GT_IMG.Buf_Img_GtRvl)data.getUser().getTemp("Agtp626_ImgGtRvl");
         v_Img_GtRvd = (Vector)data.getUser().getTemp("Agtp626_vImgGtRvd");
         Actualiza_Datos(data, context);
         i = Integer.parseInt(g_Img_GtRvl.GtRvl_Idx.toString()) - 1;
         GT_IMG.Buf_Img_GtRvd l_Img_GtRvd = new GT_IMG.Buf_Img_GtRvd();
         l_Img_GtRvd = (GT_IMG.Buf_Img_GtRvd)v_Img_GtRvd.elementAt(i);
         v_Img_GtRvd.set(i, g_Img_GtRvd); 
         rg.MoverA(g_Img_GtRvl.GtRvl_Idx, " ");
         rg.MoverA(g_Img_GtRvl.GtRvl_Psw, "Y");
         data.getUser().removeTemp("Agtp626_ImgGtRvl");
         data.getUser().setTemp("Agtp626_ImgGtRvl", g_Img_GtRvl);
         data.getUser().removeTemp("Agtp626_ImgGtRvd");
         data.getUser().removeTemp("Agtp626_vImgGtRvd");
         data.getUser().setTemp("Agtp626_vImgGtRvd", v_Img_GtRvd);
         setTemplate(data, "Garantias,Agt,AGTP626.vm" );
         return;
       }
    if (Opc.trim().equals("N"))
       {
         //Boton No
         data.getUser().removeTemp("Agtp626_ImgGtRvd");
         g_Img_GtRvl = (GT_IMG.Buf_Img_GtRvl)data.getUser().getTemp("Agtp626_ImgGtRvl");
         rg.MoverA(g_Img_GtRvl.GtRvl_Idx, " ");
         data.getUser().removeTemp("Agtp626_ImgGtRvl");
         data.getUser().setTemp("Agtp626_ImgGtRvl", g_Img_GtRvl);
         setTemplate(data, "Garantias,Agt,AGTP626.vm" );
         return;
       }
  }
  //---------------------------------------------------------------------------------------
  public void Recupera_Evt(RunData data, Context context)
              throws Exception 
  {
    g_MsgED100 = MSGED100.Inicia_MsgED100();
    rg.MoverA(g_MsgED100.ED100_Idr, "Q");
    rg.MoverA(g_MsgED100.ED100_Nev, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_Msg.Msg_Dat, MSGED100.LSet_De_MsgED100(g_MsgED100));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED500");
    g_MsgED100 = MSGED100.LSet_A_MsgED100(g_Msg.Msg_Dat.toString());
    g_Evt = BF_EVT.LSet_A_Evt(g_MsgED100.ED100_Evt.toString());
  }
  //---------------------------------------------------------------------------------------
  public void Carga_de_Host(RunData data, Context context)
              throws Exception 
  {
    String lcSvc = "";
    Vector g_Tab_Img = img.Carga_Img_Host(g_Msg, g_PrmPC080.PC080_Ntr.toString(), data, context);
    De_Tab_Img(g_Tab_Img, data);
    if (g_Tab_Img.size() == 1)
       { int liRet = Validar_Host("E", data, context); }
    rg.MoverA(g_MsgED135.ED135_Idr, "C");
    rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
    if (g_PrmPC080.PC080_Trt.toString().trim().equals(""))
       {
         rg.MoverA(g_MsgED135.ED135_Trt, "UNICO");
         lcSvc = "ED535";
       }
    else
       {
         rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
         lcSvc = "ED135";
       }
    rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, lcSvc);
    g_MsgED135 = MSGED135.LSet_A_MsgED135(g_Msg.Msg_Dat.toString());
  }
  //---------------------------------------------------------------------------------------
  public void De_Tab_Img(Vector g_Tab_Img, RunData data)
  {
    g_Img       = img.Inicia_Img();
    lcData      = img.LSet_De_Img(g_Img);
    g_Img_Base  = img.LSet_A_ImgBase(lcData);
    g_Img_GtRvl = gtimg.LSet_A_ImgGtRvl(g_Img.Img_Dat.toString());
    v_Img_GtRvd.clear();
    for (i=0; i<g_Tab_Img.size(); i++)
        {
          lcData = img.LSet_De_Img((BF_IMG.Buf_Img)g_Tab_Img.elementAt(i));
          g_Img  = img.LSet_A_Img(lcData);
          if (g_Img.Img_Dax.toString().trim().equals("BASE"))
             {
               g_Img_GtRvl = gtimg.LSet_A_ImgGtRvl(g_Img.Img_Dat.toString());
               g_Img_Base  = img.LSet_A_ImgBase(g_Img_GtRvl.GtRvl_Bse.toString());
             }
          if (g_Img.Img_Dax.toString().trim().equals("DTRVL"))
             {
               g_Img_GtRvd = gtimg.LSet_A_ImgGtRvd(g_Img.Img_Dat.toString());
               v_Img_GtRvd.add (g_Img_GtRvd); 
             }
        }
  }
  //---------------------------------------------------------------------------------------
  public int Carga_Inicial(RunData data, Context context)
             throws Exception 
  {
    g_Img       = img.Inicia_Img();
    lcData      = img.LSet_De_Img(g_Img);
    g_Img_Base  = img.LSet_A_ImgBase(lcData);
    g_Img_GtRvl = gtimg.LSet_A_ImgGtRvl(g_Img.Img_Dat.toString());
    rg.MoverA(g_Img_Base.Img_Base_Ttr, "REVAL");
    rg.MoverA(g_Img_Base.Img_Base_Sis, g_PrmPC080.PC080_Cnr.Sis);
    rg.MoverA(g_Img_Base.Img_Base_Ncn, g_PrmPC080.PC080_Cnr.Ncn);
    if (Validar_Host("N", data, context) == 1)
       { return 1; }
    rg.MoverA(g_PrmPC080.PC080_Ntr, g_Img_Base.Img_Base_Ntr);
    return 0;
  }
  //---------------------------------------------------------------------------------------
  public int Validar_Host(String pcIdr, RunData data, Context context)
             throws Exception 
  {
    int p = 0;
    String tGtRvd_Seq = "";
    MSGGT626.Bff_MsgGT626 d_MsgGT626 = new MSGGT626.Bff_MsgGT626();
    boolean liSwtFin = false;
    v_Img_GtRvd.clear();
    g_MsgGT626 = MSGGT626.Inicia_MsgGT626();
    while (liSwtFin == false)
       {
         if (Valida_Host(pcIdr, data, context) == 1)
            { return 1; }
         for (i=p; i<MSGGT626.g_Max_GT626; i++)
             {
               d_MsgGT626 = (MSGGT626.Bff_MsgGT626)g_MsgGT626.GT626_Rvl.elementAt(i);
               if (d_MsgGT626.GtRvd_Seq.toString().trim().equals(""))
                  {
                    liSwtFin = true;
                    break;
                  }
               GT_IMG.Buf_Img_GtRvd g_Img_GtRvd = new GT_IMG.Buf_Img_GtRvd();
               rg.MoverA(g_Img_GtRvd.GtRvd_Seq, d_MsgGT626.GtRvd_Seq);
               rg.MoverA(g_Img_GtRvd.GtRvd_Itb, d_MsgGT626.GtRvd_Itb);
               rg.MoverA(g_Img_GtRvd.GtRvd_Ide, d_MsgGT626.GtRvd_Ide);
               rg.MoverA(g_Img_GtRvd.GtRvd_Dsb, d_MsgGT626.GtRvd_Dsb);
               rg.MoverA(g_Img_GtRvd.GtRvd_Umd, d_MsgGT626.GtRvd_Umd);
               rg.MoverA(g_Img_GtRvd.GtRvd_Pum, d_MsgGT626.GtRvd_Pum);
               rg.MoverA(g_Img_GtRvd.GtRvd_Udd, d_MsgGT626.GtRvd_Udd);
               rg.MoverA(g_Img_GtRvd.GtRvd_Vtt, d_MsgGT626.GtRvd_Vtt);
               rg.MoverA(g_Img_GtRvd.GtRvd_Frv, d_MsgGT626.GtRvd_Frv);
               rg.MoverA(g_Img_GtRvd.GtRvd_Vur, d_MsgGT626.GtRvd_Vur);
               rg.MoverA(g_Img_GtRvd.GtRvd_Vts, d_MsgGT626.GtRvd_Vts);
               rg.MoverA(g_Img_GtRvd.GtRvd_Fvt, d_MsgGT626.GtRvd_Fvt);
               rg.MoverA(g_Img_GtRvd.GtRvd_Fre, d_MsgGT626.GtRvd_Fre);
               rg.MoverA(g_Img_GtRvd.GtRvd_Vue, d_MsgGT626.GtRvd_Vue);
               rg.MoverA(g_Img_GtRvd.GtRvd_Nvt, rg.Zeros(g_Img_GtRvd.GtRvd_Nvt));
               rg.MoverA(g_Img_GtRvd.GtRvd_Ntt, rg.Zeros(g_Img_GtRvd.GtRvd_Ntt));
               rg.MoverA(g_Img_GtRvd.GtRvd_Nts, rg.Zeros(g_Img_GtRvd.GtRvd_Nts));
               rg.MoverA(g_Img_GtRvd.GtRvd_Idr, "N");
               rg.MoverA(g_Img_GtRvd.GtRvd_Udn, rg.Zeros(g_Img_GtRvd.GtRvd_Udn));
               rg.MoverA(g_Img_GtRvd.GtRvd_Pct, d_MsgGT626.GtRvd_Pct);
               rg.MoverA(g_Img_GtRvd.GtRvd_Fll, "");
               v_Img_GtRvd.add(g_Img_GtRvd);
               tGtRvd_Seq = g_Img_GtRvd.GtRvd_Seq.toString();
             }
         g_MsgGT626 = MSGGT626.Inicia_MsgGT626();
         rg.MoverA(((MSGGT626.Bff_MsgGT626)g_MsgGT626.GT626_Rvl.elementAt(0)).GtRvd_Seq, tGtRvd_Seq);
         pcIdr = "S";
         tGtRvd_Seq = "";
         p = 1;
       }
    return 0;
  }
  //---------------------------------------------------------------------------------------
  public int Valida_Host(String pcIdr, RunData data, Context context)
             throws Exception 
  {
    if (pcIdr.equals("N"))
       { rg.MoverA(g_Img_GtRvl.GtRvl_Bse, img.LSet_De_ImgBase_Bse(g_Img_Base)); }
    rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtRvl(g_Img_GtRvl));
    rg.MoverA(g_MsgGT626.GT626_Idr, pcIdr);
    rg.MoverA(g_MsgGT626.GT626_Img, g_Img.Img_Dat);
    rg.MoverA(g_Msg.Msg_Dat, MSGGT626.LSet_De_MsgGT626(g_MsgGT626));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GT626");
    g_MsgGT626 = MSGGT626.LSet_A_MsgGT626(g_Msg.Msg_Dat.toString());
    if (g_MsgGT626.GT626_Idr.toString().equals("R"))
       {
         String lcMensaje = "NO SE";
         if (g_MsgGT626.GT626_Swt_Cli.toString().equals("1"))
            { lcMensaje = "Garantia Erronea (Sin CLI-GTI)"; }
         if (g_MsgGT626.GT626_Swt_Cli.toString().equals("2"))
            { lcMensaje = "Garantia Erronea (Sin CLI)"; }
         if (g_MsgGT626.GT626_Swt_Gti.toString().equals("1"))
            { lcMensaje = "Numero Garantia No Existe"; }
         if (g_MsgGT626.GT626_Swt_Gti.toString().equals("2"))
            { lcMensaje = "Garantia NO Esta Vigente"; }
         if (g_MsgGT626.GT626_Swt_Gti.toString().equals("3"))
            { lcMensaje = "Evento No Valido para Garantia"; }
         if (g_MsgGT626.GT626_Swt_Evt.toString().equals("1"))
            { lcMensaje = "Garantia Tiene Evento en Tramite"; }
         msg.MsgInt("Agtp626", data, g_Msg, lcMensaje, "1"); 
         return 1;
       }
  //--------------------------------------------------------------------------------------------
    rg.MoverA(g_Img.Img_Dat, g_MsgGT626.GT626_Img);
    g_Img_GtRvl = gtimg.LSet_A_ImgGtRvl(g_Img.Img_Dat.toString());
    if (pcIdr.equals("N"))
       { 
       	 g_Img_Base  = img.LSet_A_ImgBase(g_Img_GtRvl.GtRvl_Bse.toString()); 
         rg.MoverA(g_Img_Base.Img_Base_Suc, g_Msg.Msg_Suc);
         rg.MoverA(g_Img_Base.Img_Base_Nsu, g_Msg.Msg_Nsu);
       //rg.MoverA(g_Img_Base.Img_Base_Mnd, "000");
       //rg.MoverA(g_Img_Base.Img_Base_Nmn, "PESOS CHILE");
         rg.MoverA(g_Img_Base.Img_Base_Eje, g_Msg.Msg_Cus);
         rg.MoverA(g_Img_Base.Img_Base_Nej, g_Msg.Msg_Nus);
         rg.MoverA(g_Img_Base.Img_Base_Tej, g_Msg.Msg_Fus);
         rg.MoverA(g_Img_GtRvl.GtRvl_Bse,   img.LSet_De_ImgBase_Bse(g_Img_Base));
         rg.MoverA(g_Img_GtRvl.GtRvl_Fpr,   g_Msg.Msg_Fch);
         rg.MoverA(g_Img_GtRvl.GtRvl_Fig,   g_Msg.Msg_Fch);
         rg.MoverA(g_Img_GtRvl.GtRvl_Fts,   g_Msg.Msg_Fch);
         rg.MoverA(g_Img_GtRvl.GtRvl_Vtt,   rg.Zeros(g_Img_GtRvl.GtRvl_Vtt));
         rg.MoverA(g_Img_GtRvl.GtRvl_Vts,   rg.Zeros(g_Img_GtRvl.GtRvl_Vts));
       }
    return 0;
  }
  //---------------------------------------------------------------------------------------
  public void Init_Evt_GRT(RunData data, Context context)
              throws Exception 
  {
    rg.MoverA(g_Evt.Evt_Ttr, g_Img_Base.Img_Base_Ttr);
    rg.MoverA(g_Evt.Evt_Ntt, g_Img_Base.Img_Base_Ntt);
    rg.MoverA(g_Evt.Evt_Tsl, g_Img_Base.Img_Base_Tsl);
    rg.MoverA(g_Evt.Evt_Slc, g_Img_Base.Img_Base_Slc);
    rg.MoverA(g_Evt.Evt_Cli, g_Img_Base.Img_Base_Cli);
    rg.MoverA(g_Evt.Evt_Ncl, g_Img_Base.Img_Base_Ncl);
    rg.MoverA(g_Evt.Evt_Tpr, g_Img_Base.Img_Base_Tpr);
    rg.MoverA(g_Evt.Evt_Dir, g_Img_Base.Img_Base_Dir);
    rg.MoverA(g_Evt.Evt_Cmn, " ");                    //g_Img_Base.Img_Base_Cmn
    rg.MoverA(g_Evt.Evt_Tfc, g_Img_Base.Img_Base_Tfc);
    rg.MoverA(g_Evt.Evt_Sis, g_Img_Base.Img_Base_Sis);
    rg.MoverA(g_Evt.Evt_Ncn, g_Img_Base.Img_Base_Ncn);
    rg.MoverA(g_Evt.Evt_Dcn, g_Img_Base.Img_Base_Dcn);
    rg.MoverA(g_Evt.Evt_Ndc, g_Img_Base.Img_Base_Ndc);
    rg.MoverA(g_Evt.Evt_Tmn, g_Img_Base.Img_Base_Tmn);
    rg.MoverA(g_Evt.Evt_Trj, g_Img_Base.Img_Base_Trj);
    rg.MoverA(g_Evt.Evt_Dcm, g_Img_Base.Img_Base_Dcm);
    rg.MoverA(g_Evt.Evt_Prp, g_Img_Base.Img_Base_Prp);
    rg.MoverA(g_Evt.Evt_Suc, g_Img_Base.Img_Base_Suc);
    rg.MoverA(g_Evt.Evt_Nsu, g_Img_Base.Img_Base_Nsu);
    rg.MoverA(g_Evt.Evt_Mnd, g_Img_Base.Img_Base_Mnd);
    rg.MoverA(g_Evt.Evt_Nmn, g_Img_Base.Img_Base_Nmn);
    rg.MoverA(g_Evt.Evt_Eje, g_Img_Base.Img_Base_Eje);
    rg.MoverA(g_Evt.Evt_Nej, g_Img_Base.Img_Base_Nej);
    rg.MoverA(g_Evt.Evt_Tej, g_Img_Base.Img_Base_Tej);
    rg.MoverA(g_Evt.Evt_Est, "NUEVA");
    rg.MoverA(g_Evt.Evt_Fst, g_Msg.Msg_Fch);
    rg.MoverA(g_Evt.Evt_Upr, g_Img_Base.Img_Base_Trj);
    rg.MoverA(g_Evt.Evt_Vpr, rg.Zeros(g_Evt.Evt_Vpr));
    rg.MoverA(g_Evt.Evt_Fpr, g_Msg.Msg_Fch);
    rg.MoverA(g_Evt.Evt_Spr, rg.Zeros(g_Evt.Evt_Spr));
    rg.MoverA(g_Evt.Evt_Tpp, " ");
    rg.MoverA(g_Evt.Evt_Ggr, g_Img_GtRvl.GtRvl_Ggr);
    rg.MoverA(g_Evt.Evt_Fll, " ");
  }
  //---------------------------------------------------------------------------------------
  public Vector A_Tab_Img(RunData data)
  {
    Vector v_Img = new Vector();
    v_Img.clear();
    rg.MoverA(g_Img.Img_Ntr, g_PrmPC080.PC080_Ntr);
    //--------------------------------------------------------------------------
    rg.MoverA(g_Img.Img_Dax, "BASE");
    rg.MoverA(g_Img.Img_Seq, "000");
    rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtRvl(g_Img_GtRvl));
    Vector vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
    v_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
    //--------------------------------------------------------------------------
    rg.MoverA(g_Img.Img_Seq, "000");
    for (i=0; i<v_Img_GtRvd.size(); i++)
        {
          g_Img_GtRvd = (GT_IMG.Buf_Img_GtRvd)v_Img_GtRvd.elementAt(i);
          rg.MoverA(g_Img.Img_Dax, "DTRVL");
          rg.MoverA(g_Img.Img_Seq, rg.Suma(g_Img.Img_Seq,0,"1",0,3,0));
          rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtRvd(g_Img_GtRvd));
          vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
          v_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
        }
    //--------------------------------------------------------------------------
    return v_Img;
  }
  //---------------------------------------------------------------------------------------
  public void Cursatura(String pcIdr, Vector g_Tab_Img, RunData data, Context context)
              throws Exception 
  {
    g_Evt = (BF_EVT.Buf_Evt)data.getUser().getTemp("Agtp626_Evt");
    rg.MoverA(g_Evt.Evt_Est, "ENTMT");
  //rg.MoverA(g_Evt.Evt_Upr, g_Img_Base.Img_Base_Trj);
  //rg.MoverA(g_Evt.Evt_Vpr, rg.Zeros(15));
    rg.MoverA(g_Evt.Evt_Fpr, g_Msg.Msg_Fch);
  //rg.MoverA(g_Evt.Evt_Spr, rg.Zeros(15));
    BF_EVT t = new BF_EVT();
    g_MsgED100 = MSGED100.Inicia_MsgED100();
    rg.MoverA(g_MsgED100.ED100_Idr, pcIdr);
    rg.MoverA(g_MsgED100.ED100_Nev, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_MsgED100.ED100_Evt, t.LSet_De_Evt(g_Evt));
    rg.MoverA(g_Msg.Msg_Dat, MSGED100.LSet_De_MsgED100(g_MsgED100));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED500");
  }
//---------------------------------------------------------------------------------------
  public void doAgtp626_Continue(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP626[doAgtp626_Continue.start]", "[" + data.getUser().getUserName() + "]");
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Eclp090"))
       {
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp626_g_PrmPC080");
         data.getUser().removeTemp("Agtp626_g_PrmPC080");
         g_PrmCL090 = PRMCL090.LSet_A_PrmCL090(g_Msg.Msg_Dat.toString());
         if (g_PrmCL090.CL090_Cli.toString().trim().equals(""))
            {
              //Limpieza(data, context);
              rg.MoverA(g_PrmPC080.PC080_Rtn, "NK");
              rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
              BF_MSG.Return_Data("Agtp626", data, g_Msg);
              return;
            }
         rg.MoverA(g_PrmPC080.PC080_Cli, g_PrmCL090.CL090_Cli);
         rg.MoverA(g_PrmPC080.PC080_Ncl, g_PrmCL090.CL090_Ncl);
         rg.MoverA(g_PrmPC080.PC080_Tpr, g_PrmCL090.CL090_Tpr);
         rg.MoverA(g_PrmPC080.PC080_Acc, "SELECCIONA");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Link_Program("Agtp626", "Agtp400", data, g_Msg);
         return;
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Agtp400"))
       {
         g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
         if (g_PrmPC080.PC080_Rtn.toString().trim().equals("NK"))
            {
              //Limpieza(data, context);
              rg.MoverA(g_PrmPC080.PC080_Rtn, "NK");
              rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
              BF_MSG.Return_Data("Agtp626", data, g_Msg);
              return;
            }
         rg.MoverA(g_PrmPC080.PC080_Acc, "REVAL");
         rg.MoverA(g_PrmPC080.PC080_Ttr, "REVAL");
         rg.MoverA(g_PrmPC080.PC080_Ntt, "REVALORIZACION DE GARANTIA");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         doAgtp626_Init(data, context);
         return;
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Aedp210"))
       {
         g_PrmED210 = PRMED210.LSet_A_PrmED210(g_Msg.Msg_Dat.toString());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp626_g_PrmPC080");
         data.getUser().removeTemp("Agtp626_g_PrmPC080");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP626.vm" );
         return;
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Appp030"))
       {
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp626_g_PrmPC080");
         if (g_PrmPC080!=null)
            {
              data.getUser().removeTemp("Agtp626_g_PrmPC080");
              rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
              BF_MSG.Param_Program(data, g_Msg);
              setTemplate(data, "Garantias,Agt,AGTP626.vm" );
              return;
            }
         BF_MSG.Return_Program(data, g_Msg);
         return;
       }
  }
  //---------------------------------------------------------------------------------------
  public void Actualiza_Datos(RunData data, Context context)
              throws Exception 
  {
    rg.MoverA(g_Img_GtRvd.GtRvd_Nvt, data.getParameters().getString("GtRvd_Nvt", ""));
    rg.MoverA(g_Img_GtRvd.GtRvd_Udd, data.getParameters().getString("GtRvd_Udd", ""));
    rg.MoverA(g_Img_GtRvd.GtRvd_Pum, data.getParameters().getString("GtRvd_Pum", ""));
    rg.MoverA(g_Img_GtRvd.GtRvd_Ntt, data.getParameters().getString("GtRvd_Ntt", ""));
    rg.MoverA(g_Img_GtRvd.GtRvd_Nts, data.getParameters().getString("GtRvd_Nts", ""));
    rg.MoverA(g_Img_GtRvd.GtRvd_Idr, "S");
  }
  //---------------------------------------------------------------------------------------
}