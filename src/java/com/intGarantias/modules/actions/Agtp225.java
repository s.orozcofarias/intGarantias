// Source File Name:   Agtp225.java
// Descripcion     :   Devolucion a UVT para Modificar Tasacion (ED125, ED135)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.BF_IMG;
import com.FHTServlet.modules.global.PRMPC080;

import com.intGlobal.modules.global.MSGED125;
import com.intGlobal.modules.global.MSGED135;
import com.intGlobal.modules.global.PRMTG134;
import com.intGlobal.modules.global.PRMED210;

import com.intGarantias.modules.global.GT_IMG;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp225 extends FHTServletAction
{
  //===============================================================================================================================
  public int i = 0;
  public String lcData = "";
  public Vector vDatos;
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  BF_IMG img                       = new BF_IMG();
  BF_IMG.Buf_Img g_Img             = new BF_IMG.Buf_Img();
  BF_IMG.Buf_Img_Bse g_Img_Base    = new BF_IMG.Buf_Img_Bse();
  GT_IMG gtimg                     = new GT_IMG();
  GT_IMG.Buf_Img_GtApe g_Img_GtApe = new GT_IMG.Buf_Img_GtApe();
  GT_IMG.Buf_Img_GtTsd g_Img_GtTsd = new GT_IMG.Buf_Img_GtTsd();
  GT_IMG.Buf_Img_GtEsl g_Img_GtEsl = new GT_IMG.Buf_Img_GtEsl();
  PRMPC080.PrmPC080 g_PrmPC080     = new PRMPC080.PrmPC080();
  MSGED125.Buf_MsgED125 g_MsgED125 = new MSGED125.Buf_MsgED125();
  MSGED135.Buf_MsgED135 g_MsgED135 = new MSGED135.Buf_MsgED135();
  PRMTG134.Buf_PrmTG134 g_PrmTG134 = new PRMTG134.Buf_PrmTG134();
  PRMED210.Buf_PrmED210 g_PrmED210 = new PRMED210.Buf_PrmED210();
  //-------------------------------------------------------------------------------------------
  public Agtp225()
  {
  }
  //===============================================================================================================================
  public void doPerform(RunData data, Context context) throws Exception
  {
    //Log.debug("AGTP225[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp225-DIN", data);
    doAgtp225_Init(data, context);
  }
  //-------------------------------------------------------------------------------------------
  public void doAgtp225_Init(RunData data, Context context) throws Exception
  {
    //Log.debug("AGTP225[doAgtp225_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.getDat());
    Carga_de_Host(data, context);
    Carga_Inicial(data, context);
    Carga_Doctos(data, context);
    data.getUser().removeTemp("Agtp225_PrmPC080");
    data.getUser().setTemp("Agtp225_PrmPC080", g_PrmPC080);
    rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
    BF_MSG.Param_Program(data, g_Msg);
    setTemplate(data, "Garantias,Agt,AGTP225.vm");
  }
  //===============================================================================================================================
  public void doAgtp225(RunData data, Context context) throws Exception
  {
    //Log.debug("AGTP225[doAgtp225.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp225-MAN", data);
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.getDat());
    String Opc = data.getParameters().getString("Opc", "");
    if (Opc.trim().equals("S"))
       {
         //Boton Salir
         data.getUser().removeTemp("Agtp225_PrmPC080");
         data.getUser().removeTemp("Agtp225_ImgBase");
         data.getUser().removeTemp("Agtp225_ImgGtApe");
         data.getUser().removeTemp("Agtp225_ImgGtTsd");
         data.getUser().removeTemp("Agtp225_ImgGtEsl");
         data.getUser().removeTemp("Agtp225_vTabImg");
         data.getUser().removeTemp("Agtp225_MsgED125");
         data.getUser().removeTemp("Agtp225_MsgED135");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "NK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp225", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("U"))
       {
         //Boton Aceptar
         if (g_PrmPC080.getRtn().equals("RA"))
             {
               rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
               rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
               BF_MSG.Return_Data("Agtp225", data, g_Msg);
               return;
             }
         Actualiza_Datos(data, context);
         Vector g_Tab_Img = A_Tab_Img(data);
         Cursatura(g_Tab_Img, data, context);
         data.getUser().removeTemp("Agtp225_PrmPC080");
         data.getUser().removeTemp("Agtp225_ImgBase");
         data.getUser().removeTemp("Agtp225_ImgGtApe");
         data.getUser().removeTemp("Agtp225_ImgGtTsd");
         data.getUser().removeTemp("Agtp225_ImgGtEsl");
         data.getUser().removeTemp("Agtp225_vTabImg");
         data.getUser().removeTemp("Agtp225_MsgED125");
         data.getUser().removeTemp("Agtp225_MsgED135");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp225", data, g_Msg);
         return;
       }
  }
  //===============================================================================================================================
  public void Carga_de_Host(RunData data, Context context) throws Exception
  {
    Vector g_Tab_Img = img.Carga_Img_Host(g_Msg, g_PrmPC080.getNtr(), data, context);
    De_Tab_Img(g_Tab_Img, data);
    data.getUser().removeTemp("Agtp225_ImgBase");
    data.getUser().setTemp("Agtp225_ImgBase", g_Img_Base);
    data.getUser().removeTemp("Agtp225_ImgGtApe");
    data.getUser().setTemp("Agtp225_ImgGtApe", g_Img_GtApe);
    data.getUser().removeTemp("Agtp225_ImgGtTsd");
    data.getUser().setTemp("Agtp225_ImgGtTsd", g_Img_GtTsd);
    data.getUser().removeTemp("Agtp225_ImgGtEsl");
    data.getUser().setTemp("Agtp225_ImgGtEsl", g_Img_GtEsl);
    data.getUser().removeTemp("Agtp225_vTabImg");
    data.getUser().setTemp("Agtp225_vTabImg", g_Tab_Img);
  }
  //-------------------------------------------------------------------------------------------
  public void De_Tab_Img(Vector g_Tab_Img, RunData data)
  {
    g_Img       = img.Inicia_Img();
    lcData      = img.LSet_De_Img(g_Img);
    g_Img_Base  = img.LSet_A_ImgBase(lcData);
    g_Img_GtApe = gtimg.LSet_A_ImgGtApe(g_Img.getDat());
    g_Img_GtTsd = gtimg.LSet_A_ImgGtTsd(g_Img.getDat());
    g_Img_GtEsl = gtimg.LSet_A_ImgGtEsl(g_Img.getDat());
    for (i=0; i<g_Tab_Img.size(); i++)
        {
          lcData = img.LSet_De_Img((BF_IMG.Buf_Img)g_Tab_Img.elementAt(i));
          g_Img  = img.LSet_A_Img(lcData);
          if (g_Img.getDax().trim().equals("BASE"))
             {
               g_Img_GtApe = gtimg.LSet_A_ImgGtApe(g_Img.getDat());
               g_Img_Base  = img.LSet_A_ImgBase(g_Img_GtApe.getBse());
             }
          if (g_Img.getDax().trim().equals("XTSD"))
             { g_Img_GtTsd = gtimg.LSet_A_ImgGtTsd(g_Img.getDat()); }
          if (g_Img.getDax().trim().equals("YESL"))
             { g_Img_GtEsl = gtimg.LSet_A_ImgGtEsl(g_Img.getDat()); }
        }
  }
  //-------------------------------------------------------------------------------------------
  public void Carga_Inicial(RunData data, Context context) throws Exception
  {
    g_MsgED135 = MSGED135.Inicia_MsgED135();
    rg.MoverA(g_MsgED135.ED135_Idr, "C");
    rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
    rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
    g_MsgED135 = MSGED135.LSet_A_MsgED135(g_Msg.getDat());
    data.getUser().removeTemp("Agtp225_MsgED135");
    data.getUser().setTemp("Agtp225_MsgED135", g_MsgED135);
  }
  //-------------------------------------------------------------------------------------------
  public void Carga_Doctos(RunData data, Context context) throws Exception
  {
    g_MsgED125 = MSGED125.Inicia_MsgED125();
    rg.MoverA(g_MsgED125.ED125_Idr, "I");
    rg.MoverA(g_MsgED125.ED125_Nev, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_MsgED125.ED125_Trt, g_PrmPC080.PC080_Trt);
    rg.MoverA(g_Msg.Msg_Dat, MSGED125.LSet_De_MsgED125(g_MsgED125));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED125");
    g_MsgED125 = MSGED125.LSet_A_MsgED125(g_Msg.getDat());
    data.getUser().removeTemp("Agtp225_MsgED125");
    data.getUser().setTemp("Agtp225_MsgED125", g_MsgED125);
  }
  //===============================================================================================================================
  public void Actualiza_Datos(RunData data, Context context) throws Exception
  {
    g_PrmED210 = PRMED210.Inicia_PrmED210();
    rg.MoverA(g_PrmED210.ED210_Ldt, data.getParameters().getString("GtTsd_Opt", ""));
  }
  //-------------------------------------------------------------------------------------------
  public Vector A_Tab_Img(RunData data)
  {
    Vector v_TabImg = (Vector)data.getUser().getTemp("Agtp225_vTabImg");
    g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp225_ImgGtApe");
    g_Img_GtTsd = (GT_IMG.Buf_Img_GtTsd)data.getUser().getTemp("Agtp225_ImgGtTsd");
    g_Img_GtEsl = (GT_IMG.Buf_Img_GtEsl)data.getUser().getTemp("Agtp225_ImgGtEsl");
    Vector vBuf_Img = new Vector();
    rg.MoverA(g_Img.Img_Ntr, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_Img.Img_Dax, "BASE");
    rg.MoverA(g_Img.Img_Seq, "000");
    rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtApe(g_Img_GtApe));
    Vector vPaso = img.LSet_A_vImg(img.LSet_De_Img(g_Img));
    BF_IMG.Buf_Img tImg = (BF_IMG.Buf_Img)vPaso.elementAt(0);
    vBuf_Img.add(tImg);
    rg.MoverA(g_Img.Img_Dax, "XTSD");
    rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtTsd(g_Img_GtTsd));
    vPaso = img.LSet_A_vImg(img.LSet_De_Img(g_Img));
    tImg = (BF_IMG.Buf_Img)vPaso.elementAt(0);
    vBuf_Img.add(tImg);
    rg.MoverA(g_Img.Img_Dax, "YESL");
    rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtEsl(g_Img_GtEsl));
    vPaso = img.LSet_A_vImg(img.LSet_De_Img(g_Img));
    tImg = (BF_IMG.Buf_Img)vPaso.elementAt(0);
    vBuf_Img.add(tImg);
    for (i=0; i<v_TabImg.size(); i++)
        {
          BF_IMG.Buf_Img l_Img = new BF_IMG.Buf_Img();
          l_Img = (BF_IMG.Buf_Img)v_TabImg.elementAt(i);
          if (!(l_Img.getDax().trim().equals("BASE")
             || l_Img.getDax().trim().equals("XTSD")
             || l_Img.getDax().trim().equals("YESL")))
             {
               vPaso = img.LSet_A_vImg(img.LSet_De_Img(l_Img));
               tImg = (BF_IMG.Buf_Img)vPaso.elementAt(0);
               vBuf_Img.add(tImg);
             }
        }
    return vBuf_Img;
  }
  //-------------------------------------------------------------------------------------------
  public void Cursatura(Vector g_Tab_Img, RunData data, Context context) throws Exception
  {
    img.Graba_Img_Host(g_Msg, g_Tab_Img, data, context);
    g_MsgED135 = MSGED135.Inicia_MsgED135();
  //rg.MoverA(g_MsgED135.ED135_Idr, "X");
    rg.MoverA(g_MsgED135.ED135_Idr, "U");
    rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
    rg.MoverA(g_MsgED135.ED135_Est, "");
    rg.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
    rg.MoverA(g_MsgED135.ED135_Ldv, g_PrmED210.ED210_Ldt);
    rg.MoverA(g_MsgED135.ED135_Ezt, "REEJH");
    rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
  }
  //===============================================================================================================================
}