// Source File Name:   Agtp305.java
// Descripcion     :   Seleccion PContable Valores/Acciones/Warrants (ED135, GT096)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.BF_IMG;
import com.FHTServlet.modules.global.PRMPC080;

import com.intGlobal.modules.global.MSGED135;
import com.intGlobal.modules.global.PRMED210;
import com.intGlobal.modules.global.PRMCL095;
import com.intGlobal.modules.global.PRMTG130;

import com.intGarantias.modules.global.GT_IMG;
import com.intGarantias.modules.global.MSGGT096;
import com.intGarantias.modules.global.PRMGT093;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp305 extends FHTServletAction
{
  //---------------------------------------------------------------------------------------
  public int    i = 0;
  public String lcData = "";
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  PRMPC080.PrmPC080 g_PrmPC080     = new PRMPC080.PrmPC080();
  Vector v_Tab_Img                 = new Vector();
  BF_IMG img                       = new BF_IMG();
  BF_IMG.Buf_Img g_Img             = new BF_IMG.Buf_Img();
  BF_IMG.Buf_Img_Bse g_Img_Base    = new BF_IMG.Buf_Img_Bse();
  GT_IMG gtimg                     = new GT_IMG();
  GT_IMG.Buf_Img_GtApe g_Img_GtApe = new GT_IMG.Buf_Img_GtApe();
  GT_IMG.Buf_Img_GtUsu g_Img_GtUsu = new GT_IMG.Buf_Img_GtUsu();
  GT_IMG.Buf_Img_GtPrp g_Img_GtPrp = new GT_IMG.Buf_Img_GtPrp();
  Vector v_Img_GtUsu               = new Vector();
  Vector v_Img_GtPrp               = new Vector();
  MSGED135.Buf_MsgED135 g_MsgED135 = new MSGED135.Buf_MsgED135();
  MSGGT096.Buf_MsgGT096 g_MsgGT096 = new MSGGT096.Buf_MsgGT096();
  PRMED210.Buf_PrmED210 g_PrmED210 = new PRMED210.Buf_PrmED210();
  PRMTG130.Buf_PrmTG130 g_PrmTG130 = new PRMTG130.Buf_PrmTG130();
  PRMGT093.Buf_PrmGT093 g_PrmGT093 = new PRMGT093.Buf_PrmGT093();
  PRMCL095.Buf_PrmCL095 g_PrmCL095 = new PRMCL095.Buf_PrmCL095();
  //---------------------------------------------------------------------------------------
  public Agtp305()
  {
  }
  //---------------------------------------------------------------------------------------
  public void doPerform(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP305[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp305-DIN", data);
    if (g_Msg.Msg_Pgr.toString().equals("RT"))
       { doAgtp305_Continue(data, context); }
    else
       { doAgtp305_Init(data, context); }
  }
  //---------------------------------------------------------------------------------------
  public void doCancel(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP305[doCancel.start]", "[CANCELADO]");
    setTemplate(data, "Garantias,Agt,AGTP305.vm");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp305_Init(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP305[doAgtp305_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    Carga_de_Host(data, context);
    Carga_Inicial(data, context);
    data.getUser().removeTemp("Agtp305_PrmPC080");
    data.getUser().setTemp("Agtp305_PrmPC080", g_PrmPC080);					
    setTemplate(data, "Garantias,Agt,AGTP305.vm" );
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp305(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP305[doAgtp305.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp305-MAN", data);
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    String Opc = data.getParameters().getString("Opc" ,"");
    if (Opc.trim().equals("R"))
       {
         //Boton Errores
         g_MsgED135 = (MSGED135.Buf_MsgED135)data.getUser().getTemp("Agtp305_MsgED135");
         g_PrmED210 = PRMED210.Inicia_PrmED210();
         rg.MoverA(g_PrmED210.ED210_Rtn, "RO");
         rg.MoverA(g_PrmED210.ED210_Ldt, g_MsgED135.ED135_Ldv);
         data.getUser().removeTemp("Agtp305_g_PrmPC080");
         data.getUser().setTemp("Agtp305_g_PrmPC080" ,g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMED210.LSet_De_PrmED210(g_PrmED210));
         BF_MSG.Link_Program("Agtp305", "Aedp210", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("D"))
       {
         //Boton Devolver
         g_PrmED210 = PRMED210.Inicia_PrmED210();
         rg.MoverA(g_PrmED210.ED210_Rtn, "RW");
         data.getUser().removeTemp("Agtp305_g_PrmPC080");
         data.getUser().setTemp("Agtp305_g_PrmPC080" ,g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMED210.LSet_De_PrmED210(g_PrmED210));
         BF_MSG.Link_Program("Agtp305", "Aedp210", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("T"))
       {
         //Boton Detalle
         data.getUser().removeTemp("Agtp305_g_PrmPC080");
         data.getUser().setTemp("Agtp305_g_PrmPC080", g_PrmPC080);
         if (g_PrmPC080.PC080_Rtn.toString().trim().compareTo("RO")!=0)
            { Actualiza_Datos(data, context); }
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp305_ImgGtApe");
         Vector g_Tab_Img = A_Tab_Img(data);
         data.getUser().removeTemp("Agtp305_gTabImg");
         data.getUser().setTemp("Agtp305_gTabImg", g_Tab_Img);
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         if (g_Img_GtApe.GtApe_Dpp.toString().equals("W"))
            { BF_MSG.Link_Program("Agtp305", "Agtp314", data, g_Msg); }
         if (g_Img_GtApe.GtApe_Dpp.toString().equals("V"))
            { BF_MSG.Link_Program("Agtp305", "Agtp315", data, g_Msg); }
         if (g_Img_GtApe.GtApe_Dpp.toString().equals("A"))
            { BF_MSG.Link_Program("Agtp305", "Agtp316", data, g_Msg); }
         if (g_Img_GtApe.GtApe_Dpp.toString().equals("B")
          || g_Img_GtApe.GtApe_Dpp.toString().equals("I")
          || g_Img_GtApe.GtApe_Dpp.toString().equals("M"))
            { BF_MSG.Link_Program("Agtp305", "Agtp318", data, g_Msg); }
         return;
       }
    if (Opc.trim().equals("S"))
       {
         //Boton Salir
         data.getUser().removeTemp("Agtp305_MsgED135");
         data.getUser().removeTemp("Agtp305_vTabImg");
         data.getUser().removeTemp("Agtp305_ImgBase");
         data.getUser().removeTemp("Agtp305_ImgGtApe");
         data.getUser().removeTemp("Agtp305_PrmPC080");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OS");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp305", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("O"))
       {
         //Boton OK (Aceptar)
         Actualiza_Datos(data, context);
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp305_ImgGtApe");
         if (g_Img_GtApe.GtApe_Dsw.toString().equals("N"))
            {
              data.getUser().removeTemp("Agtp305_g_PrmPC080");
              data.getUser().setTemp("Agtp305_g_PrmPC080", g_PrmPC080);
              BF_MSG.MsgInt("Agtp305", data, g_Msg, "Debe Ingresar Detalle", "1"); 
              return;
            }
         Vector g_Tab_Img = A_Tab_Img(data);
         if (g_PrmPC080.PC080_Est.toString().equals("CRALZ"))
            {
              data.getUser().removeTemp("Agtp305_gTabImg");
              data.getUser().setTemp("Agtp305_gTabImg", g_Tab_Img);
            }
         else
            {
              img.Graba_Img_Host(g_Msg, g_Tab_Img, data, context);
              g_MsgED135 = MSGED135.Inicia_MsgED135();
              rg.MoverA(g_MsgED135.ED135_Idr, "M");
              rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
              rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
              rg.MoverA(g_MsgED135.ED135_Est, "");
              rg.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
              rg.MoverA(g_MsgED135.ED135_Vlc, rg.FmtValor(g_Img_GtApe.GtApe_Vtt,4,2,15,"+"));
              //rg.MoverA(g_MsgED135.ED135_Ezt, "ENVSD");
              if (g_Img_GtApe.GtApe_Ggr.toString().trim().equals("DMN")
               || g_Img_GtApe.GtApe_Ggr.toString().trim().equals("DMX")
               || g_Img_GtApe.GtApe_Ggr.toString().trim().equals("CTW"))
                 { rg.MoverA(g_MsgED135.ED135_Ezt, "ENVSD"); }
              if (g_Img_GtApe.GtApe_Ggr.toString().trim().equals("PMN")
               || g_Img_GtApe.GtApe_Ggr.toString().trim().equals("PMX")
               || g_Img_GtApe.GtApe_Ggr.toString().trim().equals("PAC"))
                 //{ rg.MoverA(g_MsgED135.ED135_Ezt, "ENSPR"); }
                 { rg.MoverA(g_MsgED135.ED135_Ezt, "ENVSD"); }
              rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
              g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
            }
         data.getUser().removeTemp("Agtp305_MsgED135");
         data.getUser().removeTemp("Agtp305_vTabImg");
         data.getUser().removeTemp("Agtp305_ImgBase");
         data.getUser().removeTemp("Agtp305_ImgGtApe");
         data.getUser().removeTemp("Agtp305_PrmPC080");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp305", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("C"))
       {
         //Boton Cursar
         g_MsgED135 = MSGED135.Inicia_MsgED135();
         rg.MoverA(g_MsgED135.ED135_Idr, "M");
         rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
         rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
         rg.MoverA(g_MsgED135.ED135_Est, "");
         rg.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
         rg.MoverA(g_MsgED135.ED135_Ezt, "TERMI");
         rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
         g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
         data.getUser().removeTemp("Agtp305_MsgED135");
         data.getUser().removeTemp("Agtp305_vTabImg");
         data.getUser().removeTemp("Agtp305_ImgBase");
         data.getUser().removeTemp("Agtp305_ImgGtApe");
         data.getUser().removeTemp("Agtp305_PrmPC080");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp305", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("3"))
       {
         //Consulta Usuarios Disponibles
         Actualiza_Datos(data, context);
         v_Img_GtUsu = (Vector)data.getUser().getTemp("Agtp305_vImgGtUsu");
         int liUsu = v_Img_GtUsu.size();
         String lcUsu = Integer.toString(liUsu);
         g_PrmCL095 = PRMCL095.Inicia_PrmCL095();
         rg.MoverA(g_PrmCL095.CL095_Cli, g_PrmPC080.PC080_Cli);
         rg.MoverA(g_PrmCL095.CL095_Ncl, g_PrmPC080.PC080_Ncl);
         rg.MoverA(g_PrmCL095.CL095_Idr, "E");
         rg.MoverA(g_PrmCL095.CL095_Nro, rg.FmtValor(lcUsu,0,0,2,"+"));
         for (i=0; i<v_Img_GtUsu.size(); i++)
             {
               PRMCL095.Bff_PrmCL095 d_PrmCL095 = new PRMCL095.Bff_PrmCL095();
               g_Img_GtUsu = (GT_IMG.Buf_Img_GtUsu)v_Img_GtUsu.elementAt(i);
               rg.MoverA(d_PrmCL095.CL095_Cli, g_Img_GtUsu.GtUsu_Cli);
               rg.MoverA(d_PrmCL095.CL095_Rel, "");
               rg.MoverA(d_PrmCL095.CL095_Prc, "");
               rg.MoverA(d_PrmCL095.CL095_Orp, "");
               rg.MoverA(d_PrmCL095.CL095_Ncl, g_Img_GtUsu.GtUsu_Ncl);
               rg.MoverA(d_PrmCL095.CL095_Dir, "");
               rg.MoverA(d_PrmCL095.CL095_Rcy, "");
               rg.MoverA(d_PrmCL095.CL095_Ncy, "");
               rg.MoverA(d_PrmCL095.CL095_Rrp, "");
               rg.MoverA(d_PrmCL095.CL095_Nrp, "");
               rg.MoverA(d_PrmCL095.CL095_Rrs, "");
               rg.MoverA(d_PrmCL095.CL095_Nrs, "");
               rg.MoverA(d_PrmCL095.CL095_Obl, "");
               rg.MoverA(d_PrmCL095.CL095_Isg, "");
               g_PrmCL095.CL095_Tap.set(i, d_PrmCL095); 
             }
         data.getUser().removeTemp("Agtp305_g_PrmPC080");
         data.getUser().setTemp("Agtp305_g_PrmPC080" ,g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMCL095.LSet_De_PrmCL095(g_PrmCL095));
         BF_MSG.Link_Program("Agtp305", "Aclp095", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("4"))
       {
         //Consulta Propietarios Disponibles
         Actualiza_Datos(data, context);
         v_Img_GtPrp = (Vector)data.getUser().getTemp("Agtp305_vImgGtPrp");
         int liPrp = v_Img_GtPrp.size();
         String lcPrp = Integer.toString(liPrp);
         g_PrmCL095 = PRMCL095.Inicia_PrmCL095();
         rg.MoverA(g_PrmCL095.CL095_Cli, g_PrmPC080.PC080_Cli);
         rg.MoverA(g_PrmCL095.CL095_Ncl, g_PrmPC080.PC080_Ncl);
         rg.MoverA(g_PrmCL095.CL095_Idr, "F");
         rg.MoverA(g_PrmCL095.CL095_Nro, rg.FmtValor(lcPrp,0,0,2,"+"));
         for (i=0; i<v_Img_GtPrp.size(); i++)
             {
               PRMCL095.Bff_PrmCL095 d_PrmCL095 = new PRMCL095.Bff_PrmCL095();
               g_Img_GtPrp = (GT_IMG.Buf_Img_GtPrp)v_Img_GtPrp.elementAt(i);
               rg.MoverA(d_PrmCL095.CL095_Cli, g_Img_GtPrp.GtPrp_Cli);
               rg.MoverA(d_PrmCL095.CL095_Rel, "");
               rg.MoverA(d_PrmCL095.CL095_Prc, "");
               rg.MoverA(d_PrmCL095.CL095_Orp, "");
               rg.MoverA(d_PrmCL095.CL095_Ncl, g_Img_GtPrp.GtPrp_Ncl);
               rg.MoverA(d_PrmCL095.CL095_Dir, "");
               rg.MoverA(d_PrmCL095.CL095_Rcy, "");
               rg.MoverA(d_PrmCL095.CL095_Ncy, "");
               rg.MoverA(d_PrmCL095.CL095_Rrp, "");
               rg.MoverA(d_PrmCL095.CL095_Nrp, "");
               rg.MoverA(d_PrmCL095.CL095_Rrs, "");
               rg.MoverA(d_PrmCL095.CL095_Nrs, "");
               rg.MoverA(d_PrmCL095.CL095_Obl, "");
               rg.MoverA(d_PrmCL095.CL095_Isg, "");
               g_PrmCL095.CL095_Tap.set(i, d_PrmCL095); 
             }
         data.getUser().removeTemp("Agtp305_g_PrmPC080");
         data.getUser().setTemp("Agtp305_g_PrmPC080" ,g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMCL095.LSet_De_PrmCL095(g_PrmCL095));
         BF_MSG.Link_Program("Agtp305", "Aclp095", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("5"))
       {
         //Consulta PContables Disponibles
         Actualiza_Datos(data, context);
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp305_ImgGtApe");
         g_Img_Base  = img.LSet_A_ImgBase(g_Img_GtApe.GtApe_Bse.toString());
         g_PrmGT093 = PRMGT093.Inicia_PrmGT093();
         rg.MoverA(g_PrmGT093.GT093_Sis, "GAR");
         rg.MoverA(g_PrmGT093.GT093_Dcn, g_Img_Base.Img_Base_Dcn);
         rg.MoverA(g_PrmGT093.GT093_Itb, "VWA");
         data.getUser().removeTemp("Agtp305_g_PrmPC080");
         data.getUser().setTemp("Agtp305_g_PrmPC080", g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMGT093.LSet_De_PrmGT093(g_PrmGT093));
         BF_MSG.Link_Program("Agtp305", "Agtp093", data, g_Msg);
         return;
       }
  }
  //---------------------------------------------------------------------------------------
  public void Actualiza_Datos(RunData data, Context context)
              throws Exception 
  {
    g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp305_ImgGtApe");
    rg.MoverA(g_Img_GtApe.GtApe_Dsg, data.getParameters().getString("GtApe_Dsg", ""));
    rg.MoverA(g_Img_GtApe.GtApe_Cbt, data.getParameters().getString("GtApe_Cbt", ""));
    data.getUser().removeTemp("Agtp305_ImgGtApe");
    data.getUser().setTemp("Agtp305_ImgGtApe", g_Img_GtApe);
  }
  //---------------------------------------------------------------------------------------
  public void Carga_de_Host(RunData data, Context context)
              throws Exception 
  {
    if (g_PrmPC080.PC080_Est.toString().equals("CRALZ"))
       { v_Tab_Img = (Vector)data.getUser().getTemp("Agtp305_gTabImg"); }
    else
       { v_Tab_Img = img.Carga_Img_Host(g_Msg, g_PrmPC080.PC080_Ntr.toString(), data, context); }
    De_Tab_Img(v_Tab_Img, data);
    data.getUser().removeTemp("Agtp305_ImgBase");
    data.getUser().setTemp("Agtp305_ImgBase", g_Img_Base);
    data.getUser().removeTemp("Agtp305_ImgGtApe");
    data.getUser().setTemp("Agtp305_ImgGtApe", g_Img_GtApe);
    data.getUser().removeTemp("Agtp305_vImgGtUsu");
    data.getUser().setTemp("Agtp305_vImgGtUsu", v_Img_GtUsu);
    data.getUser().removeTemp("Agtp305_vImgGtPrp");
    data.getUser().setTemp("Agtp305_vImgGtPrp", v_Img_GtPrp);
    data.getUser().removeTemp("Agtp305_vTabImg");
    data.getUser().setTemp("Agtp305_vTabImg", v_Tab_Img);
  }
  //---------------------------------------------------------------------------------------
  public void De_Tab_Img(Vector v_Tab_Img, RunData data)
  {
    String Dsw  = "N";
    g_Img       = img.Inicia_Img();
    lcData      = img.LSet_De_Img(g_Img);
    g_Img_Base  = img.LSet_A_ImgBase(lcData);
    g_Img_GtApe = gtimg.LSet_A_ImgGtApe(g_Img.Img_Dat.toString());
    v_Img_GtUsu.clear();
    v_Img_GtPrp.clear();
    for (i=0; i<v_Tab_Img.size(); i++)
        {
          lcData = img.LSet_De_Img((BF_IMG.Buf_Img)v_Tab_Img.elementAt(i));
          g_Img  = img.LSet_A_Img(lcData);
          if (g_Img.Img_Dax.toString().trim().equals("BASE"))
             {
               g_Img_GtApe = gtimg.LSet_A_ImgGtApe(g_Img.Img_Dat.toString());
               g_Img_Base  = img.LSet_A_ImgBase(g_Img_GtApe.GtApe_Bse.toString());
             }
          if (g_Img.Img_Dax.toString().trim().equals("YYUSU"))
             {
               g_Img_GtUsu = gtimg.LSet_A_ImgGtUsu(g_Img.Img_Dat.toString());
               v_Img_GtUsu.add (g_Img_GtUsu); 
             }
          if (g_Img.Img_Dax.toString().trim().equals("YYCMP"))
             {
               g_Img_GtUsu = gtimg.LSet_A_ImgGtUsu(g_Img.Img_Dat.toString());
               rg.MoverA(g_Img_GtUsu.GtUsu_Idc, "C");
               v_Img_GtUsu.add (g_Img_GtUsu); 
             }
          if (g_Img.Img_Dax.toString().trim().equals("YYPRP"))
             {
               g_Img_GtPrp = gtimg.LSet_A_ImgGtPrp(g_Img.Img_Dat.toString());
               v_Img_GtPrp.add (g_Img_GtPrp); 
             }
          if (g_Img.Img_Dax.toString().trim().equals("XXWAR")
           || g_Img.Img_Dax.toString().trim().equals("ZZVFI")
           || g_Img.Img_Dax.toString().trim().equals("ZZACC")
           || g_Img.Img_Dax.toString().trim().equals("ZZIFR"))
             { Dsw = "I"; }
        }
    rg.MoverA(g_Img_GtApe.GtApe_Dsw, Dsw);
    //if (v_Img_GtUsu.size()==0)
    //   {
    //     rg.MoverA(g_Img_GtUsu.GtUsu_Cli, g_PrmPC080.PC080_Cli);
    //     rg.MoverA(g_Img_GtUsu.GtUsu_Ncl, g_PrmPC080.PC080_Ncl);
    //     rg.MoverA(g_Img_GtUsu.GtUsu_Idc, " ");
    //     rg.MoverA(g_Img_GtUsu.GtUsu_Fll, " ");
    //     v_Img_GtUsu.add (g_Img_GtUsu); 
    //   }
    //if (v_Img_GtPrp.size()==0)
    //   {
    //     rg.MoverA(g_Img_GtPrp.GtPrp_Cli, g_Img_Base.Img_Base_Cli);
    //     rg.MoverA(g_Img_GtPrp.GtPrp_Ncl, g_Img_Base.Img_Base_Ncl);
    //     rg.MoverA(g_Img_GtPrp.GtPrp_Fll, " ");
    //     v_Img_GtPrp.add (g_Img_GtPrp); 
    //   }
  }
  //---------------------------------------------------------------------------------------
  public void Carga_Inicial(RunData data, Context context)
              throws Exception 
  {
    g_MsgED135 = MSGED135.Inicia_MsgED135();
    rg.MoverA(g_MsgED135.ED135_Idr, "C");
    rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
    rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
    g_MsgED135 = MSGED135.LSet_A_MsgED135(g_Msg.Msg_Dat.toString());
    data.getUser().removeTemp("Agtp305_MsgED135");
    data.getUser().setTemp("Agtp305_MsgED135", g_MsgED135);
  }
  //---------------------------------------------------------------------------------------
  public Vector A_Tab_Img(RunData data)
  {
    v_Tab_Img = (Vector)data.getUser().getTemp("Agtp305_vTabImg");
    Vector v_Img = new Vector();
    v_Img.clear();
    rg.MoverA(g_Img.Img_Ntr, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_Img.Img_Dax, "BASE");
    rg.MoverA(g_Img.Img_Seq, "000");
    rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtApe(g_Img_GtApe));
    Vector vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
    v_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
    //--------------------------------------------------------------------------
    rg.MoverA(g_Img.Img_Seq, "000");
    for (i=0; i<v_Tab_Img.size(); i++)
        {
          lcData = img.LSet_De_Img((BF_IMG.Buf_Img)v_Tab_Img.elementAt(i));
          g_Img  = img.LSet_A_Img(lcData);
          if (g_Img.Img_Dax.toString().trim().equals("BASE"))
             { }
          else
             {

               if (g_Img.Img_Dax.toString().trim().equals("YYUSU")
                || g_Img.Img_Dax.toString().trim().equals("YYCMP")
                || g_Img.Img_Dax.toString().trim().equals("YYPRP")
                || g_Img.Img_Dax.toString().trim().equals("YYINS"))
                  { }
               else
                  {
                    Vector vPaso1 = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
                    v_Img.add((BF_IMG.Buf_Img)vPaso1.elementAt(0));
                  }
             }
        }
    v_Img_GtUsu = (Vector)data.getUser().getTemp("Agtp305_vImgGtUsu");
    rg.MoverA(g_Img.Img_Seq, "000");
    for (i=0; i<v_Img_GtUsu.size(); i++)
        {
          g_Img_GtUsu = (GT_IMG.Buf_Img_GtUsu)v_Img_GtUsu.elementAt(i);
          if (g_Img_GtUsu.GtUsu_Idc.toString().equals("C"))
             { rg.MoverA(g_Img.Img_Dax, "YYCMP"); }
          else
             { rg.MoverA(g_Img.Img_Dax, "YYUSU"); }
          rg.MoverA(g_Img.Img_Seq, rg.Suma(g_Img.Img_Seq,0,"1",0,3,0));
          rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtUsu(g_Img_GtUsu));
          Vector vPaso2 = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
          v_Img.add((BF_IMG.Buf_Img)vPaso2.elementAt(0));
        }
    v_Img_GtPrp = (Vector)data.getUser().getTemp("Agtp305_vImgGtPrp");
    rg.MoverA(g_Img.Img_Seq, "000");
    for (i=0; i<v_Img_GtPrp.size(); i++)
        {
          g_Img_GtPrp = (GT_IMG.Buf_Img_GtPrp)v_Img_GtPrp.elementAt(i);
          rg.MoverA(g_Img.Img_Dax, "YYPRP");
          rg.MoverA(g_Img.Img_Seq, rg.Suma(g_Img.Img_Seq,0,"1",0,3,0));
          rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtPrp(g_Img_GtPrp));
          Vector vPaso3 = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
          v_Img.add((BF_IMG.Buf_Img)vPaso3.elementAt(0));
        }
    //--------------------------------------------------------------------------
    return v_Img;
  }
//---------------------------------------------------------------------------------------
  public void doAgtp305_Continue(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP305[doAgtp305_Continue.start]", "[" + data.getUser().getUserName() + "]");
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Agtp093"))
       {
         g_PrmGT093 = PRMGT093.LSet_A_PrmGT093(g_Msg.Msg_Dat.toString());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp305_g_PrmPC080");
         data.getUser().removeTemp("Agtp305_g_PrmPC080");
         if (g_PrmGT093.GT093_Rtn.toString().trim().equals("OK"))
             {
               g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp305_ImgGtApe");
               rg.MoverA(g_Img_GtApe.GtApe_Dpv, g_PrmGT093.GT093_Dpc);
             //rg.MoverA(g_Img_GtApe.GtApe_Dpp, "V");
               rg.MoverA(g_Img_GtApe.GtApe_Dpp, g_PrmGT093.GT093_Itb);
               rg.MoverA(g_Img_GtApe.GtApe_Pct, rg.FmtValor(g_PrmGT093.GT093_Pct,0,2,5,"+"));
               if (g_Img_GtApe.GtApe_Ggr.toString().equals("CTW"))
                  { 
                    rg.MoverA(g_Img_GtApe.GtApe_Dpp, "W"); 
                    rg.MoverA(g_Img_GtApe.GtApe_Ggr, "CTW"); 
                  }
               if (g_Img_GtApe.GtApe_Ggr.toString().equals("PAC"))
                  { 
                    rg.MoverA(g_Img_GtApe.GtApe_Dpp, "A"); 
                    rg.MoverA(g_Img_GtApe.GtApe_Ggr, "PAC"); 
                  }
               data.getUser().removeTemp("Agtp305_ImgGtApe");
               data.getUser().setTemp("Agtp305_ImgGtApe", g_Img_GtApe);
             }
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP305.vm" );
         return;
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Aclp095"))
       {
         g_PrmCL095 = PRMCL095.LSet_A_PrmCL095(g_Msg.Msg_Dat.toString());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp305_g_PrmPC080");
         data.getUser().removeTemp("Agtp305_g_PrmPC080");
         if (g_PrmCL095.CL095_Rtn.toString().equals("OK"))
            {
              PRMCL095.Bff_PrmCL095 d_PrmCL095 = new PRMCL095.Bff_PrmCL095();
              if (g_PrmCL095.CL095_Idr.toString().equals("E"))
                 {
                   v_Img_GtUsu.clear();
                   for (i=0; i<g_PrmCL095.CL095_Tap.size(); i++)
                       {
                         d_PrmCL095 = (PRMCL095.Bff_PrmCL095)g_PrmCL095.CL095_Tap.elementAt(i);
                         if (d_PrmCL095.CL095_Cli.toString().trim().equals("")) break;
                         GT_IMG.Buf_Img_GtUsu l_Img_GtUsu = new GT_IMG.Buf_Img_GtUsu();
                         rg.MoverA(l_Img_GtUsu.GtUsu_Cli, d_PrmCL095.CL095_Cli);
                         rg.MoverA(l_Img_GtUsu.GtUsu_Ncl, d_PrmCL095.CL095_Ncl);
                         rg.MoverA(l_Img_GtUsu.GtUsu_Idc, " ");
                         rg.MoverA(l_Img_GtUsu.GtUsu_Fll, " ");
                         v_Img_GtUsu.add (l_Img_GtUsu); 
                       }
                   data.getUser().removeTemp("Agtp305_vImgGtUsu");
                   data.getUser().setTemp("Agtp305_vImgGtUsu", v_Img_GtUsu);
                 }
              if (g_PrmCL095.CL095_Idr.toString().equals("F"))
                 {
                   v_Img_GtPrp.clear();
                   for (i=0; i<g_PrmCL095.CL095_Tap.size(); i++)
                       {
                         d_PrmCL095 = (PRMCL095.Bff_PrmCL095)g_PrmCL095.CL095_Tap.elementAt(i);
                         if (d_PrmCL095.CL095_Cli.toString().trim().equals("")) break;
                         GT_IMG.Buf_Img_GtPrp l_Img_GtPrp = new GT_IMG.Buf_Img_GtPrp();
                         rg.MoverA(l_Img_GtPrp.GtPrp_Cli, d_PrmCL095.CL095_Cli);
                         rg.MoverA(l_Img_GtPrp.GtPrp_Ncl, d_PrmCL095.CL095_Ncl);
                         rg.MoverA(l_Img_GtPrp.GtPrp_Fll, " ");
                         v_Img_GtPrp.add (l_Img_GtPrp); 
                       }
                   data.getUser().removeTemp("Agtp305_vImgGtPrp");
                   data.getUser().setTemp("Agtp305_vImgGtPrp", v_Img_GtPrp);
                 }
            }
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP305.vm" );
         return;
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Agtp314")
     || rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Agtp315")
     || rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Agtp316")
     || rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Agtp318"))
       {
         PRMPC080.PrmPC080 l_PrmPC080 = new PRMPC080.PrmPC080();
         l_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp305_g_PrmPC080");
         data.getUser().removeTemp("Agtp305_g_PrmPC080");
         v_Tab_Img = (Vector)data.getUser().getTemp("Agtp305_gTabImg");
         De_Tab_Img(v_Tab_Img, data);
         data.getUser().removeTemp("Agtp305_ImgBase");
         data.getUser().setTemp("Agtp305_ImgBase", g_Img_Base);
         data.getUser().removeTemp("Agtp305_ImgGtApe");
         data.getUser().setTemp("Agtp305_ImgGtApe", g_Img_GtApe);
         data.getUser().removeTemp("Agtp305_vImgGtUsu");
         data.getUser().setTemp("Agtp305_vImgGtUsu", v_Img_GtUsu);
         data.getUser().removeTemp("Agtp305_vImgGtPrp");
         data.getUser().setTemp("Agtp305_vImgGtPrp", v_Img_GtPrp);
         data.getUser().removeTemp("Agtp305_vTabImg");
         data.getUser().setTemp("Agtp305_vTabImg", v_Tab_Img);
         data.getUser().removeTemp("Agtp305_gTabImg");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP305.vm" );
         return;
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Aedp210"))
       {
         g_PrmED210 = PRMED210.LSet_A_PrmED210(g_Msg.Msg_Dat.toString());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp305_g_PrmPC080");
         data.getUser().removeTemp("Agtp305_g_PrmPC080");
         if (!g_PrmED210.ED210_Rtn.toString().equals("OK"))
            {
              //Carga_de_Host(data, context);
              //Carga_Inicial(data, context);
              data.getUser().removeTemp("Agtp305_PrmPC080");
              data.getUser().setTemp("Agtp305_PrmPC080", g_PrmPC080);					
              rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
              BF_MSG.Param_Program(data, g_Msg);
              setTemplate(data, "Garantias,Agt,AGTP305.vm" );
              return;
            }
         else
            {
              g_MsgED135 = MSGED135.Inicia_MsgED135();
              rg.MoverA(g_MsgED135.ED135_Idr, "X");
              rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
              rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
              rg.MoverA(g_MsgED135.ED135_Est, "");
              rg.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
              rg.MoverA(g_MsgED135.ED135_Ldv, g_PrmED210.ED210_Ldt);
              rg.MoverA(g_MsgED135.ED135_Ezt, "DVSPR");
              rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
              g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
              data.getUser().removeTemp("Agtp305_MsgED135");
              data.getUser().removeTemp("Agtp305_vTabImg");
              data.getUser().removeTemp("Agtp305_ImgBase");
              data.getUser().removeTemp("Agtp305_ImgGtApe");
              data.getUser().removeTemp("Agtp305_PrmPC080");
              rg.MoverA(g_PrmPC080.PC080_Rtn, "OA");
              rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
              BF_MSG.Return_Data("Agtp305", data, g_Msg);
              return;
            }
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Appp030"))
       {
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp305_g_PrmPC080");
         data.getUser().removeTemp("Agtp305_g_PrmPC080");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP305.vm" );
         return;
       }
  }
  //---------------------------------------------------------------------------------------
}