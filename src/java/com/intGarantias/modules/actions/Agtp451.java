// Source File Name:   Agtp451.java
// Descripcion     :   Consulta Garantia Especifica (GT450, GT451, GT452, GT453)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.PRMPC080;
import com.FHTServlet.modules.global.PRMPP030;

import com.intGlobal.modules.global.PRMCL090;

import com.intGarantias.modules.global.MSGGT450;
import com.intGarantias.modules.global.MSGGT451;
import com.intGarantias.modules.global.MSGGT452;
import com.intGarantias.modules.global.MSGGT453;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp451 extends FHTServletAction
{
  //===============================================================================================================================
  public int    i = 0;
  public String lcData = "";
  public Vector vDatos;
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  PRMPC080.PrmPC080 g_PrmPC080     = new PRMPC080.PrmPC080();
  PRMPP030.Buf_PrmPP030 g_PrmPP030 = new PRMPP030.Buf_PrmPP030();
  PRMCL090.Buf_PrmCL090 g_PrmCL090 = new PRMCL090.Buf_PrmCL090();
  MSGGT450.Buf_MsgGT450 g_MsgGT450 = new MSGGT450.Buf_MsgGT450();
  MSGGT451.Buf_MsgGT451 g_MsgGT451 = new MSGGT451.Buf_MsgGT451();
  MSGGT452.Buf_MsgGT452 g_MsgGT452 = new MSGGT452.Buf_MsgGT452();
//fht  MSGGT452.Bfv_Gbs v_Gbs           = new MSGGT452.Bfv_Gbs();
//fht  MSGGT452.Bfv_Gbs h_Gbs           = new MSGGT452.Bfv_Gbs();
  MSGGT453.Buf_MsgGT453 g_MsgGT453 = new MSGGT453.Buf_MsgGT453();
  //-------------------------------------------------------------------------------------------
  public Agtp451()
  {
  }
  //===============================================================================================================================
  public void doPerform(RunData data, Context context) throws Exception
  {
    //Log.debug("AGTP451[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp451-DIN", data);
    if (g_Msg.getPgr().equals("RT"))
       { doAgtp451_Continue(data, context); }
    else
       { doAgtp451_Init(data, context); }
  }
  //-------------------------------------------------------------------------------------------
  public void doAgtp451_Init(RunData data, Context context) throws Exception
  {
    //Log.debug("AGTP451[doAgtp451_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.getDat());
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (g_PrmPC080.getNcn().trim().equals("")
     && g_PrmPC080.getCli().trim().equals(""))
       {
         data.getUser().removeTemp("Agtp451_g_PrmPC080");
         data.getUser().setTemp("Agtp451_g_PrmPC080" ,g_PrmPC080);
         g_PrmCL090 = PRMCL090.Inicia_PrmCL090();
         rg.MoverA(g_PrmCL090.CL090_Pcc, "N");
         rg.MoverA(g_Msg.Msg_Dat, PRMCL090.LSet_De_PrmCL090(g_PrmCL090));
         BF_MSG.Link_Program("Agtp451", "Eclp090", data, g_Msg);
         return;
       }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (g_PrmPC080.getRtn().trim().equals(""))
       {
         Trae_Datos(data, context);
         if (!g_MsgGT450.getMen().trim().equals(""))
            {
              data.getUser().removeTemp("Agtp451_g_PrmPC080");
              data.getUser().setTemp("Agtp451_g_PrmPC080", g_PrmPC080);
              BF_MSG.MsgInt("Agtp451", data, g_Msg, "Error Host "+g_MsgGT450.getMen(), "T");
              return;
            }
         Poblar_Participes(data, context);
         Poblar_CreEsp(data, context);
         Poblar_Bienes(data, context);
         data.getUser().removeTemp("Agtp451_PrmPC080");
         data.getUser().setTemp("Agtp451_PrmPC080", g_PrmPC080);
       }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    setTemplate(data, "Garantias,Agt,AGTP451.vm" );
  }
  //===============================================================================================================================
  public void doAgtp451(RunData data, Context context) throws Exception
  {
    //Log.debug("AGTP451[doAgtp451.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp451-MAN", data);
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.getDat());
    String Opc = data.getParameters().getString("Opc", "");
    if (Opc.trim().equals("I"))
       {
         //Boton Imagen
//fht         String Tip = data.getParameters().getString("Tip", "");
         String Seq = data.getParameters().getString("Seq", "");
         i = Integer.parseInt(Seq);
         g_MsgGT450 = (MSGGT450.Buf_MsgGT450)data.getUser().getTemp("Agtp451_MsgGT450");
         g_MsgGT452 = (MSGGT452.Buf_MsgGT452)data.getUser().getTemp("Agtp451_MsgGT452");
//fht         h_Gbs      = (MSGGT452.Bfv_Gbs)data.getUser().getTemp("Agtp451_Hip");
//fht         v_Gbs      = (MSGGT452.Bfv_Gbs)data.getUser().getTemp("Agtp451_Gbs");
         MSGGT452.Bff_MsgGT452 d_MsgGT452 = new MSGGT452.Bff_MsgGT452();
         d_MsgGT452 = (MSGGT452.Bff_MsgGT452)g_MsgGT452.GT452_Tag.elementAt(i);
//fht         if (Tip.equals("2"))
//fht            { d_MsgGT452 = (MSGGT452.Bff_MsgGT452)h_Gbs.Gbs_Rgs.elementAt(i); }
//fht         else
//fht            { d_MsgGT452 = (MSGGT452.Bff_MsgGT452)v_Gbs.Gbs_Rgs.elementAt(i); }
         rg.MoverA(g_PrmPC080.PC080_Cli,     g_MsgGT450.GT450_Cli);
         rg.MoverA(g_PrmPC080.PC080_Ncl,     g_MsgGT450.GT450_Ncl);
         rg.MoverA(g_PrmPC080.PC080_Dir,     g_MsgGT450.GT450_Dir);
         rg.MoverA(g_PrmPC080.PC080_Tfc,     g_MsgGT450.GT450_Tfc);
         rg.MoverA(g_PrmPC080.PC080_Dcn,     g_MsgGT450.GT450_Dcn);
         rg.MoverA(g_PrmPC080.PC080_Ndc,     g_MsgGT450.GT450_Ndc);
         rg.MoverA(g_PrmPC080.PC080_Cnr.Sis, g_MsgGT450.GT450_Gti.toString().substring(0,3));
         rg.MoverA(g_PrmPC080.PC080_Cnr.Ncn, g_MsgGT450.GT450_Gti.toString().substring(3,10));
         rg.MoverA(g_PrmPC080.PC080_Ntc,     d_MsgGT452.GT452_Seq);
         rg.MoverA(g_PrmPC080.PC080_Dtt,     d_MsgGT452.GT452_Dsb);
         rg.MoverA(g_PrmPC080.PC080_Mnt,     d_MsgGT452.GT452_Vcn);
         rg.MoverA(g_PrmPC080.PC080_Rtn,     "RO");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Link_Program("Agtp451", "Appp096", data, g_Msg);
         return;
       }
    if (Opc.equals("E"))
       {
         //Ir al detalle de la consulta
//fht         String Tip = data.getParameters().getString("Tip", "");
         String Seq = data.getParameters().getString("Seq", "");
         i = Integer.parseInt(Seq);
         g_MsgGT450 = (MSGGT450.Buf_MsgGT450)data.getUser().getTemp("Agtp451_MsgGT450");
         g_MsgGT452 = (MSGGT452.Buf_MsgGT452)data.getUser().getTemp("Agtp451_MsgGT452");
//fht         h_Gbs      = (MSGGT452.Bfv_Gbs)data.getUser().getTemp("Agtp451_Hip");
//fht         v_Gbs      = (MSGGT452.Bfv_Gbs)data.getUser().getTemp("Agtp451_Gbs");
         MSGGT452.Bff_MsgGT452 d_MsgGT452 = new MSGGT452.Bff_MsgGT452();
         d_MsgGT452 = (MSGGT452.Bff_MsgGT452)g_MsgGT452.GT452_Tag.elementAt(i);
//fht         if (Tip.equals("2"))
//fht            { d_MsgGT452 = (MSGGT452.Bff_MsgGT452)h_Gbs.Gbs_Rgs.elementAt(i); }
//fht         else
//fht            { d_MsgGT452 = (MSGGT452.Bff_MsgGT452)v_Gbs.Gbs_Rgs.elementAt(i); }
         rg.MoverA(g_PrmPC080.PC080_Cli,     g_MsgGT450.GT450_Cli);
         rg.MoverA(g_PrmPC080.PC080_Ncl,     g_MsgGT450.GT450_Ncl);
         rg.MoverA(g_PrmPC080.PC080_Dir,     g_MsgGT450.GT450_Dir);
         rg.MoverA(g_PrmPC080.PC080_Tfc,     g_MsgGT450.GT450_Tfc);
         rg.MoverA(g_PrmPC080.PC080_Dcn,     g_MsgGT450.GT450_Dcn);
         rg.MoverA(g_PrmPC080.PC080_Ndc,     g_MsgGT450.GT450_Ndc);
         rg.MoverA(g_PrmPC080.PC080_Cnr.Sis, g_MsgGT450.GT450_Gti.toString().substring(0,3));
         rg.MoverA(g_PrmPC080.PC080_Cnr.Ncn, g_MsgGT450.GT450_Gti.toString().substring(3,10));
         rg.MoverA(g_PrmPC080.PC080_Ntc,     d_MsgGT452.GT452_Seq);
         rg.MoverA(g_PrmPC080.PC080_Dtt,     d_MsgGT452.GT452_Dsb);
         rg.MoverA(g_PrmPC080.PC080_Mnt,     d_MsgGT452.GT452_Vcn);
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         if (g_MsgGT450.getGgr().trim().equals("CRH")
          || g_MsgGT450.getGgr().trim().equals("HYP"))
            {
              if (d_MsgGT452.getItg().trim().equals("HP"))
                 BF_MSG.Link_Program("Agtp451", "Agtp411", data, g_Msg);
              else
                 BF_MSG.Link_Program("Agtp451", "Agtp412", data, g_Msg);
            }
         if (g_MsgGT450.getGgr().trim().equals("AYF"))
            { BF_MSG.Link_Program("Agtp451", "Agtp413", data, g_Msg); }
         if (g_MsgGT450.getGgr().trim().equals("CTW"))
            { BF_MSG.Link_Program("Agtp451", "Agtp414", data, g_Msg); }
         if (g_MsgGT450.getGgr().trim().equals("DMN")
          || g_MsgGT450.getGgr().trim().equals("DMX")
          || g_MsgGT450.getGgr().trim().equals("PMN")
          || g_MsgGT450.getGgr().trim().equals("PMX"))
            { BF_MSG.Link_Program("Agtp451", "Agtp415", data, g_Msg); }
         if (g_MsgGT450.getGgr().trim().equals("PAC"))
            { BF_MSG.Link_Program("Agtp451", "Agtp416", data, g_Msg); }
         if (g_MsgGT450.getGgr().trim().equals("GFU"))
            { BF_MSG.Link_Program("Agtp451", "Agtp418", data, g_Msg); }
         if (g_MsgGT450.getGgr().trim().equals("CMA"))
            { BF_MSG.Link_Program("Agtp451", "Agtp419", data, g_Msg); }
         if (g_MsgGT450.getGgr().trim().equals("MYC"))
            { BF_MSG.Link_Program("Agtp451", "Agtp455", data, g_Msg); }
         return;
       }
    if (Opc.trim().equals("L"))
       {
         //Boton Informe Legal
         g_MsgGT450 = (MSGGT450.Buf_MsgGT450)data.getUser().getTemp("Agtp451_MsgGT450");
         rg.MoverA(g_PrmPC080.PC080_Cli,     g_MsgGT450.GT450_Cli);
         rg.MoverA(g_PrmPC080.PC080_Ncl,     g_MsgGT450.GT450_Ncl);
         rg.MoverA(g_PrmPC080.PC080_Dir,     g_MsgGT450.GT450_Dir);
         rg.MoverA(g_PrmPC080.PC080_Tfc,     g_MsgGT450.GT450_Tfc);
         rg.MoverA(g_PrmPC080.PC080_Dcn,     g_MsgGT450.GT450_Dcn);
         rg.MoverA(g_PrmPC080.PC080_Ndc,     g_MsgGT450.GT450_Ndc);
         rg.MoverA(g_PrmPC080.PC080_Cnr.Sis, g_MsgGT450.GT450_Gti.toString().substring(0,3));
         rg.MoverA(g_PrmPC080.PC080_Cnr.Ncn, g_MsgGT450.GT450_Gti.toString().substring(3,10));
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Link_Program("Agtp451", "Agtp456", data, g_Msg);
         return;
       }
    if (Opc.equals("S"))
       {
         //Volver al menu de consultas
         data.getUser().removeTemp("Agtp451_PrmPC080");
         data.getUser().removeTemp("Agtp451_MsgGT450");
         data.getUser().removeTemp("Agtp451_MsgGT451");
         data.getUser().removeTemp("Agtp451_MsgGT452");
         data.getUser().removeTemp("Agtp451_MsgGT453");
//fht         data.getUser().removeTemp("Agtp451_Hip");
//fht         data.getUser().removeTemp("Agtp451_Gbs");
         rg.MoverA(g_PrmPC080.PC080_Rtn, " ");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp451", data, g_Msg);
         return;
       }
  }
  //===============================================================================================================================
  public void doAgtp451_Continue(RunData data, Context context) throws Exception
  {
    //Log.debug("AGTP451[doAgtp451_Continue.start]", "[" + data.getUser().getUserName() + "]");
    if (rg.FH_RTrim(g_Msg.getPgl()).equals("Appp030"))
       {
         g_PrmPP030 = PRMPP030.LSet_A_PrmPP030(g_Msg.getDat());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp451_g_PrmPC080");
         data.getUser().removeTemp("Agtp451_g_PrmPC080");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         if (g_PrmPP030.getRsp().trim().equals("TERMINAR"))
            {
              //Limpieza(data, context);
              rg.MoverA(g_PrmPC080.PC080_Rtn, "NK");
              rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
              BF_MSG.Return_Data("Agtp451", data, g_Msg);
              return;
            }
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP661.vm" );
         return;
       }
    if (RUTGEN.FH_RTrim(g_Msg.getPgl()).equals("Eclp090"))
       {
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp451_g_PrmPC080");
         data.getUser().removeTemp("Agtp451_g_PrmPC080");
         g_PrmCL090 = PRMCL090.LSet_A_PrmCL090(g_Msg.getDat());
         if (g_PrmCL090.getCli().trim().equals(""))
            {
              //Limpieza(data, context);
              rg.MoverA(g_PrmPC080.PC080_Rtn, "NK");
              rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
              BF_MSG.Return_Data("Agtp451", data, g_Msg);
              return;
            }
         rg.MoverA(g_PrmPC080.PC080_Cli, g_PrmCL090.CL090_Cli);
         rg.MoverA(g_PrmPC080.PC080_Ncl, g_PrmCL090.CL090_Ncl);
         rg.MoverA(g_PrmPC080.PC080_Tpr, g_PrmCL090.CL090_Tpr);
         rg.MoverA(g_PrmPC080.PC080_Acc, "SELECCIONA");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Link_Program("Agtp451", "Agtp400", data, g_Msg);
         return;
       }
    if (RUTGEN.FH_RTrim(g_Msg.getPgl()).equals("Agtp400"))
       {
         g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.getDat());
         if (g_PrmPC080.getRtn().trim().equals("NK"))
            {
              //Limpieza(data, context);
              rg.MoverA(g_PrmPC080.PC080_Rtn, "NK");
              rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
              BF_MSG.Return_Data("Agtp451", data, g_Msg);
              return;
            }
         rg.MoverA(g_PrmPC080.PC080_Rtn, "");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         doAgtp451_Init(data, context);
         return;
       }
    doAgtp451_Init(data, context);
  }
  //===============================================================================================================================
  public void Trae_Datos(RunData data, Context context) throws Exception
  {
    g_MsgGT450 = MSGGT450.Inicia_MsgGT450();
    rg.MoverA(g_MsgGT450.GT450_Idr, "I");
    rg.MoverA(g_MsgGT450.GT450_Gti, g_PrmPC080.getSis() + g_PrmPC080.getNcn());
    rg.MoverA(g_Msg.Msg_Dat, MSGGT450.LSet_De_MsgGT450(g_MsgGT450));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GT450");
    g_MsgGT450 = MSGGT450.LSet_A_MsgGT450(g_Msg.getDat());
    data.getUser().removeTemp("Agtp451_MsgGT450");
    data.getUser().setTemp("Agtp451_MsgGT450", g_MsgGT450);
    rg.MoverA(g_PrmPC080.PC080_Cnr.Sis, g_MsgGT450.GT450_Gti.toString().substring(0,3));
    rg.MoverA(g_PrmPC080.PC080_Cnr.Ncn, g_MsgGT450.GT450_Gti.toString().substring(3,10));
    rg.MoverA(g_PrmPC080.PC080_Cli, g_MsgGT450.GT450_Cli);
    rg.MoverA(g_PrmPC080.PC080_Ncl, g_MsgGT450.GT450_Ncl);
    rg.MoverA(g_PrmPC080.PC080_Dir, g_MsgGT450.GT450_Dir);
    rg.MoverA(g_PrmPC080.PC080_Tfc, g_MsgGT450.GT450_Tfc);
    Log.debug("AGTP451[Trae_Datos.sis]", "[" + g_PrmPC080.getSis() + "]");
  }
  //-------------------------------------------------------------------------------------------
  public void Poblar_Participes(RunData data, Context context) throws Exception
  {
    g_MsgGT451 = MSGGT451.Inicia_MsgGT451();
    rg.MoverA(g_MsgGT451.GT451_Idr, "I");
    Log.debug("AGTP451[Poblar_Participes.sis]", "[" + g_PrmPC080.getSis() + "]");
    rg.MoverA(g_MsgGT451.GT451_Gti, g_PrmPC080.getSis() + g_PrmPC080.getNcn());
    rg.MoverA(g_Msg.Msg_Dat, MSGGT451.LSet_De_MsgGT451(g_MsgGT451));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GT451");
    g_MsgGT451 = MSGGT451.LSet_A_MsgGT451(g_Msg.getDat());
    data.getUser().removeTemp("Agtp451_MsgGT451");
    data.getUser().setTemp("Agtp451_MsgGT451", g_MsgGT451);
  }
  //-------------------------------------------------------------------------------------------
  public void Poblar_CreEsp(RunData data, Context context) throws Exception
  {
    g_MsgGT453 = MSGGT453.Inicia_MsgGT453();
    rg.MoverA(g_MsgGT453.GT453_Idr, "I");
    rg.MoverA(g_MsgGT453.GT453_Gti, g_PrmPC080.getSis() + g_PrmPC080.getNcn());
    rg.MoverA(g_Msg.Msg_Dat, MSGGT453.LSet_De_MsgGT453(g_MsgGT453));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GT453");
    g_MsgGT453 = MSGGT453.LSet_A_MsgGT453(g_Msg.getDat());
    data.getUser().removeTemp("Agtp451_MsgGT453");
    data.getUser().setTemp("Agtp451_MsgGT453", g_MsgGT453);
  }
  //-------------------------------------------------------------------------------------------
  public void Poblar_Bienes(RunData data, Context context) throws Exception
  {
    g_MsgGT452 = MSGGT452.Inicia_MsgGT452();
    rg.MoverA(g_MsgGT452.GT452_Idr, "I");
    rg.MoverA(g_MsgGT452.GT452_Gti, g_PrmPC080.getSis() + g_PrmPC080.getNcn());
    rg.MoverA(g_Msg.Msg_Dat, MSGGT452.LSet_De_MsgGT452(g_MsgGT452));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GT452");
    g_MsgGT452 = MSGGT452.LSet_A_MsgGT452(g_Msg.getDat());
    data.getUser().removeTemp("Agtp451_MsgGT452");
    data.getUser().setTemp("Agtp451_MsgGT452", g_MsgGT452);
  }
  //-------------------------------------------------------------------------------------------
  public void fht_Poblar_Bienes(RunData data, Context context) throws Exception
  {
//fht    h_Gbs.Gbs_Rgs.clear();
//fht    v_Gbs.Gbs_Rgs.clear();
//fht    int p = 0;
//fht    g_MsgGT452 = MSGGT452.Inicia_MsgGT452();
//fht    rg.MoverA(g_MsgGT452.GT452_Idr, "I");
//fht    rg.MoverA(g_MsgGT452.GT452_Gti, g_PrmPC080.getSis() + g_PrmPC080.getNcn());
//fht    while (g_MsgGT452.getIdr().compareTo("L")!=0)
//fht          {
//fht            if (g_MsgGT452.getIdr().compareTo("I")!=0)
//fht               {
//fht                 Vector vTmp = g_MsgGT452.getTag();
//fht                 g_MsgGT452 = MSGGT452.Inicia_MsgGT452();
//fht                 rg.MoverA(g_MsgGT452.GT452_Idr, "S");
//fht                 rg.MoverA(g_MsgGT452.GT452_Gti, g_PrmPC080.getSis() + g_PrmPC080.getNcn());
//fht                 i = vTmp.size() - 1;
//fht                 MSGGT452.Bff_MsgGT452 d_Gbs = (MSGGT452.Bff_MsgGT452)vTmp.elementAt(i);
//fht                 g_MsgGT452.GT452_Tag.set(0, d_Gbs);
//fht               }
//fht            rg.MoverA(g_Msg.Msg_Dat, MSGGT452.LSet_De_MsgGT452(g_MsgGT452));
//fht            g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GT452");
//fht            g_MsgGT452 = MSGGT452.LSet_A_MsgGT452(g_Msg.getDat());
//fht            for (i=p; i<g_MsgGT452.GT452_Tag.size(); i++)
//fht                {
//fht                  MSGGT452.Bff_MsgGT452 d_Gbs = (MSGGT452.Bff_MsgGT452)g_MsgGT452.GT452_Tag.elementAt(i);
//fht                  if (d_Gbs.getItg().equals("HP"))
//fht                     { h_Gbs.Gbs_Rgs.add(d_Gbs); }
//fht                  else
//fht                     { v_Gbs.Gbs_Rgs.add(d_Gbs); }
//fht                }
//fht            p = 1;
//fht          }
//fht    if (v_Gbs.Gbs_Rgs.size()>0)
//fht       {
//fht         MSGGT452.Bff_MsgGT452 t_Gbs = new MSGGT452.Bff_MsgGT452();
//fht         rg.MoverA(t_Gbs.GT452_Seq, "999");
//fht         rg.MoverA(t_Gbs.GT452_Itg, "");
//fht         rg.MoverA(t_Gbs.GT452_Dsb, "Totales");
//fht         rg.MoverA(t_Gbs.GT452_Frv, "");
//fht         String lcVts = rg.Zeros(15);
//fht         String lcVcn = rg.Zeros(15);
//fht         String lcVlq = rg.Zeros(15);
//fht         String lcVrt = rg.Zeros(15);
//fht         String lcMve = rg.Zeros(15);
//fht         for (int i=0; i<v_Gbs.Gbs_Rgs.size(); i++)
//fht             {
//fht               MSGGT452.Bff_MsgGT452 dt_Gbs = (MSGGT452.Bff_MsgGT452)v_Gbs.Gbs_Rgs.elementAt(i);
//fht               if (dt_Gbs.getVts().trim().equals("")) { rg.MoverA(dt_Gbs.GT452_Vts, rg.Zeros(15)); }
//fht               if (dt_Gbs.getVcn().trim().equals("")) { rg.MoverA(dt_Gbs.GT452_Vcn, rg.Zeros(15)); }
//fht               if (dt_Gbs.getVlq().trim().equals("")) { rg.MoverA(dt_Gbs.GT452_Vlq, rg.Zeros(15)); }
//fht               if (dt_Gbs.getVrt().trim().equals("")) { rg.MoverA(dt_Gbs.GT452_Vrt, rg.Zeros(15)); }
//fht               if (dt_Gbs.getMve().trim().equals("")) { rg.MoverA(dt_Gbs.GT452_Mve, rg.Zeros(15)); }
//fht               lcVts = rg.Suma(lcVts,2,dt_Gbs.GT452_Vts,2,15,2);
//fht               lcVcn = rg.Suma(lcVcn,2,dt_Gbs.GT452_Vcn,2,15,2);
//fht               lcVlq = rg.Suma(lcVlq,2,dt_Gbs.GT452_Vlq,2,15,2);
//fht               lcVrt = rg.Suma(lcVrt,2,dt_Gbs.GT452_Vrt,2,15,2);
//fht               lcMve = rg.Suma(lcMve,2,dt_Gbs.GT452_Mve,2,15,2);
//fht             }
//fht         rg.MoverA(t_Gbs.GT452_Vts, lcVts);
//fht         rg.MoverA(t_Gbs.GT452_Vcn, lcVcn);
//fht         rg.MoverA(t_Gbs.GT452_Vlq, lcVlq);
//fht         rg.MoverA(t_Gbs.GT452_Vrt, lcVrt);
//fht         rg.MoverA(t_Gbs.GT452_Mve, lcMve);
//fht         v_Gbs.Gbs_Rgs.add(t_Gbs);
//fht       }
//fht    if (h_Gbs.Gbs_Rgs.size()>0)
//fht       {
//fht         MSGGT452.Bff_MsgGT452 t_Gbs = new MSGGT452.Bff_MsgGT452();
//fht         rg.MoverA(t_Gbs.GT452_Seq, "999");
//fht         rg.MoverA(t_Gbs.GT452_Itg, "");
//fht         rg.MoverA(t_Gbs.GT452_Dsb, "Totales");
//fht         rg.MoverA(t_Gbs.GT452_Frv, "");
//fht         String lcVts = rg.Zeros(15);
//fht         String lcVcn = rg.Zeros(15);
//fht         String lcVlq = rg.Zeros(15);
//fht         String lcVrt = rg.Zeros(15);
//fht         String lcMve = rg.Zeros(15);
//fht         for (int i=0; i<h_Gbs.Gbs_Rgs.size(); i++)
//fht             {
//fht               MSGGT452.Bff_MsgGT452 dt_Gbs = (MSGGT452.Bff_MsgGT452)h_Gbs.Gbs_Rgs.elementAt(i);
//fht               if (dt_Gbs.getVts().trim().equals("")) { rg.MoverA(dt_Gbs.GT452_Vts, rg.Zeros(15)); }
//fht               if (dt_Gbs.getVcn().trim().equals("")) { rg.MoverA(dt_Gbs.GT452_Vcn, rg.Zeros(15)); }
//fht               if (dt_Gbs.getVlq().trim().equals("")) { rg.MoverA(dt_Gbs.GT452_Vlq, rg.Zeros(15)); }
//fht               if (dt_Gbs.getVrt().trim().equals("")) { rg.MoverA(dt_Gbs.GT452_Vrt, rg.Zeros(15)); }
//fht               if (dt_Gbs.getMve().trim().equals("")) { rg.MoverA(dt_Gbs.GT452_Mve, rg.Zeros(15)); }
//fht               lcVts = rg.Suma(lcVts,2,dt_Gbs.GT452_Vts,2,15,2);
//fht               lcVcn = rg.Suma(lcVcn,2,dt_Gbs.GT452_Vcn,2,15,2);
//fht               lcVlq = rg.Suma(lcVlq,2,dt_Gbs.GT452_Vlq,2,15,2);
//fht               lcVrt = rg.Suma(lcVrt,2,dt_Gbs.GT452_Vrt,2,15,2);
//fht               lcMve = rg.Suma(lcMve,2,dt_Gbs.GT452_Mve,2,15,2);
//fht             }
//fht         rg.MoverA(t_Gbs.GT452_Vts, lcVts);
//fht         rg.MoverA(t_Gbs.GT452_Vcn, lcVcn);
//fht         rg.MoverA(t_Gbs.GT452_Vlq, lcVlq);
//fht         rg.MoverA(t_Gbs.GT452_Vrt, lcVrt);
//fht         rg.MoverA(t_Gbs.GT452_Mve, lcMve);
//fht         h_Gbs.Gbs_Rgs.add(t_Gbs);
//fht       }
//fht    data.getUser().removeTemp("Agtp451_MsgGT452");
//fht    data.getUser().setTemp("Agtp451_MsgGT452", g_MsgGT452);
//fht    data.getUser().removeTemp("Agtp451_Gbs");
//fht    data.getUser().setTemp("Agtp451_Gbs", v_Gbs);
//fht    data.getUser().removeTemp("Agtp451_Hip");
//fht    data.getUser().setTemp("Agtp451_Hip", h_Gbs);
  }
  //===============================================================================================================================
}