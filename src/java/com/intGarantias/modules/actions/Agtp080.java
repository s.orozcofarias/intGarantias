// Source File Name:   Agtp080.java
// Descripcion     :   Menu Ejecutivo Comercial [Fte: EJE] (-)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.PRMPC080;

import com.intGlobal.modules.global.PRMCL090;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp080 extends FHTServletAction
{
  //===============================================================================================================================
  public Vector vDatos;
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  PRMPC080.PrmPC080 g_PrmPC080     = new PRMPC080.PrmPC080();
  PRMCL090.Buf_PrmCL090 g_PrmCL090 = new PRMCL090.Buf_PrmCL090();
  //-------------------------------------------------------------------------------------------
  public Agtp080()
  {
  }
  //===============================================================================================================================
  public void doPerform(RunData data, Context context) throws Exception
  {
    //Log.debug("AGTP080[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp080-DIN", data);
    if (g_Msg.getPgr().equals("RT"))
       {
         if (RUTGEN.FH_RTrim(g_Msg.getPgl()).equals("Appp030"))
            { doAgtp080_Init(data, context); }
         else
            { doAgtp080_Continue(data, context); }
       }
    else
       { doAgtp080_Init(data, context); }
  }
  //-------------------------------------------------------------------------------------------
  public void doAgtp080_Init(RunData data, Context context) throws Exception
  {
    //Log.debug("AGTP080[doAgtp080_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp080-MAN", data);
    Nueva_PrmPC080(data);
    BF_MSG.Param_Program(data, g_Msg);
    setTemplate(data, "Garantias,Agt,AGTP080.vm");
  }
  //===============================================================================================================================
  public void doAgtp080(RunData data, Context context) throws Exception
  {
  //Log.debug("AGTP080[doAgtp080.start]", "[" + data.getUser().getUserName() + "]");
  //String lc_MsgTot = (String)data.getUser().getTemp("g_Msg", "");
  //if (lc_MsgTot.trim().equals("") || lc_MsgTot.substring(150).trim().equals("BICE"))
  //   {
         g_Msg = BF_MSG.InitIntegra("Agtp080", data, context);
         Nueva_PrmPC080(data);
         BF_MSG.Param_Program(data, g_Msg);
  //   }
  //else
  //   { g_Msg = BF_MSG.Init_Program(data); }
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.getDat());
    String Opc = data.getParameters().getString("Opc", "");
    String Cli = data.getParameters().getString("Cli", "");
    String Ncn = data.getParameters().getString("Ncn", "");
    if (Opc.trim().equals("S"))
       {
         //Boton Salir
         BF_MSG.Return_Program(data, g_Msg);
         return;
       }
    if (Opc.equals("7"))
       {
         //Garant�as en Tramitaci�n
         rg.MoverA(g_PrmPC080.PC080_Cnr.Sis, "GAR");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Link_Program("Agtp080", "Aedp102", data, g_Msg);
         return;
       }
    if (Opc.equals("24"))
       {
         //Contratos y Mandatos en Tramitaci�n
         rg.MoverA(g_PrmPC080.PC080_Cnr.Sis, "MYC");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Link_Program("Agtp080", "Aedp102", data, g_Msg);
         return;
       }
    if (Opc.equals("8"))
       {
         //Agenda de Novedades
         BF_MSG.Link_Program("Agtp080", "Agtp500", data, g_Msg);
         return;
       }
    if ((Opc.equals("1") || Opc.equals("21")) && !Cli.trim().equals(""))
       {
         //Apertura con RUT Valido
         String lcPgm = "";
         if (Opc.equals("1"))
            {
              rg.MoverA(g_PrmPC080.PC080_Cnr.Sis, "GAR");
              lcPgm = "Agtp610";
            }
         else
            {
              rg.MoverA(g_PrmPC080.PC080_Cnr.Sis, "MYC");
              lcPgm = "Agtp611";
            }
         rg.MoverA(g_PrmPC080.PC080_Cli, Cli);
         rg.MoverA(g_PrmPC080.PC080_Acc, "APERT");
         rg.MoverA(g_PrmPC080.PC080_Ttr, "APERT");
         rg.MoverA(g_PrmPC080.PC080_Ntt, "CONSTITUCION DE GARANTIA");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Link_Program("Agtp080", lcPgm, data, g_Msg);
         return;
       }
    if ((Opc.equals("1") || Opc.equals("21")) || Ncn.trim().equals(""))
       {
         rg.MoverA(g_PrmPC080.PC080_Opc, Opc);
         data.getUser().removeTemp("Agtp080_g_PrmPC080");
         data.getUser().setTemp("Agtp080_g_PrmPC080" ,PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         g_PrmCL090 = PRMCL090.Inicia_PrmCL090();
         rg.MoverA(g_PrmCL090.CL090_Pcc, "N");
         rg.MoverA(g_Msg.Msg_Dat, PRMCL090.LSet_De_PrmCL090(g_PrmCL090));
         BF_MSG.Link_Program("Agtp080", "Eclp090", data, g_Msg);
         return;
       }
    else
       {
         rg.MoverA(g_PrmPC080.PC080_Cnr.Ncn, Ncn);
         doAgtp080_Despacha(data, context, Opc);
       }
  }
  //===============================================================================================================================
  public void doAgtp080_Continue(RunData data, Context context) throws Exception
  {
    if (RUTGEN.FH_RTrim(g_Msg.getPgl()).equals("Eclp090"))
       {
         PRMPC080 vPrmPC080 = new PRMPC080();
         vDatos = vPrmPC080.LSet_A_vPrmPC080((String)data.getUser().getTemp("Agtp080_g_PrmPC080"));
         g_PrmPC080 = (PRMPC080.PrmPC080)vDatos.elementAt(0);
         data.getUser().removeTemp("Agtp080_g_PrmPC080");
         g_PrmCL090 = PRMCL090.LSet_A_PrmCL090(g_Msg.getDat());
         if (g_PrmCL090.CL090_Cli.toString().trim().equals(""))
            {
              doAgtp080_Init(data, context);
              return;
            }
         rg.MoverA(g_PrmPC080.PC080_Cli, g_PrmCL090.CL090_Cli);
         rg.MoverA(g_PrmPC080.PC080_Ncl, g_PrmCL090.CL090_Ncl);
         rg.MoverA(g_PrmPC080.PC080_Tpr, g_PrmCL090.CL090_Tpr);
         if (g_PrmPC080.PC080_Opc.toString().trim().equals("1"))
            {
              rg.MoverA(g_PrmPC080.PC080_Cnr.Sis, "GAR");
              rg.MoverA(g_PrmPC080.PC080_Acc, "APERT");
              rg.MoverA(g_PrmPC080.PC080_Ttr, "APERT");
              rg.MoverA(g_PrmPC080.PC080_Ntt, "CONSTITUCION DE GARANTIA");
              rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
              BF_MSG.Link_Program("Agtp080", "Agtp610", data, g_Msg);
            }
         else if (g_PrmPC080.PC080_Opc.toString().trim().equals("21"))
            {
              rg.MoverA(g_PrmPC080.PC080_Cnr.Sis, "MYC");
              rg.MoverA(g_PrmPC080.PC080_Acc, "APERT");
              rg.MoverA(g_PrmPC080.PC080_Ttr, "APERT");
              rg.MoverA(g_PrmPC080.PC080_Ntt, "CONSTITUCION DE GARANTIA");
              rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
              BF_MSG.Link_Program("Agtp080", "Agtp611", data, g_Msg);
            }
         else
            {
              rg.MoverA(g_PrmPC080.PC080_Acc, "SELECCIONA");
              rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
              BF_MSG.Link_Program("Agtp080", "Agtp400", data, g_Msg);
            }
         return;
       }
    if (RUTGEN.FH_RTrim(g_Msg.getPgl()).equals("Agtp400"))
       {
         g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.getDat());
         if (g_PrmPC080.PC080_Rtn.toString().trim().equals("NK"))
            { doAgtp080_Init(data, context); }
         else
            { doAgtp080_Despacha(data, context, g_PrmPC080.PC080_Opc.toString().trim()); }
         return;
       }
    doAgtp080_Init(data, context);
  }
  //===============================================================================================================================
  public void doAgtp080_Despacha(RunData data, Context context, String Opc) throws Exception
  {
    rg.MoverA(g_PrmPC080.PC080_Rtn, " ");
    String lcPrograma = "NO_" + Opc;
    if (Opc.equals("11"))
       {
         //Addendum de Garant�a
         rg.MoverA(g_PrmPC080.PC080_Acc, "APERT");
         rg.MoverA(g_PrmPC080.PC080_Ttr, "ADDEM");
         rg.MoverA(g_PrmPC080.PC080_Ntt, "ADDENDUM DE GARANTIA");
         lcPrograma = "Agtp115";
       }
    if (Opc.equals("2"))
       {
         //Estado de Avance de Obras
         rg.MoverA(g_PrmPC080.PC080_Acc, "APERT");
         rg.MoverA(g_PrmPC080.PC080_Ttr, "RETPI");
         rg.MoverA(g_PrmPC080.PC080_Ntt, "ESTADO DE AVANCE DE OBRAS");
         lcPrograma = "Agtp116";
       }
    if (Opc.equals("3"))
       {
         //Retasaci�n de Garant�a
         rg.MoverA(g_PrmPC080.PC080_Acc, "APERT");
         rg.MoverA(g_PrmPC080.PC080_Ttr, "RETAS");
         rg.MoverA(g_PrmPC080.PC080_Ntt, "RETASACION DE GARANTIA");
         lcPrograma = "Agtp115";
       }
    if (Opc.equals("4"))
       {
         //Terminaci�n de Proyecto
         rg.MoverA(g_PrmPC080.PC080_Acc, "APERT");
         rg.MoverA(g_PrmPC080.PC080_Ttr, "TERPI");
         rg.MoverA(g_PrmPC080.PC080_Ntt, "TERMINACION DE PROYECTO");
         lcPrograma = "Agtp116";
       }
    if (Opc.equals("5"))
       {
         //Alzamiento de Garant�a
         rg.MoverA(g_PrmPC080.PC080_Acc, "APERT");
         rg.MoverA(g_PrmPC080.PC080_Ttr, "ALZMT");
         rg.MoverA(g_PrmPC080.PC080_Ntt, "ALZAMIENTO DE GARANTIA");
         lcPrograma = "Agtp115";
       }
    if (Opc.equals("6"))
       {
         //Ventas Proyecto Inmobiliario
         rg.MoverA(g_PrmPC080.PC080_Acc, "APERT");
         rg.MoverA(g_PrmPC080.PC080_Ttr, "ALZPI");
         rg.MoverA(g_PrmPC080.PC080_Ntt, "VENTA PROYECTO INMOBILIARIO");
         lcPrograma = "Agtp115";
       }
    if (Opc.equals("9"))
       {
         //Detalle de Garant�a Espec�fica
         lcPrograma = "Agtp451";
       }
    if (Opc.equals("10"))
       {
         //Cartola Garant�a Espec�fica
         lcPrograma = "Aicp430";
       }
    if (Opc.equals("22"))
       {
         //Modificacion de Mandatos y Contratos
         rg.MoverA(g_PrmPC080.PC080_Cnr.Sis, "MYC");
         rg.MoverA(g_PrmPC080.PC080_Acc, "APERT");
         rg.MoverA(g_PrmPC080.PC080_Ttr, "MODMC");
         rg.MoverA(g_PrmPC080.PC080_Ntt, "MODIFICACION MYC [MODMC]");
         lcPrograma = "Agtp115";
       }
    if (Opc.equals("23"))
       {
         //Alzamiento de Mandatos y Contratos
         rg.MoverA(g_PrmPC080.PC080_Cnr.Sis, "MYC");
         rg.MoverA(g_PrmPC080.PC080_Acc, "APERT");
         rg.MoverA(g_PrmPC080.PC080_Ttr, "ALZMT");
         rg.MoverA(g_PrmPC080.PC080_Ntt, "ALZAMIENTO DE GARANTIA");
         lcPrograma = "Agtp115";
       }
    rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
    BF_MSG.Link_Program("Agtp080", lcPrograma, data, g_Msg);
  }
  //-------------------------------------------------------------------------------------------
  public void Nueva_PrmPC080(RunData data)
  {
    PRMPC080 pPrmPC080 = new PRMPC080(g_Msg.getFch(), "EJE", "Agtp80", g_Msg.getCus(), g_Msg.getNus(), g_Msg.getFus());
    data.getUser().removeTemp("pPrmPC080");
    data.getUser().setTemp("pPrmPC080", pPrmPC080);
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    g_PrmPC080 = PRMPC080.Inicia_PrmPC080();
    rg.MoverA(g_PrmPC080.PC080_Fte,     "EJE");
    rg.MoverA(g_PrmPC080.PC080_Pai,     "CL");
    rg.MoverA(g_PrmPC080.PC080_Cnr.Sis, "GAR");
    rg.MoverA(g_PrmPC080.PC080_Slr.Slc, rg.Zeros(7));
    rg.MoverA(g_PrmPC080.PC080_Suc,     g_Msg.Msg_Suc);
    rg.MoverA(g_PrmPC080.PC080_Nsu,     g_Msg.Msg_Nsu);
    rg.MoverA(g_PrmPC080.PC080_Mnd,     "000");
    rg.MoverA(g_PrmPC080.PC080_Nmn,     "PESOS CHILE");
    rg.MoverA(g_PrmPC080.PC080_Eje,     g_Msg.Msg_Cus);
    rg.MoverA(g_PrmPC080.PC080_Nej,     g_Msg.Msg_Nus);
    rg.MoverA(g_PrmPC080.PC080_Tej,     g_Msg.Msg_Fus);
    rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
  }
  //===============================================================================================================================
}