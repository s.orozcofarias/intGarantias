// Source File Name:   Agtp116.java

package com.intGarantias.modules.actions;

import java.util.Vector;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.BF_EVT;
import com.FHTServlet.modules.global.BF_IMG;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.PRMPC080;
import com.FHTServlet.modules.global.RUTGEN;
import com.intGarantias.modules.global.GT_IMG;
import com.intGarantias.modules.global.MSGGT435;
import com.intGarantias.modules.global.MSGGT436;
import com.intGlobal.modules.global.MSGED090;
import com.intGlobal.modules.global.MSGED100;
import com.intGlobal.modules.global.MSGED135;
import com.intGlobal.modules.global.PRMCL090;

public class Agtp116 extends FHTServletAction {

	public Agtp116() {
		i = 0;
		lcData = "";
		rg = new RUTGEN();
		msg = new BF_MSG();
		g_Msg = new com.FHTServlet.modules.global.BF_MSG.Buf_Msg();
		g_Evt = new com.FHTServlet.modules.global.BF_EVT.Buf_Evt();
		img = new BF_IMG();
		g_Img = new com.FHTServlet.modules.global.BF_IMG.Buf_Img();
		g_Img_Base = new com.FHTServlet.modules.global.BF_IMG.Buf_Img_Bse();
		gtimg = new GT_IMG();
		g_Img_GtApe = new com.intGarantias.modules.global.GT_IMG.Buf_Img_GtApe();
		g_Img_GtTsd = new com.intGarantias.modules.global.GT_IMG.Buf_Img_GtTsd();
		g_PrmPC080 = new com.FHTServlet.modules.global.PRMPC080.PrmPC080();
		g_MsgED090 = new com.intGlobal.modules.global.MSGED090.Buf_MsgED090();
		g_MsgED100 = new com.intGlobal.modules.global.MSGED100.Buf_MsgED100();
		g_MsgED135 = new com.intGlobal.modules.global.MSGED135.Buf_MsgED135();
		g_PrmCL090 = new com.intGlobal.modules.global.PRMCL090.Buf_PrmCL090();
		g_MsgGT435 = new com.intGarantias.modules.global.MSGGT435.Buf_MsgGT435();
		g_MsgGT436 = new com.intGarantias.modules.global.MSGGT436.Buf_MsgGT436();
	}

	public void doPerform(RunData data, Context context) throws Exception {
		g_Msg = BF_MSG.Init_Program("Agtp116-DIN", data);
		if (g_Msg.Msg_Pgr.toString().equals("RT"))
			doAgtp116_Continue(data, context);
		else
			doAgtp116_Init(data, context);
	}

	public void doCancel(RunData data, Context context) throws Exception {
		setTemplate(data, "Garantias,Agt,AGTP116.vm");
	}

	public void doAgtp116_Init(RunData data, Context context) throws Exception {
		g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
		if (g_PrmPC080.PC080_Ttr.toString().trim().equals("RETPI")) {
			RUTGEN.MoverA(g_PrmPC080.PC080_Acc, "APERT");
			RUTGEN.MoverA(g_PrmPC080.PC080_Ttr, "RETPI");
			RUTGEN.MoverA(g_PrmPC080.PC080_Ntt, "ESTADO DE AVANCE DE OBRAS");
		}
		if (g_PrmPC080.PC080_Ttr.toString().trim().equals("TERPI")) {
			RUTGEN.MoverA(g_PrmPC080.PC080_Acc, "APERT");
			RUTGEN.MoverA(g_PrmPC080.PC080_Ttr, "TERPI");
			RUTGEN.MoverA(g_PrmPC080.PC080_Ntt, "TERMINACION DE PROYECTO");
		}
		if (g_PrmPC080.PC080_Cnr.Ncn.toString().trim().equals("")
				&& g_PrmPC080.PC080_Cli.toString().trim().equals("")) {
			data.getUser().removeTemp("Agtp116_g_PrmPC080");
			data.getUser().setTemp("Agtp116_g_PrmPC080", g_PrmPC080);
			g_PrmCL090 = PRMCL090.Inicia_PrmCL090();
			RUTGEN.MoverA(g_PrmCL090.CL090_Pcc, "N");
			RUTGEN.MoverA(g_Msg.Msg_Dat, PRMCL090.LSet_De_PrmCL090(g_PrmCL090));
			BF_MSG.Link_Program("Agtp116", "Eclp090", data, g_Msg);
			return;
		}
		if (g_PrmPC080.PC080_Ntr.toString().trim().equals(""))
			RUTGEN.MoverA(g_PrmPC080.PC080_Ntr,
					Asigna_Folio("TRN-NTR", data, context));
		if (g_PrmPC080.PC080_Cnr.Ncn.toString().trim().equals(""))
			RUTGEN.MoverA(g_PrmPC080.PC080_Cnr.Ncn,
					Asigna_Folio("GRT-NCN", data, context));
		if (g_PrmPC080.PC080_Acc.toString().trim().equals("APERT")) {
			if (Carga_Nueva(data, context) == 1)
				return;
		} else {
			Carga_Inicial(data, context);
		}
		RUTGEN.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
		BF_MSG.Param_Program(data, g_Msg);
		data.getUser().removeTemp("Agtp116_PrmPC080");
		data.getUser().setTemp("Agtp116_PrmPC080", g_PrmPC080);
		setTemplate(data, "Garantias,Agt,AGTP116.vm");
	}

	public void doAgtp116(RunData data, Context context) throws Exception {
		g_Msg = BF_MSG.Init_Program("Agtp116-MAN", data);
		g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
		String Opc = data.getParameters().getString("Opc", "");
		if (Opc.trim().equals("S")) {
			data.getUser().removeTemp("Agtp116_Evt");
			data.getUser().removeTemp("Agtp116_MsgGT436");
			data.getUser().removeTemp("Agtp116_ImgBase");
			data.getUser().removeTemp("Agtp116_ImgGtApe");
			data.getUser().removeTemp("Agtp116_PrmPC080");
			RUTGEN.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
			BF_MSG.Return_Program(data, g_Msg);
			return;
		}
		if (Opc.trim().equals("E")) {
			String Seq = data.getParameters().getString("Seq", "");
			i = Integer.parseInt(Seq);
			g_Evt = (com.FHTServlet.modules.global.BF_EVT.Buf_Evt) data
					.getUser().getTemp("Agtp116_Evt");
			g_MsgGT436 = (com.intGarantias.modules.global.MSGGT436.Buf_MsgGT436) data
					.getUser().getTemp("Agtp116_MsgGT436");
			g_Img_Base = (com.FHTServlet.modules.global.BF_IMG.Buf_Img_Bse) data
					.getUser().getTemp("Agtp116_ImgBase");
			g_Img_GtApe = (com.intGarantias.modules.global.GT_IMG.Buf_Img_GtApe) data
					.getUser().getTemp("Agtp116_ImgGtApe");
			Vector vRgs = g_MsgGT436.getRgs();
			com.intGarantias.modules.global.MSGGT436.Buf_Gbs dBuf_Gbs = (com.intGarantias.modules.global.MSGGT436.Buf_Gbs) vRgs
					.elementAt(i);
			RUTGEN.MoverA(g_Img_GtApe.GtApe_Psq, dBuf_Gbs.Gbs_Seq);
			RUTGEN.MoverA(g_Img_GtApe.GtApe_Psd, dBuf_Gbs.Ghd_Sqd);
			RUTGEN.MoverA(g_Img_GtApe.GtApe_Psw, "M");
			Acepta_Evento(data, context);
			return;
		}
		if (Opc.trim().equals("N")) {
			g_Evt = (com.FHTServlet.modules.global.BF_EVT.Buf_Evt) data
					.getUser().getTemp("Agtp116_Evt");
			g_MsgGT436 = (com.intGarantias.modules.global.MSGGT436.Buf_MsgGT436) data
					.getUser().getTemp("Agtp116_MsgGT436");
			g_Img_Base = (com.FHTServlet.modules.global.BF_IMG.Buf_Img_Bse) data
					.getUser().getTemp("Agtp116_ImgBase");
			g_Img_GtApe = (com.intGarantias.modules.global.GT_IMG.Buf_Img_GtApe) data
					.getUser().getTemp("Agtp116_ImgGtApe");
			i = Integer.parseInt(g_MsgGT436.GT436_Nro.toString());
			if (i > 0) {
				Vector vRgs = g_MsgGT436.getRgs();
				com.intGarantias.modules.global.MSGGT436.Buf_Gbs dBuf_Gbs = (com.intGarantias.modules.global.MSGGT436.Buf_Gbs) vRgs
						.elementAt(i - 1);
				RUTGEN.MoverA(g_Img_GtApe.GtApe_Psq,
						RUTGEN.Suma(dBuf_Gbs.Gbs_Seq, 0, "1", 0, 3, 0));
			} else {
				RUTGEN.MoverA(g_Img_GtApe.GtApe_Psq, "001");
			}
			RUTGEN.MoverA(g_Img_GtApe.GtApe_Psd, "001");
			RUTGEN.MoverA(g_Img_GtApe.GtApe_Psw, "N");
			Acepta_Evento(data, context);
			return;
		} else {
			return;
		}
	}

	public void doAgtp116_Continue(RunData data, Context context)
			throws Exception {
		if (RUTGEN.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Eclp090")) {
			g_PrmPC080 = (com.FHTServlet.modules.global.PRMPC080.PrmPC080) data
					.getUser().getTemp("Agtp116_g_PrmPC080");
			data.getUser().removeTemp("Agtp116_g_PrmPC080");
			g_PrmCL090 = PRMCL090.LSet_A_PrmCL090(g_Msg.Msg_Dat.toString());
			if (g_PrmCL090.CL090_Cli.toString().trim().equals("")) {
				RUTGEN.MoverA(g_PrmPC080.PC080_Rtn, "NK");
				RUTGEN.MoverA(g_Msg.Msg_Dat,
						PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
				BF_MSG.Return_Data("Agtp116", data, g_Msg);
				return;
			} else {
				RUTGEN.MoverA(g_PrmPC080.PC080_Cli, g_PrmCL090.CL090_Cli);
				RUTGEN.MoverA(g_PrmPC080.PC080_Ncl, g_PrmCL090.CL090_Ncl);
				RUTGEN.MoverA(g_PrmPC080.PC080_Tpr, g_PrmCL090.CL090_Tpr);
				RUTGEN.MoverA(g_PrmPC080.PC080_Acc, "SELECCIONA");
				RUTGEN.MoverA(g_Msg.Msg_Dat,
						PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
				BF_MSG.Link_Program("Agtp116", "Agtp400", data, g_Msg);
				return;
			}
		}
		if (RUTGEN.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Agtp400")) {
			g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
			if (g_PrmPC080.PC080_Rtn.toString().trim().equals("NK")) {
				RUTGEN.MoverA(g_PrmPC080.PC080_Rtn, "NK");
				RUTGEN.MoverA(g_Msg.Msg_Dat,
						PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
				BF_MSG.Return_Data("Agtp116", data, g_Msg);
				return;
			} else {
				RUTGEN.MoverA(g_Msg.Msg_Dat,
						PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
				BF_MSG.Param_Program(data, g_Msg);
				doAgtp116_Init(data, context);
				return;
			}
		}
		if (RUTGEN.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Appp030")) {
			BF_MSG.Return_Program(data, g_Msg);
			return;
		}
		if (RUTGEN.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Aedp120")) {
			BF_MSG.Return_Program(data, g_Msg);
			return;
		}
		if (RUTGEN.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Agtp200")) {
			BF_MSG.Return_Program(data, g_Msg);
			return;
		} else {
			return;
		}
	}

	public void Acepta_Evento(RunData data, Context context) throws Exception {
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Bse,
				BF_IMG.LSet_De_ImgBase_Bse(g_Img_Base));
		Vector g_Tab_Img = A_Tab_Img(data);
		img.Graba_Img_Host(g_Msg, g_Tab_Img, data, context);
		RUTGEN.MoverA(g_Evt.Evt_Est, "NUEVA");
		BF_EVT t = new BF_EVT();
		g_MsgED100 = MSGED100.Inicia_MsgED100();
		if (g_PrmPC080.PC080_Acc.toString().trim().equals("APERT"))
			RUTGEN.MoverA(g_MsgED100.ED100_Idr, "C");
		if (g_PrmPC080.PC080_Acc.toString().trim().equals("APERM"))
			RUTGEN.MoverA(g_MsgED100.ED100_Idr, "M");
		if (g_PrmPC080.PC080_Acc.toString().trim().equals("ELIMI"))
			RUTGEN.MoverA(g_MsgED100.ED100_Idr, "D");
		RUTGEN.MoverA(g_MsgED100.ED100_Nev, g_PrmPC080.PC080_Ntr);
		RUTGEN.MoverA(g_MsgED100.ED100_Evt, t.LSet_De_Evt(g_Evt));
		RUTGEN.MoverA(g_Msg.Msg_Dat, MSGED100.LSet_De_MsgED100(g_MsgED100));
		g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED100");
		RUTGEN.MoverA(g_PrmPC080.PC080_Rtn, "OK");
		RUTGEN.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
		data.getUser().removeTemp("Agtp116_Evt");
		data.getUser().removeTemp("Agtp116_MsgGT436");
		data.getUser().removeTemp("Agtp116_ImgBase");
		data.getUser().removeTemp("Agtp116_ImgGtApe");
		data.getUser().removeTemp("Agtp116_PrmPC080");
		RUTGEN.MoverA(g_PrmPC080.PC080_Rtn, "");
		RUTGEN.MoverA(g_PrmPC080.PC080_Mnt, g_Evt.Evt_Vpr);
		RUTGEN.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
		if (g_PrmPC080.PC080_Fte.toString().trim().equals("TSD")) {
			g_MsgED135 = MSGED135.Inicia_MsgED135();
			RUTGEN.MoverA(g_MsgED135.ED135_Idr, "M");
			RUTGEN.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
			RUTGEN.MoverA(g_MsgED135.ED135_Trt, "TASAC");
			RUTGEN.MoverA(g_MsgED135.ED135_Est, "");
			RUTGEN.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
			RUTGEN.MoverA(g_MsgED135.ED135_Ezt, "RGTSD");
			RUTGEN.MoverA(g_MsgED135.ED135_Exi, "TSD");
			RUTGEN.MoverA(g_MsgED135.ED135_Exn,
					RUTGEN.FmtValor(g_Msg.Msg_Cus, 0, 0, 12, "+"));
			RUTGEN.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
			g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
			RUTGEN.MoverA(g_PrmPC080.PC080_Rtn, "");
			RUTGEN.MoverA(g_PrmPC080.PC080_Mnt, g_Evt.Evt_Vpr);
			RUTGEN.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
			BF_MSG.Link_Program("Agtp116", "Agtp200", data, g_Msg);
		} else {
			BF_MSG.Link_Program("Agtp116", "Aedp120", data, g_Msg);
		}
	}

	public String Asigna_Folio(String pcFolio, RunData data, Context context)
			throws Exception {
		g_MsgED090 = MSGED090.Inicia_MsgED090();
		RUTGEN.MoverA(g_MsgED090.ED090_Idr, "F");
		RUTGEN.MoverA(g_MsgED090.ED090_Cod, pcFolio);
		RUTGEN.MoverA(g_Msg.Msg_Dat, MSGED090.LSet_De_MsgED090(g_MsgED090));
		g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED090");
		g_MsgED090 = MSGED090.LSet_A_MsgED090(g_Msg.Msg_Dat.toString());
		String lcFolio = g_MsgED090.ED090_Fol.toString();
		return lcFolio;
	}

	public void Carga_Inicial(RunData data, Context context) throws Exception {
		g_MsgED100 = MSGED100.Inicia_MsgED100();
		RUTGEN.MoverA(g_MsgED100.ED100_Idr, "Q");
		RUTGEN.MoverA(g_MsgED100.ED100_Nev, g_PrmPC080.PC080_Ntr);
		RUTGEN.MoverA(g_Msg.Msg_Dat, MSGED100.LSet_De_MsgED100(g_MsgED100));
		g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED100");
		g_MsgED100 = MSGED100.LSet_A_MsgED100(g_Msg.Msg_Dat.toString());
		g_Evt = BF_EVT.LSet_A_Evt(g_MsgED100.ED100_Evt.toString());
	}

	public int Carga_Nueva(RunData data, Context context) throws Exception {
		lcData = g_PrmPC080.PC080_Cnr.Sis.toString()
				+ g_PrmPC080.PC080_Cnr.Ncn.toString();
		g_MsgGT435 = MSGGT435.Inicia_MsgGT435();
		RUTGEN.MoverA(g_MsgGT435.GT435_Idr, "I");
		RUTGEN.MoverA(g_MsgGT435.GT435_Gti, lcData);
		RUTGEN.MoverA(g_MsgGT435.GT435_Ttr, g_PrmPC080.PC080_Ttr);
		RUTGEN.MoverA(g_MsgGT435.GT435_Fte, g_PrmPC080.PC080_Fte);
		RUTGEN.MoverA(g_Msg.Msg_Dat, MSGGT435.LSet_De_MsgGT435(g_MsgGT435));
		g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GT435");
		g_MsgGT435 = MSGGT435.LSet_A_MsgGT435(g_Msg.Msg_Dat.toString());
		if (g_MsgGT435.GT435_Swt.toString().compareTo("0") != 0) {
			String lcMensaje = "";
			if (g_MsgGT435.GT435_Swt.toString().equals("1"))
				lcMensaje = "Garantia Erronea (Sin CLI-GTI)";
			if (g_MsgGT435.GT435_Swt.toString().equals("2"))
				lcMensaje = "Garantia Erronea (Sin CLI)";
			if (g_MsgGT435.GT435_Swt.toString().equals("5"))
				lcMensaje = "Numero Garantia No Existe";
			if (g_MsgGT435.GT435_Swt.toString().equals("6"))
				lcMensaje = "Evento No Valido para Garantia";
			if (g_MsgGT435.GT435_Swt.toString().equals("7"))
				lcMensaje = "Garantia NO Esta Vigente";
			if (g_MsgGT435.GT435_Swt.toString().equals("8"))
				lcMensaje = "Garantia NO pertenece al ITO";
			if (g_MsgGT435.GT435_Swt.toString().equals("9"))
				lcMensaje = "Garantia Tiene Evento en Tramite";
			BF_MSG.MsgInt("Agtp116", data, g_Msg, lcMensaje, "1");
			return 1;
		} else {
			lcData = g_PrmPC080.PC080_Cnr.Sis.toString()
					+ g_PrmPC080.PC080_Cnr.Ncn.toString();
			g_MsgGT436 = MSGGT436.Inicia_MsgGT436();
			RUTGEN.MoverA(g_MsgGT436.GT436_Idr, "I");
			RUTGEN.MoverA(g_MsgGT436.GT436_Gti, lcData);
			RUTGEN.MoverA(g_Msg.Msg_Dat, MSGGT436.LSet_De_MsgGT436(g_MsgGT436));
			g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GT436");
			g_MsgGT436 = MSGGT436.LSet_A_MsgGT436(g_Msg.Msg_Dat.toString());
			data.getUser().removeTemp("Agtp116_MsgGT436");
			data.getUser().setTemp("Agtp116_MsgGT436", g_MsgGT436);
			RUTGEN.MoverA(g_PrmPC080.PC080_Cli, g_MsgGT435.GT435_Cli);
			RUTGEN.MoverA(g_PrmPC080.PC080_Ncl, g_MsgGT435.GT435_Ncl);
			RUTGEN.MoverA(g_PrmPC080.PC080_Dir, g_MsgGT435.GT435_Dir);
			g_MsgED100 = MSGED100.Inicia_MsgED100();
			RUTGEN.MoverA(g_MsgED100.ED100_Nev, g_PrmPC080.PC080_Ntr);
			g_Evt = BF_EVT.LSet_A_Evt(g_MsgED100.ED100_Evt.toString());
			RUTGEN.MoverA(g_Evt.Evt_Ttr, g_PrmPC080.PC080_Ttr);
			RUTGEN.MoverA(g_Evt.Evt_Ntt, g_PrmPC080.PC080_Ntt);
			RUTGEN.MoverA(g_Evt.Evt_Tsl, g_PrmPC080.PC080_Slr.Tsl);
			RUTGEN.MoverA(g_Evt.Evt_Slc, g_PrmPC080.PC080_Slr.Slc);
			RUTGEN.MoverA(g_Evt.Evt_Cli, g_MsgGT435.GT435_Cli);
			RUTGEN.MoverA(g_Evt.Evt_Ncl, g_MsgGT435.GT435_Ncl);
			RUTGEN.MoverA(g_Evt.Evt_Tpr, g_MsgGT435.GT435_Tpr);
			RUTGEN.MoverA(g_Evt.Evt_Dir, g_MsgGT435.GT435_Dir);
			RUTGEN.MoverA(g_Evt.Evt_Cmn, g_MsgGT435.GT435_Cmn);
			RUTGEN.MoverA(g_Evt.Evt_Tfc, g_MsgGT435.GT435_Tfc);
			RUTGEN.MoverA(g_Evt.Evt_Sis, g_PrmPC080.PC080_Cnr.Sis);
			RUTGEN.MoverA(g_Evt.Evt_Ncn, g_PrmPC080.PC080_Cnr.Ncn);
			RUTGEN.MoverA(g_Evt.Evt_Dcn, g_MsgGT435.GT435_Dcn);
			RUTGEN.MoverA(g_Evt.Evt_Ndc, g_MsgGT435.GT435_Ndc);
			RUTGEN.MoverA(g_Evt.Evt_Tmn, g_MsgGT435.GT435_Tmn);
			RUTGEN.MoverA(g_Evt.Evt_Trj, g_MsgGT435.GT435_Trj);
			RUTGEN.MoverA(g_Evt.Evt_Dcm, g_MsgGT435.GT435_Dcm);
			RUTGEN.MoverA(g_Evt.Evt_Prp, "BANCO");
			RUTGEN.MoverA(g_Evt.Evt_Suc, g_Msg.Msg_Suc);
			RUTGEN.MoverA(g_Evt.Evt_Nsu, g_Msg.Msg_Nsu);
			RUTGEN.MoverA(g_Evt.Evt_Mnd, g_MsgGT435.GT435_Mnd);
			RUTGEN.MoverA(g_Evt.Evt_Nmn, g_MsgGT435.GT435_Nmn);
			RUTGEN.MoverA(g_Evt.Evt_Eje, g_Msg.Msg_Cus);
			RUTGEN.MoverA(g_Evt.Evt_Nej, g_Msg.Msg_Nus);
			RUTGEN.MoverA(g_Evt.Evt_Tej, g_Msg.Msg_Fus);
			RUTGEN.MoverA(g_Evt.Evt_Est, "NUEVA");
			RUTGEN.MoverA(g_Evt.Evt_Fst, g_Msg.Msg_Fch);
			RUTGEN.MoverA(g_Evt.Evt_Upr, g_MsgGT435.GT435_Trj);
			RUTGEN.MoverA(g_Evt.Evt_Vpr,
					RUTGEN.FmtValor(g_MsgGT435.GT435_Vcn, 2, 4, 15, "+"));
			RUTGEN.MoverA(g_Evt.Evt_Fpr, g_Msg.Msg_Fch);
			RUTGEN.MoverA(g_Evt.Evt_Spr,
					RUTGEN.FmtValor(g_MsgGT435.GT435_Vcn, 2, 4, 15, "+"));
			RUTGEN.MoverA(g_Evt.Evt_Tpp, " ");
			RUTGEN.MoverA(g_Evt.Evt_Ggr, g_MsgGT435.GT435_Ggr);
			RUTGEN.MoverA(g_Evt.Evt_Fll, " ");
			data.getUser().removeTemp("Agtp116_Evt");
			data.getUser().setTemp("Agtp116_Evt", g_Evt);
			Inicializa_Img(data, context);
			return 0;
		}
	}

	public void Inicializa_Img(RunData data, Context context) throws Exception {
		RUTGEN.MoverA(g_Img_Base.Img_Base_Ntr, g_PrmPC080.PC080_Ntr);
		RUTGEN.MoverA(g_Img_Base.Img_Base_Ttr, g_Evt.Evt_Ttr);
		RUTGEN.MoverA(g_Img_Base.Img_Base_Ntt, g_Evt.Evt_Ntt);
		RUTGEN.MoverA(g_Img_Base.Img_Base_Tsl, g_Evt.Evt_Tsl);
		RUTGEN.MoverA(g_Img_Base.Img_Base_Slc, g_Evt.Evt_Slc);
		RUTGEN.MoverA(g_Img_Base.Img_Base_Cmt, " ");
		RUTGEN.MoverA(g_Img_Base.Img_Base_Cli, g_Evt.Evt_Cli);
		RUTGEN.MoverA(g_Img_Base.Img_Base_Ncl, g_Evt.Evt_Ncl);
		RUTGEN.MoverA(g_Img_Base.Img_Base_Tpr, g_Evt.Evt_Tpr);
		RUTGEN.MoverA(g_Img_Base.Img_Base_Dir, g_Evt.Evt_Dir);
		RUTGEN.MoverA(g_Img_Base.Img_Base_Tfc, g_Evt.Evt_Tfc);
		RUTGEN.MoverA(g_Img_Base.Img_Base_Sis, g_Evt.Evt_Sis);
		RUTGEN.MoverA(g_Img_Base.Img_Base_Ncn, g_Evt.Evt_Ncn);
		RUTGEN.MoverA(g_Img_Base.Img_Base_Dcn, g_Evt.Evt_Dcn);
		RUTGEN.MoverA(g_Img_Base.Img_Base_Ndc, g_Evt.Evt_Ndc);
		RUTGEN.MoverA(g_Img_Base.Img_Base_Tmn, g_Evt.Evt_Tmn);
		RUTGEN.MoverA(g_Img_Base.Img_Base_Trj, g_Evt.Evt_Trj);
		RUTGEN.MoverA(g_Img_Base.Img_Base_Dcm, g_Evt.Evt_Dcm);
		RUTGEN.MoverA(g_Img_Base.Img_Base_Prp, g_Evt.Evt_Prp);
		RUTGEN.MoverA(g_Img_Base.Img_Base_Suc, g_Evt.Evt_Suc);
		RUTGEN.MoverA(g_Img_Base.Img_Base_Nsu, g_Evt.Evt_Nsu);
		RUTGEN.MoverA(g_Img_Base.Img_Base_Mnd, g_Evt.Evt_Mnd);
		RUTGEN.MoverA(g_Img_Base.Img_Base_Nmn, g_Evt.Evt_Nmn);
		RUTGEN.MoverA(g_Img_Base.Img_Base_Eje, g_Evt.Evt_Eje);
		RUTGEN.MoverA(g_Img_Base.Img_Base_Nej, g_Evt.Evt_Nej);
		RUTGEN.MoverA(g_Img_Base.Img_Base_Tej, g_Evt.Evt_Tej);
		data.getUser().removeTemp("Agtp116_ImgBase");
		data.getUser().setTemp("Agtp116_ImgBase", g_Img_Base);
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Fpr, g_Msg.Msg_Fch);
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Rgf, "");
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Gfu, "");
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Ftc, g_Msg.Msg_Fch);
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Cbp, " ");
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Dbp,
				RUTGEN.Zeros(g_Img_GtApe.GtApe_Dbp));
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Tob, g_MsgGT435.GT435_Cbt);
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Plm, "N");
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Ggr, g_MsgGT435.GT435_Ggr);
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Dsg, g_MsgGT435.GT435_Dsg);
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Cbt, g_MsgGT435.GT435_Cbt);
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Grd, "1");
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Cmp, "N");
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Sgr, "?");
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Ctb, "?");
		if (g_Img_GtApe.getPlm().equals("N"))
			RUTGEN.MoverA(g_Img_GtApe.GtApe_Ilm, "N");
		else
			RUTGEN.MoverA(g_Img_GtApe.GtApe_Ilm, "?");
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Pcl,
				RUTGEN.Zeros(g_Img_GtApe.GtApe_Pcl));
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Mtl,
				RUTGEN.Zeros(g_Img_GtApe.GtApe_Mtl));
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Fig, g_Msg.Msg_Fch);
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Tsd, " ");
		if (g_PrmPC080.PC080_Fte.toString().trim().equals("TSD"))
			RUTGEN.MoverA(g_Img_GtApe.GtApe_Tsd,
					RUTGEN.FmtValor(g_Msg.Msg_Cus, 0, 0, 3, "+"));
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Fts, " ");
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Vuo,
				RUTGEN.Zeros(g_Img_GtApe.GtApe_Vuo));
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Vtt,
				RUTGEN.Zeros(g_Img_GtApe.GtApe_Vtt));
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Vts,
				RUTGEN.Zeros(g_Img_GtApe.GtApe_Vts));
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Vtb,
				RUTGEN.Zeros(g_Img_GtApe.GtApe_Vtb));
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Vcn, g_MsgGT435.GT435_Vcn);
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Cbg, " ");
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Nct, " ");
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Fct, " ");
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Fvt, " ");
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Vuf,
				RUTGEN.Zeros(g_Img_GtApe.GtApe_Vuf));
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Vus,
				RUTGEN.Zeros(g_Img_GtApe.GtApe_Vus));
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Hvt,
				RUTGEN.Zeros(g_Img_GtApe.GtApe_Hvt));
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Hvl,
				RUTGEN.Zeros(g_Img_GtApe.GtApe_Hvl));
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Hvb,
				RUTGEN.Zeros(g_Img_GtApe.GtApe_Hvb));
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Hvc,
				RUTGEN.Zeros(g_Img_GtApe.GtApe_Hvc));
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Pvt,
				RUTGEN.Zeros(g_Img_GtApe.GtApe_Pvt));
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Pvl,
				RUTGEN.Zeros(g_Img_GtApe.GtApe_Pvl));
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Pvb,
				RUTGEN.Zeros(g_Img_GtApe.GtApe_Pvb));
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Pvc,
				RUTGEN.Zeros(g_Img_GtApe.GtApe_Pvc));
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Psw, " ");
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Psq, " ");
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Psd, " ");
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Idx, " ");
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Tfr, " ");
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Dpv, " ");
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Dpp, " ");
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Dsw, " ");
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Pct, " ");
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Fll, " ");
		data.getUser().removeTemp("Agtp116_ImgGtApe");
		data.getUser().setTemp("Agtp116_ImgGtApe", g_Img_GtApe);
	}

	public Vector A_Tab_Img(RunData data) {
		Vector v_Img = new Vector();
		v_Img.clear();
		RUTGEN.MoverA(g_Img.Img_Ntr, g_PrmPC080.PC080_Ntr);
		RUTGEN.MoverA(g_Img.Img_Dax, "BASE");
		RUTGEN.MoverA(g_Img.Img_Seq, "000");
		RUTGEN.MoverA(g_Img.Img_Dat, GT_IMG.LSet_De_ImgGtApe(g_Img_GtApe));
		Vector vPaso = BF_IMG.LSet_A_vImg(BF_IMG.LSet_De_Img(g_Img));
		v_Img.add((com.FHTServlet.modules.global.BF_IMG.Buf_Img) vPaso
				.elementAt(0));
		if (g_PrmPC080.PC080_Fte.toString().trim().equals("TSD")) {
			RUTGEN.MoverA(g_Img.Img_Dat, "");
			g_Img_GtTsd = GT_IMG.LSet_A_ImgGtTsd(g_Img.Img_Dat.toString());
			RUTGEN.MoverA(g_Img_GtTsd.GtTsd_Tsd,
					RUTGEN.FmtValor(g_Msg.Msg_Cus, 0, 0, 3, "+"));
			RUTGEN.MoverA(g_Img_GtTsd.GtTsd_Nts, g_Msg.Msg_Nus);
			RUTGEN.MoverA(g_Img.Img_Dax, "XTSD");
			RUTGEN.MoverA(g_Img.Img_Seq, "000");
			RUTGEN.MoverA(g_Img.Img_Dat, GT_IMG.LSet_De_ImgGtTsd(g_Img_GtTsd));
			vPaso = BF_IMG.LSet_A_vImg(BF_IMG.LSet_De_Img(g_Img));
			v_Img.add((com.FHTServlet.modules.global.BF_IMG.Buf_Img) vPaso
					.elementAt(0));
		}
		return v_Img;
	}

	public int i;
	public String lcData;
	RUTGEN rg;
	BF_MSG msg;
	com.FHTServlet.modules.global.BF_MSG.Buf_Msg g_Msg;
	com.FHTServlet.modules.global.BF_EVT.Buf_Evt g_Evt;
	BF_IMG img;
	com.FHTServlet.modules.global.BF_IMG.Buf_Img g_Img;
	com.FHTServlet.modules.global.BF_IMG.Buf_Img_Bse g_Img_Base;
	GT_IMG gtimg;
	com.intGarantias.modules.global.GT_IMG.Buf_Img_GtApe g_Img_GtApe;
	com.intGarantias.modules.global.GT_IMG.Buf_Img_GtTsd g_Img_GtTsd;
	com.FHTServlet.modules.global.PRMPC080.PrmPC080 g_PrmPC080;
	com.intGlobal.modules.global.MSGED090.Buf_MsgED090 g_MsgED090;
	com.intGlobal.modules.global.MSGED100.Buf_MsgED100 g_MsgED100;
	com.intGlobal.modules.global.MSGED135.Buf_MsgED135 g_MsgED135;
	com.intGlobal.modules.global.PRMCL090.Buf_PrmCL090 g_PrmCL090;
	com.intGarantias.modules.global.MSGGT435.Buf_MsgGT435 g_MsgGT435;
	com.intGarantias.modules.global.MSGGT436.Buf_MsgGT436 g_MsgGT436;
}
