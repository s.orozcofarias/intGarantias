// Source File Name:   Agtp223.java
// Descripcion     :   Cambio de Ejecutivo (Todas) (ED125, ED135)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.BF_EVT;
import com.FHTServlet.modules.global.PRMPC080;

import com.intGlobal.modules.global.MSGED100;
import com.intGlobal.modules.global.MSGED102;
import com.intGlobal.modules.global.PRMTG116;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp223 extends FHTServletAction
{
  //---------------------------------------------------------------------------------------
  public String Tabla = "";
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  BF_EVT.Buf_Evt g_Evt             = new BF_EVT.Buf_Evt();
  PRMPC080.PrmPC080 g_PrmPC080     = new PRMPC080.PrmPC080();
  MSGED100.Buf_MsgED100 g_MsgED100 = new MSGED100.Buf_MsgED100();
  MSGED102.Buf_MsgED102 g_MsgED102 = new MSGED102.Buf_MsgED102();
  MSGED102.Bfv_MsgED102 v_Evt      = new MSGED102.Bfv_MsgED102();
  PRMTG116.Buf_PrmTG116 g_PrmTG116 = new PRMTG116.Buf_PrmTG116();
  //---------------------------------------------------------------------------------------
  public Agtp223()
  {
  }
  //---------------------------------------------------------------------------------------
  public void doPerform(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP223[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp223-DIN", data);
    if (g_Msg.Msg_Pgr.toString().equals("RT"))
       { doAgtp223_Continue(data, context); }
    else
       { doAgtp223_Init(data, context); }
  }
  //---------------------------------------------------------------------------------------
  public void doCancel(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP223[doCancel.start]", "[CANCELADO]");
    setTemplate(data, "Garantias,Agt,AGTP223.vm");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp223_Init(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP223[doAgtp223_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    Carga_Inicial(data, context);
    rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
    BF_MSG.Param_Program(data, g_Msg);
    setTemplate(data, "Garantias,Agt,AGTP223.vm" );
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp223(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP223[doAgtp223.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp223-MAN", data);
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    String Opc = data.getParameters().getString("Opc", "");
    if (Opc.trim().equals("S"))
       {
         //Boton Salir
         data.getUser().removeTemp("Agtp223_PrmPC080");
         data.getUser().removeTemp("Agtp223_Evt");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "NK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp223", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("A"))
       {
         //Boton Aceptar
         Tabla = data.getParameters().getString("Vec", "");
         v_Evt = (MSGED102.Bfv_MsgED102)data.getUser().getTemp("Agtp223_Evt");
         for (int i=0; i<v_Evt.ED102_Rgs.size(); i++)
             {
               MSGED102.Tab_Evt d_Evt = (MSGED102.Tab_Evt)v_Evt.ED102_Rgs.elementAt(i);
               if (Tabla.substring(i,i+1).equals("S"))
                  { 
                    g_MsgED100 = MSGED100.Inicia_MsgED100();
                    g_Evt = BF_EVT.LSet_A_Evt(g_MsgED100.getEvt());
                    rg.MoverA(g_Evt.Evt_Eje, g_PrmPC080.PC080_Eje);
                    rg.MoverA(g_Evt.Evt_Nej, g_PrmPC080.PC080_Nej);
                    rg.MoverA(g_Evt.Evt_Tej, g_PrmPC080.PC080_Tej);
                    Cursatura(data, context, d_Evt.getNev()); 
                  }
             }
         data.getUser().removeTemp("Agtp223_PrmPC080");
         data.getUser().removeTemp("Agtp223_Evt");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp223", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("1"))
       {
         //Consulta Ejecutivo
         data.getUser().removeTemp("Agtp223_g_PrmPC080");
         data.getUser().setTemp("Agtp223_g_PrmPC080", g_PrmPC080);
         g_PrmTG116 = PRMTG116.Inicia_PrmTG116();
         rg.MoverA(g_Msg.Msg_Dat, PRMTG116.LSet_De_PrmTG116(g_PrmTG116));
         BF_MSG.Link_Program("Agtp223", "Atgp116", data, g_Msg);
         return;
       }
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp223_Continue(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP223[doAgtp223_Continue.start]", "[" + data.getUser().getUserName() + "]");
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Atgp116"))
       {
         g_PrmTG116 = PRMTG116.LSet_A_PrmTG116(g_Msg.Msg_Dat.toString());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp223_g_PrmPC080");
         data.getUser().removeTemp("Agtp223_g_PrmPC080");
         if (g_PrmTG116.TG116_Eje.toString().trim().compareTo("")!=0)
             {
               rg.MoverA(g_PrmPC080.PC080_Eje, g_PrmTG116.TG116_Eje);
               rg.MoverA(g_PrmPC080.PC080_Nej, g_PrmTG116.TG116_Nej);
               rg.MoverA(g_PrmPC080.PC080_Tej, g_PrmTG116.TG116_Tej);
               data.getUser().removeTemp("Agtp223_PrmPC080");
               data.getUser().setTemp("Agtp223_PrmPC080", g_PrmPC080);
             }
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP223.vm" );
         return;
       }
  }
  //---------------------------------------------------------------------------------------
  public void Carga_Inicial(RunData data, Context context)
              throws Exception 
  {
    boolean lVez = false;
    int i = 0;
    int p = 0;
    v_Evt.ED102_Rgs.clear();
    g_MsgED102 = MSGED102.Inicia_MsgED102();
    rg.MoverA(g_MsgED102.ED102_Idr, "I");
    rg.MoverA(g_MsgED102.ED102_Tpr, "T");
    rg.MoverA(g_MsgED102.ED102_Sis, g_PrmPC080.PC080_Cnr.Sis);
    rg.MoverA(g_MsgED102.ED102_Eje, g_PrmPC080.PC080_Eje);
    while (g_MsgED102.ED102_Idr.toString().compareTo("L")!=0)
          {
            if (g_MsgED102.ED102_Idr.toString().compareTo("I")!=0)
               {
                 Vector vTmp = g_MsgED102.getRgs();
                 g_MsgED102 = MSGED102.Inicia_MsgED102();
                 rg.MoverA(g_MsgED102.ED102_Idr, "N");
                 rg.MoverA(g_MsgED102.ED102_Tpr, "T");
                 rg.MoverA(g_MsgED102.ED102_Sis, g_PrmPC080.PC080_Cnr.Sis);
                 rg.MoverA(g_MsgED102.ED102_Eje, g_PrmPC080.PC080_Eje);
                 i = vTmp.size() - 1;
                 MSGED102.Tab_Evt d_Evt = (MSGED102.Tab_Evt)vTmp.elementAt(i);
                 g_MsgED102.ED102_Rgs.set(0, d_Evt);
                 lVez = true;
               }
            rg.MoverA(g_Msg.Msg_Dat, MSGED102.LSet_De_MsgED102(g_MsgED102));
            g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED102");
            g_MsgED102 = MSGED102.LSet_A_MsgED102(g_Msg.Msg_Dat.toString());
            if (g_MsgED102.ED102_Nro.toString().trim().equals("")) break;
            if (lVez)
               { p = 1; }
            for (i=p; i<g_MsgED102.ED102_Rgs.size(); i++)
                {
                  MSGED102.Tab_Evt d_Evt = (MSGED102.Tab_Evt)g_MsgED102.ED102_Rgs.elementAt(i);
                  if (!d_Evt.Evt_Nev.toString().trim().equals(""))
                     { v_Evt.ED102_Rgs.add(d_Evt); }
                }
            if (g_MsgED102.ED102_Idr.toString().equals("F")
             && Integer.parseInt(g_MsgED102.ED102_Nro.toString())<MSGED102.g_Max_ED102) break;
          }
    data.getUser().removeTemp("Agtp223_Evt");
    data.getUser().setTemp("Agtp223_Evt", v_Evt);
    data.getUser().removeTemp("Agtp223_PrmPC080");
    data.getUser().setTemp("Agtp223_PrmPC080", g_PrmPC080);
  }
  //---------------------------------------------------------------------------------------
  public void Cursatura(RunData data, Context context, String Nev)
              throws Exception 
  {
//    g_Evt = (BF_EVT.Buf_Evt)data.getUser().getTemp("Agtp223_Evt");
//    rg.MoverA(g_Evt.Evt_Fll, g_PrmPC080.PC080_Trt+"ENEJH");
    g_MsgED100 = MSGED100.Inicia_MsgED100();
    rg.MoverA(g_MsgED100.ED100_Idr, "T");
    rg.MoverA(g_MsgED100.ED100_Nev, Nev);
    BF_EVT t = new BF_EVT();
    rg.MoverA(g_MsgED100.ED100_Evt, t.LSet_De_Evt(g_Evt));
    rg.MoverA(g_Msg.Msg_Dat, MSGED100.LSet_De_MsgED100(g_MsgED100));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED100");
  }
  //---------------------------------------------------------------------------------------
}