// Source File Name:   Agtp414.java
// Descripcion     :   Consulta Detalle Warrant (GT414)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.PRMPC080;

import com.intGarantias.modules.global.MSGGT414;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp414 extends FHTServletAction
{
  //---------------------------------------------------------------------------------------
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  PRMPC080.PrmPC080 g_PrmPC080     = new PRMPC080.PrmPC080();
  MSGGT414.Buf_MsgGT414 g_MsgGT414 = new MSGGT414.Buf_MsgGT414();
  //---------------------------------------------------------------------------------------
  public Agtp414()
  {
  }
  //---------------------------------------------------------------------------------------
  public void doPerform(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP414[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp414-DIN", data);
    doAgtp414_Init(data, context);
  }
  //---------------------------------------------------------------------------------------
  public void doCancel(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP414[doCancel.start]", "[CANCELADO]");
    setTemplate(data, "Garantias,Agt,AGTP414.vm");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp414_Init(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP414[doAgtp414_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    g_MsgGT414 = MSGGT414.Inicia_MsgGT414();
    rg.MoverA(g_MsgGT414.GT414_Idr, "I");
    rg.MoverA(g_MsgGT414.GT414_Gti, g_PrmPC080.PC080_Cnr.Sis.toString() + g_PrmPC080.PC080_Cnr.Ncn.toString());
    rg.MoverA(g_MsgGT414.GT414_Seq, g_PrmPC080.PC080_Ntc);
    rg.MoverA(g_Msg.Msg_Dat, MSGGT414.LSet_De_MsgGT414(g_MsgGT414));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GT414");
    g_MsgGT414 = MSGGT414.LSet_A_MsgGT414(g_Msg.Msg_Dat.toString());
    data.getUser().removeTemp("Agtp414_MsgGT414");
    data.getUser().setTemp("Agtp414_MsgGT414", g_MsgGT414);
    data.getUser().removeTemp("Agtp414_PrmPC080");
    data.getUser().setTemp("Agtp414_PrmPC080", g_PrmPC080);					
    setTemplate(data, "Garantias,Agt,AGTP414.vm" );
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp414(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP414[doAgtp414.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp414-MAN", data);
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    String Opc = data.getParameters().getString("Opc" ,"");
    if (Opc.trim().equals("S"))
       {
       //Boton Salir
         data.getUser().removeTemp("Agtp414_MsgGT414");
         data.getUser().removeTemp("Agtp414_PrmPC080");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Program(data, g_Msg);
         return;
       }
  }
  //---------------------------------------------------------------------------------------
}