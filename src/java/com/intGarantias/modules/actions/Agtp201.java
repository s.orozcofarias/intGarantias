// Source File Name:   Agtp201.java

package com.intGarantias.modules.actions;

import java.util.Vector;

import org.apache.turbine.om.peer.BasePeer;
import org.apache.turbine.services.resources.TurbineResources;
import org.apache.turbine.util.Log;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.PRMPC080;
import com.FHTServlet.modules.global.PRMPP077;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.util.Constants;
import com.intGarantias.modules.global.MSGGT201;
import com.intGlobal.modules.global.MSGED135;
import com.intGlobal.modules.global.PRMED210;

import java.security.*;
import javax.crypto.Cipher; 
import javax.crypto.spec.SecretKeySpec;
import sun.misc.*;
public class Agtp201 extends FHTServletAction {

	private static final String RETPI = "RETPI";
	private static final String TERPI = "TERPI";
	private static final String ALGORITHM = "AES";
	public Agtp201() {
		i = 0;
		lcRetorno = "";
		rg = new RUTGEN();
		msg = new BF_MSG();
		g_Msg = new com.FHTServlet.modules.global.BF_MSG.Buf_Msg();
		g_PrmPC080 = new com.FHTServlet.modules.global.PRMPC080.PrmPC080();
		g_PrmPP077 = new com.FHTServlet.modules.global.PRMPP077.Buf_PrmPP077();
		g_MsgED135 = new com.intGlobal.modules.global.MSGED135.Buf_MsgED135();
		g_MsgGT201 = new com.intGarantias.modules.global.MSGGT201.Buf_MsgGT201();
		g_PrmED210 = new com.intGlobal.modules.global.PRMED210.Buf_PrmED210();
	}

	public void doPerform(RunData data, Context context) throws Exception {
		g_Msg = BF_MSG.Init_Program("Agtp201-DIN", data);
		if (g_Msg.Msg_Pgr.toString().equals("RT"))
			doAgtp201_Continue(data, context);
		else
			doAgtp201_Init(data, context);
	}

	public void doAgtp201_Init(RunData data, Context context) throws Exception {
		g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
		g_PrmPP077 = PRMPP077.Inicia_PrmPP077();
		RUTGEN.MoverA(g_PrmPP077.PP077_Swe, "U");
		RUTGEN.MoverA(g_PrmPP077.PP077_Sis, "GAR");
		data.getUser().removeTemp("Agtp201_g_PrmPC080");
		data.getUser().setTemp("Agtp201_g_PrmPC080", g_PrmPC080);
		RUTGEN.MoverA(g_Msg.Msg_Dat, PRMPP077.LSet_De_PrmPP077(g_PrmPP077));
		BF_MSG.Link_Program("Agtp201", "Appp077", data, g_Msg);
		
		
		
	}

	public void doAgtp201(RunData data, Context context) throws Exception {
		g_Msg = BF_MSG.Init_Program("Agtp201-MAN", data);
		g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
		String Opc = data.getParameters().getString("Opc");
		if (Opc.equals("S")) {
			data.getUser().removeTemp("Agtp201_Msg");
			data.getUser().removeTemp("Agtp201_MsgGT201");
			BF_MSG.Return_Program(data, g_Msg);
			return;
		}
		g_MsgGT201 = (com.intGarantias.modules.global.MSGGT201.Buf_MsgGT201) data
				.getUser().getTemp("Agtp201_MsgGT201");
		com.intGarantias.modules.global.MSGGT201.Bff_MsgGT201 d_MsgGT201 = new com.intGarantias.modules.global.MSGGT201.Bff_MsgGT201();
		if (Opc.equals("Pasivo")) {
			String lcTpr = g_MsgGT201.GT201_Tpr.toString();
			String lcTsd = g_MsgGT201.GT201_Tsd.toString();
			String lcSuc = g_MsgGT201.GT201_Suc.toString();
			String lcEje = g_MsgGT201.GT201_Eje.toString();
			String lcNcn = g_MsgGT201.GT201_Ncn.toString();
			String lcCli = g_MsgGT201.GT201_Cli.toString();
			g_MsgGT201 = MSGGT201.Inicia_MsgGT201();
			RUTGEN.MoverA(g_MsgGT201.GT201_Idr, "I");
			if (lcTpr.equals("A")) {
				RUTGEN.MoverA(g_MsgGT201.GT201_Tpr, "P");
			} else {
				RUTGEN.MoverA(g_MsgGT201.GT201_Tpr, "A");
			}
			RUTGEN.MoverA(g_MsgGT201.GT201_Tsd, lcTsd);
			RUTGEN.MoverA(g_MsgGT201.GT201_Suc, lcSuc);
			RUTGEN.MoverA(g_MsgGT201.GT201_Eje, lcEje);
			RUTGEN.MoverA(g_MsgGT201.GT201_Ncn, lcNcn);
			RUTGEN.MoverA(g_MsgGT201.GT201_Cli, lcCli);
			Poblar_Panel(data, context, "I");
			setTemplate(data, "Garantias,Agt,AGTP201.vm");
			return;
		}
		if (Opc.equals("B")) {
			String Seq = data.getParameters().getString("Seq", "");
			i = Integer.parseInt(Seq);
			d_MsgGT201 = (com.intGarantias.modules.global.MSGGT201.Bff_MsgGT201) g_MsgGT201.GT201_Tab
					.elementAt(i);
			g_MsgED135 = MSGED135.Inicia_MsgED135();
			RUTGEN.MoverA(g_MsgED135.ED135_Idr, "C");
			RUTGEN.MoverA(g_MsgED135.ED135_Nev, d_MsgGT201.Tmt_Nev);
			RUTGEN.MoverA(g_MsgED135.ED135_Trt, d_MsgGT201.Tmt_Trt);
			RUTGEN.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
			g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
			g_MsgED135 = MSGED135.LSet_A_MsgED135(g_Msg.Msg_Dat.toString());
			g_PrmED210 = PRMED210.Inicia_PrmED210();
			RUTGEN.MoverA(g_PrmED210.ED210_Rtn, "RO");
			RUTGEN.MoverA(g_PrmED210.ED210_Ldt, g_MsgED135.ED135_Ldv);
			data.getUser().removeTemp("Agtp201_g_PrmPC080");
			data.getUser().setTemp("Agtp201_g_PrmPC080", g_PrmPC080);
			RUTGEN.MoverA(g_Msg.Msg_Dat, PRMED210.LSet_De_PrmED210(g_PrmED210));
			BF_MSG.Link_Program("Agtp201", "Aedp210", data, g_Msg);
			return;
		}
		if (Opc.equals("A")) {
			String lcPrograma = "";
			String Seq = data.getParameters().getString("Seq", "");
			i = Integer.parseInt(Seq);
			d_MsgGT201 = (com.intGarantias.modules.global.MSGGT201.Bff_MsgGT201) g_MsgGT201.GT201_Tab
					.elementAt(i);
			if (g_MsgGT201.GT201_Tpr.toString().equals("A")) {
				RUTGEN.MoverA(g_PrmPC080.PC080_Rtn, "WR");
			} else {
				RUTGEN.MoverA(g_PrmPC080.PC080_Rtn, "RO");
			}
			RUTGEN.MoverA(g_PrmPC080.PC080_Cli, d_MsgGT201.Evt_Cli);
			RUTGEN.MoverA(g_PrmPC080.PC080_Ncl, d_MsgGT201.Evt_Ncl);
			RUTGEN.MoverA(g_PrmPC080.PC080_Cnr.Sis, d_MsgGT201.Evt_Sis);
			RUTGEN.MoverA(g_PrmPC080.PC080_Cnr.Ncn, d_MsgGT201.Evt_Ncn);
			RUTGEN.MoverA(g_PrmPC080.PC080_Dcn, d_MsgGT201.Evt_Dcn);
			RUTGEN.MoverA(g_PrmPC080.PC080_Ndc, d_MsgGT201.Evt_Dsg);
			RUTGEN.MoverA(g_PrmPC080.PC080_Ntr, d_MsgGT201.Tmt_Nev);
			RUTGEN.MoverA(g_PrmPC080.PC080_Ttr, d_MsgGT201.Evt_Ttr);
			RUTGEN.MoverA(g_PrmPC080.PC080_Trt, d_MsgGT201.Tmt_Trt);
			RUTGEN.MoverA(g_PrmPC080.PC080_Mnd, d_MsgGT201.Evt_Mnd);
			RUTGEN.MoverA(g_PrmPC080.PC080_Mnt, d_MsgGT201.Evt_Vle);
			RUTGEN.MoverA(g_PrmPC080.PC080_Trj, d_MsgGT201.Evt_Trj);
			RUTGEN.MoverA(g_PrmPC080.PC080_Dcm, d_MsgGT201.Evt_Dcm);
			RUTGEN.MoverA(g_PrmPC080.PC080_Ggr, d_MsgGT201.Evt_Ggr);
			if (d_MsgGT201.Tmt_Est.toString().equals("TMTSD")
					|| d_MsgGT201.Tmt_Est.toString().equals("DVVSD")) {
				lcPrograma = RUTGEN.NormaPgm(d_MsgGT201.Tmt_Pgm);
			} else {
				lcPrograma = "Agtp214";
			}
			Log.debug("AGTP200 valor de g_MsgGT201.GT201_Tpr  ", g_MsgGT201.GT201_Tpr.toString());
			Log.debug("AGTP200 valor de d_MsgGT201.Tmt_Est  ", d_MsgGT201.Tmt_Est.toString());
			Log.debug("AGTP200 LINK TO 204", data.getUser().toString());
			Log.debug("AGTP200 LINK TO 204", data.getUser().getName());
			Log.debug("AGTP200 LINK TO 204", data.getUser().getUserName());
			Log.debug("AGTP200 LINK TO 204", data.getUser().SESSION_KEY);
			if ((d_MsgGT201.Tmt_Est.toString().equals("TMTSD")
					|| d_MsgGT201.Tmt_Est.toString().equals("DVVSD"))) {
				
				
				String sqlTEXT=MSGGT201.GtiTraStmt(g_PrmPC080.getNtr(),g_PrmPC080.getNcn());
				Log.debug("CONSULTA A MODTRA :", sqlTEXT);
				Vector vQuery = BasePeer.executeQuery(sqlTEXT, Constants.BASE_GAR);
		        if (vQuery.size()==1){
		            //Solo si hay datos en la TT_GTI_TRA entonces regirigimos a MODTAS
		        	//Si no hay, es por que la tasaci�n se hizo en el otro sistema.
				
					String url = TurbineResources.getString("MT.url.informe.uvt");
					if (url != null) {
						String key=TurbineResources.getString("kib.modtas.key");
						String iv=TurbineResources.getString("kib.modtas.iv");
						Log.debug("AGTP201 key ", key);
						url = setUrlAction(url, key, iv);					
						url = url.replaceAll("VARGAR", encrypt(g_PrmPC080.getNcn(),key));
						url = url.replaceAll("VAREVENT", encrypt(g_PrmPC080.getNtr(),key));
						url = url.replaceAll("NUSR", encrypt(data.getUser().getPermStorage().get("LOGIN_NAME").toString(),key) );
						Log.debug("AGTP200 LINK TO 204", url);
						
	
					}
					data.getUser().setTemp("Agtp204_UrlMT", url);
					// BF_MSG.Link_Program("Agtp200", "Agtp204", data, g_Msg);
					lcPrograma = "Agtp204";
		        }
			}
	          
			RUTGEN.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
			BF_MSG.Link_Program("Agtp201", lcPrograma, data, g_Msg);
			Log.debug("AGTP200 LINK TO 204 RETORNO  ", lcRetorno);
			if (lcRetorno.trim().compareTo("") != 0) {
				data.getUser().removeTemp("Agtp201_g_PrmPC080");
				data.getUser().setTemp("Agtp201_g_PrmPC080", g_PrmPC080);
				BF_MSG.MsgInt("Agtp201", data, g_Msg, lcRetorno, "1");
				return;
			} else {
				return;
			}
		}
		if (Opc.equals("D")) {
			String Seq = data.getParameters().getString("Seq", "");
			i = Integer.parseInt(Seq);
			d_MsgGT201 = (com.intGarantias.modules.global.MSGGT201.Bff_MsgGT201) g_MsgGT201.GT201_Tab
					.elementAt(i);
			if (d_MsgGT201.Tmt_Est.toString().compareTo("ENUVT") != 0) {
				data.getUser().removeTemp("Agtp201_g_PrmPC080");
				data.getUser().setTemp("Agtp201_g_PrmPC080", g_PrmPC080);
				BF_MSG.MsgInt("Agtp201", data, g_Msg,
						"No Se Puede Devolver a Ejecutivo", "1");
				return;
			}
			RUTGEN.MoverA(g_PrmPC080.PC080_Ntr, d_MsgGT201.Tmt_Nev);
			RUTGEN.MoverA(g_PrmPC080.PC080_Trt, d_MsgGT201.Tmt_Trt);
			g_PrmED210 = PRMED210.Inicia_PrmED210();
			RUTGEN.MoverA(g_PrmED210.ED210_Rtn, "RW");
			data.getUser().removeTemp("Agtp201_g_PrmPC080");
			data.getUser().setTemp("Agtp201_g_PrmPC080", g_PrmPC080);
			RUTGEN.MoverA(g_Msg.Msg_Dat, PRMED210.LSet_De_PrmED210(g_PrmED210));
			BF_MSG.Link_Program("Agtp201", "Aedp210", data, g_Msg);
			if (lcRetorno.trim().compareTo("") != 0) {
				data.getUser().removeTemp("Agtp201_g_PrmPC080");
				data.getUser().setTemp("Agtp201_g_PrmPC080", g_PrmPC080);
				BF_MSG.MsgInt("Agtp201", data, g_Msg, lcRetorno, "1");
				return;
			} else {
				return;
			}
		}
		if (Opc.equals("N")) {
			Poblar_Panel(data, context, "S");
			setTemplate(data, "Garantias,Agt,AGTP201.vm");
			return;
		}
		if (Opc.equals("P")) {
			Poblar_Panel(data, context, "P");
			setTemplate(data, "Garantias,Agt,AGTP201.vm");
			return;
		} else {
			return;
		}
	}

	private String setUrlAction(String url,String key, String iv) {
		
		if(TERPI.equalsIgnoreCase(g_PrmPC080.PC080_Ttr.toString())){
			  try {
				url=url.replaceAll("VARACTION", encrypt("termProyecto",key));
			} catch (Exception e) {
				e.printStackTrace();
			}finally{
			  url=url.replaceAll("VARACTION", "termProyecto");
			}
		  }else if(
			  RETPI.equalsIgnoreCase(g_PrmPC080.PC080_Ttr.toString())){
			  try {
				url=url.replaceAll("VARACTION", encrypt("estAvance",key));
			} catch (Exception e) {
				e.printStackTrace();
			}finally{
			  url=url.replaceAll("VARACTION", "estAvance");
			}
		  }
		  else{
			  try {
				url=url.replaceAll("VARACTION", encrypt("index",key));
			} catch (Exception e) {
				e.printStackTrace();
			}finally{
			  url=url.replaceAll("VARACTION", "index");
			}
		  }
		return url;
	}

	public void doAgtp201_Continue(RunData data, Context context)
			throws Exception {
		if (RUTGEN.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Appp030")) {
			g_PrmPC080 = (com.FHTServlet.modules.global.PRMPC080.PrmPC080) data
					.getUser().getTemp("Agtp201_g_PrmPC080");
			data.getUser().removeTemp("Agtp201_g_PrmPC080");
			RUTGEN.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
			BF_MSG.Param_Program(data, g_Msg);
			setTemplate(data, "Garantias,Agt,AGTP201.vm");
			return;
		}
		if (RUTGEN.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Appp077")) {
			g_PrmPP077 = PRMPP077.LSet_A_PrmPP077(g_Msg.Msg_Dat.toString());
			g_PrmPC080 = (com.FHTServlet.modules.global.PRMPC080.PrmPC080) data
					.getUser().getTemp("Agtp201_g_PrmPC080");
			data.getUser().removeTemp("Agtp201_g_PrmPC080");
			if (g_PrmPP077.PP077_Swe.toString().equals("S")) {
				data.getUser().removeTemp("Agtp201_Msg");
				data.getUser().removeTemp("Agtp201_MsgGT201");
				RUTGEN.MoverA(g_Msg.Msg_Dat,
						PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
				BF_MSG.Return_Program(data, g_Msg);
				return;
			} else {
				g_MsgGT201 = MSGGT201.Inicia_MsgGT201();
				RUTGEN.MoverA(g_MsgGT201.GT201_Idr, "I");
				RUTGEN.MoverA(g_MsgGT201.GT201_Tpr, "A");
				RUTGEN.MoverA(g_MsgGT201.GT201_Tsd, g_PrmPP077.PP077_Tsd);
				RUTGEN.MoverA(g_MsgGT201.GT201_Suc, g_PrmPP077.PP077_Suc);
				RUTGEN.MoverA(g_MsgGT201.GT201_Eje, g_PrmPP077.PP077_Eje);
				RUTGEN.MoverA(g_MsgGT201.GT201_Ncn, g_PrmPP077.PP077_Ncn);
				RUTGEN.MoverA(g_MsgGT201.GT201_Cli, g_PrmPP077.PP077_Cli);
				Poblar_Panel(data, context, "I");
				RUTGEN.MoverA(g_Msg.Msg_Dat,
						PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
				BF_MSG.Param_Program(data, g_Msg);
				setTemplate(data, "Garantias,Agt,AGTP201.vm");
				return;
			}
		}
		if (RUTGEN.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Aedp210")) {
			g_PrmED210 = PRMED210.LSet_A_PrmED210(g_Msg.Msg_Dat.toString());
			g_PrmPC080 = (com.FHTServlet.modules.global.PRMPC080.PrmPC080) data
					.getUser().getTemp("Agtp201_g_PrmPC080");
			data.getUser().removeTemp("Agtp201_g_PrmPC080");
			if (g_PrmED210.ED210_Rtn.toString().equals("OK")) {
				g_MsgED135 = MSGED135.Inicia_MsgED135();
				RUTGEN.MoverA(g_MsgED135.ED135_Idr, "X");
				RUTGEN.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
				RUTGEN.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
				RUTGEN.MoverA(g_MsgED135.ED135_Est, "");
				RUTGEN.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
				RUTGEN.MoverA(g_MsgED135.ED135_Ldv, g_PrmED210.ED210_Ldt);
				RUTGEN.MoverA(g_MsgED135.ED135_Ezt, "DVUVT");
				RUTGEN.MoverA(g_MsgED135.ED135_Exi, "TSD");
				RUTGEN.MoverA(g_MsgED135.ED135_Exn,
						RUTGEN.Zeros(g_MsgED135.ED135_Exn));
				RUTGEN.MoverA(g_Msg.Msg_Dat,
						MSGED135.LSet_De_MsgED135(g_MsgED135));
				g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
				Poblar_Panel(data, context, "E");
			}
			RUTGEN.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
			BF_MSG.Param_Program(data, g_Msg);
			setTemplate(data, "Garantias,Agt,AGTP201.vm");
			return;
		}
		g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
		if (g_PrmPC080.PC080_Rtn.toString().equals("OK"))
			Poblar_Panel(data, context, "E");
		setTemplate(data, "Garantias,Agt,AGTP201.vm");
	}

	public void Poblar_Panel(RunData data, Context context, String pcIdr)
			throws Exception {
		String lcTpr = "";
		String lcTsd = "";
		String lcSuc = "";
		String lcEje = "";
		String lcNcn = "";
		String lcCli = "";
		com.intGarantias.modules.global.MSGGT201.Bff_MsgGT201 d_MsgGT201 = new com.intGarantias.modules.global.MSGGT201.Bff_MsgGT201();
		if (pcIdr.equals("E")) {
			g_MsgGT201 = (com.intGarantias.modules.global.MSGGT201.Buf_MsgGT201) data
					.getUser().getTemp("Agtp201_MsgGT201");
			i = 0;
		}
		if (pcIdr.equals("S"))
			i = Integer.parseInt(g_MsgGT201.GT201_Nro.toString()) - 1;
		if (pcIdr.equals("P"))
			i = 0;
		if (pcIdr.compareTo("I") != 0) {
			lcTpr = g_MsgGT201.GT201_Tpr.toString();
			lcTsd = g_MsgGT201.GT201_Tsd.toString();
			lcSuc = g_MsgGT201.GT201_Suc.toString();
			lcEje = g_MsgGT201.GT201_Eje.toString();
			lcNcn = g_MsgGT201.GT201_Ncn.toString();
			lcCli = g_MsgGT201.GT201_Cli.toString();
			d_MsgGT201 = (com.intGarantias.modules.global.MSGGT201.Bff_MsgGT201) g_MsgGT201.GT201_Tab
					.elementAt(i);
			g_MsgGT201 = MSGGT201.Inicia_MsgGT201();
			RUTGEN.MoverA(g_MsgGT201.GT201_Idr, pcIdr);
			RUTGEN.MoverA(g_MsgGT201.GT201_Tpr, lcTpr);
			RUTGEN.MoverA(g_MsgGT201.GT201_Tsd, lcTsd);
			RUTGEN.MoverA(g_MsgGT201.GT201_Suc, lcSuc);
			RUTGEN.MoverA(g_MsgGT201.GT201_Eje, lcEje);
			RUTGEN.MoverA(g_MsgGT201.GT201_Ncn, lcNcn);
			RUTGEN.MoverA(g_MsgGT201.GT201_Cli, lcCli);
			g_MsgGT201.GT201_Tab.set(0, d_MsgGT201);
		}
		Log.debug("AGTP201[Poblar_Panel.KEY]",
				(new StringBuffer()).append("[").append(pcIdr).append("][")
						.append(lcTpr).append("][").append(lcTsd).append("][")
						.append(lcSuc).append("][").append(lcEje).append("][")
						.append(lcNcn).append("][").append(lcCli).append("]")
						.toString());
		RUTGEN.MoverA(g_Msg.Msg_Dat, MSGGT201.LSet_De_MsgGT201(g_MsgGT201));
		g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GT201");
		g_MsgGT201 = MSGGT201.LSet_A_MsgGT201(g_Msg.Msg_Dat.toString());
		data.getUser().removeTemp("Agtp201_MsgGT201");
		data.getUser().setTemp("Agtp201_MsgGT201", g_MsgGT201);
		data.getUser().removeTemp("Agtp201_Msg");
		data.getUser().setTemp("Agtp201_Msg", g_Msg);
	}
	
	 public void Limpieza(RunData data, Context context)
             throws Exception 
 {
   data.getUser().removeTemp("Agtp724_PrmPC080");
   data.getUser().removeTemp("Agtp724_MsgED135");
   data.getUser().removeTemp("Agtp724_ImgBase");
   data.getUser().removeTemp("Agtp724_ImgGtApe");
   data.getUser().removeTemp("Agtp724_ImgGtTsd");
   data.getUser().removeTemp("Agtp724_vImgGtHyp");
   data.getUser().removeTemp("Agtp724_vImgGtHpd");
   data.getUser().removeTemp("Agtp724_vImgs");
   data.getUser().removeTemp("Agtp724_vTabImg");
 }
	 public static String encrypt(String valueToEnc,String keyProp) throws Exception {
	        Key key = generateKey(keyProp);
	        Cipher c = Cipher.getInstance(ALGORITHM);
	        c.init(Cipher.ENCRYPT_MODE, key);
	        byte[] encValue = c.doFinal(valueToEnc.getBytes());
	        String encryptedValue = new BASE64Encoder().encode(encValue);
	        return encryptedValue;
	    }
	 private static Key generateKey(String keyProp) throws Exception {
	        Key key = new SecretKeySpec(keyProp.getBytes(), ALGORITHM);
	        return key;
	    }

	public int i;
	public String lcRetorno;
	RUTGEN rg;
	BF_MSG msg;
	com.FHTServlet.modules.global.BF_MSG.Buf_Msg g_Msg;
	com.FHTServlet.modules.global.PRMPC080.PrmPC080 g_PrmPC080;
	com.FHTServlet.modules.global.PRMPP077.Buf_PrmPP077 g_PrmPP077;
	com.intGlobal.modules.global.MSGED135.Buf_MsgED135 g_MsgED135;
	com.intGarantias.modules.global.MSGGT201.Buf_MsgGT201 g_MsgGT201;
	com.intGlobal.modules.global.PRMED210.Buf_PrmED210 g_PrmED210;
}
