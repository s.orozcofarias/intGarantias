// Source File Name:   Agtp419.java
// Descripcion     :   Consulta Detalle Valores CMA (GT419)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.PRMPC080;

import com.intGarantias.modules.global.MSGGT419;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp419 extends FHTServletAction
{
  //===============================================================================================================================
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  PRMPC080.PrmPC080 g_PrmPC080     = new PRMPC080.PrmPC080();
  MSGGT419.Buf_MsgGT419 g_MsgGT419 = new MSGGT419.Buf_MsgGT419();
  //-------------------------------------------------------------------------------------------
  public Agtp419()
  {
  }
  //===============================================================================================================================
  public void doPerform(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP419[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp419-DIN", data);
    doAgtp419_Init(data, context);
  }
  //-------------------------------------------------------------------------------------------
  public void doAgtp419_Init(RunData data, Context context) throws Exception
  {
    //Log.debug("AGTP419[doAgtp419_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.getDat());
    g_MsgGT419 = MSGGT419.Inicia_MsgGT419();
    rg.MoverA(g_MsgGT419.GT419_Idr, "I");
    rg.MoverA(g_MsgGT419.GT419_Gti, g_PrmPC080.getSis() + g_PrmPC080.getNcn());
    rg.MoverA(g_MsgGT419.GT419_Seq, g_PrmPC080.getNtc());
    MSGGT419.Consulta_MsgGT419(g_MsgGT419);
    data.getUser().removeTemp("Agtp419_MsgGT419");
    data.getUser().setTemp("Agtp419_MsgGT419", g_MsgGT419);
    data.getUser().removeTemp("Agtp419_PrmPC080");
    data.getUser().setTemp("Agtp419_PrmPC080", g_PrmPC080);
    setTemplate(data, "Garantias,Agt,AGTP419.vm");
  }
  //===============================================================================================================================
  public void doAgtp419(RunData data, Context context) throws Exception
  {
    //Log.debug("AGTP419[doAgtp419.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp419-MAN", data);
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.getDat());
    String Opc = data.getParameters().getString("Opc" ,"");
    if (Opc.trim().equals("S"))
       {
         //Boton Salir
         data.getUser().removeTemp("Agtp419_MsgGT419");
         data.getUser().removeTemp("Agtp419_PrmPC080");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Program(data, g_Msg);
         return;
       }
  }
  //===============================================================================================================================
}