// Source File Name:   Agtp321.java
// Descripcion:        Consulta Envio Fiscalia (ED125, ED135)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.BF_IMG;
import com.FHTServlet.modules.global.PRMPC080;

import com.intGlobal.modules.global.MSGED125;
import com.intGlobal.modules.global.MSGED135;

import com.intGarantias.modules.global.GT_IMG;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp321 extends FHTServletAction
{
  //---------------------------------------------------------------------------------------
  public int i = 0;
  public String lcData = "";
  public Vector vDatos;
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  BF_IMG img                       = new BF_IMG();
  BF_IMG.Buf_Img g_Img             = new BF_IMG.Buf_Img();
  BF_IMG.Buf_Img_Bse g_Img_Base    = new BF_IMG.Buf_Img_Bse();
  GT_IMG gtimg                     = new GT_IMG();
  GT_IMG.Buf_Img_GtApe g_Img_GtApe = new GT_IMG.Buf_Img_GtApe();
  GT_IMG.Buf_Img_GtEsl g_Img_GtEsl = new GT_IMG.Buf_Img_GtEsl();
  PRMPC080.PrmPC080 g_PrmPC080     = new PRMPC080.PrmPC080();
  MSGED125.Buf_MsgED125 g_MsgED125 = new MSGED125.Buf_MsgED125();
  MSGED135.Buf_MsgED135 g_MsgED135 = new MSGED135.Buf_MsgED135();
  //---------------------------------------------------------------------------------------
  public Agtp321()
  {
  }
  //---------------------------------------------------------------------------------------
  public void doPerform(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP321[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp321-DIN", data);
    if (g_Msg.Msg_Pgr.toString().equals("RT"))
       { doAgtp321_Continue(data, context); }
    else
       { doAgtp321_Init(data, context); }
  }
  //---------------------------------------------------------------------------------------
  public void doCancel(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP321[doCancel.start]", "[CANCELADO]");
    setTemplate(data, "Garantias,Agt,AGTP321.vm");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp321_Init(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP321[doAgtp321_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    data.getUser().removeTemp("Agtp321_PrmPC080");
    data.getUser().setTemp("Agtp321_PrmPC080", g_PrmPC080);
    Carga_de_Host(data, context);
    Carga_Inicial(data, context);
    rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
    BF_MSG.Param_Program(data, g_Msg);
    setTemplate(data, "Garantias,Agt,AGTP321.vm" );
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp321(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP321[doAgtp321.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp321-MAN", data);
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    String Opc = data.getParameters().getString("Opc", "");
    if (Opc.trim().equals("S"))
       {
         //Boton Salir
         data.getUser().removeTemp("Agtp321_PrmPC080");
         data.getUser().removeTemp("Agtp321_Evt");
         data.getUser().removeTemp("Agtp321_ImgGtApe");
         data.getUser().removeTemp("Agtp321_ImgGtEsl");
         data.getUser().removeTemp("Agtp321_MsgED125");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "NK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp321", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("D"))
       {
         //Boton Devolver (Pedir Nuevos Documentos)
         data.getUser().removeTemp("Agtp321_g_PrmPC080");
         data.getUser().setTemp("Agtp321_g_PrmPC080" ,g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Link_Program("Agtp321", "Aedp127", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("A"))
       {
         //Boton Aceptar
         Cursar(data, context);
         data.getUser().removeTemp("Agtp321_PrmPC080");
         data.getUser().removeTemp("Agtp321_Evt");
         data.getUser().removeTemp("Agtp321_ImgGtApe");
         data.getUser().removeTemp("Agtp321_ImgGtEsl");
         data.getUser().removeTemp("Agtp321_MsgED125");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp321", data, g_Msg);
         return;
       }
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp321_Continue(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP321[doAgtp321_Continue.start]", "[" + data.getUser().getUserName() + "]");
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Atgp124"))
       {
//         g_PrmTG123 = PRMTG123.LSet_A_PrmTG123(g_Msg.Msg_Dat.toString());
//         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp321_g_PrmPC080");
//         data.getUser().removeTemp("Agtp321_g_PrmPC080");
//         if (g_PrmTG123.TG123_Prv.toString().trim().compareTo("")!=0)
//             {
//               g_Img_GtEsl = (GT_IMG.Buf_Img_GtEsl)data.getUser().getTemp("Agtp321_ImgGtEsl");
//               rg.MoverA(g_Img_GtEsl.GtEsl_Cmn, g_PrmTG123.TG123_Prv.toString() + g_PrmTG123.TG123_Cmn.toString());
//               rg.MoverA(g_Img_GtEsl.GtEsl_Ncm, g_PrmTG123.TG123_Ncm);
//               rg.MoverA(g_Img_GtEsl.GtEsl_Npv, g_PrmTG123.TG123_Npr);
//               data.getUser().removeTemp("Agtp321_ImgGtEsl");
//               data.getUser().setTemp("Agtp321_ImgGtEsl", g_Img_GtEsl);
//             }
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP321.vm" );
         return;
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Aedp127"))
       {
         PRMPC080.PrmPC080 l_PrmPC080 = new PRMPC080.PrmPC080();
         l_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp321_g_PrmPC080");
         data.getUser().removeTemp("Agtp321_g_PrmPC080");
         if (l_PrmPC080.PC080_Rtn.toString().equals("NK"))
            { 
              rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
              BF_MSG.Param_Program(data, g_Msg);
              setTemplate(data, "Garantias,Agt,AGTP321.vm" );
              return;
            }
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp321", data, g_Msg);
       }
  }
  //---------------------------------------------------------------------------------------
  public void Carga_de_Host(RunData data, Context context)
              throws Exception 
  {
    Vector g_Tab_Img = img.Carga_Img_Host(g_Msg, g_PrmPC080.PC080_Ntr.toString(), data, context);
    De_Tab_Img(g_Tab_Img, data);
    data.getUser().removeTemp("Agtp321_ImgGtApe");
    data.getUser().setTemp("Agtp321_ImgGtApe", g_Img_GtApe);
    data.getUser().removeTemp("Agtp321_ImgGtEsl");
    data.getUser().setTemp("Agtp321_ImgGtEsl", g_Img_GtEsl);
  }
  //---------------------------------------------------------------------------------------
  public void De_Tab_Img(Vector g_Tab_Img, RunData data)
  {
    g_Img       = img.Inicia_Img();
    lcData      = img.LSet_De_Img(g_Img);
    g_Img_Base  = img.LSet_A_ImgBase(lcData);
    g_Img_GtApe = gtimg.LSet_A_ImgGtApe(g_Img.Img_Dat.toString());
    g_Img_GtEsl = gtimg.LSet_A_ImgGtEsl(g_Img.Img_Dat.toString());
    for (i=0; i<g_Tab_Img.size(); i++)
        {
          lcData = img.LSet_De_Img((BF_IMG.Buf_Img)g_Tab_Img.elementAt(i));
          g_Img  = img.LSet_A_Img(lcData);
          if (g_Img.Img_Dax.toString().trim().equals("BASE"))
             {
               g_Img_GtApe = gtimg.LSet_A_ImgGtApe(g_Img.Img_Dat.toString());
               g_Img_Base  = img.LSet_A_ImgBase(g_Img_GtApe.GtApe_Bse.toString());
             }
          if (g_Img.Img_Dax.toString().trim().equals("YESL"))
             { g_Img_GtEsl = gtimg.LSet_A_ImgGtEsl(g_Img.Img_Dat.toString()); }
        }
  }
  //---------------------------------------------------------------------------------------
  public void Carga_Inicial(RunData data, Context context)
              throws Exception 
  {
    g_MsgED135 = MSGED135.Inicia_MsgED135();
    rg.MoverA(g_MsgED135.ED135_Idr, "C");
    rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
    rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
    g_MsgED135 = MSGED135.LSet_A_MsgED135(g_Msg.Msg_Dat.toString());
    data.getUser().removeTemp("Agtp321_MsgED135");
    data.getUser().setTemp("Agtp321_MsgED135", g_MsgED135);
    //****************************************************************************
    g_MsgED125 = MSGED125.Inicia_MsgED125();
    rg.MoverA(g_MsgED125.ED125_Idr, "I");
    rg.MoverA(g_MsgED125.ED125_Nev, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_MsgED125.ED125_Trt, g_PrmPC080.PC080_Trt);
    rg.MoverA(g_Msg.Msg_Dat, MSGED125.LSet_De_MsgED125(g_MsgED125));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED125");
    g_MsgED125 = MSGED125.LSet_A_MsgED125(g_Msg.Msg_Dat.toString());
    data.getUser().removeTemp("Agtp321_MsgED125");
    data.getUser().setTemp("Agtp321_MsgED125", g_MsgED125);
  }
  //---------------------------------------------------------------------------------------
  public void Cursar(RunData data, Context context)
              throws Exception 
  {
    g_MsgED135 = MSGED135.Inicia_MsgED135();
    rg.MoverA(g_MsgED135.ED135_Idr, "M");
    rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
    rg.MoverA(g_MsgED135.ED135_Est, "");
    rg.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
    rg.MoverA(g_MsgED135.ED135_Ezt, "TERMI");
    rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
  }
  //---------------------------------------------------------------------------------------
}