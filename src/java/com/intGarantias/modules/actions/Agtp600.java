// Source File Name:   Agtp600.java
// Descripcion:        Estadisticas de Tasaciones (GT600)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.PRMPC080;

import com.intGarantias.modules.global.MSGGT600;
import com.intGarantias.modules.global.PRMGT600;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp600 extends FHTServletAction
{
  //---------------------------------------------------------------------------------------
  public int    i = 0;
  public String lcRetorno = "";
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  PRMPC080.PrmPC080 g_PrmPC080     = new PRMPC080.PrmPC080();
  MSGGT600.Buf_MsgGT600 g_MsgGT600 = new MSGGT600.Buf_MsgGT600();
  PRMGT600.Buf_PrmGT600 g_PrmGT600 = new PRMGT600.Buf_PrmGT600();
  //---------------------------------------------------------------------------------------
  public Agtp600()
  {
  }
  //---------------------------------------------------------------------------------------
  public void doPerform(RunData data, Context context)
              throws Exception
  {
  //Log.debug("AGTP600[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp600-DIN", data);
    if (g_Msg.Msg_Pgr.toString().equals("RT"))
       { doAgtp600_Continue(data, context); }
    else
       { doAgtp600_Init(data, context); }
  }
  //---------------------------------------------------------------------------------------
  public void doCancel(RunData data, Context context)
              throws Exception
  {
  //Log.debug("AGTP600[doCancel.start]", "[CANCELADO]");
    setTemplate(data, "Garantias,Agt,AGTP600.vm");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp600_Init(RunData data, Context context)
              throws Exception
  {
  //Log.debug("AGTP600[doAgtp600_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    g_PrmGT600 = PRMGT600.Inicia_PrmGT600();
    rg.MoverA(g_PrmGT600.GT600_Ano, g_Msg.Msg_Fch.toString().substring(0,4));
    rg.MoverA(g_PrmGT600.GT600_Mes, g_Msg.Msg_Fch.toString().substring(4,6));

    rg.MoverA(g_PrmGT600.GT600_Ano, "2003");
    rg.MoverA(g_PrmGT600.GT600_Mes, "10");


    rg.MoverA(g_PrmGT600.GT600_Tpr, "ZTT");
    Carga_DeHost(data, context);
    data.getUser().removeTemp("Agtp600_PrmGT600");
    data.getUser().setTemp("Agtp600_PrmGT600", g_PrmGT600);
    setTemplate(data, "Garantias,Agt,AGTP600.vm" );
  //Log.debug("AGTP600[doAgtp600_Init.end]", "[" + data.getUser().getUserName() + "]");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp600(RunData data, Context context)
              throws Exception
  {
  //Log.debug("AGTP600[doAgtp600.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp600-MAN", data);
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    String Opc = data.getParameters().getString("Opc");
    if  (Opc.equals("S"))
       //Salir
        {
          data.getUser().removeTemp("Agtp600_PrmPC080");
          data.getUser().removeTemp("Agtp600_MsgGT600");
          data.getUser().removeTemp("Agtp600_PrmGT600");
          BF_MSG.Return_Program(data, g_Msg);
          return;
        }
    if  (Opc.equals("K"))
       //Cambiar
        {
          rg.MoverA(g_PrmGT600.GT600_Ano, data.getParameters().getString("Ano", ""));
          rg.MoverA(g_PrmGT600.GT600_Mes, data.getParameters().getString("Mes", ""));
          rg.MoverA(g_PrmGT600.GT600_Tpr, data.getParameters().getString("Tpr", ""));
          Carga_DeHost(data, context);
          data.getUser().removeTemp("Agtp600_PrmGT600");
          data.getUser().setTemp("Agtp600_PrmGT600", g_PrmGT600);
          setTemplate(data, "Garantias,Agt,AGTP600.vm" );
          return;
        }
  }
  //---------------------------------------------------------------------------------------
  public void Carga_DeHost(RunData data, Context context)
              throws Exception
  {
    g_MsgGT600 = MSGGT600.Inicia_MsgGT600();
    rg.MoverA(g_MsgGT600.GT600_Idr, "I");
    rg.MoverA(g_MsgGT600.GT600_Ano, g_PrmGT600.GT600_Ano);
    rg.MoverA(g_MsgGT600.GT600_Mes, g_PrmGT600.GT600_Mes);
    rg.MoverA(g_MsgGT600.GT600_Tpr, g_PrmGT600.GT600_Tpr);
    rg.MoverA(g_Msg.Msg_Dat, MSGGT600.LSet_De_MsgGT600(g_MsgGT600));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GT600");
    g_MsgGT600 = MSGGT600.LSet_A_MsgGT600(g_Msg.Msg_Dat.toString());
    data.getUser().removeTemp("Agtp600_MsgGT600");
    data.getUser().setTemp("Agtp600_MsgGT600", g_MsgGT600);
    data.getUser().removeTemp("Agtp600_PrmPC080");
    data.getUser().setTemp("Agtp600_PrmPC080", g_PrmPC080);
  }
//---------------------------------------------------------------------------------------
  public void doAgtp600_Continue(RunData data, Context context)
              throws Exception
  {
  //Log.debug("AGTP600[doAgtp600_Continue.start]", "[" + data.getUser().getUserName() + "]");
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Appp030"))
       {
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp600_g_PrmPC080");
         data.getUser().removeTemp("Agtp600_g_PrmPC080");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "garantias,Agt,AGTP600.vm" );
         return;
       }
    g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp600_g_PrmPC080");
    data.getUser().removeTemp("Agtp600_g_PrmPC080");
    rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
    BF_MSG.Param_Program(data, g_Msg);
    setTemplate(data, "garantias,Agt,AGTP600.vm" );
    return;
  }
}