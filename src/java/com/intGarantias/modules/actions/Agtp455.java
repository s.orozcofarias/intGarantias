// Source File Name:   Agtp455.java
// Descripcion:        Consulta/Seleccion InfLegal MYC (GT455)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.BF_LOB;
import com.FHTServlet.modules.global.PRMPC080;

import com.intGarantias.modules.global.MSGGT455;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp455 extends FHTServletAction
{
  //===============================================================================================================================
  public int    i = 0;
  public String lcRetorno = "";
  public Vector vDatos;
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  BF_LOB lob                       = new BF_LOB();
  PRMPC080.PrmPC080 g_PrmPC080     = new PRMPC080.PrmPC080();
  MSGGT455.Buf_MsgGT455 g_MsgGT455 = new MSGGT455.Buf_MsgGT455();
  //-------------------------------------------------------------------------------------------
  public Agtp455()
  {
  }
  //===============================================================================================================================
  public void doPerform(RunData data, Context context) throws Exception
  {
    //Log.debug("AGTP455[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp455-DIN", data);
    if (g_Msg.getPgr().equals("RT"))
       { doAgtp455_Continue(data, context); }
    else
       { doAgtp455_Init(data, context); }
  }
  //-------------------------------------------------------------------------------------------
  public void doAgtp455_Init(RunData data, Context context) throws Exception
  {
    //Log.debug("AGTP455[doAgtp455_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.getDat());
    g_MsgGT455 = MSGGT455.Inicia_MsgGT455();
    rg.MoverA(g_MsgGT455.GT455_Idr, "I");
    rg.MoverA(g_MsgGT455.GT455_Sis, g_PrmPC080.PC080_Cnr.Sis);
    rg.MoverA(g_MsgGT455.GT455_Ncn, g_PrmPC080.PC080_Cnr.Ncn);
    Poblar_Panel(data, context);
    setTemplate(data, "Garantias,Agt,AGTP455.vm");
  }
  //===============================================================================================================================
  public void doAgtp455(RunData data, Context context) throws Exception
  {
    //Log.debug("AGTP455[doAgtp455.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp455-MAN", data);
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.getDat());
    String Opc = data.getParameters().getString("Opc" ,"");
    if  (Opc.equals("S"))
        {
          //Salir
          data.getUser().removeTemp("Agtp455_PrmPC080");
          data.getUser().removeTemp("Agtp455_MsgGT455");
          rg.MoverA(g_PrmPC080.PC080_Rtn, "NK");
          rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
          BF_MSG.Return_Data("Agtp455", data, g_Msg);
          return;
        }
    g_MsgGT455 = (MSGGT455.Buf_MsgGT455)data.getUser().getTemp("Agtp455_MsgGT455");
    MSGGT455.Bff_MsgGT455 d_MsgGT455 = new MSGGT455.Bff_MsgGT455();
    if  (Opc.equals("N"))
        {
          //Siguiente
          i = Integer.parseInt(g_MsgGT455.getNro())-1;
          d_MsgGT455 = (MSGGT455.Bff_MsgGT455)g_MsgGT455.GT455_Tap.elementAt(i);
          g_MsgGT455 = MSGGT455.Inicia_MsgGT455();
          rg.MoverA(g_MsgGT455.GT455_Idr, "S");
          rg.MoverA(g_MsgGT455.GT455_Sis, g_PrmPC080.PC080_Cnr.Sis);
          rg.MoverA(g_MsgGT455.GT455_Ncn, g_PrmPC080.PC080_Cnr.Ncn);
          g_MsgGT455.GT455_Tap.set(0, d_MsgGT455);
          Poblar_Panel(data, context);
          setTemplate(data, "Garantias,Agt,AGTP455.vm");
        }
    if  (Opc.equals("P"))
        {
          //Previo
          i = 0;
          d_MsgGT455 = (MSGGT455.Bff_MsgGT455)g_MsgGT455.GT455_Tap.elementAt(i);
          g_MsgGT455 = MSGGT455.Inicia_MsgGT455();
          rg.MoverA(g_MsgGT455.GT455_Idr, "P");
          rg.MoverA(g_MsgGT455.GT455_Sis, g_PrmPC080.PC080_Cnr.Sis);
          rg.MoverA(g_MsgGT455.GT455_Ncn, g_PrmPC080.PC080_Cnr.Ncn);
          g_MsgGT455.GT455_Tap.set(0, d_MsgGT455);
          Poblar_Panel(data, context);
          setTemplate(data, "Garantias,Agt,AGTP455.vm");
        }
    if (Opc.trim().equals("I"))
       {
         //Boton Imagen
       //rg.MoverA(g_PrmPC080.PC080_Cnr.Sis, g_MsgGT450.GT450_Gti.toString().substring(0,3));
       //rg.MoverA(g_PrmPC080.PC080_Cnr.Ncn, g_MsgGT450.GT450_Gti.toString().substring(3,10));
         rg.MoverA(g_PrmPC080.PC080_Ntc,     "");
       //rg.MoverA(g_PrmPC080.PC080_Dtt,     d_MsgGT452.GT452_Dsb);
       //rg.MoverA(g_PrmPC080.PC080_Mnt,     d_MsgGT452.GT452_Vcn);
         rg.MoverA(g_PrmPC080.PC080_Rtn,     "RO");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Link_Program("Agtp455", "Appp096", data, g_Msg);
         return;
       }
  }
  //===============================================================================================================================
  public void doAgtp455_Continue(RunData data, Context context) throws Exception
  {
    //Log.debug("AGTP455[doAgtp455_Continue.start]", "[" + data.getUser().getUserName() + "]");
    if (rg.FH_RTrim(g_Msg.getPgl()).equals("Appp030"))
       {
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp455_g_PrmPC080");
         data.getUser().removeTemp("Agtp455_g_PrmPC080");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP455.vm");
         return;
       }
    setTemplate(data, "Garantias,Agt,AGTP455.vm" );
    return;
  }
  //===============================================================================================================================
  public void Poblar_Panel(RunData data, Context context) throws Exception
  {
    rg.MoverA(g_Msg.Msg_Dat, MSGGT455.LSet_De_MsgGT455(g_MsgGT455));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GT455");
    g_MsgGT455 = MSGGT455.LSet_A_MsgGT455(g_Msg.getDat());
    data.getUser().removeTemp("Agtp455_MsgGT455");
    data.getUser().setTemp("Agtp455_MsgGT455", g_MsgGT455);
    data.getUser().removeTemp("Agtp455_PrmPC080");
    data.getUser().setTemp("Agtp455_PrmPC080", g_PrmPC080);
  }
  //===============================================================================================================================
}