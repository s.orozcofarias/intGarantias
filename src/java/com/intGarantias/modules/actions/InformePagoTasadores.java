package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.bice.ipt.excel.InformePagoTasadorExcel;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;

import javax.servlet.http.HttpServletResponse;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;


public class InformePagoTasadores extends FHTServletAction{
  
	public InformePagoTasadores() {}

	public void doPerform(RunData data, Context context) throws Exception {
		
		//super.doPerform(data);
		
		Log.debug("InformePagoTasadores.doPerform");
	    setTemplate(data, "Garantias,Agt,InformePagoTasadores.vm" );
	    
	    String Opcion = data.getParameters().getString("Opcion", "");
		String Fec_Ini = data.getParameters().getString("Fec_Ini", "");
		String Fec_Fin = data.getParameters().getString("Fec_Fin", "");
        String paginaRetorno = data.getParameters().getString("paginaRetorno", "");
		
		if("".equals(paginaRetorno)){
			Log.debug("xxdata.getParameters().getString(action)"+data.getParameters().getString("action"));
			paginaRetorno = data.getParameters().getString("action");	
		}
		context.put("paginaRetorno", paginaRetorno);
		
		
		String userName = "";
		if(data.getUser()!=null && data.getUser().getUserName()!=null){
			userName = data.getUser().getUserName();
		}
		
		if (Opcion.trim().equals("A")){ //BOTON ACEPTAR
			Log.debug("Opcion " +Opcion);
			Log.debug("Fec_Ini " +Fec_Ini);
			Log.debug("Fec_Fin " +Fec_Fin);
			
			SimpleDateFormat fechaInicio = new SimpleDateFormat("dd/MM/yyyy");
			SimpleDateFormat fechaFin = new SimpleDateFormat("dd/MM/yyyy");
						
			InformePagoTasadorExcel informeExcel = new InformePagoTasadorExcel();
			
			//informeExcel.cambiaFiltroEstadoGtia("RGTSD"); //solo para pruebas
			File archivo = informeExcel.generaInformePagoTasador(fechaInicio.parse(Fec_Ini), fechaFin.parse(Fec_Fin), userName);
		
			HttpServletResponse response = data.getResponse();
			
			InputStream in = new  FileInputStream(archivo); 
			
 			response.setContentType("application/octet-stream");					
            String headerKey = "Content-Disposition";
            String headerValue = "attachment; filename=\""+archivo.getName()+ "\"";
            response.setHeader(headerKey, headerValue);
            
            OutputStream servletoutputstream = response.getOutputStream();
			int BUFFER_SIZE = 1024;
			byte[] buffer = new byte[BUFFER_SIZE];
			int sizeRead = 0;
			while ((sizeRead = in.read(buffer)) >= 0) { //leyendo del host

				servletoutputstream.write(buffer, 0, sizeRead); //escribiendo para el navegador
			}
			in.close(); 
			servletoutputstream.close();
			
		}
		
		if (Opcion.trim().equals("S")){ //BOTON SALIR
			Log.debug("Opcion " +Opcion);
			//setTemplate(data, "garantias,agt,AGTP800.vm" );  
			setTemplate(data, "garantias,agt,"+paginaRetorno+".vm" );  
		}
	    		
	}

	
	
}