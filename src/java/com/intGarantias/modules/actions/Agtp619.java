// Source File Name:   Agtp619.java
// Descripcion     :   Modifica MYC [MODMC] (ED135, ED500, ED535, GT619)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.BF_LOB;
import com.FHTServlet.modules.global.BF_IMG;
import com.FHTServlet.modules.global.BF_EVT;
import com.FHTServlet.modules.global.PRMPC080;
import com.FHTServlet.modules.util.ScreenHelper;

import com.intGlobal.modules.global.MSGED090;
import com.intGlobal.modules.global.MSGED100;
import com.intGlobal.modules.global.MSGED135;
import com.intGlobal.modules.global.PRMED210;
import com.intGlobal.modules.global.PRMCL090;

import com.intGarantias.modules.global.MSGGT619;
import com.intGarantias.modules.global.GT_IMG;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp619 extends FHTServletAction
{
  //===============================================================================================================================
  public int i = 0;
  public String lcData = "";
  RUTGEN rg                        = new RUTGEN();
  BF_LOB lob                       = new BF_LOB();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  BF_EVT.Buf_Evt g_Evt             = new BF_EVT.Buf_Evt();
  BF_IMG img                       = new BF_IMG();
  BF_IMG.Buf_Img g_Img             = new BF_IMG.Buf_Img();
  BF_IMG.Buf_Img_Bse g_Img_Base    = new BF_IMG.Buf_Img_Bse();
  GT_IMG gtimg                     = new GT_IMG();
  GT_IMG.Buf_Img_GtMmc g_Img_GtMmc = new GT_IMG.Buf_Img_GtMmc();
  GT_IMG.Buf_Img_GtMmd g_Img_GtMmd = new GT_IMG.Buf_Img_GtMmd();
  GT_IMG.Buf_Img_GtCus g_Img_GtCus = new GT_IMG.Buf_Img_GtCus();
  GT_IMG.Buf_Img_GtDoc g_Img_GtDoc = new GT_IMG.Buf_Img_GtDoc();
  Vector v_Img_GtMmd               = new Vector();
  Vector v_Img_GtCus               = new Vector();
  Vector v_Img_GtDoc               = new Vector();
  PRMPC080.PrmPC080 g_PrmPC080     = new PRMPC080.PrmPC080();
  MSGED090.Buf_MsgED090 g_MsgED090 = new MSGED090.Buf_MsgED090();
  MSGED100.Buf_MsgED100 g_MsgED100 = new MSGED100.Buf_MsgED100();
  MSGED135.Buf_MsgED135 g_MsgED135 = new MSGED135.Buf_MsgED135();
  PRMED210.Buf_PrmED210 g_PrmED210 = new PRMED210.Buf_PrmED210();
  PRMCL090.Buf_PrmCL090 g_PrmCL090 = new PRMCL090.Buf_PrmCL090();
  MSGGT619.Buf_MsgGT619 g_MsgGT619 = new MSGGT619.Buf_MsgGT619();
  //-------------------------------------------------------------------------------------------
  public Agtp619()
  {
  }
  //===============================================================================================================================
  public void doPerform(RunData data, Context context) throws Exception
  {
    //Log.debug("AGTP619[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp619-DIN", data);
    if (g_Msg.getPgr().equals("RT"))
       { doAgtp619_Continue(data, context); }
    else
       { doAgtp619_Init(data, context); }
  }
  //-------------------------------------------------------------------------------------------
  public void doAgtp619_Init(RunData data, Context context) throws Exception
  {
    //Log.debug("AGTP619[doAgtp619_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.getDat());
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (g_PrmPC080.getNcn().trim().equals("")
     && g_PrmPC080.getCli().trim().equals(""))
       {
         data.getUser().removeTemp("Agtp619_g_PrmPC080");
         data.getUser().setTemp("Agtp619_g_PrmPC080", g_PrmPC080);
         g_PrmCL090 = PRMCL090.Inicia_PrmCL090();
         rg.MoverA(g_PrmCL090.CL090_Pcc, "N");
         rg.MoverA(g_Msg.Msg_Dat, PRMCL090.LSet_De_PrmCL090(g_PrmCL090));
         BF_MSG.Link_Program("Agtp619", "Eclp090", data, g_Msg);
         return;
       }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    g_MsgED135 = MSGED135.Inicia_MsgED135();
    if (g_PrmPC080.getNtr().trim().compareTo("")!=0)
       {
         Recupera_Evt(data, context);
         Carga_de_Host(data, context);
       }
    else
       {
         if (Carga_Inicial(data, context) == 1)
            { return; }
         Init_Evt_GRT(data, context);
       }
    data.getUser().removeTemp("Agtp619_MsgED135");
    data.getUser().setTemp("Agtp619_MsgED135", g_MsgED135);
    data.getUser().removeTemp("Agtp619_Evt");
    data.getUser().setTemp("Agtp619_Evt", g_Evt);
    data.getUser().removeTemp("Agtp619_ImgBase");
    data.getUser().setTemp("Agtp619_ImgBase", g_Img_Base);
    data.getUser().removeTemp("Agtp619_ImgGtMmc");
    data.getUser().setTemp("Agtp619_ImgGtMmc", g_Img_GtMmc);
    data.getUser().removeTemp("Agtp619_vMmc");
  //data.getUser().setTemp("Agtp619_vMmc", g_MsgGT619.getMmc());
    data.getUser().setTemp("Agtp619_vMmc", v_Img_GtMmd);
    data.getUser().removeTemp("Agtp619_vImgGtCus");
    data.getUser().setTemp("Agtp619_vImgGtCus", v_Img_GtCus);
    data.getUser().removeTemp("Agtp619_vImgGtDoc");
    data.getUser().setTemp("Agtp619_vImgGtDoc", v_Img_GtDoc);
    data.getUser().removeTemp("Agtp619_Opt");
    if (g_Img_GtMmc.getNsl().trim().equals("")
     || g_Img_GtMmc.getNsl().trim().equals("000000000"))
       { data.getUser().setTemp("Agtp619_Opt", ""); }
    else
       { data.getUser().setTemp("Agtp619_Opt", lob.leeCLOB(g_Img_GtMmc.getNsl().trim())); }
    rg.MoverA(g_PrmPC080.PC080_Cli, g_Evt.Evt_Cli);
    rg.MoverA(g_PrmPC080.PC080_Ncl, g_Evt.Evt_Ncl);
    rg.MoverA(g_PrmPC080.PC080_Dir, g_Evt.Evt_Dir);
    rg.MoverA(g_PrmPC080.PC080_Tfc, g_Evt.Evt_Tfc);
    rg.MoverA(g_PrmPC080.PC080_Dcn, g_Evt.Evt_Dcn);
    rg.MoverA(g_PrmPC080.PC080_Ndc, g_Evt.Evt_Ndc);
    rg.MoverA(g_PrmPC080.PC080_Ntt, g_Evt.Evt_Ntt);
    data.getUser().removeTemp("Agtp619_PrmPC080");
    data.getUser().setTemp("Agtp619_PrmPC080", g_PrmPC080);
    rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
    BF_MSG.Param_Program(data, g_Msg);
    setTemplate(data, "Garantias,Agt,AGTP619.vm");
  }
  //===============================================================================================================================
  public void doAgtp619(RunData data, Context context) throws Exception
  {
    //Log.debug("AGTP619[doAgtp619.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp619-MAN", data);
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.getDat());
    String Opc = data.getParameters().getString("Opc", "");
    if (Opc.trim().equals("S"))
       {
         //Boton Salir
         data.getUser().removeTemp("Agtp619_vMmc");
         data.getUser().removeTemp("Agtp619_ImgGtMmc");
         data.getUser().removeTemp("Agtp619_vImgGtCus");
         data.getUser().removeTemp("Agtp619_vImgGtDoc");
         data.getUser().removeTemp("Agtp619_ImgBase");
         data.getUser().removeTemp("Agtp619_Evt");
         data.getUser().removeTemp("Agtp619_PrmPC080");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "NK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp619", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("R"))
       {
         //Boton Errores
         g_MsgED135 = (MSGED135.Buf_MsgED135)data.getUser().getTemp("Agtp619_MsgED135");
         g_PrmED210 = PRMED210.Inicia_PrmED210();
         rg.MoverA(g_PrmED210.ED210_Rtn, "RO");
         rg.MoverA(g_PrmED210.ED210_Ldt, g_MsgED135.ED135_Ldv);
         data.getUser().removeTemp("Agtp619_g_PrmPC080");
         data.getUser().setTemp("Agtp619_g_PrmPC080", g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMED210.LSet_De_PrmED210(g_PrmED210));
         BF_MSG.Link_Program("Agtp619", "Aedp210", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("C"))
       {
         //Boton OK (Cursar en Ejecutivo)
         g_Img_GtMmc = (GT_IMG.Buf_Img_GtMmc)data.getUser().getTemp("Agtp619_ImgGtMmc");
         g_Img_Base  = img.LSet_A_ImgBase(g_Img_GtMmc.GtMmc_Bse.toString());
         v_Img_GtCus = (Vector)data.getUser().getTemp("Agtp619_vImgGtCus");
         v_Img_GtDoc = (Vector)data.getUser().getTemp("Agtp619_vImgGtDoc");
         Actualiza_Datos("cuerpo", data, context);
         Vector g_Tab_Img = A_Tab_Img(data);
         img.Graba_Img_Host(g_Msg, g_Tab_Img, data, context);
         g_MsgED135 = MSGED135.Inicia_MsgED135();
         rg.MoverA(g_MsgED135.ED135_Idr, "M");
         rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
         rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
         rg.MoverA(g_MsgED135.ED135_Est, "");
         rg.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
         rg.MoverA(g_MsgED135.ED135_Vlc, g_Img_GtMmc.GtMmc_Vcn);
         rg.MoverA(g_MsgED135.ED135_Ezt, "TERMI");
         rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
         g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
         data.getUser().removeTemp("Agtp619_vMmc");
         data.getUser().removeTemp("Agtp619_ImgGtMmc");
         data.getUser().removeTemp("Agtp619_vImgGtCus");
         data.getUser().removeTemp("Agtp619_vImgGtDoc");
         data.getUser().removeTemp("Agtp619_ImgBase");
         data.getUser().removeTemp("Agtp619_Evt");
         data.getUser().removeTemp("Agtp619_PrmPC080");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp619", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("O"))
       {
         //Boton Aceptar (Ingreso en Operador)
         g_Img_GtMmc = (GT_IMG.Buf_Img_GtMmc)data.getUser().getTemp("Agtp619_ImgGtMmc");
         g_Img_Base  = img.LSet_A_ImgBase(g_Img_GtMmc.GtMmc_Bse.toString());
         v_Img_GtCus = (Vector)data.getUser().getTemp("Agtp619_vImgGtCus");
         v_Img_GtDoc = (Vector)data.getUser().getTemp("Agtp619_vImgGtDoc");
         Actualiza_Datos("cuerpo", data, context);
         Vector g_Tab_Img = A_Tab_Img(data);
         img.Graba_Img_Host(g_Msg, g_Tab_Img, data, context);
         g_MsgED135 = MSGED135.Inicia_MsgED135();
         rg.MoverA(g_MsgED135.ED135_Idr, "M");
         rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
         rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
         rg.MoverA(g_MsgED135.ED135_Est, "");
         rg.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
         rg.MoverA(g_MsgED135.ED135_Vlc, g_Img_GtMmc.GtMmc_Vcn);
         rg.MoverA(g_MsgED135.ED135_Ezt, "ENVSD");
         rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
         g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
         data.getUser().removeTemp("Agtp619_vMmc");
         data.getUser().removeTemp("Agtp619_ImgGtMmc");
         data.getUser().removeTemp("Agtp619_vImgGtCus");
         data.getUser().removeTemp("Agtp619_vImgGtDoc");
         data.getUser().removeTemp("Agtp619_ImgBase");
         data.getUser().removeTemp("Agtp619_Evt");
         data.getUser().removeTemp("Agtp619_PrmPC080");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp619", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("A"))
       {
         //Boton Agregar
         g_Img_GtMmc = (GT_IMG.Buf_Img_GtMmc)data.getUser().getTemp("Agtp619_ImgGtMmc");
         g_Img_Base  = img.LSet_A_ImgBase(g_Img_GtMmc.GtMmc_Bse.toString());
         v_Img_GtCus = (Vector)data.getUser().getTemp("Agtp619_vImgGtCus");
         v_Img_GtDoc = (Vector)data.getUser().getTemp("Agtp619_vImgGtDoc");
         Actualiza_Datos("cuerpo", data, context);
         rg.MoverA(g_Img_GtMmc.GtMmc_Psw, "A");
         rg.MoverA(g_Img_GtMmc.GtMmc_Idx, rg.Suma(g_Img_GtMmc.GtMmc_Tfr,0,"1",0,3,0));
         data.getUser().removeTemp("Agtp619_ImgGtMmc");
         data.getUser().setTemp("Agtp619_ImgGtMmc", g_Img_GtMmc);
         data.getUser().removeTemp("Agtp619_ImgGtDoc");
         data.getUser().setTemp("Agtp619_ImgGtDoc", g_Img_GtDoc);
         data.getUser().removeTemp("Agtp619_vImgGtCus");
         data.getUser().setTemp("Agtp619_vImgGtCus", v_Img_GtCus);
         setTemplate(data, "Garantias,Agt,AGTP619.vm");
         return;
       }
    if (Opc.trim().equals("E"))
       {
         //Boton Eliminar
         g_Img_GtMmc = (GT_IMG.Buf_Img_GtMmc)data.getUser().getTemp("Agtp619_ImgGtMmc");
         g_Img_Base  = img.LSet_A_ImgBase(g_Img_GtMmc.GtMmc_Bse.toString());
         v_Img_GtCus = (Vector)data.getUser().getTemp("Agtp619_vImgGtCus");
         v_Img_GtDoc = (Vector)data.getUser().getTemp("Agtp619_vImgGtDoc");
         Actualiza_Datos("cuerpo", data, context);
         data.getUser().removeTemp("Agtp619_ImgGtMmc");
         data.getUser().setTemp("Agtp619_ImgGtMmc", g_Img_GtMmc);
         data.getUser().removeTemp("Agtp619_vImgGtCus");
         data.getUser().setTemp("Agtp619_vImgGtCus", v_Img_GtCus);
         String Seq = data.getParameters().getString("Seq", "");
         i = Integer.parseInt(Seq);
         g_Img_GtDoc = (GT_IMG.Buf_Img_GtDoc)v_Img_GtDoc.elementAt(i);
         if (g_Img_GtDoc.getIad().equals("N"))
            {
              rg.MoverA(g_Img_GtMmc.GtMmc_Psw, "E");
              rg.MoverA(g_Img_GtMmc.GtMmc_Idx, rg.Suma(Seq,0,"1",0,3,0));
              data.getUser().removeTemp("Agtp619_ImgGtMmc");
              data.getUser().setTemp("Agtp619_ImgGtMmc", g_Img_GtMmc);
              data.getUser().removeTemp("Agtp619_ImgGtDoc");
              data.getUser().setTemp("Agtp619_ImgGtDoc", g_Img_GtDoc);
              setTemplate(data, "Garantias,Agt,AGTP619.vm");
              return;
            }
         data.getUser().removeTemp("Agtp619_ImgGtDoc");
         data.getUser().setTemp("Agtp619_ImgGtDoc", g_Img_GtDoc);
         data.getUser().removeTemp("Agtp619_g_PrmPC080");
         data.getUser().setTemp("Agtp619_g_PrmPC080", g_PrmPC080);
         rg.MoverA(g_PrmPC080.PC080_Idj, "E");
         rg.MoverA(g_PrmPC080.PC080_Cnr.Sis, g_Img_Base.getSis());
         rg.MoverA(g_PrmPC080.PC080_Cnr.Ncn, g_Img_Base.getNcn());
         rg.MoverA(g_PrmPC080.PC080_Ntc, "000");
         rg.MoverA(g_PrmPC080.PC080_Lob, g_Img_GtDoc.GtDoc_Adj);
         rg.MoverA(g_PrmPC080.PC080_Fll, g_Img_GtDoc.GtDoc_Fad);
         rg.MoverA(g_PrmPC080.PC080_Ccp.Dsg, g_Img_GtDoc.GtDoc_Dsc);
         rg.MoverA(g_PrmPC080.PC080_Ccp.Tpi, g_Img_GtDoc.GtDoc_Tpi);
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Link_Program("Agtp619", "Agtp719", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("DOC") || Opc.trim().equals("VER"))
       {
         //Imagen Digitalizar
         String Seq = data.getParameters().getString("Seq", "");
         i = Integer.parseInt(Seq);
         v_Img_GtDoc = (Vector)data.getUser().getTemp("Agtp619_vImgGtDoc");
         g_Img_GtDoc = (GT_IMG.Buf_Img_GtDoc)v_Img_GtDoc.elementAt(i);
         g_Img_GtMmc = (GT_IMG.Buf_Img_GtMmc)data.getUser().getTemp("Agtp619_ImgGtMmc");
         g_Img_Base  = img.LSet_A_ImgBase(g_Img_GtMmc.getBse());
         Actualiza_Datos("detalle", data, context);
         data.getUser().removeTemp("Agtp619_ImgGtDoc");
         data.getUser().setTemp("Agtp619_ImgGtDoc", g_Img_GtDoc);
         data.getUser().removeTemp("Agtp619_g_PrmPC080");
         data.getUser().setTemp("Agtp619_g_PrmPC080", g_PrmPC080);
         if (Opc.trim().equals("DOC"))
            { rg.MoverA(g_PrmPC080.PC080_Idj, "A"); }
         else
            { rg.MoverA(g_PrmPC080.PC080_Idj, "D"); }
         rg.MoverA(g_PrmPC080.PC080_Cnr.Sis, g_Img_Base.getSis());
         rg.MoverA(g_PrmPC080.PC080_Cnr.Ncn, g_Img_Base.getNcn());
         rg.MoverA(g_PrmPC080.PC080_Ntc, "000");
         rg.MoverA(g_PrmPC080.PC080_Lob, g_Img_GtDoc.GtDoc_Adj);
         rg.MoverA(g_PrmPC080.PC080_Fll, g_Img_GtDoc.GtDoc_Fad);
         rg.MoverA(g_PrmPC080.PC080_Ccp.Dsg, g_Img_GtDoc.GtDoc_Dsc);
         rg.MoverA(g_PrmPC080.PC080_Ccp.Tpi, g_Img_GtDoc.GtDoc_Tpi);
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Link_Program("Agtp619", "Agtp719", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("Y"))
       {
         //Boton Si
         String Seq = data.getParameters().getString("Seq", "0");
         i = Integer.parseInt(Seq);
         g_Img_GtDoc = (GT_IMG.Buf_Img_GtDoc)data.getUser().getTemp("Agtp619_ImgGtDoc");
         g_Img_GtMmc = (GT_IMG.Buf_Img_GtMmc)data.getUser().getTemp("Agtp619_ImgGtMmc");
         v_Img_GtDoc = (Vector)data.getUser().getTemp("Agtp619_vImgGtDoc");
         Actualiza_Datos("detalle", data, context);
         if (g_Img_GtMmc.getPsw().trim().equals("A"))
            { v_Img_GtDoc.add(g_Img_GtDoc); }
         else
            {
              if (g_Img_GtMmc.getPsw().trim().equals("M"))
                 { v_Img_GtDoc.set(i, g_Img_GtDoc); }
              else
                 {
                   if (g_Img_GtMmc.getPsw().trim().equals("E"))
                      { v_Img_GtDoc.remove(i); }
                 }
            }
         rg.MoverA(g_Img_GtMmc.GtMmc_Tfr, v_Img_GtDoc.size());
         rg.MoverA(g_Img_GtMmc.GtMmc_Vcn, rg.FmtValor("0",0,2,15,"+"));
         rg.MoverA(g_Img_GtMmc.GtMmc_Idx, " ");
         rg.MoverA(g_Img_GtMmc.GtMmc_Psw, " ");
         data.getUser().removeTemp("Agtp619_ImgGtMmc");
         data.getUser().setTemp("Agtp619_ImgGtMmc", g_Img_GtMmc);
         data.getUser().removeTemp("Agtp619_ImgGtDoc");
         data.getUser().removeTemp("Agtp619_vImgGtDoc");
         data.getUser().setTemp("Agtp619_vImgGtDoc", v_Img_GtDoc);
         setTemplate(data, "Garantias,Agt,AGTP619.vm");
         return;
       }
    if (Opc.trim().equals("N"))
       {
         //Boton No
         data.getUser().removeTemp("Agtp619_ImgGtDoc");
         g_Img_GtMmc = (GT_IMG.Buf_Img_GtMmc)data.getUser().getTemp("Agtp619_ImgGtMmc");
         rg.MoverA(g_Img_GtMmc.GtMmc_Idx, " ");
         rg.MoverA(g_Img_GtMmc.GtMmc_Psw, " ");
         data.getUser().removeTemp("Agtp619_ImgGtMmc");
         data.getUser().setTemp("Agtp619_ImgGtMmc", g_Img_GtMmc);
         setTemplate(data, "Garantias,Agt,AGTP619.vm");
         return;
       }
  }
  //-------------------------------------------------------------------------------------------
  public void Actualiza_Datos(String tipo, RunData data, Context context) throws Exception
  {
    if (tipo.equals("cuerpo"))
       {
         rg.MoverA(g_Img_GtMmc.GtMmc_Dsg, data.getParameters().getString("GtMmc_Dsg", ""));
         rg.MoverA(g_Img_GtMmc.GtMmc_Fmd, data.getParameters().getString("GtMmc_Fmd", ""));
         rg.MoverA(g_Img_GtMmc.GtMmc_Cbt, data.getParameters().getString("GtMmc_Cbt", ""));
         String lcChars = data.getParameters().getString("GtMmc_Opt", "");
         if (!lcChars.trim().equals(""))
            { 
              if (g_Img_GtMmc.getNsl().trim().equals("")
               || g_Img_GtMmc.getNsl().trim().equals("000000000"))
                 { rg.MoverA(g_Img_GtMmc.GtMmc_Nsl, Asigna_Folio("FOLIO-CLOBS", data, context)); }
              lob.mantieneCLOB("M", g_Img_GtMmc.getNsl().trim(), lcChars);
            }
         data.getUser().removeTemp("Agtp619_Opt");
         if (g_Img_GtMmc.getNsl().trim().equals("")
          || g_Img_GtMmc.getNsl().trim().equals("000000000"))
            { data.getUser().setTemp("Agtp619_Opt", ""); }
         else
            { data.getUser().setTemp("Agtp619_Opt", lob.leeCLOB(g_Img_GtMmc.getNsl().trim())); }
         String tabSel = data.getParameters().getString("tabSel", "");
         for (int p=0; p<v_Img_GtCus.size(); p++)
            {
              g_Img_GtCus = (GT_IMG.Buf_Img_GtCus)v_Img_GtCus.elementAt(p);
              if (tabSel.charAt(p)=='S')
                 { rg.MoverA(g_Img_GtCus.GtCus_Chk, "S"); }
              else
                 { rg.MoverA(g_Img_GtCus.GtCus_Chk, "N"); }
              v_Img_GtCus.set(p, g_Img_GtCus);
            }
       }
    if (tipo.equals("detalle"))
       {
         rg.MoverA(g_Img_GtDoc.GtDoc_Seq, data.getParameters().getString("GtDoc_Seq", ""));
         rg.MoverA(g_Img_GtDoc.GtDoc_Iad, data.getParameters().getString("GtDoc_Iad", ""));
         rg.MoverA(g_Img_GtDoc.GtDoc_Dsc, data.getParameters().getString("GtDoc_Dsc", ""));
         rg.MoverA(g_Img_GtDoc.GtDoc_Adj, data.getParameters().getString("GtDoc_Adj", ""));
         rg.MoverA(g_Img_GtDoc.GtDoc_Fad, data.getParameters().getString("GtDoc_Fad", ""));
         rg.MoverA(g_Img_GtDoc.GtDoc_Tpi, data.getParameters().getString("GtDoc_Tpi", ""));
         rg.MoverA(g_Img_GtDoc.GtDoc_Fll, "");
       }
  }
  //-------------------------------------------------------------------------------------------
  public String Asigna_Folio(String pcFolio, RunData data, Context context) throws Exception 
  {
    g_MsgED090 = MSGED090.Inicia_MsgED090();
    rg.MoverA(g_MsgED090.ED090_Idr, "F");
    rg.MoverA(g_MsgED090.ED090_Cod, pcFolio);
    rg.MoverA(g_Msg.Msg_Dat, MSGED090.LSet_De_MsgED090(g_MsgED090));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED090");
    g_MsgED090 = MSGED090.LSet_A_MsgED090(g_Msg.Msg_Dat.toString());
    String lcFolio = rg.FmtValor(g_MsgED090.ED090_Fol.toString(),0,0,9,"");
    return lcFolio;
  }
  //===============================================================================================================================
  public void doAgtp619_Continue(RunData data, Context context) throws Exception
  {
    //Log.debug("AGTP619[doAgtp619_Continue.start]", "[" + data.getUser().getUserName() + "]");
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Eclp090"))
       {
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp619_g_PrmPC080");
         data.getUser().removeTemp("Agtp619_g_PrmPC080");
         g_PrmCL090 = PRMCL090.LSet_A_PrmCL090(g_Msg.getDat());
         if (g_PrmCL090.CL090_Cli.toString().trim().equals(""))
            {
              //Limpieza(data, context);
              rg.MoverA(g_PrmPC080.PC080_Rtn, "NK");
              rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
              BF_MSG.Return_Data("Agtp619", data, g_Msg);
              return;
            }
         rg.MoverA(g_PrmPC080.PC080_Cli, g_PrmCL090.CL090_Cli);
         rg.MoverA(g_PrmPC080.PC080_Ncl, g_PrmCL090.CL090_Ncl);
         rg.MoverA(g_PrmPC080.PC080_Tpr, g_PrmCL090.CL090_Tpr);
         rg.MoverA(g_PrmPC080.PC080_Acc, "SELECCIONA");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Link_Program("Agtp619", "Agtp400", data, g_Msg);
         return;
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Agtp400"))
       {
         g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.getDat());
         if (g_PrmPC080.PC080_Rtn.toString().trim().equals("NK"))
            {
              //Limpieza(data, context);
              rg.MoverA(g_PrmPC080.PC080_Rtn, "NK");
              rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
              BF_MSG.Return_Data("Agtp619", data, g_Msg);
              return;
            }
         rg.MoverA(g_PrmPC080.PC080_Acc, "MODMC");
         rg.MoverA(g_PrmPC080.PC080_Ttr, "MODMC");
         rg.MoverA(g_PrmPC080.PC080_Ntt, "MODIFICACION DE GARANTIA MYC");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         doAgtp619_Init(data, context);
         return;
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Aedp210"))
       {
         g_PrmED210 = PRMED210.LSet_A_PrmED210(g_Msg.getDat());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp619_g_PrmPC080");
         data.getUser().removeTemp("Agtp619_g_PrmPC080");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP619.vm");
         return;
       }
    if (rg.FH_RTrim(g_Msg.getPgl()).equals("Agtp719"))
       {
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp619_g_PrmPC080");
         data.getUser().removeTemp("Agtp619_g_PrmPC080");
         //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
         PRMPC080.PrmPC080 l_PrmPC080 = new PRMPC080.PrmPC080();
         l_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.getDat());
         if (l_PrmPC080.getRtn().trim().equals("DG"))
            {
              g_Img_GtDoc = (GT_IMG.Buf_Img_GtDoc)data.getUser().getTemp("Agtp619_ImgGtDoc");
              Log.debug("Agtp619.doAgtp619_Continue:[l_PrmPC080.PC080_Lob....]["+l_PrmPC080.PC080_Lob+"]");
              Log.debug("Agtp619.doAgtp619_Continue:[l_PrmPC080.PC080_Fll....]["+l_PrmPC080.PC080_Fll+"]");
              Log.debug("Agtp619.doAgtp619_Continue:[l_PrmPC080.PC080_Ccp.Dsg]["+l_PrmPC080.PC080_Ccp.Dsg+"]");
              Log.debug("Agtp619.doAgtp619_Continue:[l_PrmPC080.PC080_Ccp.Tpi]["+l_PrmPC080.PC080_Ccp.Tpi+"]");
              rg.MoverA(g_Img_GtDoc.GtDoc_Iad, "S");
              rg.MoverA(g_Img_GtDoc.GtDoc_Adj, l_PrmPC080.PC080_Lob);
              rg.MoverA(g_Img_GtDoc.GtDoc_Fad, l_PrmPC080.PC080_Fll);
              rg.MoverA(g_Img_GtDoc.GtDoc_Dsc, l_PrmPC080.PC080_Ccp.Dsg);
              rg.MoverA(g_Img_GtDoc.GtDoc_Tpi, l_PrmPC080.PC080_Ccp.Tpi);
              data.getUser().removeTemp("Agtp619_ImgGtDoc");
              data.getUser().setTemp("Agtp619_ImgGtDoc", g_Img_GtDoc);
              //++++++++++++++++++++++++++++++++++++++++++++++++++++
              g_Img_GtMmc = (GT_IMG.Buf_Img_GtMmc)data.getUser().getTemp("Agtp619_ImgGtMmc");
              v_Img_GtDoc = (Vector)data.getUser().getTemp("Agtp619_vImgGtDoc");
              i = g_Img_GtDoc.igetSeq() - 1;
              Log.debug("Agtp619.doAgtp619_Continue:[i.......................]["+i+"]");
              v_Img_GtDoc.set(i, g_Img_GtDoc);
              Vector g_Tab_Img = A_Tab_Img(data);
              img.Graba_Img_Host(g_Msg, g_Tab_Img, data, context);
            }
         if (l_PrmPC080.getRtn().trim().equals("EL"))
            {
              g_Img_GtDoc = (GT_IMG.Buf_Img_GtDoc)data.getUser().getTemp("Agtp619_ImgGtDoc");
              //++++++++++++++++++++++++++++++++++++++++++++++++++++
              g_Img_GtMmc = (GT_IMG.Buf_Img_GtMmc)data.getUser().getTemp("Agtp619_ImgGtMmc");
              v_Img_GtDoc = (Vector)data.getUser().getTemp("Agtp619_vImgGtDoc");
              i = g_Img_GtDoc.igetSeq() - 1;
              Log.debug("Agtp619.doAgtp619_Continue:[i.......................]["+i+"]");
              v_Img_GtDoc.remove(i);
              Vector g_Tab_Img = A_Tab_Img(data);
              img.Graba_Img_Host(g_Msg, g_Tab_Img, data, context);
            }
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP619.vm");
         return;
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Appp030"))
       {
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp619_g_PrmPC080");
         if (g_PrmPC080!=null)
            {
              data.getUser().removeTemp("Agtp619_g_PrmPC080");
              rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
              BF_MSG.Param_Program(data, g_Msg);
              setTemplate(data, "Garantias,Agt,AGTP619.vm");
              return;
            }
         BF_MSG.Return_Program(data, g_Msg);
         return;
       }
  }
  //===============================================================================================================================
  public void Recupera_Evt(RunData data, Context context) throws Exception 
  {
    g_MsgED100 = MSGED100.Inicia_MsgED100();
    rg.MoverA(g_MsgED100.ED100_Idr, "Q");
    rg.MoverA(g_MsgED100.ED100_Nev, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_Msg.Msg_Dat, MSGED100.LSet_De_MsgED100(g_MsgED100));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED500");
    g_MsgED100 = MSGED100.LSet_A_MsgED100(g_Msg.getDat());
    g_Evt = BF_EVT.LSet_A_Evt(g_MsgED100.ED100_Evt.toString());
  }
  //-------------------------------------------------------------------------------------------
  public void Carga_de_Host(RunData data, Context context) throws Exception 
  {
    String lcSvc = "";
    Vector g_Tab_Img = img.Carga_Img_Host(g_Msg, g_PrmPC080.PC080_Ntr.toString(), data, context);
    De_Tab_Img(g_Tab_Img, data);
    if (g_Tab_Img.size() == 1)
       { 
       	 int liRet = Validar_Host("E", data, context); 
         if (g_Img_GtMmc.getTfr().trim().equals(""))
            { rg.MoverA(g_Img_GtMmc.GtMmc_Tfr, rg.Zeros(3)); }
       }
    rg.MoverA(g_MsgED135.ED135_Idr, "C");
    rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
    if (g_PrmPC080.PC080_Trt.toString().trim().equals(""))
       {
         rg.MoverA(g_MsgED135.ED135_Trt, "UNICO");
         lcSvc = "ED535";
       }
    else
       {
         rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
         lcSvc = "ED135";
       }
    rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, lcSvc);
    g_MsgED135 = MSGED135.LSet_A_MsgED135(g_Msg.getDat());
    //--------------------------------------------------------------
    if (v_Img_GtCus.size()==0)
       {
         Vector datosCUS = ScreenHelper.Consulta_SubMargenes(g_PrmPC080.getCli());
         v_Img_GtCus.clear();
         for (i=0; i<datosCUS.size(); i++)
            {
              ScreenHelper.Bff_Cus tCus = new ScreenHelper.Bff_Cus();
              tCus = (ScreenHelper.Bff_Cus)datosCUS.elementAt(i);
              GT_IMG.Buf_Img_GtCus p_Img_GtCus = new GT_IMG.Buf_Img_GtCus();
              rg.MoverA(p_Img_GtCus.GtCus_Num, tCus.getNum());
              rg.MoverA(p_Img_GtCus.GtCus_Smu, tCus.getSmu());
              rg.MoverA(p_Img_GtCus.GtCus_Csm, tCus.getCsm());
              rg.MoverA(p_Img_GtCus.GtCus_Dsc, tCus.getDsc());
              rg.MoverA(p_Img_GtCus.GtCus_Mnd, tCus.getMnd());
              rg.MoverA(p_Img_GtCus.GtCus_Mto, tCus.getMto());
              rg.MoverA(p_Img_GtCus.GtCus_Pre, tCus.getPre());
              rg.MoverA(p_Img_GtCus.GtCus_Chk, "N");
              rg.MoverA(p_Img_GtCus.GtCus_Fll, "");
              v_Img_GtCus.add (p_Img_GtCus);
            }
       }
  }
  //-------------------------------------------------------------------------------------------
  public void De_Tab_Img(Vector g_Tab_Img, RunData data)
  {
    g_Img       = img.Inicia_Img();
    lcData      = img.LSet_De_Img(g_Img);
    g_Img_Base  = img.LSet_A_ImgBase(lcData);
    g_Img_GtMmc = gtimg.LSet_A_ImgGtMmc(g_Img.Img_Dat.toString());
    v_Img_GtMmd.clear();
    v_Img_GtCus.clear();
    v_Img_GtDoc.clear();
    for (i=0; i<g_Tab_Img.size(); i++)
        {
          lcData = img.LSet_De_Img((BF_IMG.Buf_Img)g_Tab_Img.elementAt(i));
          g_Img  = img.LSet_A_Img(lcData);
          if (g_Img.Img_Dax.toString().trim().equals("BASE"))
             {
               g_Img_GtMmc = gtimg.LSet_A_ImgGtMmc(g_Img.Img_Dat.toString());
               g_Img_Base  = img.LSet_A_ImgBase(g_Img_GtMmc.GtMmc_Bse.toString());
             }
          if (g_Img.Img_Dax.toString().trim().equals("DTMMD"))
             {
               g_Img_GtMmd = gtimg.LSet_A_ImgGtMmd(g_Img.Img_Dat.toString());
               v_Img_GtMmd.add (g_Img_GtMmd); 
             }
          if (g_Img.getDax().trim().equals("ZZCUS"))
             {
               g_Img_GtCus = gtimg.LSet_A_ImgGtCus(g_Img.getDat());
               v_Img_GtCus.add (g_Img_GtCus);
             }
          if (g_Img.getDax().trim().equals("ZZDOC"))
             {
               g_Img_GtDoc = gtimg.LSet_A_ImgGtDoc(g_Img.getDat());
               v_Img_GtDoc.add (g_Img_GtDoc);
             }
        }
  }
  //===============================================================================================================================
  public int Carga_Inicial(RunData data, Context context) throws Exception 
  {
    g_Img       = img.Inicia_Img();
    lcData      = img.LSet_De_Img(g_Img);
    g_Img_Base  = img.LSet_A_ImgBase(lcData);
    g_Img_GtMmc = gtimg.LSet_A_ImgGtMmc(g_Img.Img_Dat.toString());
    rg.MoverA(g_Img_Base.Img_Base_Ttr, "MODMC");
    rg.MoverA(g_Img_Base.Img_Base_Sis, g_PrmPC080.PC080_Cnr.Sis);
    rg.MoverA(g_Img_Base.Img_Base_Ncn, g_PrmPC080.PC080_Cnr.Ncn);
    if (Validar_Host("N", data, context) == 1)
       { return 1; }
    rg.MoverA(g_PrmPC080.PC080_Ntr, g_Img_Base.Img_Base_Ntr);
    return 0;
  }
  //-------------------------------------------------------------------------------------------
  public void Init_Evt_GRT(RunData data, Context context) throws Exception 
  {
    rg.MoverA(g_Evt.Evt_Ttr, g_Img_Base.Img_Base_Ttr);
    rg.MoverA(g_Evt.Evt_Ntt, g_Img_Base.Img_Base_Ntt);
    rg.MoverA(g_Evt.Evt_Tsl, g_Img_Base.Img_Base_Tsl);
    rg.MoverA(g_Evt.Evt_Slc, g_Img_Base.Img_Base_Slc);
    rg.MoverA(g_Evt.Evt_Cli, g_Img_Base.Img_Base_Cli);
    rg.MoverA(g_Evt.Evt_Ncl, g_Img_Base.Img_Base_Ncl);
    rg.MoverA(g_Evt.Evt_Tpr, g_Img_Base.Img_Base_Tpr);
    rg.MoverA(g_Evt.Evt_Dir, g_Img_Base.Img_Base_Dir);
    rg.MoverA(g_Evt.Evt_Cmn, " ");                    //g_Img_Base.Img_Base_Cmn
    rg.MoverA(g_Evt.Evt_Tfc, g_Img_Base.Img_Base_Tfc);
    rg.MoverA(g_Evt.Evt_Sis, g_Img_Base.Img_Base_Sis);
    rg.MoverA(g_Evt.Evt_Ncn, g_Img_Base.Img_Base_Ncn);
    rg.MoverA(g_Evt.Evt_Dcn, g_Img_Base.Img_Base_Dcn);
    rg.MoverA(g_Evt.Evt_Ndc, g_Img_Base.Img_Base_Ndc);
    rg.MoverA(g_Evt.Evt_Tmn, g_Img_Base.Img_Base_Tmn);
    rg.MoverA(g_Evt.Evt_Trj, g_Img_Base.Img_Base_Trj);
    rg.MoverA(g_Evt.Evt_Dcm, g_Img_Base.Img_Base_Dcm);
    rg.MoverA(g_Evt.Evt_Prp, g_Img_Base.Img_Base_Prp);
    rg.MoverA(g_Evt.Evt_Suc, g_Img_Base.Img_Base_Suc);
    rg.MoverA(g_Evt.Evt_Nsu, g_Img_Base.Img_Base_Nsu);
    rg.MoverA(g_Evt.Evt_Mnd, g_Img_Base.Img_Base_Mnd);
    rg.MoverA(g_Evt.Evt_Nmn, g_Img_Base.Img_Base_Nmn);
    rg.MoverA(g_Evt.Evt_Eje, g_Img_Base.Img_Base_Eje);
    rg.MoverA(g_Evt.Evt_Nej, g_Img_Base.Img_Base_Nej);
    rg.MoverA(g_Evt.Evt_Tej, g_Img_Base.Img_Base_Tej);
    rg.MoverA(g_Evt.Evt_Est, "NUEVA");
    rg.MoverA(g_Evt.Evt_Fst, g_Msg.Msg_Fch);
    rg.MoverA(g_Evt.Evt_Upr, g_Img_Base.Img_Base_Trj);
    rg.MoverA(g_Evt.Evt_Vpr, rg.Zeros(g_Evt.Evt_Vpr));
    rg.MoverA(g_Evt.Evt_Fpr, g_Msg.Msg_Fch);
    rg.MoverA(g_Evt.Evt_Spr, rg.Zeros(g_Evt.Evt_Spr));
    rg.MoverA(g_Evt.Evt_Tpp, " ");
    rg.MoverA(g_Evt.Evt_Ggr, g_Img_GtMmc.GtMmc_Ggr);
    rg.MoverA(g_Evt.Evt_Fll, " ");
  }
  //===============================================================================================================================
  public int Validar_Host(String pcIdr, RunData data, Context context) throws Exception 
  {
    int p = 0;
    String tGtMmd_Iln = "";
    MSGGT619.Bff_MsgGT619 d_MsgGT619 = new MSGGT619.Bff_MsgGT619();
    boolean liSwtFin = false;
    v_Img_GtMmd.clear();
    g_MsgGT619 = MSGGT619.Inicia_MsgGT619();
    while (liSwtFin == false)
       {
         if (Valida_Host(pcIdr, data, context) == 1)
            { return 1; }
         for (int i=p; i<g_MsgGT619.GT619_Mmc.size(); i++)
            {
              d_MsgGT619 = (MSGGT619.Bff_MsgGT619)g_MsgGT619.GT619_Mmc.elementAt(i);
              GT_IMG.Buf_Img_GtMmd g_Img_GtMmd = new GT_IMG.Buf_Img_GtMmd();
              rg.MoverA(g_Img_GtMmd.GtMmd_Iln, d_MsgGT619.GtMmc_Iln);
              rg.MoverA(g_Img_GtMmd.GtMmd_Ilf, d_MsgGT619.GtMmc_Ilf);
              rg.MoverA(g_Img_GtMmd.GtMmd_Dsl, d_MsgGT619.GtMmc_Dsl);
              rg.MoverA(g_Img_GtMmd.GtMmd_Fll, "");
              v_Img_GtMmd.add(g_Img_GtMmd);
              tGtMmd_Iln = g_Img_GtMmd.getIln();
            }
         if (g_MsgGT619.GT619_Mmc.size() < MSGGT619.g_Max_GT619)
            { liSwtFin = true; }
         else
            {
              g_MsgGT619 = MSGGT619.Inicia_MsgGT619();
              MSGGT619.Bff_MsgGT619 Tap = new MSGGT619.Bff_MsgGT619();
              Tap.GtMmc_Iln.append(tGtMmd_Iln);
              Tap.GtMmc_Ilf.append("");
              Tap.GtMmc_Dsl.append("");
              g_MsgGT619.GT619_Mmc.add(Tap);
              pcIdr = "S";
              tGtMmd_Iln = "";
              p = 1;
            }
       }
    return 0;
  }
  //-------------------------------------------------------------------------------------------
  public int Valida_Host(String pcIdr, RunData data, Context context) throws Exception 
  {
    if (pcIdr.equals("N"))
       { rg.MoverA(g_Img_GtMmc.GtMmc_Bse, img.LSet_De_ImgBase_Bse(g_Img_Base)); }
    rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtMmc(g_Img_GtMmc));
    rg.MoverA(g_MsgGT619.GT619_Idr, pcIdr);
    rg.MoverA(g_MsgGT619.GT619_Img, g_Img.Img_Dat);
    rg.MoverA(g_Msg.Msg_Dat, MSGGT619.LSet_De_MsgGT619(g_MsgGT619));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GT619");
    g_MsgGT619 = MSGGT619.LSet_A_MsgGT619(g_Msg.getDat());
    if (g_MsgGT619.GT619_Idr.toString().equals("R"))
       {
         String lcMensaje = "NO SE";
         if (g_MsgGT619.GT619_Swt_Cli.toString().equals("1"))
            { lcMensaje = "Garantia Erronea (Sin CLI-GTI)"; }
         if (g_MsgGT619.GT619_Swt_Cli.toString().equals("2"))
            { lcMensaje = "Garantia Erronea (Sin CLI)"; }
         if (g_MsgGT619.GT619_Swt_Gti.toString().equals("1"))
            { lcMensaje = "Numero Garantia No Existe"; }
         if (g_MsgGT619.GT619_Swt_Gti.toString().equals("2"))
            { lcMensaje = "Garantia NO Esta Vigente"; }
         if (g_MsgGT619.GT619_Swt_Gti.toString().equals("3"))
            { lcMensaje = "Evento No Valido para Garantia"; }
         if (g_MsgGT619.GT619_Swt_Evt.toString().equals("1"))
            { lcMensaje = "Garantia Tiene Evento en Tramite"; }
         msg.MsgInt("Agtp619", data, g_Msg, lcMensaje, "1"); 
         return 1;
       }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    rg.MoverA(g_Img.Img_Dat, g_MsgGT619.GT619_Img);
    g_Img_GtMmc = gtimg.LSet_A_ImgGtMmc(g_Img.Img_Dat.toString());
    if (pcIdr.equals("N"))
       { 
       	 g_Img_Base  = img.LSet_A_ImgBase(g_Img_GtMmc.GtMmc_Bse.toString()); 
         rg.MoverA(g_Img_Base.Img_Base_Suc, g_Msg.Msg_Suc);
         rg.MoverA(g_Img_Base.Img_Base_Nsu, g_Msg.Msg_Nsu);
       //rg.MoverA(g_Img_Base.Img_Base_Mnd, "000");
       //rg.MoverA(g_Img_Base.Img_Base_Nmn, "PESOS CHILE");
         rg.MoverA(g_Img_Base.Img_Base_Eje, g_Msg.Msg_Cus);
         rg.MoverA(g_Img_Base.Img_Base_Nej, g_Msg.Msg_Nus);
         rg.MoverA(g_Img_Base.Img_Base_Tej, g_Msg.Msg_Fus);
         rg.MoverA(g_Img_GtMmc.GtMmc_Bse,   img.LSet_De_ImgBase_Bse(g_Img_Base));
         rg.MoverA(g_Img_GtMmc.GtMmc_Fmd,   g_Msg.Msg_Fch);
       //rg.MoverA(g_Img_GtMmc.GtMmc_Vts,   rg.Zeros(g_Img_GtMmc.GtMmc_Vts));
       }
    return 0;
  }
  //===============================================================================================================================
  public Vector A_Tab_Img(RunData data)
  {
    Vector v_Img = new Vector();
    v_Img.clear();
    rg.MoverA(g_Img.Img_Ntr, g_PrmPC080.PC080_Ntr);
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    rg.MoverA(g_Img.Img_Dax, "BASE");
    rg.MoverA(g_Img.Img_Seq, "000");
    rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtMmc(g_Img_GtMmc));
    Vector vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
    v_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    v_Img_GtMmd = (Vector)data.getUser().getTemp("Agtp619_vMmc");
    rg.MoverA(g_Img.Img_Seq, "000");
    for (i=0; i<v_Img_GtMmd.size(); i++)
        {
          g_Img_GtMmd = (GT_IMG.Buf_Img_GtMmd)v_Img_GtMmd.elementAt(i);
          rg.MoverA(g_Img.Img_Dax, "DTMMD");
          rg.MoverA(g_Img.Img_Seq, rg.Suma(g_Img.Img_Seq,0,"1",0,3,0));
          rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtMmd(g_Img_GtMmd));
          vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
          v_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
        }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    rg.MoverA(g_Img.Img_Seq, "000");
    for (i=0; i<v_Img_GtCus.size(); i++)
        {
          g_Img_GtCus = (GT_IMG.Buf_Img_GtCus)v_Img_GtCus.elementAt(i);
          rg.MoverA(g_Img.Img_Dax, "ZZCUS");
          rg.MoverA(g_Img.Img_Seq, rg.Suma(g_Img.Img_Seq,0,"1",0,3,0));
          rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtCus(g_Img_GtCus));
          vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
          v_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
        }
    rg.MoverA(g_Img.Img_Seq, "000");
    for (i=0; i<v_Img_GtDoc.size(); i++)
        {
          g_Img_GtDoc = (GT_IMG.Buf_Img_GtDoc)v_Img_GtDoc.elementAt(i);
          rg.MoverA(g_Img.Img_Dax, "ZZDOC");
          rg.MoverA(g_Img.Img_Seq, rg.Suma(g_Img.Img_Seq,0,"1",0,3,0));
          rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtDoc(g_Img_GtDoc));
          vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
          v_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
        }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    return v_Img;
  }
  //-------------------------------------------------------------------------------------------
  public void Cursatura(String pcIdr, Vector g_Tab_Img, RunData data, Context context) throws Exception 
  {
    g_Evt = (BF_EVT.Buf_Evt)data.getUser().getTemp("Agtp619_Evt");
    rg.MoverA(g_Evt.Evt_Est, "ENTMT");
    rg.MoverA(g_Evt.Evt_Fpr, g_Msg.Msg_Fch);
    BF_EVT t = new BF_EVT();
    g_MsgED100 = MSGED100.Inicia_MsgED100();
    rg.MoverA(g_MsgED100.ED100_Idr, pcIdr);
    rg.MoverA(g_MsgED100.ED100_Nev, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_MsgED100.ED100_Evt, t.LSet_De_Evt(g_Evt));
    rg.MoverA(g_Msg.Msg_Dat, MSGED100.LSet_De_MsgED100(g_MsgED100));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED500");
  }
  //===============================================================================================================================
}