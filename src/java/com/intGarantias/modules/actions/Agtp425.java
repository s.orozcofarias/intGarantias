// Source File Name:   Agtp425.java
// Descripcion     :   Consulta Detalle Hipoteca - Obra.Complementaria (GT425)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.BF_LOB;

import com.intGarantias.modules.global.MSGGT425;
import com.intGarantias.modules.global.PRMGT421;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp425 extends FHTServletAction
{
  //---------------------------------------------------------------------------------------
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  BF_LOB lob                       = new BF_LOB();
  MSGGT425.Buf_MsgGT425 g_MsgGT425 = new MSGGT425.Buf_MsgGT425();
  PRMGT421.Buf_PrmGT421 g_PrmGT421 = new PRMGT421.Buf_PrmGT421();
  //---------------------------------------------------------------------------------------
  public Agtp425()
  {
  }
  //---------------------------------------------------------------------------------------
  public void doPerform(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP425[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp425-DIN", data);
    doAgtp425_Init(data, context);
  }
  //---------------------------------------------------------------------------------------
  public void doCancel(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP425[doCancel.start]", "[CANCELADO]");
    setTemplate(data, "Garantias,Agt,AGTP425.vm");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp425_Init(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP425[doAgtp425_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_PrmGT421 = PRMGT421.LSet_A_PrmGT421(g_Msg.Msg_Dat.toString());
    g_MsgGT425 = MSGGT425.Inicia_MsgGT425();
    rg.MoverA(g_MsgGT425.GT425_Idr, "I");
    rg.MoverA(g_MsgGT425.GT425_Gti, g_PrmGT421.GT421_Gti);
    rg.MoverA(g_MsgGT425.GT425_Seq, g_PrmGT421.GT421_Seq);
    rg.MoverA(g_MsgGT425.GT425_Sqd, g_PrmGT421.GT421_Sqd);
    rg.MoverA(g_Msg.Msg_Dat, MSGGT425.LSet_De_MsgGT425(g_MsgGT425));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GT425");
    g_MsgGT425 = MSGGT425.LSet_A_MsgGT425(g_Msg.Msg_Dat.toString());
    data.getUser().removeTemp("Agtp425_MsgGT425");
    data.getUser().setTemp("Agtp425_MsgGT425", g_MsgGT425);
    if (g_MsgGT425.GT425_Obt.toString().trim().equals("")
      ||g_MsgGT425.GT425_Obt.toString().trim().equals("000000000"))
       { data.getUser().setTemp("Agtp425_Obt", ""); }
    else
       { data.getUser().setTemp("Agtp425_Obt", lob.leeCLOB(g_MsgGT425.GT425_Obt.toString())); }
    if (g_MsgGT425.GT425_Ovb.toString().trim().equals("")
      ||g_MsgGT425.GT425_Ovb.toString().trim().equals("000000000"))
       { data.getUser().setTemp("Agtp425_Ovb", ""); }
    else
       { data.getUser().setTemp("Agtp425_Ovb", lob.leeCLOB(g_MsgGT425.GT425_Ovb.toString())); }
    setTemplate(data, "Garantias,Agt,AGTP425.vm" );
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp425(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP425[doAgtp425.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp425-MAN", data);
    String Opc = data.getParameters().getString("Opc" ,"");
    if (Opc.trim().equals("S"))
       {
         //Boton Salir
         data.getUser().removeTemp("Agtp425_MsgGT425");
         BF_MSG.Return_Data("Agtp425", data, g_Msg);
         return;
       }
  }
  //---------------------------------------------------------------------------------------
}