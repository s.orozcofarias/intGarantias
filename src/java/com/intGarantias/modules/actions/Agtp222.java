// Source File Name:   Agtp222.java

package com.intGarantias.modules.actions;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.BF_EVT;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.PRMPC080;
import com.FHTServlet.modules.global.RUTGEN;
import com.intGlobal.modules.global.MSGED100;
import com.intGlobal.modules.global.PRMTG116;

public class Agtp222 extends FHTServletAction {

	public Agtp222() {
		rg = new RUTGEN();
		msg = new BF_MSG();
		g_Msg = new com.FHTServlet.modules.global.BF_MSG.Buf_Msg();
		g_Evt = new com.FHTServlet.modules.global.BF_EVT.Buf_Evt();
		g_PrmPC080 = new com.FHTServlet.modules.global.PRMPC080.PrmPC080();
		g_MsgED100 = new com.intGlobal.modules.global.MSGED100.Buf_MsgED100();
		g_PrmTG116 = new com.intGlobal.modules.global.PRMTG116.Buf_PrmTG116();
	}

	public void doPerform(RunData data, Context context) throws Exception {
		g_Msg = BF_MSG.Init_Program("Agtp222-DIN", data);
		if (g_Msg.Msg_Pgr.toString().equals("RT"))
			doAgtp222_Continue(data, context);
		else
			doAgtp222_Init(data, context);
	}

	public void doCancel(RunData data, Context context) throws Exception {
		setTemplate(data, "Garantias,Agt,AGTP222.vm");
	}

	public void doAgtp222_Init(RunData data, Context context) throws Exception {
		g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
		Carga_Inicial(data, context);
		RUTGEN.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
		BF_MSG.Param_Program(data, g_Msg);
		setTemplate(data, "Garantias,Agt,AGTP222.vm");
	}

	public void doAgtp222(RunData data, Context context) throws Exception {
		g_Msg = BF_MSG.Init_Program("Agtp222-MAN", data);
		g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
		String Opc = data.getParameters().getString("Opc", "");
		if (Opc.trim().equals("S")) {
			data.getUser().removeTemp("Agtp222_PrmPC080");
			data.getUser().removeTemp("Agtp222_Evt");
			RUTGEN.MoverA(g_PrmPC080.PC080_Rtn, "NK");
			RUTGEN.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
			BF_MSG.Return_Data("Agtp222", data, g_Msg);
			return;
		}
		if (Opc.trim().equals("A")) {
			Cursatura(data, context);
			data.getUser().removeTemp("Agtp222_PrmPC080");
			data.getUser().removeTemp("Agtp222_Evt");
			RUTGEN.MoverA(g_PrmPC080.PC080_Rtn, "OK");
			RUTGEN.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
			BF_MSG.Return_Data("Agtp222", data, g_Msg);
			return;
		}
		if (Opc.trim().equals("1")) {
			data.getUser().removeTemp("Agtp222_g_PrmPC080");
			data.getUser().setTemp("Agtp222_g_PrmPC080", g_PrmPC080);
			g_PrmTG116 = PRMTG116.Inicia_PrmTG116();
			RUTGEN.MoverA(g_Msg.Msg_Dat, PRMTG116.LSet_De_PrmTG116(g_PrmTG116));
			BF_MSG.Link_Program("Agtp222", "Atgp116", data, g_Msg);
			return;
		} else {
			return;
		}
	}

	public void doAgtp222_Continue(RunData data, Context context)
			throws Exception {
		if (RUTGEN.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Atgp116")) {
			g_PrmTG116 = PRMTG116.LSet_A_PrmTG116(g_Msg.Msg_Dat.toString());
			g_PrmPC080 = (com.FHTServlet.modules.global.PRMPC080.PrmPC080) data
					.getUser().getTemp("Agtp222_g_PrmPC080");
			data.getUser().removeTemp("Agtp222_g_PrmPC080");
			if (g_PrmTG116.TG116_Eje.toString().trim().compareTo("") != 0) {
				g_Evt = (com.FHTServlet.modules.global.BF_EVT.Buf_Evt) data
						.getUser().getTemp("Agtp222_Evt");
				RUTGEN.MoverA(g_Evt.Evt_Eje, g_PrmTG116.TG116_Eje);
				RUTGEN.MoverA(g_Evt.Evt_Nej, g_PrmTG116.TG116_Nej);
				RUTGEN.MoverA(g_Evt.Evt_Tej, g_PrmTG116.TG116_Tej);
				data.getUser().removeTemp("Agtp222_Evt");
				data.getUser().setTemp("Agtp222_Evt", g_Evt);
			}
			RUTGEN.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
			BF_MSG.Param_Program(data, g_Msg);
			setTemplate(data, "Garantias,Agt,AGTP222.vm");
			return;
		} else {
			return;
		}
	}

	public void Carga_Inicial(RunData data, Context context) throws Exception {
		g_MsgED100 = MSGED100.Inicia_MsgED100();
		RUTGEN.MoverA(g_MsgED100.ED100_Idr, "Q");
		RUTGEN.MoverA(g_MsgED100.ED100_Nev, g_PrmPC080.PC080_Ntr);
		RUTGEN.MoverA(g_Msg.Msg_Dat, MSGED100.LSet_De_MsgED100(g_MsgED100));
		g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED100");
		g_MsgED100 = MSGED100.LSet_A_MsgED100(g_Msg.Msg_Dat.toString());
		g_Evt = BF_EVT.LSet_A_Evt(g_MsgED100.ED100_Evt.toString());
		RUTGEN.MoverA(g_PrmPC080.PC080_Cli, g_Evt.Evt_Cli);
		RUTGEN.MoverA(g_PrmPC080.PC080_Ncl, g_Evt.Evt_Ncl);
		RUTGEN.MoverA(g_PrmPC080.PC080_Dir, g_Evt.Evt_Dir);
		data.getUser().removeTemp("Agtp222_Evt");
		data.getUser().setTemp("Agtp222_Evt", g_Evt);
		data.getUser().removeTemp("Agtp222_PrmPC080");
		data.getUser().setTemp("Agtp222_PrmPC080", g_PrmPC080);
	}

	public void Cursatura(RunData data, Context context) throws Exception {
		g_Evt = (com.FHTServlet.modules.global.BF_EVT.Buf_Evt) data.getUser()
				.getTemp("Agtp222_Evt");
		RUTGEN.MoverA(g_Evt.Evt_Fll, String.valueOf(g_PrmPC080.PC080_Trt)
				+ "ENEJH");
		g_MsgED100 = MSGED100.Inicia_MsgED100();
		RUTGEN.MoverA(g_MsgED100.ED100_Idr, "M");
		RUTGEN.MoverA(g_MsgED100.ED100_Nev, g_PrmPC080.PC080_Ntr);
		BF_EVT t = new BF_EVT();
		RUTGEN.MoverA(g_MsgED100.ED100_Evt, t.LSet_De_Evt(g_Evt));
		RUTGEN.MoverA(g_Msg.Msg_Dat, MSGED100.LSet_De_MsgED100(g_MsgED100));
		g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED100");
	}

	RUTGEN rg;
	BF_MSG msg;
	com.FHTServlet.modules.global.BF_MSG.Buf_Msg g_Msg;
	com.FHTServlet.modules.global.BF_EVT.Buf_Evt g_Evt;
	com.FHTServlet.modules.global.PRMPC080.PrmPC080 g_PrmPC080;
	com.intGlobal.modules.global.MSGED100.Buf_MsgED100 g_MsgED100;
	com.intGlobal.modules.global.PRMTG116.Buf_PrmTG116 g_PrmTG116;
}
