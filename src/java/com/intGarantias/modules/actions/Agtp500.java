// Source File Name:   Agtp500.java
// Descripcion:        Agenda de Novedades (GT500)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.PRMPC080;

import com.intGarantias.modules.global.MSGGT500;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp500 extends FHTServletAction
{
  //===============================================================================================================================
  public int    i = 0;
  public String lcRetorno = "";
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  PRMPC080.PrmPC080 g_PrmPC080     = new PRMPC080.PrmPC080();
  MSGGT500.Buf_MsgGT500 g_MsgGT500 = new MSGGT500.Buf_MsgGT500();
  //-------------------------------------------------------------------------------------------
  public Agtp500()
  {
  }
  //===============================================================================================================================
  public void doPerform(RunData data, Context context) throws Exception
  {
    //Log.debug("AGTP500[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp500-DIN", data);
    doAgtp500_Init(data, context);
  }
  //===============================================================================================================================
  public void doAgtp500_Init(RunData data, Context context) throws Exception
  {
    //Log.debug("AGTP500[doAgtp500_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.getDat());
    g_MsgGT500 = MSGGT500.Inicia_MsgGT500();
    rg.MoverA(g_MsgGT500.GT500_Idr, "I");
    rg.MoverA(g_MsgGT500.GT500_Idx, "001");
    rg.MoverA(g_MsgGT500.GT500_Eje, g_PrmPC080.PC080_Eje);
    Poblar_Panel(data, context, "I");
    setTemplate(data, "Garantias,Agt,AGTP500.vm");
  }
  //===============================================================================================================================
  public void doAgtp500(RunData data, Context context) throws Exception
  {
    //Log.debug("AGTP500[doAgtp500.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp500-MAN", data);
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.getDat());
    String Opc = data.getParameters().getString("Opc" ,"");
    if  (Opc.trim().equals("S"))
        {
          //Boton Salir
          data.getUser().removeTemp("Agtp500_Msg");
          data.getUser().removeTemp("Agtp500_MsgGT500");
          BF_MSG.Return_Program(data, g_Msg);
          return;
        }
    g_MsgGT500 = (MSGGT500.Buf_MsgGT500)data.getUser().getTemp("Agtp500_MsgGT500");
    MSGGT500.Bff_MsgGT500 d_MsgGT500 = new MSGGT500.Bff_MsgGT500();
    if  (Opc.equals("E"))
        {
          //Selecion. Ir a pantalla consulta garantia especifica
          String Seq = data.getParameters().getString("Seq", "");
          i = Integer.parseInt(Seq);
          d_MsgGT500 = (MSGGT500.Bff_MsgGT500)g_MsgGT500.GT500_Rgs.elementAt(i);
          rg.MoverA(g_PrmPC080.PC080_Cnr.Sis, d_MsgGT500.GT500_Gti.substring(0,3));
          rg.MoverA(g_PrmPC080.PC080_Cnr.Ncn, d_MsgGT500.GT500_Gti.substring(3));
          rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
          BF_MSG.Link_Program("Agtp500", "Agtp451", data, g_Msg);
          return;
        }
    if  (Opc.equals("N"))
        {
          //Siguiente
          Poblar_Panel(data, context, "S");
          setTemplate(data, "garantias,Agt,AGTP500.vm" );
          return;
        }
    if  (Opc.equals("P"))
        {
          //Previo
          Poblar_Panel(data, context, "P");
          setTemplate(data, "garantias,Agt,AGTP500.vm" );
          return;
        }
  }
  //===============================================================================================================================
  public void Poblar_Panel(RunData data, Context context, String pcIdr) throws Exception
  {
    MSGGT500.Bff_MsgGT500 d_MsgGT500 = new MSGGT500.Bff_MsgGT500();
    if (pcIdr.equals("S"))
       { i = g_MsgGT500.igetNro()-1; }
    if (pcIdr.equals("P"))
       { i = 0; }
    if (pcIdr.compareTo("I")!=0)
       {
         String lcEje = g_MsgGT500.getEje();
         d_MsgGT500 = (MSGGT500.Bff_MsgGT500)g_MsgGT500.GT500_Rgs.elementAt(i);
         g_MsgGT500 = MSGGT500.Inicia_MsgGT500();
         rg.MoverA(g_MsgGT500.GT500_Idr, pcIdr);
         rg.MoverA(g_MsgGT500.GT500_Idx, d_MsgGT500.GT500_Reg);
         rg.MoverA(g_MsgGT500.GT500_Eje, lcEje);
       }
    MSGGT500.Consulta_MsgGT500(g_MsgGT500);
    data.getUser().removeTemp("Agtp500_MsgGT500");
    data.getUser().setTemp("Agtp500_MsgGT500", g_MsgGT500);
    data.getUser().removeTemp("Agtp500_Msg");
    data.getUser().setTemp("Agtp500_Msg", g_Msg);
  }
  //===============================================================================================================================
}