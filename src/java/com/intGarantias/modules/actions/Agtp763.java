// Source File Name:   Agtp763.java
// Descripcion     :   Ingreso Tasacion Edificaciones (-)

package com.intGarantias.modules.actions;

//import com.FhtNucleo.services.configurator.FhtConfigurator;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.BF_LOB;
import com.FHTServlet.modules.global.BF_IMG;
import com.FHTServlet.modules.global.PRMPC080;
import com.FHTServlet.modules.util.ScreenHelper;

import com.intGlobal.modules.global.MSGED090;

import com.intGarantias.modules.global.GT_IMG;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp763 extends FHTServletAction
{
  //---------------------------------------------------------------------------------------
  public int    i = 0;
  public String lcData = "";
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  BF_LOB lob                       = new BF_LOB();
  PRMPC080.PrmPC080 g_PrmPC080     = new PRMPC080.PrmPC080();
  BF_IMG img                       = new BF_IMG();
  BF_IMG.Buf_Img g_Img             = new BF_IMG.Buf_Img();
  BF_IMG.Buf_Img_Bse g_Img_Base    = new BF_IMG.Buf_Img_Bse();
  GT_IMG gtimg                     = new GT_IMG();
  MSGED090.Buf_MsgED090 g_MsgED090 = new MSGED090.Buf_MsgED090();
  GT_IMG.Buf_Img_GtApe g_Img_GtApe = new GT_IMG.Buf_Img_GtApe();
  GT_IMG.Buf_Img_GtHyp g_Img_GtHyp = new GT_IMG.Buf_Img_GtHyp();
  GT_IMG.Buf_Img_GtHpd g_Img_GtHpd = new GT_IMG.Buf_Img_GtHpd();
  //---------------------------------------------------------------------------------------
  public Agtp763()
  {
  }
  //---------------------------------------------------------------------------------------
  public void doPerform(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP763[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp763-DIN", data);
    if (g_Msg.Msg_Pgr.toString().equals("RT"))
       { doAgtp763_Continue(data, context); }
    else
       { doAgtp763_Init(data, context); }
  }
  //---------------------------------------------------------------------------------------
  public void doCancel(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP763[doCancel.start]", "[CANCELADO]");
    setTemplate(data, "Garantias,Agt,AGTP763.vm");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp763_Init(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP763[doAgtp763_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    g_Img_GtHyp = (GT_IMG.Buf_Img_GtHyp)data.getUser().getTemp("Agtp727_ImgGtHyp");
    g_Img_GtHpd = (GT_IMG.Buf_Img_GtHpd)data.getUser().getTemp("Agtp727_ImgGtHpd");
    lcData = gtimg.LSet_De_ImgGtHpd(g_Img_GtHpd);
    g_Img_GtHpd = gtimg.LSet_A_ImgGtHpd(lcData);
    data.getUser().removeTemp("Agtp763_ImgGtHpd");
    data.getUser().setTemp("Agtp763_ImgGtHpd", g_Img_GtHpd);
    Carga_Combos(data, context);
    data.getUser().removeTemp("Agtp763_Ovb");
    if (g_Img_GtHpd.GtHpd_Ovb.toString().trim().equals("")
      ||g_Img_GtHpd.GtHpd_Ovb.toString().trim().equals("000000000"))
       { data.getUser().setTemp("Agtp763_Ovb", ""); }
    else
       { data.getUser().setTemp("Agtp763_Ovb", lob.leeCLOB(g_Img_GtHpd.GtHpd_Ovb.toString())); }
    data.getUser().removeTemp("Agtp763_Obt");
    if (g_Img_GtHpd.GtHpd_Obt.toString().trim().equals("")
      ||g_Img_GtHpd.GtHpd_Obt.toString().trim().equals("000000000"))
       { data.getUser().setTemp("Agtp763_Obt", ""); }
    else
       { data.getUser().setTemp("Agtp763_Obt", lob.leeCLOB(g_Img_GtHpd.GtHpd_Obt.toString())); }
    data.getUser().removeTemp("Agtp763_PrmPC080");
    data.getUser().setTemp("Agtp763_PrmPC080", g_PrmPC080);
    setTemplate(data, "Garantias,Agt,AGTP763.vm" );
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp763(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP763[doAgtp763.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp763-MAN", data);
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    String Opc = data.getParameters().getString("Opc", "");
    if (Opc.trim().equals("S"))
       {
         //Boton Salir
         data.getUser().removeTemp("Agtp763_PrmPC080");
         data.getUser().removeTemp("Agtp763_ImgGtHpd");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "NK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp763", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("R"))
       {
         //Boton RolSII
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp724_ImgGtApe");
         g_Img_GtHyp = (GT_IMG.Buf_Img_GtHyp)data.getUser().getTemp("Agtp727_ImgGtHyp");
         g_Img_GtHpd = (GT_IMG.Buf_Img_GtHpd)data.getUser().getTemp("Agtp763_ImgGtHpd");
         lcData = gtimg.LSet_De_ImgGtHpd(g_Img_GtHpd);
         g_Img_GtHpd = gtimg.LSet_A_ImgGtHpd(lcData);
         Actualiza_Datos(data, context);
         data.getUser().removeTemp("Agtp763_ImgGtHpd");
         data.getUser().setTemp("Agtp763_ImgGtHpd", g_Img_GtHpd);
         data.getUser().removeTemp("Agtp763_g_PrmPC080");
         data.getUser().setTemp("Agtp763_g_PrmPC080", g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Link_Program("Agtp763", "Agtp762", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("A"))
       {
         //Boton Aceptar
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp724_ImgGtApe");
         g_Img_GtHyp = (GT_IMG.Buf_Img_GtHyp)data.getUser().getTemp("Agtp727_ImgGtHyp");
         g_Img_GtHpd = (GT_IMG.Buf_Img_GtHpd)data.getUser().getTemp("Agtp763_ImgGtHpd");
         lcData = gtimg.LSet_De_ImgGtHpd(g_Img_GtHpd);
         g_Img_GtHpd = gtimg.LSet_A_ImgGtHpd(lcData);
         Actualiza_Datos(data, context);
         data.getUser().removeTemp("Agtp727_ImgGtHpd");
         data.getUser().setTemp("Agtp727_ImgGtHpd", g_Img_GtHpd);
         data.getUser().removeTemp("Agtp763_PrmPC080");
         data.getUser().removeTemp("Agtp763_ImgGtHpd");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp763", data, g_Msg);
         return;
       }
  }
  //---------------------------------------------------------------------------------------
  public void Actualiza_Datos(RunData data, Context context)
              throws Exception
  {
    int dPE = 0;
    int dUF = 4;
//    if (!FhtConfigurator.getPai().equals("CL"))
//       {
//         dPE = 2;
//         dUF = 2;
//       }
    rg.MoverA(g_Img_GtHpd.GtHpd_Umd, "M2");
    rg.MoverA(g_Img_GtHpd.GtHpd_Udd, data.getParameters().getString("GtHpd_Udd", ""));
    rg.MoverA(g_Img_GtHpd.GtHpd_Umt, data.getParameters().getString("GtHpd_Umt", ""));
    rg.MoverA(g_Img_GtHpd.GtHpd_Pum, data.getParameters().getString("GtHpd_Pum", ""));
    int dcm = 0;
    String vum = g_Img_GtApe.GtApe_Vuf.toString();
    if (g_Img_GtHpd.GtHpd_Umt.toString().equals("P"))
       {
         dcm = dPE;
         vum = rg.FmtValor("1",0,2,9,"+");
       }
    if (g_Img_GtHpd.GtHpd_Umt.toString().equals("U"))
       {
         dcm = dUF;
         vum = g_Img_GtApe.GtApe_Vuf.toString();
       }
    if (g_Img_GtHpd.GtHpd_Umt.toString().equals("D"))
       {
         dcm = 2;
         vum = g_Img_GtApe.GtApe_Vus.toString();
       }
    //rg.MoverA(g_Img_GtHpd.GtHpd_Vtu, rg.Multiplica(g_Img_GtHpd.GtHpd_Udd,2,g_Img_GtHpd.GtHpd_Pum,4,15,4,dcm));
    rg.MoverA(g_Img_GtHpd.GtHpd_Vtu, data.getParameters().getString("GtHpd_Vtu", ""));
    rg.MoverA(g_Img_GtHpd.GtHpd_Vbu, data.getParameters().getString("GtHpd_Vbu", ""));
    rg.MoverA(g_Img_GtHpd.GtHpd_Vlu, data.getParameters().getString("GtHpd_Vlu", ""));
    rg.MoverA(g_Img_GtHpd.GtHpd_Vsu, data.getParameters().getString("GtHpd_Vsu", ""));
    rg.MoverA(g_Img_GtHpd.GtHpd_Vts, rg.Multiplica(g_Img_GtHpd.GtHpd_Vtu,4,vum,2,15,2,dPE));
    rg.MoverA(g_Img_GtHpd.GtHpd_Vtb, rg.Multiplica(g_Img_GtHpd.GtHpd_Vbu,4,vum,2,15,2,dPE));
    rg.MoverA(g_Img_GtHpd.GtHpd_Vlq, rg.Multiplica(g_Img_GtHpd.GtHpd_Vlu,4,vum,2,15,2,dPE));
    rg.MoverA(g_Img_GtHpd.GtHpd_Vsg, rg.Multiplica(g_Img_GtHpd.GtHpd_Vsu,4,vum,2,15,2,dPE));
    if (g_Img_GtHpd.GtHpd_Vtb.toString().trim().equals("")
     || g_Img_GtHpd.GtHpd_Vtb.toString().equals(rg.Zeros(g_Img_GtHpd.GtHpd_Vtb)))
       {
         rg.MoverA(g_Img_GtHpd.GtHpd_Vbu, g_Img_GtHpd.GtHpd_Vtu);
         rg.MoverA(g_Img_GtHpd.GtHpd_Vtb, g_Img_GtHpd.GtHpd_Vts);
       }
    if (g_Img_GtHyp.GtHyp_Pct.toString().trim().compareTo("")!=0)
       {
         rg.MoverA(g_Img_GtHpd.GtHpd_Vcn, rg.Multiplica(g_Img_GtHpd.GtHpd_Vtb,2,g_Img_GtHyp.GtHyp_Pct,2,15,2,dPE));
         rg.MoverA(g_Img_GtHpd.GtHpd_Vcn, rg.Divide(g_Img_GtHpd.GtHpd_Vcn,2,"100",0,15,2,dPE));
       }
    rg.MoverA(g_Img_GtHpd.GtHpd_Ano, data.getParameters().getString("GtHpd_Ano", ""));
    rg.MoverA(g_Img_GtHpd.GtHpd_Str, data.getParameters().getString("GtHpd_Str", ""));
    rg.MoverA(g_Img_GtHpd.GtHpd_Uso, data.getParameters().getString("GtHpd_Uso", ""));
    rg.MoverA(g_Img_GtHpd.GtHpd_Fma, data.getParameters().getString("GtHpd_Fma", ""));
    rg.MoverA(g_Img_GtHpd.GtHpd_Tpe, data.getParameters().getString("GtHpd_Tpe", ""));
    rg.MoverA(g_Img_GtHpd.GtHpd_Dst, data.getParameters().getString("GtHpd_Dst", ""));
    rg.MoverA(g_Img_GtHpd.GtHpd_Ped, data.getParameters().getString("GtHpd_Ped", ""));
    rg.MoverA(g_Img_GtHpd.GtHpd_Fed, "");
    rg.MoverA(g_Img_GtHpd.GtHpd_Rem, data.getParameters().getString("GtHpd_Rem", ""));
    rg.MoverA(g_Img_GtHpd.GtHpd_Nam, data.getParameters().getString("GtHpd_Nam", ""));
    rg.MoverA(g_Img_GtHpd.GtHpd_Ams, data.getParameters().getString("GtHpd_Ams", ""));
    rg.MoverA(g_Img_GtHpd.GtHpd_Amn, data.getParameters().getString("GtHpd_Amn", ""));
    String Pgm = data.getParameters().getString("GtHpd_Pgm", "");
    g_Img_GtHpd.vGtHpd_Pgm = new Vector();
    int p = 0;
    for (i=0; i<17; i++)
        {
          lcData = Pgm.substring(p ,p + g_Img_GtHpd.GtHpd_Pgm.capacity());
          g_Img_GtHpd.vGtHpd_Pgm.add(lcData);
          p = p + g_Img_GtHpd.GtHpd_Pgm.capacity();
        }
    String Est = data.getParameters().getString("GtHpd_Est", "");
    g_Img_GtHpd.vGtHpd_Est = new Vector();
    p = 0;
    for (i=0; i<11; i++)
        {
          lcData = Est.substring(p ,p + g_Img_GtHpd.GtHpd_Est.capacity());
          g_Img_GtHpd.vGtHpd_Est.add(lcData);
          p = p + g_Img_GtHpd.GtHpd_Est.capacity();
        }
    String Ter = data.getParameters().getString("GtHpd_Ter", "");
    g_Img_GtHpd.vGtHpd_Ter = new Vector();
    p = 0;
    for (i=0; i<11; i++)
        {
          lcData = Ter.substring(p ,p + g_Img_GtHpd.GtHpd_Ter.capacity());
          g_Img_GtHpd.vGtHpd_Ter.add(lcData);
          p = p + g_Img_GtHpd.GtHpd_Ter.capacity();
        }
    String Pav = data.getParameters().getString("GtHpd_Pav", "");
    g_Img_GtHpd.vGtHpd_Pav = new Vector();
    p = 0;
    for (i=0; i<11; i++)
        {
          lcData = Pav.substring(p ,p + g_Img_GtHpd.GtHpd_Pav.capacity());
          g_Img_GtHpd.vGtHpd_Pav.add(lcData);
          p = p + g_Img_GtHpd.GtHpd_Pav.capacity();
        }
    String Cub = data.getParameters().getString("GtHpd_Cub", "");
    g_Img_GtHpd.vGtHpd_Cub = new Vector();
    p = 0;
    for (i=0; i<11; i++)
        {
          lcData = Cub.substring(p ,p + g_Img_GtHpd.GtHpd_Cub.capacity());
          g_Img_GtHpd.vGtHpd_Cub.add(lcData);
          p = p + g_Img_GtHpd.GtHpd_Cub.capacity();
        }
    String Ins = data.getParameters().getString("GtHpd_Int", "");
    g_Img_GtHpd.vGtHpd_Int = new Vector();
    p = 0;
    for (i=0; i<11; i++)
        {
          lcData = Ins.substring(p ,p + g_Img_GtHpd.GtHpd_Int.capacity());
          g_Img_GtHpd.vGtHpd_Int.add(lcData);
          p = p + g_Img_GtHpd.GtHpd_Int.capacity();
        }
    lcData = data.getParameters().getString("GtHpd_Obt", "");
    if (!lcData.trim().equals(""))
       { 
         if (g_Img_GtHpd.GtHpd_Obt.toString().trim().equals("")
           ||g_Img_GtHpd.GtHpd_Obt.toString().trim().equals("000000000"))
            { rg.MoverA(g_Img_GtHpd.GtHpd_Obt, Asigna_Folio("FOLIO-CLOBS", data, context)); }
         lob.mantieneCLOB("M", g_Img_GtHpd.GtHpd_Obt.toString(), lcData);
       }
    rg.MoverA(g_Img_GtHpd.GtHpd_Plq, data.getParameters().getString("GtHpd_Plq", ""));
    lcData = data.getParameters().getString("GtHpd_Ovb", "");
    if (!lcData.trim().equals(""))
       { 
         if (g_Img_GtHpd.GtHpd_Ovb.toString().trim().equals("")
           ||g_Img_GtHpd.GtHpd_Ovb.toString().trim().equals("000000000"))
            { rg.MoverA(g_Img_GtHpd.GtHpd_Ovb, Asigna_Folio("FOLIO-CLOBS", data, context)); }
         lob.mantieneCLOB("M", g_Img_GtHpd.GtHpd_Ovb.toString(), lcData);
       }
    data.getUser().removeTemp("Agtp763_Ovb");
    if (g_Img_GtHpd.GtHpd_Ovb.toString().trim().equals("")
      ||g_Img_GtHpd.GtHpd_Ovb.toString().trim().equals("000000000"))
       { data.getUser().setTemp("Agtp763_Ovb", ""); }
    else
       { data.getUser().setTemp("Agtp763_Ovb", lob.leeCLOB(g_Img_GtHpd.GtHpd_Ovb.toString())); }
    data.getUser().removeTemp("Agtp763_Obt");
    if (g_Img_GtHpd.GtHpd_Obt.toString().trim().equals("")
      ||g_Img_GtHpd.GtHpd_Obt.toString().trim().equals("000000000"))
       { data.getUser().setTemp("Agtp763_Obt", ""); }
    else
       { data.getUser().setTemp("Agtp763_Obt", lob.leeCLOB(g_Img_GtHpd.GtHpd_Obt.toString())); }
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp763_Continue(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP763[doAgtp763_Continue.start]", "[" + data.getUser().getUserName() + "]");
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Agtp762"))
       {
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp763_g_PrmPC080");
         data.getUser().removeTemp("Agtp763_g_PrmPC080");
         g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
         setTemplate(data, "Garantias,Agt,AGTP763.vm" );
         return;
       }
  }
  //---------------------------------------------------------------------------------------
  public String Asigna_Folio(String pcFolio, RunData data, Context context)
                throws Exception 
  {
    g_MsgED090 = MSGED090.Inicia_MsgED090();
    rg.MoverA(g_MsgED090.ED090_Idr, "F");
    rg.MoverA(g_MsgED090.ED090_Cod, pcFolio);
    rg.MoverA(g_Msg.Msg_Dat, MSGED090.LSet_De_MsgED090(g_MsgED090));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED090");
    g_MsgED090 = MSGED090.LSet_A_MsgED090(g_Msg.Msg_Dat.toString());
    String lcFolio = rg.FmtValor(g_MsgED090.ED090_Fol.toString(),0,0,9,"");
    return lcFolio;
  }
  //---------------------------------------------------------------------------------------
  public void Carga_Combos(RunData data, Context context)
              throws Exception
  {
    Vector vCod1 = ScreenHelper.vecCodGen("GAR-HED-USO");
    data.getUser().removeTemp("Agtp763_tGarHedUso");
    data.getUser().setTemp("Agtp763_tGarHedUso", vCod1);
    Vector vCod2 = ScreenHelper.vecCodGen("GAR-HED-AGP");
    data.getUser().removeTemp("Agtp763_tGarHedAgp");
    data.getUser().setTemp("Agtp763_tGarHedAgp", vCod2);
    Vector vCod3 = ScreenHelper.vecCodGen("GAR-HED-TCT");
    data.getUser().removeTemp("Agtp763_tGarHedTct");
    data.getUser().setTemp("Agtp763_tGarHedTct", vCod3);
    Vector vCod4 = ScreenHelper.vecCodGen("GAR-HED-CLD");
    data.getUser().removeTemp("Agtp763_tGarHedCld");
    data.getUser().setTemp("Agtp763_tGarHedCld", vCod4);
  }
  //---------------------------------------------------------------------------------------
}