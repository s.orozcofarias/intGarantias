package com.intGarantias.modules.actions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.turbine.services.resources.TurbineResources;
import org.apache.turbine.util.Log;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.intGarantias.bean.GtiasTasablesEnTramiteBean;
import com.intGarantias.dao.ConsultasBaseDatos;
import com.intGarantias.util.CrearExcel;
import com.intGarantias.util.DescargaDoc;


public class GarantiasTasablesEnTramite extends FHTServletAction {
	

	
	public GarantiasTasablesEnTramite() {}
	
	
	public void doPerform(RunData data, Context context) throws Exception {
		Log.debug("GarantiasTasablesEnTramite.doPerform");

		 ConsultasBaseDatos consulta = new ConsultasBaseDatos();
		 DescargaDoc descargaDoc = new DescargaDoc();

		 
		 List listDatGar = consulta.consultaDetalleTasadores();
		 
		 if(listDatGar.size() == 0){
			 Log.debug("No se encontraron datos");
			 return;
		 }
		 
		 Log.debug("Cantidad de datos : " + listDatGar.size());
		 
		 try{
			 File temp = this.generarExcel(listDatGar);			 
			 descargaDoc.descargarArchivo(data, temp);
			 
		 }catch(IOException e){
			 Log.error("No se pudo completar la descarga ", e);			 
		 }catch(Exception ex){
			 Log.error("No se pudo completar la descarga ", ex);
		 }
		 
		 setTemplate(data, "Garantias,Agt,Agtp800.vm" );
	}
	

	public File generarExcel(List garantiasData) throws FileNotFoundException, IOException{
		Log.debug("GarantiasTasablesEnTramite.generarExcel");
		CrearExcel creaExcel = new CrearExcel();
		
		Log.debug("prop : " + TurbineResources.getString("pathPlantillas"));
		Log.debug("prop : " + TurbineResources.getString("plantillaDetalle"));
		
		String pathPlantilla = 	TurbineResources.getString("pathPlantillas");
		String nombrePlantilla = TurbineResources.getString("plantillaDetalle");
		Log.debug("path absoluto " + pathPlantilla + nombrePlantilla);
		
		File file = new File(pathPlantilla + nombrePlantilla);		

//		POIFSFileSystem excel = new POIFSFileSystem(this.getClass().getClassLoader().getResourceAsStream("recursos/PLANTILLA_DETALLE_TASACIONES.xls")); 
		POIFSFileSystem excel = new POIFSFileSystem(new FileInputStream(file));
		HSSFWorkbook wb = new HSSFWorkbook(excel);
        HSSFSheet HojaExcelDetalle = wb.getSheetAt(0);
		HSSFRow fila = null;
    	HSSFCell cell = null;

		 
		 for(int i = 0 ; i < garantiasData.size() ; i++){

			 try
			 {
			 
			 fila = creaExcel.createRowFrom(HojaExcelDetalle, HojaExcelDetalle.getRow(1), i+1);
			 
			 cell = fila.getCell((short)0);
			 cell.setCellValue( ((GtiasTasablesEnTramiteBean) garantiasData.get(i)).getNumGarantia());

			 cell = fila.getCell((short)1);
			 cell.setCellValue(((GtiasTasablesEnTramiteBean) garantiasData.get(i)).getCodTransaccion());

			 cell = fila.getCell((short)2);
			 cell.setCellValue(((GtiasTasablesEnTramiteBean) garantiasData.get(i)).getGlsTransaccion());

			 cell = fila.getCell((short)3);
			 cell.setCellValue(((GtiasTasablesEnTramiteBean) garantiasData.get(i)).getCodProducto());

			 cell = fila.getCell((short)4);
			 cell.setCellValue(((GtiasTasablesEnTramiteBean) garantiasData.get(i)).getCodTasador());

			 cell = fila.getCell((short)5);
			 cell.setCellValue(((GtiasTasablesEnTramiteBean) garantiasData.get(i)).getCodTramite());

			 cell = fila.getCell((short)6);
			 cell.setCellValue(((GtiasTasablesEnTramiteBean) garantiasData.get(i)).getGlsTramite());

			 cell = fila.getCell((short)7);
			 cell.setCellValue(((GtiasTasablesEnTramiteBean) garantiasData.get(i)).getFecIniTram());

			 cell = fila.getCell((short)8);
			 cell.setCellValue(((GtiasTasablesEnTramiteBean) garantiasData.get(i)).getHrIniTram());

			 cell = fila.getCell((short)9);
			 cell.setCellValue(((GtiasTasablesEnTramiteBean) garantiasData.get(i)).getCodEstado());

			 cell = fila.getCell((short)10);
			 cell.setCellValue(((GtiasTasablesEnTramiteBean) garantiasData.get(i)).getGlsEstado());

			 cell = fila.getCell((short)11);
			 cell.setCellValue(((GtiasTasablesEnTramiteBean) garantiasData.get(i)).getRutCliente());

			 cell = fila.getCell((short)12);
			 cell.setCellValue(((GtiasTasablesEnTramiteBean) garantiasData.get(i)).getNomCliente());

			 cell = fila.getCell((short)13);
			 cell.setCellValue(((GtiasTasablesEnTramiteBean) garantiasData.get(i)).getCodEjecutivo());

			 cell = fila.getCell((short)14);
			 cell.setCellValue(((GtiasTasablesEnTramiteBean) garantiasData.get(i)).getGlsEjeCli());

			 cell = fila.getCell((short)15);
			 cell.setCellValue(((GtiasTasablesEnTramiteBean) garantiasData.get(i)).getCtaCorriente());

			 cell = fila.getCell((short)16);
			 cell.setCellValue(((GtiasTasablesEnTramiteBean) garantiasData.get(i)).getValTasado());

			 cell = fila.getCell((short)17);
			 cell.setCellValue(((GtiasTasablesEnTramiteBean) garantiasData.get(i)).getFecTasacion());	
			 
			 cell = fila.getCell((short)18);
			 cell.setCellValue(((GtiasTasablesEnTramiteBean) garantiasData.get(i)).getFecAceptadoTas());	
			 
			 cell = fila.getCell((short)19);
			 cell.setCellValue(((GtiasTasablesEnTramiteBean) garantiasData.get(i)).getDiasPenFec());	
			 
			 cell = fila.getCell((short)20);
			 cell.setCellValue(((GtiasTasablesEnTramiteBean) garantiasData.get(i)).getReenvioTas());	
			 
			 cell = fila.getCell((short)21);
			 cell.setCellValue(((GtiasTasablesEnTramiteBean) garantiasData.get(i)).getTerminadoTas());	
			 
			 }catch(NullPointerException ex){
				 Log.debug("Falla creacion de excel " + ((GtiasTasablesEnTramiteBean) garantiasData.get(i)).getNumGarantia(), ex);
			 }catch(Exception e){
				 Log.error("No se pudo completar la descarga ResGtiasTasador", e);
			 }

		 }
		 
	      File temp = File.createTempFile("Listado_de_Garantias_UVT_", ".xls");
	      temp.deleteOnExit();
	      
	      FileOutputStream fileOut = new FileOutputStream(temp);
		  wb.write(fileOut);
	      fileOut.close();
	      Log.debug("fileOut.close();");
	      return temp;
	}	
	

}
