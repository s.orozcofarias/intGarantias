// Source File Name:   Agtp800.java
// Descripcion     :   Menu UVT (-)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.PRMPC080;
import com.FHTServlet.modules.global.PRMCN080;

import com.intGlobal.modules.global.PRMCL090;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp800 extends FHTServletAction
{
  //---------------------------------------------------------------------------------------
  public String lcData = "";
  public Vector vDatos;
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  PRMPC080.PrmPC080 g_PrmPC080     = new PRMPC080.PrmPC080();
  PRMCN080.PrmCN080 g_PrmCN080     = new PRMCN080.PrmCN080();
  PRMCL090.Buf_PrmCL090 g_PrmCL090 = new PRMCL090.Buf_PrmCL090();

  //---------------------------------------------------------------------------------------
  public Agtp800()
  {
  }
  //---------------------------------------------------------------------------------------
  public void doPerform(RunData data, Context context)
              throws Exception
  {
  //Log.debug("AGTP800[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");

    g_Msg = BF_MSG.Init_Program("Agtp800-DIN", data);
    if (g_Msg.Msg_Pgr.toString().equals("RT"))
       if (RUTGEN.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Appp030"))
          { doAgtp800_Init(data, context); }
       else
          { doAgtp800_Continue(data, context); }
    else
       { doAgtp800_Init(data, context); }
  }
  //---------------------------------------------------------------------------------------
  public void doCancel(RunData data, Context context)
              throws Exception
  {
  //Log.debug("AGTP800[doCancel.start]", "[CANCELADO]");
    setTemplate(data, "Garantias,Agt,AGTP800.vm");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp800_Init(RunData data, Context context)
              throws Exception
  {
  //Log.debug("AGTP800[doAgtp800_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp800-MAN", data);
    Nueva_PrmPC080();
    BF_MSG.Param_Program(data, g_Msg);
    setTemplate(data, "Garantias,Agt,AGTP800.vm" );
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp800(RunData data, Context context)
              throws Exception
  {
	 
  //String lc_MsgTot = (String)data.getUser().getTemp("g_Msg", "");
  //if (lc_MsgTot.trim().equals("") || lc_MsgTot.substring(150).trim().equals("BICE"))
  //   { 
      	 g_Msg = BF_MSG.InitIntegra("Agtp800", data, context); 
         Nueva_PrmPC080();
         BF_MSG.Param_Program(data, g_Msg);
  //   }
  //else
  //   { g_Msg = BF_MSG.Init_Program(data); }
    String Opc = data.getParameters().getString("Opc", "");
    if (Opc.trim().equals("S"))                    //Boton Salir
       { BF_MSG.Return_Program(data, g_Msg); }
    if (Opc.equals("1"))                           //Solicitudes en Tr�mite
       { BF_MSG.Link_Program("Agtp800", "Agtp201", data, g_Msg); }
    if (Opc.equals("2"))                           //Garant�as en Tramitaci�n
      { BF_MSG.Link_Program("Agtp800", "Aedp102", data, g_Msg);   	}
    if (Opc.equals("3"))                            //Mantenci�n de Tasadores
       { BF_MSG.Link_Program("Agtp800", "Atgp111", data, g_Msg); }
    if (Opc.equals("4"))                           //Consultas de Hipotecas y Prendas
       { BF_MSG.Link_Program("Agtp800", "Agtp520", data, g_Msg); }
    if (Opc.equals("5"))                           //Estad�sticas de Tasaciones
       { BF_MSG.Link_Program("Agtp800", "Agtp600", data, g_Msg); }
    if (Opc.equals("6"))                           //Consultas Operativas de Garant�as
       { 
         RUTGEN.MoverA(g_PrmPC080.PC080_Acc, "CONSULTAS");
         RUTGEN.MoverA(g_Msg.Msg_Dat,        PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
       	 BF_MSG.Link_Program("Agtp800", "Agtp400", data, g_Msg); 
       }
    if (Opc.equals("7"))                           //Consultas Contables de Garant�as
       { 
         Nueva_PrmCN080();
       	 BF_MSG.Link_Program("Agtp800", "Acnp420", data, g_Msg); 
       }
    if (Opc.equals("8"))
       //Modifica Garant�a Constituida
       { 
         g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
         String Ncn = data.getParameters().getString("Ncn", "");
         if (Ncn.trim().equals(""))
            {
              rg.MoverA(g_PrmPC080.PC080_Opc, Opc);
              data.getUser().removeTemp("Agtp800_g_PrmPC080");
              data.getUser().setTemp("Agtp800_g_PrmPC080" ,PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
              g_PrmCL090 = PRMCL090.Inicia_PrmCL090();
              rg.MoverA(g_PrmCL090.CL090_Pcc, "N");
              rg.MoverA(g_Msg.Msg_Dat, PRMCL090.LSet_De_PrmCL090(g_PrmCL090));
              BF_MSG.Link_Program("Agtp800", "Eclp090", data, g_Msg);
              return;
            }
         else
            { 
              rg.MoverA(g_PrmPC080.PC080_Cnr.Ncn, Ncn);
              doAgtp800_Despacha(data, context, Opc);
            }
       }
    if(Opc.equals("9")){
    	BF_MSG.Link_Program("Agtp800", "GarantiasTasablesEnTramite", data, g_Msg);
    }
    
    if(Opc.equals("10")){
    	BF_MSG.Link_Program("Agtp800", "ResGtiasTasador", data, g_Msg);
    }    
    
	if (Opc.equals("11")){ //Informe pago tasadores 
		BF_MSG.Link_Program("Agtp800", "InformePagoTasadores", data, g_Msg); 
    }
	
	if (Opc.equals("12")){ //Consulta Facturas de Garant�as
		BF_MSG.Link_Program("Agtp800", "Agtp231", data, g_Msg); 
    }
    
    return;
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp800_Continue(RunData data, Context context)
              throws Exception
  {
  //Log.debug("AGTP800[doAgtp800_Continue.start]", "[" + data.getUser().getUserName() + "]");
    if (RUTGEN.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Eclp090"))
       {
         PRMPC080 vPrmPC080 = new PRMPC080();
         vDatos = vPrmPC080.LSet_A_vPrmPC080((String)data.getUser().getTemp("Agtp800_g_PrmPC080"));
         g_PrmPC080 = (PRMPC080.PrmPC080)vDatos.elementAt(0);
         data.getUser().removeTemp("Agtp800_g_PrmPC080");
         g_PrmCL090 = PRMCL090.LSet_A_PrmCL090(g_Msg.Msg_Dat.toString());
         if (g_PrmCL090.CL090_Cli.toString().trim().equals(""))
            {
              doAgtp800_Init(data, context);
              return;
            }
         rg.MoverA(g_PrmPC080.PC080_Cli, g_PrmCL090.CL090_Cli);
         rg.MoverA(g_PrmPC080.PC080_Ncl, g_PrmCL090.CL090_Ncl);
         rg.MoverA(g_PrmPC080.PC080_Tpr, g_PrmCL090.CL090_Tpr);
         rg.MoverA(g_PrmPC080.PC080_Acc, "SELECCIONA");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Link_Program("Agtp800", "Agtp400", data, g_Msg);
         return;
       }
    if (RUTGEN.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Agtp400"))
       {
         g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
         if (g_PrmPC080.PC080_Rtn.toString().trim().equals("NK"))
            { doAgtp800_Init(data, context); }
         else
            { doAgtp800_Despacha(data, context, g_PrmPC080.PC080_Opc.toString().trim()); }
         return;
       }
    doAgtp800_Init(data, context);
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp800_Despacha(RunData data, Context context, String Opc)
              throws Exception
  {
	  
  //Log.debug("AGTP800[doAgtp800_Despacha.start]", "[" + data.getUser().getUserName() + "]");
    rg.MoverA(g_PrmPC080.PC080_Rtn, " ");
    String lcPrograma = "";
    if (Opc.equals("8"))
       //Modifica Garant�a Constituida
       {
         rg.MoverA(g_PrmPC080.PC080_Acc, "MDTAS");
         rg.MoverA(g_PrmPC080.PC080_Ttr, "MDTAS");
         rg.MoverA(g_PrmPC080.PC080_Ntt, "MODIFICA GARANTIA CONSTITUIDA");
         lcPrograma = "Agtp640";
       }
    rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
    BF_MSG.Link_Program("Agtp800", lcPrograma, data, g_Msg);
  }
  //---------------------------------------------------------------------------------------
  public void Nueva_PrmPC080() 
  {
    g_PrmPC080 = PRMPC080.Inicia_PrmPC080();
    rg.MoverA(g_PrmPC080.PC080_Fte,     "SPV");
    rg.MoverA(g_PrmPC080.PC080_Pai,     "CL");
    rg.MoverA(g_PrmPC080.PC080_Cnr.Sis, "GAR");
    rg.MoverA(g_PrmPC080.PC080_Slr.Slc, rg.Zeros(7));
    rg.MoverA(g_PrmPC080.PC080_Suc,     g_Msg.Msg_Suc);
    rg.MoverA(g_PrmPC080.PC080_Nsu,     g_Msg.Msg_Nsu);
    rg.MoverA(g_PrmPC080.PC080_Mnd,     "000");
    rg.MoverA(g_PrmPC080.PC080_Nmn,     "PESOS CHILE");
    rg.MoverA(g_PrmPC080.PC080_Eje,     g_Msg.Msg_Cus);
    rg.MoverA(g_PrmPC080.PC080_Nej,     g_Msg.Msg_Nus);
    rg.MoverA(g_PrmPC080.PC080_Tej,     g_Msg.Msg_Fus);
    rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
  }
  //---------------------------------------------------------------------------------------
  public void Nueva_PrmCN080() 
  {
    g_PrmCN080 = PRMCN080.Inicia_PrmCN080();
    RUTGEN.MoverA(g_PrmCN080.CN080_Rtn, "");
    RUTGEN.MoverA(g_PrmCN080.CN080_Fte, "SPR");
    RUTGEN.MoverA(g_PrmCN080.CN080_Acc, "CNSIS");
    RUTGEN.MoverA(g_PrmCN080.CN080_Suc, g_Msg.Msg_Suc);
    RUTGEN.MoverA(g_PrmCN080.CN080_Nsu, g_Msg.Msg_Nsu);
    RUTGEN.MoverA(g_PrmCN080.CN080_Mnd, "");
    RUTGEN.MoverA(g_PrmCN080.CN080_Nmn, "");
    RUTGEN.MoverA(g_PrmCN080.CN080_Sis, "GAR");
    RUTGEN.MoverA(g_Msg.Msg_Dat,        PRMCN080.LSet_De_PrmCN080(g_PrmCN080));
  }
  //---------------------------------------------------------------------------------------
}