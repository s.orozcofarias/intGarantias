// Source File Name:   Agtp140.java
// Descripcion     :   Inicia Tramite.Tasacion (ED100, ED125, GT140)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.BF_IMG;
import com.FHTServlet.modules.global.BF_EVT;
import com.FHTServlet.modules.global.PRMPC080;

import com.intGlobal.modules.global.MSGED100;
import com.intGlobal.modules.global.MSGED125;
import com.intGlobal.modules.global.MSGED135;
import com.intGlobal.modules.global.PRMED210;
import com.intGlobal.modules.global.PRMCC092;
import com.intGlobal.modules.global.PRMCC095;
import com.intGlobal.modules.global.PRMTG123;
import com.intGlobal.modules.global.PRMTG130;

import com.intGarantias.modules.global.MSGGT140;
import com.intGarantias.modules.global.MSGGT640;
import com.intGarantias.modules.global.GT_IMG;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp140 extends FHTServletAction
{
  //---------------------------------------------------------------------------------------
  public int i = 0;
  public String lcData = "";
  public Vector vDatos;
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  BF_EVT.Buf_Evt g_Evt             = new BF_EVT.Buf_Evt();
  BF_IMG img                       = new BF_IMG();
  BF_IMG.Buf_Img g_Img             = new BF_IMG.Buf_Img();
  BF_IMG.Buf_Img_Bse g_Img_Base    = new BF_IMG.Buf_Img_Bse();
  GT_IMG gtimg                     = new GT_IMG();
  GT_IMG.Buf_Img_GtApe g_Img_GtApe = new GT_IMG.Buf_Img_GtApe();
  GT_IMG.Buf_Img_GtTsd g_Img_GtTsd = new GT_IMG.Buf_Img_GtTsd();
  GT_IMG.Buf_Img_GtMff g_Img_GtMff = new GT_IMG.Buf_Img_GtMff();
  Vector v_Tab_Img                 = new Vector();
  PRMPC080.PrmPC080 g_PrmPC080     = new PRMPC080.PrmPC080();
  MSGED100.Buf_MsgED100 g_MsgED100 = new MSGED100.Buf_MsgED100();
  MSGED125.Buf_MsgED125 g_MsgED125 = new MSGED125.Buf_MsgED125();
  MSGED135.Buf_MsgED135 g_MsgED135 = new MSGED135.Buf_MsgED135();
  PRMED210.Buf_PrmED210 g_PrmED210 = new PRMED210.Buf_PrmED210();
  MSGGT140.Buf_MsgGT140 g_MsgGT140 = new MSGGT140.Buf_MsgGT140();
  MSGGT640.Buf_MsgGT640 g_MsgGT640 = new MSGGT640.Buf_MsgGT640();
  PRMCC092.Buf_PrmCC092 g_PrmCC092 = new PRMCC092.Buf_PrmCC092();
  PRMCC095.Buf_PrmCC095 g_PrmCC095 = new PRMCC095.Buf_PrmCC095();
  PRMTG123.Buf_PrmTG123 g_PrmTG123 = new PRMTG123.Buf_PrmTG123();
  PRMTG130.Buf_PrmTG130 g_PrmTG130 = new PRMTG130.Buf_PrmTG130();
  //---------------------------------------------------------------------------------------
  public Agtp140()
  {
  }
  //---------------------------------------------------------------------------------------
  public void doPerform(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP140[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp140-DIN", data);
    if (g_Msg.Msg_Pgr.toString().equals("RT"))
       { doAgtp140_Continue(data, context); }
    else
       { doAgtp140_Init(data, context); }
  }
  //---------------------------------------------------------------------------------------
  public void doCancel(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP140[doCancel.start]", "[CANCELADO]");
    setTemplate(data, "Garantias,Agt,AGTP140.vm");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp140_Init(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP140[doAgtp140_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    data.getUser().removeTemp("Agtp140_PrmPC080");
    data.getUser().setTemp("Agtp140_PrmPC080", g_PrmPC080);
    v_Tab_Img.clear();
    Recupera_Evt(data, context);
    Carga_de_Host(data, context);
    rg.MoverA(g_PrmPC080.PC080_Ttr, g_Img_Base.Img_Base_Ttr);
    if (v_Tab_Img.size()==1)
       {
         if (g_Img_Base.Img_Base_Ttr.toString().trim().equals("RETAS"))
             { Recupera_Retasa(data, context); }
         if (g_Img_Base.Img_Base_Ttr.toString().trim().equals("RETPI")
          || g_Img_Base.Img_Base_Ttr.toString().trim().equals("TERPI"))
             { Recupera_ImgTsd(data, context); }
       }
    data.getUser().removeTemp("Agtp140_ImgGtApe");
    data.getUser().setTemp("Agtp140_ImgGtApe", g_Img_GtApe);
    data.getUser().removeTemp("Agtp140_ImgGtTsd");
    data.getUser().setTemp("Agtp140_ImgGtTsd", g_Img_GtTsd);
    data.getUser().removeTemp("Agtp140_vTabImg");
    data.getUser().setTemp("Agtp140_vTabImg", v_Tab_Img);
    Carga_Doctos(data, context);
    rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
    BF_MSG.Param_Program(data, g_Msg);
    setTemplate(data, "Garantias,Agt,AGTP140.vm" );
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp140(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP140[doAgtp140.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp140-MAN", data);
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    String Opc = data.getParameters().getString("Opc", "");
    if (Opc.trim().equals("S"))
       {
         //Boton Salir
         data.getUser().removeTemp("Agtp140_PrmPC080");
         data.getUser().removeTemp("Agtp140_Evt");
         data.getUser().removeTemp("Agtp140_ImgGtApe");
         data.getUser().removeTemp("Agtp140_ImgGtTsd");
         data.getUser().removeTemp("Agtp140_vTabImg");
         data.getUser().removeTemp("Agtp140_MsgED125");
         data.getUser().removeTemp("Agtp140_MsgED135");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "NK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp140", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("R"))
       {
         //Boton Errores
         Actualiza_Datos(data, context);
         v_Tab_Img = (Vector)data.getUser().getTemp("Agtp140_vTabImg");
         Vector g_Tab_Img = A_Tab_Img(data);
         //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
         img.Graba_Img_Host(g_Msg, g_Tab_Img, data, context);
         //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
         g_MsgED135 = (MSGED135.Buf_MsgED135)data.getUser().getTemp("Agtp140_MsgED135");
         g_PrmED210 = PRMED210.Inicia_PrmED210();
         rg.MoverA(g_PrmED210.ED210_Rtn, "RO");
         rg.MoverA(g_PrmED210.ED210_Ldt, g_MsgED135.ED135_Ldv);
         data.getUser().removeTemp("Agtp140_g_PrmPC080");
         data.getUser().setTemp("Agtp140_g_PrmPC080" ,g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMED210.LSet_De_PrmED210(g_PrmED210));
         BF_MSG.Link_Program("Agtp140", "Aedp210", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("A"))
       {
         //Boton Aceptar
         Actualiza_Datos(data, context);
         v_Tab_Img = (Vector)data.getUser().getTemp("Agtp140_vTabImg");
         Vector g_Tab_Img = A_Tab_Img(data);
         Cursatura(g_Tab_Img, data, context);
         data.getUser().removeTemp("Agtp140_PrmPC080");
         data.getUser().removeTemp("Agtp140_Evt");
         data.getUser().removeTemp("Agtp140_ImgGtApe");
         data.getUser().removeTemp("Agtp140_ImgGtTsd");
         data.getUser().removeTemp("Agtp140_vTabImg");
         data.getUser().removeTemp("Agtp140_MsgED125");
         data.getUser().removeTemp("Agtp140_MsgED135");
         data.getUser().removeTemp("Agtp140_g_PrmPC080");
         data.getUser().setTemp("Agtp140_g_PrmPC080", g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Link_Program("Agtp140", "Agtp141", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("B"))
       {
         //Boton Aceptar JServicios
         Actualiza_Datos(data, context);
         v_Tab_Img = (Vector)data.getUser().getTemp("Agtp140_vTabImg");
         Vector g_Tab_Img = A_Tab_Img(data);
         //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
         img.Graba_Img_Host(g_Msg, g_Tab_Img, data, context);
         //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
         g_MsgED135 = MSGED135.Inicia_MsgED135();
         rg.MoverA(g_MsgED135.ED135_Idr, "M");
         rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
         rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
         rg.MoverA(g_MsgED135.ED135_Est, "");
         rg.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
         rg.MoverA(g_MsgED135.ED135_Ezt, "ENJSV");
         rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
         g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
         //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
         data.getUser().removeTemp("Agtp140_PrmPC080");
         data.getUser().removeTemp("Agtp140_Evt");
         data.getUser().removeTemp("Agtp140_ImgGtApe");
         data.getUser().removeTemp("Agtp140_ImgGtTsd");
         data.getUser().removeTemp("Agtp140_vTabImg");
         data.getUser().removeTemp("Agtp140_MsgED125");
         data.getUser().removeTemp("Agtp140_MsgED135");
         data.getUser().removeTemp("Agtp140_g_PrmPC080");
         data.getUser().setTemp("Agtp140_g_PrmPC080", g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp140", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("C"))
       {
         //Boton Aceptar JServicios
         g_MsgED135 = MSGED135.Inicia_MsgED135();
         rg.MoverA(g_MsgED135.ED135_Idr, "M");
         rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
         rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
         rg.MoverA(g_MsgED135.ED135_Est, "");
         rg.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
         rg.MoverA(g_MsgED135.ED135_Ezt, "ENUVT");
         rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
         g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
         data.getUser().removeTemp("Agtp140_PrmPC080");
         data.getUser().removeTemp("Agtp140_Evt");
         data.getUser().removeTemp("Agtp140_ImgGtApe");
         data.getUser().removeTemp("Agtp140_ImgGtTsd");
         data.getUser().removeTemp("Agtp140_vTabImg");
         data.getUser().removeTemp("Agtp140_MsgED125");
         data.getUser().removeTemp("Agtp140_MsgED135");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp140", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("D"))
       {
         //Boton Devolver JServicios
         g_PrmED210 = PRMED210.Inicia_PrmED210();
         rg.MoverA(g_PrmED210.ED210_Rtn, "RW");
         data.getUser().removeTemp("Agtp140_g_PrmPC080");
         data.getUser().setTemp("Agtp140_g_PrmPC080", g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMED210.LSet_De_PrmED210(g_PrmED210));
         BF_MSG.Link_Program("Agtp140", "Aedp210", data, g_Msg); 
         return;
       }
    if (Opc.trim().equals("1"))
       {
         //Consulta Lugares Disponibles
         Actualiza_Datos(data, context);
         g_PrmTG130 = PRMTG130.Inicia_PrmTG130();
         rg.MoverA(g_PrmTG130.TG130_Cmp, "DMC-LUG");
         rg.MoverA(g_PrmTG130.TG130_Ncm, "Lugares de Domicilios");
         data.getUser().removeTemp("Agtp140_g_PrmPC080");
         data.getUser().setTemp("Agtp140_g_PrmPC080", g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMTG130.LSet_De_PrmTG130(g_PrmTG130));
         BF_MSG.Link_Program("Agtp140", "Atgp130", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("2"))
       {
         //Consulta Sectores Disponibles
         Actualiza_Datos(data, context);
         g_PrmTG130 = PRMTG130.Inicia_PrmTG130();
         rg.MoverA(g_PrmTG130.TG130_Cmp, "DMC-SEC");
         rg.MoverA(g_PrmTG130.TG130_Ncm, "Sectores de Domicilios");
         data.getUser().removeTemp("Agtp140_g_PrmPC080");
         data.getUser().setTemp("Agtp140_g_PrmPC080", g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMTG130.LSet_De_PrmTG130(g_PrmTG130));
         BF_MSG.Link_Program("Agtp140", "Atgp130", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("3"))
       {
         //Consulta Comunas Disponibles
         Actualiza_Datos(data, context);
         g_PrmTG123 = PRMTG123.Inicia_PrmTG123();
         data.getUser().removeTemp("Agtp140_g_PrmPC080");
         data.getUser().setTemp("Agtp140_g_PrmPC080", g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMTG123.LSet_De_PrmTG123(g_PrmTG123));
         BF_MSG.Link_Program("Agtp140", "Atgp124", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("CtaCte"))
       {
         //Consulta CtasCtes Disponibles
         Actualiza_Datos(data, context);
         g_PrmCC092 = PRMCC092.Inicia_PrmCC092();
         rg.MoverA(g_PrmCC092.CC092_Rut, g_PrmPC080.PC080_Cli);
         rg.MoverA(g_PrmCC092.CC092_Ncl, g_PrmPC080.PC080_Ncl);
         data.getUser().removeTemp("Agtp140_g_PrmPC080");
         data.getUser().setTemp("Agtp140_g_PrmPC080", g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMCC092.LSet_De_PrmCC092(g_PrmCC092));
         BF_MSG.Link_Program("Agtp140", "Accp092", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("Mff"))
       {
         //Consulta Mff Disponibles
         Actualiza_Datos(data, context);
         v_Tab_Img = (Vector)data.getUser().getTemp("Agtp140_vTabImg");
         g_PrmCC095 = PRMCC095.Inicia_PrmCC095();
         rg.MoverA(g_PrmCC095.CC095_Cli, g_PrmPC080.PC080_Cli);
         rg.MoverA(g_PrmCC095.CC095_Ncl, g_PrmPC080.PC080_Ncl);
         for (i=0; i<v_Tab_Img.size(); i++)
             {
               lcData = img.LSet_De_Img((BF_IMG.Buf_Img)v_Tab_Img.elementAt(i));
               g_Img  = img.LSet_A_Img(lcData);
               if (g_Img.Img_Dax.toString().trim().equals("MFF"))
                  {
                    g_Img_GtMff = gtimg.LSet_A_ImgGtMff(g_Img.Img_Dat.toString());
                    PRMCC095.Bff_PrmCC095 d_PrmCC095 = new PRMCC095.Bff_PrmCC095();
                    rg.MoverA(d_PrmCC095.CC095_Cct, g_Img_GtMff.GtMff_Cct);
                    rg.MoverA(d_PrmCC095.CC095_Dct, g_Img_GtMff.GtMff_Dct);
                    rg.MoverA(d_PrmCC095.CC095_Prc, g_Img_GtMff.GtMff_Prc);
                    rg.MoverA(d_PrmCC095.CC095_Rut, g_Img_GtMff.GtMff_Rut);
                    rg.MoverA(d_PrmCC095.CC095_Nom, g_Img_GtMff.GtMff_Nom);
                    g_PrmCC095.CC095_Tap.add(d_PrmCC095);
                  }
             }
         data.getUser().removeTemp("Agtp140_g_PrmPC080");
         data.getUser().setTemp("Agtp140_g_PrmPC080", g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMCC095.LSet_De_PrmCC095(g_PrmCC095));
         BF_MSG.Link_Program("Agtp140", "Accp095", data, g_Msg);
         return;
       }
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp140_Continue(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP140[doAgtp140_Continue.start]", "[" + data.getUser().getUserName() + "]");
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Aedp210"))
       {
         g_PrmED210 = PRMED210.LSet_A_PrmED210(g_Msg.Msg_Dat.toString());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp140_g_PrmPC080");
         data.getUser().removeTemp("Agtp140_g_PrmPC080");
         if (g_PrmED210.ED210_Rtn.toString().equals("OK"))
            {
              g_MsgED135 = MSGED135.Inicia_MsgED135();
              rg.MoverA(g_MsgED135.ED135_Idr, "X");
              rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
              rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
              rg.MoverA(g_MsgED135.ED135_Est, "");
              rg.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
              rg.MoverA(g_MsgED135.ED135_Ldv, g_PrmED210.ED210_Ldt);
              rg.MoverA(g_MsgED135.ED135_Ezt, "DVJSV");
              rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
              g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
              data.getUser().removeTemp("Agtp140_PrmPC080");
              data.getUser().removeTemp("Agtp140_Evt");
              data.getUser().removeTemp("Agtp140_ImgGtApe");
              data.getUser().removeTemp("Agtp140_ImgGtTsd");
              data.getUser().removeTemp("Agtp140_vTabImg");
              data.getUser().removeTemp("Agtp140_MsgED125");
              data.getUser().removeTemp("Agtp140_MsgED135");
              rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
              rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
              BF_MSG.Return_Data("Agtp140", data, g_Msg);
              return;
            }
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP140.vm");
         return;
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Accp092"))
       {
         g_PrmCC092 = PRMCC092.LSet_A_PrmCC092(g_Msg.Msg_Dat.toString());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp140_g_PrmPC080");
         data.getUser().removeTemp("Agtp140_g_PrmPC080");
         if (g_PrmCC092.CC092_Cct.toString().trim().compareTo("")!=0)
             {
               g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp140_ImgGtApe");
               rg.MoverA(g_Img_GtApe.GtApe_Cbp, "CCTE");
               rg.MoverA(g_Img_GtApe.GtApe_Dbp, g_PrmCC092.CC092_Cct);
               data.getUser().removeTemp("Agtp140_ImgGtApe");
               data.getUser().setTemp("Agtp140_ImgGtApe", g_Img_GtApe);
             }
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP140.vm" );
         return;
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Accp095"))
       {
         g_PrmCC095 = PRMCC095.LSet_A_PrmCC095(g_Msg.Msg_Dat.toString());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp140_g_PrmPC080");
         data.getUser().removeTemp("Agtp140_g_PrmPC080");
         if (g_PrmCC095.CC095_Rtn.toString().trim().equals("OK"))
            {
              v_Tab_Img = (Vector)data.getUser().getTemp("Agtp140_vTabImg");
              for (i=0; i<v_Tab_Img.size(); i++)
                  {
                    lcData = img.LSet_De_Img((BF_IMG.Buf_Img)v_Tab_Img.elementAt(i));
                    g_Img  = img.LSet_A_Img(lcData);
                    if (g_Img.Img_Dax.toString().trim().equals("MFF"))
                       { v_Tab_Img.remove(i); }
                  }
              rg.MoverA(g_Img.Img_Seq, "000");
              for (i=0; i<g_PrmCC095.CC095_Tap.size(); i++)
                  {
                    PRMCC095.Bff_PrmCC095 d_PrmCC095 = new PRMCC095.Bff_PrmCC095();
                    d_PrmCC095 = (PRMCC095.Bff_PrmCC095)g_PrmCC095.CC095_Tap.elementAt(i);
                    if (d_PrmCC095.CC095_Cct.toString().trim().equals("")) break;
                    GT_IMG.Buf_Img_GtMff l_Img_GtMff = new GT_IMG.Buf_Img_GtMff();
                    rg.MoverA(l_Img_GtMff.GtMff_Cct, d_PrmCC095.CC095_Cct);
                    rg.MoverA(l_Img_GtMff.GtMff_Dct, d_PrmCC095.CC095_Dct);
                    rg.MoverA(l_Img_GtMff.GtMff_Prc, d_PrmCC095.CC095_Prc);
                    rg.MoverA(l_Img_GtMff.GtMff_Rut, d_PrmCC095.CC095_Rut);
                    rg.MoverA(l_Img_GtMff.GtMff_Nom, d_PrmCC095.CC095_Nom);
                    rg.MoverA(l_Img_GtMff.GtMff_Fll, " ");
                    rg.MoverA(g_Img.Img_Dax, "MFF");
                    rg.MoverA(g_Img.Img_Seq, rg.Suma(g_Img.Img_Seq,0,"1",0,3,0));
                    rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtMff(l_Img_GtMff));
                    Vector vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
                    v_Tab_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
                  }
              data.getUser().removeTemp("Agtp140_vTabImg");
              data.getUser().setTemp("Agtp140_vTabImg", v_Tab_Img);
            }
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP140.vm" );
         return;
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Atgp124"))
       {
         g_PrmTG123 = PRMTG123.LSet_A_PrmTG123(g_Msg.Msg_Dat.toString());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp140_g_PrmPC080");
         data.getUser().removeTemp("Agtp140_g_PrmPC080");
         if (g_PrmTG123.TG123_Prv.toString().trim().compareTo("")!=0)
             {
               g_Img_GtTsd = (GT_IMG.Buf_Img_GtTsd)data.getUser().getTemp("Agtp140_ImgGtTsd");
               rg.MoverA(g_Img_GtTsd.GtTsd_Cmn, g_PrmTG123.TG123_Prv.toString() + g_PrmTG123.TG123_Cmn.toString());
               rg.MoverA(g_Img_GtTsd.GtTsd_Ncm, g_PrmTG123.TG123_Ncm);
               rg.MoverA(g_Img_GtTsd.GtTsd_Npv, g_PrmTG123.TG123_Npr);
               data.getUser().removeTemp("Agtp140_ImgGtTsd");
               data.getUser().setTemp("Agtp140_ImgGtTsd", g_Img_GtTsd);
             }
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP140.vm" );
         return;
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Atgp130"))
       {
         g_PrmTG130 = PRMTG130.LSet_A_PrmTG130(g_Msg.Msg_Dat.toString());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp140_g_PrmPC080");
         data.getUser().removeTemp("Agtp140_g_PrmPC080");
         if (g_PrmTG130.TG130_Cmp.toString().trim().equals("DMC-LUG"))
             {
               if (g_PrmTG130.TG130_Cod.toString().trim().compareTo("")!=0)
                  {
                    g_Img_GtTsd = (GT_IMG.Buf_Img_GtTsd)data.getUser().getTemp("Agtp140_ImgGtTsd");
                    rg.MoverA(g_Img_GtTsd.GtTsd_Lug, g_PrmTG130.TG130_Cod);
                    if (g_Img_GtTsd.GtTsd_Lug.toString().trim().equals("<>"))
                       { rg.MoverA(g_Img_GtTsd.GtTsd_Lnm, ""); }
                    data.getUser().removeTemp("Agtp140_ImgGtTsd");
                    data.getUser().setTemp("Agtp140_ImgGtTsd", g_Img_GtTsd);
                  }
             }
         if (g_PrmTG130.TG130_Cmp.toString().trim().equals("DMC-SEC"))
             {
               if (g_PrmTG130.TG130_Cod.toString().trim().compareTo("")!=0)
                  {
                    g_Img_GtTsd = (GT_IMG.Buf_Img_GtTsd)data.getUser().getTemp("Agtp140_ImgGtTsd");
                    rg.MoverA(g_Img_GtTsd.GtTsd_Sec, g_PrmTG130.TG130_Cod);
                    if (g_Img_GtTsd.GtTsd_Sec.toString().trim().equals("<>"))
                       { rg.MoverA(g_Img_GtTsd.GtTsd_Snb, ""); }
                    data.getUser().removeTemp("Agtp140_ImgGtTsd");
                    data.getUser().setTemp("Agtp140_ImgGtTsd", g_Img_GtTsd);
                  }
             }
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP140.vm" );
         return;
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Agtp141"))
       {
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp140_g_PrmPC080");
         data.getUser().removeTemp("Agtp140_g_PrmPC080");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp140", data, g_Msg);
         return;
       }
  }
  //---------------------------------------------------------------------------------------
  public void Recupera_Evt(RunData data, Context context)
              throws Exception 
  {
    g_MsgED100 = MSGED100.Inicia_MsgED100();
    rg.MoverA(g_MsgED100.ED100_Idr, "Q");
    rg.MoverA(g_MsgED100.ED100_Nev, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_Msg.Msg_Dat, MSGED100.LSet_De_MsgED100(g_MsgED100));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED100");
    g_MsgED100 = MSGED100.LSet_A_MsgED100(g_Msg.Msg_Dat.toString());
    g_Evt = BF_EVT.LSet_A_Evt(g_MsgED100.ED100_Evt.toString());
    data.getUser().removeTemp("Agtp140_Evt");
    data.getUser().setTemp("Agtp140_Evt", g_Evt);
  }
  //---------------------------------------------------------------------------------------
  public void Carga_de_Host(RunData data, Context context)
              throws Exception 
  {
    v_Tab_Img = img.Carga_Img_Host(g_Msg, g_PrmPC080.PC080_Ntr.toString(), data, context);
    De_Tab_Img(v_Tab_Img, data);
  }
  //---------------------------------------------------------------------------------------
  public void De_Tab_Img(Vector g_Tab_Img, RunData data)
  {
    g_Img       = img.Inicia_Img();
    lcData      = img.LSet_De_Img(g_Img);
    g_Img_Base  = img.LSet_A_ImgBase(lcData);
    g_Img_GtApe = gtimg.LSet_A_ImgGtApe(g_Img.Img_Dat.toString());
    g_Img_GtTsd = gtimg.LSet_A_ImgGtTsd(g_Img.Img_Dat.toString());
    for (i=0; i<g_Tab_Img.size(); i++)
        {
          lcData = img.LSet_De_Img((BF_IMG.Buf_Img)g_Tab_Img.elementAt(i));
          g_Img  = img.LSet_A_Img(lcData);
          if (g_Img.Img_Dax.toString().trim().equals("BASE"))
             {
               g_Img_GtApe = gtimg.LSet_A_ImgGtApe(g_Img.Img_Dat.toString());
               g_Img_Base  = img.LSet_A_ImgBase(g_Img_GtApe.GtApe_Bse.toString());
             }
          if (g_Img.Img_Dax.toString().trim().equals("XTSD"))
             { g_Img_GtTsd = gtimg.LSet_A_ImgGtTsd(g_Img.Img_Dat.toString()); }
        }
  }
  //---------------------------------------------------------------------------------------
  public void Recupera_ImgTsd(RunData data, Context context)
              throws Exception 
  {
    g_MsgGT140 = MSGGT140.Inicia_MsgGT140();
    rg.MoverA(g_MsgGT140.GT140_Idr, "Q");
    rg.MoverA(g_MsgGT140.GT140_Gti, g_PrmPC080.PC080_Cnr.Sis.toString() + g_PrmPC080.PC080_Cnr.Ncn.toString());
    rg.MoverA(g_Msg.Msg_Dat, MSGGT140.LSet_De_MsgGT140(g_MsgGT140));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GT140");
    g_MsgGT140 = MSGGT140.LSet_A_MsgGT140(g_Msg.Msg_Dat.toString());
    rg.MoverA(g_Img_GtTsd.GtTsd_Nc1, g_MsgGT140.GT140_Nc1);
    rg.MoverA(g_Img_GtTsd.GtTsd_T11, g_MsgGT140.GT140_T11);
    rg.MoverA(g_Img_GtTsd.GtTsd_Cll, g_MsgGT140.GT140_Cll);
    rg.MoverA(g_Img_GtTsd.GtTsd_Cnm, g_MsgGT140.GT140_Cnm);
    rg.MoverA(g_Img_GtTsd.GtTsd_Lug, g_MsgGT140.GT140_Lug);
    rg.MoverA(g_Img_GtTsd.GtTsd_Lnm, g_MsgGT140.GT140_Lnm);
    rg.MoverA(g_Img_GtTsd.GtTsd_Sec, g_MsgGT140.GT140_Sec);
    rg.MoverA(g_Img_GtTsd.GtTsd_Snb, g_MsgGT140.GT140_Snb);
    rg.MoverA(g_Img_GtTsd.GtTsd_Cmn, g_MsgGT140.GT140_Cmn);
    rg.MoverA(g_Img_GtTsd.GtTsd_Ncm, g_MsgGT140.GT140_Ncm);
    rg.MoverA(g_Img_GtTsd.GtTsd_Npv, g_MsgGT140.GT140_Npv);
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    rg.MoverA(g_Img.Img_Ntr, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_Img.Img_Dax, "XTSD");
    rg.MoverA(g_Img.Img_Seq, "000");
    rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtTsd(g_Img_GtTsd));
    v_Tab_Img.add(g_Img);
  }
  //---------------------------------------------------------------------------------------
  public void Recupera_Retasa(RunData data, Context context)
              throws Exception 
  {
    Vector v_TabImg                  = new Vector();
    MSGGT640.Bff_MsgGT640 d_MsgGT640 = new MSGGT640.Bff_MsgGT640();
    String lcIdx = "0001";
    String lcNtr = g_PrmPC080.PC080_Ntr.toString();
    boolean liSwtFin = false;
    v_TabImg.clear();
    g_MsgGT640 = MSGGT640.Inicia_MsgGT640();
    rg.MoverA(g_MsgGT640.GT640_Idr, "I");
    rg.MoverA(g_MsgGT640.GT640_Idx, lcIdx);
    rg.MoverA(g_MsgGT640.GT640_Sis, g_PrmPC080.PC080_Cnr.Sis);
    rg.MoverA(g_MsgGT640.GT640_Ncn, g_PrmPC080.PC080_Cnr.Ncn);
    rg.MoverA(g_MsgGT640.GT640_Ttr, g_PrmPC080.PC080_Ttr);
    rg.MoverA(g_MsgGT640.GT640_Ntr, lcNtr);
    while (liSwtFin == false)
       {
         rg.MoverA(g_Msg.Msg_Dat, MSGGT640.LSet_De_MsgGT640(g_MsgGT640));
         g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GT640");
         g_MsgGT640 = MSGGT640.LSet_A_MsgGT640(g_Msg.Msg_Dat.toString());
         lcIdx = g_MsgGT640.GT640_Idx.toString();
         lcNtr = g_MsgGT640.GT640_Ntr.toString();
         for (i=0; i<MSGGT640.g_Max_GT640; i++)
             {
               d_MsgGT640 = (MSGGT640.Bff_MsgGT640)g_MsgGT640.GT640_Img.elementAt(i);
               if (d_MsgGT640.Img_Dax.toString().trim().equals(""))
                  {
                    liSwtFin = true;
                    break;
                  }
               BF_IMG.Buf_Img t_Img = new BF_IMG.Buf_Img();
               rg.MoverA(t_Img.Img_Ntr, d_MsgGT640.Img_Ntr);
               rg.MoverA(t_Img.Img_Dax, d_MsgGT640.Img_Dax);
               rg.MoverA(t_Img.Img_Seq, d_MsgGT640.Img_Seq);
               rg.MoverA(t_Img.Img_Dat, d_MsgGT640.Img_Dat);
               v_TabImg.add(t_Img);
             }
         g_MsgGT640 = MSGGT640.Inicia_MsgGT640();
         rg.MoverA(g_MsgGT640.GT640_Idr, "S");
         rg.MoverA(g_MsgGT640.GT640_Idx, rg.Suma(lcIdx,0,"4",0,4,0));
         rg.MoverA(g_MsgGT640.GT640_Sis, g_PrmPC080.PC080_Cnr.Sis);
         rg.MoverA(g_MsgGT640.GT640_Ncn, g_PrmPC080.PC080_Cnr.Ncn);
         rg.MoverA(g_MsgGT640.GT640_Ttr, g_PrmPC080.PC080_Ttr);
         rg.MoverA(g_MsgGT640.GT640_Ntr, lcNtr);
       }
    for (i=0; i<v_TabImg.size(); i++)
        {
          lcData = img.LSet_De_Img((BF_IMG.Buf_Img)v_TabImg.elementAt(i));
          g_Img  = img.LSet_A_Img(lcData);
          if (g_Img.Img_Dax.toString().trim().equals("XTSD"))
             { g_Img_GtTsd = gtimg.LSet_A_ImgGtTsd(g_Img.Img_Dat.toString()); }
          if (!g_Img.Img_Dax.toString().trim().equals("BASE"))
             { v_Tab_Img.add(g_Img); }
        }
    rg.MoverA(g_Img_GtApe.GtApe_Fig, g_Msg.Msg_Fch);
    rg.MoverA(g_Img_GtApe.GtApe_Fts, g_Img_GtTsd.GtTsd_Fts);
    rg.MoverA(g_Img_GtApe.GtApe_Vuo, rg.FmtValor("1",0,2,9,"+"));
  }
  //---------------------------------------------------------------------------------------
  public void Carga_Doctos(RunData data, Context context)
              throws Exception 
  {
    g_MsgED125 = MSGED125.Inicia_MsgED125();
    rg.MoverA(g_MsgED125.ED125_Idr, "I");
    rg.MoverA(g_MsgED125.ED125_Nev, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_MsgED125.ED125_Trt, g_PrmPC080.PC080_Trt);
    rg.MoverA(g_Msg.Msg_Dat, MSGED125.LSet_De_MsgED125(g_MsgED125));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED125");
    g_MsgED125 = MSGED125.LSet_A_MsgED125(g_Msg.Msg_Dat.toString());
    data.getUser().removeTemp("Agtp140_MsgED125");
    data.getUser().setTemp("Agtp140_MsgED125", g_MsgED125);
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    g_MsgED135 = MSGED135.Inicia_MsgED135();
    rg.MoverA(g_MsgED135.ED135_Idr, "C");
    rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
    rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
    g_MsgED135 = MSGED135.LSet_A_MsgED135(g_Msg.Msg_Dat.toString());
    data.getUser().removeTemp("Agtp140_MsgED135");
    data.getUser().setTemp("Agtp140_MsgED135", g_MsgED135);
  }
  //---------------------------------------------------------------------------------------
  public void Actualiza_Datos(RunData data, Context context)
              throws Exception 
  {
    g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp140_ImgGtApe");
    g_Img_Base  = img.LSet_A_ImgBase(g_Img_GtApe.GtApe_Bse.toString());
    g_Img_GtTsd = (GT_IMG.Buf_Img_GtTsd)data.getUser().getTemp("Agtp140_ImgGtTsd");
    rg.MoverA(g_Img_GtTsd.GtTsd_Nc1, data.getParameters().getString("GtTsd_Nc1", ""));
    rg.MoverA(g_Img_GtTsd.GtTsd_T11, data.getParameters().getString("GtTsd_T11", ""));
    rg.MoverA(g_Img_GtTsd.GtTsd_T21, data.getParameters().getString("GtTsd_T21", ""));
    rg.MoverA(g_Img_GtTsd.GtTsd_T31, data.getParameters().getString("GtTsd_T31", ""));
    rg.MoverA(g_Img_GtTsd.GtTsd_Nc2, data.getParameters().getString("GtTsd_Nc2", ""));
    rg.MoverA(g_Img_GtTsd.GtTsd_T12, data.getParameters().getString("GtTsd_T12", ""));
    rg.MoverA(g_Img_GtTsd.GtTsd_T22, data.getParameters().getString("GtTsd_T22", ""));
    rg.MoverA(g_Img_GtTsd.GtTsd_T32, data.getParameters().getString("GtTsd_T32", ""));
    rg.MoverA(g_Img_GtTsd.GtTsd_Nc3, data.getParameters().getString("GtTsd_Nc3", ""));
    rg.MoverA(g_Img_GtTsd.GtTsd_T13, data.getParameters().getString("GtTsd_T13", ""));
    rg.MoverA(g_Img_GtTsd.GtTsd_T23, data.getParameters().getString("GtTsd_T23", ""));
    rg.MoverA(g_Img_GtTsd.GtTsd_T33, data.getParameters().getString("GtTsd_T33", ""));
    rg.MoverA(g_Img_GtTsd.GtTsd_Cll, data.getParameters().getString("GtTsd_Cll", ""));
    rg.MoverA(g_Img_GtTsd.GtTsd_Cnm, data.getParameters().getString("GtTsd_Cnm", ""));
    rg.MoverA(g_Img_GtTsd.GtTsd_Lug, data.getParameters().getString("GtTsd_Lug", ""));
    rg.MoverA(g_Img_GtTsd.GtTsd_Lnm, data.getParameters().getString("GtTsd_Lnm", ""));
    rg.MoverA(g_Img_GtTsd.GtTsd_Sec, data.getParameters().getString("GtTsd_Sec", ""));
    rg.MoverA(g_Img_GtTsd.GtTsd_Snb, data.getParameters().getString("GtTsd_Snb", ""));
    rg.MoverA(g_Img_GtTsd.GtTsd_Cmn, data.getParameters().getString("GtTsd_Cmn", ""));
    rg.MoverA(g_Img_GtTsd.GtTsd_Ncm, data.getParameters().getString("GtTsd_Ncm", ""));
    rg.MoverA(g_Img_GtTsd.GtTsd_Npv, data.getParameters().getString("GtTsd_Npv", ""));
    rg.MoverA(g_Img_GtTsd.GtTsd_Opt, data.getParameters().getString("GtTsd_Opt", ""));
    rg.MoverA(g_Img_GtApe.GtApe_Cbp, data.getParameters().getString("GtApe_Cbp", ""));
    rg.MoverA(g_Img_GtApe.GtApe_Dbp, data.getParameters().getString("GtApe_Dbp", ""));
    rg.MoverA(g_Img_GtApe.GtApe_Bse, img.LSet_De_ImgBase_Bse(g_Img_Base));
    data.getUser().removeTemp("Agtp140_ImgGtApe");
    data.getUser().setTemp("Agtp140_ImgGtApe", g_Img_GtApe);
    data.getUser().removeTemp("Agtp140_ImgGtTsd");
    data.getUser().setTemp("Agtp140_ImgGtTsd", g_Img_GtTsd);
  }
  //---------------------------------------------------------------------------------------
  public Vector A_Tab_Img(RunData data)
  {
    Vector vBuf_Img = new Vector();
    vBuf_Img.clear();
    rg.MoverA(g_Img.Img_Ntr, g_PrmPC080.PC080_Ntr);
    //--------------------------------------------------------------------------
    rg.MoverA(g_Img.Img_Dax, "BASE");
    rg.MoverA(g_Img.Img_Seq, "000");
    rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtApe(g_Img_GtApe));
    Vector vPaso = img.LSet_A_vImg(img.LSet_De_Img(g_Img));
    BF_IMG.Buf_Img tImg = (BF_IMG.Buf_Img)vPaso.elementAt(0);
    vBuf_Img.add(tImg);		
    //--------------------------------------------------------------------------
    rg.MoverA(g_Img.Img_Dax, "XTSD");
    rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtTsd(g_Img_GtTsd));
    vPaso = img.LSet_A_vImg(img.LSet_De_Img(g_Img));
    tImg = (BF_IMG.Buf_Img)vPaso.elementAt(0);
    vBuf_Img.add(tImg);		
    //--------------------------------------------------------------------------
    rg.MoverA(g_Img.Img_Seq, "000");
    for (i=0; i<v_Tab_Img.size(); i++)
        {
          lcData = img.LSet_De_Img((BF_IMG.Buf_Img)v_Tab_Img.elementAt(i));
          g_Img  = img.LSet_A_Img(lcData);
          if (!g_Img.Img_Dax.toString().trim().equals("BASE"))
             {
               if (!g_Img.Img_Dax.toString().trim().equals("XTSD"))
                  {
                    vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
                    vBuf_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
                  }
             }
        }
    //--------------------------------------------------------------------------
    return vBuf_Img;
  }
  //---------------------------------------------------------------------------------------
  public void Cursatura(Vector g_Tab_Img, RunData data, Context context)
              throws Exception 
  {
    g_MsgED135 = (MSGED135.Buf_MsgED135)data.getUser().getTemp("Agtp140_MsgED135");
    Log.debug("AGTP140[Cursatura.MSGED135]", "[" + g_MsgED135.getNev() + g_MsgED135.getTrt() + g_MsgED135.getEst() + g_MsgED135.getEzt() + "]");
    String antEzt = g_MsgED135.getEzt();
    String antLdv = g_MsgED135.getLdv();
    g_Evt = (BF_EVT.Buf_Evt)data.getUser().getTemp("Agtp140_Evt");
    img.Graba_Img_Host(g_Msg, g_Tab_Img, data, context);
    BF_EVT t = new BF_EVT();
    g_MsgED100 = MSGED100.Inicia_MsgED100();
    rg.MoverA(g_MsgED100.ED100_Idr, "M");
    rg.MoverA(g_MsgED100.ED100_Nev, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_MsgED100.ED100_Evt, t.LSet_De_Evt(g_Evt));
    rg.MoverA(g_Msg.Msg_Dat, MSGED100.LSet_De_MsgED100(g_MsgED100));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED100");


    g_MsgED135 = MSGED135.Inicia_MsgED135();
    if (antEzt.equals("DVIFS"))
       { rg.MoverA(g_MsgED135.ED135_Idr, "X"); }
    else
        { rg.MoverA(g_MsgED135.ED135_Idr, "M"); }
    rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
    rg.MoverA(g_MsgED135.ED135_Est, "");
    rg.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
  //if (g_PrmPC080.getEtt().trim().equals("DVIFS"))
    if (antEzt.equals("DVIFS"))
       {
         rg.MoverA(g_MsgED135.ED135_Ldv, antLdv);
         rg.MoverA(g_MsgED135.ED135_Ezt, "REEJE");
       }
    else
       { rg.MoverA(g_MsgED135.ED135_Ezt, g_PrmPC080.PC080_Ein); }
    rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
  }
}