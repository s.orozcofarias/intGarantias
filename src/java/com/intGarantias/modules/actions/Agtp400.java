// Source File Name:   Agtp400.java
// Descripcion:        Consulta/Seleccion Garantia (GT400)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.PRMPC080;

import com.intGlobal.modules.global.PRMCL090;

import com.intGarantias.modules.global.MSGGT400;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp400 extends FHTServletAction
{
  //---------------------------------------------------------------------------------------
  public int    i = 0;
  public String lcRetorno = "";
  public Vector vDatos;
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  PRMPC080.PrmPC080 g_PrmPC080     = new PRMPC080.PrmPC080();
  MSGGT400.Buf_MsgGT400 g_MsgGT400 = new MSGGT400.Buf_MsgGT400();
  PRMCL090.Buf_PrmCL090 g_PrmCL090 = new PRMCL090.Buf_PrmCL090();
  //---------------------------------------------------------------------------------------
  public Agtp400()
  {
  }
  //---------------------------------------------------------------------------------------
  public void doPerform(RunData data, Context context)
              throws Exception
  {
  //Log.debug("AGTP400[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp400-DIN", data);
    if (g_Msg.Msg_Pgr.toString().equals("RT"))
       { doAgtp400_Continue(data, context); }
    else
       { doAgtp400_Init(data, context); }
  }
  //---------------------------------------------------------------------------------------
  public void doCancel(RunData data, Context context)
              throws Exception
  {
  //Log.debug("AGTP400[doCancel.start]", "[CANCELADO]");
    setTemplate(data, "Garantias,Agt,AGTP400.vm");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp400_Init(RunData data, Context context)
              throws Exception
  {
  //Log.debug("AGTP400[doAgtp400_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    if (g_PrmPC080.PC080_Cli.toString().trim().equals(""))
       { 
       	 Otro_Cli(data, context);
       	 return;
       }
    g_MsgGT400 = MSGGT400.Inicia_MsgGT400();
    rg.MoverA(g_MsgGT400.GT400_Idr, "I");
    rg.MoverA(g_MsgGT400.GT400_Rut, g_PrmPC080.PC080_Cli);
    Poblar_Panel(data, context);
    setTemplate(data, "Garantias,Agt,AGTP400.vm" );
  //Log.debug("AGTP400[doAgtp400_Init.end]", "[" + data.getUser().getUserName() + "]");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp400(RunData data, Context context)
              throws Exception
  {
  //Log.debug("AGTP400[doAgtp400.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp400-MAN", data);
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    String Opc = data.getParameters().getString("Opc" ,"");
    if  (Opc.equals("S"))
        //Salir
        {
          data.getUser().removeTemp("Agtp400_PrmPC080");
          data.getUser().removeTemp("Agtp400_MsgGT400");
          if (g_PrmPC080.PC080_Acc.toString().trim().equals("SELECCIONA"))
             { 
               rg.MoverA(g_PrmPC080.PC080_Rtn, "NK");
               rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
               BF_MSG.Return_Data("Agtp400", data, g_Msg);
             }
          else
             { BF_MSG.Return_Program(data, g_Msg); }
          return;
        }
    g_MsgGT400 = (MSGGT400.Buf_MsgGT400)data.getUser().getTemp("Agtp400_MsgGT400");
    MSGGT400.Bff_MsgGT400 d_MsgGT400 = new MSGGT400.Bff_MsgGT400();
    if  (Opc.equals("E"))
        //Selecion. Ir a pantalla consulta garantia especifica
        {
          String Seq = data.getParameters().getString("Seq", "");
          i = Integer.parseInt(Seq);
          d_MsgGT400 = (MSGGT400.Bff_MsgGT400)g_MsgGT400.GT400_Tap.elementAt(i);
          rg.MoverA(g_PrmPC080.PC080_Rtn, "OA");
          rg.MoverA(g_PrmPC080.PC080_Cnr.Sis, d_MsgGT400.GT400_Cnr.toString().substring(0));
          rg.MoverA(g_PrmPC080.PC080_Cnr.Ncn, d_MsgGT400.GT400_Cnr.toString().substring(3));
          rg.MoverA(g_PrmPC080.PC080_Dcn, d_MsgGT400.GT400_Dcn);
          rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
          data.getUser().removeTemp("Agtp400_PrmPC080");
          data.getUser().removeTemp("Agtp400_MsgGT400");
          BF_MSG.Return_Data("Agtp400", data, g_Msg);
          return;
        }       
    if  (Opc.equals("D"))
       //Detalle
        {
          String Seq = data.getParameters().getString("Seq", "");
          i = Integer.parseInt(Seq);
          d_MsgGT400 = (MSGGT400.Bff_MsgGT400)g_MsgGT400.GT400_Tap.elementAt(i);
          rg.MoverA(g_PrmPC080.PC080_Rtn,     "");
          rg.MoverA(g_PrmPC080.PC080_Cnr.Sis, d_MsgGT400.GT400_Cnr.toString().substring(0));
          rg.MoverA(g_PrmPC080.PC080_Cnr.Ncn, d_MsgGT400.GT400_Cnr.toString().substring(3));
          rg.MoverA(g_PrmPC080.PC080_Dcn,     d_MsgGT400.GT400_Dcn);
          rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
          //lcRetorno = BF_MSG.Link_ProgramN("Agtp400", "Agtp451", data, g_Msg); 
BF_MSG.Link_Program("Agtp400", "Agtp451", data, g_Msg); 
          if (lcRetorno.trim().compareTo("")!=0)
             {
               data.getUser().removeTemp("Agtp400_g_PrmPC080");
               data.getUser().setTemp("Agtp400_g_PrmPC080", g_PrmPC080);
               BF_MSG.MsgInt("Agtp400", data, g_Msg, lcRetorno, "1"); 
      	       return;
             }
       	  return;
        }
    if  (Opc.equals("T"))
       //Cartola
        {
          String Seq = data.getParameters().getString("Seq", "");
          i = Integer.parseInt(Seq);
          d_MsgGT400 = (MSGGT400.Bff_MsgGT400)g_MsgGT400.GT400_Tap.elementAt(i);
          rg.MoverA(g_PrmPC080.PC080_Cnr.Sis, d_MsgGT400.GT400_Cnr.toString().substring(0));
          rg.MoverA(g_PrmPC080.PC080_Cnr.Ncn, d_MsgGT400.GT400_Cnr.toString().substring(3));
          rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
          //lcRetorno = BF_MSG.Link_ProgramN("Agtp400", "Aicp430", data, g_Msg); 
BF_MSG.Link_Program("Agtp400", "Aicp430", data, g_Msg); 
          if (lcRetorno.trim().compareTo("")!=0)
             {
               data.getUser().removeTemp("Agtp400_g_PrmPC080");
               data.getUser().setTemp("Agtp400_g_PrmPC080", g_PrmPC080);
               BF_MSG.MsgInt("Agtp400", data, g_Msg, lcRetorno, "1"); 
      	       return;
             }
       	  return;
        }
    if  (Opc.equals("N"))
        //Siguiente
        {	
          i = Integer.parseInt(g_MsgGT400.GT400_Nro.toString())-1;
          d_MsgGT400 = (MSGGT400.Bff_MsgGT400)g_MsgGT400.GT400_Tap.elementAt(i);
          g_MsgGT400 = MSGGT400.Inicia_MsgGT400();
          rg.MoverA(g_MsgGT400.GT400_Idr, "S");
          rg.MoverA(g_MsgGT400.GT400_Rut, g_PrmPC080.PC080_Cli);
          g_MsgGT400.GT400_Tap.set(0, d_MsgGT400);
          Poblar_Panel(data, context);
          setTemplate(data, "garantias,Agt,AGTP400.vm" );
        }
    if  (Opc.equals("P"))
        //Previo
        {	
          i = 0;
          d_MsgGT400 = (MSGGT400.Bff_MsgGT400)g_MsgGT400.GT400_Tap.elementAt(i);
          g_MsgGT400 = MSGGT400.Inicia_MsgGT400();
          rg.MoverA(g_MsgGT400.GT400_Idr, "P");
          rg.MoverA(g_MsgGT400.GT400_Rut, g_PrmPC080.PC080_Cli);
          g_MsgGT400.GT400_Tap.set(0, d_MsgGT400);
          Poblar_Panel(data, context);
          setTemplate(data, "garantias,Agt,AGTP400.vm" );
        }		
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp400_Continue(RunData data, Context context)
              throws Exception
  {
  //Log.debug("AGTP400[doAgtp400_Continue.start]", "[" + data.getUser().getUserName() + "]");
    if (RUTGEN.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Eclp090"))
       {
         g_PrmCL090 = PRMCL090.LSet_A_PrmCL090(g_Msg.Msg_Dat.toString());
         PRMPC080 vPrmPC080 = new PRMPC080();
         vDatos = vPrmPC080.LSet_A_vPrmPC080((String)data.getUser().getTemp("Agtp400_g_PrmPC080"));
         g_PrmPC080 = (PRMPC080.PrmPC080)vDatos.elementAt(0);
         data.getUser().removeTemp("Agtp400_g_PrmPC080");
         rg.MoverA(g_PrmPC080.PC080_Cli, g_PrmCL090.CL090_Cli);
         rg.MoverA(g_PrmPC080.PC080_Ncl, g_PrmCL090.CL090_Ncl);
         rg.MoverA(g_PrmPC080.PC080_Tpr, g_PrmCL090.CL090_Tpr);
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         if (g_PrmPC080.PC080_Cli.toString().trim().equals(""))
            {
              data.getUser().removeTemp("Agtp400_PrmPC080");
              data.getUser().removeTemp("Agtp400_MsgGT400");
              BF_MSG.Return_Program(data, g_Msg);
              return;
            }
         g_MsgGT400 = MSGGT400.Inicia_MsgGT400();
         rg.MoverA(g_MsgGT400.GT400_Idr, "I");
         rg.MoverA(g_MsgGT400.GT400_Rut, g_PrmPC080.PC080_Cli);
         Poblar_Panel(data, context);
         setTemplate(data, "Garantias,Agt,AGTP400.vm" );
         return;
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Appp030"))
       {
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp400_g_PrmPC080");
         data.getUser().removeTemp("Agtp400_g_PrmPC080");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP400.vm" );
         return;
       }
    setTemplate(data, "Garantias,Agt,AGTP400.vm" );
    return;
  }
  //---------------------------------------------------------------------------------------
  public void Poblar_Panel(RunData data, Context context)
              throws Exception
  {
    rg.MoverA(g_Msg.Msg_Dat, MSGGT400.LSet_De_MsgGT400(g_MsgGT400));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GT400");
    g_MsgGT400 = MSGGT400.LSet_A_MsgGT400(g_Msg.Msg_Dat.toString());
    data.getUser().removeTemp("Agtp400_MsgGT400");
    data.getUser().setTemp("Agtp400_MsgGT400", g_MsgGT400);
    data.getUser().removeTemp("Agtp400_PrmPC080");
    data.getUser().setTemp("Agtp400_PrmPC080", g_PrmPC080);
  }
  //---------------------------------------------------------------------------------------
  public void Otro_Cli(RunData data, Context context)
              throws Exception
  {
    data.getUser().removeTemp("Agtp400_g_PrmPC080");
    data.getUser().setTemp("Agtp400_g_PrmPC080" ,PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
    g_PrmCL090 = PRMCL090.Inicia_PrmCL090();
    rg.MoverA(g_PrmCL090.CL090_Pcc, "N");
    rg.MoverA(g_Msg.Msg_Dat, PRMCL090.LSet_De_PrmCL090(g_PrmCL090));
    BF_MSG.Link_Program("Agtp400", "Eclp090", data, g_Msg);
  }
  //---------------------------------------------------------------------------------------
}