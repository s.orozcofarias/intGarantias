// Source File Name:   Agtp316.java
// Descripcion:        Ingreso Acciones (ED135)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.BF_IMG;
import com.FHTServlet.modules.global.PRMPC080;

import com.intGlobal.modules.global.MSGED135;
import com.intGlobal.modules.global.PRMED210;
import com.intGlobal.modules.global.PRMTF108;

import com.intGarantias.modules.global.GT_IMG;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp316 extends FHTServletAction
{
  //---------------------------------------------------------------------------------------
  public int    i = 0;
  public String lcData = "";
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  PRMPC080.PrmPC080 g_PrmPC080     = new PRMPC080.PrmPC080();
  Vector v_Tab_Img                 = new Vector();
  Vector v_Img_GtAcc               = new Vector();
  BF_IMG img                       = new BF_IMG();
  BF_IMG.Buf_Img g_Img             = new BF_IMG.Buf_Img();
  BF_IMG.Buf_Img_Bse g_Img_Base    = new BF_IMG.Buf_Img_Bse();
  GT_IMG gtimg                     = new GT_IMG();
  GT_IMG.Buf_Img_GtApe g_Img_GtApe = new GT_IMG.Buf_Img_GtApe();
  GT_IMG.Buf_Img_GtAcc g_Img_GtAcc = new GT_IMG.Buf_Img_GtAcc();
  MSGED135.Buf_MsgED135 g_MsgED135 = new MSGED135.Buf_MsgED135();
  PRMED210.Buf_PrmED210 g_PrmED210 = new PRMED210.Buf_PrmED210();
  PRMTF108.Buf_PrmTF108 g_PrmTF108 = new PRMTF108.Buf_PrmTF108();
  //---------------------------------------------------------------------------------------
  public Agtp316()
  {
  }
  //---------------------------------------------------------------------------------------
  public void doPerform(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP316[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp316-DIN", data);
    if (g_Msg.Msg_Pgr.toString().equals("RT"))
       { doAgtp316_Continue(data, context); }
    else
       { doAgtp316_Init(data, context); }
  }
  //---------------------------------------------------------------------------------------
  public void doCancel(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP316[doCancel.start]", "[CANCELADO]");
    setTemplate(data, "Garantias,Agt,AGTP316.vm");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp316_Init(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP316[doAgtp316_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    Carga_de_Host(data, context);
    Carga_Inicial(data, context);
    data.getUser().removeTemp("Agtp316_PrmPC080");
    data.getUser().setTemp("Agtp316_PrmPC080", g_PrmPC080);
    setTemplate(data, "Garantias,Agt,AGTP316.vm" );
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp316(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP316[doAgtp316.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp316-MAN", data);
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    String Opc = data.getParameters().getString("Opc" ,"");
    if (Opc.trim().equals("T"))
       {
         //Imagen Cambio Tipo Accion
         g_Img_GtAcc = (GT_IMG.Buf_Img_GtAcc)data.getUser().getTemp("Agtp316_ImgGtAcc");
         Actualiza_Datos(data, context);
         data.getUser().removeTemp("Agtp316_ImgGtAcc");
         data.getUser().setTemp("Agtp316_ImgGtAcc", g_Img_GtAcc);
         g_PrmTF108 = PRMTF108.Inicia_PrmTF108();
         rg.MoverA(g_PrmTF108.TF108_Tpa, "A");
         data.getUser().removeTemp("Agtp316_g_PrmPC080");
         data.getUser().setTemp("Agtp316_g_PrmPC080" ,g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMTF108.LSet_De_PrmTF108(g_PrmTF108));
         BF_MSG.Link_Program("Agtp316", "Atfp108", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("S"))
       {
         //Boton Salir
         data.getUser().removeTemp("Agtp316_MsgED135");
         data.getUser().removeTemp("Agtp316_vTabImg");
         data.getUser().removeTemp("Agtp316_vImgGtAcc");
         data.getUser().removeTemp("Agtp316_ImgBase");
         data.getUser().removeTemp("Agtp316_ImgGtApe");
         data.getUser().removeTemp("Agtp316_PrmPC080");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp316", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("C"))
       {
         //Boton OK (CursarIdt)
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp316_ImgGtApe");
         v_Img_GtAcc = (Vector)data.getUser().getTemp("Agtp316_vImgGtAcc");
         Vector g_Tab_Img = A_Tab_Img(data);
         data.getUser().removeTemp("Agtp305_gTabImg");
         data.getUser().setTemp("Agtp305_gTabImg", g_Tab_Img);
         //img.Graba_Img_Host(g_Msg, g_Tab_Img, data, context);
         //g_MsgED135 = MSGED135.Inicia_MsgED135();
         //rg.MoverA(g_MsgED135.ED135_Idr, "M");
         //rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
         //rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
         //rg.MoverA(g_MsgED135.ED135_Est, "");
         //rg.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
         //rg.MoverA(g_MsgED135.ED135_Vlc, g_Img_GtApe.GtApe_Vcn);
         //rg.MoverA(g_MsgED135.ED135_Ezt, "TERMI");
         //rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
         //g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
         data.getUser().removeTemp("Agtp316_MsgED135");
         data.getUser().removeTemp("Agtp316_vTabImg");
         data.getUser().removeTemp("Agtp316_vImgGtAcc");
         data.getUser().removeTemp("Agtp316_ImgBase");
         data.getUser().removeTemp("Agtp316_ImgGtApe");
         data.getUser().removeTemp("Agtp316_PrmPC080");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp316", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("A"))
       {
         //Boton Agregar
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp316_ImgGtApe");
         g_Img_Base  = img.LSet_A_ImgBase(g_Img_GtApe.GtApe_Bse.toString());
         g_Img_GtAcc = gtimg.LSet_A_ImgGtAcc(rg.Blancos(827));
         rg.MoverA(g_Img_GtApe.GtApe_Psw, "A");
         rg.MoverA(g_Img_GtApe.GtApe_Idx, rg.Suma(g_Img_GtApe.GtApe_Tfr,0,"1",0,3,0));
         rg.MoverA(g_Img_GtAcc.GtAcc_Mnd, g_Img_Base.Img_Base_Mnd);
         data.getUser().removeTemp("Agtp316_ImgGtApe");
         data.getUser().setTemp("Agtp316_ImgGtApe", g_Img_GtApe);
         data.getUser().removeTemp("Agtp316_ImgGtAcc");
         data.getUser().setTemp("Agtp316_ImgGtAcc", g_Img_GtAcc);
         setTemplate(data, "Garantias,Agt,AGTP316.vm" );
         return;
       }
    if (Opc.trim().equals("M"))
       {
         //Boton Modificar
         String Seq = data.getParameters().getString("Seq", "");
         i = Integer.parseInt(Seq);
         v_Img_GtAcc = (Vector)data.getUser().getTemp("Agtp316_vImgGtAcc");
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp316_ImgGtApe");
         g_Img_Base  = img.LSet_A_ImgBase(g_Img_GtApe.GtApe_Bse.toString());
         rg.MoverA(g_Img_GtApe.GtApe_Psw, "M");
         rg.MoverA(g_Img_GtApe.GtApe_Idx, rg.Suma(Seq,0,"1",0,3,0));
         g_Img_GtAcc = (GT_IMG.Buf_Img_GtAcc)v_Img_GtAcc.elementAt(i);
         data.getUser().removeTemp("Agtp316_ImgGtApe");
         data.getUser().setTemp("Agtp316_ImgGtApe", g_Img_GtApe);
         data.getUser().removeTemp("Agtp316_ImgGtAcc");
         data.getUser().setTemp("Agtp316_ImgGtAcc", g_Img_GtAcc);
         setTemplate(data, "Garantias,Agt,AGTP316.vm" );
         return;
       }
    if (Opc.trim().equals("E"))
       {
         //Boton Eliminar
         String Seq = data.getParameters().getString("Seq", "");
         i = Integer.parseInt(Seq);
         v_Img_GtAcc = (Vector)data.getUser().getTemp("Agtp316_vImgGtAcc");
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp316_ImgGtApe");
         g_Img_Base  = img.LSet_A_ImgBase(g_Img_GtApe.GtApe_Bse.toString());
         rg.MoverA(g_Img_GtApe.GtApe_Psw, "E");
         rg.MoverA(g_Img_GtApe.GtApe_Idx, rg.Suma(Seq,0,"1",0,3,0));
         g_Img_GtAcc = (GT_IMG.Buf_Img_GtAcc)v_Img_GtAcc.elementAt(i);
         data.getUser().removeTemp("Agtp316_ImgGtApe");
         data.getUser().setTemp("Agtp316_ImgGtApe", g_Img_GtApe);
         data.getUser().removeTemp("Agtp316_ImgGtAcc");
         data.getUser().setTemp("Agtp316_ImgGtAcc", g_Img_GtAcc);
         setTemplate(data, "Garantias,Agt,AGTP316.vm" );
         return;
       }
    if (Opc.trim().equals("Y"))
       {
         //Boton Si
         String Seq = data.getParameters().getString("Seq", "0");
         i = Integer.parseInt(Seq);
         g_Img_GtAcc = (GT_IMG.Buf_Img_GtAcc)data.getUser().getTemp("Agtp316_ImgGtAcc");
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp316_ImgGtApe");
         v_Img_GtAcc = (Vector)data.getUser().getTemp("Agtp316_vImgGtAcc");
         Actualiza_Datos(data, context);
         if (g_Img_GtApe.GtApe_Psw.toString().trim().equals("A"))
            {
              v_Img_GtAcc.add(g_Img_GtAcc);
              rg.MoverA(g_Img_GtApe.GtApe_Tfr, rg.Suma(g_Img_GtApe.GtApe_Tfr,0,"1",0,3,0));
            }
         else
            {
              if (g_Img_GtApe.GtApe_Psw.toString().trim().equals("M"))
                 { v_Img_GtAcc.set(i, g_Img_GtAcc); }
              else
                 {
                   if (g_Img_GtApe.GtApe_Psw.toString().trim().equals("E"))
                      { v_Img_GtAcc.remove(i); }
                 }
            }
         Recalcula_GtApe(data, context);
         rg.MoverA(g_Img_GtApe.GtApe_Idx, " ");
         rg.MoverA(g_Img_GtApe.GtApe_Psw, " ");
         data.getUser().removeTemp("Agtp316_ImgGtApe");
         data.getUser().setTemp("Agtp316_ImgGtApe", g_Img_GtApe);
         data.getUser().removeTemp("Agtp316_ImgGtAcc");
         data.getUser().removeTemp("Agtp316_vImgGtAcc");
         data.getUser().setTemp("Agtp316_vImgGtAcc", v_Img_GtAcc);
         setTemplate(data, "Garantias,Agt,AGTP316.vm" );
         return;
       }
    if (Opc.trim().equals("N"))
       {
         //Boton No
         data.getUser().removeTemp("Agtp316_ImgGtAcc");
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp316_ImgGtApe");
         rg.MoverA(g_Img_GtApe.GtApe_Idx, " ");
         rg.MoverA(g_Img_GtApe.GtApe_Psw, " ");
         data.getUser().removeTemp("Agtp316_ImgGtApe");
         data.getUser().setTemp("Agtp316_ImgGtApe", g_Img_GtApe);
         setTemplate(data, "Garantias,Agt,AGTP316.vm" );
         return;
       }
  }
  //---------------------------------------------------------------------------------------
  public void Recalcula_GtApe(RunData data, Context context)
              throws Exception
  {
    String lcVtt = rg.Zeros(15);
    String lcVts = rg.Zeros(15);
    String lcVtb = rg.Zeros(15);
    String lcVcn = rg.Zeros(15);
    for (i=0; i<v_Img_GtAcc.size(); i++)
        {
          g_Img_GtAcc = (GT_IMG.Buf_Img_GtAcc)v_Img_GtAcc.elementAt(i);
          lcVtt = rg.Suma(lcVtt,4,g_Img_GtAcc.GtAcc_Vlc,2,15,4);
          lcVts = rg.Suma(lcVts,2,g_Img_GtAcc.GtAcc_Vlc,2,15,2);
          lcVtb = rg.Suma(lcVtb,2,g_Img_GtAcc.GtAcc_Vlc,2,15,2);
          lcVcn = rg.Suma(lcVcn,2,g_Img_GtAcc.GtAcc_Vlc,2,15,2);
        }
    rg.MoverA(g_Img_GtApe.GtApe_Vtt, lcVtt);
    rg.MoverA(g_Img_GtApe.GtApe_Vts, lcVts);
    rg.MoverA(g_Img_GtApe.GtApe_Vtb, lcVtb);
    rg.MoverA(g_Img_GtApe.GtApe_Vcn, lcVcn);
  }
  //---------------------------------------------------------------------------------------
  public void Actualiza_Datos(RunData data, Context context)
              throws Exception
  {
    rg.MoverA(g_Img_GtAcc.GtAcc_Can, data.getParameters().getString("GtAcc_Can", ""));
    rg.MoverA(g_Img_GtAcc.GtAcc_Vlc, data.getParameters().getString("GtAcc_Vlc", ""));
    rg.MoverA(g_Img_GtAcc.GtAcc_Nbe, data.getParameters().getString("GtAcc_Nbe", ""));
  }
  //---------------------------------------------------------------------------------------
  public void Carga_de_Host(RunData data, Context context)
              throws Exception
  {
    //v_Tab_Img = img.Carga_Img_Host(g_Msg, g_PrmPC080.PC080_Ntr.toString(), data, context);
    v_Tab_Img = (Vector)data.getUser().getTemp("Agtp305_gTabImg");
    De_Tab_Img(v_Tab_Img, data);
    if (g_Img_GtApe.GtApe_Tfr.toString().trim().equals(""))
       { rg.MoverA(g_Img_GtApe.GtApe_Tfr, rg.Zeros(3)); }
    if (g_Img_GtApe.GtApe_Fig.toString().trim().equals(""))
       {
         rg.MoverA(g_Img_GtApe.GtApe_Fig, g_Msg.Msg_Fch);
         rg.MoverA(g_Img_GtApe.GtApe_Vuo, rg.Zeros(g_Img_GtApe.GtApe_Vuo));
         rg.MoverA(g_Img_GtApe.GtApe_Fts, g_Msg.Msg_Fch);
       }
    if (g_Img_GtApe.GtApe_Vtt.toString().trim().equals(""))
       {
         rg.MoverA(g_Img_GtApe.GtApe_Vtt, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Vts, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Vtb, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Vcn, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Hvt, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Hvl, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Hvb, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Hvc, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Pvt, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Pvl, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Pvb, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Pvc, rg.Zeros(15));
       }
    data.getUser().removeTemp("Agtp316_ImgBase");
    data.getUser().setTemp("Agtp316_ImgBase", g_Img_Base);
    data.getUser().removeTemp("Agtp316_ImgGtApe");
    data.getUser().setTemp("Agtp316_ImgGtApe", g_Img_GtApe);
    data.getUser().removeTemp("Agtp316_vImgGtAcc");
    data.getUser().setTemp("Agtp316_vImgGtAcc", v_Img_GtAcc);
    data.getUser().removeTemp("Agtp316_vTabImg");
    data.getUser().setTemp("Agtp316_vTabImg", v_Tab_Img);
  }
  //---------------------------------------------------------------------------------------
  public void De_Tab_Img(Vector v_Tab_Img, RunData data)
  {
    g_Img       = img.Inicia_Img();
    lcData      = img.LSet_De_Img(g_Img);
    g_Img_Base  = img.LSet_A_ImgBase(lcData);
    g_Img_GtApe = gtimg.LSet_A_ImgGtApe(g_Img.Img_Dat.toString());
    v_Img_GtAcc.clear();
    for (i=0; i<v_Tab_Img.size(); i++)
        {
          lcData = img.LSet_De_Img((BF_IMG.Buf_Img)v_Tab_Img.elementAt(i));
          g_Img  = img.LSet_A_Img(lcData);
          if (g_Img.Img_Dax.toString().trim().equals("BASE"))
             {
               g_Img_GtApe = gtimg.LSet_A_ImgGtApe(g_Img.Img_Dat.toString());
               g_Img_Base  = img.LSet_A_ImgBase(g_Img_GtApe.GtApe_Bse.toString());
             }
          if (g_Img.Img_Dax.toString().trim().equals("ZZACC"))
             {
               g_Img_GtAcc = gtimg.LSet_A_ImgGtAcc(g_Img.Img_Dat.toString());
               v_Img_GtAcc.add (g_Img_GtAcc);
             }
        }
  }
  //---------------------------------------------------------------------------------------
  public void Carga_Inicial(RunData data, Context context)
              throws Exception
  {
    g_MsgED135 = MSGED135.Inicia_MsgED135();
    rg.MoverA(g_MsgED135.ED135_Idr, "C");
    rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
    rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
    g_MsgED135 = MSGED135.LSet_A_MsgED135(g_Msg.Msg_Dat.toString());
    data.getUser().removeTemp("Agtp316_MsgED135");
    data.getUser().setTemp("Agtp316_MsgED135", g_MsgED135);
  }
  //---------------------------------------------------------------------------------------
  public Vector A_Tab_Img(RunData data)
  {
    v_Tab_Img = (Vector)data.getUser().getTemp("Agtp316_vTabImg");
    Vector v_Img = new Vector();
    v_Img.clear();
    rg.MoverA(g_Img.Img_Ntr, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_Img.Img_Dax, "BASE");
    rg.MoverA(g_Img.Img_Seq, "000");
    rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtApe(g_Img_GtApe));
    Vector vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
    v_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
    for (i=0; i<v_Img_GtAcc.size(); i++)
        {
          g_Img_GtAcc = (GT_IMG.Buf_Img_GtAcc)v_Img_GtAcc.elementAt(i);
          rg.MoverA(g_Img.Img_Dax, "ZZACC");
          rg.MoverA(g_Img.Img_Seq, rg.Suma(g_Img.Img_Seq,0,"1",0,3,0));
          rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtAcc(g_Img_GtAcc));
          vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
          v_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
        }
    //--------------------------------------------------------------------------
    rg.MoverA(g_Img.Img_Seq, "000");
    for (i=0; i<v_Tab_Img.size(); i++)
        {
          lcData = img.LSet_De_Img((BF_IMG.Buf_Img)v_Tab_Img.elementAt(i));
          g_Img  = img.LSet_A_Img(lcData);
          if (g_Img.Img_Dax.toString().trim().equals("BASE")
           || g_Img.Img_Dax.toString().trim().equals("ZZACC"))
             { }
          else
             {
               vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
               v_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
             }
        }
    //--------------------------------------------------------------------------
    return v_Img;
  }
//---------------------------------------------------------------------------------------
  public void doAgtp316_Continue(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP316[doAgtp316_Continue.start]", "[" + data.getUser().getUserName() + "]");
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Atfp108"))
       {
         g_PrmTF108 = PRMTF108.LSet_A_PrmTF108(g_Msg.Msg_Dat.toString());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp316_g_PrmPC080");
         data.getUser().removeTemp("Agtp316_g_PrmPC080");
         g_Img_GtAcc = (GT_IMG.Buf_Img_GtAcc)data.getUser().getTemp("Agtp316_ImgGtAcc");
         if (g_PrmTF108.TF108_Gta.toString().trim().compareTo("")>0)
            {
              rg.MoverA(g_Img_GtAcc.GtAcc_Gta, g_PrmTF108.TF108_Gta);
              rg.MoverA(g_Img_GtAcc.GtAcc_Nse, g_PrmTF108.TF108_Nse);
              rg.MoverA(g_Img_GtAcc.GtAcc_Mnd, g_PrmTF108.TF108_Mnd);
              rg.MoverA(g_Img_GtAcc.GtAcc_Fut, g_PrmTF108.TF108_Fut);
              rg.MoverA(g_Img_GtAcc.GtAcc_Uvb, g_PrmTF108.TF108_Uvb);
            }
         data.getUser().removeTemp("Agtp316_ImgGtAcc");
         data.getUser().setTemp("Agtp316_ImgGtAcc", g_Img_GtAcc);
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP316.vm" );
         return;
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Aedp210"))
       {
         g_PrmED210 = PRMED210.LSet_A_PrmED210(g_Msg.Msg_Dat.toString());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp316_g_PrmPC080");
         data.getUser().removeTemp("Agtp316_g_PrmPC080");
       }
  }
  //---------------------------------------------------------------------------------------
}