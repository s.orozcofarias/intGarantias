// Source File Name:   Agtp418.java

package com.intGarantias.modules.actions;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.BF_LOB;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.PRMPC080;
import com.FHTServlet.modules.global.RUTGEN;
import com.intGarantias.modules.global.MSGGT418;

public class Agtp418 extends FHTServletAction {

	public Agtp418() {
		rg = new RUTGEN();
		msg = new BF_MSG();
		g_Msg = new com.FHTServlet.modules.global.BF_MSG.Buf_Msg();
		lob = new BF_LOB();
		g_PrmPC080 = new com.FHTServlet.modules.global.PRMPC080.PrmPC080();
		g_MsgGT418 = new com.intGarantias.modules.global.MSGGT418.Buf_MsgGT418();
	}

	public void doPerform(RunData data, Context context) throws Exception {
		g_Msg = BF_MSG.Init_Program("Agtp418-DIN", data);
		doAgtp418_Init(data, context);
	}

	public void doAgtp418_Init(RunData data, Context context) throws Exception {
		g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
		g_MsgGT418 = MSGGT418.Inicia_MsgGT418();
		RUTGEN.MoverA(g_MsgGT418.GT418_Idr, "I");
		RUTGEN.MoverA(g_MsgGT418.GT418_Gti, g_PrmPC080.PC080_Cnr.Sis.toString()
				+ g_PrmPC080.PC080_Cnr.Ncn.toString());
		RUTGEN.MoverA(g_MsgGT418.GT418_Seq, g_PrmPC080.PC080_Ntc);
		RUTGEN.MoverA(g_Msg.Msg_Dat, MSGGT418.LSet_De_MsgGT418(g_MsgGT418));
		g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GT418");
		g_MsgGT418 = MSGGT418.LSet_A_MsgGT418(g_Msg.Msg_Dat.toString());
		data.getUser().removeTemp("Agtp418_Obb");
		data.getUser().setTemp("Agtp418_Obb",
				lob.leeCLOB(g_MsgGT418.getGbs().getObb()));
		data.getUser().removeTemp("Agtp418_MsgGT418");
		data.getUser().setTemp("Agtp418_MsgGT418", g_MsgGT418);
		data.getUser().removeTemp("Agtp418_PrmPC080");
		data.getUser().setTemp("Agtp418_PrmPC080", g_PrmPC080);
		setTemplate(data, "Garantias,Agt,AGTP418.vm");
	}

	public void doAgtp418(RunData data, Context context) throws Exception {
		g_Msg = BF_MSG.Init_Program("Agtp418-MAN", data);
		g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
		String Opc = data.getParameters().getString("Opc", "");
		if (Opc.trim().equals("S")) {
			data.getUser().removeTemp("Agtp418_Obb");
			data.getUser().removeTemp("Agtp418_MsgGT418");
			data.getUser().removeTemp("Agtp418_PrmPC080");
			RUTGEN.MoverA(g_PrmPC080.PC080_Rtn, "OK");
			RUTGEN.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
			BF_MSG.Return_Program(data, g_Msg);
			return;
		} else {
			return;
		}
	}

	RUTGEN rg;
	BF_MSG msg;
	com.FHTServlet.modules.global.BF_MSG.Buf_Msg g_Msg;
	BF_LOB lob;
	com.FHTServlet.modules.global.PRMPC080.PrmPC080 g_PrmPC080;
	com.intGarantias.modules.global.MSGGT418.Buf_MsgGT418 g_MsgGT418;
}
