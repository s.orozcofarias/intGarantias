// Source File Name:   Agtp290.java
// Descripcion     :   Visado Garantias (ED135, GTCUR)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.MSGTXCUR;
import com.FHTServlet.modules.global.BF_TRN;
import com.FHTServlet.modules.global.BF_IMG;
import com.FHTServlet.modules.global.PRMPC080;
import com.FHTServlet.modules.global.PRMPP030;

import com.intGlobal.modules.global.MSGED135;
import com.intGlobal.modules.global.PRMED210;

import com.intGarantias.modules.global.GT_IMG;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp290 extends FHTServletAction
{
  //---------------------------------------------------------------------------------------
  public int    i = 0;
  public String lcRetorno = "";
  public String lcData = "";
  public Vector vDatos;
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  MSGTXCUR.Buf_TxCur g_MsgTxCur    = new MSGTXCUR.Buf_TxCur();
  MSGTXCUR.Buf_Tx900 g_MsgTx900    = new MSGTXCUR.Buf_Tx900();
  BF_TRN trn                       = new BF_TRN();
  BF_TRN.Buf_Trn g_Trn             = new BF_TRN.Buf_Trn();
  BF_IMG img                       = new BF_IMG();
  BF_IMG.Buf_Img g_Img             = new BF_IMG.Buf_Img();
  BF_IMG.Buf_Img_Bse g_Img_Base    = new BF_IMG.Buf_Img_Bse();
  GT_IMG gtimg                     = new GT_IMG();
  GT_IMG.Buf_Img_GtApe g_Img_GtApe = new GT_IMG.Buf_Img_GtApe();
  GT_IMG.Buf_Img_GtSgr g_Img_GtSgr = new GT_IMG.Buf_Img_GtSgr();
  GT_IMG.Buf_Img_GtRvl g_Img_GtRvl = new GT_IMG.Buf_Img_GtRvl();
  GT_IMG.Buf_Img_GtGeb g_Img_GtGeb = new GT_IMG.Buf_Img_GtGeb();
  GT_IMG.Buf_Img_GtAlz g_Img_GtAlz = new GT_IMG.Buf_Img_GtAlz();
  GT_IMG.Buf_Img_GtCof g_Img_GtCof = new GT_IMG.Buf_Img_GtCof();
  GT_IMG.Buf_Img_GtMmc g_Img_GtMmc = new GT_IMG.Buf_Img_GtMmc();
//  GT_IMG.Buf_Img_GtCpd g_Img_GtCpd = new GT_IMG.Buf_Img_GtCpd();
  GT_IMG.Buf_Img_GtAnu g_Img_GtAnu = new GT_IMG.Buf_Img_GtAnu();
  PRMPC080.PrmPC080 g_PrmPC080     = new PRMPC080.PrmPC080();
  MSGED135.Buf_MsgED135 g_MsgED135 = new MSGED135.Buf_MsgED135();
  PRMED210.Buf_PrmED210 g_PrmED210 = new PRMED210.Buf_PrmED210();
  PRMPP030.Buf_PrmPP030 g_PrmPP030 = new PRMPP030.Buf_PrmPP030();
  //---------------------------------------------------------------------------------------
  public Agtp290()
  {
  }
  //---------------------------------------------------------------------------------------
  public void doPerform(RunData data, Context context) throws Exception
  {
    //Log.debug("AGTP290[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp290-DIN", data);
    if (g_Msg.Msg_Pgr.toString().equals("RT"))
       { doAgtp290_Continue(data, context); }
    else
       { doAgtp290_Init(data, context); }
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp290_Init(RunData data, Context context) throws Exception
  {
    //Log.debug("AGTP290[doAgtp290_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    data.getUser().removeTemp("Agtp290_PrmPC080");
    data.getUser().setTemp("Agtp290_PrmPC080", g_PrmPC080);
    Carga_de_Host(data, context);
    Carga_Inicial(data, context);
    rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
    BF_MSG.Param_Program(data, g_Msg);
    setTemplate(data, "Garantias,Agt,AGTP290.vm");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp290(RunData data, Context context) throws Exception
  {
    //Log.debug("AGTP290[doAgtp290.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp290-MAN", data);
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    String Opc = data.getParameters().getString("Opc", "");
    if (Opc.trim().equals("S"))
       {
         //Boton Salir
         data.getUser().removeTemp("Agtp290_PrmPC080");
         data.getUser().removeTemp("Agtp290_ImgBase");
         data.getUser().removeTemp("Agtp290_Img");
         data.getUser().removeTemp("Agtp290_ImgVisad");
         data.getUser().removeTemp("Agtp290_vTabImg");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "NK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp290", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("A"))
       {
         //Boton Abortar
         data.getUser().removeTemp("Agtp290_g_PrmPC080");
         data.getUser().setTemp("Agtp290_g_PrmPC080", g_PrmPC080);
         rg.MoverA(g_PrmPC080.PC080_Acc, "ABORT");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Link_Program("Agtp290", "Aedp105", data, g_Msg); 
         return;
       }
    if (Opc.trim().equals("I"))
       {
         //Boton IFiscalia
         lcData = PRMPC080.LSet_De_PrmPC080(g_PrmPC080);
         PRMPC080.PrmPC080 l_PrmPC080 = new PRMPC080.PrmPC080();
         l_PrmPC080 = PRMPC080.LSet_A_PrmPC080(lcData);
         data.getUser().removeTemp("Agtp290_g_PrmPC080");
         data.getUser().setTemp("Agtp290_g_PrmPC080", l_PrmPC080);
         rg.MoverA(g_PrmPC080.PC080_Rtn, "RO");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Link_Program("Agtp290", "Agtp310", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("T"))
       {
         //Boton Detalle
         lcData = PRMPC080.LSet_De_PrmPC080(g_PrmPC080);
         PRMPC080.PrmPC080 l_PrmPC080 = new PRMPC080.PrmPC080();
         l_PrmPC080 = PRMPC080.LSet_A_PrmPC080(lcData);
         data.getUser().removeTemp("Agtp290_g_PrmPC080");
         data.getUser().setTemp("Agtp290_g_PrmPC080", l_PrmPC080);
         String lcPrograma = "NoAsignado";
         rg.MoverA(g_PrmPC080.PC080_Rtn, "RO");
         if (g_PrmPC080.PC080_Ttr.toString().trim().equals("APERT")
          || g_PrmPC080.PC080_Ttr.toString().trim().equals("RETAS")
          || g_PrmPC080.PC080_Ttr.toString().trim().equals("MDTAS"))
            {
       	      g_Img       = (BF_IMG.Buf_Img)data.getUser().getTemp("Agtp290_Img");
       	      g_Img_GtApe = gtimg.LSet_A_ImgGtApe(g_Img.Img_Dat.toString());
              if (g_Img_GtApe.GtApe_Ggr.toString().trim().equals("HYP")
               || g_Img_GtApe.GtApe_Ggr.toString().trim().equals("CRH"))
                 { lcPrograma = "Agtp724"; }
              if (g_Img_GtApe.GtApe_Ggr.toString().trim().equals("AYF"))
                 { lcPrograma = "Agtp313"; }
              if (g_Img_GtApe.GtApe_Ggr.toString().trim().equals("CTW"))
                 { lcPrograma = "Agtp305"; }  //314
              if (g_Img_GtApe.GtApe_Ggr.toString().trim().equals("DMN")
               || g_Img_GtApe.GtApe_Ggr.toString().trim().equals("DMX")
               || g_Img_GtApe.GtApe_Ggr.toString().trim().equals("PMN")
               || g_Img_GtApe.GtApe_Ggr.toString().trim().equals("PMX"))
                 { lcPrograma = "Agtp305"; }  //315
              if (g_Img_GtApe.GtApe_Ggr.toString().trim().equals("PAC"))
                 { lcPrograma = "Agtp305"; }  //316
              if (g_Img_GtApe.GtApe_Ggr.toString().trim().equals("CMA"))    
                 { lcPrograma = "Agtp110"; }
              if (g_Img_GtApe.GtApe_Ggr.toString().trim().equals("MYC"))    
                 { lcPrograma = "Agtp319"; }
              if (g_Img_GtApe.GtApe_Ggr.toString().trim().equals("GFU"))    
                 { lcPrograma = "Agtp720"; }
            }
         if (g_PrmPC080.PC080_Ttr.toString().trim().equals("RETPI"))
            { lcPrograma = "Agtp661"; }
         if (g_PrmPC080.PC080_Ttr.toString().trim().equals("TERPI"))
            { lcPrograma = "Agtp734"; }
         if (g_PrmPC080.PC080_Ttr.toString().trim().equals("RVSEG"))
            { lcPrograma = "Agtp625"; }
         if (g_PrmPC080.PC080_Ttr.toString().trim().equals("REVAL"))
            { lcPrograma = "Agtp626"; }
         if (g_PrmPC080.PC080_Ttr.toString().trim().equals("CRGES"))
            { lcPrograma = "Agtp175"; }
         if (g_PrmPC080.PC080_Ttr.toString().trim().equals("ALZMT"))
            { lcPrograma = "Agtp170"; }
         if (g_PrmPC080.PC080_Ttr.toString().trim().equals("ALZPI"))
            { lcPrograma = "Agtp171"; }
         if (g_PrmPC080.PC080_Ttr.toString().trim().equals("ADDEM"))
            { lcPrograma = "Agtp311"; }
         if (g_PrmPC080.PC080_Ttr.toString().trim().equals("INSPC"))
            { lcPrograma = "Agtp714"; }
         if (g_PrmPC080.PC080_Ttr.toString().trim().equals("CMBOF"))
            { lcPrograma = "Agtp263"; }
         if (g_PrmPC080.PC080_Ttr.toString().trim().equals("CBDCN"))
            { lcPrograma = "Agtp262"; }
         if (g_PrmPC080.PC080_Ttr.toString().trim().equals("MODMC"))
            { lcPrograma = "Agtp619"; }
         rg.MoverA(g_PrmPC080.PC080_Acc, "CONSU");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Link_Program("Agtp290", lcPrograma, data, g_Msg);
         return;
       }
    if (Opc.trim().equals("G"))
       {
         //Boton seguros
         lcData = PRMPC080.LSet_De_PrmPC080(g_PrmPC080);
         PRMPC080.PrmPC080 l_PrmPC080 = new PRMPC080.PrmPC080();
         l_PrmPC080 = PRMPC080.LSet_A_PrmPC080(lcData);
         data.getUser().removeTemp("Agtp290_g_PrmPC080");
         data.getUser().setTemp("Agtp290_g_PrmPC080", l_PrmPC080);
         rg.MoverA(g_PrmPC080.PC080_Rtn, "RO");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Link_Program("Agtp290", "Agtp150", data, g_Msg);
         return;
       }
    if (Opc.equals("D"))
       {
         //Devolver [a Operador para Correccion]
         rg.MoverA(g_PrmPC080.PC080_Acc, "DEVOL");
         data.getUser().removeTemp("Agtp290_g_PrmPC080");
         data.getUser().setTemp("Agtp290_g_PrmPC080", g_PrmPC080);
         g_PrmED210 = PRMED210.Inicia_PrmED210();
         rg.MoverA(g_PrmED210.ED210_Rtn, "RW");
         rg.MoverA(g_Msg.Msg_Dat, PRMED210.LSet_De_PrmED210(g_PrmED210));
         BF_MSG.Link_Program("Agtp290", "Aedp210", data, g_Msg); 
       	 return;
       }
    if (Opc.equals("F"))
       {
         //Devolver [a Fiscalia]
         rg.MoverA(g_PrmPC080.PC080_Acc, "DEVFIS");
         data.getUser().removeTemp("Agtp290_g_PrmPC080");
         data.getUser().setTemp("Agtp290_g_PrmPC080", g_PrmPC080);
         g_PrmED210 = PRMED210.Inicia_PrmED210();
         rg.MoverA(g_PrmED210.ED210_Rtn, "RW");
         rg.MoverA(g_Msg.Msg_Dat, PRMED210.LSet_De_PrmED210(g_PrmED210));
         BF_MSG.Link_Program("Agtp290", "Aedp210", data, g_Msg); 
       	 return;
       }
    if (Opc.equals("U"))
       {
         //Devolver [a UVT]
         rg.MoverA(g_PrmPC080.PC080_Acc, "DEVUVT");
         data.getUser().removeTemp("Agtp290_g_PrmPC080");
         data.getUser().setTemp("Agtp290_g_PrmPC080", g_PrmPC080);
         g_PrmED210 = PRMED210.Inicia_PrmED210();
         rg.MoverA(g_PrmED210.ED210_Rtn, "RW");
         rg.MoverA(g_Msg.Msg_Dat, PRMED210.LSet_De_PrmED210(g_PrmED210));
         BF_MSG.Link_Program("Agtp290", "Aedp210", data, g_Msg); 
       	 return;
       }
    if (Opc.trim().equals("C"))
       {
         //Boton Cursar
         data.getUser().removeTemp("Agtp290_g_PrmPC080");
         data.getUser().setTemp("Agtp290_g_PrmPC080", g_PrmPC080);
         if (Cursar(data, context) == false )
            {
              BF_MSG.MsgInt("Agtp290", data, g_Msg, "Msg_Rtn", "T"); 
              return;
            }
         rg.MoverA(g_Msg.Msg_Dat, MSGTXCUR.LSet_De_MsgTxCur(g_MsgTxCur));
         g_MsgTx900 = MSGTXCUR.LSet_A_MsgTx900(g_Msg.Msg_Dat.toString());
         data.getUser().removeTemp("Agtp291_MsgTx900");
         data.getUser().setTemp("Agtp291_MsgTx900", g_MsgTx900);
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Link_Program("Agtp290", "Agtp291", data, g_Msg);
         return;
       }
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp290_Continue(RunData data, Context context) throws Exception
  {
    //Log.debug("AGTP290[doAgtp290_Continue.start]", "[" + data.getUser().getUserName() + "]");
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Appp030"))
       {
         g_PrmPP030 = PRMPP030.LSet_A_PrmPP030(g_Msg.Msg_Dat.toString());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp290_g_PrmPC080");
         data.getUser().removeTemp("Agtp290_g_PrmPC080");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         if (g_PrmPP030.PP030_Rsp.toString().trim().equals("TERMINAR"))
            {
              data.getUser().removeTemp("Agtp290_PrmPC080");
              data.getUser().removeTemp("Agtp290_ImgBase");
              data.getUser().removeTemp("Agtp290_Img");
              data.getUser().removeTemp("Agtp290_ImgVisad");
              data.getUser().removeTemp("Agtp290_vTabImg");
              BF_MSG.Return_Data("Agtp290", data, g_Msg);
              return;
            }
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP290.vm" );
         return;
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Aedp105"))
       {
         PRMPC080.PrmPC080 l_PrmPC080 = new PRMPC080.PrmPC080();
         l_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp290_g_PrmPC080");
         data.getUser().removeTemp("Agtp290_g_PrmPC080");
         if (l_PrmPC080.PC080_Rtn.toString().equals("OK"))
            {
              data.getUser().removeTemp("Agtp290_PrmPC080");
              data.getUser().removeTemp("Agtp290_ImgBase");
              data.getUser().removeTemp("Agtp290_Img");
              data.getUser().removeTemp("Agtp290_ImgVisad");
              data.getUser().removeTemp("Agtp290_vTabImg");
              rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
              rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
              BF_MSG.Return_Data("Agtp290", data, g_Msg);
              return;
            }
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP290.vm" );
         return;
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Aedp210"))
       {
         g_PrmED210 = PRMED210.LSet_A_PrmED210(g_Msg.Msg_Dat.toString());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp290_g_PrmPC080");
         data.getUser().removeTemp("Agtp290_g_PrmPC080");
         if (g_PrmED210.ED210_Rtn.toString().equals("OK"))
            {
              g_MsgED135 = MSGED135.Inicia_MsgED135();
              if (g_PrmPC080.PC080_Acc.toString().trim().equals("DEVOL"))
                 { rg.MoverA(g_MsgED135.ED135_Idr, "X"); }
              if (g_PrmPC080.PC080_Acc.toString().trim().equals("DEVFIS"))
                 { rg.MoverA(g_MsgED135.ED135_Idr, "F"); }
              if (g_PrmPC080.PC080_Acc.toString().trim().equals("DEVUVT"))
                 { rg.MoverA(g_MsgED135.ED135_Idr, "U"); }
              rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
              rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
              rg.MoverA(g_MsgED135.ED135_Est, "");
              rg.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
              rg.MoverA(g_MsgED135.ED135_Ldv, g_PrmED210.ED210_Ldt);
              if (g_PrmPC080.PC080_Trt.toString().equals("UNICO"))
                 {
                   if (g_PrmPC080.PC080_Ggr.toString().equals("CMA"))
                      { rg.MoverA(g_MsgED135.ED135_Ezt, "DVSCM"); }
                   else
                      { rg.MoverA(g_MsgED135.ED135_Ezt, "DVCUR"); }
                 }
              else
                 { 
                   rg.MoverA(g_MsgED135.ED135_Ezt, "DVSPR");
                 }
              rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
              g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
              data.getUser().removeTemp("Agtp290_PrmPC080");
              data.getUser().removeTemp("Agtp290_ImgBase");
              data.getUser().removeTemp("Agtp290_Img");
              data.getUser().removeTemp("Agtp290_ImgVisad");
              data.getUser().removeTemp("Agtp290_vTabImg");
              rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
              rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
              BF_MSG.Return_Data("Agtp290", data, g_Msg);
              return;
            }
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP290.vm" );
         return;
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Agtp291"))
       {
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp290_g_PrmPC080");
         data.getUser().removeTemp("Agtp290_g_PrmPC080");
         data.getUser().removeTemp("Agtp290_PrmPC080");
         data.getUser().removeTemp("Agtp290_ImgBase");
         data.getUser().removeTemp("Agtp290_Img");
         data.getUser().removeTemp("Agtp290_ImgVisad");
         data.getUser().removeTemp("Agtp290_vTabImg");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp290", data, g_Msg);
         return;
       }
    g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp290_g_PrmPC080");
    data.getUser().removeTemp("Agtp290_g_PrmPC080");
    data.getUser().removeTemp("Agtp290_PrmPC080");
    data.getUser().setTemp("Agtp290_PrmPC080", g_PrmPC080);
    rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
    BF_MSG.Param_Program(data, g_Msg);
    setTemplate(data, "Garantias,Agt,AGTP290.vm" );
    return;
  }
  //---------------------------------------------------------------------------------------
  public void Carga_de_Host(RunData data, Context context) throws Exception 
  {
    Vector g_Tab_Img = img.Carga_Img_Host(g_Msg, g_PrmPC080.PC080_Ntr.toString(), data, context);
    De_Tab_Img(g_Tab_Img, data);
    data.getUser().removeTemp("Agtp290_ImgBase");
    data.getUser().setTemp("Agtp290_ImgBase", g_Img_Base);
    data.getUser().removeTemp("Agtp290_Img");
    data.getUser().setTemp("Agtp290_Img", g_Img);
  }
  //---------------------------------------------------------------------------------------
  public void De_Tab_Img(Vector g_Tab_Img, RunData data)
  {
    g_Img       = img.Inicia_Img();
    lcData      = img.LSet_De_Img(g_Img);
    g_Img_Base  = img.LSet_A_ImgBase(lcData);
    for (i=0; i<g_Tab_Img.size(); i++)
        {
          lcData = img.LSet_De_Img((BF_IMG.Buf_Img)g_Tab_Img.elementAt(i));
          g_Img  = img.LSet_A_Img(lcData);
          if (g_Img.Img_Dax.toString().trim().equals("BASE"))
             {
               g_Img_Base = img.LSet_A_ImgBase(g_Img.Img_Dat.toString());
               break;
             }
        }
  }
  //---------------------------------------------------------------------------------------
  public void Carga_Inicial(RunData data, Context context) throws Exception 
  {
    GT_IMG.Buf_Img_Visad g_Img_Visad = new GT_IMG.Buf_Img_Visad();
    BF_IMG.Buf_vImp vImp = new BF_IMG.Buf_vImp();
    Vector v_Imp = new Vector();
    v_Imp.clear();
    if (g_Img_Base.Img_Base_Ttr.toString().equals("APERT")
     || g_Img_Base.Img_Base_Ttr.toString().equals("RETAS")
     || g_Img_Base.Img_Base_Ttr.toString().equals("MDTAS")
     || g_Img_Base.Img_Base_Ttr.toString().equals("ADDEM")
     || g_Img_Base.Img_Base_Ttr.toString().equals("INSPC")
     || g_Img_Base.Img_Base_Ttr.toString().equals("RETPI")
     || g_Img_Base.Img_Base_Ttr.toString().equals("TERPI"))
       {
       	 g_Img_GtApe = gtimg.LSet_A_ImgGtApe(g_Img.Img_Dat.toString());
       	 rg.MoverA(g_Img_Visad.Visad_Ggr, g_Img_GtApe.GtApe_Ggr);
         rg.MoverA(g_Img_Visad.Visad_Dsg, g_Img_GtApe.GtApe_Dsg);
       //if (g_Img_GtApe.getRgf().trim().equals("S") || g_Img_GtApe.getRgf().trim().equals("SI"))
       //   { rg.MoverA(g_Img_Visad.Visad_Dsg, "GFU:"+g_Img_GtApe.getGfu()+" - "+g_Img_GtApe.getDsg().trim()); }
       //else
       //   { rg.MoverA(g_Img_Visad.Visad_Dsg, g_Img_GtApe.GtApe_Dsg); }
       	 rg.MoverA(g_Img_Visad.Visad_Fpr, g_Img_GtApe.GtApe_Fpr);
       	 rg.MoverA(g_Img_Visad.Visad_Vle, g_Img_GtApe.GtApe_Vts);
       	 rg.MoverA(g_Img_Visad.Visad_Fig, g_Img_GtApe.GtApe_Fig);
       	 rg.MoverA(g_Img_Visad.Visad_Cbt, g_Img_GtApe.GtApe_Cbt);
       	 rg.MoverA(g_Img_Visad.Visad_Vcn, g_Img_GtApe.GtApe_Vcn);
       	 rg.MoverA(g_Img_Visad.Visad_Ttr, "");
       	 rg.MoverA(g_Img_Visad.Visad_Tum, "");
         if (g_Img_GtApe.GtApe_Ggr.toString().equals("HYP")
          || g_Img_GtApe.GtApe_Ggr.toString().equals("CRH"))
            {
              rg.MoverA(vImp.vImp_Cod, "HIPOTECAS");
              rg.MoverA(vImp.vImp_Val, g_Img_GtApe.GtApe_Hvc);
              v_Imp.add(vImp);
              BF_IMG.Buf_vImp vImp1 = new BF_IMG.Buf_vImp();
              rg.MoverA(vImp1.vImp_Cod, "PRENDAS");
              rg.MoverA(vImp1.vImp_Val, g_Img_GtApe.GtApe_Pvc);
              v_Imp.add(vImp1);
            }
         else
            {
              rg.MoverA(vImp.vImp_Cod, "GARANTIA");
              rg.MoverA(vImp.vImp_Val, g_Img_GtApe.GtApe_Vcn);
              v_Imp.add(vImp);
            }
       }
    if (g_Img_Base.Img_Base_Ttr.toString().equals("RVSEG"))
       {
       	 g_Img_GtSgr = gtimg.LSet_A_ImgGtSgr(g_Img.Img_Dat.toString());
       	 rg.MoverA(g_Img_Visad.Visad_Ggr, g_Img_GtSgr.GtSgr_Ggr);
       	 rg.MoverA(g_Img_Visad.Visad_Dsg, g_Img_GtSgr.GtSgr_Dsg);
       	 rg.MoverA(g_Img_Visad.Visad_Fpr, g_Img_GtSgr.GtSgr_Fpr);
       	 rg.MoverA(g_Img_Visad.Visad_Vle, rg.Zeros(15));
       	 rg.MoverA(g_Img_Visad.Visad_Fig, g_Img_GtSgr.GtSgr_Fig);
       	 rg.MoverA(g_Img_Visad.Visad_Cbt, "");
       	 rg.MoverA(g_Img_Visad.Visad_Vcn, g_Img_GtSgr.GtSgr_Vcn);
       	 rg.MoverA(g_Img_Visad.Visad_Ttr, g_Img_Base.Img_Base_Dcn);
       	 rg.MoverA(g_Img_Visad.Visad_Tum, "");
         rg.MoverA(vImp.vImp_Cod, "GARANTIA");
         rg.MoverA(vImp.vImp_Val, rg.Zeros(15));
         v_Imp.add(vImp);
       }
    if (g_Img_Base.Img_Base_Ttr.toString().equals("REVAL"))
       {
       	 g_Img_GtRvl = gtimg.LSet_A_ImgGtRvl(g_Img.Img_Dat.toString());
       	 rg.MoverA(g_Img_Visad.Visad_Ggr, g_Img_GtRvl.GtRvl_Ggr);
       	 rg.MoverA(g_Img_Visad.Visad_Dsg, g_Img_GtRvl.GtRvl_Dsg);
       	 rg.MoverA(g_Img_Visad.Visad_Fpr, g_Img_GtRvl.GtRvl_Fpr);
       	 rg.MoverA(g_Img_Visad.Visad_Vle, g_Img_GtRvl.GtRvl_Vts);
       	 rg.MoverA(g_Img_Visad.Visad_Fig, g_Img_GtRvl.GtRvl_Fig);
       	 rg.MoverA(g_Img_Visad.Visad_Cbt, g_Img_GtRvl.GtRvl_Cbt);
       	 rg.MoverA(g_Img_Visad.Visad_Vcn, g_Img_GtRvl.GtRvl_Vcn);
       	 rg.MoverA(g_Img_Visad.Visad_Ttr, g_Img_Base.Img_Base_Dcn);
       	 rg.MoverA(g_Img_Visad.Visad_Tum, "");
         rg.MoverA(vImp.vImp_Cod, "GARANTIA");
         rg.MoverA(vImp.vImp_Val, g_Img_GtRvl.GtRvl_Vcn);
         v_Imp.add(vImp);
       }
    if (g_Img_Base.Img_Base_Ttr.toString().equals("CRGES"))
       {
       	 g_Img_GtGeb = gtimg.LSet_A_ImgGtGeb(g_Img.Img_Dat.toString());
       	 rg.MoverA(g_Img_Visad.Visad_Ggr, g_Img_GtGeb.GtGeb_Ggr);
       	 rg.MoverA(g_Img_Visad.Visad_Dsg, g_Img_GtGeb.GtGeb_Dsg);
       	 rg.MoverA(g_Img_Visad.Visad_Fpr, g_Img_GtGeb.GtGeb_Fpr);
       	 rg.MoverA(g_Img_Visad.Visad_Vle, rg.Zeros(15));
       	 rg.MoverA(g_Img_Visad.Visad_Fig, g_Img_GtGeb.GtGeb_Fig);
       	 rg.MoverA(g_Img_Visad.Visad_Cbt, g_Img_GtGeb.GtGeb_Cbt);
       	 rg.MoverA(g_Img_Visad.Visad_Vcn, g_Img_GtGeb.GtGeb_Vcn);
       	 rg.MoverA(g_Img_Visad.Visad_Ttr, g_Img_Base.Img_Base_Dcn);
       	 rg.MoverA(g_Img_Visad.Visad_Tum, "");
         rg.MoverA(vImp.vImp_Cod, "GARANTIA");
         rg.MoverA(vImp.vImp_Val, rg.Zeros(15));
         v_Imp.add(vImp);
       }
    if (g_Img_Base.Img_Base_Ttr.toString().equals("ALZMT")
     || g_Img_Base.Img_Base_Ttr.toString().equals("ALZPI"))
       {
       	 g_Img_GtAlz = gtimg.LSet_A_ImgGtAlz(g_Img.Img_Dat.toString());
       	 rg.MoverA(g_Img_Visad.Visad_Ggr, g_Img_GtAlz.GtAlz_Ggr);
       	 rg.MoverA(g_Img_Visad.Visad_Dsg, g_Img_GtAlz.GtAlz_Dsg);
       	 rg.MoverA(g_Img_Visad.Visad_Fpr, g_Img_GtAlz.GtAlz_Fpr);
       	 rg.MoverA(g_Img_Visad.Visad_Vle, g_Img_GtAlz.GtAlz_Vts);
       	 rg.MoverA(g_Img_Visad.Visad_Fig, g_Img_GtAlz.GtAlz_Fig);
       	 rg.MoverA(g_Img_Visad.Visad_Cbt, g_Img_GtAlz.GtAlz_Cbt);
       	 rg.MoverA(g_Img_Visad.Visad_Vcn, g_Img_GtAlz.GtAlz_Vcn);
       	 rg.MoverA(g_Img_Visad.Visad_Ttr, g_Img_Base.Img_Base_Dcn);
       	 rg.MoverA(g_Img_Visad.Visad_Tum, "");
         rg.MoverA(vImp.vImp_Cod, "GARANTIA");
         rg.MoverA(vImp.vImp_Val, g_Img_GtAlz.GtAlz_Vcn);
         v_Imp.add(vImp);
       }
    if (g_Img_Base.Img_Base_Ttr.toString().equals("CMBOF"))
       {
       	 g_Img_GtCof = gtimg.LSet_A_ImgGtCof(g_Img.Img_Dat.toString());
       	 rg.MoverA(g_Img_Visad.Visad_Ggr, g_Img_GtCof.GtCof_Ggr);
       	 rg.MoverA(g_Img_Visad.Visad_Dsg, g_Img_GtCof.GtCof_Dsg);
       	 rg.MoverA(g_Img_Visad.Visad_Fpr, g_Img_GtCof.GtCof_Fmd);
       	 rg.MoverA(g_Img_Visad.Visad_Vle, g_Img_GtCof.GtCof_Snp);
       	 rg.MoverA(g_Img_Visad.Visad_Fig, g_Img_GtCof.GtCof_Fot);
       	 rg.MoverA(g_Img_Visad.Visad_Cbt, "");
       	 rg.MoverA(g_Img_Visad.Visad_Vcn, g_Img_GtCof.GtCof_Snp);
       	 rg.MoverA(g_Img_Visad.Visad_Ttr, g_Img_Base.Img_Base_Dcn);
       	 rg.MoverA(g_Img_Visad.Visad_Tum, "");
         rg.MoverA(vImp.vImp_Cod, "GARANTIA");
         rg.MoverA(vImp.vImp_Val, g_Img_GtCof.GtCof_Snp);
         v_Imp.add(vImp);
       }
//    if (g_Img_Base.Img_Base_Ttr.toString().equals("CBDCN"))
//       {
//       	 g_Img_GtCpd = gtimg.LSet_A_ImgGtCpd(g_Img.Img_Dat.toString());
//       	 rg.MoverA(g_Img_Visad.Visad_Ggr, g_Img_GtCpd.GtCpd_Ggr);
//       	 rg.MoverA(g_Img_Visad.Visad_Dsg, g_Img_GtCpd.GtCpd_Dsg);
//       	 rg.MoverA(g_Img_Visad.Visad_Fpr, g_Img_GtCpd.GtCpd_Fmd);
//       	 rg.MoverA(g_Img_Visad.Visad_Vle, g_Img_GtCpd.GtCpd_Snp);
//       	 rg.MoverA(g_Img_Visad.Visad_Fig, g_Img_GtCpd.GtCpd_Fot);
//       	 rg.MoverA(g_Img_Visad.Visad_Cbt, "");
//       	 rg.MoverA(g_Img_Visad.Visad_Vcn, g_Img_GtCpd.GtCpd_Snp);
//       	 rg.MoverA(g_Img_Visad.Visad_Ttr, g_Img_Base.Img_Base_Dcn);
//       	 rg.MoverA(g_Img_Visad.Visad_Tum, "");
//         rg.MoverA(vImp.vImp_Cod, "GARANTIA");
//         rg.MoverA(vImp.vImp_Val, g_Img_GtCpd.GtCpd_Snp);
//         v_Imp.add(vImp);
//       }
    if (g_Img_Base.Img_Base_Ttr.toString().equals("MODMC"))
       {
       	 g_Img_GtMmc = gtimg.LSet_A_ImgGtMmc(g_Img.Img_Dat.toString());
       	 rg.MoverA(g_Img_Visad.Visad_Ggr, g_Img_GtMmc.GtMmc_Ggr);
       	 rg.MoverA(g_Img_Visad.Visad_Dsg, g_Img_GtMmc.GtMmc_Dsg);
       	 rg.MoverA(g_Img_Visad.Visad_Fpr, g_Img_GtMmc.GtMmc_Fmd);
       	 rg.MoverA(g_Img_Visad.Visad_Vle, g_Img_GtMmc.GtMmc_Vts);
       	 rg.MoverA(g_Img_Visad.Visad_Fig, g_Img_GtMmc.GtMmc_Fig);
       	 rg.MoverA(g_Img_Visad.Visad_Cbt, g_Img_GtMmc.GtMmc_Cbt);
       	 rg.MoverA(g_Img_Visad.Visad_Vcn, g_Img_GtMmc.GtMmc_Vcn);
       	 rg.MoverA(g_Img_Visad.Visad_Ttr, g_Img_Base.Img_Base_Dcn);
       	 rg.MoverA(g_Img_Visad.Visad_Tum, "");
         rg.MoverA(vImp.vImp_Cod, "GARANTIA");
         rg.MoverA(vImp.vImp_Val, rg.Zeros(15));
         v_Imp.add(vImp);
       }
    if (g_Img_Base.Img_Base_Ttr.toString().equals("ANULA"))
       {
       	 g_Img_GtAnu = gtimg.LSet_A_ImgGtAnu(g_Img.Img_Dat.toString());
       	 rg.MoverA(g_Img_Visad.Visad_Ggr, "");
       	 rg.MoverA(g_Img_Visad.Visad_Dsg, g_Img_GtAnu.GtAnu_Ntt);
       	 rg.MoverA(g_Img_Visad.Visad_Fpr, g_Img_GtAnu.GtAnu_Fum);
       	 rg.MoverA(g_Img_Visad.Visad_Vle, rg.Zeros(15));
       	 rg.MoverA(g_Img_Visad.Visad_Fig, "");
       	 rg.MoverA(g_Img_Visad.Visad_Cbt, "");
       	 rg.MoverA(g_Img_Visad.Visad_Vcn, rg.Zeros(15));
       	 rg.MoverA(g_Img_Visad.Visad_Ttr, g_Img_GtAnu.GtAnu_Ttr);
       	 rg.MoverA(g_Img_Visad.Visad_Tum, g_Img_GtAnu.GtAnu_Tum);
         rg.MoverA(vImp.vImp_Cod, "GARANTIA");
         rg.MoverA(vImp.vImp_Val, rg.Zeros(15));
         v_Imp.add(vImp);
       }
    data.getUser().removeTemp("Agtp290_ImgVisad");
    data.getUser().setTemp("Agtp290_ImgVisad", g_Img_Visad);
    data.getUser().removeTemp("Agtp290_vImp");
    data.getUser().setTemp("Agtp290_vImp", v_Imp);
    rg.MoverA(g_PrmPC080.PC080_Ggr, g_Img_Visad.Visad_Ggr);
  }
  //---------------------------------------------------------------------------------------
  public boolean Cursar(RunData data, Context context) throws Exception 
  {
    g_Img      = (BF_IMG.Buf_Img)data.getUser().getTemp("Agtp290_Img");
    g_Img_Base = img.LSet_A_ImgBase(g_Img.Img_Dat.toString());
    //------------------------------------------------------------------------------
    rg.MoverA(g_Trn.Trn_Ntr, g_Img_Base.Img_Base_Ntr);
    rg.MoverA(g_Trn.Trn_Trm, "PCVB");
    rg.MoverA(g_Trn.Trn_Fem, g_Msg.Msg_Fch);
    rg.MoverA(g_Trn.Trn_Hrm, rg.Zeros(6));
    rg.MoverA(g_Trn.Trn_Ttr, g_Img_Base.Img_Base_Ttr);
    rg.MoverA(g_Trn.Trn_Cli, g_Img_Base.Img_Base_Cli);
    rg.MoverA(g_Trn.Trn_Cnr, g_Img_Base.Img_Base_Sis.toString() + g_Img_Base.Img_Base_Ncn.toString());
    rg.MoverA(g_Trn.Trn_Suc, g_Img_Base.Img_Base_Suc);
    rg.MoverA(g_Trn.Trn_Eje, g_Img_Base.Img_Base_Eje);
    rg.MoverA(g_Trn.Trn_Est, "ENPRO");
    rg.MoverA(g_Trn.Trn_Usr, g_Msg.Msg_Cus);
    rg.MoverA(g_Trn.Trn_Cja, rg.Zeros(4));
    rg.MoverA(g_Trn.Trn_Stf, rg.Zeros(9));
    rg.MoverA(g_Trn.Trn_Dcn, g_Img_Base.Img_Base_Dcn);
    rg.MoverA(g_Trn.Trn_Tmn, g_Img_Base.Img_Base_Tmn);
    rg.MoverA(g_Trn.Trn_Trj, g_Img_Base.Img_Base_Trj);
    rg.MoverA(g_Trn.Trn_Dcm, g_Img_Base.Img_Base_Dcm);
    rg.MoverA(g_Trn.Trn_Mnd, g_Img_Base.Img_Base_Mnd);
    rg.MoverA(g_Trn.Trn_Mnt, rg.Zeros(15));
    //------------------------------------------------------------------------------
    rg.MoverA(g_MsgTxCur.TxCur_Bco, "DEMO");                                //"BICE"
    rg.MoverA(g_MsgTxCur.TxCur_Cic, "NO");
    rg.MoverA(g_MsgTxCur.TxCur_Lin, " ");
    rg.MoverA(g_MsgTxCur.TxCur_Rsp, " ");
    rg.MoverA(g_MsgTxCur.TxCur_Idc, "05");
    rg.MoverA(g_MsgTxCur.TxCur_Stp, " ");
    rg.MoverA(g_MsgTxCur.TxCur_Imr, " ");
    rg.MoverA(g_MsgTxCur.TxCur_Msg, " ");
    rg.MoverA(g_MsgTxCur.TxCur_Trn, BF_TRN.LSet_De_Trn(g_Trn));
    rg.MoverA(g_MsgTxCur.TxCur_Img, " ");
    rg.MoverA(g_Msg.Msg_Dat, MSGTXCUR.LSet_De_MsgTxCur(g_MsgTxCur));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GTCUR");
    if (g_Msg.Msg_Rtn.toString().compareTo("0")!=0)
       { return false; }
    //------------------------------------------------------------------------------
    rg.MoverA(g_MsgTxCur.TxCur_Idc, "00");
    rg.MoverA(g_MsgTxCur.TxCur_Img, img.LSet_De_Img(g_Img));
    rg.MoverA(g_Msg.Msg_Dat, MSGTXCUR.LSet_De_MsgTxCur(g_MsgTxCur));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GTCUR");
    if (g_Msg.Msg_Rtn.toString().compareTo("0")!=0)
       { return false; }
    g_MsgTxCur = MSGTXCUR.LSet_A_MsgTxCur(g_Msg.Msg_Dat.toString());
    g_MsgED135 = MSGED135.Inicia_MsgED135();
    rg.MoverA(g_MsgED135.ED135_Idr, "M");
    rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
    rg.MoverA(g_MsgED135.ED135_Est, "CRTRA");
    rg.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
    rg.MoverA(g_MsgED135.ED135_Ezt, "TERMI");
    rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
    //------------------------------------------------------------------------------
    return true;
  }
  //---------------------------------------------------------------------------------------
}