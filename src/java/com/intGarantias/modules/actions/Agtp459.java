// Source File Name:   Agtp459.java
// Descripcion:        Despliega Imagenes (-)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.PRMPC080;

import com.intGarantias.modules.global.MSGGT459;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp459 extends FHTServletAction
{
  //---------------------------------------------------------------------------------------
  public int    i = 0;
  public String lcData = "";
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  PRMPC080.PrmPC080 g_PrmPC080     = new PRMPC080.PrmPC080();
  MSGGT459.Buf_MsgGT459 g_MsgGT459 = new MSGGT459.Buf_MsgGT459();
  //---------------------------------------------------------------------------------------
  public Agtp459()
  {
  }
  //---------------------------------------------------------------------------------------
  public void doPerform(RunData data, Context context)
              throws Exception
  {
  //Log.debug("AGTP459[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp459-DIN", data);
    doAgtp459_Init(data, context);
  }
  //---------------------------------------------------------------------------------------
  public void doCancel(RunData data, Context context)
              throws Exception
  {
  //Log.debug("AGTP459[doCancel.start]", "[CANCELADO]");
    setTemplate(data, "Garantias,Agt,AGTP459.vm");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp459_Init(RunData data, Context context)
              throws Exception
  {
  //Log.debug("AGTP459[doAgtp459_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    g_MsgGT459 = MSGGT459.Inicia_MsgGT459();
    if (g_PrmPC080.PC080_Amv.toString().equals("I"))
       { rg.MoverA(g_MsgGT459.GT459_Idr, "I"); }
    else
       { rg.MoverA(g_MsgGT459.GT459_Idr, "F"); }
    rg.MoverA(g_MsgGT459.GT459_Sis, g_PrmPC080.PC080_Cnr.Sis);
    rg.MoverA(g_MsgGT459.GT459_Ncn, g_PrmPC080.PC080_Cnr.Ncn);
    rg.MoverA(g_MsgGT459.GT459_Seq, g_PrmPC080.PC080_Ntc);
    rg.MoverA(g_Msg.Msg_Dat, MSGGT459.LSet_De_MsgGT459(g_MsgGT459));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GT459");
    g_MsgGT459 = MSGGT459.LSet_A_MsgGT459(g_Msg.Msg_Dat.toString());
    if (g_PrmPC080.PC080_Amv.toString().equals("I"))
       { 
       	 rg.MoverA(g_MsgGT459.GT459_Idr, "I");
       	 if (g_MsgGT459.GT459_Nro.toString().compareTo("000")!=0)
       	    {
              MSGGT459.Bff_MsgGT459 d_MsgGT459 = new MSGGT459.Bff_MsgGT459();
              d_MsgGT459 = (MSGGT459.Bff_MsgGT459)g_MsgGT459.GT459_Tab.elementAt(i);
              rg.MoverA(g_MsgGT459.GT459_Fol, d_MsgGT459.Gif_Fol);
              rg.MoverA(g_MsgGT459.GT459_Dsg, d_MsgGT459.Gif_Dsg);
              rg.MoverA(g_MsgGT459.GT459_Fch, d_MsgGT459.Gif_Fch);
              rg.MoverA(g_MsgGT459.GT459_Tpi, d_MsgGT459.Gif_Tpi);
              rg.MoverA(g_MsgGT459.GT459_Idx, "001");
       	    }
       }
    else
       { rg.MoverA(g_MsgGT459.GT459_Idr, "F"); }
    data.getUser().removeTemp("Agtp459_MsgGT459");
    data.getUser().setTemp("Agtp459_MsgGT459", g_MsgGT459);
    data.getUser().removeTemp("Agtp459_PrmPC080");
    data.getUser().setTemp("Agtp459_PrmPC080", g_PrmPC080);
    setTemplate(data, "Garantias,Agt,AGTP459.vm");
  //Log.debug("AGTP459[doAgtp459_Init.end]", "[" + data.getUser().getUserName() + "]");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp459(RunData data, Context context)
              throws Exception
  {
    g_Msg = BF_MSG.Init_Program("Agtp459-MAN", data);
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    String Opc = data.getParameters().getString("Opc", "");
    if (Opc.trim().equals("S"))
       {
       //Boton Salir
         data.getUser().removeTemp("Agtp459_PrmPC080");
         data.getUser().removeTemp("Agtp459_MsgGT459");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "NK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp459", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("I"))
       {
       //Boton Imagen
         String Seq = data.getParameters().getString("Seq", "");
         i = Integer.parseInt(Seq);
         g_MsgGT459 = (MSGGT459.Buf_MsgGT459)data.getUser().getTemp("Agtp459_MsgGT459");
         MSGGT459.Bff_MsgGT459 d_MsgGT459 = new MSGGT459.Bff_MsgGT459();
         d_MsgGT459 = (MSGGT459.Bff_MsgGT459)g_MsgGT459.GT459_Tab.elementAt(i);
         rg.MoverA(g_MsgGT459.GT459_Fol, d_MsgGT459.Gif_Fol);
         rg.MoverA(g_MsgGT459.GT459_Dsg, d_MsgGT459.Gif_Dsg);
         rg.MoverA(g_MsgGT459.GT459_Fch, d_MsgGT459.Gif_Fch);
         rg.MoverA(g_MsgGT459.GT459_Tpi, d_MsgGT459.Gif_Tpi);
         rg.MoverA(g_MsgGT459.GT459_Idx, rg.FormatNum(i+1,"000"));
         data.getUser().removeTemp("Agtp459_MsgGT459");
         data.getUser().setTemp("Agtp459_MsgGT459", g_MsgGT459);
         data.getUser().removeTemp("Agtp459_PrmPC080");
         data.getUser().setTemp("Agtp459_PrmPC080", g_PrmPC080);
         setTemplate(data, "Garantias,Agt,AGTP459.vm");
         return;
       }
  }
  //---------------------------------------------------------------------------------------
}