// Source File Name:   Agtp170.java
// Descripcion     :   Alzamientos (ED135, ED500, ED535, GT170)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.BF_IMG;
import com.FHTServlet.modules.global.BF_EVT;
import com.FHTServlet.modules.global.PRMPC080;

import com.intGlobal.modules.global.MSGED090;
import com.intGlobal.modules.global.MSGED100;
import com.intGlobal.modules.global.MSGED135;
import com.intGlobal.modules.global.PRMED210;

import com.intGarantias.modules.global.MSGGT170;
import com.intGarantias.modules.global.GT_IMG;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp170 extends FHTServletAction
{
  //---------------------------------------------------------------------------------------
  public int i = 0;
  public String lcData = "";
  public String gcSeq = "";
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  BF_EVT.Buf_Evt g_Evt             = new BF_EVT.Buf_Evt();
  BF_IMG img                       = new BF_IMG();
  BF_IMG.Buf_Img g_Img             = new BF_IMG.Buf_Img();
  BF_IMG.Buf_Img_Bse g_Img_Base    = new BF_IMG.Buf_Img_Bse();
  GT_IMG gtimg                     = new GT_IMG();
  GT_IMG.Buf_Img_GtAlz g_Img_GtAlz = new GT_IMG.Buf_Img_GtAlz();
  GT_IMG.Buf_Img_GtAld g_Img_GtAld = new GT_IMG.Buf_Img_GtAld();
  Vector v_Img_GtAld               = new Vector();
  GT_IMG.Buf_Img_GtApe g_Img_GtApe = new GT_IMG.Buf_Img_GtApe();
  Vector v_Img_GtAcr               = new Vector();
  PRMPC080.PrmPC080 g_PrmPC080     = new PRMPC080.PrmPC080();
  MSGED090.Buf_MsgED090 g_MsgED090 = new MSGED090.Buf_MsgED090();
  MSGED100.Buf_MsgED100 g_MsgED100 = new MSGED100.Buf_MsgED100();
  MSGED135.Buf_MsgED135 g_MsgED135 = new MSGED135.Buf_MsgED135();
  PRMED210.Buf_PrmED210 g_PrmED210 = new PRMED210.Buf_PrmED210();
  MSGGT170.Buf_MsgGT170 g_MsgGT170 = new MSGGT170.Buf_MsgGT170();
  //---------------------------------------------------------------------------------------
  public Agtp170()
  {
  }
  //---------------------------------------------------------------------------------------
  public void doPerform(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP170[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp170-DIN", data);
    if (g_Msg.Msg_Pgr.toString().equals("RT"))
       { doAgtp170_Continue(data, context); }
    else
       { doAgtp170_Init(data, context); }
  }
  //---------------------------------------------------------------------------------------
  public void doCancel(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP170[doCancel.start]", "[CANCELADO]");
    setTemplate(data, "Garantias,Agt,AGTP170.vm");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp170_Init(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP170[doAgtp170_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    g_MsgED135 = MSGED135.Inicia_MsgED135();
    if (g_PrmPC080.PC080_Ntr.toString().trim().compareTo("")!=0)
       {
         Recupera_Evt(data, context);
         Carga_de_Host(data, context);
         if (v_Img_GtAld.size() == 0)
            { Validar_Host("V", data, context); }
         rg.MoverA(g_PrmPC080.PC080_Ntr, g_Img_Base.Img_Base_Ntr);
         rg.MoverA(g_PrmPC080.PC080_Trt, g_MsgED135.ED135_Trt);
         rg.MoverA(g_PrmPC080.PC080_Cli, g_Img_Base.Img_Base_Cli);
         rg.MoverA(g_PrmPC080.PC080_Ncl, g_Img_Base.Img_Base_Ncl);
         rg.MoverA(g_PrmPC080.PC080_Ndc, g_Img_Base.Img_Base_Ndc);
       }
    else
       {
         Carga_Inicial(data, context);
         Init_Evt_GRT(data, context);
       }
    data.getUser().removeTemp("Agtp170_MsgED135");
    data.getUser().setTemp("Agtp170_MsgED135", g_MsgED135);
    data.getUser().removeTemp("Agtp170_Evt");
    data.getUser().setTemp("Agtp170_Evt", g_Evt);
    data.getUser().removeTemp("Agtp170_ImgBase");
    data.getUser().setTemp("Agtp170_ImgBase", g_Img_Base);
    data.getUser().removeTemp("Agtp170_ImgGtAlz");
    data.getUser().setTemp("Agtp170_ImgGtAlz", g_Img_GtAlz);
    data.getUser().removeTemp("Agtp170_vImgGtAld");
    data.getUser().setTemp("Agtp170_vImgGtAld", v_Img_GtAld);
    data.getUser().removeTemp("Agtp170_vImgGtAcr");
    data.getUser().setTemp("Agtp170_vImgGtAcr", v_Img_GtAcr);
    rg.MoverA(g_PrmPC080.PC080_Cli, g_Evt.Evt_Cli);
    rg.MoverA(g_PrmPC080.PC080_Ncl, g_Evt.Evt_Ncl);
    rg.MoverA(g_PrmPC080.PC080_Dir, g_Evt.Evt_Dir);
    rg.MoverA(g_PrmPC080.PC080_Tfc, g_Evt.Evt_Tfc);
    rg.MoverA(g_PrmPC080.PC080_Dcn, g_Evt.Evt_Dcn);
    rg.MoverA(g_PrmPC080.PC080_Ndc, g_Evt.Evt_Ndc);
    rg.MoverA(g_PrmPC080.PC080_Ntt, g_Evt.Evt_Ntt);
    data.getUser().removeTemp("Agtp170_PrmPC080");
    data.getUser().setTemp("Agtp170_PrmPC080", g_PrmPC080);
    rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
    BF_MSG.Param_Program(data, g_Msg);
    setTemplate(data, "Garantias,Agt,AGTP170.vm" );
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp170(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP170[doAgtp170.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp170-MAN", data);
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    String Opc = data.getParameters().getString("Opc", "");
    if (Opc.trim().equals("S"))
       {
         //Boton Salir
         data.getUser().removeTemp("Agtp170_vImgGtAld");
         data.getUser().removeTemp("Agtp170_ImgGtAlz");
         data.getUser().removeTemp("Agtp170_ImgBase");
         data.getUser().removeTemp("Agtp170_Evt");
         data.getUser().removeTemp("Agtp170_PrmPC080");
         data.getUser().removeTemp("Agtp170_vImgGtAcr");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "NK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp170", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("R"))
       {
         //Boton Errores (Para Devolucion a OPERADOR CMA)
         g_MsgED135 = (MSGED135.Buf_MsgED135)data.getUser().getTemp("Agtp170_MsgED135");
         g_PrmED210 = PRMED210.Inicia_PrmED210();
         rg.MoverA(g_PrmED210.ED210_Rtn, "RO");
         rg.MoverA(g_PrmED210.ED210_Ldt, g_MsgED135.ED135_Ldv);
         data.getUser().removeTemp("Agtp170_g_PrmPC080");
         data.getUser().setTemp("Agtp170_g_PrmPC080" ,g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMED210.LSet_De_PrmED210(g_PrmED210));
         BF_MSG.Link_Program("Agtp170", "Aedp210", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("A"))
       {
         //Boton Aceptar SIN Carta Resguardo [OPERADOR CMA]
         v_Img_GtAcr = (Vector)data.getUser().getTemp("Agtp170_vImgGtAcr");
         v_Img_GtAld = (Vector)data.getUser().getTemp("Agtp170_vImgGtAld");
         g_Img_GtAlz = (GT_IMG.Buf_Img_GtAlz)data.getUser().getTemp("Agtp170_ImgGtAlz");
         rg.MoverA(g_Img_GtAlz.GtAlz_Fig, g_Msg.Msg_Fch);
         rg.MoverA(g_Img_GtAlz.GtAlz_Vcn, rg.Zeros(g_Img_GtAlz.GtAlz_Vcn));
         rg.MoverA(g_Img_GtAlz.GtAlz_Vts, rg.Zeros(g_Img_GtAlz.GtAlz_Vts));
         rg.MoverA(g_Img_GtAlz.GtAlz_Rcr, data.getParameters().getString("Rcr", ""));
         for (i=0; i<v_Img_GtAld.size(); i++)
             {
               g_Img_GtAld = (GT_IMG.Buf_Img_GtAld)v_Img_GtAld.elementAt(i);
               if (g_Img_GtAld.GtAld_Est.toString().equals("ALZMT"))
                  {
                    rg.MoverA(g_Img_GtAlz.GtAlz_Vcn, rg.Suma(g_Img_GtAlz.GtAlz_Vcn,2,g_Img_GtAld.GtAld_Vcn,2,15,2));
                    rg.MoverA(g_Img_GtAlz.GtAlz_Vts, rg.Suma(g_Img_GtAlz.GtAlz_Vts,2,g_Img_GtAld.GtAld_Vts,2,15,2));
                  }
             }
         Vector g_Tab_Img = A_Tab_Img(data);
         img.Graba_Img_Host(g_Msg, g_Tab_Img, data, context);
         Cursatura("U", g_Tab_Img, data, context);
         g_MsgED135 = MSGED135.Inicia_MsgED135();
         rg.MoverA(g_MsgED135.ED135_Idr, "M");
         rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
         rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
         rg.MoverA(g_MsgED135.ED135_Est, "");
         rg.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
         rg.MoverA(g_MsgED135.ED135_Vlc, g_Img_GtAlz.GtAlz_Vcn);
         rg.MoverA(g_MsgED135.ED135_Ezt, "ENSCM");
         rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
         g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED535");
         data.getUser().removeTemp("Agtp170_PrmPC080");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp170", data, g_Msg);
         return;
       }
    if (Opc.equals("T"))
       {
         //Accion. Ir a pantalla ingreso Cartas Resguardo [OPERADOR:RW/CURSATURA:RO]
         g_Img_GtAlz = (GT_IMG.Buf_Img_GtAlz)data.getUser().getTemp("Agtp170_ImgGtAlz");
         String lcDcn = "CRRES"; 
         if (g_Img_GtAlz.GtAlz_Rcr.toString().equals("B"))
            { lcDcn = "CARES"; }
         if (g_Img_GtAlz.GtAlz_Rcr.toString().equals("N"))
            { lcDcn = "CAREN"; }
         if (g_Img_GtAlz.GtAlz_Rcr.toString().equals("B")
          || g_Img_GtAlz.GtAlz_Rcr.toString().equals("N"))
            { 
              generaBaseA(data, context); 
              rg.MoverA(g_Img_GtAlz.GtAlz_Rcr, "Y");
              data.getUser().removeTemp("Agtp170_ImgGtAlz");
              data.getUser().setTemp("Agtp170_ImgGtAlz", g_Img_GtAlz);
            }
         v_Img_GtAcr = (Vector)data.getUser().getTemp("Agtp170_vImgGtAcr");
         data.getUser().removeTemp("Agtp305_gTabImg");
         data.getUser().setTemp("Agtp305_gTabImg", v_Img_GtAcr);
         data.getUser().removeTemp("Agtp170_g_PrmPC080");
         data.getUser().setTemp("Agtp170_g_PrmPC080", g_PrmPC080);
         //rg.MoverA(g_PrmPC080.PC080_Rtn,     "WR");
         //rg.MoverA(g_PrmPC080.PC080_Cli,     d_MsgED520.Evt_Cli);
         //rg.MoverA(g_PrmPC080.PC080_Ncl,     d_MsgED520.Evt_Ncl);
         //rg.MoverA(g_PrmPC080.PC080_Cnr.Sis, d_MsgED520.Evt_Sis);
         //rg.MoverA(g_PrmPC080.PC080_Cnr.Ncn, d_MsgED520.Evt_Ncn);
         rg.MoverA(g_PrmPC080.PC080_Dcn,     lcDcn);
         rg.MoverA(g_PrmPC080.PC080_Ndc,     "CARTA DE RESGUARDO");
         //rg.MoverA(g_PrmPC080.PC080_Ntr,     d_MsgED520.Tmt_Nev);
         //rg.MoverA(g_PrmPC080.PC080_Ttr,     d_MsgED520.Evt_Ttr);
         //rg.MoverA(g_PrmPC080.PC080_Trt,     d_MsgED520.Tmt_Trt);
         //rg.MoverA(g_PrmPC080.PC080_Mnd,     d_MsgED520.Evt_Mnd);
         //rg.MoverA(g_PrmPC080.PC080_Trj,     d_MsgED520.Evt_Trj);
         //rg.MoverA(g_PrmPC080.PC080_Dcm,     d_MsgED520.Evt_Dcm);
         //rg.MoverA(g_PrmPC080.PC080_Tpc,     d_MsgED520.Evt_Tpp);
         //rg.MoverA(g_PrmPC080.PC080_Ggr,     d_MsgED520.Evt_Ggr);
         rg.MoverA(g_PrmPC080.PC080_Est,     "CRALZ");
         rg.MoverA(g_Msg.Msg_Dat,            PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         //------------------------------------------------------------------------------
         BF_MSG.Link_Program("Agtp170", "Agtp305", data, g_Msg);
         return;
       }       
    if (Opc.trim().equals("G"))
       {
         //Boton Cursar SIN Carta Resguardo [EJECUTIVO COMERCIAL]
         v_Img_GtAcr = (Vector)data.getUser().getTemp("Agtp170_vImgGtAcr");
         v_Img_GtAld = (Vector)data.getUser().getTemp("Agtp170_vImgGtAld");
         g_Img_GtAlz = (GT_IMG.Buf_Img_GtAlz)data.getUser().getTemp("Agtp170_ImgGtAlz");
         rg.MoverA(g_Img_GtAlz.GtAlz_Fig, g_Msg.Msg_Fch);
         rg.MoverA(g_Img_GtAlz.GtAlz_Vcn, rg.Zeros(g_Img_GtAlz.GtAlz_Vcn));
         rg.MoverA(g_Img_GtAlz.GtAlz_Vts, rg.Zeros(g_Img_GtAlz.GtAlz_Vts));
         rg.MoverA(g_Img_GtAlz.GtAlz_Rcr, data.getParameters().getString("Rcr", ""));
         for (i=0; i<v_Img_GtAld.size(); i++)
             {
               g_Img_GtAld = (GT_IMG.Buf_Img_GtAld)v_Img_GtAld.elementAt(i);
               if (g_Img_GtAld.GtAld_Est.toString().equals("ALZMT"))
                  {
                    rg.MoverA(g_Img_GtAlz.GtAlz_Vcn, rg.Suma(g_Img_GtAlz.GtAlz_Vcn,2,g_Img_GtAld.GtAld_Vcn,2,15,2));
                    rg.MoverA(g_Img_GtAlz.GtAlz_Vts, rg.Suma(g_Img_GtAlz.GtAlz_Vts,2,g_Img_GtAld.GtAld_Vts,2,15,2));
                  }
             }
         Vector g_Tab_Img = A_Tab_Img(data);
         img.Graba_Img_Host(g_Msg, g_Tab_Img, data, context);
       //Cursatura("U", g_Tab_Img, data, context);
         g_MsgED135 = MSGED135.Inicia_MsgED135();
         rg.MoverA(g_MsgED135.ED135_Idr, "M");
         rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
         rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
         rg.MoverA(g_MsgED135.ED135_Est, "");
         rg.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
         rg.MoverA(g_MsgED135.ED135_Vlc, g_Img_GtAlz.GtAlz_Vcn);
         if (g_Img_GtAlz.GtAlz_Rcr.toString().equals("B")
          || g_Img_GtAlz.GtAlz_Rcr.toString().equals("N"))
            { rg.MoverA(g_MsgED135.ED135_Ezt, "ENOPR"); }
         else
            { rg.MoverA(g_MsgED135.ED135_Ezt, "TERMI"); }
         rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
         g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
         data.getUser().removeTemp("Agtp170_PrmPC080");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OO");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp170", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("B"))
       {
         //Boton Aceptar CON Carta Resguardo [OPERADOR COMERCIAL]
         g_Img_GtAlz = (GT_IMG.Buf_Img_GtAlz)data.getUser().getTemp("Agtp170_ImgGtAlz");
         if (!g_Img_GtAlz.GtAlz_Rcr.toString().equals("I"))
            {
              data.getUser().removeTemp("Agtp170_g_PrmPC080");
              data.getUser().setTemp("Agtp170_g_PrmPC080", g_PrmPC080);
              BF_MSG.MsgInt("Agtp170", data, g_Msg, "Debe Ingresar Carta de Resguardo", "1"); 
              return;
            }
         v_Img_GtAcr = (Vector)data.getUser().getTemp("Agtp170_vImgGtAcr");
         v_Img_GtAld = (Vector)data.getUser().getTemp("Agtp170_vImgGtAld");
         Vector g_Tab_Img = A_Tab_Img(data);
         img.Graba_Img_Host(g_Msg, g_Tab_Img, data, context);
       //Cursatura("U", g_Tab_Img, data, context);
         g_MsgED135 = MSGED135.Inicia_MsgED135();
         rg.MoverA(g_MsgED135.ED135_Idr, "M");
         rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
         rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
         rg.MoverA(g_MsgED135.ED135_Est, "");
         rg.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
         rg.MoverA(g_MsgED135.ED135_Vlc, g_Img_GtAlz.GtAlz_Vcn);
         rg.MoverA(g_MsgED135.ED135_Ezt, "TERMI");
         rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
         g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
         data.getUser().removeTemp("Agtp170_PrmPC080");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OO");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp170", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("K"))
       {
         //Boton Cambiar (Alzar/Devolver)
         String Sel = data.getParameters().getString("Sel", "");
         String Seq = data.getParameters().getString("Seq", "");
         i = Integer.parseInt(Seq);
         v_Img_GtAld = (Vector)data.getUser().getTemp("Agtp170_vImgGtAld");
         g_Img_GtAlz = (GT_IMG.Buf_Img_GtAlz)data.getUser().getTemp("Agtp170_ImgGtAlz");
         g_Img_Base  = img.LSet_A_ImgBase(g_Img_GtAlz.GtAlz_Bse.toString());
         g_Img_GtAld = (GT_IMG.Buf_Img_GtAld)v_Img_GtAld.elementAt(i);
         rg.MoverA(g_Img_GtAlz.GtAlz_Rcr, data.getParameters().getString("Rcr", ""));
         if (Sel.equals("A"))
            { rg.MoverA(g_Img_GtAld.GtAld_Est, "ALZMT"); }
         else
            { rg.MoverA(g_Img_GtAld.GtAld_Est, "NUEVA"); }
         v_Img_GtAld.set(i, g_Img_GtAld);
         data.getUser().removeTemp("Agtp170_ImgGtAlz");
         data.getUser().setTemp("Agtp170_ImgGtAlz", g_Img_GtAlz);
         data.getUser().removeTemp("Agtp170_vImgGtAld");
         data.getUser().setTemp("Agtp170_vImgGtAld", v_Img_GtAld);
         setTemplate(data, "Garantias,Agt,AGTP170.vm" );
         return;
       }
  }
  //---------------------------------------------------------------------------------------
  public void Recupera_Evt(RunData data, Context context)
              throws Exception
  {
    g_MsgED100 = MSGED100.Inicia_MsgED100();
    rg.MoverA(g_MsgED100.ED100_Idr, "Q");
    rg.MoverA(g_MsgED100.ED100_Nev, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_Msg.Msg_Dat, MSGED100.LSet_De_MsgED100(g_MsgED100));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED500");
    g_MsgED100 = MSGED100.LSet_A_MsgED100(g_Msg.Msg_Dat.toString());
    g_Evt = BF_EVT.LSet_A_Evt(g_MsgED100.ED100_Evt.toString());
  }
  //---------------------------------------------------------------------------------------
  public void Carga_de_Host(RunData data, Context context)
              throws Exception
  {
    Vector g_Tab_Img = img.Carga_Img_Host(g_Msg, g_PrmPC080.PC080_Ntr.toString(), data, context);
    De_Tab_Img(g_Tab_Img, data);
    rg.MoverA(g_MsgED135.ED135_Idr, "C");
    rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
    if (g_Evt.Evt_Ggr.toString().trim().equals("CMA"))
       { rg.MoverA(g_MsgED135.ED135_Trt, "UNICO"); }
    else
       { rg.MoverA(g_MsgED135.ED135_Trt, "ALZMT"); }
    rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED535");
    g_MsgED135 = MSGED135.LSet_A_MsgED135(g_Msg.Msg_Dat.toString());
  }
  //---------------------------------------------------------------------------------------
  public void De_Tab_Img(Vector g_Tab_Img, RunData data)
  {
    g_Img       = img.Inicia_Img();
    lcData      = img.LSet_De_Img(g_Img);
    g_Img_Base  = img.LSet_A_ImgBase(lcData);
    g_Img_GtAlz = gtimg.LSet_A_ImgGtAlz(g_Img.Img_Dat.toString());
    v_Img_GtAld.clear();
    v_Img_GtAcr.clear();
    for (i=0; i<g_Tab_Img.size(); i++)
        {
          lcData = img.LSet_De_Img((BF_IMG.Buf_Img)g_Tab_Img.elementAt(i));
          g_Img  = img.LSet_A_Img(lcData);
          if (g_Img.Img_Dax.toString().trim().equals("BASE"))
             {
               g_Img_GtAlz = gtimg.LSet_A_ImgGtAlz(g_Img.Img_Dat.toString());
               g_Img_Base  = img.LSet_A_ImgBase(g_Img_GtAlz.GtAlz_Bse.toString());
             }
          if (g_Img.Img_Dax.toString().trim().equals("DTALZ"))
             {
               g_Img_GtAld = gtimg.LSet_A_ImgGtAld(g_Img.Img_Dat.toString());
               v_Img_GtAld.add (g_Img_GtAld);
             }
          if (!g_Img.Img_Dax.toString().trim().equals("BASE")
           && !g_Img.Img_Dax.toString().trim().equals("DTALZ"))
             {
               if (g_Img.Img_Dax.toString().trim().equals("BASEA"))
                  { rg.MoverA(g_Img.Img_Dax, "BASE"); }
               v_Img_GtAcr.add (g_Img);
             }
        }
  }
  //---------------------------------------------------------------------------------------
  public void Carga_Inicial(RunData data, Context context)
              throws Exception
  {
    g_Img       = img.Inicia_Img();
    lcData      = img.LSet_De_Img(g_Img);
    g_Img_Base  = img.LSet_A_ImgBase(lcData);
    g_Img_GtAlz = gtimg.LSet_A_ImgGtAlz(g_Img.Img_Dat.toString());
    rg.MoverA(g_Img_Base.Img_Base_Ttr, "ALZMT");
    rg.MoverA(g_Img_Base.Img_Base_Sis, g_PrmPC080.PC080_Cnr.Sis);
    rg.MoverA(g_Img_Base.Img_Base_Ncn, g_PrmPC080.PC080_Cnr.Ncn);
    Validar_Host("N", data, context);
    rg.MoverA(g_PrmPC080.PC080_Ntr, g_Img_Base.Img_Base_Ntr);
  }
  //---------------------------------------------------------------------------------------
  public void Validar_Host(String pcIdr, RunData data, Context context)
              throws Exception
  {
    MSGGT170.Bff_MsgGT170 d_MsgGT170 = new MSGGT170.Bff_MsgGT170();
    boolean liSwtFin = false;
    v_Img_GtAld.clear();
    v_Img_GtAcr.clear();
    g_MsgGT170 = MSGGT170.Inicia_MsgGT170();
    while (liSwtFin == false)
       {
         Valida_Host(pcIdr, data, context);
         for (i=0; i<MSGGT170.g_Max_GT170; i++)
             {
               d_MsgGT170 = (MSGGT170.Bff_MsgGT170)g_MsgGT170.GT170_Alz.elementAt(i);
               if (d_MsgGT170.GtAlz_Seq.toString().trim().equals(""))
                  {
                    liSwtFin = true;
                    break;
                  }
               GT_IMG.Buf_Img_GtAld g_Img_GtAld = new GT_IMG.Buf_Img_GtAld();
               rg.MoverA(g_Img_GtAld.GtAld_Seq, d_MsgGT170.GtAlz_Seq);
               rg.MoverA(g_Img_GtAld.GtAld_Itb, d_MsgGT170.GtAlz_Itb);
               rg.MoverA(g_Img_GtAld.GtAld_Dsb, d_MsgGT170.GtAlz_Dsb);
               rg.MoverA(g_Img_GtAld.GtAld_Vts, d_MsgGT170.GtAlz_Vts);
               rg.MoverA(g_Img_GtAld.GtAld_Vcn, d_MsgGT170.GtAlz_Vcn);
               rg.MoverA(g_Img_GtAld.GtAld_Est, d_MsgGT170.GtAlz_Est);
               rg.MoverA(g_Img_GtAld.GtAld_Fll, "");
               v_Img_GtAld.add(g_Img_GtAld);
               gcSeq = g_Img_GtAld.GtAld_Seq.toString();
             }
         g_MsgGT170 = MSGGT170.Inicia_MsgGT170();
         pcIdr = "S";
       }
  }
  //---------------------------------------------------------------------------------------
  public void Valida_Host(String pcIdr, RunData data, Context context)
              throws Exception
  {
    if (pcIdr.equals("N"))
       { rg.MoverA(g_Img_GtAlz.GtAlz_Bse, img.LSet_De_ImgBase_Bse(g_Img_Base)); }
    rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtAlz(g_Img_GtAlz));
    rg.MoverA(g_MsgGT170.GT170_Idr, pcIdr);
    rg.MoverA(g_MsgGT170.GT170_Img, g_Img.Img_Dat);
    if (pcIdr.equals("S"))
       {
         rg.MoverA(g_MsgGT170.GT170_Idt, pcIdr);
         MSGGT170.Bff_MsgGT170 d_MsgGT170 = new MSGGT170.Bff_MsgGT170();
         rg.MoverA(d_MsgGT170.GtAlz_Seq, gcSeq);
         g_MsgGT170.GT170_Alz.set(0, d_MsgGT170);
       }

    rg.MoverA(g_Msg.Msg_Dat, MSGGT170.LSet_De_MsgGT170(g_MsgGT170));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GT170");
    g_MsgGT170 = MSGGT170.LSet_A_MsgGT170(g_Msg.Msg_Dat.toString());
    rg.MoverA(g_Img.Img_Dat, g_MsgGT170.GT170_Img);
    g_Img_GtAlz = gtimg.LSet_A_ImgGtAlz(g_Img.Img_Dat.toString());
    if (pcIdr.equals("N"))
       {
         g_Img_Base  = img.LSet_A_ImgBase(g_Img_GtAlz.GtAlz_Bse.toString());
         rg.MoverA(g_Img_Base.Img_Base_Suc, g_Msg.Msg_Suc);
         rg.MoverA(g_Img_Base.Img_Base_Nsu, g_Msg.Msg_Nsu);
       //rg.MoverA(g_Img_Base.Img_Base_Mnd, "000");
       //rg.MoverA(g_Img_Base.Img_Base_Nmn, "PESOS CHILE");
         rg.MoverA(g_Img_Base.Img_Base_Eje, g_Msg.Msg_Cus);
         rg.MoverA(g_Img_Base.Img_Base_Nej, g_Msg.Msg_Nus);
         rg.MoverA(g_Img_Base.Img_Base_Tej, g_Msg.Msg_Fus);
         rg.MoverA(g_Img_GtAlz.GtAlz_Bse,   img.LSet_De_ImgBase_Bse(g_Img_Base));
       }
    else
       {
         g_Img_Base  = img.LSet_A_ImgBase(g_Img_GtAlz.GtAlz_Bse.toString());
         rg.MoverA(g_Img_GtAlz.GtAlz_Bse,   img.LSet_De_ImgBase_Bse(g_Img_Base));
       }
    rg.MoverA(g_Img_GtAlz.GtAlz_Fpr, g_Msg.Msg_Fch);
    rg.MoverA(g_Img_GtAlz.GtAlz_Fig, g_Msg.Msg_Fch);
//    fiSwtErr = False
//    If g_Msg_GT170.GT170_Idr = "R" Then
//        Call LSet_A_ErrGT170(g_Msg_GT170.GT170_Swt, g_Swt_GT170)
//        Call MsgInt(Me, MsgErr_GT170(liCancela), 1)
//        fiSwtErr = True
//        If liCancela = True Then Call Com_Salir_Click
//    End If
  }
  //---------------------------------------------------------------------------------------
  public void Init_Evt_GRT(RunData data, Context context)
              throws Exception
  {
    rg.MoverA(g_Evt.Evt_Ttr, g_Img_Base.Img_Base_Ttr);
    rg.MoverA(g_Evt.Evt_Ntt, g_Img_Base.Img_Base_Ntt);
    rg.MoverA(g_Evt.Evt_Tsl, g_Img_Base.Img_Base_Tsl);
    rg.MoverA(g_Evt.Evt_Slc, g_Img_Base.Img_Base_Slc);
    rg.MoverA(g_Evt.Evt_Cli, g_Img_Base.Img_Base_Cli);
    rg.MoverA(g_Evt.Evt_Ncl, g_Img_Base.Img_Base_Ncl);
    rg.MoverA(g_Evt.Evt_Tpr, g_Img_Base.Img_Base_Tpr);
    rg.MoverA(g_Evt.Evt_Dir, g_Img_Base.Img_Base_Dir);
    rg.MoverA(g_Evt.Evt_Cmn, " ");                    //g_Img_Base.Img_Base_Cmn
    rg.MoverA(g_Evt.Evt_Tfc, g_Img_Base.Img_Base_Tfc);
    rg.MoverA(g_Evt.Evt_Sis, g_Img_Base.Img_Base_Sis);
    rg.MoverA(g_Evt.Evt_Ncn, g_Img_Base.Img_Base_Ncn);
    rg.MoverA(g_Evt.Evt_Dcn, g_Img_Base.Img_Base_Dcn);
    rg.MoverA(g_Evt.Evt_Ndc, g_Img_Base.Img_Base_Ndc);
    rg.MoverA(g_Evt.Evt_Tmn, g_Img_Base.Img_Base_Tmn);
    rg.MoverA(g_Evt.Evt_Trj, g_Img_Base.Img_Base_Trj);
    rg.MoverA(g_Evt.Evt_Dcm, g_Img_Base.Img_Base_Dcm);
    rg.MoverA(g_Evt.Evt_Prp, g_Img_Base.Img_Base_Prp);
    rg.MoverA(g_Evt.Evt_Suc, g_Img_Base.Img_Base_Suc);
    rg.MoverA(g_Evt.Evt_Nsu, g_Img_Base.Img_Base_Nsu);
    rg.MoverA(g_Evt.Evt_Mnd, g_Img_Base.Img_Base_Mnd);
    rg.MoverA(g_Evt.Evt_Nmn, g_Img_Base.Img_Base_Nmn);
    rg.MoverA(g_Evt.Evt_Eje, g_Img_Base.Img_Base_Eje);
    rg.MoverA(g_Evt.Evt_Nej, g_Img_Base.Img_Base_Nej);
    rg.MoverA(g_Evt.Evt_Tej, g_Img_Base.Img_Base_Tej);
    rg.MoverA(g_Evt.Evt_Est, "NUEVA");
    rg.MoverA(g_Evt.Evt_Fst, g_Msg.Msg_Fch);
    rg.MoverA(g_Evt.Evt_Upr, g_Img_Base.Img_Base_Trj);
    rg.MoverA(g_Evt.Evt_Vpr, rg.Zeros(g_Evt.Evt_Vpr));
    rg.MoverA(g_Evt.Evt_Fpr, g_Msg.Msg_Fch);
    rg.MoverA(g_Evt.Evt_Spr, rg.Zeros(g_Evt.Evt_Spr));
    rg.MoverA(g_Evt.Evt_Tpp, " ");
    rg.MoverA(g_Evt.Evt_Ggr, g_Img_GtAlz.GtAlz_Ggr);
    rg.MoverA(g_Evt.Evt_Fll, " ");
  }
  //---------------------------------------------------------------------------------------
  public Vector A_Tab_Img(RunData data)
  {
    Vector v_Img = new Vector();
    v_Img.clear();
    rg.MoverA(g_Img.Img_Ntr, g_PrmPC080.PC080_Ntr);
    //--------------------------------------------------------------------------
    rg.MoverA(g_Img.Img_Dax, "BASE");
    rg.MoverA(g_Img.Img_Seq, "000");
    rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtAlz(g_Img_GtAlz));
    Vector vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
    v_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
    //--------------------------------------------------------------------------
    rg.MoverA(g_Img.Img_Seq, "000");
    for (i=0; i<v_Img_GtAld.size(); i++)
        {
          g_Img_GtAld = (GT_IMG.Buf_Img_GtAld)v_Img_GtAld.elementAt(i);
          rg.MoverA(g_Img.Img_Dax, "DTALZ");
          rg.MoverA(g_Img.Img_Seq, rg.Suma(g_Img.Img_Seq,0,"1",0,3,0));
          rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtAld(g_Img_GtAld));
          vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
          v_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
        }
    //--------------------------------------------------------------------------
    rg.MoverA(g_Img.Img_Seq, "000");
    for (i=0; i<v_Img_GtAcr.size(); i++)
        {
          g_Img = (BF_IMG.Buf_Img)v_Img_GtAcr.elementAt(i);
          rg.MoverA(g_Img.Img_Ntr, g_PrmPC080.PC080_Ntr);
          if (g_Img.Img_Dax.toString().trim().equals("BASE"))
             { rg.MoverA(g_Img.Img_Dax, "BASEA"); }
          rg.MoverA(g_Img.Img_Seq, rg.Suma(g_Img.Img_Seq,0,"1",0,3,0));
          vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
          v_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
        }
    //--------------------------------------------------------------------------
    return v_Img;
  }
  //---------------------------------------------------------------------------------------
  public void Cursatura(String pcIdr, Vector g_Tab_Img, RunData data, Context context)
              throws Exception
  {
    g_Evt = (BF_EVT.Buf_Evt)data.getUser().getTemp("Agtp170_Evt");
    rg.MoverA(g_Evt.Evt_Est, "ENTMT");
  //rg.MoverA(g_Evt.Evt_Upr, g_Img_Base.Img_Base_Trj);
  //rg.MoverA(g_Evt.Evt_Vpr, rg.Zeros(15));
    rg.MoverA(g_Evt.Evt_Fpr, g_Msg.Msg_Fch);
  //rg.MoverA(g_Evt.Evt_Spr, rg.Zeros(15));
    BF_EVT t = new BF_EVT();
    g_MsgED100 = MSGED100.Inicia_MsgED100();
    rg.MoverA(g_MsgED100.ED100_Idr, pcIdr);
    rg.MoverA(g_MsgED100.ED100_Nev, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_MsgED100.ED100_Evt, t.LSet_De_Evt(g_Evt));
    rg.MoverA(g_Msg.Msg_Dat, MSGED100.LSet_De_MsgED100(g_MsgED100));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED500");
  }
//---------------------------------------------------------------------------------------
  public void doAgtp170_Continue(RunData data, Context context)
              throws Exception
  {
  //Log.debug("AGTP170[doAgtp170_Continue.start]", "[" + data.getUser().getUserName() + "]");
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Aedp210"))
       {
         g_PrmED210 = PRMED210.LSet_A_PrmED210(g_Msg.Msg_Dat.toString());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp170_g_PrmPC080");
         data.getUser().removeTemp("Agtp170_g_PrmPC080");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP170.vm" );
         return;
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Agtp305"))
       {
         PRMPC080.PrmPC080 l_PrmPC080 = new PRMPC080.PrmPC080();
         l_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp170_g_PrmPC080");
         if (l_PrmPC080.PC080_Rtn.toString().equals("OK"))
            {
              g_Img_GtAlz = (GT_IMG.Buf_Img_GtAlz)data.getUser().getTemp("Agtp170_ImgGtAlz");
              v_Img_GtAcr = (Vector)data.getUser().getTemp("Agtp305_gTabImg");
              data.getUser().removeTemp("Agtp170_vImgGtAcr");
              data.getUser().setTemp("Agtp170_vImgGtAcr", v_Img_GtAcr);
              data.getUser().removeTemp("Agtp305_gTabImg");
              rg.MoverA(g_Img_GtAlz.GtAlz_Rcr, "I");
              data.getUser().removeTemp("Agtp170_ImgGtAlz");
              data.getUser().setTemp("Agtp170_ImgGtAlz", g_Img_GtAlz);
            }
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP170.vm" );
         return;
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Appp030"))
       {
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp170_g_PrmPC080");
         data.getUser().removeTemp("Agtp170_g_PrmPC080");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP170.vm" );
         return;
       }
  }
  //---------------------------------------------------------------------------------------
  public void generaBaseA(RunData data, Context context)
              throws Exception
  {
    BF_IMG.Buf_Img_Bse p_Img_Base = new BF_IMG.Buf_Img_Bse();
    g_Img_Base = img.LSet_A_ImgBase(g_Img_GtAlz.GtAlz_Bse.toString());
    //--------------------------------------------------------------------------
    rg.MoverA(p_Img_Base.Img_Base_Ntr, Asigna_Folio("TRN-NTR", data, context)); 
    rg.MoverA(p_Img_Base.Img_Base_Ttr, "APERT"); 
    rg.MoverA(p_Img_Base.Img_Base_Ntt, "APERTURA DE GARANTIA"); 
    rg.MoverA(p_Img_Base.Img_Base_Tsl, g_Img_Base.Img_Base_Tsl); 
    rg.MoverA(p_Img_Base.Img_Base_Slc, g_Img_Base.Img_Base_Slc); 
    rg.MoverA(p_Img_Base.Img_Base_Cmt, g_Img_Base.Img_Base_Cmt);
    rg.MoverA(p_Img_Base.Img_Base_Cli, g_Img_Base.Img_Base_Cli); 
    rg.MoverA(p_Img_Base.Img_Base_Ncl, g_Img_Base.Img_Base_Ncl); 
    rg.MoverA(p_Img_Base.Img_Base_Tpr, g_Img_Base.Img_Base_Tpr); 
    rg.MoverA(p_Img_Base.Img_Base_Dir, g_Img_Base.Img_Base_Dir); 
    rg.MoverA(p_Img_Base.Img_Base_Tfc, g_Img_Base.Img_Base_Tfc); 
    rg.MoverA(p_Img_Base.Img_Base_Sis, g_Img_Base.Img_Base_Sis); 
    rg.MoverA(p_Img_Base.Img_Base_Ncn, Asigna_Folio("GRT-NCN", data, context));
    rg.MoverA(p_Img_Base.Img_Base_Dcn, "CRRES"); 
    if (g_Img_GtAlz.GtAlz_Rcr.toString().equals("B"))
       { rg.MoverA(p_Img_Base.Img_Base_Dcn, "CARES"); }
    if (g_Img_GtAlz.GtAlz_Rcr.toString().equals("N"))
       { rg.MoverA(p_Img_Base.Img_Base_Dcn, "CAREN"); }
    rg.MoverA(p_Img_Base.Img_Base_Ndc, "CARTA DE RESGUARDO"); 
    rg.MoverA(p_Img_Base.Img_Base_Tmn, g_Img_Base.Img_Base_Tmn); 
    rg.MoverA(p_Img_Base.Img_Base_Trj, g_Img_Base.Img_Base_Trj); 
    rg.MoverA(p_Img_Base.Img_Base_Dcm, g_Img_Base.Img_Base_Dcm); 
    rg.MoverA(p_Img_Base.Img_Base_Prp, g_Img_Base.Img_Base_Prp); 
    rg.MoverA(p_Img_Base.Img_Base_Suc, g_Img_Base.Img_Base_Suc); 
    rg.MoverA(p_Img_Base.Img_Base_Nsu, g_Img_Base.Img_Base_Nsu); 
    rg.MoverA(p_Img_Base.Img_Base_Mnd, g_Img_Base.Img_Base_Mnd); 
    rg.MoverA(p_Img_Base.Img_Base_Nmn, g_Img_Base.Img_Base_Nmn); 
    rg.MoverA(p_Img_Base.Img_Base_Eje, g_Img_Base.Img_Base_Eje); 
    rg.MoverA(p_Img_Base.Img_Base_Nej, g_Img_Base.Img_Base_Nej); 
    rg.MoverA(p_Img_Base.Img_Base_Tej, g_Img_Base.Img_Base_Tej); 
    //--------------------------------------------------------------------------
    rg.MoverA(g_Img_GtApe.GtApe_Bse, img.LSet_De_ImgBase_Bse(p_Img_Base));
    rg.MoverA(g_Img_GtApe.GtApe_Fpr, g_Msg.Msg_Fch); 
    rg.MoverA(g_Img_GtApe.GtApe_Rgf, "");
    rg.MoverA(g_Img_GtApe.GtApe_Gfu, "");
    rg.MoverA(g_Img_GtApe.GtApe_Ftc, g_Msg.Msg_Fch);
    rg.MoverA(g_Img_GtApe.GtApe_Cbp, " ");
    rg.MoverA(g_Img_GtApe.GtApe_Dbp, rg.Zeros(g_Img_GtApe.GtApe_Dbp));
    rg.MoverA(g_Img_GtApe.GtApe_Tob, "E");
    rg.MoverA(g_Img_GtApe.GtApe_Plm, "N");
    rg.MoverA(g_Img_GtApe.GtApe_Ggr, "DMN");
    rg.MoverA(g_Img_GtApe.GtApe_Dsg, g_Evt.Evt_Ndc.toString().trim());
    rg.MoverA(g_Img_GtApe.GtApe_Cbt, "E");
    rg.MoverA(g_Img_GtApe.GtApe_Grd, "1");
    rg.MoverA(g_Img_GtApe.GtApe_Cmp, "N");
    rg.MoverA(g_Img_GtApe.GtApe_Sgr, "?");
    rg.MoverA(g_Img_GtApe.GtApe_Ctb, "?");
    rg.MoverA(g_Img_GtApe.GtApe_Ilm, "N");
    rg.MoverA(g_Img_GtApe.GtApe_Pcl, rg.Zeros(g_Img_GtApe.GtApe_Pcl));   
    rg.MoverA(g_Img_GtApe.GtApe_Mtl, rg.Zeros(g_Img_GtApe.GtApe_Mtl));   
    rg.MoverA(g_Img_GtApe.GtApe_Fig, g_Msg.Msg_Fch); 
    rg.MoverA(g_Img_GtApe.GtApe_Tsd, " ");
    rg.MoverA(g_Img_GtApe.GtApe_Fts, " ");
    rg.MoverA(g_Img_GtApe.GtApe_Vuo, rg.Zeros(g_Img_GtApe.GtApe_Vuo));   
    rg.MoverA(g_Img_GtApe.GtApe_Vtt, rg.Zeros(g_Img_GtApe.GtApe_Vtt));   
    rg.MoverA(g_Img_GtApe.GtApe_Vts, rg.Zeros(g_Img_GtApe.GtApe_Vts));   
    rg.MoverA(g_Img_GtApe.GtApe_Vtb, rg.Zeros(g_Img_GtApe.GtApe_Vtb));   
    rg.MoverA(g_Img_GtApe.GtApe_Vcn, rg.Zeros(g_Img_GtApe.GtApe_Vcn));   
    rg.MoverA(g_Img_GtApe.GtApe_Cbg, " ");
    rg.MoverA(g_Img_GtApe.GtApe_Nct, " ");
    rg.MoverA(g_Img_GtApe.GtApe_Fct, " ");
    rg.MoverA(g_Img_GtApe.GtApe_Fvt, " ");
    rg.MoverA(g_Img_GtApe.GtApe_Vuf, rg.Zeros(g_Img_GtApe.GtApe_Vuf));   
    rg.MoverA(g_Img_GtApe.GtApe_Vus, rg.Zeros(g_Img_GtApe.GtApe_Vus));   
    rg.MoverA(g_Img_GtApe.GtApe_Hvt, rg.Zeros(g_Img_GtApe.GtApe_Hvt));   
    rg.MoverA(g_Img_GtApe.GtApe_Hvl, rg.Zeros(g_Img_GtApe.GtApe_Hvl));   
    rg.MoverA(g_Img_GtApe.GtApe_Hvb, rg.Zeros(g_Img_GtApe.GtApe_Hvb));   
    rg.MoverA(g_Img_GtApe.GtApe_Hvc, rg.Zeros(g_Img_GtApe.GtApe_Hvc));   
    rg.MoverA(g_Img_GtApe.GtApe_Pvt, rg.Zeros(g_Img_GtApe.GtApe_Pvt));   
    rg.MoverA(g_Img_GtApe.GtApe_Pvl, rg.Zeros(g_Img_GtApe.GtApe_Pvl));   
    rg.MoverA(g_Img_GtApe.GtApe_Pvb, rg.Zeros(g_Img_GtApe.GtApe_Pvb));   
    rg.MoverA(g_Img_GtApe.GtApe_Pvc, rg.Zeros(g_Img_GtApe.GtApe_Pvc));   
    rg.MoverA(g_Img_GtApe.GtApe_Psw, " ");
    rg.MoverA(g_Img_GtApe.GtApe_Psq, " ");
    rg.MoverA(g_Img_GtApe.GtApe_Psd, " ");
    rg.MoverA(g_Img_GtApe.GtApe_Idx, " ");
    rg.MoverA(g_Img_GtApe.GtApe_Tfr, " ");
    rg.MoverA(g_Img_GtApe.GtApe_Dpv, " ");
    rg.MoverA(g_Img_GtApe.GtApe_Dpp, " ");
    rg.MoverA(g_Img_GtApe.GtApe_Dsw, " ");
    rg.MoverA(g_Img_GtApe.GtApe_Pct, " ");
    rg.MoverA(g_Img_GtApe.GtApe_Fll, " ");
    //--------------------------------------------------------------------------
    v_Img_GtAcr.clear();
    rg.MoverA(g_Img.Img_Ntr, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_Img.Img_Dax, "BASE");
    rg.MoverA(g_Img.Img_Seq, "000");
    rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtApe(g_Img_GtApe));
    v_Img_GtAcr.add (g_Img);
    //--------------------------------------------------------------------------
    data.getUser().removeTemp("Agtp170_vImgGtAcr");
    data.getUser().setTemp("Agtp170_vImgGtAcr", v_Img_GtAcr);
  }
  //---------------------------------------------------------------------------------------
  public String Asigna_Folio(String pcFolio, RunData data, Context context)
                throws Exception 
  {
    g_MsgED090 = MSGED090.Inicia_MsgED090();
    rg.MoverA(g_MsgED090.ED090_Idr, "F");
    rg.MoverA(g_MsgED090.ED090_Cod, pcFolio);
    rg.MoverA(g_Msg.Msg_Dat, MSGED090.LSet_De_MsgED090(g_MsgED090));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED090");
    g_MsgED090 = MSGED090.LSet_A_MsgED090(g_Msg.Msg_Dat.toString());
    String lcFolio = g_MsgED090.ED090_Fol.toString();
    return lcFolio;
  }
  //---------------------------------------------------------------------------------------
}