// Source File Name:   Agtp130.java
// Descripcion     :   Inicia Tramite.Fiscalia (ED125, ED135)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.BF_IMG;
import com.FHTServlet.modules.global.PRMPC080;

import com.intGlobal.modules.global.MSGED125;
import com.intGlobal.modules.global.MSGED135;
import com.intGlobal.modules.global.PRMED210;

import com.intGarantias.modules.global.GT_IMG;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp130 extends FHTServletAction
{
  //---------------------------------------------------------------------------------------
  public int i = 0;
  public int j = 0;
  public String lcData = "";
  public String Tabla = "";
  public Vector vPaso;
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  BF_IMG img                       = new BF_IMG();
  BF_IMG.Buf_Img g_Img             = new BF_IMG.Buf_Img();
  BF_IMG.Buf_Img_Bse g_Img_Base    = new BF_IMG.Buf_Img_Bse();
  GT_IMG gtimg                     = new GT_IMG();
  GT_IMG.Buf_Img_GtApe g_Img_GtApe = new GT_IMG.Buf_Img_GtApe();
  GT_IMG.Buf_Img_GtEsl g_Img_GtEsl = new GT_IMG.Buf_Img_GtEsl();
  GT_IMG.Buf_Img_GtHyp g_Img_GtHyp = new GT_IMG.Buf_Img_GtHyp();
  GT_IMG.Buf_Img_GtHpd g_Img_GtHpd = new GT_IMG.Buf_Img_GtHpd();
  Vector v_Img_GtHyp               = new Vector();
  Vector v_Img_GtHpd               = new Vector();
  PRMPC080.PrmPC080 g_PrmPC080     = new PRMPC080.PrmPC080();
  MSGED125.Buf_MsgED125 g_MsgED125 = new MSGED125.Buf_MsgED125();
  MSGED135.Buf_MsgED135 g_MsgED135 = new MSGED135.Buf_MsgED135();
  PRMED210.Buf_PrmED210 g_PrmED210 = new PRMED210.Buf_PrmED210();
  //---------------------------------------------------------------------------------------
  public Agtp130()
  {
  }
  //---------------------------------------------------------------------------------------
  public void doPerform(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP130[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp130-DIN", data);
    if (g_Msg.Msg_Pgr.toString().equals("RT"))
       { doAgtp130_Continue(data, context); }
    else
       { doAgtp130_Init(data, context); }
  }
  //---------------------------------------------------------------------------------------
  public void doCancel(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP130[doCancel.start]", "[CANCELADO]");
    setTemplate(data, "Garantias,Agt,AGTP130.vm");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp130_Init(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP130[doAgtp130_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    data.getUser().removeTemp("Agtp130_PrmPC080");
    data.getUser().setTemp("Agtp130_PrmPC080", g_PrmPC080);
    Carga_de_Host(data, context);
    Carga_Inicial(data, context);
    rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
    BF_MSG.Param_Program(data, g_Msg);
    setTemplate(data, "Garantias,Agt,AGTP130.vm" );
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp130(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP130[doAgtp130.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp130-MAN", data);
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    String Opc = data.getParameters().getString("Opc", "");
    if (Opc.trim().equals("S"))
       {
         //Boton Salir
         data.getUser().removeTemp("Agtp130_PrmPC080");
         data.getUser().removeTemp("Agtp130_ImgGtApe");
         data.getUser().removeTemp("Agtp130_ImgGtEsl");
         data.getUser().removeTemp("Agtp130_vImgGtHyp");
         data.getUser().removeTemp("Agtp130_vTabImg");
         data.getUser().removeTemp("Agtp130_MsgED125");
         data.getUser().removeTemp("Agtp130_MsgED135");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "NK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp130", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("R"))
       {
         //Boton Errores
         g_Img_GtEsl = (GT_IMG.Buf_Img_GtEsl)data.getUser().getTemp("Agtp130_ImgGtEsl");
         rg.MoverA(g_Img_GtEsl.GtEsl_Opf, data.getParameters().getString("GtEsl_Opf", ""));
         Cursar(Opc.trim(), data, context);
         g_MsgED135 = (MSGED135.Buf_MsgED135)data.getUser().getTemp("Agtp130_MsgED135");
         g_PrmED210 = PRMED210.Inicia_PrmED210();
         rg.MoverA(g_PrmED210.ED210_Rtn, "RO");
         rg.MoverA(g_PrmED210.ED210_Ldt, g_MsgED135.ED135_Ldv);
         data.getUser().removeTemp("Agtp130_g_PrmPC080");
         data.getUser().setTemp("Agtp130_g_PrmPC080" ,g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMED210.LSet_De_PrmED210(g_PrmED210));
         BF_MSG.Link_Program("Agtp130", "Aedp210", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("A") || Opc.trim().equals("B"))
       {
         //Boton Aceptar/AceptarDevuelto
         g_Img_GtEsl = (GT_IMG.Buf_Img_GtEsl)data.getUser().getTemp("Agtp130_ImgGtEsl");
         rg.MoverA(g_Img_GtEsl.GtEsl_Opf, data.getParameters().getString("GtEsl_Opf", ""));
         Cursar(Opc.trim(), data, context);
         data.getUser().removeTemp("Agtp130_PrmPC080");
         data.getUser().removeTemp("Agtp130_ImgGtApe");
         data.getUser().removeTemp("Agtp130_ImgGtEsl");
         data.getUser().removeTemp("Agtp130_vTabImg");
         data.getUser().removeTemp("Agtp130_MsgED125");
         data.getUser().removeTemp("Agtp130_MsgED135");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp130", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("C"))
       {
         //Boton Aceptar JServicios
         g_Img_GtEsl = (GT_IMG.Buf_Img_GtEsl)data.getUser().getTemp("Agtp130_ImgGtEsl");
         rg.MoverA(g_Img_GtEsl.GtEsl_Opf, data.getParameters().getString("GtEsl_Opf", ""));
         Tabla = data.getParameters().getString("Vec", "");
         Cursar(Opc.trim(), data, context);
         g_MsgED135 = MSGED135.Inicia_MsgED135();
         rg.MoverA(g_MsgED135.ED135_Idr, "M");
         rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
         rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
         rg.MoverA(g_MsgED135.ED135_Est, "");
         rg.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
         if (g_Img_GtApe.GtApe_Ggr.toString().equals("CRH"))
            { rg.MoverA(g_MsgED135.ED135_Ezt, "ENCTH"); }
         else
            { rg.MoverA(g_MsgED135.ED135_Ezt, "ENFIS"); }
         rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
         g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
         data.getUser().removeTemp("Agtp130_PrmPC080");
         data.getUser().removeTemp("Agtp130_ImgGtApe");
         data.getUser().removeTemp("Agtp130_ImgGtEsl");
         data.getUser().removeTemp("Agtp130_vImgGtHyp");
         data.getUser().removeTemp("Agtp130_vTabImg");
         data.getUser().removeTemp("Agtp130_MsgED125");
         data.getUser().removeTemp("Agtp130_MsgED135");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp130", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("D"))
       {
         //Boton Devolver JServicios
         g_PrmED210 = PRMED210.Inicia_PrmED210();
         rg.MoverA(g_PrmED210.ED210_Rtn, "RW");
         data.getUser().removeTemp("Agtp130_g_PrmPC080");
         data.getUser().setTemp("Agtp130_g_PrmPC080", g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMED210.LSet_De_PrmED210(g_PrmED210));
         BF_MSG.Link_Program("Agtp130", "Aedp210", data, g_Msg); 
         return;
       }
  }
  //---------------------------------------------------------------------------------------
  public void Carga_de_Host(RunData data, Context context)
              throws Exception 
  {
    Vector g_Tab_Img = img.Carga_Img_Host(g_Msg, g_PrmPC080.PC080_Ntr.toString(), data, context);
    De_Tab_Img(g_Tab_Img, data);
    data.getUser().removeTemp("Agtp130_ImgGtApe");
    data.getUser().setTemp("Agtp130_ImgGtApe", g_Img_GtApe);
    data.getUser().removeTemp("Agtp130_ImgGtEsl");
    data.getUser().setTemp("Agtp130_ImgGtEsl", g_Img_GtEsl);
    data.getUser().removeTemp("Agtp130_vImgGtHyp");
    data.getUser().setTemp("Agtp130_vImgGtHyp", v_Img_GtHyp);
    data.getUser().removeTemp("Agtp130_vTabImg");
    data.getUser().setTemp("Agtp130_vTabImg", g_Tab_Img);
  }
  //---------------------------------------------------------------------------------------
  public void De_Tab_Img(Vector g_Tab_Img, RunData data)
  {
    g_Img       = img.Inicia_Img();
    lcData      = img.LSet_De_Img(g_Img);
    g_Img_Base  = img.LSet_A_ImgBase(lcData);
    g_Img_GtApe = gtimg.LSet_A_ImgGtApe(g_Img.Img_Dat.toString());
    g_Img_GtEsl = gtimg.LSet_A_ImgGtEsl(g_Img.Img_Dat.toString());
    v_Img_GtHyp.clear();
    for (i=0; i<g_Tab_Img.size(); i++)
        {
          lcData = img.LSet_De_Img((BF_IMG.Buf_Img)g_Tab_Img.elementAt(i));
          g_Img  = img.LSet_A_Img(lcData);
          if (g_Img.Img_Dax.toString().trim().equals("BASE"))
             {
               g_Img_GtApe = gtimg.LSet_A_ImgGtApe(g_Img.Img_Dat.toString());
               g_Img_Base  = img.LSet_A_ImgBase(g_Img_GtApe.GtApe_Bse.toString());
             }
          if (g_Img.Img_Dax.toString().trim().equals("YESL"))
             { g_Img_GtEsl = gtimg.LSet_A_ImgGtEsl(g_Img.Img_Dat.toString()); }
          if (g_Img.Img_Dax.toString().trim().equals("XXHIP")
           || g_Img.Img_Dax.toString().trim().equals("YYPRD"))
             {
               g_Img_GtHyp = gtimg.LSet_A_ImgGtHyp(g_Img.Img_Dat.toString());
               v_Img_GtHyp.add (g_Img_GtHyp); 
             }
        }
  }
  //---------------------------------------------------------------------------------------
  public void Carga_Inicial(RunData data, Context context)
              throws Exception 
  {
    g_MsgED125 = MSGED125.Inicia_MsgED125();
    rg.MoverA(g_MsgED125.ED125_Idr, "I");
    rg.MoverA(g_MsgED125.ED125_Nev, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_MsgED125.ED125_Trt, g_PrmPC080.PC080_Trt);
    rg.MoverA(g_Msg.Msg_Dat, MSGED125.LSet_De_MsgED125(g_MsgED125));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED125");
    g_MsgED125 = MSGED125.LSet_A_MsgED125(g_Msg.Msg_Dat.toString());
    data.getUser().removeTemp("Agtp130_MsgED125");
    data.getUser().setTemp("Agtp130_MsgED125", g_MsgED125);
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    g_MsgED135 = MSGED135.Inicia_MsgED135();
    rg.MoverA(g_MsgED135.ED135_Idr, "C");
    rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
    rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
    g_MsgED135 = MSGED135.LSet_A_MsgED135(g_Msg.Msg_Dat.toString());
    data.getUser().removeTemp("Agtp130_MsgED135");
    data.getUser().setTemp("Agtp130_MsgED135", g_MsgED135);
  }
  //---------------------------------------------------------------------------------------
  public void Cursar(String codAcc, RunData data, Context context)
              throws Exception 
  {
    Vector g_Tab_Img = (Vector)data.getUser().getTemp("Agtp130_vTabImg");
    g_Tab_Img = A_Tab_Img(data, g_Tab_Img);
    g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp130_ImgGtApe");
    if (g_Img_GtApe.GtApe_Ggr.toString().equals("HYP")
     || g_Img_GtApe.GtApe_Ggr.toString().equals("CRH"))
       { Recalcula_GtApe(data, g_Tab_Img); }
    rg.MoverA(g_Img.Img_Ntr, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_Img.Img_Dax, "BASE");
    rg.MoverA(g_Img.Img_Seq, "000");
    rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtApe(g_Img_GtApe));
    Vector vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
    g_Tab_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
    img.Graba_Img_Host(g_Msg, g_Tab_Img, data, context);
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (codAcc.equals("B"))
       {
         g_MsgED135 = MSGED135.Inicia_MsgED135();
         rg.MoverA(g_MsgED135.ED135_Idr, "M");
         rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
         rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
         rg.MoverA(g_MsgED135.ED135_Est, "");
         rg.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
         rg.MoverA(g_MsgED135.ED135_Ezt, "ENJSV");
         rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
         g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
       }
  }
  //---------------------------------------------------------------------------------------
  public Vector A_Tab_Img(RunData data, Vector g_Tab_Img)
  {
    int    p     = 0;
    int    liSwt = 0;
    Vector v_Img = new Vector();
    v_Img.clear();
    //------------------------------------------------------------------------------
    v_Img_GtHyp.clear();
    v_Img_GtHpd.clear();
    for (i=0; i<g_Tab_Img.size(); i++)
        {
          lcData = img.LSet_De_Img((BF_IMG.Buf_Img)g_Tab_Img.elementAt(i));
          g_Img  = img.LSet_A_Img(lcData);
          if (g_Img.Img_Dax.toString().trim().equals("XXHIP")
           || g_Img.Img_Dax.toString().trim().equals("YYPRD"))
             {
               g_Img_GtHyp = gtimg.LSet_A_ImgGtHyp(g_Img.Img_Dat.toString());
               if (!Tabla.trim().equals(""))
                  {
                    rg.MoverA(g_Img_GtHyp.GtHyp_Psw, Tabla.substring(p,p+1));
                    p = p + 1;
                  }
               v_Img_GtHyp.add (g_Img_GtHyp); 
             }
          else if (g_Img.Img_Dax.toString().trim().equals("ZZHPD"))
             {
               g_Img_GtHpd = gtimg.LSet_A_ImgGtHpd(g_Img.Img_Dat.toString());
               v_Img_GtHpd.add (g_Img_GtHpd); 
             }
          else
             {
               if (g_Img.Img_Dax.toString().trim().equals("YESL"))
                  { 
                    liSwt = 1;
                    rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtEsl(g_Img_GtEsl));
                  }
               if (!g_Img.Img_Dax.toString().trim().equals("BASE"))
                  {
                    Vector vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
                    v_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
                  }
             }
        }
    if (liSwt == 0)
       {
         rg.MoverA(g_Img.Img_Ntr, g_PrmPC080.PC080_Ntr);
         rg.MoverA(g_Img.Img_Dax, "YESL");
         rg.MoverA(g_Img.Img_Seq, "000");
         rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtEsl(g_Img_GtEsl));
         Vector vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
         v_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
       }
    //------------------------------------------------------------------------------
    String c_Seq = "000";
    String d_Seq = "000";
    String i_Seq = "000";
    for (i=0; i<v_Img_GtHyp.size(); i++)
        {
          g_Img_GtHyp = (GT_IMG.Buf_Img_GtHyp)v_Img_GtHyp.elementAt(i);
          if (g_Img_GtHyp.GtHyp_Psw.toString().trim().equals("S"))
             {
               i_Seq = rg.Suma(i_Seq,0,"1",0,3,0);
               for (j=0; j<v_Img_GtHpd.size(); j++)
                   {
                     g_Img_GtHpd = (GT_IMG.Buf_Img_GtHpd)v_Img_GtHpd.elementAt(j);
                     if (g_Img_GtHyp.GtHyp_Seq.toString().equals(g_Img_GtHpd.GtHpd_Seq.toString()))
                        {
                          d_Seq = rg.Suma(d_Seq,0,"1",0,3,0);
                          rg.MoverA(g_Img_GtHpd.GtHpd_Seq, i_Seq);
                          rg.MoverA(g_Img.Img_Ntr, g_PrmPC080.PC080_Ntr);
                          rg.MoverA(g_Img.Img_Dax, "ZZHPD");
                          rg.MoverA(g_Img.Img_Seq, d_Seq);
                          rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtHpd(g_Img_GtHpd));
                          vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
                          v_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
                        }
                   }
               c_Seq = rg.Suma(c_Seq,0,"1",0,3,0);
               rg.MoverA(g_Img_GtHyp.GtHyp_Seq, i_Seq);
               rg.MoverA(g_Img.Img_Ntr, g_PrmPC080.PC080_Ntr);
               if (g_Img_GtHyp.GtHyp_Itb.toString().equals("HP"))
                  { rg.MoverA(g_Img.Img_Dax, "XXHIP"); }
               else
                  { rg.MoverA(g_Img.Img_Dax, "YYPRD"); }
               rg.MoverA(g_Img.Img_Seq, rg.Suma(c_Seq,0,"1",0,3,0));
               rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtHyp(g_Img_GtHyp));
               vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
               v_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
             }
        }
    //------------------------------------------------------------------------------
    return v_Img;
  }
  //---------------------------------------------------------------------------------------
  public void Recalcula_GtApe(RunData data, Vector g_Tab_Img)
  {
    String lcVtt = rg.Zeros(15);
    String lcVts = rg.Zeros(15);
    String lcVtb = rg.Zeros(15);
    String lcVcn = rg.Zeros(15);
    //------------------------------------------------------------------------------
    rg.MoverA(g_Img_GtApe.GtApe_Hvt, rg.Zeros(15));
    rg.MoverA(g_Img_GtApe.GtApe_Hvl, rg.Zeros(15));
    rg.MoverA(g_Img_GtApe.GtApe_Hvb, rg.Zeros(15));
    rg.MoverA(g_Img_GtApe.GtApe_Hvc, rg.Zeros(15));
    rg.MoverA(g_Img_GtApe.GtApe_Pvt, rg.Zeros(15));
    rg.MoverA(g_Img_GtApe.GtApe_Pvl, rg.Zeros(15));
    rg.MoverA(g_Img_GtApe.GtApe_Pvb, rg.Zeros(15));
    rg.MoverA(g_Img_GtApe.GtApe_Pvc, rg.Zeros(15));
    //------------------------------------------------------------------------------
    for (i=0; i<g_Tab_Img.size(); i++)
        {
          lcData = img.LSet_De_Img((BF_IMG.Buf_Img)g_Tab_Img.elementAt(i));
          g_Img  = img.LSet_A_Img(lcData);
          if (g_Img.Img_Dax.toString().trim().equals("XXHIP")
           || g_Img.Img_Dax.toString().trim().equals("YYPRD"))
             {
               g_Img_GtHyp = gtimg.LSet_A_ImgGtHyp(g_Img.Img_Dat.toString());
               if (g_Img_GtHyp.GtHyp_Psw.toString().trim().equals("S"))
                  {
                    lcVtt = rg.Suma(lcVtt,4,g_Img_GtHyp.GtHyp_Vts,2,15,4);
                    lcVts = rg.Suma(lcVts,2,g_Img_GtHyp.GtHyp_Vts,2,15,2);
                    lcVtb = rg.Suma(lcVtb,2,g_Img_GtHyp.GtHyp_Vtb,2,15,2);
                    lcVcn = rg.Suma(lcVcn,2,g_Img_GtHyp.GtHyp_Vcn,2,15,2);
                  }
             }
          if (g_Img.Img_Dax.toString().trim().equals("ZZHPD"))
             {
               g_Img_GtHpd = gtimg.LSet_A_ImgGtHpd(g_Img.Img_Dat.toString());
               if (g_Img_GtHpd.GtHpd_Itp.toString().equals("TE")
                || g_Img_GtHpd.GtHpd_Itp.toString().equals("ED")
                || g_Img_GtHpd.GtHpd_Itp.toString().equals("OC")
                || g_Img_GtHpd.GtHpd_Itp.toString().equals("SA"))
                   {
                     rg.MoverA(g_Img_GtApe.GtApe_Hvt, rg.Suma(g_Img_GtApe.GtApe_Hvt,2,g_Img_GtHpd.GtHpd_Vts,2,15,2));
                     rg.MoverA(g_Img_GtApe.GtApe_Hvl, rg.Suma(g_Img_GtApe.GtApe_Hvl,2,g_Img_GtHpd.GtHpd_Vlq,2,15,2));
                     rg.MoverA(g_Img_GtApe.GtApe_Hvb, rg.Suma(g_Img_GtApe.GtApe_Hvb,2,g_Img_GtHpd.GtHpd_Vtb,2,15,2));
                     rg.MoverA(g_Img_GtApe.GtApe_Hvc, rg.Suma(g_Img_GtApe.GtApe_Hvc,2,g_Img_GtHpd.GtHpd_Vcn,2,15,2));
                   }
               else
                   {
                     rg.MoverA(g_Img_GtApe.GtApe_Pvt, rg.Suma(g_Img_GtApe.GtApe_Pvt,2,g_Img_GtHpd.GtHpd_Vts,2,15,2));
                     rg.MoverA(g_Img_GtApe.GtApe_Pvl, rg.Suma(g_Img_GtApe.GtApe_Pvl,2,g_Img_GtHpd.GtHpd_Vlq,2,15,2));
                     rg.MoverA(g_Img_GtApe.GtApe_Pvb, rg.Suma(g_Img_GtApe.GtApe_Pvb,2,g_Img_GtHpd.GtHpd_Vtb,2,15,2));
                     rg.MoverA(g_Img_GtApe.GtApe_Pvc, rg.Suma(g_Img_GtApe.GtApe_Pvc,2,g_Img_GtHpd.GtHpd_Vcn,2,15,2));
                   }
             }
        }
    rg.MoverA(g_Img_GtApe.GtApe_Vtt, lcVtt);
    rg.MoverA(g_Img_GtApe.GtApe_Vts, lcVts);
    rg.MoverA(g_Img_GtApe.GtApe_Vtb, lcVtb);
    rg.MoverA(g_Img_GtApe.GtApe_Vcn, lcVcn);
  }
//---------------------------------------------------------------------------------------
  public void doAgtp130_Continue(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP130[doAgtp130_Continue.start]", "[" + data.getUser().getUserName() + "]");
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Aedp210"))
       {
         g_PrmED210 = PRMED210.LSet_A_PrmED210(g_Msg.Msg_Dat.toString());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp130_g_PrmPC080");
         data.getUser().removeTemp("Agtp130_g_PrmPC080");
         if (g_PrmED210.ED210_Rtn.toString().equals("OK"))
            {
              g_MsgED135 = MSGED135.Inicia_MsgED135();
              rg.MoverA(g_MsgED135.ED135_Idr, "X");
              rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
              rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
              rg.MoverA(g_MsgED135.ED135_Est, "");
              rg.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
              rg.MoverA(g_MsgED135.ED135_Ldv, g_PrmED210.ED210_Ldt);
              rg.MoverA(g_MsgED135.ED135_Ezt, "DVJSV");
              rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
              g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
              data.getUser().removeTemp("Agtp130_PrmPC080");
              data.getUser().removeTemp("Agtp130_ImgGtApe");
              data.getUser().removeTemp("Agtp130_ImgGtEsl");
              data.getUser().removeTemp("Agtp130_vTabImg");
              data.getUser().removeTemp("Agtp130_MsgED125");
              data.getUser().removeTemp("Agtp130_MsgED135");
              rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
              rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
              BF_MSG.Return_Data("Agtp130", data, g_Msg);
              return;
            }
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP130.vm");
         return;
       }
  }
  //---------------------------------------------------------------------------------------
}