// Source File Name:   Agtp177.java

package com.intGarantias.modules.actions;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.PRMPC080;
import com.FHTServlet.modules.global.RUTGEN;
import com.intGarantias.modules.global.MSGGT177;

public class Agtp177 extends FHTServletAction {

	public Agtp177() {
		i = 0;
		lcRetorno = "";
		rg = new RUTGEN();
		msg = new BF_MSG();
		g_Msg = new com.FHTServlet.modules.global.BF_MSG.Buf_Msg();
		g_PrmPC080 = new com.FHTServlet.modules.global.PRMPC080.PrmPC080();
		g_MsgGT177 = new com.intGarantias.modules.global.MSGGT177.Buf_MsgGT177();
	}

	public void doPerform(RunData data, Context context) throws Exception {
		g_Msg = BF_MSG.Init_Program("Agtp177-DIN", data);
		doAgtp177_Init(data, context);
	}

	public void doCancel(RunData data, Context context) throws Exception {
		setTemplate(data, "Garantias,Agt,AGTP177.vm");
	}

	public void doAgtp177_Init(RunData data, Context context) throws Exception {
		g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
		g_MsgGT177 = MSGGT177.Inicia_MsgGT177();
		RUTGEN.MoverA(g_MsgGT177.GT177_Idr, "I");
		RUTGEN.MoverA(g_MsgGT177.GT177_Cli, g_PrmPC080.PC080_Cli.toString());
		Poblar_Panel(data, context, "I");
		setTemplate(data, "Garantias,Agt,AGTP177.vm");
	}

	public void doAgtp177(RunData data, Context context) throws Exception {
		g_Msg = BF_MSG.Init_Program("Agtp177-MAN", data);
		g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
		String Opc = data.getParameters().getString("Opc");
		if (Opc.equals("S")) {
			RUTGEN.MoverA(g_PrmPC080.PC080_Rtn, "NK");
			RUTGEN.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
			data.getUser().removeTemp("Agtp177_Msg");
			data.getUser().removeTemp("Agtp177_MsgGT177");
			BF_MSG.Return_Data("Agtp177", data, g_Msg);
			return;
		}
		g_MsgGT177 = (com.intGarantias.modules.global.MSGGT177.Buf_MsgGT177) data
				.getUser().getTemp("Agtp177_MsgGT177");
		com.intGarantias.modules.global.MSGGT177.Bff_MsgGT177 d_MsgGT177 = new com.intGarantias.modules.global.MSGGT177.Bff_MsgGT177();
		if (Opc.equals("A")) {
			String Seq = data.getParameters().getString("Seq", "");
			i = Integer.parseInt(Seq);
			d_MsgGT177 = (com.intGarantias.modules.global.MSGGT177.Bff_MsgGT177) g_MsgGT177.GT177_Tab
					.elementAt(i);
			RUTGEN.MoverA(g_PrmPC080.PC080_Tgr.Gfu, d_MsgGT177.Mod_Gbs);
			RUTGEN.MoverA(g_PrmPC080.PC080_Rtn, "OK");
			RUTGEN.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
			data.getUser().removeTemp("Agtp177_Msg");
			data.getUser().removeTemp("Agtp177_MsgGT177");
			BF_MSG.Return_Data("Agtp177", data, g_Msg);
			return;
		}
		if (Opc.equals("N")) {
			Poblar_Panel(data, context, "S");
			setTemplate(data, "garantias,Agt,AGTP177.vm");
			return;
		}
		if (Opc.equals("P")) {
			Poblar_Panel(data, context, "P");
			setTemplate(data, "garantias,Agt,AGTP177.vm");
			return;
		} else {
			return;
		}
	}

	public void Poblar_Panel(RunData data, Context context, String pcIdr)
			throws Exception {
		com.intGarantias.modules.global.MSGGT177.Bff_MsgGT177 d_MsgGT177 = new com.intGarantias.modules.global.MSGGT177.Bff_MsgGT177();
		if (pcIdr.equals("E")) {
			g_MsgGT177 = (com.intGarantias.modules.global.MSGGT177.Buf_MsgGT177) data
					.getUser().getTemp("Agtp177_MsgGT177");
			i = 0;
		}
		if (pcIdr.equals("S"))
			i = Integer.parseInt(g_MsgGT177.GT177_Nro.toString()) - 1;
		if (pcIdr.equals("P"))
			i = 0;
		if (pcIdr.compareTo("I") != 0) {
			String lcCli = g_MsgGT177.GT177_Cli.toString();
			d_MsgGT177 = (com.intGarantias.modules.global.MSGGT177.Bff_MsgGT177) g_MsgGT177.GT177_Tab
					.elementAt(i);
			g_MsgGT177 = MSGGT177.Inicia_MsgGT177();
			RUTGEN.MoverA(g_MsgGT177.GT177_Idr, pcIdr);
			RUTGEN.MoverA(g_MsgGT177.GT177_Cli, lcCli);
			g_MsgGT177.GT177_Tab.set(0, d_MsgGT177);
		}
		RUTGEN.MoverA(g_Msg.Msg_Dat, MSGGT177.LSet_De_MsgGT177(g_MsgGT177));
		g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GT177");
		g_MsgGT177 = MSGGT177.LSet_A_MsgGT177(g_Msg.Msg_Dat.toString());
		data.getUser().removeTemp("Agtp177_MsgGT177");
		data.getUser().setTemp("Agtp177_MsgGT177", g_MsgGT177);
		data.getUser().removeTemp("Agtp177_Msg");
		data.getUser().setTemp("Agtp177_Msg", g_Msg);
	}

	public int i;
	public String lcRetorno;
	RUTGEN rg;
	BF_MSG msg;
	com.FHTServlet.modules.global.BF_MSG.Buf_Msg g_Msg;
	com.FHTServlet.modules.global.PRMPC080.PrmPC080 g_PrmPC080;
	com.intGarantias.modules.global.MSGGT177.Buf_MsgGT177 g_MsgGT177;
}
