// Source File Name:   Agtp725.java
// Descripcion     :   Impresion Informe Tasacion (-)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;

import java.util.*;
import org.apache.turbine.util.*;
import org.apache.velocity.context.Context;

public class Agtp725 extends FHTServletAction
{
  //---------------------------------------------------------------------------------------
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  //---------------------------------------------------------------------------------------
  public Agtp725()
  {
  }
  //---------------------------------------------------------------------------------------
  public void doPerform(RunData data, Context context)
              throws Exception
  {
  //Log.debug("AGTP725[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp725-DIN", data);
    doAgtp725_Init(data, context);
  }
  //---------------------------------------------------------------------------------------
  public void doCancel(RunData data, Context context)
              throws Exception
  {
  //Log.debug("AGTP725[doCancel.start]", "[CANCELADO]");
    setTemplate(data, "Control,AGTP725.vm");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp725_Init(RunData data, Context context)
              throws Exception
  {
  //Log.debug("AGTP725[doAgtp725_Init.start]", "[" + data.getUser().getUserName() + "]");
  //La data la genero y grabo el AGTP724
    setTemplate(data, "Garantias,Agt,AGTP725.vm" );
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp725(RunData data, Context context)
              throws Exception
  {
  //Log.debug("AGTP725[doAgtp725.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp725-MAN", data);
    String Opc = data.getParameters().getString("Opc", "");	
    if (Opc.trim().equals("S"))                    //Boton Salir
       { 
         BF_MSG.Return_Data("Agtp725", data, g_Msg);
       }
  }
  //---------------------------------------------------------------------------------------
}