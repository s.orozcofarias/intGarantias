// Source File Name:   Agtp319.java
// Descripcion:        Ingreso de Contratos y Mandatos (CL090, CL095, ED135)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.BF_LOB;
import com.FHTServlet.modules.global.BF_IMG;
import com.FHTServlet.modules.global.PRMPC080;
import com.FHTServlet.modules.util.ScreenHelper;

import com.intGlobal.modules.global.MSGED090;
import com.intGlobal.modules.global.MSGED135;
import com.intGlobal.modules.global.MSGCL090;
import com.intGlobal.modules.global.MSGCL095;
import com.intGlobal.modules.global.PRMCL090;
import com.intGlobal.modules.global.PRMED210;

import com.intGarantias.modules.global.GT_IMG;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp319 extends FHTServletAction
{
  //===============================================================================================================================
  public int    i = 0;
  public String lcData = "";
  RUTGEN rg                        = new RUTGEN();
  BF_LOB lob                       = new BF_LOB();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  PRMPC080.PrmPC080 g_PrmPC080     = new PRMPC080.PrmPC080();
  Vector v_Tab_Img                 = new Vector();
  Vector v_Img_GtCus               = new Vector();
  Vector v_Img_GtDoc               = new Vector();
  BF_IMG img                       = new BF_IMG();
  BF_IMG.Buf_Img g_Img             = new BF_IMG.Buf_Img();
  BF_IMG.Buf_Img_Bse g_Img_Base    = new BF_IMG.Buf_Img_Bse();
  GT_IMG gtimg                     = new GT_IMG();
  GT_IMG.Buf_Img_GtApe g_Img_GtApe = new GT_IMG.Buf_Img_GtApe();
  GT_IMG.Buf_Img_GtCus g_Img_GtCus = new GT_IMG.Buf_Img_GtCus();
  GT_IMG.Buf_Img_GtDoc g_Img_GtDoc = new GT_IMG.Buf_Img_GtDoc();
  MSGED090.Buf_MsgED090 g_MsgED090 = new MSGED090.Buf_MsgED090();
  MSGED135.Buf_MsgED135 g_MsgED135 = new MSGED135.Buf_MsgED135();
  MSGCL090.Buf_MsgCL090 g_MsgCL090 = new MSGCL090.Buf_MsgCL090();
  MSGCL095.Buf_MsgCL095 g_MsgCL095 = new MSGCL095.Buf_MsgCL095();
  PRMCL090.Buf_PrmCL090 g_PrmCL090 = new PRMCL090.Buf_PrmCL090();
  PRMED210.Buf_PrmED210 g_PrmED210 = new PRMED210.Buf_PrmED210();
  //-------------------------------------------------------------------------------------------
  public Agtp319()
  {
  }
  //===============================================================================================================================
  public void doPerform(RunData data, Context context) throws Exception
  {
    //Log.debug("AGTP319[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp319-DIN", data);
    if (g_Msg.getPgr().equals("RT"))
       { doAgtp319_Continue(data, context); }
    else
       { doAgtp319_Init(data, context); }
  }
  //-------------------------------------------------------------------------------------------
  public void doAgtp319_Init(RunData data, Context context) throws Exception
  {
    //Log.debug("AGTP319[doAgtp319_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.getDat());
    Carga_de_Host(data, context);
    Carga_Inicial(data, context);
    poblar_Combos(data, context);
    data.getUser().removeTemp("Agtp319_PrmPC080");
    data.getUser().setTemp("Agtp319_PrmPC080", g_PrmPC080);
    setTemplate(data, "Garantias,Agt,AGTP319.vm");
  }
  //===============================================================================================================================
  public void doAgtp319(RunData data, Context context) throws Exception
  {
    //Log.debug("AGTP319[doAgtp319.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp319-MAN", data);
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.getDat());
    String Opc = data.getParameters().getString("Opc" ,"");
    if (Opc.trim().equals("S"))
       {
         //Boton Salir
         data.getUser().removeTemp("Agtp319_MsgED135");
         data.getUser().removeTemp("Agtp319_vTabImg");
         data.getUser().removeTemp("Agtp319_vImgGtCus");
         data.getUser().removeTemp("Agtp319_vImgGtDoc");
         data.getUser().removeTemp("Agtp319_ImgBase");
         data.getUser().removeTemp("Agtp319_ImgGtApe");
         data.getUser().removeTemp("Agtp319_PrmPC080");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp319", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("C"))
       {
         //Boton OK (Cursar en Ejecutivo)
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp319_ImgGtApe");
         v_Img_GtCus = (Vector)data.getUser().getTemp("Agtp319_vImgGtCus");
         v_Img_GtDoc = (Vector)data.getUser().getTemp("Agtp319_vImgGtDoc");
         Actualiza_Datos("cuerpo", data, context);
         Vector g_Tab_Img = A_Tab_Img(data);
         img.Graba_Img_Host(g_Msg, g_Tab_Img, data, context);
         g_MsgED135 = MSGED135.Inicia_MsgED135();
         rg.MoverA(g_MsgED135.ED135_Idr, "M");
         rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
         rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
         rg.MoverA(g_MsgED135.ED135_Est, "");
         rg.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
         rg.MoverA(g_MsgED135.ED135_Vlc, g_Img_GtApe.GtApe_Vcn);
         rg.MoverA(g_MsgED135.ED135_Ezt, "TERMI");
         rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
         g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
         data.getUser().removeTemp("Agtp319_MsgED135");
         data.getUser().removeTemp("Agtp319_vTabImg");
         data.getUser().removeTemp("Agtp319_vImgGtCus");
         data.getUser().removeTemp("Agtp319_vImgGtDoc");
         data.getUser().removeTemp("Agtp319_ImgBase");
         data.getUser().removeTemp("Agtp319_ImgGtApe");
         data.getUser().removeTemp("Agtp319_PrmPC080");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp319", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("O"))
       {
         //Boton Aceptar (Ingreso en Operador)
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp319_ImgGtApe");
         v_Img_GtCus = (Vector)data.getUser().getTemp("Agtp319_vImgGtCus");
         v_Img_GtDoc = (Vector)data.getUser().getTemp("Agtp319_vImgGtDoc");
         Actualiza_Datos("cuerpo", data, context);
         Vector g_Tab_Img = A_Tab_Img(data);
         img.Graba_Img_Host(g_Msg, g_Tab_Img, data, context);
         g_MsgED135 = MSGED135.Inicia_MsgED135();
         rg.MoverA(g_MsgED135.ED135_Idr, "M");
         rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
         rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
         rg.MoverA(g_MsgED135.ED135_Est, "");
         rg.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
         rg.MoverA(g_MsgED135.ED135_Vlc, g_Img_GtApe.GtApe_Vcn);
         rg.MoverA(g_MsgED135.ED135_Ezt, "ENVSD");
         rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
         g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
         data.getUser().removeTemp("Agtp319_MsgED135");
         data.getUser().removeTemp("Agtp319_vTabImg");
         data.getUser().removeTemp("Agtp319_vImgGtCus");
         data.getUser().removeTemp("Agtp319_vImgGtDoc");
         data.getUser().removeTemp("Agtp319_ImgBase");
         data.getUser().removeTemp("Agtp319_ImgGtApe");
         data.getUser().removeTemp("Agtp319_PrmPC080");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp319", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("A"))
       {
         //Boton Agregar
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp319_ImgGtApe");
         g_Img_Base  = img.LSet_A_ImgBase(g_Img_GtApe.getBse());
         v_Img_GtCus = (Vector)data.getUser().getTemp("Agtp319_vImgGtCus");
         g_Img_GtDoc = gtimg.LSet_A_ImgGtDoc(rg.Blancos(827));
         Actualiza_Datos("cuerpo", data, context);
         rg.MoverA(g_Img_GtApe.GtApe_Psw, "A");
         rg.MoverA(g_Img_GtApe.GtApe_Idx, rg.Suma(g_Img_GtApe.GtApe_Tfr,0,"1",0,3,0));
         data.getUser().removeTemp("Agtp319_ImgGtApe");
         data.getUser().setTemp("Agtp319_ImgGtApe", g_Img_GtApe);
         data.getUser().removeTemp("Agtp319_ImgGtDoc");
         data.getUser().setTemp("Agtp319_ImgGtDoc", g_Img_GtDoc);
         data.getUser().removeTemp("Agtp319_vImgGtCus");
         data.getUser().setTemp("Agtp319_vImgGtCus", v_Img_GtCus);
         setTemplate(data, "Garantias,Agt,AGTP319.vm");
         return;
       }
    if (Opc.trim().equals("E"))
       {
         //Boton Eliminar
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp319_ImgGtApe");
         g_Img_Base  = img.LSet_A_ImgBase(g_Img_GtApe.getBse());
         v_Img_GtCus = (Vector)data.getUser().getTemp("Agtp319_vImgGtCus");
         Actualiza_Datos("cuerpo", data, context);
         data.getUser().removeTemp("Agtp319_ImgGtApe");
         data.getUser().setTemp("Agtp319_ImgGtApe", g_Img_GtApe);
         data.getUser().removeTemp("Agtp319_vImgGtCus");
         data.getUser().setTemp("Agtp319_vImgGtCus", v_Img_GtCus);
         v_Img_GtDoc = (Vector)data.getUser().getTemp("Agtp319_vImgGtDoc");
         String Seq = data.getParameters().getString("Seq", "");
         i = Integer.parseInt(Seq);
         g_Img_GtDoc = (GT_IMG.Buf_Img_GtDoc)v_Img_GtDoc.elementAt(i);
         if (g_Img_GtDoc.getIad().equals("N"))
            {
              rg.MoverA(g_Img_GtApe.GtApe_Psw, "E");
              rg.MoverA(g_Img_GtApe.GtApe_Idx, rg.Suma(Seq,0,"1",0,3,0));
              data.getUser().removeTemp("Agtp319_ImgGtApe");
              data.getUser().setTemp("Agtp319_ImgGtApe", g_Img_GtApe);
              data.getUser().removeTemp("Agtp319_ImgGtDoc");
              data.getUser().setTemp("Agtp319_ImgGtDoc", g_Img_GtDoc);
              setTemplate(data, "Garantias,Agt,AGTP319.vm");
              return;
            }
         data.getUser().removeTemp("Agtp319_ImgGtDoc");
         data.getUser().setTemp("Agtp319_ImgGtDoc", g_Img_GtDoc);
         data.getUser().removeTemp("Agtp319_g_PrmPC080");
         data.getUser().setTemp("Agtp319_g_PrmPC080", g_PrmPC080);
         rg.MoverA(g_PrmPC080.PC080_Idj, "E");
         rg.MoverA(g_PrmPC080.PC080_Cnr.Sis, g_Img_Base.getSis());
         rg.MoverA(g_PrmPC080.PC080_Cnr.Ncn, g_Img_Base.getNcn());
         rg.MoverA(g_PrmPC080.PC080_Ntc, "000");
         rg.MoverA(g_PrmPC080.PC080_Lob, g_Img_GtDoc.GtDoc_Adj);
         rg.MoverA(g_PrmPC080.PC080_Fll, g_Img_GtDoc.GtDoc_Fad);
         rg.MoverA(g_PrmPC080.PC080_Ccp.Dsg, g_Img_GtDoc.GtDoc_Dsc);
         rg.MoverA(g_PrmPC080.PC080_Ccp.Tpi, g_Img_GtDoc.GtDoc_Tpi);
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Link_Program("Agtp319", "Agtp719", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("DOC") || Opc.trim().equals("VER"))
       {
         //Imagen Digitalizar
         String Seq = data.getParameters().getString("Seq", "");
         i = Integer.parseInt(Seq);
         v_Img_GtDoc = (Vector)data.getUser().getTemp("Agtp319_vImgGtDoc");
         g_Img_GtDoc = (GT_IMG.Buf_Img_GtDoc)v_Img_GtDoc.elementAt(i);
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp319_ImgGtApe");
         g_Img_Base  = img.LSet_A_ImgBase(g_Img_GtApe.getBse());
         Actualiza_Datos("detalle", data, context);
         data.getUser().removeTemp("Agtp319_ImgGtDoc");
         data.getUser().setTemp("Agtp319_ImgGtDoc", g_Img_GtDoc);
         data.getUser().removeTemp("Agtp319_g_PrmPC080");
         data.getUser().setTemp("Agtp319_g_PrmPC080", g_PrmPC080);
         if (Opc.trim().equals("DOC"))
            { rg.MoverA(g_PrmPC080.PC080_Idj, "A"); }
         else
            { rg.MoverA(g_PrmPC080.PC080_Idj, "D"); }
         rg.MoverA(g_PrmPC080.PC080_Cnr.Sis, g_Img_Base.getSis());
         rg.MoverA(g_PrmPC080.PC080_Cnr.Ncn, g_Img_Base.getNcn());
         rg.MoverA(g_PrmPC080.PC080_Ntc, "000");
         rg.MoverA(g_PrmPC080.PC080_Lob, g_Img_GtDoc.GtDoc_Adj);
         rg.MoverA(g_PrmPC080.PC080_Fll, g_Img_GtDoc.GtDoc_Fad);
         rg.MoverA(g_PrmPC080.PC080_Ccp.Dsg, g_Img_GtDoc.GtDoc_Dsc);
         rg.MoverA(g_PrmPC080.PC080_Ccp.Tpi, g_Img_GtDoc.GtDoc_Tpi);
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Link_Program("Agtp319", "Agtp719", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("Y"))
       {
         //Boton Si
         String Seq = data.getParameters().getString("Seq", "0");
         i = Integer.parseInt(Seq);
         g_Img_GtDoc = (GT_IMG.Buf_Img_GtDoc)data.getUser().getTemp("Agtp319_ImgGtDoc");
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp319_ImgGtApe");
         v_Img_GtDoc = (Vector)data.getUser().getTemp("Agtp319_vImgGtDoc");
         Actualiza_Datos("detalle", data, context);
         if (g_Img_GtApe.getPsw().trim().equals("A"))
            { v_Img_GtDoc.add(g_Img_GtDoc); }
         else
            {
              if (g_Img_GtApe.getPsw().trim().equals("M"))
                 { v_Img_GtDoc.set(i, g_Img_GtDoc); }
              else
                 {
                   if (g_Img_GtApe.getPsw().trim().equals("E"))
                      { v_Img_GtDoc.remove(i); }
                 }
            }
         rg.MoverA(g_Img_GtApe.GtApe_Tfr, v_Img_GtDoc.size());
         Recalcula_GtApe(data, context);
         rg.MoverA(g_Img_GtApe.GtApe_Vcn, rg.FmtValor("0",0,2,15,"+"));
         rg.MoverA(g_Img_GtApe.GtApe_Idx, " ");
         rg.MoverA(g_Img_GtApe.GtApe_Psw, " ");
         data.getUser().removeTemp("Agtp319_ImgGtApe");
         data.getUser().setTemp("Agtp319_ImgGtApe", g_Img_GtApe);
         data.getUser().removeTemp("Agtp319_ImgGtDoc");
         data.getUser().removeTemp("Agtp319_vImgGtDoc");
         data.getUser().setTemp("Agtp319_vImgGtDoc", v_Img_GtDoc);
         setTemplate(data, "Garantias,Agt,AGTP319.vm");
         return;
       }
    if (Opc.trim().equals("N"))
       {
         //Boton No
         data.getUser().removeTemp("Agtp319_ImgGtDoc");
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp319_ImgGtApe");
         rg.MoverA(g_Img_GtApe.GtApe_Idx, " ");
         rg.MoverA(g_Img_GtApe.GtApe_Psw, " ");
         data.getUser().removeTemp("Agtp319_ImgGtApe");
         data.getUser().setTemp("Agtp319_ImgGtApe", g_Img_GtApe);
         setTemplate(data, "Garantias,Agt,AGTP319.vm");
         return;
       }
    if (Opc.trim().equals("R"))
       {
         //Boton Errores
         Actualiza_Datos("cuerpo", data, context);
         g_MsgED135 = (MSGED135.Buf_MsgED135)data.getUser().getTemp("Agtp319_MsgED135");
         g_PrmED210 = PRMED210.Inicia_PrmED210();
         rg.MoverA(g_PrmED210.ED210_Rtn, "RO");
         rg.MoverA(g_PrmED210.ED210_Ldt, g_MsgED135.ED135_Ldv);
         data.getUser().removeTemp("Agtp319_g_PrmPC080");
         data.getUser().setTemp("Agtp319_g_PrmPC080" ,g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMED210.LSet_De_PrmED210(g_PrmED210));
         BF_MSG.Link_Program("Agtp319", "Aedp210", data, g_Msg);
         return;
       }
  }
  //-------------------------------------------------------------------------------------------
  public void Recalcula_GtApe(RunData data, Context context) throws Exception
  {
    String lcVtt = rg.Zeros(15);
    String lcVts = rg.Zeros(15);
    String lcVtb = rg.Zeros(15);
    String lcVcn = rg.Zeros(15);
  //for (i=0; i<v_Img_GtDoc.size(); i++)
  //    {
  //      g_Img_GtDoc = (GT_IMG.Buf_Img_GtDoc)v_Img_GtDoc.elementAt(i);
  //      lcVtt = rg.Suma(lcVtt,4,"1",2,15,4);
  //      lcVts = rg.Suma(lcVts,2,"1",2,15,2);
  //      lcVtb = rg.Suma(lcVtb,2,"1",2,15,2);
  //      lcVcn = rg.Suma(lcVcn,2,"1",2,15,2);
  //    }
    rg.MoverA(g_Img_GtApe.GtApe_Vtt, lcVtt);
    rg.MoverA(g_Img_GtApe.GtApe_Vts, lcVts);
    rg.MoverA(g_Img_GtApe.GtApe_Vtb, lcVtb);
    rg.MoverA(g_Img_GtApe.GtApe_Vcn, lcVcn);
  }
  //===============================================================================================================================
  public void Actualiza_Datos(String tipo, RunData data, Context context) throws Exception
  {
    if (tipo.equals("cuerpo"))
       {
         rg.MoverA(g_Img_GtApe.GtApe_Dsg, data.getParameters().getString("GtApe_Dsg", ""));
         rg.MoverA(g_Img_GtApe.GtApe_Fts, data.getParameters().getString("GtApe_Fts", ""));
         rg.MoverA(g_Img_GtApe.GtApe_Cbt, data.getParameters().getString("GtApe_Cbt", ""));
         String lcChars = data.getParameters().getString("GtApe_Opt", "");
         if (!lcChars.trim().equals(""))
            { 
              if (g_Img_GtApe.getGfu().trim().equals("")
               || g_Img_GtApe.getGfu().trim().equals("000000000"))
                 { rg.MoverA(g_Img_GtApe.GtApe_Gfu, Asigna_Folio("FOLIO-CLOBS", data, context)); }
              lob.mantieneCLOB("M", g_Img_GtApe.getGfu().trim(), lcChars);
            }
         data.getUser().removeTemp("Agtp319_Opt");
         if (g_Img_GtApe.getGfu().trim().equals("")
          || g_Img_GtApe.getGfu().trim().equals("000000000"))
            { data.getUser().setTemp("Agtp319_Opt", ""); }
         else
            { data.getUser().setTemp("Agtp319_Opt", lob.leeCLOB(g_Img_GtApe.getGfu().trim())); }
         String tabSel = data.getParameters().getString("tabSel", "");
         for (int p=0; p<v_Img_GtCus.size(); p++)
            {
              g_Img_GtCus = (GT_IMG.Buf_Img_GtCus)v_Img_GtCus.elementAt(p);
              if (tabSel.charAt(p)=='S')
                 { rg.MoverA(g_Img_GtCus.GtCus_Chk, "S"); }
              else
                 { rg.MoverA(g_Img_GtCus.GtCus_Chk, "N"); }
              v_Img_GtCus.set(p, g_Img_GtCus);
            }
       }
    if (tipo.equals("detalle"))
       {
         rg.MoverA(g_Img_GtDoc.GtDoc_Seq, data.getParameters().getString("GtDoc_Seq", ""));
         rg.MoverA(g_Img_GtDoc.GtDoc_Iad, data.getParameters().getString("GtDoc_Iad", ""));
         rg.MoverA(g_Img_GtDoc.GtDoc_Dsc, data.getParameters().getString("GtDoc_Dsc", ""));
         rg.MoverA(g_Img_GtDoc.GtDoc_Adj, data.getParameters().getString("GtDoc_Adj", ""));
         rg.MoverA(g_Img_GtDoc.GtDoc_Fad, data.getParameters().getString("GtDoc_Fad", ""));
         rg.MoverA(g_Img_GtDoc.GtDoc_Tpi, data.getParameters().getString("GtDoc_Tpi", ""));
         rg.MoverA(g_Img_GtDoc.GtDoc_Fll, "");
       }
  }
  //-------------------------------------------------------------------------------------------
  public String Asigna_Folio(String pcFolio, RunData data, Context context) throws Exception 
  {
    g_MsgED090 = MSGED090.Inicia_MsgED090();
    rg.MoverA(g_MsgED090.ED090_Idr, "F");
    rg.MoverA(g_MsgED090.ED090_Cod, pcFolio);
    rg.MoverA(g_Msg.Msg_Dat, MSGED090.LSet_De_MsgED090(g_MsgED090));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED090");
    g_MsgED090 = MSGED090.LSet_A_MsgED090(g_Msg.Msg_Dat.toString());
    String lcFolio = rg.FmtValor(g_MsgED090.ED090_Fol.toString(),0,0,9,"");
    return lcFolio;
  }
  //===============================================================================================================================
  public void Carga_de_Host(RunData data, Context context) throws Exception
  {
    v_Tab_Img = img.Carga_Img_Host(g_Msg, g_PrmPC080.getNtr(), data, context);
    De_Tab_Img(v_Tab_Img, data);
    if (g_Img_GtApe.getTfr().trim().equals(""))
       { rg.MoverA(g_Img_GtApe.GtApe_Tfr, rg.Zeros(3)); }
    if (g_Img_GtApe.getFig().trim().equals(""))
       {
         rg.MoverA(g_Img_GtApe.GtApe_Fig, g_Msg.Msg_Fch);
         rg.MoverA(g_Img_GtApe.GtApe_Vuo, rg.Zeros(g_Img_GtApe.GtApe_Vuo));
         rg.MoverA(g_Img_GtApe.GtApe_Fts, g_Msg.Msg_Fch);
       }
    if (g_Img_GtApe.getVtt().trim().equals(""))
       {
         rg.MoverA(g_Img_GtApe.GtApe_Vtt, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Vts, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Vtb, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Vcn, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Hvt, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Hvl, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Hvb, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Hvc, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Pvt, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Pvl, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Pvb, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Pvc, rg.Zeros(15));
       }
    data.getUser().removeTemp("Agtp319_ImgBase");
    data.getUser().setTemp("Agtp319_ImgBase", g_Img_Base);
    data.getUser().removeTemp("Agtp319_ImgGtApe");
    data.getUser().setTemp("Agtp319_ImgGtApe", g_Img_GtApe);
    data.getUser().removeTemp("Agtp319_vImgGtCus");
    data.getUser().setTemp("Agtp319_vImgGtCus", v_Img_GtCus);
    data.getUser().removeTemp("Agtp319_vImgGtDoc");
    data.getUser().setTemp("Agtp319_vImgGtDoc", v_Img_GtDoc);
    data.getUser().removeTemp("Agtp319_vTabImg");
    data.getUser().setTemp("Agtp319_vTabImg", v_Tab_Img);
    data.getUser().removeTemp("Agtp319_Opt");
    if (g_Img_GtApe.getGfu().trim().equals("")
     || g_Img_GtApe.getGfu().trim().equals("000000000"))
       { data.getUser().setTemp("Agtp319_Opt", ""); }
    else
       { data.getUser().setTemp("Agtp319_Opt", lob.leeCLOB(g_Img_GtApe.getGfu().trim())); }
  }
  //-------------------------------------------------------------------------------------------
  public void De_Tab_Img(Vector v_Tab_Img, RunData data)
  {
    g_Img       = img.Inicia_Img();
    lcData      = img.LSet_De_Img(g_Img);
    g_Img_Base  = img.LSet_A_ImgBase(lcData);
    g_Img_GtApe = gtimg.LSet_A_ImgGtApe(g_Img.getDat());
    v_Img_GtCus.clear();
    v_Img_GtDoc.clear();
    for (i=0; i<v_Tab_Img.size(); i++)
        {
          lcData = img.LSet_De_Img((BF_IMG.Buf_Img)v_Tab_Img.elementAt(i));
          g_Img  = img.LSet_A_Img(lcData);
          if (g_Img.getDax().trim().equals("BASE"))
             {
               g_Img_GtApe = gtimg.LSet_A_ImgGtApe(g_Img.getDat());
               g_Img_Base  = img.LSet_A_ImgBase(g_Img_GtApe.getBse());
             }
          if (g_Img.getDax().trim().equals("ZZCUS"))
             {
               g_Img_GtCus = gtimg.LSet_A_ImgGtCus(g_Img.getDat());
               v_Img_GtCus.add (g_Img_GtCus);
             }
          if (g_Img.getDax().trim().equals("ZZDOC"))
             {
               g_Img_GtDoc = gtimg.LSet_A_ImgGtDoc(g_Img.getDat());
               v_Img_GtDoc.add (g_Img_GtDoc);
             }
        }
  }
  //===============================================================================================================================
  public void Carga_Inicial(RunData data, Context context) throws Exception
  {
    g_MsgED135 = MSGED135.Inicia_MsgED135();
    rg.MoverA(g_MsgED135.ED135_Idr, "C");
    rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
    rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
    g_MsgED135 = MSGED135.LSet_A_MsgED135(g_Msg.getDat());
    data.getUser().removeTemp("Agtp319_MsgED135");
    data.getUser().setTemp("Agtp319_MsgED135", g_MsgED135);
    //--------------------------------------------------------------
    if (v_Img_GtCus.size()==0)
       {
         Vector datosCUS = ScreenHelper.Consulta_SubMargenes(g_PrmPC080.getCli());
         v_Img_GtCus.clear();
         for (i=0; i<datosCUS.size(); i++)
            {
              ScreenHelper.Bff_Cus tCus = new ScreenHelper.Bff_Cus();
              tCus = (ScreenHelper.Bff_Cus)datosCUS.elementAt(i);
              GT_IMG.Buf_Img_GtCus p_Img_GtCus = new GT_IMG.Buf_Img_GtCus();
              rg.MoverA(p_Img_GtCus.GtCus_Num, tCus.getNum());
              rg.MoverA(p_Img_GtCus.GtCus_Smu, tCus.getSmu());
              rg.MoverA(p_Img_GtCus.GtCus_Csm, tCus.getCsm());
              rg.MoverA(p_Img_GtCus.GtCus_Dsc, tCus.getDsc());
              rg.MoverA(p_Img_GtCus.GtCus_Mnd, tCus.getMnd());
              rg.MoverA(p_Img_GtCus.GtCus_Mto, tCus.getMto());
              rg.MoverA(p_Img_GtCus.GtCus_Pre, tCus.getPre());
              rg.MoverA(p_Img_GtCus.GtCus_Chk, "N");
              rg.MoverA(p_Img_GtCus.GtCus_Fll, "");
              v_Img_GtCus.add (p_Img_GtCus);
            }
         data.getUser().removeTemp("Agtp319_vImgGtCus");
         data.getUser().setTemp("Agtp319_vImgGtCus", v_Img_GtCus);
       }
  }
  //-------------------------------------------------------------------------------------------
  public Vector A_Tab_Img(RunData data)
  {
    v_Tab_Img = (Vector)data.getUser().getTemp("Agtp319_vTabImg");
    Vector v_Img = new Vector();
    v_Img.clear();
    rg.MoverA(g_Img.Img_Ntr, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_Img.Img_Dax, "BASE");
    rg.MoverA(g_Img.Img_Seq, "000");
    rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtApe(g_Img_GtApe));
    Vector vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
    v_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
    for (i=0; i<v_Img_GtCus.size(); i++)
        {
          g_Img_GtCus = (GT_IMG.Buf_Img_GtCus)v_Img_GtCus.elementAt(i);
          rg.MoverA(g_Img.Img_Dax, "ZZCUS");
          rg.MoverA(g_Img.Img_Seq, rg.Suma(g_Img.Img_Seq,0,"1",0,3,0));
          rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtCus(g_Img_GtCus));
          vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
          v_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
        }
    for (i=0; i<v_Img_GtDoc.size(); i++)
        {
          g_Img_GtDoc = (GT_IMG.Buf_Img_GtDoc)v_Img_GtDoc.elementAt(i);
          rg.MoverA(g_Img.Img_Dax, "ZZDOC");
          rg.MoverA(g_Img.Img_Seq, rg.Suma(g_Img.Img_Seq,0,"1",0,3,0));
          rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtDoc(g_Img_GtDoc));
          vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
          v_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
        }
    //--------------------------------------------------------------
    rg.MoverA(g_Img.Img_Seq, "000");
    for (i=0; i<v_Tab_Img.size(); i++)
        {
          lcData = img.LSet_De_Img((BF_IMG.Buf_Img)v_Tab_Img.elementAt(i));
          g_Img  = img.LSet_A_Img(lcData);
          if (g_Img.getDax().trim().equals("BASE")
           || g_Img.getDax().trim().equals("ZZCUS")
           || g_Img.getDax().trim().equals("ZZDOC"))
             { }
          else
             {
               vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
               v_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
             }
        }
    //--------------------------------------------------------------
    return v_Img;
  }
  //===============================================================================================================================
  public void doAgtp319_Continue(RunData data, Context context) throws Exception
  {
    //Log.debug("AGTP319[doAgtp319_Continue.start]", "[" + data.getUser().getUserName() + "]");
    if (rg.FH_RTrim(g_Msg.getPgl()).equals("Agtp719"))
       {
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp319_g_PrmPC080");
         data.getUser().removeTemp("Agtp319_g_PrmPC080");
         //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
         PRMPC080.PrmPC080 l_PrmPC080 = new PRMPC080.PrmPC080();
         l_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.getDat());
         if (l_PrmPC080.getRtn().trim().equals("DG"))
            {
              g_Img_GtDoc = (GT_IMG.Buf_Img_GtDoc)data.getUser().getTemp("Agtp319_ImgGtDoc");
              Log.debug("Agtp319.doAgtp319_Continue:[l_PrmPC080.PC080_Lob....]["+l_PrmPC080.PC080_Lob+"]");
              Log.debug("Agtp319.doAgtp319_Continue:[l_PrmPC080.PC080_Fll....]["+l_PrmPC080.PC080_Fll+"]");
              Log.debug("Agtp319.doAgtp319_Continue:[l_PrmPC080.PC080_Ccp.Dsg]["+l_PrmPC080.PC080_Ccp.Dsg+"]");
              Log.debug("Agtp319.doAgtp319_Continue:[l_PrmPC080.PC080_Ccp.Tpi]["+l_PrmPC080.PC080_Ccp.Tpi+"]");
              rg.MoverA(g_Img_GtDoc.GtDoc_Iad, "S");
              rg.MoverA(g_Img_GtDoc.GtDoc_Adj, l_PrmPC080.PC080_Lob);
              rg.MoverA(g_Img_GtDoc.GtDoc_Fad, l_PrmPC080.PC080_Fll);
              rg.MoverA(g_Img_GtDoc.GtDoc_Dsc, l_PrmPC080.PC080_Ccp.Dsg);
              rg.MoverA(g_Img_GtDoc.GtDoc_Tpi, l_PrmPC080.PC080_Ccp.Tpi);
              data.getUser().removeTemp("Agtp319_ImgGtDoc");
              data.getUser().setTemp("Agtp319_ImgGtDoc", g_Img_GtDoc);
              //++++++++++++++++++++++++++++++++++++++++++++++++++++
              g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp319_ImgGtApe");
              v_Img_GtDoc = (Vector)data.getUser().getTemp("Agtp319_vImgGtDoc");
              i = g_Img_GtDoc.igetSeq() - 1;
              Log.debug("Agtp319.doAgtp319_Continue:[i.......................]["+i+"]");
              v_Img_GtDoc.set(i, g_Img_GtDoc);
              Vector g_Tab_Img = A_Tab_Img(data);
              img.Graba_Img_Host(g_Msg, g_Tab_Img, data, context);
            }
         if (l_PrmPC080.getRtn().trim().equals("EL"))
            {
              g_Img_GtDoc = (GT_IMG.Buf_Img_GtDoc)data.getUser().getTemp("Agtp319_ImgGtDoc");
              //++++++++++++++++++++++++++++++++++++++++++++++++++++
              g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp319_ImgGtApe");
              v_Img_GtDoc = (Vector)data.getUser().getTemp("Agtp319_vImgGtDoc");
              i = g_Img_GtDoc.igetSeq() - 1;
              Log.debug("Agtp319.doAgtp319_Continue:[i.......................]["+i+"]");
              v_Img_GtDoc.remove(i);
              Vector g_Tab_Img = A_Tab_Img(data);
              img.Graba_Img_Host(g_Msg, g_Tab_Img, data, context);
            }
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP319.vm");
         return;
       }
    if (rg.FH_RTrim(g_Msg.getPgl()).equals("Appp030"))
       {
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp319_g_PrmPC080");
         data.getUser().removeTemp("Agtp319_g_PrmPC080");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP319.vm");
         return;
       }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (RUTGEN.FH_RTrim(g_Msg.getPgl()).equals("Aedp210"))
       {
         g_PrmED210 = PRMED210.LSet_A_PrmED210(g_Msg.getDat());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp319_g_PrmPC080");
         data.getUser().removeTemp("Agtp319_g_PrmPC080");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP319.vm");
         return;
       }
  }
  //===============================================================================================================================
  private void poblar_Combos(RunData data, Context context) throws Exception
  {
  //Vector datosCUS = ScreenHelper.Consulta_SubMargenes(g_PrmPC080.getCli());
  //data.getUser().removeTemp("Agtp319_vSubMgn");
  //data.getUser().setTemp("Agtp319_vSubMgn", datosCUS);
  }
  //===============================================================================================================================
}