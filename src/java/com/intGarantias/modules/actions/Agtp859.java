// Source File Name:   Agtp859.java
// Descripcion     :   Men� Consultas [Fte: GCO] (-)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.PRMPC080;

import com.intGlobal.modules.global.PRMCL090;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp859 extends FHTServletAction
{
  //---------------------------------------------------------------------------------------
  public String lcData = "";
  public Vector vDatos;
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  PRMPC080.PrmPC080 g_PrmPC080     = new PRMPC080.PrmPC080();
  PRMCL090.Buf_PrmCL090 g_PrmCL090 = new PRMCL090.Buf_PrmCL090();
  //---------------------------------------------------------------------------------------
  public Agtp859()
  {
  }
  //---------------------------------------------------------------------------------------
  public void doPerform(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP859[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp859-DIN", data);
    if (g_Msg.Msg_Pgr.toString().equals("RT"))
       { doAgtp859_Continue(data, context); }
    else
       { doAgtp859_Init(data, context); }
  }
  //---------------------------------------------------------------------------------------
  public void doCancel(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP859[doCancel.start]", "[CANCELADO]");
    setTemplate(data, "Garantias,Agt,AGTP859.vm");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp859_Init(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP859[doAgtp859_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp859-DIN", data);
    Nueva_PrmPC080();
    BF_MSG.Param_Program(data, g_Msg);
    setTemplate(data, "Garantias,Agt,AGTP859.vm" );
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp859(RunData data, Context context)
              throws Exception
  {
    g_Msg = BF_MSG.InitIntegra("Agtp859", data, context); 
    Nueva_PrmPC080();
    BF_MSG.Param_Program(data, g_Msg);
    String Opc = data.getParameters().getString("Opc", "");
    String Cli = data.getParameters().getString("Cli", "");
    String Ncn = data.getParameters().getString("Ncn", "");
    if (Opc.trim().equals("S"))
       {
         //Boton Salir
         BF_MSG.Return_Program(data, g_Msg);
       	 return;
       }
    if (Opc.equals("X"))
       { 
         //Opcion por definir; Esta como ejemplo de salto directo
       	 BF_MSG.Link_Program("Agtp859", "Xyyyzzz", data, g_Msg);
       	 return;
       }
    if (Opc.equals("4"))
       { 
         //Garantias en Tramitacion
         rg.MoverA(g_PrmPC080.PC080_Cnr.Sis, "GAR");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
       	 BF_MSG.Link_Program("Agtp859", "Aedp102", data, g_Msg);
       	 return;
       }
    if (Opc.equals("5"))
       {
         //Contratos y Mandatos en Tramitaci�n
         rg.MoverA(g_PrmPC080.PC080_Cnr.Sis, "MYC");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Link_Program("Agtp859", "Aedp102", data, g_Msg);
         return;
       }
    if (Ncn.trim().equals(""))
       {
         //Opciones que requieren RUT si no ingresan el Numero de Operacion
         rg.MoverA(g_PrmPC080.PC080_Opc, Opc);
         if (!Cli.trim().equals(""))
            {
              rg.MoverA(g_PrmPC080.PC080_Cli, Cli);
              if (g_PrmPC080.PC080_Opc.toString().trim().equals("1"))
                 { rg.MoverA(g_PrmPC080.PC080_Acc, "CONSULTAS"); }
              else
                 { rg.MoverA(g_PrmPC080.PC080_Acc, "SELECCIONA"); }
              rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
              BF_MSG.Link_Program("Agtp859", "Agtp400", data, g_Msg);
              return;
            }
         data.getUser().removeTemp("Agtp859_g_PrmPC080");
         data.getUser().setTemp("Agtp859_g_PrmPC080" ,PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         g_PrmCL090 = PRMCL090.Inicia_PrmCL090();
         rg.MoverA(g_PrmCL090.CL090_Pcc, "N");
         rg.MoverA(g_Msg.Msg_Dat, PRMCL090.LSet_De_PrmCL090(g_PrmCL090));
         BF_MSG.Link_Program("Agtp859", "Eclp090", data, g_Msg);
         return;
       }
    else
       { 
         rg.MoverA(g_PrmPC080.PC080_Cnr.Ncn, Ncn);
         doAgtp859_Despacha(data, context, Opc);
       }
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp859_Continue(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP859[doAgtp859_Continue.start]", "[" + data.getUser().getUserName() + "]");
    if (RUTGEN.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Eclp090"))
       {
         PRMPC080 vPrmPC080 = new PRMPC080();
         vDatos = vPrmPC080.LSet_A_vPrmPC080((String)data.getUser().getTemp("Agtp859_g_PrmPC080"));
         g_PrmPC080 = (PRMPC080.PrmPC080)vDatos.elementAt(0);
         data.getUser().removeTemp("Agtp859_g_PrmPC080");
         g_PrmCL090 = PRMCL090.LSet_A_PrmCL090(g_Msg.Msg_Dat.toString());
         if (g_PrmCL090.CL090_Cli.toString().trim().equals(""))
            {
              doAgtp859_Init(data, context);
              return;
            }
         rg.MoverA(g_PrmPC080.PC080_Cli, g_PrmCL090.CL090_Cli);
         rg.MoverA(g_PrmPC080.PC080_Ncl, g_PrmCL090.CL090_Ncl);
         rg.MoverA(g_PrmPC080.PC080_Tpr, g_PrmCL090.CL090_Tpr);
         if (g_PrmPC080.PC080_Opc.toString().trim().equals("1"))
            { rg.MoverA(g_PrmPC080.PC080_Acc, "CONSULTAS"); }
         else
            { rg.MoverA(g_PrmPC080.PC080_Acc, "SELECCIONA"); }
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Link_Program("Agtp859", "Agtp400", data, g_Msg);
         return;
       }
    if (RUTGEN.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Agtp400"))
       {
         g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
         if (g_PrmPC080.PC080_Rtn.toString().trim().equals("NK"))
            { doAgtp859_Init(data, context); }
         else
            { doAgtp859_Despacha(data, context, g_PrmPC080.PC080_Opc.toString().trim()); }
         return;
       }
    doAgtp859_Init(data, context);
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp859_Despacha(RunData data, Context context, String Opc)
              throws Exception
  {
    rg.MoverA(g_PrmPC080.PC080_Rtn, " ");
    String lcPrograma = "";
    if (Opc.equals("2"))
       {
         //Detalle de Garant�a Espec�fica
         lcPrograma = "Agtp451";
       }
    if (Opc.equals("3"))
       {
         //Cartola Garant�a Espec�fica
         lcPrograma = "Aicp430";
       }
    Log.debug("AGTP859[doAgtp859_Despacha.Opc]", "[" + Opc + "]");
	Log.debug("AGTP859[doAgtp859_Despacha.lcPrograma]", "[" + lcPrograma + "]");
	rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
    BF_MSG.Link_Program("Agtp859", lcPrograma, data, g_Msg);
  }
  //---------------------------------------------------------------------------------------
  public void Nueva_PrmPC080() 
  {
    g_PrmPC080 = PRMPC080.Inicia_PrmPC080();
    RUTGEN.MoverA(g_PrmPC080.PC080_Fte,     "GCO");
    RUTGEN.MoverA(g_PrmPC080.PC080_Pai,     "CL");
    RUTGEN.MoverA(g_PrmPC080.PC080_Cnr.Sis, "GAR");
    RUTGEN.MoverA(g_PrmPC080.PC080_Slr.Slc, RUTGEN.Zeros(7));
    RUTGEN.MoverA(g_PrmPC080.PC080_Suc,     g_Msg.Msg_Suc);
    RUTGEN.MoverA(g_PrmPC080.PC080_Nsu,     g_Msg.Msg_Nsu);
    RUTGEN.MoverA(g_PrmPC080.PC080_Mnd,     "000");
    RUTGEN.MoverA(g_PrmPC080.PC080_Nmn,     "PESOS CHILE");
    RUTGEN.MoverA(g_PrmPC080.PC080_Eje,     g_Msg.Msg_Cus);
    RUTGEN.MoverA(g_PrmPC080.PC080_Nej,     g_Msg.Msg_Nus);
    RUTGEN.MoverA(g_PrmPC080.PC080_Tej,     g_Msg.Msg_Fus);
    RUTGEN.MoverA(g_Msg.Msg_Dat,            PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
  }
  //---------------------------------------------------------------------------------------
}