// Source File Name:   Agtp729.java

package com.intGarantias.modules.actions;

import java.io.InputStream;
import java.io.OutputStream;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import oracle.jdbc.OracleConnection;
import oracle.jdbc.OraclePreparedStatement;
import oracle.jdbc.OracleStatement;
import oracle.sql.BLOB;

import org.apache.turbine.util.Log;
import org.apache.turbine.util.RunData;
import org.apache.turbine.util.upload.FileItem;
import org.apache.velocity.context.Context;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.PRMPC080;
import com.FHTServlet.modules.global.RUTGEN;
import com.intGarantias.modules.global.MSGGT459;
import com.intGlobal.modules.global.MSGED090;

public class Agtp729 extends FHTServletAction {

	public Agtp729() {
		i = 0;
		lcData = "";
		rg = new RUTGEN();
		msg = new BF_MSG();
		g_Msg = new com.FHTServlet.modules.global.BF_MSG.Buf_Msg();
		g_PrmPC080 = new com.FHTServlet.modules.global.PRMPC080.PrmPC080();
		g_MsgED090 = new com.intGlobal.modules.global.MSGED090.Buf_MsgED090();
		g_MsgGT459 = new com.intGarantias.modules.global.MSGGT459.Buf_MsgGT459();
	}

	public void doPerform(RunData data, Context context) throws Exception {
		g_Msg = BF_MSG.Init_Program("Agtp729-DIN", data);
		doAgtp729_Init(data, context);
	}

	public void doCancel(RunData data, Context context) throws Exception {
		setTemplate(data, "Garantias,Agt,AGTP729.vm");
	}

	public void doAgtp729_Init(RunData data, Context context) throws Exception {
		g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
		g_MsgGT459 = MSGGT459.Inicia_MsgGT459();
		RUTGEN.MoverA(g_MsgGT459.GT459_Idr, "I");
		RUTGEN.MoverA(g_MsgGT459.GT459_Sis, g_PrmPC080.PC080_Cnr.Sis);
		RUTGEN.MoverA(g_MsgGT459.GT459_Ncn, g_PrmPC080.PC080_Cnr.Ncn);
		RUTGEN.MoverA(g_MsgGT459.GT459_Seq, g_PrmPC080.PC080_Ntc);
		RUTGEN.MoverA(g_Msg.Msg_Dat, MSGGT459.LSet_De_MsgGT459(g_MsgGT459));
		g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GT459");
		g_MsgGT459 = MSGGT459.LSet_A_MsgGT459(g_Msg.Msg_Dat.toString());
		data.getUser().removeTemp("Agtp729_MsgGT459");
		data.getUser().setTemp("Agtp729_MsgGT459", g_MsgGT459);
		data.getUser().removeTemp("Agtp729_PrmPC080");
		data.getUser().setTemp("Agtp729_PrmPC080", g_PrmPC080);
		setTemplate(data, "Garantias,Agt,AGTP729.vm");
	}

	public void doAgtp729(RunData data, Context context) throws Exception {
		g_Msg = BF_MSG.Init_Program("Agtp729-MAN", data);
		g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
		String Opc = data.getParameters().getString("Opc", "");
		if (Opc.trim().equals("T")) {
			String Seq = data.getParameters().getString("Seq", "");
			i = Integer.parseInt(Seq);
			g_MsgGT459 = (com.intGarantias.modules.global.MSGGT459.Buf_MsgGT459) data
					.getUser().getTemp("Agtp729_MsgGT459");
			RUTGEN.MoverA(g_MsgGT459.GT459_Psw, "");
			RUTGEN.MoverA(g_MsgGT459.GT459_Idx, "");
			com.intGarantias.modules.global.MSGGT459.Bff_MsgGT459 d_MsgGT459 = new com.intGarantias.modules.global.MSGGT459.Bff_MsgGT459();
			d_MsgGT459 = (com.intGarantias.modules.global.MSGGT459.Bff_MsgGT459) g_MsgGT459.GT459_Tab
					.elementAt(i);
			RUTGEN.MoverA(g_MsgGT459.GT459_Fol, d_MsgGT459.Gif_Fol);
			RUTGEN.MoverA(g_MsgGT459.GT459_Dsg, d_MsgGT459.Gif_Dsg);
			RUTGEN.MoverA(g_MsgGT459.GT459_Fch, d_MsgGT459.Gif_Fch);
			RUTGEN.MoverA(g_MsgGT459.GT459_Tpi, d_MsgGT459.Gif_Tpi);
			data.getUser().removeTemp("Agtp729_MsgGT459");
			data.getUser().setTemp("Agtp729_MsgGT459", g_MsgGT459);
			setTemplate(data, "Garantias,Agt,AGTP729.vm");
			return;
		}
		if (Opc.trim().equals("G")) {
			g_MsgGT459 = (com.intGarantias.modules.global.MSGGT459.Buf_MsgGT459) data
					.getUser().getTemp("Agtp729_MsgGT459");
			int Idx = g_MsgGT459.GT459_Tab.size() + 1;
			RUTGEN.MoverA(g_MsgGT459.GT459_Psw, "A");
			RUTGEN.MoverA(g_MsgGT459.GT459_Idx, RUTGEN.FormatNum(Idx, "000"));
			RUTGEN.MoverA(g_MsgGT459.GT459_Fol,
					Asigna_Folio("GIF-FOL", data, context));
			RUTGEN.MoverA(g_MsgGT459.GT459_Dsg, "");
			RUTGEN.MoverA(g_MsgGT459.GT459_Fch, g_Msg.Msg_Fch);
			RUTGEN.MoverA(g_MsgGT459.GT459_Tpi, "");
			data.getUser().removeTemp("Agtp729_MsgGT459");
			data.getUser().setTemp("Agtp729_MsgGT459", g_MsgGT459);
			setTemplate(data, "Garantias,Agt,AGTP729.vm");
			return;
		}
		if (Opc.trim().equals("E")) {
			String Seq = data.getParameters().getString("Seq", "");
			i = Integer.parseInt(Seq);
			g_MsgGT459 = (com.intGarantias.modules.global.MSGGT459.Buf_MsgGT459) data
					.getUser().getTemp("Agtp729_MsgGT459");
			RUTGEN.MoverA(g_MsgGT459.GT459_Psw, "E");
			RUTGEN.MoverA(g_MsgGT459.GT459_Idx,
					RUTGEN.Suma(Seq, 0, "1", 0, 3, 0));
			com.intGarantias.modules.global.MSGGT459.Bff_MsgGT459 d_MsgGT459 = new com.intGarantias.modules.global.MSGGT459.Bff_MsgGT459();
			d_MsgGT459 = (com.intGarantias.modules.global.MSGGT459.Bff_MsgGT459) g_MsgGT459.GT459_Tab
					.elementAt(i);
			RUTGEN.MoverA(g_MsgGT459.GT459_Fol, d_MsgGT459.Gif_Fol);
			RUTGEN.MoverA(g_MsgGT459.GT459_Dsg, d_MsgGT459.Gif_Dsg);
			RUTGEN.MoverA(g_MsgGT459.GT459_Fch, d_MsgGT459.Gif_Fch);
			RUTGEN.MoverA(g_MsgGT459.GT459_Tpi, d_MsgGT459.Gif_Tpi);
			data.getUser().removeTemp("Agtp729_MsgGT459");
			data.getUser().setTemp("Agtp729_MsgGT459", g_MsgGT459);
			setTemplate(data, "Garantias,Agt,AGTP729.vm");
			return;
		}
		if (Opc.trim().equals("S")) {
			data.getUser().removeTemp("Agtp729_MsgGT459");
			data.getUser().removeTemp("Agtp729_PrmPC080");
			RUTGEN.MoverA(g_PrmPC080.PC080_Rtn, "NK");
			RUTGEN.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
			BF_MSG.Return_Data("Agtp729", data, g_Msg);
			return;
		}
		if (Opc.trim().equals("A")) {
			g_MsgGT459 = (com.intGarantias.modules.global.MSGGT459.Buf_MsgGT459) data
					.getUser().getTemp("Agtp729_MsgGT459");
			RUTGEN.MoverA(g_MsgGT459.GT459_Dsg,
					data.getParameters().getString("Gif_Dsg", ""));
			RUTGEN.MoverA(g_MsgGT459.GT459_Tpi,
					data.getParameters().getString("Gif_Tpi", ""));
			db_Banco(data, context);
			g_MsgGT459 = MSGGT459.Inicia_MsgGT459();
			RUTGEN.MoverA(g_MsgGT459.GT459_Idr, "I");
			RUTGEN.MoverA(g_MsgGT459.GT459_Sis, g_PrmPC080.PC080_Cnr.Sis);
			RUTGEN.MoverA(g_MsgGT459.GT459_Ncn, g_PrmPC080.PC080_Cnr.Ncn);
			RUTGEN.MoverA(g_MsgGT459.GT459_Seq, g_PrmPC080.PC080_Ntc);
			RUTGEN.MoverA(g_Msg.Msg_Dat, MSGGT459.LSet_De_MsgGT459(g_MsgGT459));
			g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GT459");
			g_MsgGT459 = MSGGT459.LSet_A_MsgGT459(g_Msg.Msg_Dat.toString());
			RUTGEN.MoverA(g_MsgGT459.GT459_Psw, "");
			RUTGEN.MoverA(g_MsgGT459.GT459_Idx, "");
			RUTGEN.MoverA(g_MsgGT459.GT459_Fol, "");
			RUTGEN.MoverA(g_MsgGT459.GT459_Dsg, "");
			RUTGEN.MoverA(g_MsgGT459.GT459_Fch, "");
			RUTGEN.MoverA(g_MsgGT459.GT459_Tpi, "");
			data.getUser().removeTemp("Agtp729_MsgGT459");
			data.getUser().setTemp("Agtp729_MsgGT459", g_MsgGT459);
			setTemplate(data, "Garantias,Agt,AGTP729.vm");
			return;
		}
		if (Opc.trim().equals("C")) {
			g_MsgGT459 = (com.intGarantias.modules.global.MSGGT459.Buf_MsgGT459) data
					.getUser().getTemp("Agtp729_MsgGT459");
			RUTGEN.MoverA(g_MsgGT459.GT459_Psw, "");
			RUTGEN.MoverA(g_MsgGT459.GT459_Idx, "");
			RUTGEN.MoverA(g_MsgGT459.GT459_Fol, "");
			RUTGEN.MoverA(g_MsgGT459.GT459_Dsg, "");
			RUTGEN.MoverA(g_MsgGT459.GT459_Fch, "");
			RUTGEN.MoverA(g_MsgGT459.GT459_Tpi, "");
			data.getUser().removeTemp("Agtp729_MsgGT459");
			data.getUser().setTemp("Agtp729_MsgGT459", g_MsgGT459);
			setTemplate(data, "Garantias,Agt,AGTP729.vm");
			return;
		} else {
			return;
		}
	}

	public String Asigna_Folio(String pcPraCod, RunData data, Context context)
			throws Exception {
		g_MsgED090 = MSGED090.Inicia_MsgED090();
		RUTGEN.MoverA(g_MsgED090.ED090_Idr, "F");
		RUTGEN.MoverA(g_MsgED090.ED090_Cod, pcPraCod);
		RUTGEN.MoverA(g_Msg.Msg_Dat, MSGED090.LSet_De_MsgED090(g_MsgED090));
		g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED090");
		g_MsgED090 = MSGED090.LSet_A_MsgED090(g_Msg.Msg_Dat.toString());
		return g_MsgED090.ED090_Fol.toString();
	}

	public void db_Banco(RunData data, Context context) throws Exception {
		String url = "jdbc:oracle:oci8:@";
		String userName = "fht";
		String password = "fht";
		try {
			OracleConnection conn = (OracleConnection) DriverManager
					.getConnection(url, userName, password);
			String GIF_SIS = g_MsgGT459.GT459_Sis.toString();
			String GIF_NCN = g_MsgGT459.GT459_Ncn.toString();
			String GIF_SEQ = g_MsgGT459.GT459_Seq.toString();
			String GIF_FOL = g_MsgGT459.GT459_Fol.toString();
			String GIF_KYV = GIF_SIS + GIF_NCN + GIF_SEQ + GIF_FOL;
			if (g_MsgGT459.GT459_Psw.toString().equals("E")) {
				OracleStatement stmt = (OracleStatement) conn.createStatement();
				stmt.executeUpdate("delete TB_GIF where GIF_KYV = '" + GIF_KYV
						+ "'");
				stmt.close();
				conn.close();
				return;
			}
			String GIF_FCH = RUTGEN.EdtFec(g_MsgGT459.GT459_Fch.toString());
			String GIF_DSG = g_MsgGT459.GT459_Dsg.toString();
			String GIF_TPI = g_MsgGT459.GT459_Tpi.toString();
			BLOB GIF_GIF = BLOB.createTemporary(conn, false, 1);
			GIF_GIF.open(1);
			FileItem fi = data.getParameters().getFileItem("Archivo");
			String Archivo = fi.getName();
			InputStream instream = fi.getStream();
			long largo = fi.getSize();
			Log.debug("AGTP729[doAgtp729.Archivo]", "[" + Archivo + largo + "]");
			fillBlob(GIF_GIF, instream, largo);
			PreparedStatement pstmt = (OraclePreparedStatement) conn
					.prepareStatement("insert into TB_GIF (GIF_SIS, GIF_NCN, GIF_SEQ, GIF_FOL, GIF_FCH, GIF_DSG, GIF_TPI, GIF_GIF, GIF_KYV) values (?, ?, ?, ?, ?, ?, ?, ?, ?)");
			pstmt.setString(1, GIF_SIS);
			pstmt.setString(2, GIF_NCN);
			pstmt.setString(3, GIF_SEQ);
			pstmt.setString(4, GIF_FOL);
			pstmt.setString(5, GIF_FCH);
			pstmt.setString(6, GIF_DSG);
			pstmt.setString(7, GIF_TPI);
			((OraclePreparedStatement) pstmt).setBLOB(8, GIF_GIF);
			pstmt.setString(9, GIF_KYV);
			pstmt.execute();
			pstmt.close();
			GIF_GIF.close();
			conn.close();
		} catch (SQLException e) {
			Log.debug("JDBCConnection.connect: [" + e.toString() + "]");
		}
	}

	static void fillBlob(BLOB blob, InputStream instream, long length)
			throws Exception {
		OutputStream outstream = blob.getBinaryOutputStream();
		int c;
		while ((c = instream.read()) != -1)
			outstream.write(c);
		outstream.close();
		instream.close();
	}

	public int i;
	public String lcData;
	RUTGEN rg;
	BF_MSG msg;
	com.FHTServlet.modules.global.BF_MSG.Buf_Msg g_Msg;
	com.FHTServlet.modules.global.PRMPC080.PrmPC080 g_PrmPC080;
	com.intGlobal.modules.global.MSGED090.Buf_MsgED090 g_MsgED090;
	com.intGarantias.modules.global.MSGGT459.Buf_MsgGT459 g_MsgGT459;
}
