// Source File Name:   Agtp661.java
// Descripcion     :   Ingreso Estado Avance (ED135, GT096, GT661)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.MSGTXCUR;
import com.FHTServlet.modules.global.BF_TRN;
import com.FHTServlet.modules.global.BF_IMG;
import com.FHTServlet.modules.global.PRMPC080;
import com.FHTServlet.modules.global.PRMPP030;

import com.intGlobal.modules.global.MSGED090;
import com.intGlobal.modules.global.MSGED135;
import com.intGlobal.modules.global.PRMED210;

import com.intGarantias.modules.global.MSGGT096;
import com.intGarantias.modules.global.MSGGT661;
import com.intGarantias.modules.global.GT_IMG;
import com.intGarantias.modules.global.PRMGT093;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp661 extends FHTServletAction
{
  //---------------------------------------------------------------------------------------
  public int    i = 0;
  public String lcData = "";
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  MSGTXCUR.Buf_TxCur g_MsgTxCur    = new MSGTXCUR.Buf_TxCur();
  MSGTXCUR.Buf_Tx900 g_MsgTx900    = new MSGTXCUR.Buf_Tx900();
  BF_TRN trn                       = new BF_TRN();
  BF_TRN.Buf_Trn g_Trn             = new BF_TRN.Buf_Trn();
  PRMPC080.PrmPC080 g_PrmPC080     = new PRMPC080.PrmPC080();
  Vector v_Tab_Img                 = new Vector();
  BF_IMG img                       = new BF_IMG();
  BF_IMG.Buf_Img g_Img             = new BF_IMG.Buf_Img();
  BF_IMG.Buf_Img_Bse g_Img_Base    = new BF_IMG.Buf_Img_Bse();
  GT_IMG gtimg                     = new GT_IMG();
  GT_IMG.Buf_Img_GtApe g_Img_GtApe = new GT_IMG.Buf_Img_GtApe();
  GT_IMG.Buf_Img_GtTsd g_Img_GtTsd = new GT_IMG.Buf_Img_GtTsd();
  GT_IMG.Buf_Img_GtAvn g_Img_GtAvn = new GT_IMG.Buf_Img_GtAvn();
  GT_IMG.Buf_Img_GtEao g_Img_GtEao = new GT_IMG.Buf_Img_GtEao();
  GT_IMG.Buf_Img_GtPts g_Img_GtPts = new GT_IMG.Buf_Img_GtPts();
  MSGED090.Buf_MsgED090 g_MsgED090 = new MSGED090.Buf_MsgED090();
  MSGED135.Buf_MsgED135 g_MsgED135 = new MSGED135.Buf_MsgED135();
  MSGGT096.Buf_MsgGT096 g_MsgGT096 = new MSGGT096.Buf_MsgGT096();
  MSGGT661.Buf_MsgGT661 g_MsgGT661 = new MSGGT661.Buf_MsgGT661();
  PRMED210.Buf_PrmED210 g_PrmED210 = new PRMED210.Buf_PrmED210();
  PRMPP030.Buf_PrmPP030 g_PrmPP030 = new PRMPP030.Buf_PrmPP030();
  PRMGT093.Buf_PrmGT093 g_PrmGT093 = new PRMGT093.Buf_PrmGT093();
  //---------------------------------------------------------------------------------------
  public Agtp661()
  {
  }
  //---------------------------------------------------------------------------------------
  public void doPerform(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP661[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp661-DIN", data);
    if (g_Msg.Msg_Pgr.toString().equals("RT"))
       { doAgtp661_Continue(data, context); }
    else
       { doAgtp661_Init(data, context); }
  }
  //---------------------------------------------------------------------------------------
  public void doCancel(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP661[doCancel.start]", "[CANCELADO]");
    setTemplate(data, "Garantias,Agt,AGTP661.vm");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp661_Init(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP661[doAgtp661_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    Carga_de_Host(data, context);
    Carga_Inicial(data, context);
    if (g_PrmPC080.PC080_Fte.toString().trim().equals("SPV")
     && g_PrmPC080.PC080_Rtn.toString().trim().equals("WR"))
       { rg.MoverA(g_PrmPC080.PC080_Rtn, "RA"); }
    else
       {
         if (g_PrmPC080.PC080_Fte.toString().trim().compareTo("TSD")!=0)
            { rg.MoverA(g_PrmPC080.PC080_Rtn, "RO"); }
       }
    data.getUser().removeTemp("Agtp661_PrmPC080");
    data.getUser().setTemp("Agtp661_PrmPC080", g_PrmPC080);                                     
    rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
    BF_MSG.Param_Program(data, g_Msg);
    setTemplate(data, "Garantias,Agt,AGTP661.vm" );
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp661(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP661[doAgtp661.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp661-MAN", data);
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    String Opc = data.getParameters().getString("Opc" ,"");
    if (Opc.trim().equals("R"))
       {
         //Boton Errores
         g_MsgED135 = (MSGED135.Buf_MsgED135)data.getUser().getTemp("Agtp661_MsgED135");
         g_PrmED210 = PRMED210.Inicia_PrmED210();
         rg.MoverA(g_PrmED210.ED210_Rtn, "RO");
         rg.MoverA(g_PrmED210.ED210_Ldt, g_MsgED135.ED135_Ldv);
         data.getUser().removeTemp("Agtp661_g_PrmPC080");
         data.getUser().setTemp("Agtp661_g_PrmPC080" ,g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMED210.LSet_De_PrmED210(g_PrmED210));
         BF_MSG.Link_Program("Agtp661", "Aedp210", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("5"))
       {
         //Consulta PContables Disponibles
         g_PrmGT093 = PRMGT093.Inicia_PrmGT093();
         rg.MoverA(g_PrmGT093.GT093_Sis, "GAR");
         rg.MoverA(g_PrmGT093.GT093_Dcn, "PROCO");
         rg.MoverA(g_PrmGT093.GT093_Itb, "H");
         data.getUser().removeTemp("Agtp661_g_PrmPC080");
         data.getUser().setTemp("Agtp661_g_PrmPC080" ,g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMGT093.LSet_De_PrmGT093(g_PrmGT093));
         BF_MSG.Link_Program("Agtp661", "Agtp093", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("D"))
       {
         //Boton Devolver
         g_PrmED210 = PRMED210.Inicia_PrmED210();
         rg.MoverA(g_PrmED210.ED210_Rtn, "RW");
         data.getUser().removeTemp("Agtp661_g_PrmPC080");
         data.getUser().setTemp("Agtp661_g_PrmPC080" ,g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMED210.LSet_De_PrmED210(g_PrmED210));
         BF_MSG.Link_Program("Agtp661", "Aedp210", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("K"))
       {
         //Boton Cambio Fecha
         String Fec = data.getParameters().getString("Fec", "");
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp661_ImgGtApe");
         g_Img_GtTsd = (GT_IMG.Buf_Img_GtTsd)data.getUser().getTemp("Agtp661_ImgGtTsd");
         g_Img_GtAvn = (GT_IMG.Buf_Img_GtAvn)data.getUser().getTemp("Agtp661_ImgGtAvn");
         rg.MoverA(g_Img_GtApe.GtApe_Tfr, rg.Zeros(3));  
         rg.MoverA(g_Img_GtApe.GtApe_Vtt, rg.Zeros(15));  
         rg.MoverA(g_Img_GtApe.GtApe_Vts, rg.Zeros(15));  
         rg.MoverA(g_Img_GtApe.GtApe_Vtb, rg.Zeros(15));  
         rg.MoverA(g_Img_GtApe.GtApe_Vcn, rg.Zeros(15));  
       	 rg.MoverA(g_Img_GtApe.GtApe_Hvt, rg.Zeros(15));
       	 rg.MoverA(g_Img_GtApe.GtApe_Hvl, rg.Zeros(15));
       	 rg.MoverA(g_Img_GtApe.GtApe_Hvb, rg.Zeros(15));
       	 rg.MoverA(g_Img_GtApe.GtApe_Hvc, rg.Zeros(15));
       	 rg.MoverA(g_Img_GtApe.GtApe_Pvt, rg.Zeros(15));
       	 rg.MoverA(g_Img_GtApe.GtApe_Pvl, rg.Zeros(15));
       	 rg.MoverA(g_Img_GtApe.GtApe_Pvb, rg.Zeros(15));
       	 rg.MoverA(g_Img_GtApe.GtApe_Pvc, rg.Zeros(15));
         Actualiza_Fecha(Fec, data, context);
         rg.MoverA(g_Img_GtAvn.GtAvn_Frv, g_Img_GtTsd.GtTsd_Fts);
         rg.MoverA(g_Img_GtAvn.GtAvn_Vur, rg.FmtValor("1",0,2,9,"+"));
         rg.MoverA(g_Img_GtAvn.GtAvn_Fvt, rg.DateAdd(g_Img_GtAvn.GtAvn_Frv,"A",2));
         data.getUser().removeTemp("Agtp661_ImgGtApe");
         data.getUser().setTemp("Agtp661_ImgGtApe", g_Img_GtApe);
         data.getUser().removeTemp("Agtp661_ImgGtTsd");
         data.getUser().setTemp("Agtp661_ImgGtTsd", g_Img_GtTsd);
         data.getUser().removeTemp("Agtp661_ImgGtAvn");
         data.getUser().setTemp("Agtp661_ImgGtAvn", g_Img_GtAvn);
         setTemplate(data, "Garantias,Agt,AGTP661.vm" );
         return;
       }
    if (Opc.trim().equals("T"))
       {
         //Boton Detalle (Captura Imagenes)
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp661_ImgGtApe");
         g_Img_GtTsd = (GT_IMG.Buf_Img_GtTsd)data.getUser().getTemp("Agtp661_ImgGtTsd");
         g_Img_GtAvn = (GT_IMG.Buf_Img_GtAvn)data.getUser().getTemp("Agtp661_ImgGtAvn");
         g_Img_GtEao = (GT_IMG.Buf_Img_GtEao)data.getUser().getTemp("Agtp661_ImgGtEao");
         Actualiza_Datos(data, context);
         Carga_vEao(data, context);
         data.getUser().removeTemp("Agtp661_ImgGtApe");
         data.getUser().setTemp("Agtp661_ImgGtApe", g_Img_GtApe);
         data.getUser().removeTemp("Agtp661_ImgGtTsd");
         data.getUser().setTemp("Agtp661_ImgGtTsd", g_Img_GtTsd);
         data.getUser().removeTemp("Agtp661_ImgGtAvn");
         data.getUser().setTemp("Agtp661_ImgGtAvn", g_Img_GtAvn);
         data.getUser().removeTemp("Agtp661_ImgGtEao");
         data.getUser().setTemp("Agtp661_ImgGtEao", g_Img_GtEao);
         rg.MoverA(g_PrmPC080.PC080_Ntc, g_Img_GtAvn.GtAvn_Seq);
         data.getUser().removeTemp("Agtp661_g_PrmPC080");
         data.getUser().setTemp("Agtp661_g_PrmPC080" ,g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         //BF_MSG.Link_Program("Agtp661", "Agtp729", data, g_Msg);
         BF_MSG.Link_Program("Agtp661", "Appp096", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("G"))
       {
         //Boton Guardar
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp661_ImgGtApe");
         g_Img_GtTsd = (GT_IMG.Buf_Img_GtTsd)data.getUser().getTemp("Agtp661_ImgGtTsd");
         g_Img_GtAvn = (GT_IMG.Buf_Img_GtAvn)data.getUser().getTemp("Agtp661_ImgGtAvn");
         g_Img_GtEao = (GT_IMG.Buf_Img_GtEao)data.getUser().getTemp("Agtp661_ImgGtEao");
         Actualiza_Datos(data, context);
         Vector g_Tab_Img = A_Tab_Img(data);
         img.Graba_Img_Host(g_Msg, g_Tab_Img, data, context);
         Carga_vEao(data, context);
         data.getUser().removeTemp("Agtp661_ImgGtApe");
         data.getUser().setTemp("Agtp661_ImgGtApe", g_Img_GtApe);
         data.getUser().removeTemp("Agtp661_ImgGtTsd");
         data.getUser().setTemp("Agtp661_ImgGtTsd", g_Img_GtTsd);
         data.getUser().removeTemp("Agtp661_ImgGtAvn");
         data.getUser().setTemp("Agtp661_ImgGtAvn", g_Img_GtAvn);
         data.getUser().removeTemp("Agtp661_ImgGtEao");
         data.getUser().setTemp("Agtp661_ImgGtEao", g_Img_GtEao);
         setTemplate(data, "Garantias,Agt,AGTP661.vm" );
         return;
       }
    if (Opc.trim().equals("I"))
       {
         //Boton Imagen
         g_Img_GtAvn = (GT_IMG.Buf_Img_GtAvn)data.getUser().getTemp("Agtp661_ImgGtAvn");
         rg.MoverA(g_PrmPC080.PC080_Ntc, g_Img_GtAvn.GtAvn_Seq);
         rg.MoverA(g_PrmPC080.PC080_Dtt, g_Img_GtAvn.GtAvn_Dsb);
         rg.MoverA(g_PrmPC080.PC080_Mnt, g_Img_GtAvn.GtAvn_Vcn);
         rg.MoverA(g_PrmPC080.PC080_Amv, "I");
         //rg.MoverA(g_PrmPC080.PC080_Rtn, "RO");
         data.getUser().removeTemp("Agtp661_g_PrmPC080");
         data.getUser().setTemp("Agtp661_g_PrmPC080", g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         //BF_MSG.Link_Program("Agtp661", "Agtp459", data, g_Msg);
         BF_MSG.Link_Program("Agtp661", "Appp096", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("S"))
       {
         //Boton Salir
         data.getUser().removeTemp("Agtp661_MsgED135");
         data.getUser().removeTemp("Agtp661_ImgGtEao");
         data.getUser().removeTemp("Agtp661_ImgGtAvn");
         data.getUser().removeTemp("Agtp661_ImgGtTsd");
         data.getUser().removeTemp("Agtp661_ImgBase");
         data.getUser().removeTemp("Agtp661_ImgGtApe");
         data.getUser().removeTemp("Agtp661_PrmPC080");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp661", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("O"))
       {
         //Boton OK (Aceptar)
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp661_ImgGtApe");
         g_Img_GtTsd = (GT_IMG.Buf_Img_GtTsd)data.getUser().getTemp("Agtp661_ImgGtTsd");
         g_Img_GtAvn = (GT_IMG.Buf_Img_GtAvn)data.getUser().getTemp("Agtp661_ImgGtAvn");
         g_Img_GtEao = (GT_IMG.Buf_Img_GtEao)data.getUser().getTemp("Agtp661_ImgGtEao");
         Actualiza_Datos(data, context);
         Recalcula_GtApe(data, context);
         Vector g_Tab_Img = A_Tab_Img(data);
         img.Graba_Img_Host(g_Msg, g_Tab_Img, data, context);
         g_MsgED135 = MSGED135.Inicia_MsgED135();
         rg.MoverA(g_MsgED135.ED135_Idr, "M");
         rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
         rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
         rg.MoverA(g_MsgED135.ED135_Est, "");
         rg.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
         rg.MoverA(g_MsgED135.ED135_Vlc, g_Img_GtApe.GtApe_Vts);
         rg.MoverA(g_MsgED135.ED135_Ezt, "TMTSD");
         rg.MoverA(g_MsgED135.ED135_Exi, "TSD");
         rg.MoverA(g_MsgED135.ED135_Exn, rg.Zeros(12));
         rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
         g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
         data.getUser().removeTemp("Agtp661_MsgED135");
         data.getUser().removeTemp("Agtp661_ImgGtEao");
         data.getUser().removeTemp("Agtp661_ImgGtAvn");
         data.getUser().removeTemp("Agtp661_ImgGtTsd");
         data.getUser().removeTemp("Agtp661_ImgBase");
         data.getUser().removeTemp("Agtp661_ImgGtApe");
         data.getUser().removeTemp("Agtp661_PrmPC080");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp661", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("C"))
       {
         //Boton Cursar
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp661_ImgGtApe");
         g_Img_GtTsd = (GT_IMG.Buf_Img_GtTsd)data.getUser().getTemp("Agtp661_ImgGtTsd");
         g_Img_GtAvn = (GT_IMG.Buf_Img_GtAvn)data.getUser().getTemp("Agtp661_ImgGtAvn");
         g_Img_GtEao = (GT_IMG.Buf_Img_GtEao)data.getUser().getTemp("Agtp661_ImgGtEao");
         //if (PagoTasador(data, context) == false )
         //   {
         //     data.getUser().removeTemp("Agtp661_g_PrmPC080");
         //     data.getUser().setTemp("Agtp661_g_PrmPC080", g_PrmPC080);
         //     BF_MSG.MsgInt("Agtp661", data, g_Msg, "Msg_Rtn", "T"); 
         //     return;
         //   }
         Vector g_Tab_Img = A_Tab_Img(data);
         img.Graba_Img_Host(g_Msg, g_Tab_Img, data, context);
         g_MsgED135 = MSGED135.Inicia_MsgED135();
         rg.MoverA(g_MsgED135.ED135_Idr, "M");
         rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
         rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
         rg.MoverA(g_MsgED135.ED135_Est, "");
         rg.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
         rg.MoverA(g_MsgED135.ED135_Ezt, "TERMI");
         rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
         g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
         //data.getUser().removeTemp("Agtp661_g_PrmPC080");
         //data.getUser().setTemp("Agtp661_g_PrmPC080", g_PrmPC080);
         //rg.MoverA(g_Msg.Msg_Dat, MSGTXCUR.LSet_De_MsgTxCur(g_MsgTxCur));
         //g_MsgTx900 = MSGTXCUR.LSet_A_MsgTx900(g_Msg.Msg_Dat.toString());
         //data.getUser().removeTemp("Agtp291_MsgTx900");
         //data.getUser().setTemp("Agtp291_MsgTx900", g_MsgTx900);
         //rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         //BF_MSG.Link_Program("Agtp661", "Agtp291", data, g_Msg);
         data.getUser().removeTemp("Agtp661_MsgED135");
         data.getUser().removeTemp("Agtp661_ImgGtEao");
         data.getUser().removeTemp("Agtp661_ImgGtAvn");
         data.getUser().removeTemp("Agtp661_ImgGtTsd");
         data.getUser().removeTemp("Agtp661_ImgBase");
         data.getUser().removeTemp("Agtp661_ImgGtApe");
         data.getUser().removeTemp("Agtp661_PrmPC080");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp661", data, g_Msg);
         return;
       }
  }
  //---------------------------------------------------------------------------------------
  public void Carga_de_Host(RunData data, Context context)
              throws Exception 
  {
    v_Tab_Img = img.Carga_Img_Host(g_Msg, g_PrmPC080.PC080_Ntr.toString(), data, context);
    De_Tab_Img(v_Tab_Img, data);
    if (g_Img_GtApe.GtApe_Tfr.toString().trim().equals(""))
       { rg.MoverA(g_Img_GtApe.GtApe_Tfr, rg.Zeros(3)); }
    if (g_Img_GtApe.GtApe_Vtt.toString().trim().equals(""))
       { 
       	 rg.MoverA(g_Img_GtApe.GtApe_Vtt, rg.Zeros(15));
       	 rg.MoverA(g_Img_GtApe.GtApe_Vts, rg.Zeros(15));
       	 rg.MoverA(g_Img_GtApe.GtApe_Vtb, rg.Zeros(15));
       	 rg.MoverA(g_Img_GtApe.GtApe_Vcn, rg.Zeros(15));
       	 rg.MoverA(g_Img_GtApe.GtApe_Hvt, rg.Zeros(15));
       	 rg.MoverA(g_Img_GtApe.GtApe_Hvl, rg.Zeros(15));
       	 rg.MoverA(g_Img_GtApe.GtApe_Hvb, rg.Zeros(15));
       	 rg.MoverA(g_Img_GtApe.GtApe_Hvc, rg.Zeros(15));
       	 rg.MoverA(g_Img_GtApe.GtApe_Pvt, rg.Zeros(15));
       	 rg.MoverA(g_Img_GtApe.GtApe_Pvl, rg.Zeros(15));
       	 rg.MoverA(g_Img_GtApe.GtApe_Pvb, rg.Zeros(15));
       	 rg.MoverA(g_Img_GtApe.GtApe_Pvc, rg.Zeros(15));
       }
    if (g_Img_GtTsd.GtTsd_Fts.toString().trim().equals(""))
       {
       	 rg.MoverA(g_Img_GtApe.GtApe_Fig, g_Msg.Msg_Fch);
         Actualiza_Fecha(g_Msg.Msg_Fch.toString(), data, context);
       }
    if (g_Img_GtAvn.GtAvn_Seq.toString().trim().equals(""))
       { Inicializa(data, context); }
    Carga_vEao(data, context);
    data.getUser().removeTemp("Agtp661_ImgBase");
    data.getUser().setTemp("Agtp661_ImgBase", g_Img_Base);
    data.getUser().removeTemp("Agtp661_ImgGtApe");
    data.getUser().setTemp("Agtp661_ImgGtApe", g_Img_GtApe);
    data.getUser().removeTemp("Agtp661_ImgGtTsd");
    data.getUser().setTemp("Agtp661_ImgGtTsd", g_Img_GtTsd);
    data.getUser().removeTemp("Agtp661_ImgGtAvn");
    data.getUser().setTemp("Agtp661_ImgGtAvn", g_Img_GtAvn);
    data.getUser().removeTemp("Agtp661_ImgGtEao");
    data.getUser().setTemp("Agtp661_ImgGtEao", g_Img_GtEao);
  }
  //---------------------------------------------------------------------------------------
  public void Actualiza_Fecha(String Fecha, RunData data, Context context)
              throws Exception 
  {
    rg.MoverA(g_Img_GtTsd.GtTsd_Fts, Fecha);
    g_MsgGT096 = MSGGT096.Inicia_MsgGT096();
    rg.MoverA(g_MsgGT096.GT096_Idr, "V");
    rg.MoverA(g_MsgGT096.GT096_Fec, g_Img_GtTsd.GtTsd_Fts);
    rg.MoverA(g_Msg.Msg_Dat, MSGGT096.LSet_De_MsgGT096(g_MsgGT096));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GT096");
    g_MsgGT096 = MSGGT096.LSet_A_MsgGT096(g_Msg.Msg_Dat.toString());
    rg.MoverA(g_Img_GtApe.GtApe_Fts, g_Img_GtTsd.GtTsd_Fts);
    rg.MoverA(g_Img_GtApe.GtApe_Vuo, rg.FmtValor("1",0,2,9,"+"));
    if (g_Img_Base.Img_Base_Trj.toString().trim().equals("UF"))
       { rg.MoverA(g_Img_GtApe.GtApe_Vuo, rg.FmtValor(g_MsgGT096.GT096_Vuf,2,2,9,"+")); }
    if (g_Img_Base.Img_Base_Trj.toString().trim().equals("IVP"))
       { rg.MoverA(g_Img_GtApe.GtApe_Vuo, rg.FmtValor(g_MsgGT096.GT096_Viv,2,2,9,"+")); }
    if (g_Img_Base.Img_Base_Trj.toString().trim().equals("TC"))
       { rg.MoverA(g_Img_GtApe.GtApe_Vuo, rg.FmtValor(g_MsgGT096.GT096_Vtc,2,2,9,"+")); }
    rg.MoverA(g_Img_GtTsd.GtTsd_Vuf, rg.FmtValor(g_MsgGT096.GT096_Vuf,2,2,9,"+"));
    rg.MoverA(g_Img_GtTsd.GtTsd_Vtc, rg.FmtValor(g_MsgGT096.GT096_Vtc,2,2,9,"+"));
  }
  //---------------------------------------------------------------------------------------
  public void De_Tab_Img(Vector v_Tab_Img, RunData data)
  {
    g_Img       = img.Inicia_Img();
    lcData      = img.LSet_De_Img(g_Img);
    g_Img_Base  = img.LSet_A_ImgBase(lcData);
    g_Img_GtApe = gtimg.LSet_A_ImgGtApe(g_Img.Img_Dat.toString());
    g_Img_GtTsd = gtimg.LSet_A_ImgGtTsd(g_Img.Img_Dat.toString());
    g_Img_GtAvn = gtimg.LSet_A_ImgGtAvn(g_Img.Img_Dat.toString());
    g_Img_GtEao = gtimg.LSet_A_ImgGtEao(g_Img.Img_Dat.toString());
    for (i=0; i<v_Tab_Img.size(); i++)
        {
          lcData = img.LSet_De_Img((BF_IMG.Buf_Img)v_Tab_Img.elementAt(i));
          g_Img  = img.LSet_A_Img(lcData);
          if (g_Img.Img_Dax.toString().trim().equals("BASE"))
             {
               g_Img_GtApe = gtimg.LSet_A_ImgGtApe(g_Img.Img_Dat.toString());
               g_Img_Base  = img.LSet_A_ImgBase(g_Img_GtApe.GtApe_Bse.toString());
             }
          if (g_Img.Img_Dax.toString().trim().equals("XTSD"))
             { g_Img_GtTsd = gtimg.LSet_A_ImgGtTsd(g_Img.Img_Dat.toString()); }
          if (g_Img.Img_Dax.toString().trim().equals("XYAVN"))
             { g_Img_GtAvn = gtimg.LSet_A_ImgGtAvn(g_Img.Img_Dat.toString()); }
          if (g_Img.Img_Dax.toString().trim().equals("XYEAO"))
             { g_Img_GtEao = gtimg.LSet_A_ImgGtEao(g_Img.Img_Dat.toString()); }
        }
    if (g_Img_GtEao.GtEao_Seq.toString().trim().equals(""))
       {
         rg.MoverA(g_Img_GtEao.GtEao_Ppv, rg.Zeros(9));
         rg.MoverA(g_Img_GtEao.GtEao_Oep, rg.Zeros(5));
         rg.MoverA(g_Img_GtEao.GtEao_Oev, rg.Zeros(9));
         rg.MoverA(g_Img_GtEao.GtEao_Eap, rg.Zeros(5));
         rg.MoverA(g_Img_GtEao.GtEao_Eav, rg.Zeros(9));
         rg.MoverA(g_Img_GtEao.GtEao_Epp, rg.Zeros(5));
         rg.MoverA(g_Img_GtEao.GtEao_Epv, rg.Zeros(9));
         for (int i=0;i<15;i++)
             {
               g_Img_GtEao.vGtEao_Ppv.set(i, g_Img_GtEao.GtEao_Ppv);
               g_Img_GtEao.vGtEao_Oep.set(i, g_Img_GtEao.GtEao_Oep);
               g_Img_GtEao.vGtEao_Oev.set(i, g_Img_GtEao.GtEao_Oev);
               g_Img_GtEao.vGtEao_Eap.set(i, g_Img_GtEao.GtEao_Eap);
               g_Img_GtEao.vGtEao_Eav.set(i, g_Img_GtEao.GtEao_Eav);
               g_Img_GtEao.vGtEao_Epp.set(i, g_Img_GtEao.GtEao_Epp);
               g_Img_GtEao.vGtEao_Epv.set(i, g_Img_GtEao.GtEao_Epv);
             }
       }
  }
  //---------------------------------------------------------------------------------------
  public void Inicializa(RunData data, Context context)
              throws Exception 
  {
    rg.MoverA(g_Img_GtAvn.GtAvn_Seq, g_Img_GtApe.GtApe_Psq);
    rg.MoverA(g_Img_GtAvn.GtAvn_Itb, "EP");
    rg.MoverA(g_Img_GtApe.GtApe_Idx, g_Img_GtApe.GtApe_Psq);
    rg.MoverA(g_Img_GtApe.GtApe_Tfr, "001");
    rg.MoverA(g_Img_GtAvn.GtAvn_Frv, g_Img_GtTsd.GtTsd_Fts);
    rg.MoverA(g_Img_GtAvn.GtAvn_Vur, rg.FmtValor("1",0,2,9,"+"));
    rg.MoverA(g_Img_GtAvn.GtAvn_Fvt, rg.DateAdd(g_Img_GtAvn.GtAvn_Frv,"A",2));
    g_MsgGT661 = MSGGT661.Inicia_MsgGT661();
    rg.MoverA(g_MsgGT661.GT661_Idr, "I");
    rg.MoverA(g_MsgGT661.GT661_Gti, g_Img_Base.Img_Base_Sis.toString() + g_Img_Base.Img_Base_Ncn.toString());
    rg.MoverA(g_MsgGT661.GT661_Seq, g_Img_GtApe.GtApe_Psq);
    rg.MoverA(g_Msg.Msg_Dat, MSGGT661.LSet_De_MsgGT661(g_MsgGT661));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GT661");
    g_MsgGT661 = MSGGT661.LSet_A_MsgGT661(g_Msg.Msg_Dat.toString());
    rg.MoverA(g_Img_GtEao.GtEao_Seq, g_Img_GtAvn.GtAvn_Seq);
    rg.MoverA(g_Img_GtEao.GtEao_Sqd, g_Img_GtAvn.GtAvn_Sqd);
    rg.MoverA(g_Img_GtAvn.GtAvn_Sqd, g_MsgGT661.GT661_Sqd);
    rg.MoverA(g_Img_GtAvn.GtAvn_Itg, g_MsgGT661.GT661_Itg);
    rg.MoverA(g_Img_GtAvn.GtAvn_Dpc, g_MsgGT661.GT661_Dpc);
    if (g_MsgGT661.GT661_Idr.toString().equals("N"))
       {
         rg.MoverA(g_Img_GtAvn.GtAvn_Sqd, g_Img_GtApe.GtApe_Psd);
         if (g_Img_GtAvn.GtAvn_Fin.toString().trim().equals(""))
            { rg.MoverA(g_Img_GtAvn.GtAvn_Fin, g_Msg.Msg_Fch); }
         if (g_Img_GtAvn.GtAvn_Fte.toString().trim().equals(""))
            { rg.MoverA(g_Img_GtAvn.GtAvn_Fte, g_Msg.Msg_Fch); }
         if (g_Img_GtAvn.GtAvn_Fed.toString().trim().equals(""))
            { rg.MoverA(g_Img_GtAvn.GtAvn_Fed, g_Msg.Msg_Fch); }
         if (g_Img_GtAvn.GtAvn_Sps.toString().trim().equals(""))
            { rg.MoverA(g_Img_GtAvn.GtAvn_Sps, "19000"); }                          //IVA inicial
         if (g_Img_GtAvn.GtAvn_Vsg.toString().trim().equals(""))
            { rg.MoverA(g_Img_GtAvn.GtAvn_Vsg, rg.Zeros(15)); }
         return;
       }
    rg.MoverA(g_Img_GtAvn.GtAvn_Dsb, g_MsgGT661.GT661_Dsb);
    rg.MoverA(g_Img_GtAvn.GtAvn_Ctr, g_MsgGT661.GT661_Ctr);
    rg.MoverA(g_Img_GtAvn.GtAvn_Fin, g_MsgGT661.GT661_Fin);
    rg.MoverA(g_Img_GtAvn.GtAvn_Fte, g_MsgGT661.GT661_Fte);
    rg.MoverA(g_Img_GtAvn.GtAvn_Ped, g_MsgGT661.GT661_Ped);
    rg.MoverA(g_Img_GtAvn.GtAvn_Fed, g_MsgGT661.GT661_Fed);
    rg.MoverA(g_Img_GtAvn.GtAvn_Spc, g_MsgGT661.GT661_Spc);
    rg.MoverA(g_Img_GtAvn.GtAvn_Sps, g_MsgGT661.GT661_Sps);
    rg.MoverA(g_Img_GtAvn.GtAvn_Rem, g_MsgGT661.GT661_Rem);
    rg.MoverA(g_Img_GtAvn.GtAvn_Vsg, g_MsgGT661.GT661_Vsg);
    rg.MoverA(g_Img_GtAvn.GtAvn_Dpy, g_MsgGT661.GT661_Dpy);
    rg.MoverA(g_Img_GtAvn.GtAvn_Occ, g_MsgGT661.GT661_Occ);
    rg.MoverA(g_Img_GtAvn.GtAvn_Opp, g_MsgGT661.GT661_Opp);
    rg.MoverA(g_Img_GtAvn.GtAvn_Iro, "R");
    rg.MoverA(g_Img_GtEao.GtEao_Seq, g_Img_GtAvn.GtAvn_Seq);
    rg.MoverA(g_Img_GtEao.GtEao_Sqd, g_Img_GtAvn.GtAvn_Sqd);
    for (int i=0;i<15;i++)
        {
          GT_IMG.Buf_Img_GtEao l_Img_GtEao = new GT_IMG.Buf_Img_GtEao();
          rg.MoverA(l_Img_GtEao.GtEao_Ppv, ((MSGGT661.vPpv)g_MsgGT661.vGT661_Ppv.elementAt(i)).GT661_Ppv);
          rg.MoverA(l_Img_GtEao.GtEao_Eap, ((MSGGT661.vEap)g_MsgGT661.vGT661_Eap.elementAt(i)).GT661_Eap);
          rg.MoverA(l_Img_GtEao.GtEao_Eav, ((MSGGT661.vEav)g_MsgGT661.vGT661_Eav.elementAt(i)).GT661_Eav);
          g_Img_GtEao.vGtEao_Ppv.set(i, l_Img_GtEao.GtEao_Ppv);
          g_Img_GtEao.vGtEao_Eap.set(i, l_Img_GtEao.GtEao_Eap);
          g_Img_GtEao.vGtEao_Eav.set(i, l_Img_GtEao.GtEao_Eav);
        }
  }
  //---------------------------------------------------------------------------------------
  public void Carga_vEao(RunData data, Context context)
              throws Exception 
  {
    Vector v_Eao = new Vector();
    v_Eao.clear();
    for (int i=0;i<15;i++)
        {
          GT_IMG.Buf_vEao vEao = new GT_IMG.Buf_vEao();
          if (i==0)   { rg.MoverA(vEao.vEao_Tit, "Obras Previas"); }
          if (i==1)   { rg.MoverA(vEao.vEao_Tit, "Obra Gruesa"); }
          if (i==2)   { rg.MoverA(vEao.vEao_Tit, "Terminaciones"); }
          if (i==3)   { rg.MoverA(vEao.vEao_Tit, "Instalaciones"); }
          if (i==4)   { rg.MoverA(vEao.vEao_Tit, "Obras Exteriores"); }
          if (i==5)   { rg.MoverA(vEao.vEao_Tit, "O. Complementarias"); }
          if (i==6)   { rg.MoverA(vEao.vEao_Tit, "Urbanizacion"); }
          if (i==7)   { rg.MoverA(vEao.vEao_Tit, "Obras Adicionales"); }
          if (i==8)   { rg.MoverA(vEao.vEao_Tit, "Otras Obras"); }
          if (i==9)   { rg.MoverA(vEao.vEao_Tit, "Costo Directo Obras"); }
          if (i==10)  { rg.MoverA(vEao.vEao_Tit, "Gastos Generales"); }
          if (i==11)  { rg.MoverA(vEao.vEao_Tit, "Utilidad"); }
          if (i==12)  { rg.MoverA(vEao.vEao_Tit, "Costo Construccion"); }
          if (i==13)  { rg.MoverA(vEao.vEao_Tit, "IVA"); }
          if (i==14)  { rg.MoverA(vEao.vEao_Tit, "Costo Total"); }
          rg.MoverA(vEao.vEao_Ppv, g_Img_GtEao.vGtEao_Ppv.elementAt(i).toString());
          rg.MoverA(vEao.vEao_Oep, g_Img_GtEao.vGtEao_Oep.elementAt(i).toString());
          rg.MoverA(vEao.vEao_Oev, g_Img_GtEao.vGtEao_Oev.elementAt(i).toString());
          rg.MoverA(vEao.vEao_Eap, g_Img_GtEao.vGtEao_Eap.elementAt(i).toString());
          rg.MoverA(vEao.vEao_Eav, g_Img_GtEao.vGtEao_Eav.elementAt(i).toString());
          rg.MoverA(vEao.vEao_Epp, g_Img_GtEao.vGtEao_Epp.elementAt(i).toString());
          rg.MoverA(vEao.vEao_Epv, g_Img_GtEao.vGtEao_Epv.elementAt(i).toString());
          v_Eao.add(vEao);
        }               
    data.getUser().removeTemp("Agtp661_vEao");
    data.getUser().setTemp("Agtp661_vEao", v_Eao);
  }
  //---------------------------------------------------------------------------------------
  public void Carga_Inicial(RunData data, Context context)
              throws Exception 
  {
    g_MsgED135 = MSGED135.Inicia_MsgED135();
    rg.MoverA(g_MsgED135.ED135_Idr, "C");
    rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
    rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
    g_MsgED135 = MSGED135.LSet_A_MsgED135(g_Msg.Msg_Dat.toString());
    data.getUser().removeTemp("Agtp661_MsgED135");
    data.getUser().setTemp("Agtp661_MsgED135", g_MsgED135);
  }
  //---------------------------------------------------------------------------------------
  public void Actualiza_Datos(RunData data, Context context)
              throws Exception 
  {
    rg.MoverA(g_Img_GtApe.GtApe_Vtt, data.getParameters().getString("GtApe_Vtt", ""));
    rg.MoverA(g_Img_GtApe.GtApe_Vts, data.getParameters().getString("GtApe_Vts", ""));
    rg.MoverA(g_Img_GtTsd.GtTsd_Fts, data.getParameters().getString("GtTsd_Fts", ""));
    rg.MoverA(g_Img_GtTsd.GtTsd_Nbf, data.getParameters().getString("GtTsd_Nbf", ""));
    rg.MoverA(g_Img_GtTsd.GtTsd_Pcb, data.getParameters().getString("GtTsd_Pcb", ""));
    rg.MoverA(g_Img_GtTsd.GtTsd_Hon, data.getParameters().getString("GtTsd_Hon", ""));
    rg.MoverA(g_Img_GtTsd.GtTsd_Vst, data.getParameters().getString("GtTsd_Vst", ""));
    rg.MoverA(g_Img_GtAvn.GtAvn_Dsb, data.getParameters().getString("GtAvn_Dsb", ""));
    rg.MoverA(g_Img_GtAvn.GtAvn_Ctr, data.getParameters().getString("GtAvn_Ctr", ""));
    rg.MoverA(g_Img_GtAvn.GtAvn_Fin, data.getParameters().getString("GtAvn_Fin", ""));
    rg.MoverA(g_Img_GtAvn.GtAvn_Fte, data.getParameters().getString("GtAvn_Fte", ""));
    rg.MoverA(g_Img_GtAvn.GtAvn_Ped, data.getParameters().getString("GtAvn_Ped", ""));
    rg.MoverA(g_Img_GtAvn.GtAvn_Fed, data.getParameters().getString("GtAvn_Fed", ""));
    rg.MoverA(g_Img_GtAvn.GtAvn_Spc, rg.Zeros(5));
    rg.MoverA(g_Img_GtAvn.GtAvn_Sps, data.getParameters().getString("GtAvn_Sps", ""));
    rg.MoverA(g_Img_GtAvn.GtAvn_Rem, data.getParameters().getString("GtAvn_Rem", ""));
    rg.MoverA(g_Img_GtAvn.GtAvn_Vsg, data.getParameters().getString("GtAvn_Vsg", ""));
    rg.MoverA(g_Img_GtAvn.GtAvn_Dpy, "");
    rg.MoverA(g_Img_GtAvn.GtAvn_Occ, "");
    rg.MoverA(g_Img_GtAvn.GtAvn_Opp, "");
    String Ppv = data.getParameters().getString("GtEao_Ppv", "");
    String Oep = data.getParameters().getString("GtEao_Oep", "");
    String Oev = data.getParameters().getString("GtEao_Oev", "");
    String Eap = data.getParameters().getString("GtEao_Eap", "");
    String Eav = data.getParameters().getString("GtEao_Eav", "");
    String Epp = data.getParameters().getString("GtEao_Epp", "");
    String Epv = data.getParameters().getString("GtEao_Epv", "");
    int p = 0;
    int q = 0;
    for (int i=0;i<15;i++)
        {
          GT_IMG.Buf_Img_GtEao l_Img_GtEao = new GT_IMG.Buf_Img_GtEao();
          rg.MoverA(l_Img_GtEao.GtEao_Ppv, Ppv.substring(p, p+9));
          rg.MoverA(l_Img_GtEao.GtEao_Oep, Oep.substring(q, q+5));
          rg.MoverA(l_Img_GtEao.GtEao_Oev, Oev.substring(p, p+9));
          rg.MoverA(l_Img_GtEao.GtEao_Eap, Eap.substring(q, q+5));
          rg.MoverA(l_Img_GtEao.GtEao_Eav, Eav.substring(p, p+9));
          rg.MoverA(l_Img_GtEao.GtEao_Epp, Epp.substring(q, q+5));
          rg.MoverA(l_Img_GtEao.GtEao_Epv, Epv.substring(p, p+9));
          g_Img_GtEao.vGtEao_Ppv.set(i, l_Img_GtEao.GtEao_Ppv);
          g_Img_GtEao.vGtEao_Oep.set(i, l_Img_GtEao.GtEao_Oep);
          g_Img_GtEao.vGtEao_Oev.set(i, l_Img_GtEao.GtEao_Oev);
          g_Img_GtEao.vGtEao_Eap.set(i, l_Img_GtEao.GtEao_Eap);
          g_Img_GtEao.vGtEao_Eav.set(i, l_Img_GtEao.GtEao_Eav);
          g_Img_GtEao.vGtEao_Epp.set(i, l_Img_GtEao.GtEao_Epp);
          g_Img_GtEao.vGtEao_Epv.set(i, l_Img_GtEao.GtEao_Epv);
          p = p + 9;
          q = q + 5;
        }
  }
  //---------------------------------------------------------------------------------------
  public void Recalcula_GtApe(RunData data, Context context)
              throws Exception 
  {
    rg.MoverA(g_Img_GtApe.GtApe_Vtb, g_Img_GtApe.GtApe_Vts);
    rg.MoverA(g_Img_GtApe.GtApe_Vcn, g_Img_GtApe.GtApe_Vts);
    rg.MoverA(g_Img_GtApe.GtApe_Tsd, g_Img_GtTsd.GtTsd_Tsd);
    rg.MoverA(g_Img_GtApe.GtApe_Fts, g_Img_GtTsd.GtTsd_Fts);
    rg.MoverA(g_Img_GtApe.GtApe_Fvt, rg.DateAdd(g_Img_GtTsd.GtTsd_Fts,"A",2));
    rg.MoverA(g_Img_GtTsd.GtTsd_Vts, g_Img_GtApe.GtApe_Vts);
    rg.MoverA(g_Img_GtTsd.GtTsd_Ibf, g_Img_GtTsd.GtTsd_Bof);
    rg.MoverA(g_Img_GtAvn.GtAvn_Vtt, g_Img_GtApe.GtApe_Vtt);
    rg.MoverA(g_Img_GtAvn.GtAvn_Vts, g_Img_GtApe.GtApe_Vts);
    rg.MoverA(g_Img_GtAvn.GtAvn_Vrt, g_Img_GtAvn.GtAvn_Vts);
    rg.MoverA(g_Img_GtAvn.GtAvn_Vlq, g_Img_GtAvn.GtAvn_Vts);
    rg.MoverA(g_Img_GtAvn.GtAvn_Vcn, g_Img_GtAvn.GtAvn_Vts);
    //Call Graba_Imagen(fcIdrAcc, flIndex);
  }
  //---------------------------------------------------------------------------------------
  public Vector A_Tab_Img(RunData data)
  {
    Vector v_Img = new Vector();
    v_Img.clear();
    rg.MoverA(g_Img.Img_Ntr, g_PrmPC080.PC080_Ntr);
    //--------------------------------------------------------------------------
    rg.MoverA(g_Img.Img_Dax, "BASE");
    rg.MoverA(g_Img.Img_Seq, "000");
    rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtApe(g_Img_GtApe));
    Vector vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
    v_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
    //--------------------------------------------------------------------------
    rg.MoverA(g_Img.Img_Dax, "XTSD");
    rg.MoverA(g_Img.Img_Seq, "000");
    rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtTsd(g_Img_GtTsd));
    vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
    v_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
    //--------------------------------------------------------------------------
    rg.MoverA(g_Img.Img_Dax, "XYAVN");
    rg.MoverA(g_Img.Img_Seq, "000");
    rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtAvn(g_Img_GtAvn));
    vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
    v_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
    //--------------------------------------------------------------------------
    rg.MoverA(g_Img.Img_Dax, "XYEAO");
    rg.MoverA(g_Img.Img_Seq, "001");
    rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtEao(g_Img_GtEao));
    vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
    v_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
    //--------------------------------------------------------------------------
    return v_Img;
  }
//---------------------------------------------------------------------------------------
  public void doAgtp661_Continue(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP661[doAgtp661_Continue.start]", "[" + data.getUser().getUserName() + "]");
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Appp030"))
       {
         g_PrmPP030 = PRMPP030.LSet_A_PrmPP030(g_Msg.Msg_Dat.toString());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp661_g_PrmPC080");
         data.getUser().removeTemp("Agtp661_g_PrmPC080");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         if (g_PrmPP030.PP030_Rsp.toString().trim().equals("TERMINAR"))
            {
              data.getUser().removeTemp("Agtp661_MsgED135");
              data.getUser().removeTemp("Agtp661_ImgGtEao");
              data.getUser().removeTemp("Agtp661_ImgGtAvn");
              data.getUser().removeTemp("Agtp661_ImgGtTsd");
              data.getUser().removeTemp("Agtp661_ImgBase");
              data.getUser().removeTemp("Agtp661_ImgGtApe");
              data.getUser().removeTemp("Agtp661_PrmPC080");
              BF_MSG.Return_Data("Agtp661", data, g_Msg);
              return;
            }
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP661.vm" );
         return;
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Aedp210"))
       {
         g_PrmED210 = PRMED210.LSet_A_PrmED210(g_Msg.Msg_Dat.toString());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp661_g_PrmPC080");
         data.getUser().removeTemp("Agtp661_g_PrmPC080");
         if (g_PrmED210.ED210_Rtn.toString().equals("NK")
          || g_PrmED210.ED210_Rtn.toString().equals("YA"))
            {
              rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
              BF_MSG.Param_Program(data, g_Msg);
              setTemplate(data, "Garantias,Agt,AGTP661.vm" );
              return;
            }
         else
            {
              g_Img_GtTsd = (GT_IMG.Buf_Img_GtTsd)data.getUser().getTemp("Agtp661_ImgGtTsd");
              g_MsgED135 = MSGED135.Inicia_MsgED135();
              rg.MoverA(g_MsgED135.ED135_Idr, "X");
              rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
              rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
              rg.MoverA(g_MsgED135.ED135_Est, "");
              rg.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
              rg.MoverA(g_MsgED135.ED135_Ldv, g_PrmED210.ED210_Ldt);
              rg.MoverA(g_MsgED135.ED135_Ezt, "RETSD");
              rg.MoverA(g_MsgED135.ED135_Exi, "TSD");
              rg.MoverA(g_MsgED135.ED135_Exn, rg.FmtValor(g_Img_GtTsd.GtTsd_Tsd,0,0,12,"+"));
              rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
              g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
              data.getUser().removeTemp("Agtp661_MsgED135");
              data.getUser().removeTemp("Agtp661_ImgGtEao");
              data.getUser().removeTemp("Agtp661_ImgGtAvn");
              data.getUser().removeTemp("Agtp661_ImgGtTsd");
              data.getUser().removeTemp("Agtp661_ImgBase");
              data.getUser().removeTemp("Agtp661_ImgGtApe");
              data.getUser().removeTemp("Agtp661_PrmPC080");
              rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
              rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
              BF_MSG.Return_Data("Agtp661", data, g_Msg);
              return;
            }
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Agtp459")
    // || rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Agtp729"))
     || rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Appp096"))
       {
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp661_g_PrmPC080");
         data.getUser().removeTemp("Agtp661_g_PrmPC080");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP661.vm" );
         return;
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Agtp291"))
       {
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp661_g_PrmPC080");
         data.getUser().removeTemp("Agtp661_g_PrmPC080");
         data.getUser().removeTemp("Agtp661_MsgED135");
         data.getUser().removeTemp("Agtp661_ImgGtEao");
         data.getUser().removeTemp("Agtp661_ImgGtAvn");
         data.getUser().removeTemp("Agtp661_ImgGtTsd");
         data.getUser().removeTemp("Agtp661_ImgBase");
         data.getUser().removeTemp("Agtp661_ImgGtApe");
         data.getUser().removeTemp("Agtp661_PrmPC080");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp661", data, g_Msg);
         return;
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Agtp093"))
       {
         g_PrmGT093 = PRMGT093.LSet_A_PrmGT093(g_Msg.Msg_Dat.toString());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp661_g_PrmPC080");
         data.getUser().removeTemp("Agtp661_g_PrmPC080");
         if (g_PrmGT093.GT093_Rtn.toString().trim().equals("OK"))
             {
               g_Img_GtAvn = (GT_IMG.Buf_Img_GtAvn)data.getUser().getTemp("Agtp661_ImgGtAvn");
               rg.MoverA(g_Img_GtAvn.GtAvn_Dpc, g_PrmGT093.GT093_Dpc);
               rg.MoverA(g_Img_GtAvn.GtAvn_Pct, rg.FmtValor(g_PrmGT093.GT093_Pct,0,2,5,"+"));
               data.getUser().removeTemp("Agtp661_ImgGtAvn");
               data.getUser().setTemp("Agtp661_ImgGtAvn", g_Img_GtAvn);
             }
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP661.vm" );
         return;
       }
  }
  //---------------------------------------------------------------------------------------
  public boolean PagoTasador(RunData data, Context context)
              throws Exception 
  {
    g_Img       = img.Inicia_Img();
    rg.MoverA(g_Img.Img_Ntr, Asigna_Folio("TRN-NTR", data, context));
    rg.MoverA(g_Img.Img_Dax, "BASE");
    rg.MoverA(g_Img.Img_Seq, "000");
    //------------------------------------------------------------------------------
    g_Img_Base = img.LSet_A_ImgBase(g_Img_GtApe.GtApe_Bse.toString());
    rg.MoverA(g_Img_Base.Img_Base_Ntr, g_Img.Img_Ntr);
    rg.MoverA(g_Img_Base.Img_Base_Ttr, "PGTSD");
    rg.MoverA(g_Img_Base.Img_Base_Ntt, "PAGO A TASADORES");
    //------------------------------------------------------------------------------
    rg.MoverA(g_Trn.Trn_Ntr, g_Img.Img_Ntr);
    rg.MoverA(g_Trn.Trn_Trm, "PCVB");
    rg.MoverA(g_Trn.Trn_Fem, g_Msg.Msg_Fch);
    rg.MoverA(g_Trn.Trn_Hrm, rg.Zeros(6));
    rg.MoverA(g_Trn.Trn_Ttr, g_Img_Base.Img_Base_Ttr);
    rg.MoverA(g_Trn.Trn_Cli, g_Img_Base.Img_Base_Cli);
    rg.MoverA(g_Trn.Trn_Cnr, g_Img_Base.Img_Base_Sis.toString() + g_Img_Base.Img_Base_Ncn.toString());
    rg.MoverA(g_Trn.Trn_Suc, g_Img_Base.Img_Base_Suc);
    rg.MoverA(g_Trn.Trn_Eje, g_Img_Base.Img_Base_Eje);
    rg.MoverA(g_Trn.Trn_Est, "ENPRO");
    rg.MoverA(g_Trn.Trn_Usr, g_Msg.Msg_Cus);
    rg.MoverA(g_Trn.Trn_Cja, rg.Zeros(4));
    rg.MoverA(g_Trn.Trn_Stf, rg.Zeros(9));
    rg.MoverA(g_Trn.Trn_Dcn, g_Img_Base.Img_Base_Dcn);
    rg.MoverA(g_Trn.Trn_Tmn, g_Img_Base.Img_Base_Tmn);
    rg.MoverA(g_Trn.Trn_Trj, g_Img_Base.Img_Base_Trj);
    rg.MoverA(g_Trn.Trn_Dcm, g_Img_Base.Img_Base_Dcm);
    rg.MoverA(g_Trn.Trn_Mnd, g_Img_Base.Img_Base_Mnd);
    rg.MoverA(g_Trn.Trn_Mnt, rg.Zeros(15));
    //------------------------------------------------------------------------------
    g_Img_GtPts = gtimg.LSet_A_ImgGtPts(g_Img.Img_Dat.toString());
    rg.MoverA(g_Img_GtPts.GtPts_Bse, img.LSet_De_ImgBase_Bse(g_Img_Base));
    rg.MoverA(g_Img_GtPts.GtPts_Fec, g_Msg.Msg_Fch);
    rg.MoverA(g_Img_GtPts.GtPts_Cbp, g_Img_GtApe.GtApe_Cbp);
    rg.MoverA(g_Img_GtPts.GtPts_Dbp, g_Img_GtApe.GtApe_Dbp);
    rg.MoverA(g_Img_GtPts.GtPts_Gts, g_Img_GtTsd.GtTsd_Vst);
    rg.MoverA(g_Img_GtPts.GtPts_Hon, g_Img_GtTsd.GtTsd_Hon);
    rg.MoverA(g_Img_GtPts.GtPts_Pcb, g_Img_GtTsd.GtTsd_Pcb);
    rg.MoverA(g_Img_GtPts.GtPts_Cct, g_Img_GtTsd.GtTsd_Cct);
    rg.MoverA(g_Img_GtPts.GtPts_Bof, g_Img_GtTsd.GtTsd_Bof);
    rg.MoverA(g_Img_GtPts.GtPts_Nbf, g_Img_GtTsd.GtTsd_Nbf);
    rg.MoverA(g_Img_GtPts.GtPts_Nts, g_Img_GtTsd.GtTsd_Nts);
    rg.MoverA(g_Img_GtPts.GtPts_Rts, g_Img_GtTsd.GtTsd_Rts);
    rg.MoverA(g_Img_GtPts.GtPts_Dpc, g_Img_GtAvn.GtAvn_Dpc);
    rg.MoverA(g_Img_GtPts.GtPts_Fll, "");
    rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtPts(g_Img_GtPts));
    //------------------------------------------------------------------------------
    rg.MoverA(g_MsgTxCur.TxCur_Bco, "DEMO");                                //"BICE"
    rg.MoverA(g_MsgTxCur.TxCur_Cic, "NO");
    rg.MoverA(g_MsgTxCur.TxCur_Lin, " ");
    rg.MoverA(g_MsgTxCur.TxCur_Rsp, " ");
    rg.MoverA(g_MsgTxCur.TxCur_Idc, "00");
    rg.MoverA(g_MsgTxCur.TxCur_Stp, " ");
    rg.MoverA(g_MsgTxCur.TxCur_Imr, " ");
    rg.MoverA(g_MsgTxCur.TxCur_Msg, " ");
    rg.MoverA(g_MsgTxCur.TxCur_Trn, BF_TRN.LSet_De_Trn(g_Trn));
    rg.MoverA(g_MsgTxCur.TxCur_Img, img.LSet_De_Img(g_Img));
    rg.MoverA(g_Msg.Msg_Dat, MSGTXCUR.LSet_De_MsgTxCur(g_MsgTxCur));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GTCUR");
    if (g_Msg.Msg_Rtn.toString().compareTo("0")!=0)
       { return false; }
    g_MsgTxCur = MSGTXCUR.LSet_A_MsgTxCur(g_Msg.Msg_Dat.toString());
    return true;
  }
  //---------------------------------------------------------------------------------------
  public String Asigna_Folio(String pcFolio, RunData data, Context context)
                throws Exception 
  {
    g_MsgED090 = MSGED090.Inicia_MsgED090();
    rg.MoverA(g_MsgED090.ED090_Idr, "F");
    rg.MoverA(g_MsgED090.ED090_Cod, pcFolio);
    rg.MoverA(g_Msg.Msg_Dat, MSGED090.LSet_De_MsgED090(g_MsgED090));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED090");
    g_MsgED090 = MSGED090.LSet_A_MsgED090(g_Msg.Msg_Dat.toString());
    String lcFolio = g_MsgED090.ED090_Fol.toString();
    return lcFolio;
  }
  //---------------------------------------------------------------------------------------
}