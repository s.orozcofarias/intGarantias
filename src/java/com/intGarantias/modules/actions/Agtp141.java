// Source File Name:   Agtp141.java
// Descripcion     :   Impresion Autoriza Tasar (-)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.BF_IMG;
import com.FHTServlet.modules.global.PRMPC080;

import com.intGarantias.modules.global.GT_IMG;

import java.util.*;
import org.apache.turbine.util.*;
import org.apache.velocity.context.Context;

public class Agtp141 extends FHTServletAction
{
  //---------------------------------------------------------------------------------------
  public int i = 0;
  public String lcData = "";
  Vector v_Img_GtMff               = new Vector();
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  BF_IMG img                       = new BF_IMG();
  BF_IMG.Buf_Img g_Img             = new BF_IMG.Buf_Img();
  BF_IMG.Buf_Img_Bse g_Img_Base    = new BF_IMG.Buf_Img_Bse();
  GT_IMG gtimg                     = new GT_IMG();
  GT_IMG.Buf_Img_GtApe g_Img_GtApe = new GT_IMG.Buf_Img_GtApe();
  GT_IMG.Buf_Img_GtTsd g_Img_GtTsd = new GT_IMG.Buf_Img_GtTsd();
  GT_IMG.Buf_Img_GtMff g_Img_GtMff = new GT_IMG.Buf_Img_GtMff();
  PRMPC080.PrmPC080 g_PrmPC080     = new PRMPC080.PrmPC080();
  //---------------------------------------------------------------------------------------
  public Agtp141()
  {
  }
  //---------------------------------------------------------------------------------------
  public void doPerform(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP141[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp141-DIN", data);
    doAgtp141_Init(data, context);
  }
  //---------------------------------------------------------------------------------------
  public void doCancel(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP141[doCancel.start]", "[CANCELADO]");
    setTemplate(data, "Control,AGTP141.vm");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp141_Init(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP141[doAgtp141_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    Carga_de_Host(data, context);
    setTemplate(data, "garantias,Agt,AGTP141.vm" );
  }
  //---------------------------------------------------------------------------------------
  public void Carga_de_Host(RunData data, Context context)
              throws Exception 
  {
    Vector g_Tab_Img = img.Carga_Img_Host(g_Msg, g_PrmPC080.PC080_Ntr.toString(), data, context);
    De_Tab_Img(g_Tab_Img, data);
    data.getUser().removeTemp("Agtp141_ImgBase");
    data.getUser().setTemp("Agtp141_ImgBase", g_Img_Base);
    data.getUser().removeTemp("Agtp141_ImgGtApe");
    data.getUser().setTemp("Agtp141_ImgGtApe", g_Img_GtApe);
    data.getUser().removeTemp("Agtp141_ImgGtTsd");
    data.getUser().setTemp("Agtp141_ImgGtTsd", g_Img_GtTsd);
    data.getUser().removeTemp("Agtp141_vImgGtMff");
    data.getUser().setTemp("Agtp141_vImgGtMff", v_Img_GtMff);
  }
  //---------------------------------------------------------------------------------------
  public void De_Tab_Img(Vector g_Tab_Img, RunData data)
  {
    g_Img       = img.Inicia_Img();
    lcData      = img.LSet_De_Img(g_Img);
    g_Img_Base  = img.LSet_A_ImgBase(lcData);
    g_Img_GtApe = gtimg.LSet_A_ImgGtApe(g_Img.Img_Dat.toString());
    g_Img_GtTsd = gtimg.LSet_A_ImgGtTsd(g_Img.Img_Dat.toString());
    v_Img_GtMff.clear();
    for (i=0; i<g_Tab_Img.size(); i++)
        {
          lcData = img.LSet_De_Img((BF_IMG.Buf_Img)g_Tab_Img.elementAt(i));
          g_Img  = img.LSet_A_Img(lcData);
          if (g_Img.Img_Dax.toString().trim().equals("BASE"))
             {
               g_Img_GtApe = gtimg.LSet_A_ImgGtApe(g_Img.Img_Dat.toString());
               g_Img_Base  = img.LSet_A_ImgBase(g_Img_GtApe.GtApe_Bse.toString());
             }
          if (g_Img.Img_Dax.toString().trim().equals("XTSD"))
             { g_Img_GtTsd = gtimg.LSet_A_ImgGtTsd(g_Img.Img_Dat.toString()); }
          if (g_Img.Img_Dax.toString().trim().equals("MFF"))
             {
               g_Img_GtMff = gtimg.LSet_A_ImgGtMff(g_Img.Img_Dat.toString());
               v_Img_GtMff.add (g_Img_GtMff); 
             }
        }
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp141(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP141[doAgtp141.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp141-MAN", data);
    String Opc = data.getParameters().getString("Opc", "");	
    if (Opc.trim().equals("S"))                    //Boton Salir
       { 
         data.getUser().removeTemp("Agtp141_ImgBase");
         data.getUser().removeTemp("Agtp141_ImgGtApe");
         data.getUser().removeTemp("Agtp141_ImgGtTsd");
         data.getUser().removeTemp("Agtp141_vImgGtMff");
         BF_MSG.Return_Data("Agtp141", data, g_Msg);
       }
  }
  //---------------------------------------------------------------------------------------
}