// Source File Name:   Agtp415.java
// Descripcion     :   Consulta Detalle Valores (GT415)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.PRMPC080;

import com.intGarantias.modules.global.MSGGT415;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp415 extends FHTServletAction
{
  //---------------------------------------------------------------------------------------
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  PRMPC080.PrmPC080 g_PrmPC080     = new PRMPC080.PrmPC080();
  MSGGT415.Buf_MsgGT415 g_MsgGT415 = new MSGGT415.Buf_MsgGT415();
  //---------------------------------------------------------------------------------------
  public Agtp415()
  {
  }
  //---------------------------------------------------------------------------------------
  public void doPerform(RunData data, Context context)
              throws Exception
  {
  //Log.debug("AGTP415[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp415-DIN", data);
    doAgtp415_Init(data, context);
  }
  //---------------------------------------------------------------------------------------
  public void doCancel(RunData data, Context context)
              throws Exception
  {
  //Log.debug("AGTP415[doCancel.start]", "[CANCELADO]");
    setTemplate(data, "Garantias,Agt,AGTP415.vm");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp415_Init(RunData data, Context context)
              throws Exception
  {
  //Log.debug("AGTP415[doAgtp415_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    g_MsgGT415 = MSGGT415.Inicia_MsgGT415();
    rg.MoverA(g_MsgGT415.GT415_Idr, "I");
    rg.MoverA(g_MsgGT415.GT415_Gti, g_PrmPC080.PC080_Cnr.Sis.toString() + g_PrmPC080.PC080_Cnr.Ncn.toString());
    rg.MoverA(g_MsgGT415.GT415_Seq, g_PrmPC080.PC080_Ntc);
    rg.MoverA(g_Msg.Msg_Dat, MSGGT415.LSet_De_MsgGT415(g_MsgGT415));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GT415");
    g_MsgGT415 = MSGGT415.LSet_A_MsgGT415(g_Msg.Msg_Dat.toString());
    data.getUser().removeTemp("Agtp415_MsgGT415");
    data.getUser().setTemp("Agtp415_MsgGT415", g_MsgGT415);
    data.getUser().removeTemp("Agtp415_PrmPC080");
    data.getUser().setTemp("Agtp415_PrmPC080", g_PrmPC080);					
    setTemplate(data, "Garantias,Agt,AGTP415.vm" );
  //Log.debug("AGTP415[doAgtp415_Init.end]", "[" + data.getUser().getUserName() + "]");
  //---------------------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp415(RunData data, Context context)
              throws Exception
  {
  //Log.debug("AGTP415[doAgtp415.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp415-MAN", data);
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    String Opc = data.getParameters().getString("Opc" ,"");
    if (Opc.trim().equals("S"))
       {
       //Boton Salir
         data.getUser().removeTemp("Agtp415_MsgGT415");
         data.getUser().removeTemp("Agtp415_PrmPC080");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Program(data, g_Msg);
         return;
       }
  }
  //---------------------------------------------------------------------------------------
}