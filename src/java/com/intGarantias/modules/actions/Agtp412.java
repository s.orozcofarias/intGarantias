// Source File Name:   Agtp412.java
// Descripcion     :   Consulta Detalle Prenda (GT412)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.BF_LOB;
import com.FHTServlet.modules.global.PRMPC080;

import com.intGarantias.modules.global.MSGGT412;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp412 extends FHTServletAction
{
  //---------------------------------------------------------------------------------------
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  BF_LOB lob                       = new BF_LOB();
  PRMPC080.PrmPC080 g_PrmPC080     = new PRMPC080.PrmPC080();
  MSGGT412.Buf_MsgGT412 g_MsgGT412 = new MSGGT412.Buf_MsgGT412();
  //---------------------------------------------------------------------------------------
  public Agtp412()
  {
  }
  //---------------------------------------------------------------------------------------
  public void doPerform(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP412[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp412-DIN", data);
    doAgtp412_Init(data, context);
  }
  //---------------------------------------------------------------------------------------
  public void doCancel(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP412[doCancel.start]", "[CANCELADO]");
    setTemplate(data, "Garantias,Agt,AGTP412.vm");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp412_Init(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP412[doAgtp412_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    g_MsgGT412 = MSGGT412.Inicia_MsgGT412();
    rg.MoverA(g_MsgGT412.GT412_Idr, "I");
    rg.MoverA(g_MsgGT412.GT412_Gti, g_PrmPC080.PC080_Cnr.Sis.toString() + g_PrmPC080.PC080_Cnr.Ncn.toString());
    rg.MoverA(g_MsgGT412.GT412_Seq, g_PrmPC080.PC080_Ntc);
    rg.MoverA(g_Msg.Msg_Dat, MSGGT412.LSet_De_MsgGT412(g_MsgGT412));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GT412");
    g_MsgGT412 = MSGGT412.LSet_A_MsgGT412(g_Msg.Msg_Dat.toString());
    data.getUser().removeTemp("Agtp412_MsgGT412");
    data.getUser().setTemp("Agtp412_MsgGT412", g_MsgGT412);
    if (g_MsgGT412.GT412_Gbs.GT412_Obs.toString().trim().equals("")
      ||g_MsgGT412.GT412_Gbs.GT412_Obs.toString().trim().equals("000000000"))
       { data.getUser().setTemp("Agtp412_Obs", ""); }
    else
       { data.getUser().setTemp("Agtp412_Obs", lob.leeCLOB(g_MsgGT412.GT412_Gbs.GT412_Obs.toString())); }
    data.getUser().removeTemp("Agtp412_PrmPC080");
    data.getUser().setTemp("Agtp412_PrmPC080", g_PrmPC080);					
    setTemplate(data, "Garantias,Agt,AGTP412.vm" );
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp412(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP412[doAgtp412.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp412-MAN", data);
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    String Opc = data.getParameters().getString("Opc" ,"");
    if (Opc.trim().equals("S"))
       {
         //Boton Salir
         data.getUser().removeTemp("Agtp412_MsgGT412");
         data.getUser().removeTemp("Agtp412_PrmPC080");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Program(data, g_Msg);
         return;
       }
  }
  //---------------------------------------------------------------------------------------
}