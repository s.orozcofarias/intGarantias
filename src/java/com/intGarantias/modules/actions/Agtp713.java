// Source File Name:   Agtp713.java
// Descripcion:        Solo Despligue Panel para Solicitar Cambio de Fecha

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;

import java.util.*;
import org.apache.turbine.util.*;
import org.apache.velocity.context.Context;

public class Agtp713 extends FHTServletAction
{
  //---------------------------------------------------------------------------------------
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  //---------------------------------------------------------------------------------------
  public Agtp713()
  {
  }
  //---------------------------------------------------------------------------------------
  public void doPerform(RunData data, Context context)
              throws Exception
  {
  //Log.debug("AGTP713[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp713-DIN", data);
    doAgtp713_Init(data, context);
  }
  //---------------------------------------------------------------------------------------
  public void doCancel(RunData data, Context context)
              throws Exception
  {
  //Log.debug("AGTP713[doCancel.start]", "[CANCELADO]");
    setTemplate(data, "Garantias,Agt,AGTP713.vm");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp713_Init(RunData data, Context context)
              throws Exception
  {
  //Log.debug("AGTP713[doAgtp713_Init.start]", "[" + data.getUser().getUserName() + "]");
    setTemplate(data, "Garantias,Agt,AGTP713.vm");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp713(RunData data, Context context)
              throws Exception
  {
  //Log.debug("AGTP713[doAgtp713.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp713-MAN", data);
  }
  //---------------------------------------------------------------------------------------
}