// Source File Name:   Agtp625.java
// Descripcion     :   Seguros (ED135, ED500, ED535, GT625)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.BF_IMG;
import com.FHTServlet.modules.global.BF_EVT;
import com.FHTServlet.modules.global.PRMPC080;

import com.intGlobal.modules.global.MSGED100;
import com.intGlobal.modules.global.MSGED135;
import com.intGlobal.modules.global.PRMED210;
import com.intGlobal.modules.global.PRMCL090;
import com.intGlobal.modules.global.PRMTG114;

import com.intGarantias.modules.global.MSGGT625;
import com.intGarantias.modules.global.GT_IMG;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp625 extends FHTServletAction
{
  //---------------------------------------------------------------------------------------
  public int i = 0;
  public String lcData = "";
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  BF_EVT.Buf_Evt g_Evt             = new BF_EVT.Buf_Evt();
  BF_IMG img                       = new BF_IMG();
  BF_IMG.Buf_Img g_Img             = new BF_IMG.Buf_Img();
  BF_IMG.Buf_Img_Bse g_Img_Base    = new BF_IMG.Buf_Img_Bse();
  GT_IMG gtimg                     = new GT_IMG();
  GT_IMG.Buf_Img_GtSgr g_Img_GtSgr = new GT_IMG.Buf_Img_GtSgr();
  GT_IMG.Buf_Img_GtSgd g_Img_GtSgd = new GT_IMG.Buf_Img_GtSgd();
  Vector v_Img_GtSgd               = new Vector();
  PRMPC080.PrmPC080 g_PrmPC080     = new PRMPC080.PrmPC080();
  MSGED100.Buf_MsgED100 g_MsgED100 = new MSGED100.Buf_MsgED100();
  MSGED135.Buf_MsgED135 g_MsgED135 = new MSGED135.Buf_MsgED135();
  PRMED210.Buf_PrmED210 g_PrmED210 = new PRMED210.Buf_PrmED210();
  PRMCL090.Buf_PrmCL090 g_PrmCL090 = new PRMCL090.Buf_PrmCL090();
  MSGGT625.Buf_MsgGT625 g_MsgGT625 = new MSGGT625.Buf_MsgGT625();
  PRMTG114.Buf_PrmTG114 g_PrmTG114 = new PRMTG114.Buf_PrmTG114();
  //---------------------------------------------------------------------------------------
  public Agtp625()
  {
  }
  //---------------------------------------------------------------------------------------
  public void doPerform(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP625[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp625-DIN", data);
    if (g_Msg.Msg_Pgr.toString().equals("RT"))
       { doAgtp625_Continue(data, context); }
    else
       { doAgtp625_Init(data, context); }
  }
  //---------------------------------------------------------------------------------------
  public void doCancel(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP625[doCancel.start]", "[CANCELADO]");
    setTemplate(data, "Garantias,Agt,AGTP625.vm");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp625_Init(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP625[doAgtp625_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (g_PrmPC080.PC080_Cnr.Ncn.toString().trim().equals("")
     && g_PrmPC080.PC080_Cli.toString().trim().equals(""))
       {
         data.getUser().removeTemp("Agtp625_g_PrmPC080");
         data.getUser().setTemp("Agtp625_g_PrmPC080" ,g_PrmPC080);
         g_PrmCL090 = PRMCL090.Inicia_PrmCL090();
         rg.MoverA(g_PrmCL090.CL090_Pcc, "N");
         rg.MoverA(g_Msg.Msg_Dat, PRMCL090.LSet_De_PrmCL090(g_PrmCL090));
         BF_MSG.Link_Program("Agtp625", "Eclp090", data, g_Msg);
         return;
       }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    g_MsgED135 = MSGED135.Inicia_MsgED135();
    if (g_PrmPC080.PC080_Ntr.toString().trim().compareTo("")!=0)
       {
         Recupera_Evt(data, context);
         Carga_de_Host(data, context);
       }
    else
       {
         if (Carga_Inicial(data, context) == 1)
            { return; }
         Init_Evt_GRT(data, context);
       }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    data.getUser().removeTemp("Agtp625_MsgED135");
    data.getUser().setTemp("Agtp625_MsgED135", g_MsgED135);
    data.getUser().removeTemp("Agtp625_Evt");
    data.getUser().setTemp("Agtp625_Evt", g_Evt);
    data.getUser().removeTemp("Agtp625_ImgBase");
    data.getUser().setTemp("Agtp625_ImgBase", g_Img_Base);
    data.getUser().removeTemp("Agtp625_ImgGtSgr");
    data.getUser().setTemp("Agtp625_ImgGtSgr", g_Img_GtSgr);
    data.getUser().removeTemp("Agtp625_vImgGtSgd");
    data.getUser().setTemp("Agtp625_vImgGtSgd", v_Img_GtSgd);
    rg.MoverA(g_PrmPC080.PC080_Cli, g_Evt.Evt_Cli);
    rg.MoverA(g_PrmPC080.PC080_Ncl, g_Evt.Evt_Ncl);
    rg.MoverA(g_PrmPC080.PC080_Dir, g_Evt.Evt_Dir);
    rg.MoverA(g_PrmPC080.PC080_Tfc, g_Evt.Evt_Tfc);
    rg.MoverA(g_PrmPC080.PC080_Dcn, g_Evt.Evt_Dcn);
    rg.MoverA(g_PrmPC080.PC080_Ndc, g_Evt.Evt_Ndc);
    rg.MoverA(g_PrmPC080.PC080_Ntt, g_Evt.Evt_Ntt);
    data.getUser().removeTemp("Agtp625_PrmPC080");
    data.getUser().setTemp("Agtp625_PrmPC080", g_PrmPC080);
    rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
    BF_MSG.Param_Program(data, g_Msg);
    setTemplate(data, "Garantias,Agt,AGTP625.vm" );
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp625(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP625[doAgtp625.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp625-MAN", data);
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    String Opc = data.getParameters().getString("Opc", "");
    if (Opc.trim().equals("S"))
       {
         //Boton Salir
         Limpieza(data, context);
         rg.MoverA(g_PrmPC080.PC080_Rtn, "NK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp625", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("R"))
       {
         //Boton Errores
         g_MsgED135 = (MSGED135.Buf_MsgED135)data.getUser().getTemp("Agtp625_MsgED135");
         g_PrmED210 = PRMED210.Inicia_PrmED210();
         rg.MoverA(g_PrmED210.ED210_Rtn, "RO");
         rg.MoverA(g_PrmED210.ED210_Ldt, g_MsgED135.ED135_Ldv);
         data.getUser().removeTemp("Agtp625_g_PrmPC080");
         data.getUser().setTemp("Agtp625_g_PrmPC080" ,g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMED210.LSet_De_PrmED210(g_PrmED210));
         BF_MSG.Link_Program("Agtp625", "Aedp210", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("A"))
       {
         //Boton Aceptar Operador (Tramite Unico)
         g_Img_GtSgr = (GT_IMG.Buf_Img_GtSgr)data.getUser().getTemp("Agtp625_ImgGtSgr");
         v_Img_GtSgd = (Vector)data.getUser().getTemp("Agtp625_vImgGtSgd");
         rg.MoverA(g_Img_GtSgr.GtSgr_Fig, g_Msg.Msg_Fch);
         rg.MoverA(g_Img_GtSgr.GtSgr_Psw, " ");
         //Valida_Host("V", data, context);
         Vector g_Tab_Img = A_Tab_Img(data);
         img.Graba_Img_Host(g_Msg, g_Tab_Img, data, context);
         Cursatura("U", g_Tab_Img, data, context);
         //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
         g_MsgED135 = MSGED135.Inicia_MsgED135();
         rg.MoverA(g_MsgED135.ED135_Idr, "M");
         rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
         rg.MoverA(g_MsgED135.ED135_Trt, "UNICO");
         rg.MoverA(g_MsgED135.ED135_Est, "");
         rg.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
         rg.MoverA(g_MsgED135.ED135_Ezt, "ENCUR");
         //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
         rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
         g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
         Limpieza(data, context);
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp625", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("M"))
       {
         //Boton Modificar
         String Seq = data.getParameters().getString("Seq", "");
         i = Integer.parseInt(Seq);
         g_Img_GtSgr = (GT_IMG.Buf_Img_GtSgr)data.getUser().getTemp("Agtp625_ImgGtSgr");
         g_Img_Base  = img.LSet_A_ImgBase(g_Img_GtSgr.GtSgr_Bse.toString());
         v_Img_GtSgd = (Vector)data.getUser().getTemp("Agtp625_vImgGtSgd");
         rg.MoverA(g_Img_GtSgr.GtSgr_Idx, rg.Suma(Seq,0,"1",0,3,0));
         g_Img_GtSgd = (GT_IMG.Buf_Img_GtSgd)v_Img_GtSgd.elementAt(i);
         data.getUser().removeTemp("Agtp625_ImgGtSgr");
         data.getUser().setTemp("Agtp625_ImgGtSgr", g_Img_GtSgr);
         data.getUser().removeTemp("Agtp625_ImgGtSgd");
         data.getUser().setTemp("Agtp625_ImgGtSgd", g_Img_GtSgd);
         setTemplate(data, "Garantias,Agt,AGTP625.vm" );
         return;
       }
    if (Opc.trim().equals("T"))
       {
         //Boton Modificar Todos
         g_Img_GtSgr = (GT_IMG.Buf_Img_GtSgr)data.getUser().getTemp("Agtp625_ImgGtSgr");
         g_Img_Base  = img.LSet_A_ImgBase(g_Img_GtSgr.GtSgr_Bse.toString());
         rg.MoverA(g_Img_GtSgr.GtSgr_Psw, "T");
         GT_IMG.Buf_Img_GtSgd t_Img_GtSgd = new GT_IMG.Buf_Img_GtSgd();
         rg.MoverA(t_Img_GtSgd.GtSgd_Vsu, g_Img_GtSgr.GtSgr_Vsu);
         rg.MoverA(t_Img_GtSgd.GtSgd_Umt, g_Img_GtSgr.GtSgr_Umt);
         rg.MoverA(t_Img_GtSgd.GtSgd_Sum, g_Img_GtSgr.GtSgr_Umt);
         rg.MoverA(t_Img_GtSgd.GtSgd_Isg, "T");
         data.getUser().removeTemp("Agtp625_ImgGtSgr");
         data.getUser().setTemp("Agtp625_ImgGtSgr", g_Img_GtSgr);
         data.getUser().removeTemp("Agtp625_ImgGtSgd");
         data.getUser().setTemp("Agtp625_ImgGtSgd", t_Img_GtSgd);
         setTemplate(data, "Garantias,Agt,AGTP625.vm" );
         return;
       }
    if (Opc.trim().equals("1"))
       {
         //Boton Aseguradora
         g_Img_GtSgd = (GT_IMG.Buf_Img_GtSgd)data.getUser().getTemp("Agtp625_ImgGtSgd");
         Actualiza_Datos(data, context);
         data.getUser().removeTemp("Agtp625_ImgGtSgd");
         data.getUser().setTemp("Agtp625_ImgGtSgd", g_Img_GtSgd);
         g_PrmTG114 = PRMTG114.Inicia_PrmTG114();
         data.getUser().removeTemp("Agtp625_g_PrmPC080");
         data.getUser().setTemp("Agtp625_g_PrmPC080" ,g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMTG114.LSet_De_PrmTG114(g_PrmTG114));
         BF_MSG.Link_Program("Agtp625", "Atgp114", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("Y"))
       {
         //Boton Si
         g_Img_GtSgd = (GT_IMG.Buf_Img_GtSgd)data.getUser().getTemp("Agtp625_ImgGtSgd");
         g_Img_GtSgr = (GT_IMG.Buf_Img_GtSgr)data.getUser().getTemp("Agtp625_ImgGtSgr");
         v_Img_GtSgd = (Vector)data.getUser().getTemp("Agtp625_vImgGtSgd");
         //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
         String tGtSgd_Svl = g_Img_GtSgd.GtSgd_Svl.toString();
         Actualiza_Datos(data, context);
         if (g_Img_GtSgr.GtSgr_Psw.toString().trim().equals("T"))
            { Prorratea_Seguros(data, context); }
         else
            {
              i = Integer.parseInt(g_Img_GtSgr.GtSgr_Idx.toString()) - 1;
              GT_IMG.Buf_Img_GtSgd l_Img_GtSgd = new GT_IMG.Buf_Img_GtSgd();
              l_Img_GtSgd = (GT_IMG.Buf_Img_GtSgd)v_Img_GtSgd.elementAt(i);
              rg.MoverA(g_Img_GtSgr.GtSgr_Svl, rg.Resta(g_Img_GtSgr.GtSgr_Svl,4,tGtSgd_Svl,4,15,4));
              rg.MoverA(g_Img_GtSgr.GtSgr_Svl, rg.Suma(g_Img_GtSgr.GtSgr_Svl,4,g_Img_GtSgd.GtSgd_Svl,4,15,4));
              v_Img_GtSgd.set(i, g_Img_GtSgd); 
            }
         rg.MoverA(g_Img_GtSgr.GtSgr_Idx, " ");
         rg.MoverA(g_Img_GtSgr.GtSgr_Psw, "Y");
         //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
         data.getUser().removeTemp("Agtp625_ImgGtSgr");
         data.getUser().setTemp("Agtp625_ImgGtSgr", g_Img_GtSgr);
         data.getUser().removeTemp("Agtp625_ImgGtSgd");
         data.getUser().removeTemp("Agtp625_vImgGtSgd");
         data.getUser().setTemp("Agtp625_vImgGtSgd", v_Img_GtSgd);
         setTemplate(data, "Garantias,Agt,AGTP625.vm" );
         return;
       }
    if (Opc.trim().equals("N"))
       {
         //Boton No
         data.getUser().removeTemp("Agtp625_ImgGtSgd");
         g_Img_GtSgr = (GT_IMG.Buf_Img_GtSgr)data.getUser().getTemp("Agtp625_ImgGtSgr");
         rg.MoverA(g_Img_GtSgr.GtSgr_Psw, " ");
         rg.MoverA(g_Img_GtSgr.GtSgr_Idx, " ");
         data.getUser().removeTemp("Agtp625_ImgGtSgr");
         data.getUser().setTemp("Agtp625_ImgGtSgr", g_Img_GtSgr);
         setTemplate(data, "Garantias,Agt,AGTP625.vm" );
         return;
       }
  }
  //---------------------------------------------------------------------------------------
  public void Recupera_Evt(RunData data, Context context)
              throws Exception 
  {
    g_MsgED100 = MSGED100.Inicia_MsgED100();
    rg.MoverA(g_MsgED100.ED100_Idr, "Q");
    rg.MoverA(g_MsgED100.ED100_Nev, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_Msg.Msg_Dat, MSGED100.LSet_De_MsgED100(g_MsgED100));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED500");
    g_MsgED100 = MSGED100.LSet_A_MsgED100(g_Msg.Msg_Dat.toString());
    g_Evt = BF_EVT.LSet_A_Evt(g_MsgED100.ED100_Evt.toString());
  }
  //---------------------------------------------------------------------------------------
  public void Carga_de_Host(RunData data, Context context)
              throws Exception 
  {
    String lcSvc = "";
    Vector g_Tab_Img = img.Carga_Img_Host(g_Msg, g_PrmPC080.PC080_Ntr.toString(), data, context);
    De_Tab_Img(g_Tab_Img, data);
    if (g_Tab_Img.size() == 1)
       { int liRet = Validar_Host("E", data, context); }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    rg.MoverA(g_MsgED135.ED135_Idr, "C");
    rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
    if (g_PrmPC080.PC080_Trt.toString().trim().equals(""))
       {
         rg.MoverA(g_MsgED135.ED135_Trt, "UNICO");
         lcSvc = "ED535";
       }
    else
       {
         rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
         lcSvc = "ED135";
       }
    rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, lcSvc);
    g_MsgED135 = MSGED135.LSet_A_MsgED135(g_Msg.Msg_Dat.toString());
  }
  //---------------------------------------------------------------------------------------
  public void De_Tab_Img(Vector g_Tab_Img, RunData data)
  {
    g_Img       = img.Inicia_Img();
    lcData      = img.LSet_De_Img(g_Img);
    g_Img_Base  = img.LSet_A_ImgBase(lcData);
    g_Img_GtSgr = gtimg.LSet_A_ImgGtSgr(g_Img.Img_Dat.toString());
    v_Img_GtSgd.clear();
    for (i=0; i<g_Tab_Img.size(); i++)
        {
          lcData = img.LSet_De_Img((BF_IMG.Buf_Img)g_Tab_Img.elementAt(i));
          g_Img  = img.LSet_A_Img(lcData);
          if (g_Img.Img_Dax.toString().trim().equals("BASE"))
             {
               g_Img_GtSgr = gtimg.LSet_A_ImgGtSgr(g_Img.Img_Dat.toString());
               g_Img_Base  = img.LSet_A_ImgBase(g_Img_GtSgr.GtSgr_Bse.toString());
             }
          if (g_Img.Img_Dax.toString().trim().equals("DTSGR"))
             {
               g_Img_GtSgd = gtimg.LSet_A_ImgGtSgd(g_Img.Img_Dat.toString());
               v_Img_GtSgd.add (g_Img_GtSgd); 
             }
        }
  }
  //---------------------------------------------------------------------------------------
  public int Carga_Inicial(RunData data, Context context)
             throws Exception 
  {
    g_Img       = img.Inicia_Img();
    lcData      = img.LSet_De_Img(g_Img);
    g_Img_Base  = img.LSet_A_ImgBase(lcData);
    g_Img_GtSgr = gtimg.LSet_A_ImgGtSgr(g_Img.Img_Dat.toString());
    rg.MoverA(g_Img_Base.Img_Base_Ttr, "RVSEG");
    rg.MoverA(g_Img_Base.Img_Base_Sis, g_PrmPC080.PC080_Cnr.Sis);
    rg.MoverA(g_Img_Base.Img_Base_Ncn, g_PrmPC080.PC080_Cnr.Ncn);
    if (Validar_Host("N", data, context) == 1)
       { return 1; }
    rg.MoverA(g_PrmPC080.PC080_Ntr, g_Img_Base.Img_Base_Ntr);
    return 0;
  }
  //---------------------------------------------------------------------------------------
  public int Validar_Host(String pcIdr, RunData data, Context context)
             throws Exception 
  {
    int p = 0;
    String tGtSgd_Seq = "";
    String tGtSgr_Vsu = rg.Zeros(15);
    String tGtSgr_Vsg = rg.Zeros(15);
    String tGtSgr_Svl = rg.Zeros(15);
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    MSGGT625.Bff_MsgGT625 d_MsgGT625 = new MSGGT625.Bff_MsgGT625();
    boolean liSwtFin = false;
    v_Img_GtSgd.clear();
    g_MsgGT625 = MSGGT625.Inicia_MsgGT625();
    while (liSwtFin == false)
       {
         if (Valida_Host(pcIdr, data, context) == 1)
            { return 1; }
         for (i=p; i<MSGGT625.g_Max_GT625; i++)
             {
               d_MsgGT625 = (MSGGT625.Bff_MsgGT625)g_MsgGT625.GT625_Sgr.elementAt(i);
               if (d_MsgGT625.GtSgr_Seq.toString().trim().equals(""))
                  {
                    liSwtFin = true;
                    break;
                  }
               GT_IMG.Buf_Img_GtSgd g_Img_GtSgd = new GT_IMG.Buf_Img_GtSgd();
               rg.MoverA(g_Img_GtSgd.GtSgd_Seq, d_MsgGT625.GtSgr_Seq);
               rg.MoverA(g_Img_GtSgd.GtSgd_Itb, d_MsgGT625.GtSgr_Itb);
               rg.MoverA(g_Img_GtSgd.GtSgd_Dsb, d_MsgGT625.GtSgr_Dsb);
               rg.MoverA(g_Img_GtSgd.GtSgd_Frv, d_MsgGT625.GtSgr_Frv);
               rg.MoverA(g_Img_GtSgd.GtSgd_Vcn, d_MsgGT625.GtSgr_Vcn);
               rg.MoverA(g_Img_GtSgd.GtSgd_Umt, d_MsgGT625.GtSgr_Umt);
               rg.MoverA(g_Img_GtSgd.GtSgd_Vum, d_MsgGT625.GtSgr_Vum);
               rg.MoverA(g_Img_GtSgd.GtSgd_Vsu, d_MsgGT625.GtSgr_Vsu);
               rg.MoverA(g_Img_GtSgd.GtSgd_Vsg, d_MsgGT625.GtSgr_Vsg);
               rg.MoverA(g_Img_GtSgd.GtSgd_Isg, d_MsgGT625.GtSgr_Isg);
               rg.MoverA(g_Img_GtSgd.GtSgd_Csg, d_MsgGT625.GtSgr_Csg);
               rg.MoverA(g_Img_GtSgd.GtSgd_Nsg, d_MsgGT625.GtSgr_Nsg);
               rg.MoverA(g_Img_GtSgd.GtSgd_Spz, d_MsgGT625.GtSgr_Spz);
               rg.MoverA(g_Img_GtSgd.GtSgd_Sfv, d_MsgGT625.GtSgr_Sfv);
               rg.MoverA(g_Img_GtSgd.GtSgd_Sum, d_MsgGT625.GtSgr_Sum);
               rg.MoverA(g_Img_GtSgd.GtSgd_Svl, d_MsgGT625.GtSgr_Svl);
               rg.MoverA(g_Img_GtSgd.GtSgd_Fll, "");
               v_Img_GtSgd.add(g_Img_GtSgd);
               tGtSgd_Seq = g_Img_GtSgd.GtSgd_Seq.toString();
               rg.MoverA(g_Img_GtSgr.GtSgr_Umt, d_MsgGT625.GtSgr_Umt);
               tGtSgr_Vsu = rg.Suma(tGtSgr_Vsu,4,d_MsgGT625.GtSgr_Vsu,4,15,4);
               tGtSgr_Vsg = rg.Suma(tGtSgr_Vsg,2,d_MsgGT625.GtSgr_Vsg,2,15,2);
               tGtSgr_Svl = rg.Suma(tGtSgr_Svl,4,d_MsgGT625.GtSgr_Svl,4,15,4);
             }
         g_MsgGT625 = MSGGT625.Inicia_MsgGT625();
         rg.MoverA(((MSGGT625.Bff_MsgGT625)g_MsgGT625.GT625_Sgr.elementAt(0)).GtSgr_Seq, tGtSgd_Seq);
         pcIdr = "S";
         tGtSgd_Seq = "";
         p = 1;
       }
    rg.MoverA(g_Img_GtSgr.GtSgr_Vsu, tGtSgr_Vsu);
    rg.MoverA(g_Img_GtSgr.GtSgr_Vsg, tGtSgr_Vsg);
    rg.MoverA(g_Img_GtSgr.GtSgr_Svl, tGtSgr_Svl);
    return 0;
  }
  //---------------------------------------------------------------------------------------
  public int Valida_Host(String pcIdr, RunData data, Context context)
             throws Exception 
  {
    if (pcIdr.equals("N"))
       { rg.MoverA(g_Img_GtSgr.GtSgr_Bse, img.LSet_De_ImgBase_Bse(g_Img_Base)); }
    rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtSgr(g_Img_GtSgr));
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    rg.MoverA(g_MsgGT625.GT625_Idr, pcIdr);
    rg.MoverA(g_MsgGT625.GT625_Img, g_Img.Img_Dat);
    rg.MoverA(g_Msg.Msg_Dat, MSGGT625.LSet_De_MsgGT625(g_MsgGT625));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GT625");
    g_MsgGT625 = MSGGT625.LSet_A_MsgGT625(g_Msg.Msg_Dat.toString());
    if (g_MsgGT625.GT625_Idr.toString().equals("R"))
       {
         String lcMensaje = "NO SE";
         if (g_MsgGT625.GT625_Swt_Gti.toString().equals("1")){lcMensaje = "Numero Garantia No Existe";}
         if (g_MsgGT625.GT625_Swt_Gti.toString().equals("2")){lcMensaje = "Garantia NO Esta Vigente";}
         if (g_MsgGT625.GT625_Swt_Gti.toString().equals("3")){lcMensaje = "Evento No Valido para Garantia";}
         if (g_MsgGT625.GT625_Swt_Evt.toString().equals("1")){lcMensaje = "Garantia Tiene Evento en Tramite";}
         if (g_MsgGT625.GT625_Swt_Cli.toString().equals("1")){lcMensaje = "Garantia Erronea (Sin CLI-GTI)";}
         if (g_MsgGT625.GT625_Swt_Cli.toString().equals("2")){lcMensaje = "Garantia Erronea (Sin CLI)";}
         msg.MsgInt("Agtp625", data, g_Msg, lcMensaje, "1"); 
         return 1;
       }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    rg.MoverA(g_Img.Img_Dat, g_MsgGT625.GT625_Img);
    g_Img_GtSgr = gtimg.LSet_A_ImgGtSgr(g_Img.Img_Dat.toString());
    if (pcIdr.equals("N"))
       { 
       	 g_Img_Base  = img.LSet_A_ImgBase(g_Img_GtSgr.GtSgr_Bse.toString()); 
         rg.MoverA(g_Img_Base.Img_Base_Suc, g_Msg.Msg_Suc);
         rg.MoverA(g_Img_Base.Img_Base_Nsu, g_Msg.Msg_Nsu);
         rg.MoverA(g_Img_Base.Img_Base_Eje, g_Msg.Msg_Cus);
         rg.MoverA(g_Img_Base.Img_Base_Nej, g_Msg.Msg_Nus);
         rg.MoverA(g_Img_Base.Img_Base_Tej, g_Msg.Msg_Fus);
         rg.MoverA(g_Img_GtSgr.GtSgr_Bse,   img.LSet_De_ImgBase_Bse(g_Img_Base));
         rg.MoverA(g_Img_GtSgr.GtSgr_Fpr,   g_Msg.Msg_Fch);
         rg.MoverA(g_Img_GtSgr.GtSgr_Fig,   g_Msg.Msg_Fch);
       }
    return 0;
  }
  //---------------------------------------------------------------------------------------
  public void Init_Evt_GRT(RunData data, Context context)
              throws Exception 
  {
    rg.MoverA(g_Evt.Evt_Ttr, g_Img_Base.Img_Base_Ttr);
    rg.MoverA(g_Evt.Evt_Ntt, g_Img_Base.Img_Base_Ntt);
    rg.MoverA(g_Evt.Evt_Tsl, g_Img_Base.Img_Base_Tsl);
    rg.MoverA(g_Evt.Evt_Slc, g_Img_Base.Img_Base_Slc);
    rg.MoverA(g_Evt.Evt_Cli, g_Img_Base.Img_Base_Cli);
    rg.MoverA(g_Evt.Evt_Ncl, g_Img_Base.Img_Base_Ncl);
    rg.MoverA(g_Evt.Evt_Tpr, g_Img_Base.Img_Base_Tpr);
    rg.MoverA(g_Evt.Evt_Dir, g_Img_Base.Img_Base_Dir);
    rg.MoverA(g_Evt.Evt_Cmn, rg.Zeros(g_Evt.Evt_Cmn));      //g_Img_Base.Img_Base_Cmn
    rg.MoverA(g_Evt.Evt_Tfc, g_Img_Base.Img_Base_Tfc);
    rg.MoverA(g_Evt.Evt_Sis, g_Img_Base.Img_Base_Sis);
    rg.MoverA(g_Evt.Evt_Ncn, g_Img_Base.Img_Base_Ncn);
    rg.MoverA(g_Evt.Evt_Dcn, g_Img_Base.Img_Base_Dcn);
    rg.MoverA(g_Evt.Evt_Ndc, g_Img_Base.Img_Base_Ndc);
    rg.MoverA(g_Evt.Evt_Tmn, g_Img_Base.Img_Base_Tmn);
    rg.MoverA(g_Evt.Evt_Trj, g_Img_Base.Img_Base_Trj);
    rg.MoverA(g_Evt.Evt_Dcm, g_Img_Base.Img_Base_Dcm);
    rg.MoverA(g_Evt.Evt_Prp, g_Img_Base.Img_Base_Prp);
    rg.MoverA(g_Evt.Evt_Suc, g_Img_Base.Img_Base_Suc);
    rg.MoverA(g_Evt.Evt_Nsu, g_Img_Base.Img_Base_Nsu);
    rg.MoverA(g_Evt.Evt_Mnd, g_Img_Base.Img_Base_Mnd);
    rg.MoverA(g_Evt.Evt_Nmn, g_Img_Base.Img_Base_Nmn);
    rg.MoverA(g_Evt.Evt_Eje, g_Img_Base.Img_Base_Eje);
    rg.MoverA(g_Evt.Evt_Nej, g_Img_Base.Img_Base_Nej);
    rg.MoverA(g_Evt.Evt_Tej, g_Img_Base.Img_Base_Tej);
    rg.MoverA(g_Evt.Evt_Est, "NUEVA");
    rg.MoverA(g_Evt.Evt_Fst, g_Msg.Msg_Fch);
    rg.MoverA(g_Evt.Evt_Upr, g_Img_Base.Img_Base_Trj);
    rg.MoverA(g_Evt.Evt_Vpr, rg.Zeros(g_Evt.Evt_Vpr));
    rg.MoverA(g_Evt.Evt_Fpr, g_Msg.Msg_Fch);
    rg.MoverA(g_Evt.Evt_Spr, rg.Zeros(g_Evt.Evt_Spr));
    rg.MoverA(g_Evt.Evt_Tpp, " ");
    rg.MoverA(g_Evt.Evt_Ggr, g_Img_GtSgr.GtSgr_Ggr);
    rg.MoverA(g_Evt.Evt_Fll, " ");
  }
  //---------------------------------------------------------------------------------------
  public Vector A_Tab_Img(RunData data)
  {
    Vector v_Img = new Vector();
    v_Img.clear();
    rg.MoverA(g_Img.Img_Ntr, g_PrmPC080.PC080_Ntr);
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    rg.MoverA(g_Img.Img_Dax, "BASE");
    rg.MoverA(g_Img.Img_Seq, "000");
    rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtSgr(g_Img_GtSgr));
    Vector vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
    v_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    rg.MoverA(g_Img.Img_Seq, "000");
    for (i=0; i<v_Img_GtSgd.size(); i++)
        {
          g_Img_GtSgd = (GT_IMG.Buf_Img_GtSgd)v_Img_GtSgd.elementAt(i);
          rg.MoverA(g_Img.Img_Dax, "DTSGR");
          rg.MoverA(g_Img.Img_Seq, rg.Suma(g_Img.Img_Seq,0,"1",0,3,0));
          rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtSgd(g_Img_GtSgd));
          vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
          v_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
        }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    return v_Img;
  }
  //---------------------------------------------------------------------------------------
  public void Cursatura(String pcIdr, Vector g_Tab_Img, RunData data, Context context)
              throws Exception 
  {
    g_Evt = (BF_EVT.Buf_Evt)data.getUser().getTemp("Agtp625_Evt");
    rg.MoverA(g_Evt.Evt_Est, "ENTMT");
    rg.MoverA(g_Evt.Evt_Fpr, g_Msg.Msg_Fch);
    BF_EVT t = new BF_EVT();
    g_MsgED100 = MSGED100.Inicia_MsgED100();
    rg.MoverA(g_MsgED100.ED100_Idr, pcIdr);
    rg.MoverA(g_MsgED100.ED100_Nev, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_MsgED100.ED100_Evt, t.LSet_De_Evt(g_Evt));
    rg.MoverA(g_Msg.Msg_Dat, MSGED100.LSet_De_MsgED100(g_MsgED100));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED500");
  }
//---------------------------------------------------------------------------------------
  public void doAgtp625_Continue(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP625[doAgtp625_Continue.start]", "[" + data.getUser().getUserName() + "]");
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Eclp090"))
       {
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp625_g_PrmPC080");
         data.getUser().removeTemp("Agtp625_g_PrmPC080");
         g_PrmCL090 = PRMCL090.LSet_A_PrmCL090(g_Msg.Msg_Dat.toString());
         if (g_PrmCL090.CL090_Cli.toString().trim().equals(""))
            {
              Limpieza(data, context);
              rg.MoverA(g_PrmPC080.PC080_Rtn, "NK");
              rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
              BF_MSG.Return_Data("Agtp625", data, g_Msg);
              return;
            }
         rg.MoverA(g_PrmPC080.PC080_Cli, g_PrmCL090.CL090_Cli);
         rg.MoverA(g_PrmPC080.PC080_Ncl, g_PrmCL090.CL090_Ncl);
         rg.MoverA(g_PrmPC080.PC080_Tpr, g_PrmCL090.CL090_Tpr);
         rg.MoverA(g_PrmPC080.PC080_Acc, "SELECCIONA");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Link_Program("Agtp625", "Agtp400", data, g_Msg);
         return;
       }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Agtp400"))
       {
         g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
         if (g_PrmPC080.PC080_Rtn.toString().trim().equals("NK"))
            {
              Limpieza(data, context);
              rg.MoverA(g_PrmPC080.PC080_Rtn, "NK");
              rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
              BF_MSG.Return_Data("Agtp625", data, g_Msg);
              return;
            }
         rg.MoverA(g_PrmPC080.PC080_Acc, "RVSEG");
         rg.MoverA(g_PrmPC080.PC080_Ttr, "RVSEG");
         rg.MoverA(g_PrmPC080.PC080_Ntt, "RENOVACION SEGURO DE GARANTIA");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         doAgtp625_Init(data, context);
         return;
       }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Atgp114"))
       {
         g_PrmTG114 = PRMTG114.LSet_A_PrmTG114(g_Msg.Msg_Dat.toString());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp625_g_PrmPC080");
         data.getUser().removeTemp("Agtp625_g_PrmPC080");
         if (g_PrmTG114.TG114_Csg.toString().trim().compareTo("")!=0)
             {
               g_Img_GtSgd = (GT_IMG.Buf_Img_GtSgd)data.getUser().getTemp("Agtp625_ImgGtSgd");
               rg.MoverA(g_Img_GtSgd.GtSgd_Csg, g_PrmTG114.TG114_Csg);
               rg.MoverA(g_Img_GtSgd.GtSgd_Nsg, g_PrmTG114.TG114_Ncs);
               data.getUser().removeTemp("Agtp625_ImgGtSgd");
               data.getUser().setTemp("Agtp625_ImgGtSgd", g_Img_GtSgd);
             }
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP625.vm" );
         return;
       }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Aedp210"))
       {
         g_PrmED210 = PRMED210.LSet_A_PrmED210(g_Msg.Msg_Dat.toString());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp625_g_PrmPC080");
         data.getUser().removeTemp("Agtp625_g_PrmPC080");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP625.vm" );
         return;
       }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Appp030"))
       {
         Limpieza(data, context);
         rg.MoverA(g_PrmPC080.PC080_Rtn, "NK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp625", data, g_Msg);
         return;
       }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  }
  //---------------------------------------------------------------------------------------
  public void Actualiza_Datos(RunData data, Context context)
              throws Exception 
  {
    rg.MoverA(g_Img_GtSgd.GtSgd_Isg, data.getParameters().getString("GtSgd_Isg", ""));
    rg.MoverA(g_Img_GtSgd.GtSgd_Csg, data.getParameters().getString("GtSgd_Csg", ""));
    rg.MoverA(g_Img_GtSgd.GtSgd_Nsg, data.getParameters().getString("GtSgd_Nsg", ""));
    rg.MoverA(g_Img_GtSgd.GtSgd_Spz, data.getParameters().getString("GtSgd_Spz", ""));
    rg.MoverA(g_Img_GtSgd.GtSgd_Sfv, data.getParameters().getString("GtSgd_Sfv", ""));
    rg.MoverA(g_Img_GtSgd.GtSgd_Svl, data.getParameters().getString("GtSgd_Svl", ""));
  }
  //---------------------------------------------------------------------------------------
  public void Limpieza(RunData data, Context context)
              throws Exception 
  {
    data.getUser().removeTemp("Agtp625_PrmPC080");
    data.getUser().removeTemp("Agtp625_MsgED135");
    data.getUser().removeTemp("Agtp625_Evt");
    data.getUser().removeTemp("Agtp625_ImgBase");
    data.getUser().removeTemp("Agtp625_ImgGtSgr");
    data.getUser().removeTemp("Agtp625_vImgGtSgd");
  }
  //---------------------------------------------------------------------------------------
  public void Prorratea_Seguros(RunData data, Context context)
              throws Exception 
  {
    Log.debug("AGTP625[Prorratea_Seguros]", "[" + "Partimos" + "]");
    String tGtSgr_Svl = rg.Zeros(15);
    for (i=0; i<v_Img_GtSgd.size(); i++)
        {
          GT_IMG.Buf_Img_GtSgd t_Img_GtSgd = new GT_IMG.Buf_Img_GtSgd();
          t_Img_GtSgd = (GT_IMG.Buf_Img_GtSgd)v_Img_GtSgd.elementAt(i);
          rg.MoverA(t_Img_GtSgd.GtSgd_Isg, "M");
          rg.MoverA(t_Img_GtSgd.GtSgd_Csg, g_Img_GtSgd.GtSgd_Csg);
          rg.MoverA(t_Img_GtSgd.GtSgd_Nsg, g_Img_GtSgd.GtSgd_Nsg);
          rg.MoverA(t_Img_GtSgd.GtSgd_Spz, g_Img_GtSgd.GtSgd_Spz);
          rg.MoverA(t_Img_GtSgd.GtSgd_Sfv, g_Img_GtSgd.GtSgd_Sfv);
    Log.debug("AGTP625[Prorratea_Seguros]", "[" + g_Img_GtSgr.GtSgr_Vsu.toString() + "]");
    Log.debug("AGTP625[Prorratea_Seguros]", "[" + t_Img_GtSgd.GtSgd_Vsu.toString() + "]");
          String factor = "000000000000000";
          if (Long.parseLong(t_Img_GtSgd.GtSgd_Vsu.toString()) !=0
           && Long.parseLong(g_Img_GtSgr.GtSgr_Vsu.toString()) !=0)
          {factor = rg.Divide(t_Img_GtSgd.GtSgd_Vsu,4,g_Img_GtSgr.GtSgr_Vsu,4,15,9,9);}
          
    Log.debug("AGTP625[Prorratea_Seguros]", "[" + factor + "]");
    Log.debug("AGTP625[Prorratea_Seguros]", "[" + g_Img_GtSgd.GtSgd_Svl.toString() + "]");
          rg.MoverA(t_Img_GtSgd.GtSgd_Svl, rg.Multiplica(g_Img_GtSgd.GtSgd_Svl,4,factor,9,15,4,4));
          v_Img_GtSgd.set(i, t_Img_GtSgd); 
          tGtSgr_Svl = rg.Suma(tGtSgr_Svl,4,t_Img_GtSgd.GtSgd_Svl,4,15,4);
        }
    rg.MoverA(g_Img_GtSgr.GtSgr_Svl, tGtSgr_Svl);
  }
  //---------------------------------------------------------------------------------------
}