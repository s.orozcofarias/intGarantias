// Source File Name:   Agtp640.java

package com.intGarantias.modules.actions;

import java.util.Vector;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.BF_EVT;
import com.FHTServlet.modules.global.BF_IMG;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.PRMPC080;
import com.FHTServlet.modules.global.RUTGEN;
import com.intGarantias.modules.global.GT_IMG;
import com.intGarantias.modules.global.MSGGT640;
import com.intGlobal.modules.global.MSGED100;
import com.intGlobal.modules.global.MSGED135;
import com.intGlobal.modules.global.PRMCL090;
import com.intGlobal.modules.global.PRMTG112;
import com.intGlobal.modules.global.PRMTG123;
import com.intGlobal.modules.global.PRMTG130;

public class Agtp640 extends FHTServletAction {

	public Agtp640() {
		i = 0;
		lcData = "";
		rg = new RUTGEN();
		msg = new BF_MSG();
		g_Msg = new com.FHTServlet.modules.global.BF_MSG.Buf_Msg();
		g_Evt = new com.FHTServlet.modules.global.BF_EVT.Buf_Evt();
		v_Tab_Img = new Vector();
		v_Img_GtHyp = new Vector();
		v_Img_GtHpd = new Vector();
		img = new BF_IMG();
		g_Img = new com.FHTServlet.modules.global.BF_IMG.Buf_Img();
		g_Img_Base = new com.FHTServlet.modules.global.BF_IMG.Buf_Img_Bse();
		gtimg = new GT_IMG();
		g_Img_GtApe = new com.intGarantias.modules.global.GT_IMG.Buf_Img_GtApe();
		g_Img_GtTsd = new com.intGarantias.modules.global.GT_IMG.Buf_Img_GtTsd();
		g_Img_GtHyp = new com.intGarantias.modules.global.GT_IMG.Buf_Img_GtHyp();
		g_Img_GtHpd = new com.intGarantias.modules.global.GT_IMG.Buf_Img_GtHpd();
		g_PrmPC080 = new com.FHTServlet.modules.global.PRMPC080.PrmPC080();
		g_MsgED100 = new com.intGlobal.modules.global.MSGED100.Buf_MsgED100();
		g_MsgED135 = new com.intGlobal.modules.global.MSGED135.Buf_MsgED135();
		g_PrmCL090 = new com.intGlobal.modules.global.PRMCL090.Buf_PrmCL090();
		g_MsgGT640 = new com.intGarantias.modules.global.MSGGT640.Buf_MsgGT640();
		g_PrmTG123 = new com.intGlobal.modules.global.PRMTG123.Buf_PrmTG123();
		g_PrmTG130 = new com.intGlobal.modules.global.PRMTG130.Buf_PrmTG130();
		g_PrmTG112 = new com.intGlobal.modules.global.PRMTG112.Buf_PrmTG112();
	}

	public void doPerform(RunData data, Context context) throws Exception {
		g_Msg = BF_MSG.Init_Program("Agtp640-DIN", data);
		if (g_Msg.Msg_Pgr.toString().equals("RT"))
			doAgtp640_Continue(data, context);
		else
			doAgtp640_Init(data, context);
	}

	public void doCancel(RunData data, Context context) throws Exception {
		setTemplate(data, "Garantias,Agt,AGTP640.vm");
	}

	public void doAgtp640_Init(RunData data, Context context) throws Exception {
		g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
		if (g_PrmPC080.PC080_Cnr.Ncn.toString().trim().equals("")
				&& g_PrmPC080.PC080_Cli.toString().trim().equals("")) {
			data.getUser().removeTemp("Agtp640_g_PrmPC080");
			data.getUser().setTemp("Agtp640_g_PrmPC080", g_PrmPC080);
			g_PrmCL090 = PRMCL090.Inicia_PrmCL090();
			RUTGEN.MoverA(g_PrmCL090.CL090_Pcc, "N");
			RUTGEN.MoverA(g_Msg.Msg_Dat, PRMCL090.LSet_De_PrmCL090(g_PrmCL090));
			BF_MSG.Link_Program("Agtp640", "Eclp090", data, g_Msg);
			return;
		}
		if (Carga_Inicial(data, context) == 1) {
			return;
		} else {
			Init_Evt_GRT(data, context);
			RUTGEN.MoverA(g_PrmPC080.PC080_Rtn, "WR");
			RUTGEN.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
			BF_MSG.Param_Program(data, g_Msg);
			data.getUser().removeTemp("Agtp640_PrmPC080");
			data.getUser().setTemp("Agtp640_PrmPC080", g_PrmPC080);
			setTemplate(data, "Garantias,Agt,AGTP640.vm");
			return;
		}
	}

	public void doAgtp640(RunData data, Context context) throws Exception {
		g_Msg = BF_MSG.Init_Program("Agtp640-MAN", data);
		g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
		String Opc = data.getParameters().getString("Opc", "");
		if (Opc.trim().equals("S")) {
			data.getUser().removeTemp("Agtp640_PrmPC080");
			data.getUser().removeTemp("Agtp640_Evt");
			data.getUser().removeTemp("Agtp640_ImgGtTsd");
			data.getUser().removeTemp("Agtp640_g_ImgBase");
			data.getUser().removeTemp("Agtp640_ImgGtApe");
			data.getUser().removeTemp("Agtp640_vImgGtHyp");
			data.getUser().removeTemp("Agtp640_vImgGtHpd");
			RUTGEN.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
			BF_MSG.Return_Program(data, g_Msg);
			return;
		}
		if (Opc.trim().equals("1")) {
			Actualiza_Datos(data, context);
			g_PrmTG130 = PRMTG130.Inicia_PrmTG130();
			RUTGEN.MoverA(g_PrmTG130.TG130_Cmp, "DMC-LUG");
			RUTGEN.MoverA(g_PrmTG130.TG130_Ncm, "Lugares de Domicilios");
			data.getUser().removeTemp("Agtp640_g_PrmPC080");
			data.getUser().setTemp("Agtp640_g_PrmPC080", g_PrmPC080);
			RUTGEN.MoverA(g_Msg.Msg_Dat, PRMTG130.LSet_De_PrmTG130(g_PrmTG130));
			BF_MSG.Link_Program("Agtp640", "Atgp130", data, g_Msg);
			return;
		}
		if (Opc.trim().equals("2")) {
			Actualiza_Datos(data, context);
			g_PrmTG130 = PRMTG130.Inicia_PrmTG130();
			RUTGEN.MoverA(g_PrmTG130.TG130_Cmp, "DMC-SEC");
			RUTGEN.MoverA(g_PrmTG130.TG130_Ncm, "Sectores de Domicilios");
			data.getUser().removeTemp("Agtp640_g_PrmPC080");
			data.getUser().setTemp("Agtp640_g_PrmPC080", g_PrmPC080);
			RUTGEN.MoverA(g_Msg.Msg_Dat, PRMTG130.LSet_De_PrmTG130(g_PrmTG130));
			BF_MSG.Link_Program("Agtp640", "Atgp130", data, g_Msg);
			return;
		}
		if (Opc.trim().equals("3")) {
			Actualiza_Datos(data, context);
			g_PrmTG123 = PRMTG123.Inicia_PrmTG123();
			data.getUser().removeTemp("Agtp640_g_PrmPC080");
			data.getUser().setTemp("Agtp640_g_PrmPC080", g_PrmPC080);
			RUTGEN.MoverA(g_Msg.Msg_Dat, PRMTG123.LSet_De_PrmTG123(g_PrmTG123));
			BF_MSG.Link_Program("Agtp640", "Atgp124", data, g_Msg);
			return;
		}
		if (Opc.trim().equals("4")) {
			Actualiza_Datos(data, context);
			g_PrmTG112 = PRMTG112.Inicia_PrmTG112();
			data.getUser().removeTemp("Agtp640_g_PrmPC080");
			data.getUser().setTemp("Agtp640_g_PrmPC080", g_PrmPC080);
			RUTGEN.MoverA(g_Msg.Msg_Dat, PRMTG112.LSet_De_PrmTG112(g_PrmTG112));
			BF_MSG.Link_Program("Agtp640", "Atgp142", data, g_Msg);
			return;
		}
		if (Opc.trim().equals("A")) {
			g_Evt = (com.FHTServlet.modules.global.BF_EVT.Buf_Evt) data
					.getUser().getTemp("Agtp640_Evt");
			Actualiza_Datos(data, context);
			v_Img_GtHyp = (Vector) data.getUser().getTemp("Agtp640_vImgGtHyp");
			v_Img_GtHpd = (Vector) data.getUser().getTemp("Agtp640_vImgGtHpd");
			Vector g_Tab_Img = A_Tab_Img(data);
			img.Graba_Img_Host(g_Msg, g_Tab_Img, data, context);
			RUTGEN.MoverA(g_Evt.Evt_Ndc, g_Img_GtApe.GtApe_Dsg);
			RUTGEN.MoverA(g_Evt.Evt_Vpr,
					RUTGEN.FmtValor(g_Img_GtApe.GtApe_Vcn, 2, 4, 15, "+"));
			BF_EVT t = new BF_EVT();
			g_MsgED100 = MSGED100.Inicia_MsgED100();
			RUTGEN.MoverA(g_MsgED100.ED100_Idr, "C");
			RUTGEN.MoverA(g_MsgED100.ED100_Nev, g_PrmPC080.PC080_Ntr);
			RUTGEN.MoverA(g_MsgED100.ED100_Evt, t.LSet_De_Evt(g_Evt));
			RUTGEN.MoverA(g_Msg.Msg_Dat, MSGED100.LSet_De_MsgED100(g_MsgED100));
			g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED100");
			g_MsgED135 = MSGED135.Inicia_MsgED135();
			RUTGEN.MoverA(g_MsgED135.ED135_Idr, "M");
			RUTGEN.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
			RUTGEN.MoverA(g_MsgED135.ED135_Trt, "MODTS");
			RUTGEN.MoverA(g_MsgED135.ED135_Est, "");
			RUTGEN.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
			RUTGEN.MoverA(g_MsgED135.ED135_Ezt, "ENTSD");
			RUTGEN.MoverA(g_MsgED135.ED135_Exi, "TSD");
			RUTGEN.MoverA(g_MsgED135.ED135_Exn,
					RUTGEN.FmtValor(g_Img_GtTsd.GtTsd_Tsd, 0, 0, 12, "+"));
			RUTGEN.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
			g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
			data.getUser().removeTemp("Agtp640_PrmPC080");
			data.getUser().removeTemp("Agtp640_Evt");
			data.getUser().removeTemp("Agtp640_ImgGtTsd");
			data.getUser().removeTemp("Agtp640_g_ImgBase");
			data.getUser().removeTemp("Agtp640_ImgGtApe");
			data.getUser().removeTemp("Agtp640_vImgGtHyp");
			data.getUser().removeTemp("Agtp640_vImgGtHpd");
			RUTGEN.MoverA(g_PrmPC080.PC080_Rtn, "");
			RUTGEN.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
			BF_MSG.Return_Program(data, g_Msg);
			return;
		} else {
			return;
		}
	}

	public void Actualiza_Datos(RunData data, Context context) throws Exception {
		g_Img_GtApe = (com.intGarantias.modules.global.GT_IMG.Buf_Img_GtApe) data
				.getUser().getTemp("Agtp640_ImgGtApe");
		g_Img_Base = BF_IMG.LSet_A_ImgBase(g_Img_GtApe.GtApe_Bse.toString());
		g_Img_GtTsd = (com.intGarantias.modules.global.GT_IMG.Buf_Img_GtTsd) data
				.getUser().getTemp("Agtp640_ImgGtTsd");
		RUTGEN.MoverA(g_Img_GtTsd.GtTsd_Nc1,
				data.getParameters().getString("GtTsd_Nco", ""));
		RUTGEN.MoverA(g_Img_GtTsd.GtTsd_T11,
				data.getParameters().getString("GtTsd_Tco", ""));
		RUTGEN.MoverA(g_Img_GtTsd.GtTsd_Cll,
				data.getParameters().getString("GtTsd_Cll", ""));
		RUTGEN.MoverA(g_Img_GtTsd.GtTsd_Cnm,
				data.getParameters().getString("GtTsd_Cnm", ""));
		RUTGEN.MoverA(g_Img_GtTsd.GtTsd_Lug,
				data.getParameters().getString("GtTsd_Lug", ""));
		RUTGEN.MoverA(g_Img_GtTsd.GtTsd_Lnm,
				data.getParameters().getString("GtTsd_Lnm", ""));
		RUTGEN.MoverA(g_Img_GtTsd.GtTsd_Sec,
				data.getParameters().getString("GtTsd_Sec", ""));
		RUTGEN.MoverA(g_Img_GtTsd.GtTsd_Snb,
				data.getParameters().getString("GtTsd_Snb", ""));
		RUTGEN.MoverA(g_Img_GtTsd.GtTsd_Cmn,
				data.getParameters().getString("GtTsd_Cmn", ""));
		RUTGEN.MoverA(g_Img_GtTsd.GtTsd_Ncm,
				data.getParameters().getString("GtTsd_Ncm", ""));
		RUTGEN.MoverA(g_Img_GtTsd.GtTsd_Npv,
				data.getParameters().getString("GtTsd_Npv", ""));
		RUTGEN.MoverA(g_Img_GtTsd.GtTsd_Opt,
				data.getParameters().getString("GtTsd_Opt", ""));
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Bse,
				BF_IMG.LSet_De_ImgBase_Bse(g_Img_Base));
		data.getUser().removeTemp("Agtp640_ImgGtApe");
		data.getUser().setTemp("Agtp640_ImgGtApe", g_Img_GtApe);
		data.getUser().removeTemp("Agtp640_ImgGtTsd");
		data.getUser().setTemp("Agtp640_ImgGtTsd", g_Img_GtTsd);
	}

	public void doAgtp640_Continue(RunData data, Context context)
			throws Exception {
		if (RUTGEN.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Eclp090")) {
			g_PrmPC080 = (com.FHTServlet.modules.global.PRMPC080.PrmPC080) data
					.getUser().getTemp("Agtp640_g_PrmPC080");
			data.getUser().removeTemp("Agtp640_g_PrmPC080");
			g_PrmCL090 = PRMCL090.LSet_A_PrmCL090(g_Msg.Msg_Dat.toString());
			if (g_PrmCL090.CL090_Cli.toString().trim().equals("")) {
				RUTGEN.MoverA(g_PrmPC080.PC080_Rtn, "NK");
				RUTGEN.MoverA(g_Msg.Msg_Dat,
						PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
				BF_MSG.Return_Data("Agtp640", data, g_Msg);
				return;
			} else {
				RUTGEN.MoverA(g_PrmPC080.PC080_Cli, g_PrmCL090.CL090_Cli);
				RUTGEN.MoverA(g_PrmPC080.PC080_Ncl, g_PrmCL090.CL090_Ncl);
				RUTGEN.MoverA(g_PrmPC080.PC080_Tpr, g_PrmCL090.CL090_Tpr);
				RUTGEN.MoverA(g_PrmPC080.PC080_Acc, "SELECCIONA");
				RUTGEN.MoverA(g_Msg.Msg_Dat,
						PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
				BF_MSG.Link_Program("Agtp640", "Agtp400", data, g_Msg);
				return;
			}
		}
		if (RUTGEN.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Agtp400")) {
			g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
			if (g_PrmPC080.PC080_Rtn.toString().trim().equals("NK")) {
				RUTGEN.MoverA(g_PrmPC080.PC080_Rtn, "NK");
				RUTGEN.MoverA(g_Msg.Msg_Dat,
						PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
				BF_MSG.Return_Data("Agtp640", data, g_Msg);
				return;
			} else {
				RUTGEN.MoverA(g_PrmPC080.PC080_Acc, "MDTAS");
				RUTGEN.MoverA(g_PrmPC080.PC080_Ttr, "MDTAS");
				RUTGEN.MoverA(g_PrmPC080.PC080_Ntt,
						"MODIFICA GARANTIA CONSTITUIDA");
				RUTGEN.MoverA(g_Msg.Msg_Dat,
						PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
				BF_MSG.Param_Program(data, g_Msg);
				doAgtp640_Init(data, context);
				return;
			}
		}
		if (RUTGEN.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Atgp124")) {
			g_PrmTG123 = PRMTG123.LSet_A_PrmTG123(g_Msg.Msg_Dat.toString());
			g_PrmPC080 = (com.FHTServlet.modules.global.PRMPC080.PrmPC080) data
					.getUser().getTemp("Agtp640_g_PrmPC080");
			data.getUser().removeTemp("Agtp640_g_PrmPC080");
			if (g_PrmTG123.TG123_Prv.toString().trim().compareTo("") != 0) {
				g_Img_GtTsd = (com.intGarantias.modules.global.GT_IMG.Buf_Img_GtTsd) data
						.getUser().getTemp("Agtp640_ImgGtTsd");
				RUTGEN.MoverA(
						g_Img_GtTsd.GtTsd_Cmn,
						g_PrmTG123.TG123_Prv.toString()
								+ g_PrmTG123.TG123_Cmn.toString());
				RUTGEN.MoverA(g_Img_GtTsd.GtTsd_Ncm, g_PrmTG123.TG123_Ncm);
				RUTGEN.MoverA(g_Img_GtTsd.GtTsd_Npv, g_PrmTG123.TG123_Npr);
				data.getUser().removeTemp("Agtp640_ImgGtTsd");
				data.getUser().setTemp("Agtp640_ImgGtTsd", g_Img_GtTsd);
			}
			RUTGEN.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
			BF_MSG.Param_Program(data, g_Msg);
			setTemplate(data, "Garantias,Agt,AGTP640.vm");
			return;
		}
		if (RUTGEN.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Atgp130")) {
			g_PrmTG130 = PRMTG130.LSet_A_PrmTG130(g_Msg.Msg_Dat.toString());
			g_PrmPC080 = (com.FHTServlet.modules.global.PRMPC080.PrmPC080) data
					.getUser().getTemp("Agtp640_g_PrmPC080");
			data.getUser().removeTemp("Agtp640_g_PrmPC080");
			if (g_PrmTG130.TG130_Cmp.toString().trim().equals("DMC-LUG")
					&& g_PrmTG130.TG130_Cod.toString().trim().compareTo("") != 0) {
				g_Img_GtTsd = (com.intGarantias.modules.global.GT_IMG.Buf_Img_GtTsd) data
						.getUser().getTemp("Agtp640_ImgGtTsd");
				RUTGEN.MoverA(g_Img_GtTsd.GtTsd_Lug, g_PrmTG130.TG130_Cod);
				if (g_Img_GtTsd.GtTsd_Lug.toString().trim().equals("<>"))
					RUTGEN.MoverA(g_Img_GtTsd.GtTsd_Lnm, "");
				data.getUser().removeTemp("Agtp640_ImgGtTsd");
				data.getUser().setTemp("Agtp640_ImgGtTsd", g_Img_GtTsd);
			}
			if (g_PrmTG130.TG130_Cmp.toString().trim().equals("DMC-SEC")
					&& g_PrmTG130.TG130_Cod.toString().trim().compareTo("") != 0) {
				g_Img_GtTsd = (com.intGarantias.modules.global.GT_IMG.Buf_Img_GtTsd) data
						.getUser().getTemp("Agtp640_ImgGtTsd");
				RUTGEN.MoverA(g_Img_GtTsd.GtTsd_Sec, g_PrmTG130.TG130_Cod);
				if (g_Img_GtTsd.GtTsd_Sec.toString().trim().equals("<>"))
					RUTGEN.MoverA(g_Img_GtTsd.GtTsd_Snb, "");
				data.getUser().removeTemp("Agtp640_ImgGtTsd");
				data.getUser().setTemp("Agtp640_ImgGtTsd", g_Img_GtTsd);
			}
			RUTGEN.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
			BF_MSG.Param_Program(data, g_Msg);
			setTemplate(data, "Garantias,Agt,AGTP640.vm");
			return;
		}
		if (RUTGEN.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Atgp142")) {
			g_PrmTG112 = PRMTG112.LSet_A_PrmTG112(g_Msg.Msg_Dat.toString());
			g_PrmPC080 = (com.FHTServlet.modules.global.PRMPC080.PrmPC080) data
					.getUser().getTemp("Agtp640_g_PrmPC080");
			data.getUser().removeTemp("Agtp640_g_PrmPC080");
			if (g_PrmTG112.TG112_Tsd.toString().trim().compareTo("") != 0) {
				g_Img_GtApe = (com.intGarantias.modules.global.GT_IMG.Buf_Img_GtApe) data
						.getUser().getTemp("Agtp640_ImgGtApe");
				g_Img_GtTsd = (com.intGarantias.modules.global.GT_IMG.Buf_Img_GtTsd) data
						.getUser().getTemp("Agtp640_ImgGtTsd");
				RUTGEN.MoverA(g_Img_GtTsd.GtTsd_Tsd, g_PrmTG112.TG112_Tsd);
				RUTGEN.MoverA(g_Img_GtTsd.GtTsd_Nts, g_PrmTG112.TG112_Nts);
				RUTGEN.MoverA(g_Img_GtTsd.GtTsd_Bof, g_PrmTG112.TG112_Bof);
				RUTGEN.MoverA(g_Img_GtTsd.GtTsd_Cct, g_PrmTG112.TG112_Cct);
				RUTGEN.MoverA(g_Img_GtTsd.GtTsd_Rts, g_PrmTG112.TG112_Rts);
				RUTGEN.MoverA(g_Img_GtTsd.GtTsd_Ept, g_PrmTG112.TG112_Ept);
				RUTGEN.MoverA(g_Img_GtApe.GtApe_Tsd, g_PrmTG112.TG112_Tsd);
				data.getUser().removeTemp("Agtp640_ImgGtTsd");
				data.getUser().setTemp("Agtp640_ImgGtTsd", g_Img_GtTsd);
				data.getUser().removeTemp("Agtp640_ImgGtApe");
				data.getUser().setTemp("Agtp640_ImgGtApe", g_Img_GtApe);
			}
			RUTGEN.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
			BF_MSG.Param_Program(data, g_Msg);
			setTemplate(data, "Garantias,Agt,AGTP640.vm");
			return;
		}
		if (RUTGEN.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Appp030")) {
			g_PrmPC080 = (com.FHTServlet.modules.global.PRMPC080.PrmPC080) data
					.getUser().getTemp("Agtp640_g_PrmPC080");
			data.getUser().removeTemp("Agtp640_g_PrmPC080");
			RUTGEN.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
			data.getUser().removeTemp("Agtp640_PrmPC080");
			data.getUser().removeTemp("Agtp640_Evt");
			data.getUser().removeTemp("Agtp640_ImgGtTsd");
			data.getUser().removeTemp("Agtp640_g_ImgBase");
			data.getUser().removeTemp("Agtp640_ImgGtApe");
			data.getUser().removeTemp("Agtp640_vImgGtHyp");
			data.getUser().removeTemp("Agtp640_vImgGtHpd");
			RUTGEN.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
			BF_MSG.Return_Program(data, g_Msg);
			return;
		}
		if (RUTGEN.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Aedp120")) {
			BF_MSG.Return_Program(data, g_Msg);
			return;
		} else {
			return;
		}
	}

	public Vector A_Tab_Img(RunData data) {
		Vector v_Img = new Vector();
		v_Img.clear();
		RUTGEN.MoverA(g_Img.Img_Ntr, g_PrmPC080.PC080_Ntr);
		RUTGEN.MoverA(g_Img.Img_Dax, "BASE");
		RUTGEN.MoverA(g_Img.Img_Seq, "000");
		RUTGEN.MoverA(g_Img.Img_Dat, GT_IMG.LSet_De_ImgGtApe(g_Img_GtApe));
		Vector vPaso = BF_IMG.LSet_A_vImg(BF_IMG.LSet_De_Img(g_Img));
		v_Img.add((com.FHTServlet.modules.global.BF_IMG.Buf_Img) vPaso
				.elementAt(0));
		RUTGEN.MoverA(g_Img.Img_Dax, "XTSD");
		RUTGEN.MoverA(g_Img.Img_Seq, "000");
		RUTGEN.MoverA(g_Img.Img_Dat, GT_IMG.LSet_De_ImgGtTsd(g_Img_GtTsd));
		vPaso = BF_IMG.LSet_A_vImg(BF_IMG.LSet_De_Img(g_Img));
		v_Img.add((com.FHTServlet.modules.global.BF_IMG.Buf_Img) vPaso
				.elementAt(0));
		RUTGEN.MoverA(g_Img.Img_Seq, "000");
		for (i = 0; i < v_Img_GtHyp.size(); i++) {
			g_Img_GtHyp = (com.intGarantias.modules.global.GT_IMG.Buf_Img_GtHyp) v_Img_GtHyp
					.elementAt(i);
			if (g_Img_GtHyp.GtHyp_Itb.toString().equals("HP"))
				RUTGEN.MoverA(g_Img.Img_Dax, "XXHIP");
			else
				RUTGEN.MoverA(g_Img.Img_Dax, "YYPRD");
			RUTGEN.MoverA(g_Img.Img_Seq,
					RUTGEN.Suma(g_Img.Img_Seq, 0, "1", 0, 3, 0));
			RUTGEN.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtHyp(g_Img_GtHyp));
			vPaso = BF_IMG.LSet_A_vImg(BF_IMG.LSet_De_Img(g_Img));
			v_Img.add((com.FHTServlet.modules.global.BF_IMG.Buf_Img) vPaso
					.elementAt(0));
		}

		RUTGEN.MoverA(g_Img.Img_Seq, "000");
		for (i = 0; i < v_Img_GtHpd.size(); i++) {
			g_Img_GtHpd = (com.intGarantias.modules.global.GT_IMG.Buf_Img_GtHpd) v_Img_GtHpd
					.elementAt(i);
			RUTGEN.MoverA(g_Img.Img_Dax, "ZZHPD");
			RUTGEN.MoverA(g_Img.Img_Seq,
					RUTGEN.Suma(g_Img.Img_Seq, 0, "1", 0, 3, 0));
			RUTGEN.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtHpd(g_Img_GtHpd));
			vPaso = BF_IMG.LSet_A_vImg(BF_IMG.LSet_De_Img(g_Img));
			v_Img.add((com.FHTServlet.modules.global.BF_IMG.Buf_Img) vPaso
					.elementAt(0));
		}

		return v_Img;
	}

	public int Carga_Inicial(RunData data, Context context) throws Exception {
		com.intGarantias.modules.global.MSGGT640.Bff_MsgGT640 d_MsgGT640 = new com.intGarantias.modules.global.MSGGT640.Bff_MsgGT640();
		String lcIdx = "0001";
		String lcNtr = "0000000";
		boolean liSwtFin = false;
		v_Tab_Img.clear();
		g_MsgGT640 = MSGGT640.Inicia_MsgGT640();
		RUTGEN.MoverA(g_MsgGT640.GT640_Idr, "I");
		RUTGEN.MoverA(g_MsgGT640.GT640_Idx, lcIdx);
		RUTGEN.MoverA(g_MsgGT640.GT640_Sis, g_PrmPC080.PC080_Cnr.Sis);
		RUTGEN.MoverA(g_MsgGT640.GT640_Ncn, g_PrmPC080.PC080_Cnr.Ncn);
		RUTGEN.MoverA(g_MsgGT640.GT640_Ttr, g_PrmPC080.PC080_Ttr);
		RUTGEN.MoverA(g_MsgGT640.GT640_Ntr, lcNtr);
		while (!liSwtFin) {
			if (Valida_Host(data, context) == 1)
				return 1;
			lcIdx = g_MsgGT640.GT640_Idx.toString();
			lcNtr = g_MsgGT640.GT640_Ntr.toString();
			for (i = 0; i < MSGGT640.g_Max_GT640; i++) {
				d_MsgGT640 = (com.intGarantias.modules.global.MSGGT640.Bff_MsgGT640) g_MsgGT640.GT640_Img
						.elementAt(i);
				if (d_MsgGT640.Img_Dax.toString().trim().equals("")) {
					liSwtFin = true;
					break;
				}
				com.FHTServlet.modules.global.BF_IMG.Buf_Img t_Img = new com.FHTServlet.modules.global.BF_IMG.Buf_Img();
				RUTGEN.MoverA(t_Img.Img_Ntr, d_MsgGT640.Img_Ntr);
				RUTGEN.MoverA(t_Img.Img_Dax, d_MsgGT640.Img_Dax);
				RUTGEN.MoverA(t_Img.Img_Seq, d_MsgGT640.Img_Seq);
				RUTGEN.MoverA(t_Img.Img_Dat, d_MsgGT640.Img_Dat);
				v_Tab_Img.add(t_Img);
			}

			g_MsgGT640 = MSGGT640.Inicia_MsgGT640();
			RUTGEN.MoverA(g_MsgGT640.GT640_Idr, "S");
			RUTGEN.MoverA(g_MsgGT640.GT640_Idx,
					RUTGEN.Suma(lcIdx, 0, "4", 0, 4, 0));
			RUTGEN.MoverA(g_MsgGT640.GT640_Sis, g_PrmPC080.PC080_Cnr.Sis);
			RUTGEN.MoverA(g_MsgGT640.GT640_Ncn, g_PrmPC080.PC080_Cnr.Ncn);
			RUTGEN.MoverA(g_MsgGT640.GT640_Ttr, g_PrmPC080.PC080_Ttr);
			RUTGEN.MoverA(g_MsgGT640.GT640_Ntr, lcNtr);
		}
		g_Img = BF_IMG.Inicia_Img();
		lcData = BF_IMG.LSet_De_Img(g_Img);
		g_Img_Base = BF_IMG.LSet_A_ImgBase(lcData);
		g_Img_GtApe = GT_IMG.LSet_A_ImgGtApe(g_Img.Img_Dat.toString());
		g_Img_GtTsd = GT_IMG.LSet_A_ImgGtTsd(g_Img.Img_Dat.toString());
		v_Img_GtHyp.clear();
		v_Img_GtHpd.clear();
		for (i = 0; i < v_Tab_Img.size(); i++) {
			lcData = BF_IMG
					.LSet_De_Img((com.FHTServlet.modules.global.BF_IMG.Buf_Img) v_Tab_Img
							.elementAt(i));
			g_Img = BF_IMG.LSet_A_Img(lcData);
			if (g_Img.Img_Dax.toString().trim().equals("BASE")) {
				g_Img_GtApe = GT_IMG.LSet_A_ImgGtApe(g_Img.Img_Dat.toString());
				g_Img_Base = BF_IMG.LSet_A_ImgBase(g_Img_GtApe.GtApe_Bse
						.toString());
			}
			if (g_Img.Img_Dax.toString().trim().equals("XTSD"))
				g_Img_GtTsd = GT_IMG.LSet_A_ImgGtTsd(g_Img.Img_Dat.toString());
			if (g_Img.Img_Dax.toString().trim().equals("XXHIP")
					|| g_Img.Img_Dax.toString().trim().equals("YYPRD")) {
				g_Img_GtHyp = GT_IMG.LSet_A_ImgGtHyp(g_Img.Img_Dat.toString());
				v_Img_GtHyp.add(g_Img_GtHyp);
			}
			if (g_Img.Img_Dax.toString().trim().equals("ZZHPD")) {
				g_Img_GtHpd = GT_IMG.LSet_A_ImgGtHpd(g_Img.Img_Dat.toString());
				v_Img_GtHpd.add(g_Img_GtHpd);
			}
		}

		RUTGEN.MoverA(g_Img_Base.Img_Base_Suc, g_PrmPC080.PC080_Suc);
		RUTGEN.MoverA(g_Img_Base.Img_Base_Nsu, g_PrmPC080.PC080_Nsu);
		RUTGEN.MoverA(g_Img_Base.Img_Base_Eje, g_PrmPC080.PC080_Eje);
		RUTGEN.MoverA(g_Img_Base.Img_Base_Nej, g_PrmPC080.PC080_Nej);
		RUTGEN.MoverA(g_Img_Base.Img_Base_Tej, g_PrmPC080.PC080_Tej);
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Bse,
				BF_IMG.LSet_De_ImgBase_Bse(g_Img_Base));
		RUTGEN.MoverA(g_PrmPC080.PC080_Cli, g_Img_Base.Img_Base_Cli);
		RUTGEN.MoverA(g_PrmPC080.PC080_Ncl, g_Img_Base.Img_Base_Ncl);
		RUTGEN.MoverA(g_PrmPC080.PC080_Dir, g_Img_Base.Img_Base_Dir);
		RUTGEN.MoverA(g_PrmPC080.PC080_Tfc, g_Img_Base.Img_Base_Tfc);
		RUTGEN.MoverA(g_PrmPC080.PC080_Ntr, g_Img_Base.Img_Base_Ntr);
		RUTGEN.MoverA(g_PrmPC080.PC080_Dcn, g_Img_Base.Img_Base_Dcn);
		RUTGEN.MoverA(g_PrmPC080.PC080_Ndc, g_Img_Base.Img_Base_Ndc);
		data.getUser().removeTemp("Agtp640_ImgBase");
		data.getUser().setTemp("Agtp640_ImgBase", g_Img_Base);
		data.getUser().removeTemp("Agtp640_ImgGtApe");
		data.getUser().setTemp("Agtp640_ImgGtApe", g_Img_GtApe);
		data.getUser().removeTemp("Agtp640_ImgGtTsd");
		data.getUser().setTemp("Agtp640_ImgGtTsd", g_Img_GtTsd);
		data.getUser().removeTemp("Agtp640_vImgGtHyp");
		data.getUser().setTemp("Agtp640_vImgGtHyp", v_Img_GtHyp);
		data.getUser().removeTemp("Agtp640_vImgGtHpd");
		data.getUser().setTemp("Agtp640_vImgGtHpd", v_Img_GtHpd);
		return 0;
	}

	public int Valida_Host(RunData data, Context context) throws Exception {
		RUTGEN.MoverA(g_Msg.Msg_Dat, MSGGT640.LSet_De_MsgGT640(g_MsgGT640));
		g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GT640");
		g_MsgGT640 = MSGGT640.LSet_A_MsgGT640(g_Msg.Msg_Dat.toString());
		if (g_MsgGT640.GT640_Idr.toString().equals("R")) {
			String lcMensaje = "NO SE";
			if (g_MsgGT640.GT640_Err.toString().equals("01"))
				lcMensaje = "Garantia Erronea (Sin CLI-GTI)";
			if (g_MsgGT640.GT640_Err.toString().equals("02"))
				lcMensaje = "Garantia Erronea (Sin CLI)";
			if (g_MsgGT640.GT640_Err.toString().equals("03"))
				lcMensaje = "Numero Garantia No Existe";
			if (g_MsgGT640.GT640_Err.toString().equals("04"))
				lcMensaje = "Garantia NO Esta Vigente";
			if (g_MsgGT640.GT640_Err.toString().equals("05"))
				lcMensaje = "Evento No Valido para Garantia";
			if (g_MsgGT640.GT640_Err.toString().equals("06"))
				lcMensaje = "Garantia Tiene Evento en Tramite";
			if (g_MsgGT640.GT640_Err.toString().equals("07"))
				lcMensaje = "Garantia Tiene Errores";
			if (g_MsgGT640.GT640_Err.toString().equals("08"))
				lcMensaje = "Evento Valido Solo Hipotecas/Prendas";
			data.getUser().removeTemp("Agtp640_g_PrmPC080");
			data.getUser().setTemp("Agtp640_g_PrmPC080", g_PrmPC080);
			BF_MSG.MsgInt("Agtp640", data, g_Msg, lcMensaje, "1");
			return 1;
		} else {
			return 0;
		}
	}

	public void Init_Evt_GRT(RunData data, Context context) throws Exception {
		RUTGEN.MoverA(g_Evt.Evt_Ttr, g_Img_Base.Img_Base_Ttr);
		RUTGEN.MoverA(g_Evt.Evt_Ntt, g_Img_Base.Img_Base_Ntt);
		RUTGEN.MoverA(g_Evt.Evt_Tsl, g_Img_Base.Img_Base_Tsl);
		RUTGEN.MoverA(g_Evt.Evt_Slc, g_Img_Base.Img_Base_Slc);
		RUTGEN.MoverA(g_Evt.Evt_Cli, g_Img_Base.Img_Base_Cli);
		RUTGEN.MoverA(g_Evt.Evt_Ncl, g_Img_Base.Img_Base_Ncl);
		RUTGEN.MoverA(g_Evt.Evt_Tpr, g_Img_Base.Img_Base_Tpr);
		RUTGEN.MoverA(g_Evt.Evt_Dir, g_Img_Base.Img_Base_Dir);
		RUTGEN.MoverA(g_Evt.Evt_Cmn, " ");
		RUTGEN.MoverA(g_Evt.Evt_Tfc, g_Img_Base.Img_Base_Tfc);
		RUTGEN.MoverA(g_Evt.Evt_Sis, g_Img_Base.Img_Base_Sis);
		RUTGEN.MoverA(g_Evt.Evt_Ncn, g_Img_Base.Img_Base_Ncn);
		RUTGEN.MoverA(g_Evt.Evt_Dcn, g_Img_Base.Img_Base_Dcn);
		RUTGEN.MoverA(g_Evt.Evt_Ndc, g_Img_Base.Img_Base_Ndc);
		RUTGEN.MoverA(g_Evt.Evt_Tmn, g_Img_Base.Img_Base_Tmn);
		RUTGEN.MoverA(g_Evt.Evt_Trj, g_Img_Base.Img_Base_Trj);
		RUTGEN.MoverA(g_Evt.Evt_Dcm, g_Img_Base.Img_Base_Dcm);
		RUTGEN.MoverA(g_Evt.Evt_Prp, g_Img_Base.Img_Base_Prp);
		RUTGEN.MoverA(g_Evt.Evt_Suc, g_Img_Base.Img_Base_Suc);
		RUTGEN.MoverA(g_Evt.Evt_Nsu, g_Img_Base.Img_Base_Nsu);
		RUTGEN.MoverA(g_Evt.Evt_Mnd, g_Img_Base.Img_Base_Mnd);
		RUTGEN.MoverA(g_Evt.Evt_Nmn, g_Img_Base.Img_Base_Nmn);
		RUTGEN.MoverA(g_Evt.Evt_Eje, g_Img_Base.Img_Base_Eje);
		RUTGEN.MoverA(g_Evt.Evt_Nej, g_Img_Base.Img_Base_Nej);
		RUTGEN.MoverA(g_Evt.Evt_Tej, g_Img_Base.Img_Base_Tej);
		RUTGEN.MoverA(g_Evt.Evt_Est, "NUEVA");
		RUTGEN.MoverA(g_Evt.Evt_Fst, g_Msg.Msg_Fch);
		RUTGEN.MoverA(g_Evt.Evt_Upr, g_Img_Base.Img_Base_Trj);
		RUTGEN.MoverA(g_Evt.Evt_Vpr, RUTGEN.Zeros(g_Evt.Evt_Vpr));
		RUTGEN.MoverA(g_Evt.Evt_Fpr, g_Msg.Msg_Fch);
		RUTGEN.MoverA(g_Evt.Evt_Spr, RUTGEN.Zeros(g_Evt.Evt_Spr));
		RUTGEN.MoverA(g_Evt.Evt_Tpp, " ");
		RUTGEN.MoverA(g_Evt.Evt_Ggr, g_Img_GtApe.GtApe_Ggr);
		RUTGEN.MoverA(g_Evt.Evt_Fll, " ");
		data.getUser().removeTemp("Agtp640_Evt");
		data.getUser().setTemp("Agtp640_Evt", g_Evt);
	}

	public int i;
	public String lcData;
	public Vector vDatos;
	RUTGEN rg;
	BF_MSG msg;
	com.FHTServlet.modules.global.BF_MSG.Buf_Msg g_Msg;
	com.FHTServlet.modules.global.BF_EVT.Buf_Evt g_Evt;
	Vector v_Tab_Img;
	Vector v_Img_GtHyp;
	Vector v_Img_GtHpd;
	BF_IMG img;
	com.FHTServlet.modules.global.BF_IMG.Buf_Img g_Img;
	com.FHTServlet.modules.global.BF_IMG.Buf_Img_Bse g_Img_Base;
	GT_IMG gtimg;
	com.intGarantias.modules.global.GT_IMG.Buf_Img_GtApe g_Img_GtApe;
	com.intGarantias.modules.global.GT_IMG.Buf_Img_GtTsd g_Img_GtTsd;
	com.intGarantias.modules.global.GT_IMG.Buf_Img_GtHyp g_Img_GtHyp;
	com.intGarantias.modules.global.GT_IMG.Buf_Img_GtHpd g_Img_GtHpd;
	com.FHTServlet.modules.global.PRMPC080.PrmPC080 g_PrmPC080;
	com.intGlobal.modules.global.MSGED100.Buf_MsgED100 g_MsgED100;
	com.intGlobal.modules.global.MSGED135.Buf_MsgED135 g_MsgED135;
	com.intGlobal.modules.global.PRMCL090.Buf_PrmCL090 g_PrmCL090;
	com.intGarantias.modules.global.MSGGT640.Buf_MsgGT640 g_MsgGT640;
	com.intGlobal.modules.global.PRMTG123.Buf_PrmTG123 g_PrmTG123;
	com.intGlobal.modules.global.PRMTG130.Buf_PrmTG130 g_PrmTG130;
	com.intGlobal.modules.global.PRMTG112.Buf_PrmTG112 g_PrmTG112;
}
