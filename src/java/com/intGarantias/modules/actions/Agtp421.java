// Source File Name:   Agtp421.java
// Descripcion     :   Consulta Detalle Hipoteca - Terreno (GT421)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.BF_LOB;

import com.intGarantias.modules.global.MSGGT421;
import com.intGarantias.modules.global.PRMGT421;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp421 extends FHTServletAction
{
  //---------------------------------------------------------------------------------------
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  BF_LOB lob                       = new BF_LOB();
  MSGGT421.Buf_MsgGT421 g_MsgGT421 = new MSGGT421.Buf_MsgGT421();
  PRMGT421.Buf_PrmGT421 g_PrmGT421 = new PRMGT421.Buf_PrmGT421();
  //---------------------------------------------------------------------------------------
  public Agtp421()
  {
  }
  //---------------------------------------------------------------------------------------
  public void doPerform(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP421[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp421-DIN", data);
    doAgtp421_Init(data, context);
  }
  //---------------------------------------------------------------------------------------
  public void doCancel(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP421[doCancel.start]", "[CANCELADO]");
    setTemplate(data, "Garantias,Agt,AGTP421.vm");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp421_Init(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP421[doAgtp421_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_PrmGT421 = PRMGT421.LSet_A_PrmGT421(g_Msg.Msg_Dat.toString());
    g_MsgGT421 = MSGGT421.Inicia_MsgGT421();
    rg.MoverA(g_MsgGT421.GT421_Idr, "I");
    rg.MoverA(g_MsgGT421.GT421_Gti, g_PrmGT421.GT421_Gti);
    rg.MoverA(g_MsgGT421.GT421_Seq, g_PrmGT421.GT421_Seq);
    rg.MoverA(g_MsgGT421.GT421_Sqd, g_PrmGT421.GT421_Sqd);
    rg.MoverA(g_Msg.Msg_Dat, MSGGT421.LSet_De_MsgGT421(g_MsgGT421));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GT421");
    g_MsgGT421 = MSGGT421.LSet_A_MsgGT421(g_Msg.Msg_Dat.toString());
    data.getUser().removeTemp("Agtp421_MsgGT421");
    data.getUser().setTemp("Agtp421_MsgGT421", g_MsgGT421);
    if (g_MsgGT421.GT421_Obt.toString().trim().equals("")
      ||g_MsgGT421.GT421_Obt.toString().trim().equals("000000000"))
       { data.getUser().setTemp("Agtp421_Obt", ""); }
    else
       { data.getUser().setTemp("Agtp421_Obt", lob.leeCLOB(g_MsgGT421.GT421_Obt.toString())); }
    if (g_MsgGT421.GT421_Ovb.toString().trim().equals("")
      ||g_MsgGT421.GT421_Ovb.toString().trim().equals("000000000"))
       { data.getUser().setTemp("Agtp421_Ovb", ""); }
    else
       { data.getUser().setTemp("Agtp421_Ovb", lob.leeCLOB(g_MsgGT421.GT421_Ovb.toString())); }
    setTemplate(data, "Garantias,Agt,AGTP421.vm" );
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp421(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP421[doAgtp421.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp421-MAN", data);
    String Opc = data.getParameters().getString("Opc" ,"");
    if (Opc.trim().equals("S"))
       {
         //Boton Salir
         data.getUser().removeTemp("Agtp421_MsgGT421");
         BF_MSG.Return_Data("Agtp421", data, g_Msg);
         return;
       }
  }
  //---------------------------------------------------------------------------------------
}