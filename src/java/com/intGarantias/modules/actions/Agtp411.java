// Source File Name:   Agtp411.java
// Descripcion     :   Consulta Detalle Hipoteca (GT411)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.BF_LOB;
import com.FHTServlet.modules.global.PRMPC080;

import com.intGarantias.modules.global.MSGGT411;
import com.intGarantias.modules.global.PRMGT421;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp411 extends FHTServletAction
{
  //---------------------------------------------------------------------------------------
  public int i = 0;
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  BF_LOB lob                       = new BF_LOB();
  PRMPC080.PrmPC080 g_PrmPC080     = new PRMPC080.PrmPC080();
  MSGGT411.Buf_MsgGT411 g_MsgGT411 = new MSGGT411.Buf_MsgGT411();
  MSGGT411.Bfv_Ghd v_Ghd           = new MSGGT411.Bfv_Ghd();
  PRMGT421.Buf_PrmGT421 g_PrmGT421 = new PRMGT421.Buf_PrmGT421();
  //---------------------------------------------------------------------------------------
  public Agtp411()
  {
  }
  //---------------------------------------------------------------------------------------
  public void doPerform(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP411[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp411-DIN", data);
    if (g_Msg.Msg_Pgr.toString().equals("RT"))
       { doAgtp411_Continue(data, context); }
    else
       { doAgtp411_Init(data, context); }
  }
  //---------------------------------------------------------------------------------------
  public void doCancel(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP411[doCancel.start]", "[CANCELADO]");
    setTemplate(data, "Garantias,Agt,AGTP411.vm");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp411_Init(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP411[doAgtp411_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    v_Ghd.Ghd_Rgs.clear();
    int p = 0;
    g_MsgGT411 = MSGGT411.Inicia_MsgGT411();
    rg.MoverA(g_MsgGT411.GT411_Idr, "I");
    rg.MoverA(g_MsgGT411.GT411_Gti, g_PrmPC080.PC080_Cnr.Sis.toString() + g_PrmPC080.PC080_Cnr.Ncn.toString());
    rg.MoverA(g_MsgGT411.GT411_Seq, g_PrmPC080.PC080_Ntc);
    while (g_MsgGT411.GT411_Idr.toString().compareTo("L")!=0)
          {
            if (g_MsgGT411.GT411_Idr.toString().compareTo("I")!=0)
               {
                 Vector vTmp = g_MsgGT411.getGhd();
                 g_MsgGT411 = MSGGT411.Inicia_MsgGT411();
                 rg.MoverA(g_MsgGT411.GT411_Idr, "S");
                 rg.MoverA(g_MsgGT411.GT411_Gti, g_PrmPC080.PC080_Cnr.Sis.toString() + g_PrmPC080.PC080_Cnr.Ncn.toString());
                 rg.MoverA(g_MsgGT411.GT411_Seq, g_PrmPC080.PC080_Ntc);
                 i = vTmp.size() - 1;
                 MSGGT411.Grt_Ghd_Det d_Ghd = (MSGGT411.Grt_Ghd_Det)vTmp.elementAt(i);
                 g_MsgGT411.GT411_Ghd.set(0, d_Ghd);
               }
            rg.MoverA(g_Msg.Msg_Dat, MSGGT411.LSet_De_MsgGT411(g_MsgGT411));
            g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GT411");
            g_MsgGT411 = MSGGT411.LSet_A_MsgGT411(g_Msg.Msg_Dat.toString());
            for (i=p; i<g_MsgGT411.GT411_Ghd.size(); i++)
                {
                  MSGGT411.Grt_Ghd_Det d_Ghd = (MSGGT411.Grt_Ghd_Det)g_MsgGT411.GT411_Ghd.elementAt(i);
                  v_Ghd.Ghd_Rgs.add(d_Ghd);
                }
            p = 1;
          }
    data.getUser().removeTemp("Agtp411_MsgGT411");
    data.getUser().setTemp("Agtp411_MsgGT411", g_MsgGT411);
    data.getUser().removeTemp("Agtp411_Ghd");
    data.getUser().setTemp("Agtp411_Ghd", v_Ghd);
    if (g_MsgGT411.GT411_Gbs.Gbs_Obs.toString().trim().equals("")
      ||g_MsgGT411.GT411_Gbs.Gbs_Obs.toString().trim().equals("000000000"))
       { data.getUser().setTemp("Agtp411_Obs", ""); }
    else
       { data.getUser().setTemp("Agtp411_Obs", lob.leeCLOB(g_MsgGT411.GT411_Gbs.Gbs_Obs.toString())); }
    data.getUser().removeTemp("Agtp411_PrmPC080");
    data.getUser().setTemp("Agtp411_PrmPC080",g_PrmPC080);					
    setTemplate(data, "Garantias,Agt,AGTP411.vm" );
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp411_Continue(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP411[doAgtp411_Continue.start]", "[" + data.getUser().getUserName() + "]");
    g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp411_g_PrmPC080");
    rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
    BF_MSG.Param_Program(data, g_Msg);
    data.getUser().removeTemp("Agtp411_g_PrmPC080");
    setTemplate(data, "Garantias,Agt,AGTP411.vm" );
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp411(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP411[doAgtp411.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp411-MAN", data);
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    String Opc = data.getParameters().getString("Opc" ,"");
    if (Opc.equals("E"))
       {
         //Selecion. Ir a pantalla consulta garantia especifica
         data.getUser().removeTemp("Agtp411_g_PrmPC080");
         data.getUser().setTemp("Agtp411_g_PrmPC080" ,g_PrmPC080);
         String Sqd = data.getParameters().getString("Sqd" ,"");
         i = Integer.parseInt(Sqd);
         g_MsgGT411 = (MSGGT411.Buf_MsgGT411)data.getUser().getTemp("Agtp411_MsgGT411");
         Vector vTmp = g_MsgGT411.getGhd();
         MSGGT411.Grt_Ghd_Det d_MsgGT411 = new MSGGT411.Grt_Ghd_Det();
         d_MsgGT411 = (MSGGT411.Grt_Ghd_Det)vTmp.elementAt(i);
         g_PrmGT421 = PRMGT421.Inicia_PrmGT421();
         rg.MoverA(g_PrmGT421.GT421_Acc, "?");
         rg.MoverA(g_PrmGT421.GT421_Gti, g_PrmPC080.PC080_Cnr.Sis.toString() + g_PrmPC080.PC080_Cnr.Ncn.toString());
         rg.MoverA(g_PrmGT421.GT421_Seq, g_PrmPC080.PC080_Ntc);
         rg.MoverA(g_PrmGT421.GT421_Sqd, d_MsgGT411.Ghd_Sqd);
         rg.MoverA(g_PrmGT421.GT421_Cli, g_PrmPC080.PC080_Cli);
         rg.MoverA(g_PrmGT421.GT421_Ncl, g_PrmPC080.PC080_Ncl);
         rg.MoverA(g_PrmGT421.GT421_Dcn, g_PrmPC080.PC080_Dcn);
         rg.MoverA(g_PrmGT421.GT421_Ndc, g_PrmPC080.PC080_Ndc);
         rg.MoverA(g_Msg.Msg_Dat, PRMGT421.LSet_De_PrmGT421(g_PrmGT421));
         if (d_MsgGT411.Ghd_Ith.toString().trim().equals("TE"))
            BF_MSG.Link_Program("Agtp411", "Agtp421", data, g_Msg);
         if (d_MsgGT411.Ghd_Ith.toString().trim().equals("ED"))
            BF_MSG.Link_Program("Agtp411", "Agtp422", data, g_Msg);
         if (d_MsgGT411.Ghd_Ith.toString().trim().equals("EP"))
            BF_MSG.Link_Program("Agtp411", "Agtp423", data, g_Msg);
         if (d_MsgGT411.Ghd_Ith.toString().trim().equals("SA"))
            BF_MSG.Link_Program("Agtp411", "Agtp424", data, g_Msg);
         if (d_MsgGT411.Ghd_Ith.toString().trim().equals("OC"))
            BF_MSG.Link_Program("Agtp411", "Agtp425", data, g_Msg);
         return;
       }       
    if (Opc.trim().equals("S"))
       {
         //Boton Salir
         data.getUser().removeTemp("Agtp411_MsgGT411");
         data.getUser().removeTemp("Agtp411_PrmPC080");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Program(data, g_Msg);
         return;
       }
  }
  //---------------------------------------------------------------------------------------
}