// Source File Name:   Agtp456.java
// Descripcion:        Consulta/Seleccion InfLegal (GT456)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.BF_LOB;
import com.FHTServlet.modules.global.PRMPC080;

import com.intGarantias.modules.global.MSGGT456;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp456 extends FHTServletAction
{
  //---------------------------------------------------------------------------------------
  public int    i = 0;
  public String lcRetorno = "";
  public Vector vDatos;
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  BF_LOB lob                       = new BF_LOB();
  PRMPC080.PrmPC080 g_PrmPC080     = new PRMPC080.PrmPC080();
  MSGGT456.Buf_MsgGT456 g_MsgGT456 = new MSGGT456.Buf_MsgGT456();
  //---------------------------------------------------------------------------------------
  public Agtp456()
  {
  }
  //---------------------------------------------------------------------------------------
  public void doPerform(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP456[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp456-DIN", data);
    if (g_Msg.Msg_Pgr.toString().equals("RT"))
       { doAgtp456_Continue(data, context); }
    else
       { doAgtp456_Init(data, context); }
  }
  //---------------------------------------------------------------------------------------
  public void doCancel(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP456[doCancel.start]", "[CANCELADO]");
    setTemplate(data, "Garantias,Agt,AGTP456.vm");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp456_Init(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP456[doAgtp456_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    g_MsgGT456 = MSGGT456.Inicia_MsgGT456();
    rg.MoverA(g_MsgGT456.GT456_Idr, "I");
    rg.MoverA(g_MsgGT456.GT456_Sis, g_PrmPC080.PC080_Cnr.Sis);
    rg.MoverA(g_MsgGT456.GT456_Ncn, g_PrmPC080.PC080_Cnr.Ncn);
    Poblar_Panel(data, context);
    setTemplate(data, "Garantias,Agt,AGTP456.vm" );
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp456(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP456[doAgtp456.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp456-MAN", data);
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    String Opc = data.getParameters().getString("Opc" ,"");
    if  (Opc.equals("S"))
        {
          //Salir
          data.getUser().removeTemp("Agtp456_PrmPC080");
          data.getUser().removeTemp("Agtp456_MsgGT456");
          data.getUser().removeTemp("Agtp456_DetGT456");
          data.getUser().removeTemp("Agtp456_Dsl");
          rg.MoverA(g_PrmPC080.PC080_Rtn, "NK");
          rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
          BF_MSG.Return_Data("Agtp456", data, g_Msg);
          return;
        }
    g_MsgGT456 = (MSGGT456.Buf_MsgGT456)data.getUser().getTemp("Agtp456_MsgGT456");
    MSGGT456.Bff_MsgGT456 d_MsgGT456 = new MSGGT456.Bff_MsgGT456();
    if  (Opc.equals("D"))
        {
          //Detalle
          String Seq = data.getParameters().getString("Seq", "");
          i = Integer.parseInt(Seq);
          d_MsgGT456 = (MSGGT456.Bff_MsgGT456)g_MsgGT456.GT456_Tap.elementAt(i);
          rg.MoverA(g_PrmPC080.PC080_Rtn, "");
          rg.MoverA(g_PrmPC080.PC080_Ntr, d_MsgGT456.GT456_Iln);
          rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
          data.getUser().removeTemp("Agtp456_DetGT456");
          data.getUser().setTemp("Agtp456_DetGT456", d_MsgGT456);
          if (d_MsgGT456.GT456_Dsl.toString().trim().equals("000000000"))
             { data.getUser().setTemp("Agtp456_Dsl", ""); }
          else
             { data.getUser().setTemp("Agtp456_Dsl", lob.leeCLOB(d_MsgGT456.GT456_Dsl.toString())); }
          BF_MSG.Link_Program("Agtp456", "Agtp457", data, g_Msg);
          return;
        }
    if  (Opc.equals("N"))
        {
          //Siguiente
          i = Integer.parseInt(g_MsgGT456.GT456_Nro.toString())-1;
          d_MsgGT456 = (MSGGT456.Bff_MsgGT456)g_MsgGT456.GT456_Tap.elementAt(i);
          g_MsgGT456 = MSGGT456.Inicia_MsgGT456();
          rg.MoverA(g_MsgGT456.GT456_Idr, "S");
          rg.MoverA(g_MsgGT456.GT456_Sis, g_PrmPC080.PC080_Cnr.Sis);
          rg.MoverA(g_MsgGT456.GT456_Ncn, g_PrmPC080.PC080_Cnr.Ncn);
          g_MsgGT456.GT456_Tap.set(0, d_MsgGT456);
          Poblar_Panel(data, context);
          setTemplate(data, "garantias,Agt,AGTP456.vm" );
        }
    if  (Opc.equals("P"))
        {
          //Previo
          i = 0;
          d_MsgGT456 = (MSGGT456.Bff_MsgGT456)g_MsgGT456.GT456_Tap.elementAt(i);
          g_MsgGT456 = MSGGT456.Inicia_MsgGT456();
          rg.MoverA(g_MsgGT456.GT456_Idr, "P");
          rg.MoverA(g_MsgGT456.GT456_Sis, g_PrmPC080.PC080_Cnr.Sis);
          rg.MoverA(g_MsgGT456.GT456_Ncn, g_PrmPC080.PC080_Cnr.Ncn);
          g_MsgGT456.GT456_Tap.set(0, d_MsgGT456);
          Poblar_Panel(data, context);
          setTemplate(data, "garantias,Agt,AGTP456.vm" );
        }
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp456_Continue(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP456[doAgtp456_Continue.start]", "[" + data.getUser().getUserName() + "]");
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Appp030"))
       {
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp456_g_PrmPC080");
         data.getUser().removeTemp("Agtp456_g_PrmPC080");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP456.vm" );
         return;
       }
    setTemplate(data, "Garantias,Agt,AGTP456.vm" );
    return;
  }
  //---------------------------------------------------------------------------------------
  public void Poblar_Panel(RunData data, Context context)
              throws Exception
  {
    rg.MoverA(g_Msg.Msg_Dat, MSGGT456.LSet_De_MsgGT456(g_MsgGT456));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GT456");
    g_MsgGT456 = MSGGT456.LSet_A_MsgGT456(g_Msg.Msg_Dat.toString());
    data.getUser().removeTemp("Agtp456_MsgGT456");
    data.getUser().setTemp("Agtp456_MsgGT456", g_MsgGT456);
    data.getUser().removeTemp("Agtp456_PrmPC080");
    data.getUser().setTemp("Agtp456_PrmPC080", g_PrmPC080);
  }
  //---------------------------------------------------------------------------------------
}