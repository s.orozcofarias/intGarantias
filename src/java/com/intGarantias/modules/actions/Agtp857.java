// Source File Name:   Agtp857.java
// Descripcion     :   Menu Abogado (Hipotecarios) [Fte: ABG] (-)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.PRMPC080;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp857 extends FHTServletAction
{
  //---------------------------------------------------------------------------------------
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  PRMPC080.PrmPC080 g_PrmPC080     = new PRMPC080.PrmPC080();
  //---------------------------------------------------------------------------------------
  public Agtp857()
  {
  }
  //---------------------------------------------------------------------------------------
  public void doPerform(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP857[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    doAgtp857_Init(data, context);
  }
  //---------------------------------------------------------------------------------------
  public void doCancel(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP857[doCancel.start]", "[CANCELADO]");
    setTemplate(data, "Garantias,Agt,AGTP857.vm");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp857_Init(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP857[doAgtp857_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp857-DIN", data);
    Re_PRMPC080(data, context);
    setTemplate(data, "Garantias,Agt,AGTP857.vm" );
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp857(RunData data, Context context)
              throws Exception
  {
    g_Msg = BF_MSG.InitIntegraTasador("Agtp857", data, context);
    Re_PRMPC080(data, context);
    String Opc = data.getParameters().getString("Opc", "");
    if (Opc.trim().equals("S"))
       {
         //Boton Salir
         BF_MSG.Return_Program(data, g_Msg);
         return;
       }
    if (Opc.equals("1"))
       {
         //Garantias para Registro
         Re_PRMPC080(data, context);
         BF_MSG.Link_Program("Agtp857", "Agtp210", data, g_Msg);
         return;
       }
  }
  //---------------------------------------------------------------------------------------
  public void Re_PRMPC080(RunData data, Context context)
              throws Exception
  {
    g_PrmPC080 = PRMPC080.Inicia_PrmPC080();
    RUTGEN.MoverA(g_PrmPC080.PC080_Fte,     "ABG");
    RUTGEN.MoverA(g_PrmPC080.PC080_Pai,     "CL");
    RUTGEN.MoverA(g_PrmPC080.PC080_Cnr.Sis, "GAR");
    RUTGEN.MoverA(g_PrmPC080.PC080_Slr.Slc, RUTGEN.Zeros(7));
    RUTGEN.MoverA(g_PrmPC080.PC080_Suc,     g_Msg.Msg_Suc);
    RUTGEN.MoverA(g_PrmPC080.PC080_Nsu,     g_Msg.Msg_Nsu);
    RUTGEN.MoverA(g_PrmPC080.PC080_Mnd,     "000");
    RUTGEN.MoverA(g_PrmPC080.PC080_Nmn,     "PESOS CHILE");
    RUTGEN.MoverA(g_PrmPC080.PC080_Eje,     g_Msg.Msg_Cus);
    RUTGEN.MoverA(g_PrmPC080.PC080_Nej,     g_Msg.Msg_Nus);
    RUTGEN.MoverA(g_PrmPC080.PC080_Tej,     g_Msg.Msg_Fus);
    RUTGEN.MoverA(g_PrmPC080.PC080_Abg,     g_Msg.Msg_Cus.substring(0));
    RUTGEN.MoverA(g_Msg.Msg_Dat,            PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
    BF_MSG.Param_Program(data, g_Msg);
  }
  //---------------------------------------------------------------------------------------
}