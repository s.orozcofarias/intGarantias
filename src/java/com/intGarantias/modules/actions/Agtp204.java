// Source File Name:   Agtp724.java
// Descripcion     :   Ingreso Hipotecas y Prendas (ED135, GT096, GT147)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.MSGTXCUR;
import com.FHTServlet.modules.global.BF_TRN;
import com.FHTServlet.modules.global.BF_IMG;
import com.FHTServlet.modules.global.PRMPC080;
import com.FHTServlet.modules.global.PRMPP030;

import com.intGlobal.modules.global.MSGED090;
import com.intGlobal.modules.global.MSGED135;
import com.intGlobal.modules.global.PRMED210;

import com.intGarantias.modules.global.GT_IMG;
import com.intGarantias.modules.global.MSGGT096;
import com.intGarantias.modules.global.vecGT724;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp204 extends FHTServletAction
{
  //---------------------------------------------------------------------------------------
  public int    i = 0;
  public int    j = 0;
  public int    p = 0;
  public int    q = 0;
  public String lcData = "";
  public Vector vDatos;
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  MSGTXCUR.Buf_TxCur g_MsgTxCur    = new MSGTXCUR.Buf_TxCur();
  MSGTXCUR.Buf_Tx900 g_MsgTx900    = new MSGTXCUR.Buf_Tx900();
  BF_TRN trn                       = new BF_TRN();
  BF_TRN.Buf_Trn g_Trn             = new BF_TRN.Buf_Trn();
  PRMPC080.PrmPC080 g_PrmPC080     = new PRMPC080.PrmPC080();
  vecGT724.Buf_vecGT724 v_Imgs     = new vecGT724.Buf_vecGT724();
  Vector g_Tab_Img                 = new Vector();
  Vector v_Img_GtHyp               = new Vector();
  Vector v_Img_GtHpd               = new Vector();
  BF_IMG img                       = new BF_IMG();
  BF_IMG.Buf_Img g_Img             = new BF_IMG.Buf_Img();
  BF_IMG.Buf_Img_Bse g_Img_Base    = new BF_IMG.Buf_Img_Bse();
  GT_IMG gtimg                     = new GT_IMG();
  GT_IMG.Buf_Img_GtApe g_Img_GtApe = new GT_IMG.Buf_Img_GtApe();
  GT_IMG.Buf_Img_GtTsd g_Img_GtTsd = new GT_IMG.Buf_Img_GtTsd();
  GT_IMG.Buf_Img_GtHyp g_Img_GtHyp = new GT_IMG.Buf_Img_GtHyp();
  GT_IMG.Buf_Img_GtHpd g_Img_GtHpd = new GT_IMG.Buf_Img_GtHpd();
  GT_IMG.Buf_Img_GtPts g_Img_GtPts = new GT_IMG.Buf_Img_GtPts();
  GT_IMG.Buf_Img_GtMff g_Img_GtMff = new GT_IMG.Buf_Img_GtMff();
  MSGED090.Buf_MsgED090 g_MsgED090 = new MSGED090.Buf_MsgED090();
  MSGED135.Buf_MsgED135 g_MsgED135 = new MSGED135.Buf_MsgED135();
  MSGGT096.Buf_MsgGT096 g_MsgGT096 = new MSGGT096.Buf_MsgGT096();
  PRMED210.Buf_PrmED210 g_PrmED210 = new PRMED210.Buf_PrmED210();
  PRMPP030.Buf_PrmPP030 g_PrmPP030 = new PRMPP030.Buf_PrmPP030();
  //---------------------------------------------------------------------------------------
  public Agtp204()
  {
  }
  //---------------------------------------------------------------------------------------
  public void doPerform(RunData data, Context context)
              throws Exception
  {
    Log.debug("AGTP204[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp204-DIN", data);
    if (g_Msg.Msg_Pgr.toString().equals("RT"))
       { doAgtp724_Continue(data, context); }
    else
       { doAgtp724_Init(data, context); }
  }
  //---------------------------------------------------------------------------------------
  public void doCancel(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP724[doCancel.start]", "[CANCELADO]");
    setTemplate(data, "Garantias,Agt,AGTP204.vm");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp724_Init(RunData data, Context context)
              throws Exception
  {
    Log.debug("AGTP724[doAgtp204_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    Carga_de_Host(data, context);
    Carga_Inicial(data, context);
	
	 String Opc = data.getParameters().getString("Opc" ,"");
	 Log.debug("AGTP204 REvisa 2", "INICIO OPC " +Opc);
	 if (Opc.trim().equals("S"))
       {

		 Limpieza(data, context);
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp724", data, g_Msg);
         return;
       }
	
    if (g_PrmPC080.PC080_Fte.toString().trim().equals("SPV")
     && g_PrmPC080.PC080_Rtn.toString().trim().equals("WR"))
       { rg.MoverA(g_PrmPC080.PC080_Rtn, "RA"); }
    else
       {
         if (g_PrmPC080.PC080_Fte.toString().trim().compareTo("TSD")!=0)
            { rg.MoverA(g_PrmPC080.PC080_Rtn, "RO"); }
       }
    data.getUser().removeTemp("Agtp724_PrmPC080");
    data.getUser().setTemp("Agtp724_PrmPC080", g_PrmPC080);
    rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
    BF_MSG.Param_Program(data, g_Msg);
    aux_vImgs(data, context);
    setTemplate(data, "Garantias,Agt,AGTP204.vm");
  }
  //---------------------------------------------------------------------------------------
  public void Carga_Inicial(RunData data, Context context)
              throws Exception 
  {
    g_MsgED135 = MSGED135.Inicia_MsgED135();
    rg.MoverA(g_MsgED135.ED135_Idr, "C");
    rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
    rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
    g_MsgED135 = MSGED135.LSet_A_MsgED135(g_Msg.Msg_Dat.toString());
    data.getUser().removeTemp("Agtp724_MsgED135");
    data.getUser().setTemp("Agtp724_MsgED135", g_MsgED135);
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp724(RunData data, Context context)
              throws Exception
  {
    Log.debug("AGTP204[doAgtp204.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp204-MAN", data);
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    String Opc = data.getParameters().getString("Opc" ,"");
	
	Log.debug("AGTP204 REvisa", "INICIO OPC " +Opc);
    if (Opc.trim().equals("R"))
       {
         //Boton Errores
         g_MsgED135 = (MSGED135.Buf_MsgED135)data.getUser().getTemp("Agtp724_MsgED135");
         g_PrmED210 = PRMED210.Inicia_PrmED210();
         rg.MoverA(g_PrmED210.ED210_Rtn, "RO");
         rg.MoverA(g_PrmED210.ED210_Ldt, g_MsgED135.ED135_Ldv);
         data.getUser().removeTemp("Agtp724_g_PrmPC080");
         data.getUser().setTemp("Agtp724_g_PrmPC080", g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMED210.LSet_De_PrmED210(g_PrmED210));
         BF_MSG.Link_Program("Agtp724", "Aedp210", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("D"))
       {
         //Boton Devolver
         g_PrmED210 = PRMED210.Inicia_PrmED210();
         rg.MoverA(g_PrmED210.ED210_Rtn, "RW");
         rg.MoverA(g_PrmPC080.PC080_Amv, "D");
         data.getUser().removeTemp("Agtp724_g_PrmPC080");
         data.getUser().setTemp("Agtp724_g_PrmPC080", g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMED210.LSet_De_PrmED210(g_PrmED210));
         BF_MSG.Link_Program("Agtp724", "Aedp210", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("I"))
       {
         //Boton Informe Tasador
         vecImpresion(data, context);
         data.getUser().removeTemp("Agtp724_g_PrmPC080");
         data.getUser().setTemp("Agtp724_g_PrmPC080", g_PrmPC080);
         //Para no ocupar area direccionada que no cambia --------------------------------
         PRMPC080.PrmPC080 l_PrmPC080 = new PRMPC080.PrmPC080();
         l_PrmPC080 = PRMPC080.LSet_A_PrmPC080(PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         rg.MoverA(l_PrmPC080.PC080_Rtn, "RW");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(l_PrmPC080));
         //-------------------------------------------------------------------------------
       //BF_MSG.Link_Program("Agtp724", "Agtp725", data, g_Msg);
         BF_MSG.Link_Program("Agtp724", "Appp096", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("V"))
       {
         //Boton Envio (Recibido)
         g_Img_GtTsd = (GT_IMG.Buf_Img_GtTsd)data.getUser().getTemp("Agtp724_ImgGtTsd");
         Actualiza_Datos("1", data, context);
         data.getUser().removeTemp("Agtp724_ImgGtTsd");
         data.getUser().setTemp("Agtp724_ImgGtTsd", g_Img_GtTsd);
         data.getUser().removeTemp("Agtp724_g_PrmPC080");
         data.getUser().setTemp("Agtp724_g_PrmPC080", g_PrmPC080);
         //Para no ocupar area direccionada que no cambia --------------------------------
         PRMPC080.PrmPC080 l_PrmPC080 = new PRMPC080.PrmPC080();
         l_PrmPC080 = PRMPC080.LSet_A_PrmPC080(PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         rg.MoverA(l_PrmPC080.PC080_Rtn, "RO");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(l_PrmPC080));
         //-------------------------------------------------------------------------------
         BF_MSG.Link_Program("Agtp724", "Agtp214", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("G"))
       {
         //Boton Guardar
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp724_ImgGtApe");
         g_Img_GtTsd = (GT_IMG.Buf_Img_GtTsd)data.getUser().getTemp("Agtp724_ImgGtTsd");
         Actualiza_Datos("1", data, context);
         v_Imgs = (vecGT724.Buf_vecGT724)data.getUser().getTemp("Agtp724_vImgs");
         vImgs_A_Host(data, context);
         setTemplate(data, "Garantias,Agt,AGTP204.vm");
         return;
       }
    if (Opc.trim().equals("P"))
       {
         //Boton Pendiente
         g_Img_GtTsd = (GT_IMG.Buf_Img_GtTsd)data.getUser().getTemp("Agtp724_ImgGtTsd");
         Actualiza_Datos("1", data, context);
         data.getUser().removeTemp("Agtp724_ImgGtTsd");
         data.getUser().setTemp("Agtp724_ImgGtTsd", g_Img_GtTsd);
         g_PrmED210 = PRMED210.Inicia_PrmED210();
         rg.MoverA(g_PrmED210.ED210_Rtn, "RW");
         rg.MoverA(g_PrmPC080.PC080_Amv, "P");
         data.getUser().removeTemp("Agtp724_g_PrmPC080");
         data.getUser().setTemp("Agtp724_g_PrmPC080", g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMED210.LSet_De_PrmED210(g_PrmED210));
         BF_MSG.Link_Program("Agtp724", "Aedp210", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("T"))
       {
         //Boton Detalle
         String Seq = data.getParameters().getString("Seq", "");
         i = Integer.parseInt(Seq);
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp724_ImgGtApe");
         g_Img_GtTsd = (GT_IMG.Buf_Img_GtTsd)data.getUser().getTemp("Agtp724_ImgGtTsd");
         if (g_PrmPC080.PC080_Rtn.toString().trim().compareTo("RO")!=0)
            { Actualiza_Datos("1", data, context); }
         v_Imgs = (vecGT724.Buf_vecGT724)data.getUser().getTemp("Agtp724_vImgs");
         rg.MoverA(v_Imgs.GT724_Idx, rg.FmtValor(Seq,0,0,3,"+"));
         vecGT724.Bff_Hyp l_Hyp = new vecGT724.Bff_Hyp();
         l_Hyp = (vecGT724.Bff_Hyp)v_Imgs.GT724_Hyp.elementAt(i);
         g_Img_GtHyp = gtimg.LSet_A_ImgGtHyp(l_Hyp.Hyp_Hyp.toString());
         v_Img_GtHpd.clear();
         for (j=0; j<l_Hyp.Hyp_Hpd.size(); j++)
             {
               vecGT724.Bff_Hpd l_Hpd = new vecGT724.Bff_Hpd();
               l_Hpd = (vecGT724.Bff_Hpd)l_Hyp.Hyp_Hpd.elementAt(j);
               g_Img_GtHpd = gtimg.LSet_A_ImgGtHpd(l_Hpd.Hpd_Hpd.toString());
               v_Img_GtHpd.add(g_Img_GtHpd); 
             }
         data.getUser().removeTemp("Agtp724_ImgGtApe");
         data.getUser().setTemp("Agtp724_ImgGtApe", g_Img_GtApe);
         data.getUser().removeTemp("Agtp724_ImgGtTsd");
         data.getUser().setTemp("Agtp724_ImgGtTsd", g_Img_GtTsd);
         data.getUser().removeTemp("Agtp724_vImgs");
         data.getUser().setTemp("Agtp724_vImgs", v_Imgs);
         data.getUser().removeTemp("Agtp724_ImgGtHyp");
         data.getUser().setTemp("Agtp724_ImgGtHyp", g_Img_GtHyp);
         data.getUser().removeTemp("Agtp724_vImgGtHpd");
         data.getUser().setTemp("Agtp724_vImgGtHpd", v_Img_GtHpd);
         data.getUser().removeTemp("Agtp724_g_PrmPC080");
         data.getUser().setTemp("Agtp724_g_PrmPC080", g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         if (g_Img_GtHyp.GtHyp_Itb.toString().equals("HP"))
            { BF_MSG.Link_Program("Agtp724", "Agtp727", data, g_Msg); }
         if (g_Img_GtHyp.GtHyp_Itb.toString().equals("CD"))
            { BF_MSG.Link_Program("Agtp724", "Agtp764", data, g_Msg); }
         if (g_Img_GtHyp.GtHyp_Itb.toString().equals("PD"))
            { BF_MSG.Link_Program("Agtp724", "Agtp765", data, g_Msg); }
         if (g_Img_GtHyp.GtHyp_Itb.toString().equals("PL"))
            { BF_MSG.Link_Program("Agtp724", "Agtp766", data, g_Msg); }
         return;
       }
    if (Opc.trim().equals("S"))
       {
         //Boton Salir
         //Boton Salir
         //Limpieza(data, context);
         //rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         //rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         //BF_MSG.Return_Data("Agtp204", data, g_Msg);
		 //BF_MSG.Link_Program("Agtp204", "Agtp200", data, g_Msg); 
    	
		 Log.debug("AGTP204 Opc S", "EEEEEAASASASA FABIAN ASAAA ");
		 Re_PRMPC080(data, context);
         BF_MSG.Link_Program("Agtp082", "Agtp201", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("O"))
       {
         //Boton OK (Aceptar)
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp724_ImgGtApe");
         g_Img_GtTsd = (GT_IMG.Buf_Img_GtTsd)data.getUser().getTemp("Agtp724_ImgGtTsd");
         Actualiza_Datos("1", data, context);
         v_Imgs = (vecGT724.Buf_vecGT724)data.getUser().getTemp("Agtp724_vImgs");
         vImgs_A_Host(data, context);
         g_MsgED135 = MSGED135.Inicia_MsgED135();
         rg.MoverA(g_MsgED135.ED135_Idr, "M");
         rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
         rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
         rg.MoverA(g_MsgED135.ED135_Est, "");
         rg.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
         rg.MoverA(g_MsgED135.ED135_Vlc, g_Img_GtApe.GtApe_Vts);
         rg.MoverA(g_MsgED135.ED135_Ezt, "TMTSD");
         rg.MoverA(g_MsgED135.ED135_Exi, "TSD");
         rg.MoverA(g_MsgED135.ED135_Exn, rg.Zeros(12));
         rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
         g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
         Limpieza(data, context);
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp724", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("C"))
       {
         //Boton Cursar
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp724_ImgGtApe");
         g_Img_GtTsd = (GT_IMG.Buf_Img_GtTsd)data.getUser().getTemp("Agtp724_ImgGtTsd");
         v_Imgs = (vecGT724.Buf_vecGT724)data.getUser().getTemp("Agtp724_vImgs");
         vImgs_A_Host(data, context);
         //if (PagoTasador(g_Tab_Img, data, context) == false )
         //   {
         //     data.getUser().removeTemp("Agtp724_g_PrmPC080");
         //     data.getUser().setTemp("Agtp724_g_PrmPC080", g_PrmPC080);
         //     BF_MSG.MsgInt("Agtp724", data, g_Msg, "Msg_Rtn", "T"); 
         //     return;
         //   }
         g_MsgED135 = MSGED135.Inicia_MsgED135();
         rg.MoverA(g_MsgED135.ED135_Idr, "M");
         rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
         rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
         rg.MoverA(g_MsgED135.ED135_Est, "");
         rg.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
         rg.MoverA(g_MsgED135.ED135_Ezt, "TERMI");
         rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
         g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
         //data.getUser().removeTemp("Agtp724_g_PrmPC080");
         //data.getUser().setTemp("Agtp724_g_PrmPC080", g_PrmPC080);
         //rg.MoverA(g_Msg.Msg_Dat, MSGTXCUR.LSet_De_MsgTxCur(g_MsgTxCur));
         //g_MsgTx900 = MSGTXCUR.LSet_A_MsgTx900(g_Msg.Msg_Dat.toString());
         //data.getUser().removeTemp("Agtp291_MsgTx900");
         //data.getUser().setTemp("Agtp291_MsgTx900", g_MsgTx900);
         //rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         //BF_MSG.Link_Program("Agtp724", "Agtp291", data, g_Msg);
         Limpieza(data, context);
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp724", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("A"))
       {
         //Boton Agregar
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp724_ImgGtApe");
         g_Img_GtTsd = (GT_IMG.Buf_Img_GtTsd)data.getUser().getTemp("Agtp724_ImgGtTsd");
         Actualiza_Datos("1", data, context);
         rg.MoverA(g_Img_GtApe.GtApe_Psw, "A");
         v_Imgs = (vecGT724.Buf_vecGT724)data.getUser().getTemp("Agtp724_vImgs");
         String Idx = String.valueOf(v_Imgs.GT724_Hyp.size());
         rg.MoverA(g_Img_GtApe.GtApe_Idx, rg.Suma(Idx,0,"1",0,3,0));
         g_Img_GtHyp = gtimg.LSet_A_ImgGtHyp(rg.Blancos(827));
         rg.MoverA(g_Img_GtHyp.GtHyp_Seq, rg.Suma(Idx,0,"1",0,3,0));
         data.getUser().removeTemp("Agtp724_ImgGtApe");
         data.getUser().setTemp("Agtp724_ImgGtApe", g_Img_GtApe);
         data.getUser().removeTemp("Agtp724_ImgGtTsd");
         data.getUser().setTemp("Agtp724_ImgGtTsd", g_Img_GtTsd);
         data.getUser().removeTemp("Agtp724_ImgGtHyp");
         data.getUser().setTemp("Agtp724_ImgGtHyp", g_Img_GtHyp);
         setTemplate(data, "Garantias,Agt,AGTP204.vm");
         return;
       }
    if (Opc.trim().equals("M"))
       {
         //Boton Modificar
         String Seq = data.getParameters().getString("Seq", "");
         i = Integer.parseInt(Seq);
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp724_ImgGtApe");
         g_Img_GtTsd = (GT_IMG.Buf_Img_GtTsd)data.getUser().getTemp("Agtp724_ImgGtTsd");
         Actualiza_Datos("1", data, context);
         rg.MoverA(g_Img_GtApe.GtApe_Psw, "M");
         rg.MoverA(g_Img_GtApe.GtApe_Idx, rg.Suma(Seq,0,"1",0,3,0));
         v_Imgs = (vecGT724.Buf_vecGT724)data.getUser().getTemp("Agtp724_vImgs");
         rg.MoverA(v_Imgs.GT724_Idx, rg.FmtValor(Seq,0,0,3,"+"));
         vecGT724.Bff_Hyp l_Hyp = new vecGT724.Bff_Hyp();
         l_Hyp = (vecGT724.Bff_Hyp)v_Imgs.GT724_Hyp.elementAt(i);
         g_Img_GtHyp = gtimg.LSet_A_ImgGtHyp(l_Hyp.Hyp_Hyp.toString());
         data.getUser().removeTemp("Agtp724_ImgGtApe");
         data.getUser().setTemp("Agtp724_ImgGtApe", g_Img_GtApe);
         data.getUser().removeTemp("Agtp724_ImgGtTsd");
         data.getUser().setTemp("Agtp724_ImgGtTsd", g_Img_GtTsd);
         data.getUser().removeTemp("Agtp724_ImgGtHyp");
         data.getUser().setTemp("Agtp724_ImgGtHyp", g_Img_GtHyp);
         data.getUser().removeTemp("Agtp724_vImgs");
         data.getUser().setTemp("Agtp724_vImgs", v_Imgs);
         setTemplate(data, "Garantias,Agt,AGTP204.vm");
         return;
       }
    if (Opc.trim().equals("E"))
       {
         //Boton Eliminar
         String Seq = data.getParameters().getString("Seq", "");
         i = Integer.parseInt(Seq);
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp724_ImgGtApe");
         g_Img_GtTsd = (GT_IMG.Buf_Img_GtTsd)data.getUser().getTemp("Agtp724_ImgGtTsd");
         Actualiza_Datos("1", data, context);
         rg.MoverA(g_Img_GtApe.GtApe_Psw, "E");
         rg.MoverA(g_Img_GtApe.GtApe_Idx, rg.Suma(Seq,0,"1",0,3,0));
         v_Imgs = (vecGT724.Buf_vecGT724)data.getUser().getTemp("Agtp724_vImgs");
         rg.MoverA(v_Imgs.GT724_Idx, rg.FmtValor(Seq,0,0,3,"+"));
         vecGT724.Bff_Hyp l_Hyp = new vecGT724.Bff_Hyp();
         l_Hyp = (vecGT724.Bff_Hyp)v_Imgs.GT724_Hyp.elementAt(i);
         g_Img_GtHyp = gtimg.LSet_A_ImgGtHyp(l_Hyp.Hyp_Hyp.toString());
         data.getUser().removeTemp("Agtp724_ImgGtApe");
         data.getUser().setTemp("Agtp724_ImgGtApe", g_Img_GtApe);
         data.getUser().removeTemp("Agtp724_ImgGtTsd");
         data.getUser().setTemp("Agtp724_ImgGtTsd", g_Img_GtTsd);
         data.getUser().removeTemp("Agtp724_ImgGtHyp");
         data.getUser().setTemp("Agtp724_ImgGtHyp", g_Img_GtHyp);
         data.getUser().removeTemp("Agtp724_vImgs");
         data.getUser().setTemp("Agtp724_vImgs", v_Imgs);
         setTemplate(data, "Garantias,Agt,AGTP204.vm");
         return;
       }
    if (Opc.trim().equals("Y"))
       {
         //Boton Si
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp724_ImgGtApe");
         g_Img_GtTsd = (GT_IMG.Buf_Img_GtTsd)data.getUser().getTemp("Agtp724_ImgGtTsd");
         g_Img_GtHyp = (GT_IMG.Buf_Img_GtHyp)data.getUser().getTemp("Agtp724_ImgGtHyp");
         Actualiza_Datos(Opc, data, context);
         v_Imgs = (vecGT724.Buf_vecGT724)data.getUser().getTemp("Agtp724_vImgs");
         if (g_Img_GtApe.GtApe_Psw.toString().trim().equals("A"))
            {
              vecGT724.Bff_Hyp l_Hyp = new vecGT724.Bff_Hyp();
              rg.MoverA(l_Hyp.Hyp_Idx, rg.FmtValor("0",0,0,3,"+"));
              rg.MoverA(l_Hyp.Hyp_Hyp, gtimg.LSet_De_ImgGtHyp(g_Img_GtHyp));
              l_Hyp.Hyp_Hpd.clear();
              v_Imgs.GT724_Hyp.add(l_Hyp);
            }
         else
            {
              i = Integer.parseInt(v_Imgs.GT724_Idx.toString());
              if (g_Img_GtApe.GtApe_Psw.toString().trim().equals("M"))
                 { 
                   vecGT724.Bff_Hyp l_Hyp = new vecGT724.Bff_Hyp();
                   l_Hyp = (vecGT724.Bff_Hyp)v_Imgs.GT724_Hyp.elementAt(i);
                   rg.MoverA(l_Hyp.Hyp_Hyp, gtimg.LSet_De_ImgGtHyp(g_Img_GtHyp));
                   v_Imgs.GT724_Hyp.set(i, l_Hyp);
                 }
              else           //g_Img_GtApe.GtApe_Psw.toString().trim().equals("E")
                 {
                   v_Imgs.GT724_Hyp.remove(i);
                 }
            }
//         Recalcula_GtApe(data, context);
         rg.MoverA(g_Img_GtApe.GtApe_Psw, "");
         rg.MoverA(g_Img_GtApe.GtApe_Idx, "");
         data.getUser().removeTemp("Agtp724_ImgGtHyp");
         data.getUser().removeTemp("Agtp724_ImgGtApe");
         data.getUser().setTemp("Agtp724_ImgGtApe", g_Img_GtApe);
         data.getUser().removeTemp("Agtp724_ImgGtTsd");
         data.getUser().setTemp("Agtp724_ImgGtTsd", g_Img_GtTsd);
         data.getUser().removeTemp("Agtp724_vImgs");
         data.getUser().setTemp("Agtp724_vImgs", v_Imgs);
         aux_vImgs(data, context);
         setTemplate(data, "Garantias,Agt,AGTP204.vm");
         return;
       }
    if (Opc.trim().equals("N"))
       {
         //Boton No
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp724_ImgGtApe");
         rg.MoverA(g_Img_GtApe.GtApe_Psw, "");
         rg.MoverA(g_Img_GtApe.GtApe_Idx, "");
         data.getUser().removeTemp("Agtp724_ImgGtHyp");
         data.getUser().removeTemp("Agtp724_ImgGtApe");
         data.getUser().setTemp("Agtp724_ImgGtApe", g_Img_GtApe);
         setTemplate(data, "Garantias,Agt,AGTP204.vm");
         return;
       }
    if (Opc.trim().equals("K"))
       {
         //Boton Cambio Fecha
         String Fec = data.getParameters().getString("Fec", "");
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp724_ImgGtApe");
         g_Img_GtTsd = (GT_IMG.Buf_Img_GtTsd)data.getUser().getTemp("Agtp724_ImgGtTsd");
         v_Imgs = (vecGT724.Buf_vecGT724)data.getUser().getTemp("Agtp724_vImgs");
         //Solo deja valores en cero en vectores------------------------------------------
         Actualiza_Valores(data, context);
         //-------------------------------------------------------------------------------
         rg.MoverA(g_Img_GtApe.GtApe_Vtt, rg.Zeros(15));  
         rg.MoverA(g_Img_GtApe.GtApe_Vts, rg.Zeros(15));  
         rg.MoverA(g_Img_GtApe.GtApe_Vcn, rg.Zeros(15));  
       	 rg.MoverA(g_Img_GtApe.GtApe_Hvt, rg.Zeros(15));
       	 rg.MoverA(g_Img_GtApe.GtApe_Hvl, rg.Zeros(15));
       	 rg.MoverA(g_Img_GtApe.GtApe_Hvb, rg.Zeros(15));
       	 rg.MoverA(g_Img_GtApe.GtApe_Hvc, rg.Zeros(15));
       	 rg.MoverA(g_Img_GtApe.GtApe_Pvt, rg.Zeros(15));
       	 rg.MoverA(g_Img_GtApe.GtApe_Pvl, rg.Zeros(15));
       	 rg.MoverA(g_Img_GtApe.GtApe_Pvb, rg.Zeros(15));
       	 rg.MoverA(g_Img_GtApe.GtApe_Pvc, rg.Zeros(15));
         Actualiza_Fecha(Fec, data, context);
         data.getUser().removeTemp("Agtp724_ImgGtApe");
         data.getUser().setTemp("Agtp724_ImgGtApe", g_Img_GtApe);
         data.getUser().removeTemp("Agtp724_ImgGtTsd");
         data.getUser().setTemp("Agtp724_ImgGtTsd", g_Img_GtTsd);
         data.getUser().removeTemp("Agtp724_vImgs");
         data.getUser().setTemp("Agtp724_vImgs", v_Imgs);
         aux_vImgs(data, context);
         setTemplate(data, "Garantias,Agt,AGTP204.vm");
         return;
       }
  }
  //---------------------------------------------------------------------------------------
  public void Actualiza_Datos(String Opc, RunData data, Context context)
              throws Exception 
  {
    if (Opc.trim().equals("1"))
       {
         rg.MoverA(g_Img_GtTsd.GtTsd_Fts, data.getParameters().getString("GtTsd_Fts", ""));
         rg.MoverA(g_Img_GtTsd.GtTsd_Nbf, data.getParameters().getString("GtTsd_Nbf", ""));
         rg.MoverA(g_Img_GtTsd.GtTsd_Pcb, data.getParameters().getString("GtTsd_Pcb", ""));
         rg.MoverA(g_Img_GtTsd.GtTsd_Hon, data.getParameters().getString("GtTsd_Hon", ""));
         rg.MoverA(g_Img_GtTsd.GtTsd_Vst, data.getParameters().getString("GtTsd_Vst", ""));
       }
    else
       {
         rg.MoverA(g_Img_GtHyp.GtHyp_Itb, data.getParameters().getString("GtHyp_Itb", ""));
         rg.MoverA(g_Img_GtHyp.GtHyp_Dsb, data.getParameters().getString("GtHyp_Dsb", ""));
         rg.MoverA(g_Img_GtHyp.GtHyp_Cll, g_Img_GtTsd.GtTsd_Cll);
         rg.MoverA(g_Img_GtHyp.GtHyp_Cnm, g_Img_GtTsd.GtTsd_Cnm);
         rg.MoverA(g_Img_GtHyp.GtHyp_Lug, g_Img_GtTsd.GtTsd_Lug);
         rg.MoverA(g_Img_GtHyp.GtHyp_Lnm, g_Img_GtTsd.GtTsd_Lnm);
         rg.MoverA(g_Img_GtHyp.GtHyp_Sec, g_Img_GtTsd.GtTsd_Sec);
         rg.MoverA(g_Img_GtHyp.GtHyp_Snb, g_Img_GtTsd.GtTsd_Snb);
         rg.MoverA(g_Img_GtHyp.GtHyp_Cmn, g_Img_GtTsd.GtTsd_Cmn);
         rg.MoverA(g_Img_GtHyp.GtHyp_Ncm, g_Img_GtTsd.GtTsd_Ncm);
         rg.MoverA(g_Img_GtHyp.GtHyp_Npv, g_Img_GtTsd.GtTsd_Npv);
       }
  }
//---------------------------------------------------------------------------------------
  public void doAgtp724_Continue(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP724[doAgtp724_Continue.start]", "[" + data.getUser().getUserName() + "]");
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Appp030"))
       {
         g_PrmPP030 = PRMPP030.LSet_A_PrmPP030(g_Msg.Msg_Dat.toString());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp724_g_PrmPC080");
         data.getUser().removeTemp("Agtp724_g_PrmPC080");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         if (g_PrmPP030.PP030_Rsp.toString().trim().equals("TERMINAR"))
            {
              Limpieza(data, context);
              BF_MSG.Return_Data("Agtp724", data, g_Msg);
              return;
            }
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP204.vm");
         return;
       }
  //if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Agtp725"))
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Appp096"))
       {
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp724_g_PrmPC080");
         data.getUser().removeTemp("Agtp724_g_PrmPC080");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "RA");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP204.vm");
         return;
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Agtp214"))
       {
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp724_g_PrmPC080");
         data.getUser().removeTemp("Agtp724_g_PrmPC080");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP204.vm");
         return;
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Agtp291"))
       {
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp724_g_PrmPC080");
         data.getUser().removeTemp("Agtp724_g_PrmPC080");
         Limpieza(data, context);
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp724", data, g_Msg);
         return;
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Aedp210"))
       {
         g_PrmED210 = PRMED210.LSet_A_PrmED210(g_Msg.Msg_Dat.toString());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp724_g_PrmPC080");
         data.getUser().removeTemp("Agtp724_g_PrmPC080");
         if (g_PrmED210.ED210_Rtn.toString().equals("NK")
          || g_PrmED210.ED210_Rtn.toString().equals("YA"))
            {
              rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
              BF_MSG.Param_Program(data, g_Msg);
              setTemplate(data, "Garantias,Agt,AGTP204.vm");
              return;
            }
         else
            {
              if (g_PrmPC080.PC080_Amv.toString().equals("D"))
                 {
                   g_Img_GtTsd = (GT_IMG.Buf_Img_GtTsd)data.getUser().getTemp("Agtp724_ImgGtTsd");
                   g_MsgED135 = MSGED135.Inicia_MsgED135();
                   rg.MoverA(g_MsgED135.ED135_Idr, "X");
                   rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
                   rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
                   rg.MoverA(g_MsgED135.ED135_Est, "");
                   rg.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
                   rg.MoverA(g_MsgED135.ED135_Ldv, g_PrmED210.ED210_Ldt);
                   rg.MoverA(g_MsgED135.ED135_Ezt, "RETSD");
                   rg.MoverA(g_MsgED135.ED135_Exi, "TSD");
                   rg.MoverA(g_MsgED135.ED135_Exn, rg.FmtValor(g_Img_GtTsd.GtTsd_Tsd,0,0,12,"+"));
                   rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
                   g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
                   Limpieza(data, context);
                   rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
                   rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
                   BF_MSG.Return_Data("Agtp724", data, g_Msg);
                   return;
                 } 
              if (g_PrmPC080.PC080_Amv.toString().equals("P"))
                 {
                   g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp724_ImgGtApe");
                   g_Img_GtTsd = (GT_IMG.Buf_Img_GtTsd)data.getUser().getTemp("Agtp724_ImgGtTsd");
                   v_Imgs = (vecGT724.Buf_vecGT724)data.getUser().getTemp("Agtp724_vImgs");
                   vImgs_A_Host(data, context);
                   g_MsgED135 = MSGED135.Inicia_MsgED135();
                   rg.MoverA(g_MsgED135.ED135_Idr, "X");
                   rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
                   rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
                   rg.MoverA(g_MsgED135.ED135_Est, "");
                   rg.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
                   rg.MoverA(g_MsgED135.ED135_Ldv, g_PrmED210.ED210_Ldt);
                   rg.MoverA(g_MsgED135.ED135_Ezt, "PDTSD");
                   rg.MoverA(g_MsgED135.ED135_Exi, "TSD");
                   rg.MoverA(g_MsgED135.ED135_Exn, rg.FmtValor(g_Img_GtTsd.GtTsd_Tsd,0,0,12,"+"));
                   rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
                   g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
                   Limpieza(data, context);
                   rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
                   rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
                   BF_MSG.Return_Data("Agtp724", data, g_Msg);
                   return;
                 } 
            }
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Agtp727")
     || rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Agtp764")
     || rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Agtp765")
     || rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Agtp766"))
       {
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp724_g_PrmPC080");
         data.getUser().removeTemp("Agtp724_g_PrmPC080");
         PRMPC080.PrmPC080 l_PrmPC080 = new PRMPC080.PrmPC080();
         l_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp724_ImgGtApe");
         g_Img_GtHyp = (GT_IMG.Buf_Img_GtHyp)data.getUser().getTemp("Agtp724_ImgGtHyp");
         v_Img_GtHpd = (Vector)data.getUser().getTemp("Agtp724_vImgGtHpd");
         v_Imgs = (vecGT724.Buf_vecGT724)data.getUser().getTemp("Agtp724_vImgs");
         if (l_PrmPC080.PC080_Rtn.toString().equals("OK"))
            {
              i = Integer.parseInt(v_Imgs.GT724_Idx.toString());
              vecGT724.Bff_Hyp l_Hyp = new vecGT724.Bff_Hyp();
              l_Hyp = (vecGT724.Bff_Hyp)v_Imgs.GT724_Hyp.elementAt(i);
              rg.MoverA(l_Hyp.Hyp_Hyp, gtimg.LSet_De_ImgGtHyp(g_Img_GtHyp));
              l_Hyp.Hyp_Hpd.clear();
              for (j=0; j<v_Img_GtHpd.size(); j++)
                  {
                    vecGT724.Bff_Hpd l_Hpd = new vecGT724.Bff_Hpd();
                    g_Img_GtHpd = (GT_IMG.Buf_Img_GtHpd)v_Img_GtHpd.elementAt(j);
                    rg.MoverA(l_Hpd.Hpd_Hpd, gtimg.LSet_De_ImgGtHpd(g_Img_GtHpd));
                    l_Hyp.Hyp_Hpd.add(l_Hpd); 
                  }
              v_Imgs.GT724_Hyp.set(i, l_Hyp); 
              data.getUser().removeTemp("Agtp724_vImgs");
              data.getUser().setTemp("Agtp724_vImgs", v_Imgs);
              Recalcula_GtApe(data, context);
            }
         data.getUser().removeTemp("Agtp724_ImgGtHyp");
         data.getUser().removeTemp("Agtp724_vImgGtHpd");
         data.getUser().removeTemp("Agtp724_ImgGtApe");
         data.getUser().setTemp("Agtp724_ImgGtApe", g_Img_GtApe);
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         aux_vImgs(data, context);
         setTemplate(data, "Garantias,Agt,AGTP204.vm");
         return;
       }
  }
  //---------------------------------------------------------------------------------------
  public boolean PagoTasador(Vector g_Tab_Img, RunData data, Context context)
              throws Exception 
  {
    g_Img      = img.Inicia_Img();
    rg.MoverA(g_Img.Img_Ntr, Asigna_Folio("TRN-NTR", data, context));
    rg.MoverA(g_Img.Img_Dax, "BASE");
    rg.MoverA(g_Img.Img_Seq, "000");
    //------------------------------------------------------------------------------
    g_Img_Base = img.LSet_A_ImgBase(g_Img_GtApe.GtApe_Bse.toString());
    rg.MoverA(g_Img_Base.Img_Base_Ntr, g_Img.Img_Ntr);
    rg.MoverA(g_Img_Base.Img_Base_Ttr, "PGTSD");
    rg.MoverA(g_Img_Base.Img_Base_Ntt, "PAGO A TASADORES");
    if (g_PrmPC080.PC080_Pai.toString().trim().equals("CL"))
       { rg.MoverA(g_Img_Base.Img_Base_Dcm, "0"); }
    else
       { rg.MoverA(g_Img_Base.Img_Base_Dcm, "2"); }
    //------------------------------------------------------------------------------
    rg.MoverA(g_Trn.Trn_Ntr, g_Img.Img_Ntr);
    rg.MoverA(g_Trn.Trn_Trm, "PCVB");
    rg.MoverA(g_Trn.Trn_Fem, g_Msg.Msg_Fch);
    rg.MoverA(g_Trn.Trn_Hrm, rg.Zeros(6));
    rg.MoverA(g_Trn.Trn_Ttr, g_Img_Base.Img_Base_Ttr);
    rg.MoverA(g_Trn.Trn_Cli, g_Img_Base.Img_Base_Cli);
    rg.MoverA(g_Trn.Trn_Cnr, g_Img_Base.Img_Base_Sis.toString() + g_Img_Base.Img_Base_Ncn.toString());
    rg.MoverA(g_Trn.Trn_Suc, g_Img_Base.Img_Base_Suc);
    rg.MoverA(g_Trn.Trn_Eje, g_Img_Base.Img_Base_Eje);
    rg.MoverA(g_Trn.Trn_Est, "ENPRO");
    rg.MoverA(g_Trn.Trn_Usr, g_Msg.Msg_Cus);
    rg.MoverA(g_Trn.Trn_Cja, rg.Zeros(4));
    rg.MoverA(g_Trn.Trn_Stf, rg.Zeros(9));
    rg.MoverA(g_Trn.Trn_Dcn, g_Img_Base.Img_Base_Dcn);
    rg.MoverA(g_Trn.Trn_Tmn, g_Img_Base.Img_Base_Tmn);
    rg.MoverA(g_Trn.Trn_Trj, g_Img_Base.Img_Base_Trj);
    rg.MoverA(g_Trn.Trn_Dcm, g_Img_Base.Img_Base_Dcm);
    rg.MoverA(g_Trn.Trn_Mnd, g_Img_Base.Img_Base_Mnd);
    rg.MoverA(g_Trn.Trn_Mnt, rg.Zeros(15));
    //------------------------------------------------------------------------------
    g_Img_GtPts = gtimg.LSet_A_ImgGtPts(g_Img.Img_Dat.toString());
    rg.MoverA(g_Img_GtPts.GtPts_Bse, img.LSet_De_ImgBase_Bse(g_Img_Base));
    rg.MoverA(g_Img_GtPts.GtPts_Fec, g_Msg.Msg_Fch);
    rg.MoverA(g_Img_GtPts.GtPts_Cbp, g_Img_GtApe.GtApe_Cbp);
    rg.MoverA(g_Img_GtPts.GtPts_Dbp, g_Img_GtApe.GtApe_Dbp);
    rg.MoverA(g_Img_GtPts.GtPts_Gts, g_Img_GtTsd.GtTsd_Vst);
    rg.MoverA(g_Img_GtPts.GtPts_Hon, g_Img_GtTsd.GtTsd_Hon);
    rg.MoverA(g_Img_GtPts.GtPts_Pcb, g_Img_GtTsd.GtTsd_Pcb);
    rg.MoverA(g_Img_GtPts.GtPts_Cct, g_Img_GtTsd.GtTsd_Cct);
    rg.MoverA(g_Img_GtPts.GtPts_Bof, g_Img_GtTsd.GtTsd_Bof);
    rg.MoverA(g_Img_GtPts.GtPts_Nbf, g_Img_GtTsd.GtTsd_Nbf);
    rg.MoverA(g_Img_GtPts.GtPts_Nts, g_Img_GtTsd.GtTsd_Nts);
    rg.MoverA(g_Img_GtPts.GtPts_Rts, g_Img_GtTsd.GtTsd_Rts);
    rg.MoverA(g_Img_GtPts.GtPts_Dpc, g_Img_GtHyp.GtHyp_Dpc);
    int p = 0;
    for (i=0; i<g_Tab_Img.size(); i++)
        {
          BF_IMG.Buf_Img l_Img = new BF_IMG.Buf_Img();
          l_Img = (BF_IMG.Buf_Img)g_Tab_Img.elementAt(i);
          if (l_Img.Img_Dax.toString().trim().equals("MFF"))
             { 
               g_Img_GtMff = gtimg.LSet_A_ImgGtMff(l_Img.Img_Dat.toString());
               rg.MoverA(g_Img_GtPts.GtPts_Cbn, g_Img_GtMff.GtMff_Cct);
               rg.MoverA(g_Img_GtPts.GtPts_Dbn, g_Img_GtMff.GtMff_Dct);
               rg.MoverA(g_Img_GtPts.GtPts_Prc, g_Img_GtMff.GtMff_Prc);
               String lcData = g_Img_GtPts.GtPts_Cbn.toString() + g_Img_GtPts.GtPts_Dbn.toString() 
                                                                + g_Img_GtPts.GtPts_Prc.toString();
               g_Img_GtPts.vGtPts_Ctg.set(p, lcData);
               p = p + 1;
             }
        }
    rg.MoverA(g_Img_GtPts.GtPts_Fll, "");
    rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtPts(g_Img_GtPts));
    //------------------------------------------------------------------------------
    rg.MoverA(g_MsgTxCur.TxCur_Bco, "DEMO");                                //"BICE"
    rg.MoverA(g_MsgTxCur.TxCur_Cic, "NO");
    rg.MoverA(g_MsgTxCur.TxCur_Lin, " ");
    rg.MoverA(g_MsgTxCur.TxCur_Rsp, " ");
    rg.MoverA(g_MsgTxCur.TxCur_Idc, "00");
    rg.MoverA(g_MsgTxCur.TxCur_Stp, " ");
    rg.MoverA(g_MsgTxCur.TxCur_Imr, " ");
    rg.MoverA(g_MsgTxCur.TxCur_Msg, " ");
    rg.MoverA(g_MsgTxCur.TxCur_Trn, BF_TRN.LSet_De_Trn(g_Trn));
    rg.MoverA(g_MsgTxCur.TxCur_Img, img.LSet_De_Img(g_Img));
    rg.MoverA(g_Msg.Msg_Dat, MSGTXCUR.LSet_De_MsgTxCur(g_MsgTxCur));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GTCUR");
    if (g_Msg.Msg_Rtn.toString().compareTo("0")!=0)
       { return false; }
    g_MsgTxCur = MSGTXCUR.LSet_A_MsgTxCur(g_Msg.Msg_Dat.toString());
    return true;
  }
  //---------------------------------------------------------------------------------------
  public String Asigna_Folio(String pcFolio, RunData data, Context context)
                throws Exception 
  {
    g_MsgED090 = MSGED090.Inicia_MsgED090();
    rg.MoverA(g_MsgED090.ED090_Idr, "F");
    rg.MoverA(g_MsgED090.ED090_Cod, pcFolio);
    rg.MoverA(g_Msg.Msg_Dat, MSGED090.LSet_De_MsgED090(g_MsgED090));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED090");
    g_MsgED090 = MSGED090.LSet_A_MsgED090(g_Msg.Msg_Dat.toString());
    String lcFolio = g_MsgED090.ED090_Fol.toString();
    return lcFolio;
  }
  //---------------------------------------------------------------------------------------
  public void Limpieza(RunData data, Context context)
              throws Exception 
  {
    data.getUser().removeTemp("Agtp724_PrmPC080");
    data.getUser().removeTemp("Agtp724_MsgED135");
    data.getUser().removeTemp("Agtp724_ImgBase");
    data.getUser().removeTemp("Agtp724_ImgGtApe");
    data.getUser().removeTemp("Agtp724_ImgGtTsd");
    data.getUser().removeTemp("Agtp724_vImgGtHyp");
    data.getUser().removeTemp("Agtp724_vImgGtHpd");
    data.getUser().removeTemp("Agtp724_vImgs");
    data.getUser().removeTemp("Agtp724_vTabImg");
  }
  //---------------------------------------------------------------------------------------
  public void Carga_de_Host(RunData data, Context context)
              throws Exception 
  {
    Vector v_Tab_Img = img.Carga_Img_Host(g_Msg, g_PrmPC080.PC080_Ntr.toString(), data, context);
    De_Tab_Img(v_Tab_Img, data);
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (!g_Img_GtTsd.GtTsd_Vuf.toString().trim().equals(""))
       {
         rg.MoverA(g_Img_GtApe.GtApe_Vuf, rg.FmtValor(g_Img_GtTsd.GtTsd_Vuf,2,2,9,"+"));
         rg.MoverA(g_Img_GtApe.GtApe_Vus, rg.FmtValor(g_Img_GtTsd.GtTsd_Vtc,2,2,9,"+"));
      }
    if (g_Img_GtApe.GtApe_Tfr.toString().trim().equals(""))
       { rg.MoverA(g_Img_GtApe.GtApe_Tfr, rg.Zeros(3)); }
    if (g_Img_GtApe.GtApe_Vtt.toString().trim().equals(""))
       { rg.MoverA(g_Img_GtApe.GtApe_Vtt, rg.Zeros(15)); }
    if (g_Img_GtApe.GtApe_Vts.toString().trim().equals(""))
       { rg.MoverA(g_Img_GtApe.GtApe_Vts, rg.Zeros(15)); }
    if (g_Img_GtApe.GtApe_Vcn.toString().trim().equals(""))
       { rg.MoverA(g_Img_GtApe.GtApe_Vcn, rg.Zeros(15)); }
    if (g_Img_GtApe.GtApe_Hvt.toString().trim().equals(""))
       { 
       	 rg.MoverA(g_Img_GtApe.GtApe_Hvt, rg.Zeros(15));
       	 rg.MoverA(g_Img_GtApe.GtApe_Hvl, rg.Zeros(15));
       	 rg.MoverA(g_Img_GtApe.GtApe_Hvb, rg.Zeros(15));
       	 rg.MoverA(g_Img_GtApe.GtApe_Hvc, rg.Zeros(15));
       	 rg.MoverA(g_Img_GtApe.GtApe_Pvt, rg.Zeros(15));
       	 rg.MoverA(g_Img_GtApe.GtApe_Pvl, rg.Zeros(15));
       	 rg.MoverA(g_Img_GtApe.GtApe_Pvb, rg.Zeros(15));
       	 rg.MoverA(g_Img_GtApe.GtApe_Pvc, rg.Zeros(15));
       }
    if (g_Img_GtTsd.GtTsd_Fts.toString().trim().equals(""))
       { Actualiza_Fecha(g_Msg.Msg_Fch.toString(), data, context); }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    vImgs_De_Host(data);
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    data.getUser().removeTemp("Agtp724_ImgBase");
    data.getUser().setTemp("Agtp724_ImgBase", g_Img_Base);
    data.getUser().removeTemp("Agtp724_ImgGtApe");
    data.getUser().setTemp("Agtp724_ImgGtApe", g_Img_GtApe);
    data.getUser().removeTemp("Agtp724_ImgGtTsd");
    data.getUser().setTemp("Agtp724_ImgGtTsd", g_Img_GtTsd);
    data.getUser().removeTemp("Agtp724_vImgs");
    data.getUser().setTemp("Agtp724_vImgs", v_Imgs);
    data.getUser().removeTemp("Agtp724_vTabImg");
    data.getUser().setTemp("Agtp724_vTabImg", v_Tab_Img);
  }
  //---------------------------------------------------------------------------------------
  public void De_Tab_Img(Vector v_Tab_Img, RunData data)
  {
    g_Img       = img.Inicia_Img();
    lcData      = img.LSet_De_Img(g_Img);
    g_Img_Base  = img.LSet_A_ImgBase(lcData);
    g_Img_GtApe = gtimg.LSet_A_ImgGtApe(g_Img.Img_Dat.toString());
    g_Img_GtTsd = gtimg.LSet_A_ImgGtTsd(g_Img.Img_Dat.toString());
    v_Img_GtHyp.clear();
    v_Img_GtHpd.clear();
    for (i=0; i<v_Tab_Img.size(); i++)
        {
          lcData = img.LSet_De_Img((BF_IMG.Buf_Img)v_Tab_Img.elementAt(i));
          g_Img  = img.LSet_A_Img(lcData);
          if (g_Img.Img_Dax.toString().trim().equals("BASE"))
             {
               g_Img_GtApe = gtimg.LSet_A_ImgGtApe(g_Img.Img_Dat.toString());
               g_Img_Base  = img.LSet_A_ImgBase(g_Img_GtApe.GtApe_Bse.toString());
             }
          if (g_Img.Img_Dax.toString().trim().equals("XTSD"))
             { g_Img_GtTsd = gtimg.LSet_A_ImgGtTsd(g_Img.Img_Dat.toString()); }
          if (g_Img.Img_Dax.toString().trim().equals("XXHIP")
           || g_Img.Img_Dax.toString().trim().equals("YYPRD"))
             {
               g_Img_GtHyp = gtimg.LSet_A_ImgGtHyp(g_Img.Img_Dat.toString());
               v_Img_GtHyp.add (g_Img_GtHyp); 
             }
          if (g_Img.Img_Dax.toString().trim().equals("ZZHPD"))
             {
               g_Img_GtHpd = gtimg.LSet_A_ImgGtHpd(g_Img.Img_Dat.toString());
               if (g_Img_GtHpd.GtHpd_Itp.toString().trim().equals("TE")
                || g_Img_GtHpd.GtHpd_Itp.toString().trim().equals("SA"))
                  {
                    rg.MoverA(g_Img_GtHpd.GtHpd_Vsu, rg.Zeros(15));
                    rg.MoverA(g_Img_GtHpd.GtHpd_Vsg, rg.Zeros(15));
                  }
               v_Img_GtHpd.add (g_Img_GtHpd); 
             }
        }
  }
  //---------------------------------------------------------------------------------------
  public void Actualiza_Fecha(String Fecha, RunData data, Context context)
              throws Exception 
  {
    rg.MoverA(g_Img_GtTsd.GtTsd_Fts, Fecha);
    g_MsgGT096 = MSGGT096.Inicia_MsgGT096();
    rg.MoverA(g_MsgGT096.GT096_Idr, "V");
    rg.MoverA(g_MsgGT096.GT096_Fec, g_Img_GtTsd.GtTsd_Fts);
    rg.MoverA(g_Msg.Msg_Dat, MSGGT096.LSet_De_MsgGT096(g_MsgGT096));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GT096");
    g_MsgGT096 = MSGGT096.LSet_A_MsgGT096(g_Msg.Msg_Dat.toString());
    rg.MoverA(g_Img_GtApe.GtApe_Fig, g_Msg.Msg_Fch);
    rg.MoverA(g_Img_GtApe.GtApe_Fts, g_Img_GtTsd.GtTsd_Fts);
    rg.MoverA(g_Img_GtApe.GtApe_Vuo, rg.FmtValor("1",0,2,9,"+"));
    if (g_Img_Base.Img_Base_Trj.toString().trim().equals("UF"))
       { rg.MoverA(g_Img_GtApe.GtApe_Vuo, rg.FmtValor(g_MsgGT096.GT096_Vuf,2,2,9,"+")); }
    if (g_Img_Base.Img_Base_Trj.toString().trim().equals("IVP"))
       { rg.MoverA(g_Img_GtApe.GtApe_Vuo, rg.FmtValor(g_MsgGT096.GT096_Viv,2,2,9,"+")); }
    if (g_Img_Base.Img_Base_Trj.toString().trim().equals("TC"))
       { rg.MoverA(g_Img_GtApe.GtApe_Vuo, rg.FmtValor(g_MsgGT096.GT096_Vtc,2,2,9,"+")); }
    rg.MoverA(g_Img_GtApe.GtApe_Fvt, rg.DateAdd(g_Img_GtTsd.GtTsd_Fts,"A",2));
    rg.MoverA(g_Img_GtApe.GtApe_Vuf, rg.FmtValor(g_MsgGT096.GT096_Vuf,2,2,9,"+"));
    rg.MoverA(g_Img_GtApe.GtApe_Vus, rg.FmtValor(g_MsgGT096.GT096_Vtc,2,2,9,"+"));
    rg.MoverA(g_Img_GtTsd.GtTsd_Vuf, rg.FmtValor(g_MsgGT096.GT096_Vuf,2,2,9,"+"));
    rg.MoverA(g_Img_GtTsd.GtTsd_Vtc, rg.FmtValor(g_MsgGT096.GT096_Vtc,2,2,9,"+"));
  }
  //---------------------------------------------------------------------------------------
  public void vImgs_De_Host(RunData data)
  {
    rg.MoverA(v_Imgs.GT724_Idx, "000");
    rg.MoverA(v_Imgs.GT724_Ape, gtimg.LSet_De_ImgGtApe(g_Img_GtApe));
    rg.MoverA(v_Imgs.GT724_Tsd, gtimg.LSet_De_ImgGtTsd(g_Img_GtTsd));
    v_Imgs.GT724_Hyp.clear();
    for (i=0; i<v_Img_GtHyp.size(); i++)
        {
          g_Img_GtHyp = (GT_IMG.Buf_Img_GtHyp)v_Img_GtHyp.elementAt(i);
          vecGT724.Bff_Hyp l_Hyp = new vecGT724.Bff_Hyp();
          rg.MoverA(l_Hyp.Hyp_Idx, "000");
          rg.MoverA(l_Hyp.Hyp_Hyp, gtimg.LSet_De_ImgGtHyp(g_Img_GtHyp));
          l_Hyp.Hyp_Hpd.clear();
          for (j=0; j<v_Img_GtHpd.size(); j++)
              {
                g_Img_GtHpd = (GT_IMG.Buf_Img_GtHpd)v_Img_GtHpd.elementAt(j);
                if (g_Img_GtHyp.GtHyp_Seq.toString().equals(g_Img_GtHpd.GtHpd_Seq.toString()))
                   {
                     vecGT724.Bff_Hpd l_Hpd = new vecGT724.Bff_Hpd();
                     rg.MoverA(l_Hpd.Hpd_Hpd, gtimg.LSet_De_ImgGtHpd(g_Img_GtHpd));
                     l_Hyp.Hyp_Hpd.add(l_Hpd); 
                   }
              }
          v_Imgs.GT724_Hyp.add(l_Hyp); 
        }
  }
  //---------------------------------------------------------------------------------------
  public void vImgs_A_Host(RunData data, Context context)
              throws Exception 
  {
    v_Img_GtHyp.clear();
    v_Img_GtHpd.clear();
    for (i=0; i<v_Imgs.GT724_Hyp.size(); i++)
        {
          vecGT724.Bff_Hyp l_Hyp = new vecGT724.Bff_Hyp();
          l_Hyp = (vecGT724.Bff_Hyp)v_Imgs.GT724_Hyp.elementAt(i);
          g_Img_GtHyp = gtimg.LSet_A_ImgGtHyp(l_Hyp.Hyp_Hyp.toString());
          String Seq = String.valueOf(i);
          rg.MoverA(g_Img_GtHyp.GtHyp_Seq, rg.Suma(Seq,0,"1",0,3,0));
          for (j=0; j<l_Hyp.Hyp_Hpd.size(); j++)
              {
                vecGT724.Bff_Hpd l_Hpd = new vecGT724.Bff_Hpd();
                l_Hpd = (vecGT724.Bff_Hpd)l_Hyp.Hyp_Hpd.elementAt(j);
                g_Img_GtHpd = gtimg.LSet_A_ImgGtHpd(l_Hpd.Hpd_Hpd.toString());
                String Sqd = String.valueOf(j);
                rg.MoverA(g_Img_GtHpd.GtHpd_Seq, g_Img_GtHyp.GtHyp_Seq);
                rg.MoverA(g_Img_GtHpd.GtHpd_Sqd, rg.Suma(Sqd,0,"1",0,3,0));
                v_Img_GtHpd.add(g_Img_GtHpd); 
              }
          v_Img_GtHyp.add(g_Img_GtHyp); 
        }
    g_Tab_Img = A_Tab_Img(data);
    img.Graba_Img_Host(g_Msg, g_Tab_Img, data, context);
  }
  //---------------------------------------------------------------------------------------
  public Vector A_Tab_Img(RunData data)
  {
    Vector v_TabImg = (Vector)data.getUser().getTemp("Agtp724_vTabImg");
    Vector v_Img = new Vector();
    v_Img.clear();
    rg.MoverA(g_Img.Img_Ntr, g_PrmPC080.PC080_Ntr);
    //--------------------------------------------------------------------------
    rg.MoverA(g_Img.Img_Dax, "BASE");
    rg.MoverA(g_Img.Img_Seq, "000");
    rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtApe(g_Img_GtApe));
    Vector vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
    v_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
    //--------------------------------------------------------------------------
    rg.MoverA(g_Img.Img_Dax, "XTSD");
    rg.MoverA(g_Img.Img_Seq, "000");
    rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtTsd(g_Img_GtTsd));
    vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
    v_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
    //--------------------------------------------------------------------------
    rg.MoverA(g_Img.Img_Seq, "000");
    for (i=0; i<v_Img_GtHyp.size(); i++)
        {
          g_Img_GtHyp = (GT_IMG.Buf_Img_GtHyp)v_Img_GtHyp.elementAt(i);
          if (g_Img_GtHyp.GtHyp_Itb.toString().equals("HP"))
             { rg.MoverA(g_Img.Img_Dax, "XXHIP"); }
          else
             { rg.MoverA(g_Img.Img_Dax, "YYPRD"); }
          rg.MoverA(g_Img.Img_Seq, rg.Suma(g_Img.Img_Seq,0,"1",0,3,0));
          rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtHyp(g_Img_GtHyp));
          vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
          v_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
        }
    //--------------------------------------------------------------------------
    rg.MoverA(g_Img.Img_Seq, "000");
    for (i=0; i<v_Img_GtHpd.size(); i++)
        {
          g_Img_GtHpd = (GT_IMG.Buf_Img_GtHpd)v_Img_GtHpd.elementAt(i);
          rg.MoverA(g_Img.Img_Dax, "ZZHPD");
          rg.MoverA(g_Img.Img_Seq, rg.Suma(g_Img.Img_Seq,0,"1",0,3,0));
          rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtHpd(g_Img_GtHpd));
          vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
          v_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
        }
    //--------------------------------------------------------------------------
    for (i=0; i<v_TabImg.size(); i++)
        {
          BF_IMG.Buf_Img l_Img = new BF_IMG.Buf_Img();
          l_Img = (BF_IMG.Buf_Img)v_TabImg.elementAt(i);
          if (!(l_Img.Img_Dax.toString().trim().equals("BASE")
             || l_Img.Img_Dax.toString().trim().equals("XTSD")
             || l_Img.Img_Dax.toString().trim().equals("XXHIP")
             || l_Img.Img_Dax.toString().trim().equals("YYPRD")
             || l_Img.Img_Dax.toString().trim().equals("ZZHPD")))
             { 
               vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(l_Img));
               v_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
             }
        }
    return v_Img;
  }
  //---------------------------------------------------------------------------------------
  public void aux_vImgs(RunData data, Context context)
              throws Exception 
  {
    v_Img_GtHyp.clear();
    for (i=0; i<v_Imgs.GT724_Hyp.size(); i++)
        {
          vecGT724.Bff_Hyp l_Hyp = new vecGT724.Bff_Hyp();
          l_Hyp = (vecGT724.Bff_Hyp)v_Imgs.GT724_Hyp.elementAt(i);
          g_Img_GtHyp = gtimg.LSet_A_ImgGtHyp(l_Hyp.Hyp_Hyp.toString());
          v_Img_GtHyp.add(g_Img_GtHyp); 
        }
    data.getUser().removeTemp("Agtp724_vImgGtHyp");
    data.getUser().setTemp("Agtp724_vImgGtHyp", v_Img_GtHyp);
  }
  //---------------------------------------------------------------------------------------
  public void Recalcula_GtApe(RunData data, Context context)
              throws Exception 
  {
    g_Img_GtTsd = (GT_IMG.Buf_Img_GtTsd)data.getUser().getTemp("Agtp724_ImgGtTsd");
    v_Img_GtHyp.clear();
    v_Img_GtHpd.clear();
    for (i=0; i<v_Imgs.GT724_Hyp.size(); i++)
        {
          vecGT724.Bff_Hyp l_Hyp = new vecGT724.Bff_Hyp();
          l_Hyp = (vecGT724.Bff_Hyp)v_Imgs.GT724_Hyp.elementAt(i);
          g_Img_GtHyp = gtimg.LSet_A_ImgGtHyp(l_Hyp.Hyp_Hyp.toString());
          for (j=0; j<l_Hyp.Hyp_Hpd.size(); j++)
              {
                vecGT724.Bff_Hpd l_Hpd = new vecGT724.Bff_Hpd();
                l_Hpd = (vecGT724.Bff_Hpd)l_Hyp.Hyp_Hpd.elementAt(j);
                g_Img_GtHpd = gtimg.LSet_A_ImgGtHpd(l_Hpd.Hpd_Hpd.toString());
                v_Img_GtHpd.add(g_Img_GtHpd); 
              }
          v_Img_GtHyp.add(g_Img_GtHyp); 
        }
    String lcVtt = rg.Zeros(15);
    String lcVts = rg.Zeros(15);
    String lcVtb = rg.Zeros(15);
    String lcVcn = rg.Zeros(15);
    for (i=0; i<v_Img_GtHyp.size(); i++)
        {
          g_Img_GtHyp = (GT_IMG.Buf_Img_GtHyp)v_Img_GtHyp.elementAt(i);
          if (g_Img_GtHyp.GtHyp_Vts.toString().trim().equals(""))
             { rg.MoverA(g_Img_GtHyp.GtHyp_Vts, rg.Zeros(15)); }
          if (g_Img_GtHyp.GtHyp_Vtb.toString().trim().equals(""))
             { rg.MoverA(g_Img_GtHyp.GtHyp_Vtb, rg.Zeros(15)); }
          if (g_Img_GtHyp.GtHyp_Vcn.toString().trim().equals(""))
             { rg.MoverA(g_Img_GtHyp.GtHyp_Vcn, rg.Zeros(15)); }
          lcVtt = rg.Suma(lcVtt,4,g_Img_GtHyp.GtHyp_Vts,2,15,4);
          lcVts = rg.Suma(lcVts,2,g_Img_GtHyp.GtHyp_Vts,2,15,2);
          lcVtb = rg.Suma(lcVtb,2,g_Img_GtHyp.GtHyp_Vtb,2,15,2);
          lcVcn = rg.Suma(lcVcn,2,g_Img_GtHyp.GtHyp_Vcn,2,15,2);
        }
    rg.MoverA(g_Img_GtApe.GtApe_Vtt, lcVtt);
    rg.MoverA(g_Img_GtApe.GtApe_Vts, lcVts);
    rg.MoverA(g_Img_GtApe.GtApe_Vtb, lcVtb);
    rg.MoverA(g_Img_GtApe.GtApe_Vcn, lcVcn);
    //------------------------------------------------------------------------------
    rg.MoverA(g_Img_GtApe.GtApe_Hvt, rg.Zeros(15));
    rg.MoverA(g_Img_GtApe.GtApe_Hvl, rg.Zeros(15));
    rg.MoverA(g_Img_GtApe.GtApe_Hvb, rg.Zeros(15));
    rg.MoverA(g_Img_GtApe.GtApe_Hvc, rg.Zeros(15));
    rg.MoverA(g_Img_GtApe.GtApe_Pvt, rg.Zeros(15));
    rg.MoverA(g_Img_GtApe.GtApe_Pvl, rg.Zeros(15));
    rg.MoverA(g_Img_GtApe.GtApe_Pvb, rg.Zeros(15));
    rg.MoverA(g_Img_GtApe.GtApe_Pvc, rg.Zeros(15));
    for (i=0; i<v_Img_GtHpd.size(); i++)
        {
          lcData = gtimg.LSet_De_ImgGtHpd((GT_IMG.Buf_Img_GtHpd)v_Img_GtHpd.elementAt(i));
          g_Img_GtHpd = gtimg.LSet_A_ImgGtHpd(lcData);
          if (g_Img_GtHpd.GtHpd_Itp.toString().equals("TE")
           || g_Img_GtHpd.GtHpd_Itp.toString().equals("ED")
           || g_Img_GtHpd.GtHpd_Itp.toString().equals("OC")
           || g_Img_GtHpd.GtHpd_Itp.toString().equals("SA"))
              {
                rg.MoverA(g_Img_GtApe.GtApe_Hvt, rg.Suma(g_Img_GtApe.GtApe_Hvt,2,g_Img_GtHpd.GtHpd_Vts,2,15,2));
                rg.MoverA(g_Img_GtApe.GtApe_Hvl, rg.Suma(g_Img_GtApe.GtApe_Hvl,2,g_Img_GtHpd.GtHpd_Vlq,2,15,2));
                rg.MoverA(g_Img_GtApe.GtApe_Hvb, rg.Suma(g_Img_GtApe.GtApe_Hvb,2,g_Img_GtHpd.GtHpd_Vtb,2,15,2));
                rg.MoverA(g_Img_GtApe.GtApe_Hvc, rg.Suma(g_Img_GtApe.GtApe_Hvc,2,g_Img_GtHpd.GtHpd_Vcn,2,15,2));
              }
          else
              {
                rg.MoverA(g_Img_GtApe.GtApe_Pvt, rg.Suma(g_Img_GtApe.GtApe_Pvt,2,g_Img_GtHpd.GtHpd_Vts,2,15,2));
                rg.MoverA(g_Img_GtApe.GtApe_Pvl, rg.Suma(g_Img_GtApe.GtApe_Pvl,2,g_Img_GtHpd.GtHpd_Vlq,2,15,2));
                rg.MoverA(g_Img_GtApe.GtApe_Pvb, rg.Suma(g_Img_GtApe.GtApe_Pvb,2,g_Img_GtHpd.GtHpd_Vtb,2,15,2));
                rg.MoverA(g_Img_GtApe.GtApe_Pvc, rg.Suma(g_Img_GtApe.GtApe_Pvc,2,g_Img_GtHpd.GtHpd_Vcn,2,15,2));
              }
        }
  }
  //---------------------------------------------------------------------------------------
  public void vecImpresion(RunData data, Context context)
              throws Exception 
  {
    v_Imgs = (vecGT724.Buf_vecGT724)data.getUser().getTemp("Agtp724_vImgs");
    v_Img_GtHyp.clear();
    v_Img_GtHpd.clear();
    for (i=0; i<v_Imgs.GT724_Hyp.size(); i++)
        {
          vecGT724.Bff_Hyp l_Hyp = new vecGT724.Bff_Hyp();
          l_Hyp = (vecGT724.Bff_Hyp)v_Imgs.GT724_Hyp.elementAt(i);
          g_Img_GtHyp = gtimg.LSet_A_ImgGtHyp(l_Hyp.Hyp_Hyp.toString());
          for (j=0; j<l_Hyp.Hyp_Hpd.size(); j++)
              {
                vecGT724.Bff_Hpd l_Hpd = new vecGT724.Bff_Hpd();
                l_Hpd = (vecGT724.Bff_Hpd)l_Hyp.Hyp_Hpd.elementAt(j);
                g_Img_GtHpd = gtimg.LSet_A_ImgGtHpd(l_Hpd.Hpd_Hpd.toString());
                v_Img_GtHpd.add(g_Img_GtHpd); 
              }
          v_Img_GtHyp.add(g_Img_GtHyp); 
        }
    data.getUser().removeTemp("Agtp724_vImgGtHyp");
    data.getUser().setTemp("Agtp724_vImgGtHyp", v_Img_GtHyp);
    data.getUser().removeTemp("Agtp724_vImgGtHpd");
    data.getUser().setTemp("Agtp724_vImgGtHpd", v_Img_GtHpd);
  }
  //---------------------------------------------------------------------------------------
  public void Actualiza_Valores(RunData data, Context context)
              throws Exception 
  {
    //for (i=0; i<v_Img_GtHyp.size(); i++)
    //    {
    //      g_Img_GtHyp = (GT_IMG.Buf_Img_GtHyp)v_Img_GtHyp.elementAt(i);
    //      rg.MoverA(g_Img_GtHyp.GtHyp_Frv, g_Img_GtTsd.GtTsd_Fts);
    //      rg.MoverA(g_Img_GtHyp.GtHyp_Vur, g_Img_GtApe.GtApe_Vuo);
    //      rg.MoverA(g_Img_GtHyp.GtHyp_Fvt, rg.DateAdd(g_Img_GtHyp.GtHyp_Frv,"A",2));
    //      rg.MoverA(g_Img_GtHyp.GtHyp_Vtt, rg.Zeros(g_Img_GtHyp.GtHyp_Vtt));
    //      rg.MoverA(g_Img_GtHyp.GtHyp_Vts, rg.Zeros(g_Img_GtHyp.GtHyp_Vts));
    //      rg.MoverA(g_Img_GtHyp.GtHyp_Vcn, rg.Zeros(g_Img_GtHyp.GtHyp_Vcn));
    //      rg.MoverA(g_Img_GtHyp.GtHyp_Vlq, rg.Zeros(g_Img_GtHyp.GtHyp_Vlq));
    //      rg.MoverA(g_Img_GtHyp.GtHyp_Vrt, rg.Zeros(g_Img_GtHyp.GtHyp_Vrt));
    //      rg.MoverA(g_Img_GtHyp.GtHyp_Vsg, rg.Zeros(g_Img_GtHyp.GtHyp_Vsg));
    //      v_Img_GtHyp.set(i, g_Img_GtHyp);
    //    }
    ////--------------------------------------------------------------------------
    //for (i=0; i<v_Img_GtHpd.size(); i++)
    //    {
    //      g_Img_GtHpd = (GT_IMG.Buf_Img_GtHpd)v_Img_GtHpd.elementAt(i);
    //      rg.MoverA(g_Img_GtHpd.GtHpd_Pum, rg.Zeros(g_Img_GtHpd.GtHpd_Pum));
    //      rg.MoverA(g_Img_GtHpd.GtHpd_Vtu, rg.Zeros(g_Img_GtHpd.GtHpd_Vtu));
    //      rg.MoverA(g_Img_GtHpd.GtHpd_Vum, rg.Zeros(g_Img_GtHpd.GtHpd_Vum));
    //      rg.MoverA(g_Img_GtHpd.GtHpd_Vtt, rg.Zeros(g_Img_GtHpd.GtHpd_Vtt));
    //      rg.MoverA(g_Img_GtHpd.GtHpd_Vts, rg.Zeros(g_Img_GtHpd.GtHpd_Vts));
    //      rg.MoverA(g_Img_GtHpd.GtHpd_Tuf, rg.Zeros(g_Img_GtHpd.GtHpd_Tuf));
    //      rg.MoverA(g_Img_GtHpd.GtHpd_Ttc, rg.Zeros(g_Img_GtHpd.GtHpd_Ttc));
    //      rg.MoverA(g_Img_GtHpd.GtHpd_Tps, rg.Zeros(g_Img_GtHpd.GtHpd_Tps));
    //      rg.MoverA(g_Img_GtHpd.GtHpd_Vcn, rg.Zeros(g_Img_GtHpd.GtHpd_Vcn));
    //      rg.MoverA(g_Img_GtHpd.GtHpd_Vlq, rg.Zeros(g_Img_GtHpd.GtHpd_Vlq));
    //      rg.MoverA(g_Img_GtHpd.GtHpd_Vtb, rg.Zeros(g_Img_GtHpd.GtHpd_Vtb));
    //      rg.MoverA(g_Img_GtHpd.GtHpd_Vsg, rg.Zeros(g_Img_GtHpd.GtHpd_Vsg));
    //      v_Img_GtHpd.set(i, g_Img_GtHpd);
    //    }
  }
  public void Re_PRMPC080(RunData data, Context context)
              throws Exception
  {
    g_PrmPC080 = PRMPC080.Inicia_PrmPC080();
    RUTGEN.MoverA(g_PrmPC080.PC080_Fte,     "TSD");
    RUTGEN.MoverA(g_PrmPC080.PC080_Pai,     "CL");
    RUTGEN.MoverA(g_PrmPC080.PC080_Cnr.Sis, "GAR");
    RUTGEN.MoverA(g_PrmPC080.PC080_Slr.Slc, RUTGEN.Zeros(7));
    RUTGEN.MoverA(g_PrmPC080.PC080_Suc,     g_Msg.Msg_Suc);
    RUTGEN.MoverA(g_PrmPC080.PC080_Nsu,     g_Msg.Msg_Nsu);
    RUTGEN.MoverA(g_PrmPC080.PC080_Mnd,     "000");
    RUTGEN.MoverA(g_PrmPC080.PC080_Nmn,     "PESOS CHILE");
    RUTGEN.MoverA(g_PrmPC080.PC080_Eje,     g_Msg.Msg_Cus);
    RUTGEN.MoverA(g_PrmPC080.PC080_Nej,     g_Msg.Msg_Nus);
    RUTGEN.MoverA(g_PrmPC080.PC080_Tej,     g_Msg.Msg_Fus);
    RUTGEN.MoverA(g_PrmPC080.PC080_Tsd,     g_Msg.Msg_Cus.substring(1));
    RUTGEN.MoverA(g_Msg.Msg_Dat,            PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
    BF_MSG.Param_Program(data, g_Msg);
  }
  //---------------------------------------------------------------------------------------
}