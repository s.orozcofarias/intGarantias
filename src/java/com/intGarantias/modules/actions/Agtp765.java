// Source File Name:   Agtp765.java
// Descripcion     :   Ingreso Tasacion Prendas

package com.intGarantias.modules.actions;

//import com.FhtNucleo.services.configurator.FhtConfigurator;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.BF_LOB;
import com.FHTServlet.modules.global.BF_IMG;
import com.FHTServlet.modules.global.PRMPC080;

import com.intGlobal.modules.global.MSGED090;
import com.intGlobal.modules.global.PRMTG123;
import com.intGlobal.modules.global.PRMTG130;

import com.intGarantias.modules.global.GT_IMG;
import com.intGarantias.modules.global.PRMGT093;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp765 extends FHTServletAction
{
  //---------------------------------------------------------------------------------------
  public int i = 0;
  public String lcData = "";
  public Vector vDatos;
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  BF_LOB lob                       = new BF_LOB();
  PRMPC080.PrmPC080 g_PrmPC080     = new PRMPC080.PrmPC080();
  Vector v_Img_GtHpd               = new Vector();
  BF_IMG img                       = new BF_IMG();
  BF_IMG.Buf_Img g_Img             = new BF_IMG.Buf_Img();
  BF_IMG.Buf_Img_Bse g_Img_Base    = new BF_IMG.Buf_Img_Bse();
  GT_IMG gtimg                     = new GT_IMG();
  GT_IMG.Buf_Img_GtApe g_Img_GtApe = new GT_IMG.Buf_Img_GtApe();
  GT_IMG.Buf_Img_GtHyp g_Img_GtHyp = new GT_IMG.Buf_Img_GtHyp();
  GT_IMG.Buf_Img_GtHpd g_Img_GtHpd = new GT_IMG.Buf_Img_GtHpd();
  MSGED090.Buf_MsgED090 g_MsgED090 = new MSGED090.Buf_MsgED090();
  PRMGT093.Buf_PrmGT093 g_PrmGT093 = new PRMGT093.Buf_PrmGT093();
  PRMTG123.Buf_PrmTG123 g_PrmTG123 = new PRMTG123.Buf_PrmTG123();
  PRMTG130.Buf_PrmTG130 g_PrmTG130 = new PRMTG130.Buf_PrmTG130();
  //---------------------------------------------------------------------------------------
  public Agtp765()
  {
  }
  //---------------------------------------------------------------------------------------
  public void doPerform(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP765[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp765-DIN", data);
    if (g_Msg.Msg_Pgr.toString().equals("RT"))
       { doAgtp765_Continue(data, context); }
    else
       { doAgtp765_Init(data, context); }
  }
  //---------------------------------------------------------------------------------------
  public void doCancel(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP765[doCancel.start]", "[CANCELADO]");
    setTemplate(data, "Garantias,Agt,AGTP765.vm");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp765_Init(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP765[doAgtp765_Init.start]", "[" + data.getUser().getUserName() + "]");
    GT_IMG.Buf_Img_GtHyp l_Img_GtHyp = new GT_IMG.Buf_Img_GtHyp();
    g_PrmPC080  = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp724_ImgGtApe");
    l_Img_GtHyp = (GT_IMG.Buf_Img_GtHyp)data.getUser().getTemp("Agtp724_ImgGtHyp");
    g_Img_GtHyp = gtimg.LSet_A_ImgGtHyp(gtimg.LSet_De_ImgGtHyp(l_Img_GtHyp));
    v_Img_GtHpd = (Vector)data.getUser().getTemp("Agtp724_vImgGtHpd");
    if  (v_Img_GtHpd.size()==0)
        { 
          g_Img_GtHpd = gtimg.LSet_A_ImgGtHpd(rg.Blancos(827)); 
          Inicializa_GtHpd();
        }
    else
        {
          lcData = gtimg.LSet_De_ImgGtHpd((GT_IMG.Buf_Img_GtHpd)v_Img_GtHpd.elementAt(0));
          g_Img_GtHpd = gtimg.LSet_A_ImgGtHpd(lcData);
        }
    data.getUser().removeTemp("Agtp765_ImgGtHpd");
    data.getUser().setTemp("Agtp765_ImgGtHpd", g_Img_GtHpd);
    data.getUser().removeTemp("Agtp765_Ovb");
    if (g_Img_GtHpd.GtHpd_Ovb.toString().trim().equals("")
      ||g_Img_GtHpd.GtHpd_Ovb.toString().trim().equals("000000000"))
       { data.getUser().setTemp("Agtp765_Ovb", ""); }
    else
       { data.getUser().setTemp("Agtp765_Ovb", lob.leeCLOB(g_Img_GtHpd.GtHpd_Ovb.toString())); }
    data.getUser().removeTemp("Agtp765_ImgGtHyp");
    data.getUser().setTemp("Agtp765_ImgGtHyp", g_Img_GtHyp);
    data.getUser().removeTemp("Agtp765_Obs");
    if (g_Img_GtHyp.GtHyp_Obs.toString().trim().equals("")
      ||g_Img_GtHyp.GtHyp_Obs.toString().trim().equals("000000000"))
       { data.getUser().setTemp("Agtp765_Obs", ""); }
    else
       { data.getUser().setTemp("Agtp765_Obs", lob.leeCLOB(g_Img_GtHyp.GtHyp_Obs.toString())); }
    data.getUser().removeTemp("Agtp765_PrmPC080");
    data.getUser().setTemp("Agtp765_PrmPC080", g_PrmPC080);
    setTemplate(data, "Garantias,Agt,AGTP765.vm" );
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp765(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP765[doAgtp765.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp765-MAN", data);
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    String Opc = data.getParameters().getString("Opc", "");
    if (Opc.trim().equals("I"))
       {
         //Boton Imagen
         data.getUser().removeTemp("Agtp765_g_PrmPC080");
         data.getUser().setTemp("Agtp765_g_PrmPC080", g_PrmPC080);
         Actualiza_Datos(data, context);
         g_Img_GtHyp = (GT_IMG.Buf_Img_GtHyp)data.getUser().getTemp("Agtp765_ImgGtHyp");
         rg.MoverA(g_PrmPC080.PC080_Ntc, g_Img_GtHyp.GtHyp_Seq);
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         //BF_MSG.Link_Program("Agtp765", "Agtp729", data, g_Msg);
         BF_MSG.Link_Program("Agtp765", "Appp096", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("S"))
       {
         //Boton Salir
         data.getUser().removeTemp("Agtp765_PrmPC080");
         data.getUser().removeTemp("Agtp765_ImgGtHyp");
         data.getUser().removeTemp("Agtp765_ImgGtHpd");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "NK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp765", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("A"))
       {
         //Boton Aceptar
         Actualiza_Datos(data, context);
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp724_ImgGtApe");
         g_Img_GtHyp = (GT_IMG.Buf_Img_GtHyp)data.getUser().getTemp("Agtp765_ImgGtHyp");
         g_Img_GtHpd = (GT_IMG.Buf_Img_GtHpd)data.getUser().getTemp("Agtp765_ImgGtHpd");
         v_Img_GtHpd = (Vector)data.getUser().getTemp("Agtp724_vImgGtHpd");
         if  (v_Img_GtHpd.size()==0)
             { v_Img_GtHpd.add(g_Img_GtHpd); }
         else
             { v_Img_GtHpd.set(0, g_Img_GtHpd); }
         rg.MoverA(g_Img_GtHyp.GtHyp_Vtf, rg.Divide(g_Img_GtHpd.GtHpd_Vts,2,g_Img_GtApe.GtApe_Vuf,2,15,4,4));
         rg.MoverA(g_Img_GtHyp.GtHyp_Vtd, rg.Divide(g_Img_GtHpd.GtHpd_Vts,2,g_Img_GtApe.GtApe_Vus,2,15,4,2));
         rg.MoverA(g_Img_GtHyp.GtHyp_Vts, g_Img_GtHpd.GtHpd_Vts);
         rg.MoverA(g_Img_GtHyp.GtHyp_Vtb, g_Img_GtHpd.GtHpd_Vtb);
         rg.MoverA(g_Img_GtHyp.GtHyp_Vlq, g_Img_GtHpd.GtHpd_Vlq);
         rg.MoverA(g_Img_GtHyp.GtHyp_Vcn, g_Img_GtHpd.GtHpd_Vcn);
         rg.MoverA(g_Img_GtHyp.GtHyp_Vsg, g_Img_GtHpd.GtHpd_Vsg);
         data.getUser().removeTemp("Agtp724_vImgGtHpd");
         data.getUser().setTemp("Agtp724_vImgGtHpd", v_Img_GtHpd);
         data.getUser().removeTemp("Agtp724_ImgGtHyp");
         data.getUser().setTemp("Agtp724_ImgGtHyp", g_Img_GtHyp);
         data.getUser().removeTemp("Agtp765_PrmPC080");
         data.getUser().removeTemp("Agtp765_ImgGtHyp");
         data.getUser().removeTemp("Agtp765_ImgGtHpd");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp765", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("1"))
       {
         //Consulta Lugares Disponibles
         Actualiza_Datos(data, context);
         g_PrmTG130 = PRMTG130.Inicia_PrmTG130();
         rg.MoverA(g_PrmTG130.TG130_Cmp, "DMC-LUG");
         rg.MoverA(g_PrmTG130.TG130_Ncm, "Lugares de Domicilios");
         data.getUser().removeTemp("Agtp765_g_PrmPC080");
         data.getUser().setTemp("Agtp765_g_PrmPC080" ,g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMTG130.LSet_De_PrmTG130(g_PrmTG130));
         BF_MSG.Link_Program("Agtp765", "Atgp130", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("2"))
       {
         //Consulta Sectores Disponibles
         Actualiza_Datos(data, context);
         g_PrmTG130 = PRMTG130.Inicia_PrmTG130();
         rg.MoverA(g_PrmTG130.TG130_Cmp, "DMC-SEC");
         rg.MoverA(g_PrmTG130.TG130_Ncm, "Sectores de Domicilios");
         data.getUser().removeTemp("Agtp765_g_PrmPC080");
         data.getUser().setTemp("Agtp765_g_PrmPC080" ,g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMTG130.LSet_De_PrmTG130(g_PrmTG130));
         BF_MSG.Link_Program("Agtp765", "Atgp130", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("3"))
       {
         //Consulta Comunas Disponibles
         Actualiza_Datos(data, context);
         g_PrmTG123 = PRMTG123.Inicia_PrmTG123();
         data.getUser().removeTemp("Agtp765_g_PrmPC080");
         data.getUser().setTemp("Agtp765_g_PrmPC080" ,g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMTG123.LSet_De_PrmTG123(g_PrmTG123));
         BF_MSG.Link_Program("Agtp765", "Atgp124", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("4"))
       {
         //Consulta Especialidades Disponibles
         Actualiza_Datos(data, context);
         g_PrmTG130 = PRMTG130.Inicia_PrmTG130();
         rg.MoverA(g_PrmTG130.TG130_Cmp, "GAR-ITG");
         rg.MoverA(g_PrmTG130.TG130_Ncm, "Especialidades de Garantias");
         data.getUser().removeTemp("Agtp765_g_PrmPC080");
         data.getUser().setTemp("Agtp765_g_PrmPC080" ,g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMTG130.LSet_De_PrmTG130(g_PrmTG130));
         BF_MSG.Link_Program("Agtp765", "Atgp130", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("5"))
       {
         //Consulta PContables Disponibles
         Actualiza_Datos(data, context);
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp724_ImgGtApe");
         g_Img_Base  = img.LSet_A_ImgBase(g_Img_GtApe.GtApe_Bse.toString());
         g_Img_GtHyp = (GT_IMG.Buf_Img_GtHyp)data.getUser().getTemp("Agtp765_ImgGtHyp");
         g_PrmGT093 = PRMGT093.Inicia_PrmGT093();
         rg.MoverA(g_PrmGT093.GT093_Sis, "GAR");
         rg.MoverA(g_PrmGT093.GT093_Dcn, g_Img_Base.Img_Base_Dcn);
         rg.MoverA(g_PrmGT093.GT093_Itb, "P");
         data.getUser().removeTemp("Agtp765_g_PrmPC080");
         data.getUser().setTemp("Agtp765_g_PrmPC080" ,g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMGT093.LSet_De_PrmGT093(g_PrmGT093));
         BF_MSG.Link_Program("Agtp765", "Agtp093", data, g_Msg);
         return;
       }
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp765_Continue(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP765[doAgtp765_Continue.start]", "[" + data.getUser().getUserName() + "]");
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Atgp124"))
       {
         g_PrmTG123 = PRMTG123.LSet_A_PrmTG123(g_Msg.Msg_Dat.toString());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp765_g_PrmPC080");
         data.getUser().removeTemp("Agtp765_g_PrmPC080");
         if (g_PrmTG123.TG123_Prv.toString().trim().compareTo("")!=0)
             {
               g_Img_GtHyp = (GT_IMG.Buf_Img_GtHyp)data.getUser().getTemp("Agtp765_ImgGtHyp");
               rg.MoverA(g_Img_GtHyp.GtHyp_Cmn, g_PrmTG123.TG123_Prv.toString() + g_PrmTG123.TG123_Cmn.toString());
               rg.MoverA(g_Img_GtHyp.GtHyp_Ncm, g_PrmTG123.TG123_Ncm);
               rg.MoverA(g_Img_GtHyp.GtHyp_Npv, g_PrmTG123.TG123_Npr);
               data.getUser().removeTemp("Agtp765_ImgGtHyp");
               data.getUser().setTemp("Agtp765_ImgGtHyp", g_Img_GtHyp);
             }
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP765.vm" );
         return;
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Atgp130"))
       {
         g_PrmTG130 = PRMTG130.LSet_A_PrmTG130(g_Msg.Msg_Dat.toString());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp765_g_PrmPC080");
         data.getUser().removeTemp("Agtp765_g_PrmPC080");
         if (g_PrmTG130.TG130_Cmp.toString().trim().equals("DMC-LUG"))
             {
               if (g_PrmTG130.TG130_Cod.toString().trim().compareTo("")!=0)
                  {
                    g_Img_GtHyp = (GT_IMG.Buf_Img_GtHyp)data.getUser().getTemp("Agtp765_ImgGtHyp");
                    rg.MoverA(g_Img_GtHyp.GtHyp_Lug, g_PrmTG130.TG130_Cod);
                    if (g_Img_GtHyp.GtHyp_Lug.toString().trim().equals("<>"))
                       { rg.MoverA(g_Img_GtHyp.GtHyp_Lnm, ""); }
                    data.getUser().removeTemp("Agtp765_ImgGtHyp");
                    data.getUser().setTemp("Agtp765_ImgGtHyp", g_Img_GtHyp);
                  }
             }
         if (g_PrmTG130.TG130_Cmp.toString().trim().equals("DMC-SEC"))
             {
               if (g_PrmTG130.TG130_Cod.toString().trim().compareTo("")!=0)
                  {
                    g_Img_GtHyp = (GT_IMG.Buf_Img_GtHyp)data.getUser().getTemp("Agtp765_ImgGtHyp");
                    rg.MoverA(g_Img_GtHyp.GtHyp_Sec, g_PrmTG130.TG130_Cod);
                    if (g_Img_GtHyp.GtHyp_Sec.toString().trim().equals("<>"))
                       { rg.MoverA(g_Img_GtHyp.GtHyp_Snb, ""); }
                    data.getUser().removeTemp("Agtp765_ImgGtHyp");
                    data.getUser().setTemp("Agtp765_ImgGtHyp", g_Img_GtHyp);
                  }
             }
         if (g_PrmTG130.TG130_Cmp.toString().trim().equals("GAR-ITG"))
             {
               if (g_PrmTG130.TG130_Cod.toString().trim().compareTo("")!=0)
                  {
                    g_Img_GtHyp = (GT_IMG.Buf_Img_GtHyp)data.getUser().getTemp("Agtp765_ImgGtHyp");
                    rg.MoverA(g_Img_GtHyp.GtHyp_Itg, g_PrmTG130.TG130_Cod);
                    data.getUser().removeTemp("Agtp765_ImgGtHyp");
                    data.getUser().setTemp("Agtp765_ImgGtHyp", g_Img_GtHyp);
                  }
             }
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP765.vm" );
         return;
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Agtp093"))
       {
         g_PrmGT093 = PRMGT093.LSet_A_PrmGT093(g_Msg.Msg_Dat.toString());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp765_g_PrmPC080");
         data.getUser().removeTemp("Agtp765_g_PrmPC080");
         if (g_PrmGT093.GT093_Rtn.toString().trim().equals("OK"))
             {
               g_Img_GtHyp = (GT_IMG.Buf_Img_GtHyp)data.getUser().getTemp("Agtp765_ImgGtHyp");
               rg.MoverA(g_Img_GtHyp.GtHyp_Dpc, g_PrmGT093.GT093_Dpc);
               rg.MoverA(g_Img_GtHyp.GtHyp_Pct, rg.FmtValor(g_PrmGT093.GT093_Pct,0,2,5,"+"));
               data.getUser().removeTemp("Agtp765_ImgGtHyp");
               data.getUser().setTemp("Agtp765_ImgGtHyp", g_Img_GtHyp);
             }
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP765.vm" );
         return;
       }
    //if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Agtp729"))
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Appp096"))
       {
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp765_g_PrmPC080");
         data.getUser().removeTemp("Agtp765_g_PrmPC080");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP765.vm" );
         return;
       }
  }
  //---------------------------------------------------------------------------------------
  public void Actualiza_Datos(RunData data, Context context)
              throws Exception 
  {
    int dPE = 0;
    int dUF = 4;
//    if (!FhtConfigurator.getPai().equals("CL"))
//       { 
//         dPE = 2; 
//         dUF = 2; 
//       }
    g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp724_ImgGtApe");
    g_Img_GtHyp = (GT_IMG.Buf_Img_GtHyp)data.getUser().getTemp("Agtp765_ImgGtHyp");
    g_Img_GtHpd = (GT_IMG.Buf_Img_GtHpd)data.getUser().getTemp("Agtp765_ImgGtHpd");
    rg.MoverA(g_Img_GtHyp.GtHyp_Itg, data.getParameters().getString("GtHyp_Itg", ""));
    rg.MoverA(g_Img_GtHyp.GtHyp_Dpc, data.getParameters().getString("GtHyp_Dpc", ""));
    rg.MoverA(g_Img_GtHyp.GtHyp_Dsb, data.getParameters().getString("GtHyp_Dsb", ""));
    rg.MoverA(g_Img_GtHyp.GtHyp_Cll, data.getParameters().getString("GtHyp_Cll", ""));
    rg.MoverA(g_Img_GtHyp.GtHyp_Cnm, data.getParameters().getString("GtHyp_Cnm", ""));
    rg.MoverA(g_Img_GtHyp.GtHyp_Lug, data.getParameters().getString("GtHyp_Lug", ""));
    rg.MoverA(g_Img_GtHyp.GtHyp_Lnm, data.getParameters().getString("GtHyp_Lnm", ""));
    rg.MoverA(g_Img_GtHyp.GtHyp_Sec, data.getParameters().getString("GtHyp_Sec", ""));
    rg.MoverA(g_Img_GtHyp.GtHyp_Snb, data.getParameters().getString("GtHyp_Snb", ""));
    rg.MoverA(g_Img_GtHyp.GtHyp_Cmn, data.getParameters().getString("GtHyp_Cmn", ""));
    rg.MoverA(g_Img_GtHyp.GtHyp_Ncm, data.getParameters().getString("GtHyp_Ncm", ""));
    rg.MoverA(g_Img_GtHyp.GtHyp_Npv, data.getParameters().getString("GtHyp_Npv", ""));
    lcData = data.getParameters().getString("GtHyp_Obs", "");
    if (!lcData.trim().equals(""))
       { 
         if (g_Img_GtHyp.GtHyp_Obs.toString().trim().equals("")
           ||g_Img_GtHyp.GtHyp_Obs.toString().trim().equals("000000000"))
            { rg.MoverA(g_Img_GtHyp.GtHyp_Obs, Asigna_Folio("FOLIO-CLOBS", data, context)); }
         lob.mantieneCLOB("M", g_Img_GtHyp.GtHyp_Obs.toString(), lcData);
       }
    rg.MoverA(g_Img_GtHpd.GtHpd_Faf, data.getParameters().getString("GtHpd_Faf", ""));
    rg.MoverA(g_Img_GtHpd.GtHpd_Vaf, data.getParameters().getString("GtHpd_Vaf", ""));
    rg.MoverA(g_Img_GtHpd.GtHpd_Umd, "U");
    rg.MoverA(g_Img_GtHpd.GtHpd_Udd, rg.FmtValor("1",0,2,9,"+"));
    rg.MoverA(g_Img_GtHpd.GtHpd_Umt, data.getParameters().getString("GtHpd_Umt", ""));
    rg.MoverA(g_Img_GtHpd.GtHpd_Pum, data.getParameters().getString("GtHpd_Vtu", ""));
    int dcm = 0;
    String vum = g_Img_GtApe.GtApe_Vuf.toString();
    if (g_Img_GtHpd.GtHpd_Umt.toString().equals("P"))
       {
         dcm = dPE;
         vum = rg.FmtValor("1",0,2,9,"+");
       }
    if (g_Img_GtHpd.GtHpd_Umt.toString().equals("U"))
       {
         dcm = dUF;
         vum = g_Img_GtApe.GtApe_Vuf.toString();
       }
    if (g_Img_GtHpd.GtHpd_Umt.toString().equals("D"))
       {
         dcm = 2;
         vum = g_Img_GtApe.GtApe_Vus.toString();
       }
    //rg.MoverA(g_Img_GtHpd.GtHpd_Vtu, rg.Multiplica(g_Img_GtHpd.GtHpd_Udd,2,g_Img_GtHpd.GtHpd_Pum,4,15,4,dcm));
    rg.MoverA(g_Img_GtHpd.GtHpd_Vtu, data.getParameters().getString("GtHpd_Vtu", ""));
    rg.MoverA(g_Img_GtHpd.GtHpd_Vbu, data.getParameters().getString("GtHpd_Vbu", ""));
    rg.MoverA(g_Img_GtHpd.GtHpd_Vlu, data.getParameters().getString("GtHpd_Vlu", ""));
    rg.MoverA(g_Img_GtHpd.GtHpd_Vsu, data.getParameters().getString("GtHpd_Vsu", ""));
    rg.MoverA(g_Img_GtHpd.GtHpd_Vts, rg.Multiplica(g_Img_GtHpd.GtHpd_Vtu,4,vum,2,15,2,dPE));
    rg.MoverA(g_Img_GtHpd.GtHpd_Vtb, rg.Multiplica(g_Img_GtHpd.GtHpd_Vbu,4,vum,2,15,2,dPE));
    rg.MoverA(g_Img_GtHpd.GtHpd_Vlq, rg.Multiplica(g_Img_GtHpd.GtHpd_Vlu,4,vum,2,15,2,dPE));
    rg.MoverA(g_Img_GtHpd.GtHpd_Vsg, rg.Multiplica(g_Img_GtHpd.GtHpd_Vsu,4,vum,2,15,2,dPE));
    if (g_Img_GtHpd.GtHpd_Vtb.toString().trim().equals("")
     || g_Img_GtHpd.GtHpd_Vtb.toString().equals(rg.Zeros(g_Img_GtHpd.GtHpd_Vtb)))
       {
         rg.MoverA(g_Img_GtHpd.GtHpd_Vbu, g_Img_GtHpd.GtHpd_Vtu);
         rg.MoverA(g_Img_GtHpd.GtHpd_Vtb, g_Img_GtHpd.GtHpd_Vts);
       }
    if (g_Img_GtHyp.GtHyp_Pct.toString().trim().compareTo("")!=0)
       {
         rg.MoverA(g_Img_GtHpd.GtHpd_Vcn, rg.Multiplica(g_Img_GtHpd.GtHpd_Vtb,2,g_Img_GtHyp.GtHyp_Pct,2,15,2,dPE));
         rg.MoverA(g_Img_GtHpd.GtHpd_Vcn, rg.Divide(g_Img_GtHpd.GtHpd_Vcn,2,"100",0,15,2,dPE));
       }
    rg.MoverA(g_Img_GtHpd.GtHpd_Ano, data.getParameters().getString("GtHpd_Ano", ""));
    rg.MoverA(g_Img_GtHpd.GtHpd_Mka, data.getParameters().getString("GtHpd_Mka", ""));
    rg.MoverA(g_Img_GtHpd.GtHpd_Mdl, data.getParameters().getString("GtHpd_Mdl", ""));
    rg.MoverA(g_Img_GtHpd.GtHpd_Nmt, data.getParameters().getString("GtHpd_Nmt", ""));
    rg.MoverA(g_Img_GtHpd.GtHpd_Nse, data.getParameters().getString("GtHpd_Nse", ""));
    rg.MoverA(g_Img_GtHpd.GtHpd_Nhu, data.getParameters().getString("GtHpd_Nhu", ""));
    rg.MoverA(g_Img_GtHpd.GtHpd_Avu, data.getParameters().getString("GtHpd_Avu", ""));
    rg.MoverA(g_Img_GtHpd.GtHpd_Plu, data.getParameters().getString("GtHpd_Plu", ""));
    rg.MoverA(g_Img_GtHpd.GtHpd_Nfa, data.getParameters().getString("GtHpd_Nfa", ""));
    rg.MoverA(g_Img_GtHpd.GtHpd_Mfa, data.getParameters().getString("GtHpd_Mfa", ""));
    rg.MoverA(g_Img_GtHpd.GtHpd_Plq, data.getParameters().getString("GtHpd_Plq", ""));
    lcData = data.getParameters().getString("GtHpd_Ovb", "");
    if (!lcData.trim().equals(""))
       { 
         if (g_Img_GtHpd.GtHpd_Ovb.toString().trim().equals("")
           ||g_Img_GtHpd.GtHpd_Ovb.toString().trim().equals("000000000"))
            { rg.MoverA(g_Img_GtHpd.GtHpd_Ovb, Asigna_Folio("FOLIO-CLOBS", data, context)); }
         lob.mantieneCLOB("M", g_Img_GtHpd.GtHpd_Ovb.toString(), lcData);
       }
    data.getUser().removeTemp("Agtp765_Ovb");
    if (g_Img_GtHpd.GtHpd_Ovb.toString().trim().equals("")
      ||g_Img_GtHpd.GtHpd_Ovb.toString().trim().equals("000000000"))
       { data.getUser().setTemp("Agtp765_Ovb", ""); }
    else
       { data.getUser().setTemp("Agtp765_Ovb", lob.leeCLOB(g_Img_GtHpd.GtHpd_Ovb.toString())); }
    data.getUser().removeTemp("Agtp765_Obs");
    if (g_Img_GtHyp.GtHyp_Obs.toString().trim().equals("")
      ||g_Img_GtHyp.GtHyp_Obs.toString().trim().equals("000000000"))
       { data.getUser().setTemp("Agtp765_Obs", ""); }
    else
       { data.getUser().setTemp("Agtp765_Obs", lob.leeCLOB(g_Img_GtHyp.GtHyp_Obs.toString())); }
    data.getUser().removeTemp("Agtp765_ImgGtHpd");
    data.getUser().setTemp("Agtp765_ImgGtHpd", g_Img_GtHpd);
    data.getUser().removeTemp("Agtp765_ImgGtHyp");
    data.getUser().setTemp("Agtp765_ImgGtHyp", g_Img_GtHyp);
  }
  //---------------------------------------------------------------------------------------
  public void Inicializa_GtHpd()
              throws Exception 
  {
    rg.MoverA(g_Img_GtHyp.GtHyp_Ibs, "001");
    rg.MoverA(g_Img_GtHyp.GtHyp_Idx, g_Img_GtHyp.GtHyp_Ibs);
    rg.MoverA(g_Img_GtHpd.GtHpd_Seq, g_Img_GtHyp.GtHyp_Seq);
    rg.MoverA(g_Img_GtHpd.GtHpd_Sqd, g_Img_GtHyp.GtHyp_Idx);
    rg.MoverA(g_Img_GtHpd.GtHpd_Itp, "PD");
    String Umt = "D";
//    if (FhtConfigurator.getPai().equals("CL"))
//       { Umt = "P"; }
Umt = "P";
    rg.MoverA(g_Img_GtHpd.GtHpd_Umt, Umt);
  }
  //---------------------------------------------------------------------------------------
  public String Asigna_Folio(String pcFolio, RunData data, Context context)
                throws Exception 
  {
    g_MsgED090 = MSGED090.Inicia_MsgED090();
    rg.MoverA(g_MsgED090.ED090_Idr, "F");
    rg.MoverA(g_MsgED090.ED090_Cod, pcFolio);
    rg.MoverA(g_Msg.Msg_Dat, MSGED090.LSet_De_MsgED090(g_MsgED090));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED090");
    g_MsgED090 = MSGED090.LSet_A_MsgED090(g_Msg.Msg_Dat.toString());
    String lcFolio = rg.FmtValor(g_MsgED090.ED090_Fol.toString(),0,0,9,"");
    return lcFolio;
  }
  //---------------------------------------------------------------------------------------
}