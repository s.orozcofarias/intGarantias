// Source File Name:   PRUEBA.java
// Descripcion     :   Ex AGTR459 Consulta Imagenes (-)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;

import com.intGarantias.modules.global.MSGGT459;

import java.util.*;
import java.sql.*;
import java.io.*;
import javax.servlet.http.*;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

import oracle.jdbc.*;
import oracle.sql.*;
// para accersar base de datos
//import org.apache.turbine.util.db.*;
//import org.apache.turbine.services.*;
//import org.apache.turbine.services.db.PoolBrokerService;
////import org.apache.turbine.util.db.pool.DBConnection;
//import org.apache.turbine.services.db.TurbineDB;

public class PRUEBA extends FHTServletAction
{ 
  //---------------------------------------------------------------------------------------
  MSGGT459.Buf_MsgGT459 g_MsgGT459 = new MSGGT459.Buf_MsgGT459();
  //---------------------------------------------------------------------------------------
  public PRUEBA()
  {
  }
  //---------------------------------------------------------------------------------------
  public void doPerform(RunData data, Context context)
              throws Exception
  {
  //Log.debug("PRUEBA.entra: ["  + "]");
    String BD = "GFF";
    g_MsgGT459 = (MSGGT459.Buf_MsgGT459)data.getUser().getTemp("Agtp459_MsgGT459");
    try
      { 
      	String Algo = g_MsgGT459.GT459_Idr.toString();
      	if (Algo.equals("I"))
      	   { BD = "GIF"; }
      }
    catch (Exception  e)
      { 
      	BD = "GIF";
      	g_MsgGT459 = (MSGGT459.Buf_MsgGT459)data.getUser().getTemp("Agtp729_MsgGT459");
      }
    String Llave = g_MsgGT459.GT459_Sis.toString() + g_MsgGT459.GT459_Ncn.toString() +
                   g_MsgGT459.GT459_Seq.toString() + g_MsgGT459.GT459_Fol.toString();
  //Log.debug("PRUEBA.BD...: [" + BD + "]");
  //Log.debug("PRUEBA.Llave: [" + Llave + "]");
    db_Banco(BD, Llave, data, context);
  //Log.debug("PRUEBA.sale: ["  + "]");
  }
  //---------------------------------------------------------------------------------------
  public void db_Banco(String BD, String llave, RunData data, Context context)
              throws Exception
  {
    String       url      = "jdbc:oracle:oci8:@";
    String       userName = "fht";
    String       password = "fht";
    Connection   conn;
    Statement    stmt;
    ResultSet    rset;
    String       sql;
  //String       llave = "GAR10016460010000050";
    String       GFF_TPI = "";
    String       GFF_KYV = "";

//DBConn 	= TurbineDB.getConnection("gar"); // se abre coneccion a una bd 
//DBConn.setAutoCommit(false);



    try
      {
//conn = DBConn.getConnection();
        conn = DriverManager.getConnection(url, userName, password);
        BLOB GFF_GIF = BLOB.createTemporary (conn, false, BLOB.DURATION_SESSION);
        stmt = conn.createStatement();
        if (BD.equals("GIF"))
           { rset = stmt.executeQuery("select * from TB_GIF where GIF_KYV = '" + llave + "'"); }
        else
           { rset = stmt.executeQuery("select * from TB_GFF where GFF_KYV = '" + llave + "'"); }
        while (rset.next())
           { 
             GFF_TPI = ((OracleResultSet)rset).getString(7); 
             GFF_GIF = ((OracleResultSet)rset).getBLOB(8); 
           }
        GFF_GIF.open(BLOB.MODE_READONLY);
      //Log.debug("PRUEBA.en: ["  + "]");
        data.getResponse().setContentType("text/HTML");
      //data.setContentType("application/x-cdf");
        if (GFF_TPI.equals("BMP"))
           { data.getResponse().setContentType("image/bmp"); }
        if (GFF_TPI.equals("GIF"))
           { data.getResponse().setContentType("image/gif"); }
        if (GFF_TPI.equals("JPG"))
           { data.getResponse().setContentType("image/jpeg"); }
        if (GFF_TPI.equals("RTF"))
         //{ data.getResponse().setContentType("text/richtext"); }
           { data.getResponse().setContentType("application/msword"); }
        if (GFF_TPI.equals("DOC"))
           { data.getResponse().setContentType("application/msword"); }
        if (GFF_TPI.equals("PPT"))
           { data.getResponse().setContentType("application/PowerPoint"); }
        if (GFF_TPI.equals("XLS"))
           { data.getResponse().setContentType("application/excel"); }
        if (GFF_TPI.equals("PDF"))
           { data.getResponse().setContentType("application/pdf"); }
data.getResponse().setHeader("Expires", "Tue, 04 Dec 1993 21:29:02 GMT");
        DataOutputStream out = new DataOutputStream(data.getResponse().getOutputStream());
//    out.println("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=ISO-8859-1\">");
//out.writeChars("<META HTTP-EQUIV='Expires' CONTENT='0'>");
        out.write(dumpBlob(GFF_GIF));
        out.close();
        GFF_GIF.close();
        conn.close();
      }
    catch (SQLException  e)
      { Log.debug("JDBCConnection.connect: [" + e.toString() + "]"); }
  }
  //---------------------------------------------------------------------------------------
  static byte[] dumpBlob (BLOB blob)
    throws Exception
  {
Log.debug("PRUEBA.dumpBlob:[" + "Entrada" + "]");
    InputStream instream = blob.getBinaryStream();
  //int lenBuf = blob.getChunkSize();
    int lenBuf = blob.getBufferSize();
Log.debug("PRUEBA.dumpBlob.lenBuf:[" + lenBuf + "]");
  //byte[] buffer = new byte[lenBuf];
    byte[] buffer = new byte[1048576];
    int leng = 0;
    int leng1 = 0;
//    leng = instream.read(buffer);
    while ((leng = instream.read(buffer)) != -1)
          {
Log.debug("PRUEBA.dumpBlob.leng:[" + leng + "]");
            leng1 = leng;
          }
    instream.close();
    byte[] buffers = new byte[leng1];
    for (int j=0; j<leng1; j++)
        {
          buffers[j] = buffer[j];
        }
Log.debug("PRUEBA.dumpBlob.buffers.length:[" + buffers.length + "]");
Log.debug("PRUEBA.dumpBlob:[" + "Salida" + "]");
    return buffers;
  }
  //---------------------------------------------------------------------------------------
}