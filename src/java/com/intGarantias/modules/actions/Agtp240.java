// Source File Name:   Agtp240.java
// Descripcion     :   Anula Ultima Transaccion (ED135, ED500, ED535, GT240)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.BF_IMG;
import com.FHTServlet.modules.global.BF_EVT;
import com.FHTServlet.modules.global.PRMPC080;

import com.intGlobal.modules.global.MSGED100;
import com.intGlobal.modules.global.MSGED135;
import com.intGlobal.modules.global.PRMED210;
import com.intGlobal.modules.global.PRMCL090;

import com.intGarantias.modules.global.MSGGT240;
import com.intGarantias.modules.global.GT_IMG;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp240 extends FHTServletAction
{
  //---------------------------------------------------------------------------------------
  public int i = 0;
  public String lcData = "";
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  BF_EVT.Buf_Evt g_Evt             = new BF_EVT.Buf_Evt();
  BF_IMG img                       = new BF_IMG();
  BF_IMG.Buf_Img g_Img             = new BF_IMG.Buf_Img();
  BF_IMG.Buf_Img_Bse g_Img_Base    = new BF_IMG.Buf_Img_Bse();
  GT_IMG gtimg                     = new GT_IMG();
  GT_IMG.Buf_Img_GtAnu g_Img_GtAnu = new GT_IMG.Buf_Img_GtAnu();
  PRMPC080.PrmPC080 g_PrmPC080     = new PRMPC080.PrmPC080();
  MSGED100.Buf_MsgED100 g_MsgED100 = new MSGED100.Buf_MsgED100();
  MSGED135.Buf_MsgED135 g_MsgED135 = new MSGED135.Buf_MsgED135();
  PRMED210.Buf_PrmED210 g_PrmED210 = new PRMED210.Buf_PrmED210();
  PRMCL090.Buf_PrmCL090 g_PrmCL090 = new PRMCL090.Buf_PrmCL090();
  MSGGT240.Buf_MsgGT240 g_MsgGT240 = new MSGGT240.Buf_MsgGT240();
  //---------------------------------------------------------------------------------------
  public Agtp240()
  {
  }
  //---------------------------------------------------------------------------------------
  public void doPerform(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP240[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp240-DIN", data);
    if (g_Msg.Msg_Pgr.toString().equals("RT"))
       { doAgtp240_Continue(data, context); }
    else
       { doAgtp240_Init(data, context); }
  }
  //---------------------------------------------------------------------------------------
  public void doCancel(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP240[doCancel.start]", "[CANCELADO]");
    setTemplate(data, "Garantias,Agt,AGTP240.vm");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp240_Init(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP240[doAgtp240_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (g_PrmPC080.PC080_Cnr.Ncn.toString().trim().equals("")
     && g_PrmPC080.PC080_Cli.toString().trim().equals(""))
       {
         data.getUser().removeTemp("Agtp240_g_PrmPC080");
         data.getUser().setTemp("Agtp240_g_PrmPC080" ,g_PrmPC080);
         g_PrmCL090 = PRMCL090.Inicia_PrmCL090();
         rg.MoverA(g_PrmCL090.CL090_Pcc, "N");
         rg.MoverA(g_Msg.Msg_Dat, PRMCL090.LSet_De_PrmCL090(g_PrmCL090));
         BF_MSG.Link_Program("Agtp240", "Eclp090", data, g_Msg);
         return;
       }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    g_MsgED135 = MSGED135.Inicia_MsgED135();
    if (g_PrmPC080.PC080_Ntr.toString().trim().compareTo("")!=0)
       {
         Recupera_Evt(data, context);
         Carga_de_Host(data, context);
       }
    else
       {
         if (Carga_Inicial(data, context) == 1)
            { return; }
         Init_Evt_GRT(data, context);
       }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    data.getUser().removeTemp("Agtp240_MsgED135");
    data.getUser().setTemp("Agtp240_MsgED135", g_MsgED135);
    data.getUser().removeTemp("Agtp240_Evt");
    data.getUser().setTemp("Agtp240_Evt", g_Evt);
    data.getUser().removeTemp("Agtp240_ImgBase");
    data.getUser().setTemp("Agtp240_ImgBase", g_Img_Base);
    data.getUser().removeTemp("Agtp240_ImgGtAnu");
    data.getUser().setTemp("Agtp240_ImgGtAnu", g_Img_GtAnu);
    rg.MoverA(g_PrmPC080.PC080_Cli, g_Evt.Evt_Cli);
    rg.MoverA(g_PrmPC080.PC080_Ncl, g_Evt.Evt_Ncl);
    rg.MoverA(g_PrmPC080.PC080_Dir, g_Evt.Evt_Dir);
    rg.MoverA(g_PrmPC080.PC080_Tfc, g_Evt.Evt_Tfc);
    rg.MoverA(g_PrmPC080.PC080_Dcn, g_Evt.Evt_Dcn);
    rg.MoverA(g_PrmPC080.PC080_Ndc, g_Evt.Evt_Ndc);
    rg.MoverA(g_PrmPC080.PC080_Ntt, g_Evt.Evt_Ntt);
    data.getUser().removeTemp("Agtp240_PrmPC080");
    data.getUser().setTemp("Agtp240_PrmPC080", g_PrmPC080);
    rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
    BF_MSG.Param_Program(data, g_Msg);
    setTemplate(data, "Garantias,Agt,AGTP240.vm" );
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp240(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP240[doAgtp240.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp240-MAN", data);
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    String Opc = data.getParameters().getString("Opc", "");
    if (Opc.trim().equals("S"))
       {
         //Boton Salir
         Limpieza(data, context);
         rg.MoverA(g_PrmPC080.PC080_Rtn, "NK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp240", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("R"))
       {
         //Boton Errores
         g_MsgED135 = (MSGED135.Buf_MsgED135)data.getUser().getTemp("Agtp240_MsgED135");
         g_PrmED210 = PRMED210.Inicia_PrmED210();
         rg.MoverA(g_PrmED210.ED210_Rtn, "RO");
         rg.MoverA(g_PrmED210.ED210_Ldt, g_MsgED135.ED135_Ldv);
         data.getUser().removeTemp("Agtp240_g_PrmPC080");
         data.getUser().setTemp("Agtp240_g_PrmPC080" ,g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMED210.LSet_De_PrmED210(g_PrmED210));
         BF_MSG.Link_Program("Agtp240", "Aedp210", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("A"))
       {
         //Boton Aceptar
         g_Img_GtAnu = (GT_IMG.Buf_Img_GtAnu)data.getUser().getTemp("Agtp240_ImgGtAnu");
         Valida_Host("V", data, context);
         Vector g_Tab_Img = A_Tab_Img(data);
         img.Graba_Img_Host(g_Msg, g_Tab_Img, data, context);
         Cursatura("U", g_Tab_Img, data, context);
         g_MsgED135 = MSGED135.Inicia_MsgED135();
         rg.MoverA(g_MsgED135.ED135_Idr, "M");
         rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
         rg.MoverA(g_MsgED135.ED135_Trt, "UNICO");
         rg.MoverA(g_MsgED135.ED135_Est, "");
         rg.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
         rg.MoverA(g_MsgED135.ED135_Ezt, "ENCUR");
         rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
         g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
         Limpieza(data, context);
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp240", data, g_Msg);
         return;
       }
  }
  //---------------------------------------------------------------------------------------
  public void Recupera_Evt(RunData data, Context context)
              throws Exception
  {
    g_MsgED100 = MSGED100.Inicia_MsgED100();
    rg.MoverA(g_MsgED100.ED100_Idr, "Q");
    rg.MoverA(g_MsgED100.ED100_Nev, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_Msg.Msg_Dat, MSGED100.LSet_De_MsgED100(g_MsgED100));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED500");
    g_MsgED100 = MSGED100.LSet_A_MsgED100(g_Msg.Msg_Dat.toString());
    g_Evt = BF_EVT.LSet_A_Evt(g_MsgED100.ED100_Evt.toString());
  }
  //---------------------------------------------------------------------------------------
  public void Carga_de_Host(RunData data, Context context)
              throws Exception
  {
    Vector g_Tab_Img = img.Carga_Img_Host(g_Msg, g_PrmPC080.PC080_Ntr.toString(), data, context);
    De_Tab_Img(g_Tab_Img, data);
    rg.MoverA(g_MsgED135.ED135_Idr, "C");
    rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_MsgED135.ED135_Trt, "UNICO");
    rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
    g_MsgED135 = MSGED135.LSet_A_MsgED135(g_Msg.Msg_Dat.toString());
  }
  //---------------------------------------------------------------------------------------
  public void De_Tab_Img(Vector g_Tab_Img, RunData data)
  {
    g_Img       = img.Inicia_Img();
    lcData      = img.LSet_De_Img(g_Img);
    g_Img_Base  = img.LSet_A_ImgBase(lcData);
    g_Img_GtAnu = gtimg.LSet_A_ImgGtAnu(g_Img.Img_Dat.toString());
    for (i=0; i<g_Tab_Img.size(); i++)
        {
          lcData = img.LSet_De_Img((BF_IMG.Buf_Img)g_Tab_Img.elementAt(i));
          g_Img  = img.LSet_A_Img(lcData);
          if (g_Img.Img_Dax.toString().trim().equals("BASE"))
             {
               g_Img_GtAnu = gtimg.LSet_A_ImgGtAnu(g_Img.Img_Dat.toString());
               g_Img_Base  = img.LSet_A_ImgBase(g_Img_GtAnu.GtAnu_Bse.toString());
             }
        }
  }
  //---------------------------------------------------------------------------------------
  public int Carga_Inicial(RunData data, Context context)
             throws Exception
  {
    g_Img       = img.Inicia_Img();
    lcData      = img.LSet_De_Img(g_Img);
    g_Img_Base  = img.LSet_A_ImgBase(lcData);
    g_Img_GtAnu = gtimg.LSet_A_ImgGtAnu(g_Img.Img_Dat.toString());
    rg.MoverA(g_Img_Base.Img_Base_Ttr, "ANULA");
    rg.MoverA(g_Img_Base.Img_Base_Sis, g_PrmPC080.PC080_Cnr.Sis);
    rg.MoverA(g_Img_Base.Img_Base_Ncn, g_PrmPC080.PC080_Cnr.Ncn);
    if (Valida_Host("N", data, context) == 1)
       { return 1; }
    rg.MoverA(g_PrmPC080.PC080_Ntr, g_Img_Base.Img_Base_Ntr);
    return 0;
  }
  //---------------------------------------------------------------------------------------
  public int Valida_Host(String pcIdr, RunData data, Context context)
             throws Exception
  {
    if (pcIdr.equals("N"))
       { rg.MoverA(g_Img_GtAnu.GtAnu_Bse, img.LSet_De_ImgBase_Bse(g_Img_Base)); }
    rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtAnu(g_Img_GtAnu));
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    g_MsgGT240 = MSGGT240.Inicia_MsgGT240();
    rg.MoverA(g_MsgGT240.GT240_Idr, pcIdr);
    rg.MoverA(g_MsgGT240.GT240_Img, g_Img.Img_Dat);
    rg.MoverA(g_Msg.Msg_Dat, MSGGT240.LSet_De_MsgGT240(g_MsgGT240));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GT240");
    g_MsgGT240 = MSGGT240.LSet_A_MsgGT240(g_Msg.Msg_Dat.toString());
    if (g_MsgGT240.GT240_Idr.toString().equals("R"))
       {
         String lcMensaje = "NO SE";
         if (g_MsgGT240.GT240_Swt_Dcn.toString().equals("1")){lcMensaje = "Garantia Erronea (GTI-DCN No Existe)";}
         if (g_MsgGT240.GT240_Swt_Gti.toString().equals("1")){lcMensaje = "Numero Garantia No Existe";}
         if (g_MsgGT240.GT240_Swt_Gti.toString().equals("2")){lcMensaje = "Garantia NO Esta Vigente";}
         if (g_MsgGT240.GT240_Swt_Gti.toString().equals("3")){lcMensaje = "Evento No Valido para Garantia";}
         if (g_MsgGT240.GT240_Swt_Gti.toString().equals("5")){lcMensaje = "Transaccion NO Anulable (STOCK)";}
         if (g_MsgGT240.GT240_Swt_Gti.toString().equals("7")){lcMensaje = "Garantia Erronea (GTI-TTR No Existe)";}
         if (g_MsgGT240.GT240_Swt_Evt.toString().equals("1")){lcMensaje = "Garantia Tiene Evento en Tramite";}
         if (g_MsgGT240.GT240_Swt_Cli.toString().equals("1")){lcMensaje = "Garantia Erronea (Sin CLI-GTI)";}
         if (g_MsgGT240.GT240_Swt_Cli.toString().equals("2")){lcMensaje = "Garantia Erronea (Sin CLI)";}
         msg.MsgInt("Agtp240", data, g_Msg, lcMensaje, "1");
         return 1;
       }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    rg.MoverA(g_Img.Img_Dat, g_MsgGT240.GT240_Img);
    g_Img_GtAnu = gtimg.LSet_A_ImgGtAnu(g_Img.Img_Dat.toString());
    if (pcIdr.equals("N"))
       {
         g_Img_Base  = img.LSet_A_ImgBase(g_Img_GtAnu.GtAnu_Bse.toString());
         rg.MoverA(g_Img_Base.Img_Base_Suc, g_Msg.Msg_Suc);
         rg.MoverA(g_Img_Base.Img_Base_Nsu, g_Msg.Msg_Nsu);
         rg.MoverA(g_Img_Base.Img_Base_Eje, g_Msg.Msg_Cus);
         rg.MoverA(g_Img_Base.Img_Base_Nej, g_Msg.Msg_Nus);
         rg.MoverA(g_Img_Base.Img_Base_Tej, g_Msg.Msg_Fus);
         rg.MoverA(g_Img_GtAnu.GtAnu_Bse,   img.LSet_De_ImgBase_Bse(g_Img_Base));
       }
    return 0;
  }
  //---------------------------------------------------------------------------------------
  public void Init_Evt_GRT(RunData data, Context context)
              throws Exception
  {
    rg.MoverA(g_Evt.Evt_Ttr, g_Img_Base.Img_Base_Ttr);
    rg.MoverA(g_Evt.Evt_Ntt, g_Img_Base.Img_Base_Ntt);
    rg.MoverA(g_Evt.Evt_Tsl, g_Img_Base.Img_Base_Tsl);
    rg.MoverA(g_Evt.Evt_Slc, g_Img_Base.Img_Base_Slc);
    rg.MoverA(g_Evt.Evt_Cli, g_Img_Base.Img_Base_Cli);
    rg.MoverA(g_Evt.Evt_Ncl, g_Img_Base.Img_Base_Ncl);
    rg.MoverA(g_Evt.Evt_Tpr, g_Img_Base.Img_Base_Tpr);
    rg.MoverA(g_Evt.Evt_Dir, g_Img_Base.Img_Base_Dir);
    rg.MoverA(g_Evt.Evt_Cmn, rg.Zeros(g_Evt.Evt_Cmn));      //g_Img_Base.Img_Base_Cmn
    rg.MoverA(g_Evt.Evt_Tfc, g_Img_Base.Img_Base_Tfc);
    rg.MoverA(g_Evt.Evt_Sis, g_Img_Base.Img_Base_Sis);
    rg.MoverA(g_Evt.Evt_Ncn, g_Img_Base.Img_Base_Ncn);
    rg.MoverA(g_Evt.Evt_Dcn, g_Img_Base.Img_Base_Dcn);
    rg.MoverA(g_Evt.Evt_Ndc, g_Img_Base.Img_Base_Ndc);
    rg.MoverA(g_Evt.Evt_Tmn, g_Img_Base.Img_Base_Tmn);
    rg.MoverA(g_Evt.Evt_Trj, g_Img_Base.Img_Base_Trj);
    rg.MoverA(g_Evt.Evt_Dcm, g_Img_Base.Img_Base_Dcm);
    rg.MoverA(g_Evt.Evt_Prp, g_Img_Base.Img_Base_Prp);
    rg.MoverA(g_Evt.Evt_Suc, g_Img_Base.Img_Base_Suc);
    rg.MoverA(g_Evt.Evt_Nsu, g_Img_Base.Img_Base_Nsu);
    rg.MoverA(g_Evt.Evt_Mnd, g_Img_Base.Img_Base_Mnd);
    rg.MoverA(g_Evt.Evt_Nmn, g_Img_Base.Img_Base_Nmn);
    rg.MoverA(g_Evt.Evt_Eje, g_Img_Base.Img_Base_Eje);
    rg.MoverA(g_Evt.Evt_Nej, g_Img_Base.Img_Base_Nej);
    rg.MoverA(g_Evt.Evt_Tej, g_Img_Base.Img_Base_Tej);
    rg.MoverA(g_Evt.Evt_Est, "NUEVA");
    rg.MoverA(g_Evt.Evt_Fst, g_Msg.Msg_Fch);
    rg.MoverA(g_Evt.Evt_Upr, g_Img_Base.Img_Base_Trj);
    rg.MoverA(g_Evt.Evt_Vpr, rg.Zeros(g_Evt.Evt_Vpr));
    rg.MoverA(g_Evt.Evt_Fpr, g_Msg.Msg_Fch);
    rg.MoverA(g_Evt.Evt_Spr, rg.Zeros(g_Evt.Evt_Spr));
    rg.MoverA(g_Evt.Evt_Tpp, " ");
    rg.MoverA(g_Evt.Evt_Ggr, g_Img_GtAnu.GtAnu_Ggr);
    rg.MoverA(g_Evt.Evt_Fll, " ");
  }
  //---------------------------------------------------------------------------------------
  public Vector A_Tab_Img(RunData data)
  {
    Vector v_Img = new Vector();
    v_Img.clear();
    rg.MoverA(g_Img.Img_Ntr, g_PrmPC080.PC080_Ntr);
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    rg.MoverA(g_Img.Img_Dax, "BASE");
    rg.MoverA(g_Img.Img_Seq, "000");
    rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtAnu(g_Img_GtAnu));
    Vector vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
    v_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    return v_Img;
  }
  //---------------------------------------------------------------------------------------
  public void Cursatura(String pcIdr, Vector g_Tab_Img, RunData data, Context context)
              throws Exception
  {
    g_Evt = (BF_EVT.Buf_Evt)data.getUser().getTemp("Agtp240_Evt");
    rg.MoverA(g_Evt.Evt_Est, "ENTMT");
    rg.MoverA(g_Evt.Evt_Fpr, g_Msg.Msg_Fch);
    BF_EVT t = new BF_EVT();
    g_MsgED100 = MSGED100.Inicia_MsgED100();
    rg.MoverA(g_MsgED100.ED100_Idr, pcIdr);
    rg.MoverA(g_MsgED100.ED100_Nev, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_MsgED100.ED100_Evt, t.LSet_De_Evt(g_Evt));
    rg.MoverA(g_Msg.Msg_Dat, MSGED100.LSet_De_MsgED100(g_MsgED100));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED500");
  }
//---------------------------------------------------------------------------------------
  public void doAgtp240_Continue(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP240[doAgtp240_Continue.start]", "[" + data.getUser().getUserName() + "]");
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Eclp090"))
       {
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp240_g_PrmPC080");
         data.getUser().removeTemp("Agtp240_g_PrmPC080");
         g_PrmCL090 = PRMCL090.LSet_A_PrmCL090(g_Msg.Msg_Dat.toString());
         if (g_PrmCL090.CL090_Cli.toString().trim().equals(""))
            {
              Limpieza(data, context);
              rg.MoverA(g_PrmPC080.PC080_Rtn, "NK");
              rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
              BF_MSG.Return_Data("Agtp240", data, g_Msg);
              return;
            }
         rg.MoverA(g_PrmPC080.PC080_Cli, g_PrmCL090.CL090_Cli);
         rg.MoverA(g_PrmPC080.PC080_Ncl, g_PrmCL090.CL090_Ncl);
         rg.MoverA(g_PrmPC080.PC080_Tpr, g_PrmCL090.CL090_Tpr);
         rg.MoverA(g_PrmPC080.PC080_Acc, "SELECCIONA");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Link_Program("Agtp240", "Agtp400", data, g_Msg);
         return;
       }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Agtp400"))
       {
         g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
         if (g_PrmPC080.PC080_Rtn.toString().trim().equals("NK"))
            {
              Limpieza(data, context);
              rg.MoverA(g_PrmPC080.PC080_Rtn, "NK");
              rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
              BF_MSG.Return_Data("Agtp240", data, g_Msg);
              return;
            }
         rg.MoverA(g_PrmPC080.PC080_Acc, "ANULA");
         rg.MoverA(g_PrmPC080.PC080_Ttr, "ANULA");
         rg.MoverA(g_PrmPC080.PC080_Ntt, "ANULA ULT.TTR. GARANTIA");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         doAgtp240_Init(data, context);
         return;
       }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Aedp210"))
       {
         g_PrmED210 = PRMED210.LSet_A_PrmED210(g_Msg.Msg_Dat.toString());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp240_g_PrmPC080");
         data.getUser().removeTemp("Agtp240_g_PrmPC080");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP240.vm" );
         return;
       }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Appp030"))
       {
         Limpieza(data, context);
         rg.MoverA(g_PrmPC080.PC080_Rtn, "NK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp240", data, g_Msg);
         return;
       }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  }
  //---------------------------------------------------------------------------------------
  public void Limpieza(RunData data, Context context)
              throws Exception 
  {
    data.getUser().removeTemp("Agtp240_PrmPC080");
    data.getUser().removeTemp("Agtp240_MsgED135");
    data.getUser().removeTemp("Agtp240_Evt");
    data.getUser().removeTemp("Agtp240_ImgBase");
    data.getUser().removeTemp("Agtp240_ImgGtAnu");
  }
  //---------------------------------------------------------------------------------------
}