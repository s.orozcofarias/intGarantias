package com.intGarantias.modules.actions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.turbine.services.resources.TurbineResources;
import org.apache.turbine.util.Log;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.intGarantias.bean.GtiasTasablesEnTramiteBean;
import com.intGarantias.bean.ResGtiasTasadorBean;
import com.intGarantias.dao.ConsultasBaseDatos;
import com.intGarantias.util.CrearExcel;
import com.intGarantias.util.DescargaDoc;


public class ResGtiasTasador extends FHTServletAction {
	

	
	public ResGtiasTasador() {}

	
	public void doPerform(RunData data, Context context) throws Exception {
		Log.debug("ResGtiasTasador.doPerform");
		
		ConsultasBaseDatos consulta = new ConsultasBaseDatos();
		DescargaDoc descargaDoc = new DescargaDoc(); 
		
		 List listDatGar = consulta.consultaResumenTasador();
		 
		 if(listDatGar.size() == 0){
			 Log.debug("No se encontraron datos");
			 return;
		 }
		 
		 Log.debug("Cantidad de datos : " + listDatGar.size());
		 
		 try{
			 File temp = this.generarExcel(listDatGar);
			 descargaDoc.descargarArchivo(data, temp);
			 
		 }catch(IOException e){
			 Log.error("No se pudo completar la descarga ", e);			 
		 }catch(Exception ex){
			 Log.error("No se pudo completar la descarga ", ex);
		 }
		 
		 setTemplate(data, "Garantias,Agt,Agtp800.vm" );
	}
	

	public File generarExcel(List garantiasData) throws FileNotFoundException, IOException{
		Log.debug("ResGtiasTasador.generarExcel()");
		CrearExcel creaExcel = new CrearExcel();

			
		Log.debug("prop : " + TurbineResources.getString("pathPlantillas"));
		Log.debug("prop : " + TurbineResources.getString("plantillaTasasiones"));
		
		String pathPlantilla = 	TurbineResources.getString("pathPlantillas");
		String nombrePlantilla = TurbineResources.getString("plantillaTasasiones");
		Log.debug("path absoluto " + pathPlantilla + nombrePlantilla);
		
		File file = new File(pathPlantilla + nombrePlantilla);
		
//		POIFSFileSystem excel = new POIFSFileSystem(this.getClass().getClassLoader().getResourceAsStream("recursos/PLANTILLA_TASACIONES_TASADOR.xls"));
		POIFSFileSystem excel = new POIFSFileSystem(new FileInputStream(file));
		HSSFWorkbook wb = new HSSFWorkbook(excel);
		HSSFSheet HojaExcelDetalle = wb.getSheetAt(0);
		HSSFRow fila = null;
		HSSFCell cell = null;
    	 
		 
		 for(int i = 0 ; i < garantiasData.size() ; i++){
			 
			 try{
	  
			 fila = creaExcel.createRowFrom(HojaExcelDetalle, HojaExcelDetalle.getRow(1), i+1);
			 
			 cell = fila.getCell((short)0);
			 cell.setCellValue(((ResGtiasTasadorBean)garantiasData.get(i)).getNomTasador());

			 cell = fila.getCell((short)1);
			 cell.setCellValue(((ResGtiasTasadorBean)garantiasData.get(i)).getCodTasador());

			 cell = fila.getCell((short)2);
			 cell.setCellValue(((ResGtiasTasadorBean)garantiasData.get(i)).getCantTasaciones());

			 cell = fila.getCell((short)3);
			 cell.setCellValue(((ResGtiasTasadorBean)garantiasData.get(i)).getPorcTasaciones());

			 cell = fila.getCell((short)4);
			 cell.setCellValue(((ResGtiasTasadorBean)garantiasData.get(i)).getAgricola());
			 
			 cell = fila.getCell((short)5);
			 cell.setCellValue(((ResGtiasTasadorBean)garantiasData.get(i)).getPorAceptar());

			 cell = fila.getCell((short)6);
			 cell.setCellValue(((ResGtiasTasadorBean)garantiasData.get(i)).getRegistroTas());
			 
			 cell = fila.getCell((short)7);
			 cell.setCellValue(((ResGtiasTasadorBean)garantiasData.get(i)).getPendienteTas());
			 
			 cell = fila.getCell((short)8);
			 cell.setCellValue(((ResGtiasTasadorBean)garantiasData.get(i)).getReenvioTas());
			 
			 }catch(NullPointerException ex){
				 Log.debug("Falla creacion de excel " + ((GtiasTasablesEnTramiteBean) garantiasData.get(i)).getNumGarantia(), ex);
			 }catch(Exception e){
				 Log.error("No se pudo completar la descarga ResGtiasTasador", e);
			 }
		 }
		 
		 File temp = File.createTempFile("Informe_Tasaciones_por_Tasador_", ".xls");
		 temp.deleteOnExit();
		 
		 FileOutputStream fileOut = new FileOutputStream(temp);
		  wb.write(fileOut);	      
	      fileOut.close();
	      return temp;
	}
	
	
	

}


	

		