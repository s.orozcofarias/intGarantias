// Source File Name:   Agtp176.java
// Descripcion     :   Consulta/Seleccion Colocaciones (GT176)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.PRMPC080;

import com.intGarantias.modules.global.MSGGT176;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp176 extends FHTServletAction
{
  //---------------------------------------------------------------------------------------
  public int    i = 0;
  public String lcRetorno = "";
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  PRMPC080.PrmPC080 g_PrmPC080     = new PRMPC080.PrmPC080();
  MSGGT176.Buf_MsgGT176 g_MsgGT176 = new MSGGT176.Buf_MsgGT176();
  //---------------------------------------------------------------------------------------
  public Agtp176()
  {
  }
  //---------------------------------------------------------------------------------------
  public void doPerform(RunData data, Context context)
              throws Exception
  {
  //Log.debug("AGTP176[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp176-DIN", data);
    doAgtp176_Init(data, context);
  }
  //---------------------------------------------------------------------------------------
  public void doCancel(RunData data, Context context)
              throws Exception
  {
  //Log.debug("AGTP176[doCancel.start]", "[CANCELADO]");
    setTemplate(data, "Garantias,Agt,AGTP176.vm");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp176_Init(RunData data, Context context)
              throws Exception
  {
  //Log.debug("AGTP176[doAgtp176_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    g_MsgGT176 = MSGGT176.Inicia_MsgGT176();
    rg.MoverA(g_MsgGT176.GT176_Idr, "I");
    rg.MoverA(g_MsgGT176.GT176_Gti, g_PrmPC080.PC080_Cnr.Sis.toString() + g_PrmPC080.PC080_Cnr.Ncn.toString());
    Poblar_Panel(data, context, "I");
    setTemplate(data, "Garantias,Agt,AGTP176.vm" );
  //Log.debug("AGTP176[doAgtp176_Init.end]", "[" + data.getUser().getUserName() + "]");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp176(RunData data, Context context)
              throws Exception
  {
  //Log.debug("AGTP176[doAgtp176.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp176-MAN", data);
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    String Opc = data.getParameters().getString("Opc");
    if  (Opc.equals("S"))
       //Salir
        {
          rg.MoverA(g_PrmPC080.PC080_Rtn, "NK");
          rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
          data.getUser().removeTemp("Agtp176_Msg");
          data.getUser().removeTemp("Agtp176_MsgGT176");
          BF_MSG.Return_Data("Agtp176", data, g_Msg);
          return;
        }
    g_MsgGT176 = (MSGGT176.Buf_MsgGT176)data.getUser().getTemp("Agtp176_MsgGT176");
    MSGGT176.Bff_MsgGT176 d_MsgGT176 = new MSGGT176.Bff_MsgGT176();
    if  (Opc.equals("A"))
       //Selecciona
        {
          String Seq = data.getParameters().getString("Seq", "");
          i = Integer.parseInt(Seq);
          d_MsgGT176 = (MSGGT176.Bff_MsgGT176)g_MsgGT176.GT176_Tab.elementAt(i);
          //rg.MoverA(g_PrmPC080.PC080_Cnr.Sis, d_MsgGT176.Mod_Cre.substring(0,3));
          //rg.MoverA(g_PrmPC080.PC080_Cnr.Ncn, d_MsgGT176.Mod_Cre.substring(3));
          rg.MoverA(g_PrmPC080.PC080_Tgr.Nug, d_MsgGT176.Mod_Cre);
          rg.MoverA(g_PrmPC080.PC080_Tgr.Rcg, d_MsgGT176.Mod_Rel);
          rg.MoverA(g_PrmPC080.PC080_Rtn,     "OK");
          rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
          data.getUser().removeTemp("Agtp176_Msg");
          data.getUser().removeTemp("Agtp176_MsgGT176");
          BF_MSG.Return_Data("Agtp176", data, g_Msg);
          return;
        }
    if  (Opc.equals("N"))
       //Siguiente
        {	
          Poblar_Panel(data, context, "S");
          setTemplate(data, "garantias,Agt,AGTP176.vm" );
          return;
        }
    if  (Opc.equals("P"))
       //Previo
        {	
          Poblar_Panel(data, context, "P");
          setTemplate(data, "garantias,Agt,AGTP176.vm" );
          return;
        }		
  }
  //---------------------------------------------------------------------------------------
  public void Poblar_Panel(RunData data, Context context, String pcIdr)
              throws Exception
  {
    MSGGT176.Bff_MsgGT176 d_MsgGT176 = new MSGGT176.Bff_MsgGT176();
    if (pcIdr.equals("E"))
       {
         g_MsgGT176 = (MSGGT176.Buf_MsgGT176)data.getUser().getTemp("Agtp176_MsgGT176");
         i = 0;
       }
    if (pcIdr.equals("S"))
       { i = Integer.parseInt(g_MsgGT176.GT176_Nro.toString())-1; }
    if (pcIdr.equals("P"))
       { i = 0; }
    if (pcIdr.compareTo("I")!=0)
       {
         String lcGti = g_MsgGT176.GT176_Gti.toString();
         d_MsgGT176 = (MSGGT176.Bff_MsgGT176)g_MsgGT176.GT176_Tab.elementAt(i);
         g_MsgGT176 = MSGGT176.Inicia_MsgGT176();
         rg.MoverA(g_MsgGT176.GT176_Idr, pcIdr);
         rg.MoverA(g_MsgGT176.GT176_Gti, lcGti);
         g_MsgGT176.GT176_Tab.set(0, d_MsgGT176);
       }
    rg.MoverA(g_Msg.Msg_Dat, MSGGT176.LSet_De_MsgGT176(g_MsgGT176));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GT176");
    g_MsgGT176 = MSGGT176.LSet_A_MsgGT176(g_Msg.Msg_Dat.toString());
    data.getUser().removeTemp("Agtp176_MsgGT176");
    data.getUser().setTemp("Agtp176_MsgGT176", g_MsgGT176);
    data.getUser().removeTemp("Agtp176_Msg");
    data.getUser().setTemp("Agtp176_Msg", g_Msg);
  }
//---------------------------------------------------------------------------------------
}