// Source File Name:   Agtp150.java
// Descripcion     :   Seguros Constitucion (ED135, ED500, ED535, GT625)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.BF_IMG;
import com.FHTServlet.modules.global.PRMPC080;

import com.intGlobal.modules.global.MSGED125;
import com.intGlobal.modules.global.MSGED135;
import com.intGlobal.modules.global.PRMED210;
import com.intGlobal.modules.global.PRMTG114;

import com.intGarantias.modules.global.GT_IMG;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp150 extends FHTServletAction
{
  //---------------------------------------------------------------------------------------
  public int i = 0;
  public String gcSeg = "";
  public String lcData = "";
  public Vector vDatos;
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  BF_IMG img                       = new BF_IMG();
  BF_IMG.Buf_Img g_Img             = new BF_IMG.Buf_Img();
  BF_IMG.Buf_Img_Bse g_Img_Base    = new BF_IMG.Buf_Img_Bse();
  GT_IMG gtimg                     = new GT_IMG();
  GT_IMG.Buf_Img_GtApe g_Img_GtApe = new GT_IMG.Buf_Img_GtApe();
  GT_IMG.Buf_Img_GtHyp g_Img_GtHyp = new GT_IMG.Buf_Img_GtHyp();
  Vector v_Img_GtHyp               = new Vector();
  PRMPC080.PrmPC080 g_PrmPC080     = new PRMPC080.PrmPC080();
  MSGED125.Buf_MsgED125 g_MsgED125 = new MSGED125.Buf_MsgED125();
  MSGED135.Buf_MsgED135 g_MsgED135 = new MSGED135.Buf_MsgED135();
  PRMED210.Buf_PrmED210 g_PrmED210 = new PRMED210.Buf_PrmED210();
  PRMTG114.Buf_PrmTG114 g_PrmTG114 = new PRMTG114.Buf_PrmTG114();
  //---------------------------------------------------------------------------------------
  public Agtp150()
  {
  }
  //---------------------------------------------------------------------------------------
  public void doPerform(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP150[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp150-DIN", data);
    if (g_Msg.Msg_Pgr.toString().equals("RT"))
       { doAgtp150_Continue(data, context); }
    else
       { doAgtp150_Init(data, context); }
  }
  //---------------------------------------------------------------------------------------
  public void doCancel(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP150[doCancel.start]", "[CANCELADO]");
    setTemplate(data, "Garantias,Agt,AGTP150.vm");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp150_Init(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP150[doAgtp150_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    data.getUser().removeTemp("Agtp150_PrmPC080");
    data.getUser().setTemp("Agtp150_PrmPC080", g_PrmPC080);
    Carga_de_Host(data, context);
    Carga_Inicial(data, context);
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (g_PrmPC080.PC080_Rtn.toString().trim().equals("WR"))
       {
         if (g_PrmPC080.PC080_Fte.toString().trim().compareTo("EJE")!=0)
            { rg.MoverA(g_PrmPC080.PC080_Rtn, "RO"); }
         else
            { rg.MoverA(g_PrmPC080.PC080_Rtn, "WR"); }
       }
    rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
    BF_MSG.Param_Program(data, g_Msg);
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    setTemplate(data, "Garantias,Agt,AGTP150.vm" );
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp150(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP150[doAgtp150.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp150-MAN", data);
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    String Opc = data.getParameters().getString("Opc", "");
    if (Opc.trim().equals("S"))
       {
         //Boton Salir
         data.getUser().removeTemp("Agtp150_PrmPC080");
         data.getUser().removeTemp("Agtp150_ImgBase");
         data.getUser().removeTemp("Agtp150_ImgGtApe");
         data.getUser().removeTemp("Agtp150_vImgGtHyp");
         data.getUser().removeTemp("Agtp150_vTabImg");
         data.getUser().removeTemp("Agtp150_MsgED125");
         data.getUser().removeTemp("Agtp150_MsgED135");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "NK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp150", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("R"))
       {
         //Boton Errores
         g_MsgED135 = (MSGED135.Buf_MsgED135)data.getUser().getTemp("Agtp150_MsgED135");
         g_PrmED210 = PRMED210.Inicia_PrmED210();
         rg.MoverA(g_PrmED210.ED210_Rtn, "RO");
         rg.MoverA(g_PrmED210.ED210_Ldt, g_MsgED135.ED135_Ldv);
         data.getUser().removeTemp("Agtp150_g_PrmPC080");
         data.getUser().setTemp("Agtp150_g_PrmPC080" ,g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMED210.LSet_De_PrmED210(g_PrmED210));
         BF_MSG.Link_Program("Agtp150", "Aedp210", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("A"))
       {
         //Boton Aceptar Ejecutivo
         v_Img_GtHyp = (Vector)data.getUser().getTemp("Agtp150_vImgGtHyp");
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp150_ImgGtApe");
         rg.MoverA(g_Img_GtApe.GtApe_Psw, " ");
         Cursar(data, context);
         data.getUser().removeTemp("Agtp150_PrmPC080");
         data.getUser().removeTemp("Agtp150_ImgBase");
         data.getUser().removeTemp("Agtp150_ImgGtApe");
         data.getUser().removeTemp("Agtp150_vImgGtHyp");
         data.getUser().removeTemp("Agtp150_vTabImg");
         data.getUser().removeTemp("Agtp150_MsgED125");
         data.getUser().removeTemp("Agtp150_MsgED135");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp150", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("J"))
       {
         //Boton Aceptar JServicios
         g_MsgED135 = MSGED135.Inicia_MsgED135();
         rg.MoverA(g_MsgED135.ED135_Idr, "M");
         rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
         rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
         rg.MoverA(g_MsgED135.ED135_Est, "");
         rg.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
         rg.MoverA(g_MsgED135.ED135_Ezt, "ENNVA");
         rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
         g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
         data.getUser().removeTemp("Agtp150_PrmPC080");
         data.getUser().removeTemp("Agtp150_ImgBase");
         data.getUser().removeTemp("Agtp150_ImgGtApe");
         data.getUser().removeTemp("Agtp150_vImgGtHyp");
         data.getUser().removeTemp("Agtp150_vTabImg");
         data.getUser().removeTemp("Agtp150_MsgED125");
         data.getUser().removeTemp("Agtp150_MsgED135");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp150", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("D"))
       {
         //Boton Devolver JServicios
         g_PrmED210 = PRMED210.Inicia_PrmED210();
         rg.MoverA(g_PrmED210.ED210_Rtn, "RW");
         rg.MoverA(g_PrmPC080.PC080_Amv, "D");
         data.getUser().removeTemp("Agtp150_g_PrmPC080");
         data.getUser().setTemp("Agtp150_g_PrmPC080", g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMED210.LSet_De_PrmED210(g_PrmED210));
         BF_MSG.Link_Program("Agtp150", "Aedp210", data, g_Msg); 
         return;
       }
    if (Opc.trim().equals("P"))
       {
         //Boton Pendiente NOVA
         g_PrmED210 = PRMED210.Inicia_PrmED210();
         rg.MoverA(g_PrmED210.ED210_Rtn, "RW");
         rg.MoverA(g_PrmPC080.PC080_Amv, "P");
         data.getUser().removeTemp("Agtp150_g_PrmPC080");
         data.getUser().setTemp("Agtp150_g_PrmPC080" ,g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMED210.LSet_De_PrmED210(g_PrmED210));
         BF_MSG.Link_Program("Agtp150", "Aedp210", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("V"))
       {
         //Boton Aceptar NOVA
         v_Img_GtHyp = (Vector)data.getUser().getTemp("Agtp150_vImgGtHyp");
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp150_ImgGtApe");
         rg.MoverA(g_Img_GtApe.GtApe_Psw, " ");
         //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
         Vector g_Tab_Img = (Vector)data.getUser().getTemp("Agtp150_vTabImg");
         g_Tab_Img = A_Tab_Img(data, g_Tab_Img);
         img.Graba_Img_Host(g_Msg, g_Tab_Img, data, context);
         g_MsgED135 = MSGED135.Inicia_MsgED135();
         rg.MoverA(g_MsgED135.ED135_Idr, "M");
         rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
         rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
         rg.MoverA(g_MsgED135.ED135_Est, "");
         rg.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
         rg.MoverA(g_MsgED135.ED135_Ezt, "TERMI");
         rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
         g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
         //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
         data.getUser().removeTemp("Agtp150_PrmPC080");
         data.getUser().removeTemp("Agtp150_ImgBase");
         data.getUser().removeTemp("Agtp150_ImgGtApe");
         data.getUser().removeTemp("Agtp150_vImgGtHyp");
         data.getUser().removeTemp("Agtp150_vTabImg");
         data.getUser().removeTemp("Agtp150_MsgED125");
         data.getUser().removeTemp("Agtp150_MsgED135");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp150", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("M"))
       {
         //Boton Modificar
         String Seq = data.getParameters().getString("Seq", "");
         i = Integer.parseInt(Seq);
         v_Img_GtHyp = (Vector)data.getUser().getTemp("Agtp150_vImgGtHyp");
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp150_ImgGtApe");
         g_Img_Base  = img.LSet_A_ImgBase(g_Img_GtApe.GtApe_Bse.toString());
         rg.MoverA(g_Img_GtApe.GtApe_Idx, rg.Suma(Seq,0,"1",0,3,0));
         g_Img_GtHyp = (GT_IMG.Buf_Img_GtHyp)v_Img_GtHyp.elementAt(i);
         data.getUser().removeTemp("Agtp150_ImgGtApe");
         data.getUser().setTemp("Agtp150_ImgGtApe", g_Img_GtApe);
         data.getUser().removeTemp("Agtp150_ImgGtHyp");
         data.getUser().setTemp("Agtp150_ImgGtHyp", g_Img_GtHyp);
         setTemplate(data, "Garantias,Agt,AGTP150.vm" );
         return;
       }
    if (Opc.trim().equals("1"))
       {
         //Boton Aseguradora
         g_Img_GtHyp = (GT_IMG.Buf_Img_GtHyp)data.getUser().getTemp("Agtp150_ImgGtHyp");
         Actualiza_Datos(data, context);
         data.getUser().removeTemp("Agtp150_ImgGtHyp");
         data.getUser().setTemp("Agtp150_ImgGtHyp", g_Img_GtHyp);
         g_PrmTG114 = PRMTG114.Inicia_PrmTG114();
         data.getUser().removeTemp("Agtp150_g_PrmPC080");
         data.getUser().setTemp("Agtp150_g_PrmPC080" ,g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMTG114.LSet_De_PrmTG114(g_PrmTG114));
         BF_MSG.Link_Program("Agtp150", "Atgp114", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("Y"))
       {
         //Boton Si
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp150_ImgGtApe");
         v_Img_GtHyp = (Vector)data.getUser().getTemp("Agtp150_vImgGtHyp");
         g_Img_GtHyp = (GT_IMG.Buf_Img_GtHyp)data.getUser().getTemp("Agtp150_ImgGtHyp");
         Actualiza_Datos(data, context);
         i = Integer.parseInt(g_Img_GtApe.GtApe_Idx.toString()) - 1;
         GT_IMG.Buf_Img_GtHyp l_Img_GtHyp = new GT_IMG.Buf_Img_GtHyp();
         l_Img_GtHyp = (GT_IMG.Buf_Img_GtHyp)v_Img_GtHyp.elementAt(i);
         v_Img_GtHyp.set(i, g_Img_GtHyp); 
         data.getUser().removeTemp("Agtp150_ImgGtHyp");
         rg.MoverA(g_Img_GtApe.GtApe_Idx, " ");
         rg.MoverA(g_Img_GtApe.GtApe_Psw, "Y");
         data.getUser().removeTemp("Agtp150_ImgGtApe");
         data.getUser().setTemp("Agtp150_ImgGtApe", g_Img_GtApe);
         data.getUser().removeTemp("Agtp150_vImgGtHyp");
         data.getUser().setTemp("Agtp150_vImgGtHyp", v_Img_GtHyp);
         setTemplate(data, "Garantias,Agt,AGTP150.vm" );
         return;
       }
    if (Opc.trim().equals("N"))
       {
         //Boton No
         data.getUser().removeTemp("Agtp150_ImgGtHyp");
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp150_ImgGtApe");
         rg.MoverA(g_Img_GtApe.GtApe_Idx, " ");
         data.getUser().removeTemp("Agtp150_ImgGtApe");
         data.getUser().setTemp("Agtp150_ImgGtApe", g_Img_GtApe);
         setTemplate(data, "Garantias,Agt,AGTP150.vm" );
         return;
       }
  }
  //---------------------------------------------------------------------------------------
  public void Carga_de_Host(RunData data, Context context)
              throws Exception 
  {
    Vector g_Tab_Img = img.Carga_Img_Host(g_Msg, g_PrmPC080.PC080_Ntr.toString(), data, context);
    De_Tab_Img(g_Tab_Img, data);
    data.getUser().removeTemp("Agtp150_ImgBase");
    data.getUser().setTemp("Agtp150_ImgBase", g_Img_Base);
    data.getUser().removeTemp("Agtp150_ImgGtApe");
    data.getUser().setTemp("Agtp150_ImgGtApe", g_Img_GtApe);
    data.getUser().removeTemp("Agtp150_vImgGtHyp");
    data.getUser().setTemp("Agtp150_vImgGtHyp", v_Img_GtHyp);
    data.getUser().removeTemp("Agtp150_vTabImg");
    data.getUser().setTemp("Agtp150_vTabImg", g_Tab_Img);
  }
  //---------------------------------------------------------------------------------------
  public void De_Tab_Img(Vector g_Tab_Img, RunData data)
  {
    g_Img       = img.Inicia_Img();
    lcData      = img.LSet_De_Img(g_Img);
    g_Img_Base  = img.LSet_A_ImgBase(lcData);
    g_Img_GtApe = gtimg.LSet_A_ImgGtApe(g_Img.Img_Dat.toString());
    v_Img_GtHyp.clear();
    for (i=0; i<g_Tab_Img.size(); i++)
        {
          lcData = img.LSet_De_Img((BF_IMG.Buf_Img)g_Tab_Img.elementAt(i));
          g_Img  = img.LSet_A_Img(lcData);
          if (g_Img.Img_Dax.toString().trim().equals("BASE"))
             {
               g_Img_GtApe = gtimg.LSet_A_ImgGtApe(g_Img.Img_Dat.toString());
               g_Img_Base  = img.LSet_A_ImgBase(g_Img_GtApe.GtApe_Bse.toString());
             }
          if (g_Img.Img_Dax.toString().trim().equals("XXHIP")
           || g_Img.Img_Dax.toString().trim().equals("YYPRD"))
             {
               g_Img_GtHyp = gtimg.LSet_A_ImgGtHyp(g_Img.Img_Dat.toString());
               v_Img_GtHyp.add (g_Img_GtHyp); 
             }
        }
  }
  //---------------------------------------------------------------------------------------
  public void Carga_Inicial(RunData data, Context context)
              throws Exception 
  {
    g_MsgED125 = MSGED125.Inicia_MsgED125();
    rg.MoverA(g_MsgED125.ED125_Idr, "I");
    rg.MoverA(g_MsgED125.ED125_Nev, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_MsgED125.ED125_Trt, g_PrmPC080.PC080_Trt);
    rg.MoverA(g_Msg.Msg_Dat, MSGED125.LSet_De_MsgED125(g_MsgED125));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED125");
    g_MsgED125 = MSGED125.LSet_A_MsgED125(g_Msg.Msg_Dat.toString());
    data.getUser().removeTemp("Agtp150_MsgED125");
    data.getUser().setTemp("Agtp150_MsgED125", g_MsgED125);
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    g_MsgED135 = MSGED135.Inicia_MsgED135();
    rg.MoverA(g_MsgED135.ED135_Idr, "C");
    rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
    rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
    g_MsgED135 = MSGED135.LSet_A_MsgED135(g_Msg.Msg_Dat.toString());
    data.getUser().removeTemp("Agtp150_MsgED135");
    data.getUser().setTemp("Agtp150_MsgED135", g_MsgED135);
  }
  //---------------------------------------------------------------------------------------
  public void Cursar(RunData data, Context context)
              throws Exception 
  {
    Vector g_Tab_Img = (Vector)data.getUser().getTemp("Agtp150_vTabImg");
    g_Tab_Img = A_Tab_Img(data, g_Tab_Img);
    if (!gcSeg.trim().equals("NOVA"))
       { rg.MoverA(g_PrmPC080.PC080_Ein, "RGEJE"); }
    img.Graba_Img_Host(g_Msg, g_Tab_Img, data, context);
    g_MsgED135 = MSGED135.Inicia_MsgED135();
    rg.MoverA(g_MsgED135.ED135_Idr, "M");
    rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
    rg.MoverA(g_MsgED135.ED135_Est, "");
    rg.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
    if (!gcSeg.trim().equals("NOVA"))
       { rg.MoverA(g_MsgED135.ED135_Ezt, "TERMI"); }
    else
       { rg.MoverA(g_MsgED135.ED135_Ezt, "ENJSV"); }
    rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
  }
  //---------------------------------------------------------------------------------------
  public Vector A_Tab_Img(RunData data, Vector g_Tab_Img)
  {
    int    liSwt = 0;
    Vector v_Img = new Vector();
    v_Img.clear();
    for (i=0; i<g_Tab_Img.size(); i++)
        {
          lcData = img.LSet_De_Img((BF_IMG.Buf_Img)g_Tab_Img.elementAt(i));
          g_Img  = img.LSet_A_Img(lcData);
          if (g_Img.Img_Dax.toString().trim().equals("BASE"))
             { rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtApe(g_Img_GtApe)); }
          if (g_Img.Img_Dax.toString().trim().equals("XXHIP")
           || g_Img.Img_Dax.toString().trim().equals("YYPRD"))
             { }
          else
             {
               Vector vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
               v_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
             }
        }
    v_Img_GtHyp = (Vector)data.getUser().getTemp("Agtp150_vImgGtHyp");
    rg.MoverA(g_Img.Img_Seq, "000");
    for (i=0; i<v_Img_GtHyp.size(); i++)
        {
          g_Img_GtHyp = (GT_IMG.Buf_Img_GtHyp)v_Img_GtHyp.elementAt(i);
          if (g_Img_GtHyp.GtHyp_Itb.toString().equals("HP"))
             { rg.MoverA(g_Img.Img_Dax, "XXHIP"); }
          else
             { rg.MoverA(g_Img.Img_Dax, "YYPRD"); }
          if (g_Img_GtHyp.GtHyp_Isg.toString().equals("M"))
             { gcSeg = "NOVA"; }
          rg.MoverA(g_Img.Img_Seq, rg.Suma(g_Img.Img_Seq,0,"1",0,3,0));
          rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtHyp(g_Img_GtHyp));
          Vector vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
          v_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
        }
    return v_Img;
  }
//---------------------------------------------------------------------------------------
  public void doAgtp150_Continue(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP150[doAgtp150_Continue.start]", "[" + data.getUser().getUserName() + "]");
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Atgp114"))
       {
         g_PrmTG114 = PRMTG114.LSet_A_PrmTG114(g_Msg.Msg_Dat.toString());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp150_g_PrmPC080");
         data.getUser().removeTemp("Agtp150_g_PrmPC080");
         if (g_PrmTG114.TG114_Csg.toString().trim().compareTo("")!=0)
             {
               g_Img_GtHyp = (GT_IMG.Buf_Img_GtHyp)data.getUser().getTemp("Agtp150_ImgGtHyp");
               rg.MoverA(g_Img_GtHyp.GtHyp_Csg, g_PrmTG114.TG114_Csg);
               rg.MoverA(g_Img_GtHyp.GtHyp_Nsg, g_PrmTG114.TG114_Ncs);
               data.getUser().removeTemp("Agtp150_ImgGtHyp");
               data.getUser().setTemp("Agtp150_ImgGtHyp", g_Img_GtHyp);
             }
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP150.vm" );
         return;
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Aedp210"))
       {
         g_PrmED210 = PRMED210.LSet_A_PrmED210(g_Msg.Msg_Dat.toString());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp150_g_PrmPC080");
         data.getUser().removeTemp("Agtp150_g_PrmPC080");
         if (g_PrmED210.ED210_Rtn.toString().equals("OK"))
            {
              g_MsgED135 = MSGED135.Inicia_MsgED135();
              rg.MoverA(g_MsgED135.ED135_Idr, "X");
              rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
              rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
              rg.MoverA(g_MsgED135.ED135_Est, "");
              rg.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
              rg.MoverA(g_MsgED135.ED135_Ldv, g_PrmED210.ED210_Ldt);
              if (g_PrmPC080.PC080_Amv.toString().equals("D"))
                 { rg.MoverA(g_MsgED135.ED135_Ezt, "DVJSV"); }
              if (g_PrmPC080.PC080_Amv.toString().equals("P"))
                 { rg.MoverA(g_MsgED135.ED135_Ezt, "PDNVA"); }
              rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
              g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
              data.getUser().removeTemp("Agtp150_PrmPC080");
              data.getUser().removeTemp("Agtp150_ImgBase");
              data.getUser().removeTemp("Agtp150_ImgGtApe");
              data.getUser().removeTemp("Agtp150_vImgGtHyp");
              data.getUser().removeTemp("Agtp150_vTabImg");
              data.getUser().removeTemp("Agtp150_MsgED125");
              data.getUser().removeTemp("Agtp150_MsgED135");
              rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
              rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
              BF_MSG.Return_Data("Agtp150", data, g_Msg);
              return;
            }
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP150.vm");
         return;
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Appp030"))
       {
         BF_MSG.Return_Program(data, g_Msg);
         return;
       }
  }
  //---------------------------------------------------------------------------------------
  public void Actualiza_Datos(RunData data, Context context)
              throws Exception 
  {
    rg.MoverA(g_Img_GtHyp.GtHyp_Isg, data.getParameters().getString("GtHyp_Isg", ""));
    rg.MoverA(g_Img_GtHyp.GtHyp_Csg, data.getParameters().getString("GtHyp_Csg", ""));
    rg.MoverA(g_Img_GtHyp.GtHyp_Nsg, data.getParameters().getString("GtHyp_Nsg", ""));
    rg.MoverA(g_Img_GtHyp.GtHyp_Spz, data.getParameters().getString("GtHyp_Spz", ""));
    rg.MoverA(g_Img_GtHyp.GtHyp_Sfv, data.getParameters().getString("GtHyp_Sfv", ""));
    rg.MoverA(g_Img_GtHyp.GtHyp_Svl, data.getParameters().getString("GtHyp_Svl", ""));
  }
  //---------------------------------------------------------------------------------------
}