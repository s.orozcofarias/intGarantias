// Source File Name:   Agtp734.java
// Descripcion:        Ingreso Unidades Vendibles (ED135, GT096)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.MSGTXCUR;
import com.FHTServlet.modules.global.BF_TRN;
import com.FHTServlet.modules.global.BF_IMG;
import com.FHTServlet.modules.global.PRMPC080;
import com.FHTServlet.modules.global.PRMPP030;

import com.intGlobal.modules.global.MSGED090;
import com.intGlobal.modules.global.MSGED135;
import com.intGlobal.modules.global.PRMED210;

import com.intGarantias.modules.global.GT_IMG;
import com.intGarantias.modules.global.MSGGT096;
import com.intGarantias.modules.global.MSGGT734;

import java.util.*;
import java.io.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.ParameterParser;
import org.apache.turbine.util.upload.FileItem;
import org.apache.turbine.util.Log;

public class Agtp734 extends FHTServletAction
{
  //---------------------------------------------------------------------------------------
  public int    i = 0;
  public String lcData = "";
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  MSGTXCUR.Buf_TxCur g_MsgTxCur    = new MSGTXCUR.Buf_TxCur();
  MSGTXCUR.Buf_Tx900 g_MsgTx900    = new MSGTXCUR.Buf_Tx900();
  BF_TRN trn                       = new BF_TRN();
  BF_TRN.Buf_Trn g_Trn             = new BF_TRN.Buf_Trn();
  PRMPC080.PrmPC080 g_PrmPC080     = new PRMPC080.PrmPC080();
  Vector v_Tab_Img                 = new Vector();
  Vector v_Img_GtUvd               = new Vector();
  BF_IMG img                       = new BF_IMG();
  BF_IMG.Buf_Img g_Img             = new BF_IMG.Buf_Img();
  BF_IMG.Buf_Img_Bse g_Img_Base    = new BF_IMG.Buf_Img_Bse();
  GT_IMG gtimg                     = new GT_IMG();
  GT_IMG.Buf_Img_GtApe g_Img_GtApe = new GT_IMG.Buf_Img_GtApe();
  GT_IMG.Buf_Img_GtTsd g_Img_GtTsd = new GT_IMG.Buf_Img_GtTsd();
  GT_IMG.Buf_Img_GtSte g_Img_GtSte = new GT_IMG.Buf_Img_GtSte();
  GT_IMG.Buf_Img_GtHpd g_Img_GtHpd = new GT_IMG.Buf_Img_GtHpd();
  GT_IMG.Buf_Img_GtPts g_Img_GtPts = new GT_IMG.Buf_Img_GtPts();
  MSGED090.Buf_MsgED090 g_MsgED090 = new MSGED090.Buf_MsgED090();
  MSGED135.Buf_MsgED135 g_MsgED135 = new MSGED135.Buf_MsgED135();
  MSGGT096.Buf_MsgGT096 g_MsgGT096 = new MSGGT096.Buf_MsgGT096();
  MSGGT734.Buf_MsgGT734 g_MsgGT734 = new MSGGT734.Buf_MsgGT734();
  PRMED210.Buf_PrmED210 g_PrmED210 = new PRMED210.Buf_PrmED210();
  PRMPP030.Buf_PrmPP030 g_PrmPP030 = new PRMPP030.Buf_PrmPP030();
  //---------------------------------------------------------------------------------------
  public Agtp734()
  {
  }
  //---------------------------------------------------------------------------------------
  public void doPerform(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP734[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp734-DIN", data);
    if (g_Msg.Msg_Pgr.toString().equals("RT"))
       { doAgtp734_Continue(data, context); }
    else
       { doAgtp734_Init(data, context); }
  }
  //---------------------------------------------------------------------------------------
  public void doCancel(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP734[doCancel.start]", "[CANCELADO]");
    setTemplate(data, "Garantias,Agt,AGTP734.vm");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp734_Init(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP734[doAgtp734_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    Carga_de_Host(data, context);
    Carga_Inicial(data, context);
    if (g_PrmPC080.PC080_Fte.toString().trim().equals("SPV")
     && g_PrmPC080.PC080_Rtn.toString().trim().equals("WR"))
       { rg.MoverA(g_PrmPC080.PC080_Rtn, "RA"); }
    else
       {
         if (g_PrmPC080.PC080_Fte.toString().trim().compareTo("TSD")!=0)
            { rg.MoverA(g_PrmPC080.PC080_Rtn, "RO"); }
       }
    data.getUser().removeTemp("Agtp734_PrmPC080");
    data.getUser().setTemp("Agtp734_PrmPC080", g_PrmPC080);
    rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
    BF_MSG.Param_Program(data, g_Msg);
    setTemplate(data, "Garantias,Agt,AGTP734.vm" );
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp734(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP734[doAgtp734.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp734-MAN", data);
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    String Opc = data.getParameters().getString("Opc" ,"");
    if (Opc.trim().equals("R"))
       {
         //Boton Errores
         g_MsgED135 = (MSGED135.Buf_MsgED135)data.getUser().getTemp("Agtp734_MsgED135");
         g_PrmED210 = PRMED210.Inicia_PrmED210();
         rg.MoverA(g_PrmED210.ED210_Rtn, "RO");
         rg.MoverA(g_PrmED210.ED210_Ldt, g_MsgED135.ED135_Ldv);
         data.getUser().removeTemp("Agtp734_g_PrmPC080");
         data.getUser().setTemp("Agtp734_g_PrmPC080" ,g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMED210.LSet_De_PrmED210(g_PrmED210));
         BF_MSG.Link_Program("Agtp734", "Aedp210", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("D"))
       {
         //Boton Devolver
         g_PrmED210 = PRMED210.Inicia_PrmED210();
         rg.MoverA(g_PrmED210.ED210_Rtn, "RW");
         rg.MoverA(g_PrmPC080.PC080_Amv, "D");
         data.getUser().removeTemp("Agtp734_g_PrmPC080");
         data.getUser().setTemp("Agtp734_g_PrmPC080" ,g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMED210.LSet_De_PrmED210(g_PrmED210));
         BF_MSG.Link_Program("Agtp734", "Aedp210", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("F"))
       {
         //Boton Cambio Fecha
         String Fec = data.getParameters().getString("Fec", "");
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp734_ImgGtApe");
         g_Img_GtTsd = (GT_IMG.Buf_Img_GtTsd)data.getUser().getTemp("Agtp734_ImgGtTsd");
         g_Img_GtSte = (GT_IMG.Buf_Img_GtSte)data.getUser().getTemp("Agtp734_ImgGtSte");
         v_Img_GtUvd.clear();
         rg.MoverA(g_Img_GtApe.GtApe_Tfr, rg.Zeros(3));
         rg.MoverA(g_Img_GtApe.GtApe_Vtt, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Vts, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Vtb, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Vcn, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Hvt, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Hvl, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Hvb, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Hvc, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Pvt, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Pvl, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Pvb, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Pvc, rg.Zeros(15));
         Actualiza_Fecha(Fec, data, context);
         data.getUser().removeTemp("Agtp734_ImgGtApe");
         data.getUser().setTemp("Agtp734_ImgGtApe", g_Img_GtApe);
         data.getUser().removeTemp("Agtp734_ImgGtTsd");
         data.getUser().setTemp("Agtp734_ImgGtTsd", g_Img_GtTsd);
         data.getUser().removeTemp("Agtp734_ImgGtSte");
         data.getUser().setTemp("Agtp734_ImgGtSte", g_Img_GtSte);
         data.getUser().removeTemp("Agtp734_vImgGtUvd");
         data.getUser().setTemp("Agtp734_vImgGtUvd", v_Img_GtUvd);
         setTemplate(data, "Garantias,Agt,AGTP734.vm" );
         return;
       }
    if (Opc.trim().equals("V"))
       {
         //Boton Envio (Recibido)
         g_Img_GtTsd = (GT_IMG.Buf_Img_GtTsd)data.getUser().getTemp("Agtp734_ImgGtTsd");
         g_Img_GtSte = (GT_IMG.Buf_Img_GtSte)data.getUser().getTemp("Agtp734_ImgGtSte");
         Actualiza_Datos("1", data, context);
         data.getUser().removeTemp("Agtp734_ImgGtTsd");
         data.getUser().setTemp("Agtp734_ImgGtTsd", g_Img_GtTsd);
         data.getUser().removeTemp("Agtp734_ImgGtSte");
         data.getUser().setTemp("Agtp734_ImgGtSte", g_Img_GtSte);
         data.getUser().removeTemp("Agtp734_g_PrmPC080");
         data.getUser().setTemp("Agtp734_g_PrmPC080", g_PrmPC080);
         rg.MoverA(g_PrmPC080.PC080_Rtn, "RO");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Link_Program("Agtp734", "Agtp214", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("H"))
       {
         //Boton Detalle (Captura Imagenes)
         g_Img_GtTsd = (GT_IMG.Buf_Img_GtTsd)data.getUser().getTemp("Agtp734_ImgGtTsd");
         g_Img_GtSte = (GT_IMG.Buf_Img_GtSte)data.getUser().getTemp("Agtp734_ImgGtSte");
         Actualiza_Datos("1", data, context);
         data.getUser().removeTemp("Agtp734_ImgGtTsd");
         data.getUser().setTemp("Agtp734_ImgGtTsd", g_Img_GtTsd);
         data.getUser().removeTemp("Agtp734_ImgGtSte");
         data.getUser().setTemp("Agtp734_ImgGtSte", g_Img_GtSte);
         data.getUser().removeTemp("Agtp734_g_PrmPC080");
         data.getUser().setTemp("Agtp734_g_PrmPC080" ,g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         //BF_MSG.Link_Program("Agtp734", "Agtp729", data, g_Msg);
         BF_MSG.Link_Program("Agtp734", "Appp096", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("J"))
       {
         //Boton Mostrar Imagen
         rg.MoverA(g_PrmPC080.PC080_Amv, "I");
         //rg.MoverA(g_PrmPC080.PC080_Rtn, "RO");
         data.getUser().removeTemp("Agtp734_g_PrmPC080");
         data.getUser().setTemp("Agtp734_g_PrmPC080", g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         //BF_MSG.Link_Program("Agtp734", "Agtp459", data, g_Msg);
         BF_MSG.Link_Program("Agtp734", "Appp096", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("G"))
       {
         //Boton Guardar
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp734_ImgGtApe");
         g_Img_GtTsd = (GT_IMG.Buf_Img_GtTsd)data.getUser().getTemp("Agtp734_ImgGtTsd");
         g_Img_GtSte = (GT_IMG.Buf_Img_GtSte)data.getUser().getTemp("Agtp734_ImgGtSte");
         Actualiza_Datos("1", data, context);
         v_Img_GtUvd = (Vector)data.getUser().getTemp("Agtp734_vImgGtUvd");
         Vector g_Tab_Img = A_Tab_Img(data);
         img.Graba_Img_Host(g_Msg, g_Tab_Img, data, context);
         setTemplate(data, "Garantias,Agt,AGTP734.vm" );
         return;
       }
    if (Opc.trim().equals("P"))
       {
         //Boton Pendiente
         g_Img_GtTsd = (GT_IMG.Buf_Img_GtTsd)data.getUser().getTemp("Agtp734_ImgGtTsd");
         g_Img_GtSte = (GT_IMG.Buf_Img_GtSte)data.getUser().getTemp("Agtp734_ImgGtSte");
         Actualiza_Datos("1", data, context);
         data.getUser().removeTemp("Agtp734_ImgGtTsd");
         data.getUser().setTemp("Agtp734_ImgGtTsd", g_Img_GtTsd);
         data.getUser().removeTemp("Agtp734_ImgGtSte");
         data.getUser().setTemp("Agtp734_ImgGtSte", g_Img_GtSte);
         g_PrmED210 = PRMED210.Inicia_PrmED210();
         rg.MoverA(g_PrmED210.ED210_Rtn, "RW");
         rg.MoverA(g_PrmPC080.PC080_Amv, "P");
         data.getUser().removeTemp("Agtp734_g_PrmPC080");
         data.getUser().setTemp("Agtp734_g_PrmPC080", g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMED210.LSet_De_PrmED210(g_PrmED210));
         BF_MSG.Link_Program("Agtp734", "Aedp210", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("S"))
       {
         //Boton Salir
         data.getUser().removeTemp("Agtp734_MsgED135");
         data.getUser().removeTemp("Agtp734_vImgGtUvd");
         data.getUser().removeTemp("Agtp734_ImgGtSte");
         data.getUser().removeTemp("Agtp734_ImgGtTsd");
         data.getUser().removeTemp("Agtp734_ImgBase");
         data.getUser().removeTemp("Agtp734_ImgGtApe");
         data.getUser().removeTemp("Agtp734_PrmPC080");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp734", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("I"))
       {
         //Boton Importar
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp734_ImgGtApe");
         g_Img_GtTsd = (GT_IMG.Buf_Img_GtTsd)data.getUser().getTemp("Agtp734_ImgGtTsd");
         g_Img_GtSte = (GT_IMG.Buf_Img_GtSte)data.getUser().getTemp("Agtp734_ImgGtSte");
         Actualiza_Datos("1", data, context);
         rg.MoverA(g_Img_GtApe.GtApe_Idx, " ");
         rg.MoverA(g_Img_GtApe.GtApe_Psw, "I");
         data.getUser().removeTemp("Agtp734_ImgGtApe");
         data.getUser().setTemp("Agtp734_ImgGtApe", g_Img_GtApe);
         data.getUser().removeTemp("Agtp734_ImgGtTsd");
         data.getUser().setTemp("Agtp734_ImgGtTsd", g_Img_GtTsd);
         data.getUser().removeTemp("Agtp734_ImgGtSte");
         data.getUser().setTemp("Agtp734_ImgGtSte", g_Img_GtSte);
         setTemplate(data, "Garantias,Agt,AGTP734.vm" );
         return;
       }
    if (Opc.trim().equals("T"))
       {
         //Boton Acepta Importar
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp734_ImgGtApe");
         g_Img_GtTsd = (GT_IMG.Buf_Img_GtTsd)data.getUser().getTemp("Agtp734_ImgGtTsd");
         g_Img_GtSte = (GT_IMG.Buf_Img_GtSte)data.getUser().getTemp("Agtp734_ImgGtSte");
         v_Img_GtUvd = (Vector)data.getUser().getTemp("Agtp734_vImgGtUvd");
         File_Banco(data, context);
         Recalcula_GtApe(data, context);
         rg.MoverA(g_Img_GtApe.GtApe_Fig, g_Msg.Msg_Fch);
         //rg.MoverA(g_Img_GtApe.GtApe_Fts, g_Msg.Msg_Fch);
         rg.MoverA(g_Img_GtApe.GtApe_Vcn, g_Img_GtApe.GtApe_Vts);
         rg.MoverA(g_Img_GtApe.GtApe_Idx, " ");
         rg.MoverA(g_Img_GtApe.GtApe_Psw, " ");
         data.getUser().removeTemp("Agtp734_ImgGtApe");
         data.getUser().setTemp("Agtp734_ImgGtApe", g_Img_GtApe);
         data.getUser().removeTemp("Agtp734_vImgGtUvd");
         data.getUser().setTemp("Agtp734_vImgGtUvd", v_Img_GtUvd);
         setTemplate(data, "Garantias,Agt,AGTP734.vm" );
         return;
       }
    if (Opc.trim().equals("K"))
       {
         //Boton Cancela Importar
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp734_ImgGtApe");
         rg.MoverA(g_Img_GtApe.GtApe_Idx, " ");
         rg.MoverA(g_Img_GtApe.GtApe_Psw, " ");
         data.getUser().removeTemp("Agtp734_ImgGtApe");
         data.getUser().setTemp("Agtp734_ImgGtApe", g_Img_GtApe);
         setTemplate(data, "Garantias,Agt,AGTP734.vm" );
         return;
       }
    if (Opc.trim().equals("O"))
       {
         //Boton OK (Aceptar)
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp734_ImgGtApe");
         g_Img_GtTsd = (GT_IMG.Buf_Img_GtTsd)data.getUser().getTemp("Agtp734_ImgGtTsd");
         g_Img_GtSte = (GT_IMG.Buf_Img_GtSte)data.getUser().getTemp("Agtp734_ImgGtSte");
         Actualiza_Datos("1", data, context);
         v_Img_GtUvd = (Vector)data.getUser().getTemp("Agtp734_vImgGtUvd");
         Recalcula_GtApe(data, context);
         Vector g_Tab_Img = A_Tab_Img(data);
         img.Graba_Img_Host(g_Msg, g_Tab_Img, data, context);
         g_MsgED135 = MSGED135.Inicia_MsgED135();
         rg.MoverA(g_MsgED135.ED135_Idr, "M");
         rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
         rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
         rg.MoverA(g_MsgED135.ED135_Est, "");
         rg.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
         rg.MoverA(g_MsgED135.ED135_Vlc, g_Img_GtApe.GtApe_Vts);
         rg.MoverA(g_MsgED135.ED135_Ezt, "TMTSD");
         rg.MoverA(g_MsgED135.ED135_Exi, "TSD");
         rg.MoverA(g_MsgED135.ED135_Exn, rg.Zeros(12));
         rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
         g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
         data.getUser().removeTemp("Agtp734_MsgED135");
         data.getUser().removeTemp("Agtp734_vImgGtUvd");
         data.getUser().removeTemp("Agtp734_ImgGtSte");
         data.getUser().removeTemp("Agtp734_ImgGtTsd");
         data.getUser().removeTemp("Agtp734_ImgBase");
         data.getUser().removeTemp("Agtp734_ImgGtApe");
         data.getUser().removeTemp("Agtp734_PrmPC080");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp734", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("C"))
       {
         //Boton Cursar
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp734_ImgGtApe");
         g_Img_GtTsd = (GT_IMG.Buf_Img_GtTsd)data.getUser().getTemp("Agtp734_ImgGtTsd");
         g_Img_GtSte = (GT_IMG.Buf_Img_GtSte)data.getUser().getTemp("Agtp734_ImgGtSte");
         //if (PagoTasador(data, context) == false )
         //   {
         //     data.getUser().removeTemp("Agtp734_g_PrmPC080");
         //     data.getUser().setTemp("Agtp734_g_PrmPC080", g_PrmPC080);
         //     BF_MSG.MsgInt("Agtp734", data, g_Msg, "Msg_Rtn", "T");
         //     return;
         //   }
         Actualiza_Datos("1", data, context);
         v_Img_GtUvd = (Vector)data.getUser().getTemp("Agtp734_vImgGtUvd");
         Recalcula_GtApe(data, context);
         Vector g_Tab_Img = A_Tab_Img(data);
         img.Graba_Img_Host(g_Msg, g_Tab_Img, data, context);
         g_MsgED135 = MSGED135.Inicia_MsgED135();
         rg.MoverA(g_MsgED135.ED135_Idr, "M");
         rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
         rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
         rg.MoverA(g_MsgED135.ED135_Est, "");
         rg.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
         rg.MoverA(g_MsgED135.ED135_Ezt, "TERMI");
         rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
         g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
         //data.getUser().removeTemp("Agtp734_g_PrmPC080");
         //data.getUser().setTemp("Agtp734_g_PrmPC080", g_PrmPC080);
         //rg.MoverA(g_Msg.Msg_Dat, MSGTXCUR.LSet_De_MsgTxCur(g_MsgTxCur));
         //g_MsgTx900 = MSGTXCUR.LSet_A_MsgTx900(g_Msg.Msg_Dat.toString());
         //data.getUser().removeTemp("Agtp291_MsgTx900");
         //data.getUser().setTemp("Agtp291_MsgTx900", g_MsgTx900);
         //rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         //BF_MSG.Link_Program("Agtp734", "Agtp291", data, g_Msg);
         data.getUser().removeTemp("Agtp734_MsgED135");
         data.getUser().removeTemp("Agtp734_vImgGtUvd");
         data.getUser().removeTemp("Agtp734_ImgGtSte");
         data.getUser().removeTemp("Agtp734_ImgGtTsd");
         data.getUser().removeTemp("Agtp734_ImgBase");
         data.getUser().removeTemp("Agtp734_ImgGtApe");
         data.getUser().removeTemp("Agtp734_PrmPC080");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp734", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("A"))
       {
         //Boton Agregar
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp734_ImgGtApe");
         g_Img_Base  = img.LSet_A_ImgBase(g_Img_GtApe.GtApe_Bse.toString());
         g_Img_GtTsd = (GT_IMG.Buf_Img_GtTsd)data.getUser().getTemp("Agtp734_ImgGtTsd");
         g_Img_GtSte = (GT_IMG.Buf_Img_GtSte)data.getUser().getTemp("Agtp734_ImgGtSte");
         Actualiza_Datos("1", data, context);
         g_Img_GtHpd = gtimg.LSet_A_ImgGtHpd(rg.Blancos(827));
         rg.MoverA(g_Img_GtApe.GtApe_Psw, "A");
         rg.MoverA(g_Img_GtApe.GtApe_Idx, rg.Suma(g_Img_GtApe.GtApe_Tfr,0,"1",0,3,0));
         data.getUser().removeTemp("Agtp734_ImgGtApe");
         data.getUser().setTemp("Agtp734_ImgGtApe", g_Img_GtApe);
         data.getUser().removeTemp("Agtp734_ImgGtTsd");
         data.getUser().setTemp("Agtp734_ImgGtTsd", g_Img_GtTsd);
         data.getUser().removeTemp("Agtp734_ImgGtSte");
         data.getUser().setTemp("Agtp734_ImgGtSte", g_Img_GtSte);
         data.getUser().removeTemp("Agtp734_ImgGtHpd");
         data.getUser().setTemp("Agtp734_ImgGtHpd", g_Img_GtHpd);
         setTemplate(data, "Garantias,Agt,AGTP734.vm" );
         return;
       }
    if (Opc.trim().equals("M"))
       {
         //Boton Modificar
         String Seq = data.getParameters().getString("Seq", "");
         i = Integer.parseInt(Seq);
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp734_ImgGtApe");
         g_Img_Base  = img.LSet_A_ImgBase(g_Img_GtApe.GtApe_Bse.toString());
         g_Img_GtTsd = (GT_IMG.Buf_Img_GtTsd)data.getUser().getTemp("Agtp734_ImgGtTsd");
         g_Img_GtSte = (GT_IMG.Buf_Img_GtSte)data.getUser().getTemp("Agtp734_ImgGtSte");
         Actualiza_Datos("1", data, context);
         v_Img_GtUvd = (Vector)data.getUser().getTemp("Agtp734_vImgGtUvd");
         g_Img_GtHpd = (GT_IMG.Buf_Img_GtHpd)v_Img_GtUvd.elementAt(i);
         rg.MoverA(g_Img_GtApe.GtApe_Psw, "M");
         rg.MoverA(g_Img_GtApe.GtApe_Idx, rg.Suma(Seq,0,"1",0,3,0));
         data.getUser().removeTemp("Agtp734_ImgGtApe");
         data.getUser().setTemp("Agtp734_ImgGtApe", g_Img_GtApe);
         data.getUser().removeTemp("Agtp734_ImgGtTsd");
         data.getUser().setTemp("Agtp734_ImgGtTsd", g_Img_GtTsd);
         data.getUser().removeTemp("Agtp734_ImgGtSte");
         data.getUser().setTemp("Agtp734_ImgGtSte", g_Img_GtSte);
         data.getUser().removeTemp("Agtp734_ImgGtHpd");
         data.getUser().setTemp("Agtp734_ImgGtHpd", g_Img_GtHpd);
         setTemplate(data, "Garantias,Agt,AGTP734.vm" );
         return;
       }
    if (Opc.trim().equals("E"))
       {
         //Boton Eliminar
         String Seq = data.getParameters().getString("Seq", "");
         i = Integer.parseInt(Seq);
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp734_ImgGtApe");
         g_Img_Base  = img.LSet_A_ImgBase(g_Img_GtApe.GtApe_Bse.toString());
         g_Img_GtTsd = (GT_IMG.Buf_Img_GtTsd)data.getUser().getTemp("Agtp734_ImgGtTsd");
         g_Img_GtSte = (GT_IMG.Buf_Img_GtSte)data.getUser().getTemp("Agtp734_ImgGtSte");
         Actualiza_Datos("1", data, context);
         v_Img_GtUvd = (Vector)data.getUser().getTemp("Agtp734_vImgGtUvd");
         g_Img_GtHpd = (GT_IMG.Buf_Img_GtHpd)v_Img_GtUvd.elementAt(i);
         rg.MoverA(g_Img_GtApe.GtApe_Psw, "E");
         rg.MoverA(g_Img_GtApe.GtApe_Idx, rg.Suma(Seq,0,"1",0,3,0));
         data.getUser().removeTemp("Agtp734_ImgGtApe");
         data.getUser().setTemp("Agtp734_ImgGtApe", g_Img_GtApe);
         data.getUser().removeTemp("Agtp734_ImgGtTsd");
         data.getUser().setTemp("Agtp734_ImgGtTsd", g_Img_GtTsd);
         data.getUser().removeTemp("Agtp734_ImgGtSte");
         data.getUser().setTemp("Agtp734_ImgGtSte", g_Img_GtSte);
         data.getUser().removeTemp("Agtp734_ImgGtHpd");
         data.getUser().setTemp("Agtp734_ImgGtHpd", g_Img_GtHpd);
         setTemplate(data, "Garantias,Agt,AGTP734.vm" );
         return;
       }
    if (Opc.trim().equals("Y"))
       {
         //Boton Si
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp734_ImgGtApe");
         g_Img_GtTsd = (GT_IMG.Buf_Img_GtTsd)data.getUser().getTemp("Agtp734_ImgGtTsd");
         g_Img_GtSte = (GT_IMG.Buf_Img_GtSte)data.getUser().getTemp("Agtp734_ImgGtSte");
         g_Img_GtHpd = (GT_IMG.Buf_Img_GtHpd)data.getUser().getTemp("Agtp734_ImgGtHpd");
         Actualiza_Datos(Opc, data, context);
         v_Img_GtUvd = (Vector)data.getUser().getTemp("Agtp734_vImgGtUvd");
         if (g_Img_GtApe.GtApe_Psw.toString().trim().equals("A"))
            {
              v_Img_GtUvd.add(g_Img_GtHpd);
              rg.MoverA(g_Img_GtApe.GtApe_Tfr, rg.Suma(g_Img_GtApe.GtApe_Tfr,0,"1",0,3,0));
            }
         else
            {
            i = Integer.parseInt(g_Img_GtApe.GtApe_Idx.toString()) - 1;
            GT_IMG.Buf_Img_GtHpd l_Img_GtHpd = new GT_IMG.Buf_Img_GtHpd();
            l_Img_GtHpd = (GT_IMG.Buf_Img_GtHpd)v_Img_GtUvd.elementAt(i);
            if (g_Img_GtApe.GtApe_Psw.toString().trim().equals("M"))
               {
                 v_Img_GtUvd.set(i, g_Img_GtHpd);
               }
            else
               {
                 if (g_Img_GtApe.GtApe_Psw.toString().trim().equals("E"))
                    {
                      v_Img_GtUvd.remove(i);
                    }
               }
            }
         Recalcula_GtApe(data, context);
         rg.MoverA(g_Img_GtApe.GtApe_Fig, g_Msg.Msg_Fch);
         //rg.MoverA(g_Img_GtApe.GtApe_Fts, g_Msg.Msg_Fch);
         rg.MoverA(g_Img_GtApe.GtApe_Vcn, g_Img_GtApe.GtApe_Vts);
         rg.MoverA(g_Img_GtApe.GtApe_Idx, " ");
         rg.MoverA(g_Img_GtApe.GtApe_Psw, " ");
         data.getUser().removeTemp("Agtp734_ImgGtApe");
         data.getUser().setTemp("Agtp734_ImgGtApe", g_Img_GtApe);
         data.getUser().removeTemp("Agtp734_ImgGtTsd");
         data.getUser().setTemp("Agtp734_ImgGtTsd", g_Img_GtTsd);
         data.getUser().removeTemp("Agtp734_ImgGtSte");
         data.getUser().setTemp("Agtp734_ImgGtSte", g_Img_GtSte);
         data.getUser().removeTemp("Agtp734_vImgGtUvd");
         data.getUser().setTemp("Agtp734_vImgGtUvd", v_Img_GtUvd);
         data.getUser().removeTemp("Agtp734_ImgGtHpd");
         setTemplate(data, "Garantias,Agt,AGTP734.vm" );
         return;
       }
    if (Opc.trim().equals("N"))
       {
         //Boton No
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp734_ImgGtApe");
         rg.MoverA(g_Img_GtApe.GtApe_Idx, " ");
         rg.MoverA(g_Img_GtApe.GtApe_Psw, " ");
         data.getUser().removeTemp("Agtp734_ImgGtApe");
         data.getUser().setTemp("Agtp734_ImgGtApe", g_Img_GtApe);
         data.getUser().removeTemp("Agtp734_ImgGtHpd");
         setTemplate(data, "Garantias,Agt,AGTP734.vm" );
         return;
       }
  }
  //---------------------------------------------------------------------------------------
  public void Actualiza_Datos(String Opc, RunData data, Context context)
              throws Exception
  {
    if (Opc.trim().equals("1"))
       {
         rg.MoverA(g_Img_GtTsd.GtTsd_Fts, data.getParameters().getString("GtTsd_Fts", ""));
         rg.MoverA(g_Img_GtTsd.GtTsd_Nbf, data.getParameters().getString("GtTsd_Nbf", ""));
         rg.MoverA(g_Img_GtTsd.GtTsd_Pcb, data.getParameters().getString("GtTsd_Pcb", ""));
         rg.MoverA(g_Img_GtTsd.GtTsd_Hon, data.getParameters().getString("GtTsd_Hon", ""));
         rg.MoverA(g_Img_GtTsd.GtTsd_Vst, data.getParameters().getString("GtTsd_Vst", ""));
         rg.MoverA(g_Img_GtSte.GtSte_Udd, data.getParameters().getString("GtSte_Udd", ""));
         rg.MoverA(g_Img_GtSte.GtSte_Pum, data.getParameters().getString("GtSte_Pum", ""));
         rg.MoverA(g_Img_GtSte.GtSte_Vlq, data.getParameters().getString("GtSte_Vlq", ""));
         rg.MoverA(g_Img_GtSte.GtSte_Vtb, data.getParameters().getString("GtSte_Vrt", ""));
         rg.MoverA(g_Img_GtSte.GtSte_Vsg, data.getParameters().getString("GtSte_Vsg", ""));
         rg.MoverA(g_Img_GtSte.GtSte_Vum, g_Img_GtTsd.GtTsd_Vuf);
         rg.MoverA(g_Img_GtSte.GtSte_Vtu, rg.Multiplica(g_Img_GtSte.GtSte_Udd,2,g_Img_GtSte.GtSte_Pum,4,15,4,4));
         rg.MoverA(g_Img_GtSte.GtSte_Vtt, rg.Multiplica(g_Img_GtSte.GtSte_Vtu,4,g_Img_GtSte.GtSte_Vum,2,15,4,0));
         rg.MoverA(g_Img_GtSte.GtSte_Vts, rg.Multiplica(g_Img_GtSte.GtSte_Vtu,4,g_Img_GtSte.GtSte_Vum,2,15,2,0));
         rg.MoverA(g_Img_GtSte.GtSte_Tuf, rg.Multiplica(g_Img_GtSte.GtSte_Udd,2,g_Img_GtSte.GtSte_Pum,4,15,4,4));
         rg.MoverA(g_Img_GtSte.GtSte_Tps, rg.Multiplica(g_Img_GtSte.GtSte_Tuf,4,g_Img_GtTsd.GtTsd_Vuf,2,15,2,0));
         rg.MoverA(g_Img_GtSte.GtSte_Ttc, rg.Divide(g_Img_GtSte.GtSte_Tps,2,g_Img_GtTsd.GtTsd_Vtc,2,15,2,2));
         rg.MoverA(g_Img_GtSte.GtSte_Vcn, rg.Multiplica(g_Img_GtSte.GtSte_Vtb,2,g_Img_GtSte.GtSte_Pct,4,15,2,0));
         rg.MoverA(g_Img_GtSte.GtSte_Frv, g_Img_GtTsd.GtTsd_Fts);
         rg.MoverA(g_Img_GtSte.GtSte_Vur, rg.FmtValor("1",0,2,9,"+"));
         rg.MoverA(g_Img_GtSte.GtSte_Fvt, rg.DateAdd(g_Img_GtSte.GtSte_Frv,"A",2));
       }
    else
       {
         rg.MoverA(g_Img_GtHpd.GtHpd_Seq, g_Img_GtApe.GtApe_Idx);
         rg.MoverA(g_Img_GtHpd.GtHpd_Rs1, data.getParameters().getString("GtHpd_Rs1", ""));
         rg.MoverA(g_Img_GtHpd.GtHpd_Rs2, data.getParameters().getString("GtHpd_Rs2", ""));
         rg.MoverA(g_Img_GtHpd.GtHpd_Str, data.getParameters().getString("GtHpd_Str", ""));
         rg.MoverA(g_Img_GtHpd.GtHpd_Udd, data.getParameters().getString("GtHpd_Udd", ""));
         rg.MoverA(g_Img_GtHpd.GtHpd_Umd, "M2");
         rg.MoverA(g_Img_GtHpd.GtHpd_Umt, "U");
         rg.MoverA(g_Img_GtHpd.GtHpd_Vtu, data.getParameters().getString("GtHpd_Vtu", ""));
         rg.MoverA(g_Img_GtHpd.GtHpd_Pum, rg.Divide(g_Img_GtHpd.GtHpd_Vtu,4,g_Img_GtHpd.GtHpd_Udd,2,15,4,4));
         rg.MoverA(g_Img_GtHpd.GtHpd_Vbu, g_Img_GtHpd.GtHpd_Vtu);
         rg.MoverA(g_Img_GtHpd.GtHpd_Vlu, g_Img_GtHpd.GtHpd_Vtu);
         rg.MoverA(g_Img_GtHpd.GtHpd_Vsu, g_Img_GtHpd.GtHpd_Vtu);
         rg.MoverA(g_Img_GtHpd.GtHpd_Vts, rg.Multiplica(g_Img_GtHpd.GtHpd_Vtu,4,g_Img_GtTsd.GtTsd_Vuf,2,15,2,0));
         rg.MoverA(g_Img_GtHpd.GtHpd_Vlq, g_Img_GtHpd.GtHpd_Vts);
         rg.MoverA(g_Img_GtHpd.GtHpd_Vtb, g_Img_GtHpd.GtHpd_Vts);
         rg.MoverA(g_Img_GtHpd.GtHpd_Vcn, g_Img_GtHpd.GtHpd_Vts);
         rg.MoverA(g_Img_GtHpd.GtHpd_Vsg, g_Img_GtHpd.GtHpd_Vts);
       }
  }
  //---------------------------------------------------------------------------------------
  public void Carga_de_Host(RunData data, Context context)
              throws Exception
  {
    v_Tab_Img = img.Carga_Img_Host(g_Msg, g_PrmPC080.PC080_Ntr.toString(), data, context);
    De_Tab_Img(v_Tab_Img, data);
    rg.MoverA(g_Img_GtApe.GtApe_Psw, " ");
    if (g_Img_GtApe.GtApe_Tfr.toString().trim().equals(""))
       { rg.MoverA(g_Img_GtApe.GtApe_Tfr, rg.Zeros(3)); }
    if (g_Img_GtApe.GtApe_Vtt.toString().trim().equals(""))
       {
         rg.MoverA(g_Img_GtApe.GtApe_Vtt, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Vts, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Vtb, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Vcn, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Hvt, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Hvl, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Hvb, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Hvc, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Pvt, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Pvl, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Pvb, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Pvc, rg.Zeros(15));
       }
    if (g_Img_GtTsd.GtTsd_Fts.toString().trim().equals(""))
       {
         rg.MoverA(g_Img_GtApe.GtApe_Fig, g_Msg.Msg_Fch);
         Actualiza_Fecha(g_Msg.Msg_Fch.toString(), data, context);
       }
    if (g_Img_GtSte.GtSte_Seq.toString().trim().equals(""))
       { Inicializa(data, context); }
    data.getUser().removeTemp("Agtp734_ImgBase");
    data.getUser().setTemp("Agtp734_ImgBase", g_Img_Base);
    data.getUser().removeTemp("Agtp734_ImgGtApe");
    data.getUser().setTemp("Agtp734_ImgGtApe", g_Img_GtApe);
    data.getUser().removeTemp("Agtp734_ImgGtTsd");
    data.getUser().setTemp("Agtp734_ImgGtTsd", g_Img_GtTsd);
    data.getUser().removeTemp("Agtp734_ImgGtSte");
    data.getUser().setTemp("Agtp734_ImgGtSte", g_Img_GtSte);
    data.getUser().removeTemp("Agtp734_vImgGtUvd");
    data.getUser().setTemp("Agtp734_vImgGtUvd", v_Img_GtUvd);
  }
  //---------------------------------------------------------------------------------------
  public void Actualiza_Fecha(String Fecha, RunData data, Context context)
              throws Exception
  {
    rg.MoverA(g_Img_GtTsd.GtTsd_Fts, Fecha);
    g_MsgGT096 = MSGGT096.Inicia_MsgGT096();
    rg.MoverA(g_MsgGT096.GT096_Idr, "V");
    rg.MoverA(g_MsgGT096.GT096_Fec, g_Img_GtTsd.GtTsd_Fts);
    rg.MoverA(g_Msg.Msg_Dat, MSGGT096.LSet_De_MsgGT096(g_MsgGT096));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GT096");
    g_MsgGT096 = MSGGT096.LSet_A_MsgGT096(g_Msg.Msg_Dat.toString());
    rg.MoverA(g_Img_GtApe.GtApe_Fts, g_Img_GtTsd.GtTsd_Fts);
    rg.MoverA(g_Img_GtApe.GtApe_Vuo, rg.FmtValor("1",0,2,9,"+"));
    if (g_Img_Base.Img_Base_Trj.toString().trim().equals("UF"))
       { rg.MoverA(g_Img_GtApe.GtApe_Vuo, rg.FmtValor(g_MsgGT096.GT096_Vuf,2,2,9,"+")); }
    if (g_Img_Base.Img_Base_Trj.toString().trim().equals("IVP"))
       { rg.MoverA(g_Img_GtApe.GtApe_Vuo, rg.FmtValor(g_MsgGT096.GT096_Viv,2,2,9,"+")); }
    if (g_Img_Base.Img_Base_Trj.toString().trim().equals("TC"))
       { rg.MoverA(g_Img_GtApe.GtApe_Vuo, rg.FmtValor(g_MsgGT096.GT096_Vtc,2,2,9,"+")); }
    rg.MoverA(g_Img_GtTsd.GtTsd_Vuf, rg.FmtValor(g_MsgGT096.GT096_Vuf,2,2,9,"+"));
    rg.MoverA(g_Img_GtTsd.GtTsd_Vtc, rg.FmtValor(g_MsgGT096.GT096_Vtc,2,2,9,"+"));
  }
  //---------------------------------------------------------------------------------------
  public void De_Tab_Img(Vector v_Tab_Img, RunData data)
  {
    g_Img       = img.Inicia_Img();
    lcData      = img.LSet_De_Img(g_Img);
    g_Img_Base  = img.LSet_A_ImgBase(lcData);
    g_Img_GtApe = gtimg.LSet_A_ImgGtApe(g_Img.Img_Dat.toString());
    g_Img_GtTsd = gtimg.LSet_A_ImgGtTsd(g_Img.Img_Dat.toString());
    g_Img_GtSte = gtimg.LSet_A_ImgGtSte(g_Img.Img_Dat.toString());
    v_Img_GtUvd.clear();
    for (i=0; i<v_Tab_Img.size(); i++)
        {
          lcData = img.LSet_De_Img((BF_IMG.Buf_Img)v_Tab_Img.elementAt(i));
          g_Img  = img.LSet_A_Img(lcData);
          if (g_Img.Img_Dax.toString().trim().equals("BASE"))
             {
               g_Img_GtApe = gtimg.LSet_A_ImgGtApe(g_Img.Img_Dat.toString());
               g_Img_Base  = img.LSet_A_ImgBase(g_Img_GtApe.GtApe_Bse.toString());
             }
          if (g_Img.Img_Dax.toString().trim().equals("XTSD"))
             { g_Img_GtTsd = gtimg.LSet_A_ImgGtTsd(g_Img.Img_Dat.toString()); }
          if (g_Img.Img_Dax.toString().trim().equals("XYSTE"))
             { g_Img_GtSte = gtimg.LSet_A_ImgGtSte(g_Img.Img_Dat.toString()); }
          if (g_Img.Img_Dax.toString().trim().equals("ZZHPD"))
             {
               g_Img_GtHpd = gtimg.LSet_A_ImgGtHpd(g_Img.Img_Dat.toString());
               v_Img_GtUvd.add (g_Img_GtHpd);
             }
        }
  }
  //---------------------------------------------------------------------------------------
  public void Inicializa(RunData data, Context context)
              throws Exception
  {
    g_MsgGT734 = MSGGT734.Inicia_MsgGT734();
    rg.MoverA(g_MsgGT734.GT734_Idr, "I");
    rg.MoverA(g_MsgGT734.GT734_Gti, g_Img_Base.Img_Base_Sis.toString() + g_Img_Base.Img_Base_Ncn.toString());
    rg.MoverA(g_MsgGT734.GT734_Seq, "000");
    rg.MoverA(g_Msg.Msg_Dat, MSGGT734.LSet_De_MsgGT734(g_MsgGT734));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GT734");
    g_MsgGT734 = MSGGT734.LSet_A_MsgGT734(g_Msg.Msg_Dat.toString());
    rg.MoverA(g_Img_GtSte.GtSte_Seq, "000");
    rg.MoverA(g_Img_GtSte.GtSte_Itb, g_MsgGT734.GT734_Itb);
    rg.MoverA(g_Img_GtSte.GtSte_Itg, g_MsgGT734.GT734_Itg);
    rg.MoverA(g_Img_GtSte.GtSte_Dpc, g_MsgGT734.GT734_Dpc);
    rg.MoverA(g_Img_GtSte.GtSte_Dsb, g_MsgGT734.GT734_Dsb);
    rg.MoverA(g_Img_GtSte.GtSte_Sqd, g_MsgGT734.GT734_Sqd);
    rg.MoverA(g_Img_GtSte.GtSte_Frv, g_Img_GtTsd.GtTsd_Fts);
    rg.MoverA(g_Img_GtSte.GtSte_Vur, rg.FmtValor("1",0,2,9,"+"));
    rg.MoverA(g_Img_GtSte.GtSte_Udd, g_MsgGT734.GT734_Udd);
    rg.MoverA(g_Img_GtSte.GtSte_Umd, g_MsgGT734.GT734_Umd);
    rg.MoverA(g_Img_GtSte.GtSte_Umt, g_MsgGT734.GT734_Umt);
    rg.MoverA(g_Img_GtSte.GtSte_Pum, g_MsgGT734.GT734_Pum);
    rg.MoverA(g_Img_GtSte.GtSte_Vtu, g_MsgGT734.GT734_Vtu);
    rg.MoverA(g_Img_GtSte.GtSte_Vum, g_MsgGT734.GT734_Vum);
    rg.MoverA(g_Img_GtSte.GtSte_Vtt, g_MsgGT734.GT734_Vtt);
    rg.MoverA(g_Img_GtSte.GtSte_Vts, g_MsgGT734.GT734_Vts);
    rg.MoverA(g_Img_GtSte.GtSte_Tuf, g_MsgGT734.GT734_Vtu);
    rg.MoverA(g_Img_GtSte.GtSte_Tps, rg.Multiplica(g_Img_GtSte.GtSte_Tuf,4,g_Img_GtTsd.GtTsd_Vuf,2,15,2,0));
    rg.MoverA(g_Img_GtSte.GtSte_Ttc, rg.Divide(g_Img_GtSte.GtSte_Tps,2,g_Img_GtTsd.GtTsd_Vtc,2,15,2,2));
    rg.MoverA(g_Img_GtSte.GtSte_Pct, g_MsgGT734.GT734_Pct);
    rg.MoverA(g_Img_GtSte.GtSte_Vcn, g_MsgGT734.GT734_Vcn);
    rg.MoverA(g_Img_GtSte.GtSte_Vlq, g_MsgGT734.GT734_Vlq);
    rg.MoverA(g_Img_GtSte.GtSte_Vtb, g_MsgGT734.GT734_Vtb);
    rg.MoverA(g_Img_GtSte.GtSte_Vsg, g_MsgGT734.GT734_Vsg);
    rg.MoverA(g_Img_GtSte.GtSte_Fvt, rg.DateAdd(g_Img_GtSte.GtSte_Frv,"A",2));
    rg.MoverA(g_Img_GtSte.GtSte_Fll, "");
    rg.MoverA(g_Img_GtSte.GtSte_Iro, "R");
  }
  //---------------------------------------------------------------------------------------
  public void Carga_Inicial(RunData data, Context context)
              throws Exception
  {
    g_MsgED135 = MSGED135.Inicia_MsgED135();
    rg.MoverA(g_MsgED135.ED135_Idr, "C");
    rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
    rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
    if (g_Msg.Msg_Rtn.toString().compareTo("0")!=0)
       {
         data.getUser().removeTemp("Agtp734_g_PrmPC080");
         data.getUser().setTemp("Agtp734_g_PrmPC080", g_PrmPC080);
         BF_MSG.MsgInt("Agtp734", data, g_Msg, "Msg_Rtn", "1");
         return;
       }
    g_MsgED135 = MSGED135.LSet_A_MsgED135(g_Msg.Msg_Dat.toString());
    data.getUser().removeTemp("Agtp734_MsgED135");
    data.getUser().setTemp("Agtp734_MsgED135", g_MsgED135);
  }
  //---------------------------------------------------------------------------------------
  public Vector A_Tab_Img(RunData data)
  {
    Vector v_Img = new Vector();
    v_Img.clear();
    rg.MoverA(g_Img.Img_Ntr, g_PrmPC080.PC080_Ntr);
    //--------------------------------------------------------------------------
    rg.MoverA(g_Img.Img_Dax, "BASE");
    rg.MoverA(g_Img.Img_Seq, "000");
    rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtApe(g_Img_GtApe));
    Vector vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
    v_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
    //--------------------------------------------------------------------------
    rg.MoverA(g_Img.Img_Dax, "XTSD");
    rg.MoverA(g_Img.Img_Seq, "000");
    rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtTsd(g_Img_GtTsd));
    vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
    v_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
    //--------------------------------------------------------------------------
    rg.MoverA(g_Img.Img_Dax, "XYSTE");
    rg.MoverA(g_Img.Img_Seq, "000");
    rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtSte(g_Img_GtSte));
    vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
    v_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
    //--------------------------------------------------------------------------
    rg.MoverA(g_Img.Img_Seq, "000");
    for (i=0; i<v_Img_GtUvd.size(); i++)
        {
          g_Img_GtHpd = (GT_IMG.Buf_Img_GtHpd)v_Img_GtUvd.elementAt(i);
          rg.MoverA(g_Img.Img_Dax, "ZZHPD");
          rg.MoverA(g_Img.Img_Seq, rg.Suma(g_Img.Img_Seq,0,"1",0,3,0));
          rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtHpd(g_Img_GtHpd));
          vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
          v_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
        }
    //--------------------------------------------------------------------------
    return v_Img;
  }
//---------------------------------------------------------------------------------------
  public void doAgtp734_Continue(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP734[doAgtp734_Continue.start]", "[" + data.getUser().getUserName() + "]");
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Aedp210"))
       {
         g_PrmED210 = PRMED210.LSet_A_PrmED210(g_Msg.Msg_Dat.toString());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp734_g_PrmPC080");
         data.getUser().removeTemp("Agtp734_g_PrmPC080");
         if (g_PrmED210.ED210_Rtn.toString().equals("NK")
          || g_PrmED210.ED210_Rtn.toString().equals("YA"))
            {
              rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
              BF_MSG.Param_Program(data, g_Msg);
              setTemplate(data, "Garantias,Agt,AGTP734.vm" );
              return;
            }
         else
            {
              if (g_PrmPC080.PC080_Amv.toString().equals("D"))
                 {
                   g_Img_GtTsd = (GT_IMG.Buf_Img_GtTsd)data.getUser().getTemp("Agtp734_ImgGtTsd");
                   g_MsgED135 = MSGED135.Inicia_MsgED135();
                   rg.MoverA(g_MsgED135.ED135_Idr, "X");
                   rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
                   rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
                   rg.MoverA(g_MsgED135.ED135_Est, "");
                   rg.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
                   rg.MoverA(g_MsgED135.ED135_Ldv, g_PrmED210.ED210_Ldt);
                   rg.MoverA(g_MsgED135.ED135_Ezt, "RETSD");
                   rg.MoverA(g_MsgED135.ED135_Exi, "TSD");
                   rg.MoverA(g_MsgED135.ED135_Exn, rg.FmtValor(g_Img_GtTsd.GtTsd_Tsd,0,0,12,"+"));
                   rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
                   g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
                   data.getUser().removeTemp("Agtp734_MsgED135");
                   data.getUser().removeTemp("Agtp734_vImgGtUvd");
                   data.getUser().removeTemp("Agtp734_ImgGtSte");
                   data.getUser().removeTemp("Agtp734_ImgGtTsd");
                   data.getUser().removeTemp("Agtp734_ImgBase");
                   data.getUser().removeTemp("Agtp734_ImgGtApe");
                   data.getUser().removeTemp("Agtp734_PrmPC080");
                   if (g_Msg.Msg_Rtn.toString().compareTo("0")!=0)
                      {
                        data.getUser().removeTemp("Agtp734_g_PrmPC080");
                        data.getUser().setTemp("Agtp734_g_PrmPC080", g_PrmPC080);
                        BF_MSG.MsgInt("Agtp734", data, g_Msg, "Msg_Rtn", "1");
                        return;
                      }
                   rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
                   rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
                   BF_MSG.Return_Data("Agtp734", data, g_Msg);
                   return;
                 }
              if (g_PrmPC080.PC080_Amv.toString().equals("P"))
                 {
                   g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp734_ImgGtApe");
                   g_Img_GtTsd = (GT_IMG.Buf_Img_GtTsd)data.getUser().getTemp("Agtp734_ImgGtTsd");
                   g_Img_GtSte = (GT_IMG.Buf_Img_GtSte)data.getUser().getTemp("Agtp734_ImgGtSte");
                   v_Img_GtUvd = (Vector)data.getUser().getTemp("Agtp734_vImgGtUvd");
                   Vector g_Tab_Img = A_Tab_Img(data);
                   img.Graba_Img_Host(g_Msg, g_Tab_Img, data, context);
                   g_MsgED135 = MSGED135.Inicia_MsgED135();
                   rg.MoverA(g_MsgED135.ED135_Idr, "X");
                   rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
                   rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
                   rg.MoverA(g_MsgED135.ED135_Est, "");
                   rg.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
                   rg.MoverA(g_MsgED135.ED135_Ldv, g_PrmED210.ED210_Ldt);
                   rg.MoverA(g_MsgED135.ED135_Ezt, "PDTSD");
                   rg.MoverA(g_MsgED135.ED135_Exi, "TSD");
                   rg.MoverA(g_MsgED135.ED135_Exn, rg.FmtValor(g_Img_GtTsd.GtTsd_Tsd,0,0,12,"+"));
                   rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
                   g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
                   data.getUser().removeTemp("Agtp734_MsgED135");
                   data.getUser().removeTemp("Agtp734_vImgGtUvd");
                   data.getUser().removeTemp("Agtp734_ImgGtSte");
                   data.getUser().removeTemp("Agtp734_ImgGtTsd");
                   data.getUser().removeTemp("Agtp734_ImgBase");
                   data.getUser().removeTemp("Agtp734_ImgGtApe");
                   data.getUser().removeTemp("Agtp734_PrmPC080");
                   if (g_Msg.Msg_Rtn.toString().compareTo("0")!=0)
                      {
                        data.getUser().removeTemp("Agtp734_g_PrmPC080");
                        data.getUser().setTemp("Agtp734_g_PrmPC080", g_PrmPC080);
                        BF_MSG.MsgInt("Agtp734", data, g_Msg, "Msg_Rtn", "1");
                        return;
                      }
                   rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
                   rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
                   BF_MSG.Return_Data("Agtp734", data, g_Msg);
                   return;
                 }
            }
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Agtp291"))
       {
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp734_g_PrmPC080");
         data.getUser().removeTemp("Agtp734_g_PrmPC080");
         data.getUser().removeTemp("Agtp734_MsgED135");
         data.getUser().removeTemp("Agtp734_vImgGtUvd");
         data.getUser().removeTemp("Agtp734_ImgGtSte");
         data.getUser().removeTemp("Agtp734_ImgGtTsd");
         data.getUser().removeTemp("Agtp734_ImgBase");
         data.getUser().removeTemp("Agtp734_ImgGtApe");
         data.getUser().removeTemp("Agtp734_PrmPC080");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp734", data, g_Msg);
         return;
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Appp030"))
       {
         g_PrmPP030 = PRMPP030.LSet_A_PrmPP030(g_Msg.Msg_Dat.toString());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp734_g_PrmPC080");
         data.getUser().removeTemp("Agtp734_g_PrmPC080");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         if (g_PrmPP030.PP030_Rsp.toString().trim().equals("TERMINAR"))
            {
              data.getUser().removeTemp("Agtp734_MsgED135");
              data.getUser().removeTemp("Agtp734_vImgGtUvd");
              data.getUser().removeTemp("Agtp734_ImgGtSte");
              data.getUser().removeTemp("Agtp734_ImgGtTsd");
              data.getUser().removeTemp("Agtp734_ImgBase");
              data.getUser().removeTemp("Agtp734_ImgGtApe");
              data.getUser().removeTemp("Agtp734_PrmPC080");
              BF_MSG.Return_Data("Agtp734", data, g_Msg);
              return;
            }
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP734.vm" );
         return;
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Agtp214")
     || rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Appp096"))
       {
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp734_g_PrmPC080");
         data.getUser().removeTemp("Agtp734_g_PrmPC080");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP734.vm" );
         return;
       }
  }
  //---------------------------------------------------------------------------------------
  public void Recalcula_GtApe(RunData data, Context context)
              throws Exception
  {
    String lcVtt = rg.Zeros(15);
    String lcVts = rg.Zeros(15);
    String lcVcn = rg.Zeros(15);
    for (i=0; i<v_Img_GtUvd.size(); i++)
        {
          g_Img_GtHpd = (GT_IMG.Buf_Img_GtHpd)v_Img_GtUvd.elementAt(i);
          //if (g_Img_GtHpd.GtHpd_Vtu.toString().trim().equals(""))
          //   { rg.MoverA(g_Img_GtHpd.GtHpd_Vtu, g_Img_GtHpd.GtHpd_Vtu); }
          //if (g_Img_GtHpd.GtHpd_Vts.toString().trim().equals(""))
          //   { rg.MoverA(g_Img_GtHpd.GtHpd_Vts, rg.Multiplica(g_Img_GtHpd.GtHpd_Vtu,4,g_Img_GtTsd.GtTsd_Vuf,2,15,2,0)); }
          //if (g_Img_GtHpd.GtHpd_Vcn.toString().trim().equals(""))
          //   { rg.MoverA(g_Img_GtHpd.GtHpd_Vcn, g_Img_GtHpd.GtHpd_Vts); }
          lcVtt = rg.Suma(lcVtt,4,g_Img_GtHpd.GtHpd_Vtu,4,15,4);
          lcVts = rg.Suma(lcVts,2,g_Img_GtHpd.GtHpd_Vts,2,15,2);
          lcVcn = rg.Suma(lcVcn,2,g_Img_GtHpd.GtHpd_Vcn,2,15,2);
        }
    rg.MoverA(g_Img_GtApe.GtApe_Vtt, lcVtt);
    rg.MoverA(g_Img_GtApe.GtApe_Vts, lcVts);
    rg.MoverA(g_Img_GtApe.GtApe_Vcn, lcVcn);
    rg.MoverA(g_Img_GtApe.GtApe_Tsd, g_Img_GtTsd.GtTsd_Tsd);
    rg.MoverA(g_Img_GtApe.GtApe_Fts, g_Img_GtTsd.GtTsd_Fts);
//    rg.MoverA(g_Img_GtApe.GtApe_Ftv, g_Img_GtTsd.GtTsd_Ftv);
  }
  //---------------------------------------------------------------------------------------
  public boolean PagoTasador(RunData data, Context context)
              throws Exception
  {
    g_Img       = img.Inicia_Img();
    rg.MoverA(g_Img.Img_Ntr, Asigna_Folio("TRN-NTR", data, context));
    rg.MoverA(g_Img.Img_Dax, "BASE");
    rg.MoverA(g_Img.Img_Seq, "000");
    //------------------------------------------------------------------------------
    g_Img_Base = img.LSet_A_ImgBase(g_Img_GtApe.GtApe_Bse.toString());
    rg.MoverA(g_Img_Base.Img_Base_Ntr, g_Img.Img_Ntr);
    rg.MoverA(g_Img_Base.Img_Base_Ttr, "PGTSD");
    rg.MoverA(g_Img_Base.Img_Base_Ntt, "PAGO A TASADORES");
    //------------------------------------------------------------------------------
    rg.MoverA(g_Trn.Trn_Ntr, g_Img.Img_Ntr);
    rg.MoverA(g_Trn.Trn_Trm, "PCVB");
    rg.MoverA(g_Trn.Trn_Fem, g_Msg.Msg_Fch);
    rg.MoverA(g_Trn.Trn_Hrm, rg.Zeros(6));
    rg.MoverA(g_Trn.Trn_Ttr, g_Img_Base.Img_Base_Ttr);
    rg.MoverA(g_Trn.Trn_Cli, g_Img_Base.Img_Base_Cli);
    rg.MoverA(g_Trn.Trn_Cnr, g_Img_Base.Img_Base_Sis.toString() + g_Img_Base.Img_Base_Ncn.toString());
    rg.MoverA(g_Trn.Trn_Suc, g_Img_Base.Img_Base_Suc);
    rg.MoverA(g_Trn.Trn_Eje, g_Img_Base.Img_Base_Eje);
    rg.MoverA(g_Trn.Trn_Est, "ENPRO");
    rg.MoverA(g_Trn.Trn_Usr, g_Msg.Msg_Cus);
    rg.MoverA(g_Trn.Trn_Cja, rg.Zeros(4));
    rg.MoverA(g_Trn.Trn_Stf, rg.Zeros(9));
    rg.MoverA(g_Trn.Trn_Dcn, g_Img_Base.Img_Base_Dcn);
    rg.MoverA(g_Trn.Trn_Tmn, g_Img_Base.Img_Base_Tmn);
    rg.MoverA(g_Trn.Trn_Trj, g_Img_Base.Img_Base_Trj);
    rg.MoverA(g_Trn.Trn_Dcm, g_Img_Base.Img_Base_Dcm);
    rg.MoverA(g_Trn.Trn_Mnd, g_Img_Base.Img_Base_Mnd);
    rg.MoverA(g_Trn.Trn_Mnt, rg.Zeros(15));
    //------------------------------------------------------------------------------
    g_Img_GtPts = gtimg.LSet_A_ImgGtPts(g_Img.Img_Dat.toString());
    rg.MoverA(g_Img_GtPts.GtPts_Bse, img.LSet_De_ImgBase_Bse(g_Img_Base));
    rg.MoverA(g_Img_GtPts.GtPts_Fec, g_Msg.Msg_Fch);
    rg.MoverA(g_Img_GtPts.GtPts_Cbp, g_Img_GtApe.GtApe_Cbp);
    rg.MoverA(g_Img_GtPts.GtPts_Dbp, g_Img_GtApe.GtApe_Dbp);
    rg.MoverA(g_Img_GtPts.GtPts_Gts, g_Img_GtTsd.GtTsd_Vst);
    rg.MoverA(g_Img_GtPts.GtPts_Hon, g_Img_GtTsd.GtTsd_Hon);
    rg.MoverA(g_Img_GtPts.GtPts_Pcb, g_Img_GtTsd.GtTsd_Pcb);
    rg.MoverA(g_Img_GtPts.GtPts_Cct, g_Img_GtTsd.GtTsd_Cct);
    rg.MoverA(g_Img_GtPts.GtPts_Bof, g_Img_GtTsd.GtTsd_Bof);
    rg.MoverA(g_Img_GtPts.GtPts_Nbf, g_Img_GtTsd.GtTsd_Nbf);
    rg.MoverA(g_Img_GtPts.GtPts_Nts, g_Img_GtTsd.GtTsd_Nts);
    rg.MoverA(g_Img_GtPts.GtPts_Rts, g_Img_GtTsd.GtTsd_Rts);
    rg.MoverA(g_Img_GtPts.GtPts_Dpc, g_Img_GtSte.GtSte_Dpc);
    rg.MoverA(g_Img_GtPts.GtPts_Fll, "");
    rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtPts(g_Img_GtPts));
    //------------------------------------------------------------------------------
    rg.MoverA(g_MsgTxCur.TxCur_Bco, "DEMO");                                //"BICE"
    rg.MoverA(g_MsgTxCur.TxCur_Cic, "NO");
    rg.MoverA(g_MsgTxCur.TxCur_Lin, " ");
    rg.MoverA(g_MsgTxCur.TxCur_Rsp, " ");
    rg.MoverA(g_MsgTxCur.TxCur_Idc, "00");
    rg.MoverA(g_MsgTxCur.TxCur_Stp, " ");
    rg.MoverA(g_MsgTxCur.TxCur_Imr, " ");
    rg.MoverA(g_MsgTxCur.TxCur_Msg, " ");
    rg.MoverA(g_MsgTxCur.TxCur_Trn, BF_TRN.LSet_De_Trn(g_Trn));
    rg.MoverA(g_MsgTxCur.TxCur_Img, img.LSet_De_Img(g_Img));
    rg.MoverA(g_Msg.Msg_Dat, MSGTXCUR.LSet_De_MsgTxCur(g_MsgTxCur));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GTCUR");
    if (g_Msg.Msg_Rtn.toString().compareTo("0")!=0)
       { return false; }
    g_MsgTxCur = MSGTXCUR.LSet_A_MsgTxCur(g_Msg.Msg_Dat.toString());
    return true;
  }
  //---------------------------------------------------------------------------------------
  public String Asigna_Folio(String pcFolio, RunData data, Context context)
                throws Exception
  {
    g_MsgED090 = MSGED090.Inicia_MsgED090();
    rg.MoverA(g_MsgED090.ED090_Idr, "F");
    rg.MoverA(g_MsgED090.ED090_Cod, pcFolio);
    rg.MoverA(g_Msg.Msg_Dat, MSGED090.LSet_De_MsgED090(g_MsgED090));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED090");
    g_MsgED090 = MSGED090.LSet_A_MsgED090(g_Msg.Msg_Dat.toString());
    String lcFolio = g_MsgED090.ED090_Fol.toString();
    return lcFolio;
  }
  //---------------------------------------------------------------------------------------
  public void File_Banco(RunData data, Context context)
              throws Exception
  {
    FileItem fi = data.getParameters().getFileItem("Archivo");
    String Archivo = fi.getName();
    long largo = fi.getSize();
    InputStream in = fi.getStream();
    BufferedReader bf = new BufferedReader(new InputStreamReader(in));
    try
      {
        String linea = "";
        while(linea!=null)
          {
            linea = bf.readLine();
            if (linea!=null)
               {
                 int p = 0;
                 int ceros = 0;
                 String Rl1 = Parsea("N", linea, p);
                 p = p + Rl1.length() + 1;
                 String Rl2 = Parsea("N", linea, p);
                 p = p + Rl2.length() + 1;
                 String Dsc = Parsea("C", linea, p);
                 p = p + Dsc.length() + 1;
                 ceros = ParseaDec("D", linea, p);
                 String Mt2 = Parsea("D", linea, p);
                 if (ceros == 0)
                    {
                      p = p + Mt2.length() + 2;
                    }
                 if (ceros == 1)
                    {
                      p = p + Mt2.length() + 2;
                      Mt2 = Mt2 + "0";
                    }
                 if (ceros == 2)
                    {
                      p = p + Mt2.length() + 1;
                      Mt2 = Mt2 + "00";
                    }
                 ceros = ParseaDec("D", linea, p);
                 String Vuf = Parsea("D", linea, p);
                 if (ceros == 2)
                    { Vuf = Vuf + "00"; }
                 if (ceros == 1)
                    { Vuf = Vuf + "0"; }
                 g_Img_GtHpd = gtimg.LSet_A_ImgGtHpd(rg.Blancos(827));
                 rg.MoverA(g_Img_GtHpd.GtHpd_Seq, rg.Suma(g_Img_GtApe.GtApe_Tfr,0,"1",0,3,0));
                 rg.MoverA(g_Img_GtHpd.GtHpd_Rs1, rg.FmtValor(Rl1,0,0,5,"+"));
                 rg.MoverA(g_Img_GtHpd.GtHpd_Rs2, rg.FmtValor(Rl2,0,0,3,"+"));
                 rg.MoverA(g_Img_GtHpd.GtHpd_Str, Dsc);
                 rg.MoverA(g_Img_GtHpd.GtHpd_Udd, rg.FmtValor(Mt2,2,2,9,"+"));
                 rg.MoverA(g_Img_GtHpd.GtHpd_Umd, "M2");
                 rg.MoverA(g_Img_GtHpd.GtHpd_Umt, "U");
                 rg.MoverA(g_Img_GtHpd.GtHpd_Vtu, rg.FmtValor(Vuf,2,4,15,"+"));
                 rg.MoverA(g_Img_GtHpd.GtHpd_Pum, rg.Divide(g_Img_GtHpd.GtHpd_Vtu,4,g_Img_GtHpd.GtHpd_Udd,2,15,4,4));
                 rg.MoverA(g_Img_GtHpd.GtHpd_Vbu, g_Img_GtHpd.GtHpd_Vtu);
                 rg.MoverA(g_Img_GtHpd.GtHpd_Vlu, g_Img_GtHpd.GtHpd_Vtu);
                 rg.MoverA(g_Img_GtHpd.GtHpd_Vsu, g_Img_GtHpd.GtHpd_Vtu);
                 rg.MoverA(g_Img_GtHpd.GtHpd_Vts, rg.Multiplica(g_Img_GtHpd.GtHpd_Vtu,4,g_Img_GtTsd.GtTsd_Vuf,2,15,2,0));
                 rg.MoverA(g_Img_GtHpd.GtHpd_Vlq, g_Img_GtHpd.GtHpd_Vts);
                 rg.MoverA(g_Img_GtHpd.GtHpd_Vtb, g_Img_GtHpd.GtHpd_Vts);
                 rg.MoverA(g_Img_GtHpd.GtHpd_Vcn, g_Img_GtHpd.GtHpd_Vts);
                 rg.MoverA(g_Img_GtHpd.GtHpd_Vsg, g_Img_GtHpd.GtHpd_Vts);
                 v_Img_GtUvd.add(g_Img_GtHpd);
                 rg.MoverA(g_Img_GtApe.GtApe_Tfr, rg.Suma(g_Img_GtApe.GtApe_Tfr,0,"1",0,3,0));
               }
          }
      }
    catch (EOFException  e)
      {
        Log.debug("AGTP734.EOFException: [" + e.toString() + "]");
      }
    bf.close();
  }
  //---------------------------------------------------------------------------------------
  public static int ParseaDec(String tipo, String linea, int p)
  {
    String dato = "";
    int ceros = 0;
    int punto = 0;
    int dec = 0;
    for (int j=p; j<linea.length(); j++)
        {
          char chr = linea.charAt(j);
          if (chr == ';')
             { break; }
          if (chr == ',')
             { punto = 1; }
          else
             {
               if (chr != '.')
                  {
                    dato = dato + chr;
                    if (punto==1)
                       { dec = dec + 1; }
                  }
             }
        }
    if (tipo.equals("D"))
       {
         if (dec == 0)
            { ceros = 2; }
         if (dec == 1)
            { ceros = 1; }
       }
    return ceros;
  }
  //---------------------------------------------------------------------------------------
  public static String Parsea(String tipo, String linea, int p)
  {
    String dato = "";
    int punto = 0;
    int dec = 0;
    for (int j=p; j<linea.length(); j++)
        {
          char chr = linea.charAt(j);
          if (chr == ';')
             { break; }
          if (tipo.equals("C"))
             { dato = dato + chr; }
          else
             {
                if (chr == ',')
                   { punto = 1; }
                else
                   {
                     if (chr != '.')
                        {
                          dato = dato + chr;
                          if (punto==1)
                             { dec = dec + 1; }
                        }
                   }
              }
        }
    if (tipo.equals("C"))
       { dato = dato.toUpperCase(); }
    return dato;
  }
  //---------------------------------------------------------------------------------------
}