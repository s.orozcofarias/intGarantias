// Source File Name:   Agtp200.java
// Descripcion     :   Eventos en Tramite [Tasador] (ED135, GT200)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.PRMPC080;

import com.intGlobal.modules.global.MSGED135;
import com.intGlobal.modules.global.PRMED210;

import com.intGarantias.modules.global.MSGGT200;

import java.util.*;

import org.apache.turbine.services.resources.TurbineResources;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

import java.security.*;
import javax.crypto.Cipher; 
import javax.crypto.spec.SecretKeySpec;
import sun.misc.*;
public class Agtp200 extends FHTServletAction
{
  //---------------------------------------------------------------------------------------
  public int    i = 0;
  public String lcRetorno = "";
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  PRMPC080.PrmPC080 g_PrmPC080     = new PRMPC080.PrmPC080();
  MSGED135.Buf_MsgED135 g_MsgED135 = new MSGED135.Buf_MsgED135();
  MSGGT200.Buf_MsgGT200 g_MsgGT200 = new MSGGT200.Buf_MsgGT200();
  PRMED210.Buf_PrmED210 g_PrmED210 = new PRMED210.Buf_PrmED210();
  
	private static final String RETPI = "RETPI";
	private static final String TERPI = "TERPI";
	private static final String ALGORITHM = "AES";
  //---------------------------------------------------------------------------------------
  public Agtp200()
  {
  }
  //---------------------------------------------------------------------------------------
  public void doPerform(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP200[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp200-DIN", data);
    if (g_Msg.Msg_Pgr.toString().equals("RT"))
       { doAgtp200_Continue(data, context); }
    else
       { doAgtp200_Init(data, context); }
  }
  //---------------------------------------------------------------------------------------
  public void doCancel(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP200[doCancel.start]", "[CANCELADO]");
    setTemplate(data, "Garantias,Agt,AGTP200.vm");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp200_Init(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP200[doAgtp200_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    g_MsgGT200 = MSGGT200.Inicia_MsgGT200();
    rg.MoverA(g_MsgGT200.GT200_Idr, "I");
    rg.MoverA(g_MsgGT200.GT200_Tsd, g_PrmPC080.PC080_Tsd);
    Poblar_Panel(data, context, "I");
    setTemplate(data, "Garantias,Agt,AGTP200.vm" );
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp200(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP200[doAgtp200.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp200-MAN", data);
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    String Opc = data.getParameters().getString("Opc");
    if  (Opc.equals("S"))
        {
          //Salir
          data.getUser().removeTemp("Agtp200_Msg");
          data.getUser().removeTemp("Agtp200_MsgGT200");
          //BF_MSG.Return_Program(data, g_Msg);
          BF_MSG.Return_Data("Agtp200", data, g_Msg);
          return;
        }
    g_MsgGT200 = (MSGGT200.Buf_MsgGT200)data.getUser().getTemp("Agtp200_MsgGT200");
    MSGGT200.Bff_MsgGT200 d_MsgGT200 = new MSGGT200.Bff_MsgGT200();
    if  (Opc.equals("O"))
        {
          //Aceptar OK
          String Seq = data.getParameters().getString("Seq", "");
          i = Integer.parseInt(Seq);
          d_MsgGT200 = (MSGGT200.Bff_MsgGT200)g_MsgGT200.GT200_Tab.elementAt(i);
          if (d_MsgGT200.Tmt_Est.toString().compareTo("ENTSD")!=0)
             {
               data.getUser().removeTemp("Agtp200_g_PrmPC080");
               data.getUser().setTemp("Agtp200_g_PrmPC080", g_PrmPC080);
               BF_MSG.MsgInt("Agtp200", data, g_Msg, "Ya Est� Aceptado", "1"); 
      	       return;
             }
          rg.MoverA(g_PrmPC080.PC080_Rtn,     "RA");
          rg.MoverA(g_PrmPC080.PC080_Cli,     d_MsgGT200.Evt_Cli);
          rg.MoverA(g_PrmPC080.PC080_Ncl,     d_MsgGT200.Evt_Ncl);
          rg.MoverA(g_PrmPC080.PC080_Cnr.Sis, d_MsgGT200.Evt_Sis);
          rg.MoverA(g_PrmPC080.PC080_Cnr.Ncn, d_MsgGT200.Evt_Ncn);
          rg.MoverA(g_PrmPC080.PC080_Dcn,     d_MsgGT200.Evt_Dcn);
          rg.MoverA(g_PrmPC080.PC080_Ndc,     d_MsgGT200.Evt_Dsc);
          rg.MoverA(g_PrmPC080.PC080_Ntr,     d_MsgGT200.Tmt_Nev);
          rg.MoverA(g_PrmPC080.PC080_Ttr,     d_MsgGT200.Evt_Ttr);
          rg.MoverA(g_PrmPC080.PC080_Trt,     d_MsgGT200.Tmt_Trt);
          rg.MoverA(g_PrmPC080.PC080_Mnd,     d_MsgGT200.Evt_Mnd);
          rg.MoverA(g_PrmPC080.PC080_Mnt,     d_MsgGT200.Evt_Vle);
          rg.MoverA(g_PrmPC080.PC080_Trj,     d_MsgGT200.Evt_Trj);
          rg.MoverA(g_PrmPC080.PC080_Dcm,     d_MsgGT200.Evt_Dcm);
          rg.MoverA(g_PrmPC080.PC080_Ggr,     " ");
          rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
          BF_MSG.Link_Program("Agtp200", "Agtp214", data, g_Msg); 
          if (lcRetorno.trim().compareTo("")!=0)
             {
               data.getUser().removeTemp("Agtp200_g_PrmPC080");
               data.getUser().setTemp("Agtp200_g_PrmPC080", g_PrmPC080);
               BF_MSG.MsgInt("Agtp200", data, g_Msg, lcRetorno, "1"); 
      	       return;
             }
          return;
        }
    if  (Opc.equals("A"))
        {
          //Accion
          String Seq = data.getParameters().getString("Seq", "");
          i = Integer.parseInt(Seq);
          d_MsgGT200 = (MSGGT200.Bff_MsgGT200)g_MsgGT200.GT200_Tab.elementAt(i);
          if (d_MsgGT200.Tmt_Est.toString().compareTo("RGTSD")!=0
           && d_MsgGT200.Tmt_Est.toString().compareTo("PDTSD")!=0
           && d_MsgGT200.Tmt_Est.toString().compareTo("RETSD")!=0)
             {
               data.getUser().removeTemp("Agtp200_g_PrmPC080");
               data.getUser().setTemp("Agtp200_g_PrmPC080", g_PrmPC080);
               BF_MSG.MsgInt("Agtp200", data, g_Msg, "Antes Debe Aceptarlo", "1"); 
      	       return;
             }
          rg.MoverA(g_PrmPC080.PC080_Rtn,     "WR");
          rg.MoverA(g_PrmPC080.PC080_Cli,     d_MsgGT200.Evt_Cli);
          rg.MoverA(g_PrmPC080.PC080_Ncl,     d_MsgGT200.Evt_Ncl);
          rg.MoverA(g_PrmPC080.PC080_Cnr.Sis, d_MsgGT200.Evt_Sis);
          rg.MoverA(g_PrmPC080.PC080_Cnr.Ncn, d_MsgGT200.Evt_Ncn);
          rg.MoverA(g_PrmPC080.PC080_Dcn,     d_MsgGT200.Evt_Dcn);
          rg.MoverA(g_PrmPC080.PC080_Ndc,     d_MsgGT200.Evt_Dsc);
          rg.MoverA(g_PrmPC080.PC080_Ntr,     d_MsgGT200.Tmt_Nev);
          rg.MoverA(g_PrmPC080.PC080_Ttr,     d_MsgGT200.Evt_Ttr);
          rg.MoverA(g_PrmPC080.PC080_Trt,     d_MsgGT200.Tmt_Trt);
          rg.MoverA(g_PrmPC080.PC080_Mnd,     d_MsgGT200.Evt_Mnd);
          rg.MoverA(g_PrmPC080.PC080_Mnt,     d_MsgGT200.Evt_Vle);
          rg.MoverA(g_PrmPC080.PC080_Trj,     d_MsgGT200.Evt_Trj);
          rg.MoverA(g_PrmPC080.PC080_Dcm,     d_MsgGT200.Evt_Dcm);
          rg.MoverA(g_PrmPC080.PC080_Ggr,     " ");
          rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));//rg.NormaPgm(d_MsgGT200.Tmt_Pgm)
          String url = TurbineResources.getString("MT.url.informe");
          if(url!=null){
        	  String key=TurbineResources.getString("kib.modtas.key");
			  String iv=TurbineResources.getString("kib.modtas.iv");
			  Log.debug("AGTP200 key ", key);

        	  url = setUrlAction(url,key,iv);
        	  
        	  url = url.replaceAll("VARGAR", encrypt(g_PrmPC080.getNcn(),key));
			  url = url.replaceAll("VAREVENT", encrypt(g_PrmPC080.getNtr(),key));
			  url = url.replaceAll("NUSR", encrypt(data.getUser().getPermStorage().get("LOGIN_NAME").toString(),key) );
        	  Log.debug("AGTP200 LINK TO 203", url);
          }else{
        	  url="http://10.110.3.21:8003/web-kib-primefaces/ModTasacionServlet?action=index&nGarantia=1083067&nEvent=596165&numeroUsuario=249&rolUsuario=GarTasador";
        	  Log.debug("AGTP200 LINK TO 203 FIJA ", url);
              
          }
          data.getUser().setTemp("Agtp203_UrlMT", url);
          BF_MSG.Link_Program("Agtp200", "Agtp203", data, g_Msg); 
          if (lcRetorno.trim().compareTo("")!=0)
             {
               data.getUser().removeTemp("Agtp200_g_PrmPC080");
               data.getUser().setTemp("Agtp200_g_PrmPC080", g_PrmPC080);
               BF_MSG.MsgInt("Agtp200", data, g_Msg, lcRetorno, "1"); 
      	       return;
             }
       	  return;
        }
    if  (Opc.equals("D"))
        {
          //Devolver
          String Seq = data.getParameters().getString("Seq", "");
          i = Integer.parseInt(Seq);
          d_MsgGT200 = (MSGGT200.Bff_MsgGT200)g_MsgGT200.GT200_Tab.elementAt(i);
          if (d_MsgGT200.Tmt_Est.toString().compareTo("ENTSD")!=0)
             {
               data.getUser().removeTemp("Agtp200_g_PrmPC080");
               data.getUser().setTemp("Agtp200_g_PrmPC080", g_PrmPC080);
               BF_MSG.MsgInt("Agtp200", data, g_Msg, "Ya No Puede Devolver; Deber Terminarlo", "1"); 
      	       return;
             }
          rg.MoverA(g_PrmPC080.PC080_Ntr, d_MsgGT200.Tmt_Nev);
          rg.MoverA(g_PrmPC080.PC080_Trt, d_MsgGT200.Tmt_Trt);
          g_PrmED210 = PRMED210.Inicia_PrmED210();
          rg.MoverA(g_PrmED210.ED210_Rtn, "RW");
          data.getUser().removeTemp("Agtp200_g_PrmPC080");
          data.getUser().setTemp("Agtp200_g_PrmPC080", g_PrmPC080);
          rg.MoverA(g_Msg.Msg_Dat, PRMED210.LSet_De_PrmED210(g_PrmED210));
          BF_MSG.Link_Program("Agtp200", "Aedp210", data, g_Msg); 
          if (lcRetorno.trim().compareTo("")!=0)
             {
               data.getUser().removeTemp("Agtp200_g_PrmPC080");
               data.getUser().setTemp("Agtp200_g_PrmPC080", g_PrmPC080);
               BF_MSG.MsgInt("Agtp200", data, g_Msg, lcRetorno, "1"); 
      	       return;
             }
       	  return;
        }
    if  (Opc.equals("N"))
        {	
          //Siguiente
          Poblar_Panel(data, context, "S");
          setTemplate(data, "Garantias,Agt,AGTP200.vm" );
          return;
        }
    if  (Opc.equals("P"))
        {	
          //Previo
          Poblar_Panel(data, context, "P");
          setTemplate(data, "Garantias,Agt,AGTP200.vm" );
          return;
        }		
  }
  
  private String setUrlAction(String url,String key, String iv) {
		
		if(TERPI.equalsIgnoreCase(g_PrmPC080.PC080_Ttr.toString())){
			  try {
				url=url.replaceAll("VARACTION", encrypt("termProyecto",key));
			} catch (Exception e) {
				e.printStackTrace();
			}finally{
			  url=url.replaceAll("VARACTION", "termProyecto");
			}
		  }else if(
			  RETPI.equalsIgnoreCase(g_PrmPC080.PC080_Ttr.toString())){
			  try {
				url=url.replaceAll("VARACTION", encrypt("estAvance",key));
			} catch (Exception e) {
				e.printStackTrace();
			}finally{
			  url=url.replaceAll("VARACTION", "estAvance");
			}
		  }
		  else{
			  try {
				url=url.replaceAll("VARACTION", encrypt("index",key));
			} catch (Exception e) {
				e.printStackTrace();
			}finally{
			  url=url.replaceAll("VARACTION", "index");
			}
		  }
		return url;
	}
  public static String encrypt(String valueToEnc,String keyProp) throws Exception {
      Key key = generateKey(keyProp);
      Cipher c = Cipher.getInstance(ALGORITHM);
      c.init(Cipher.ENCRYPT_MODE, key);
      byte[] encValue = c.doFinal(valueToEnc.getBytes("UTF-8"));
      String encryptedValue = new BASE64Encoder().encode(encValue);
      return encryptedValue;
  }
private static Key generateKey(String keyProp) throws Exception {
      Key key = new SecretKeySpec(keyProp.getBytes("UTF-8"), ALGORITHM);
      return key;
	}
  
  //---------------------------------------------------------------------------------------
  public void Poblar_Panel(RunData data, Context context, String pcIdr)
              throws Exception
  {
    MSGGT200.Bff_MsgGT200 d_MsgGT200 = new MSGGT200.Bff_MsgGT200();
    if (pcIdr.equals("E"))
       {
         g_MsgGT200 = (MSGGT200.Buf_MsgGT200)data.getUser().getTemp("Agtp200_MsgGT200");
         i = 0;
       }
    if (pcIdr.equals("S"))
       { i = Integer.parseInt(g_MsgGT200.GT200_Nro.toString())-1; }
    if (pcIdr.equals("P"))
       { i = 0; }
    if (pcIdr.compareTo("I")!=0)
       {
         String lcTsd = g_MsgGT200.GT200_Tsd.toString();
         String lcNts = g_MsgGT200.GT200_Nts.toString();
         d_MsgGT200 = (MSGGT200.Bff_MsgGT200)g_MsgGT200.GT200_Tab.elementAt(i);
         g_MsgGT200 = MSGGT200.Inicia_MsgGT200();
         rg.MoverA(g_MsgGT200.GT200_Idr, pcIdr);
         rg.MoverA(g_MsgGT200.GT200_Tsd, lcTsd);
         rg.MoverA(g_MsgGT200.GT200_Nts, lcNts);
         g_MsgGT200.GT200_Tab.set(0, d_MsgGT200);
       }
    rg.MoverA(g_Msg.Msg_Dat, MSGGT200.LSet_De_MsgGT200(g_MsgGT200));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GT200");
    g_MsgGT200 = MSGGT200.LSet_A_MsgGT200(g_Msg.Msg_Dat.toString());
    data.getUser().removeTemp("Agtp200_MsgGT200");
    data.getUser().setTemp("Agtp200_MsgGT200", g_MsgGT200);
    data.getUser().removeTemp("Agtp200_Msg");
    data.getUser().setTemp("Agtp200_Msg", g_Msg);
  }
//---------------------------------------------------------------------------------------
  public void doAgtp200_Continue(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP200[doAgtp200_Continue.start]", "[" + data.getUser().getUserName() + "]");
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Appp030"))
       {
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp200_g_PrmPC080");
         data.getUser().removeTemp("Agtp200_g_PrmPC080");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP200.vm" );
         return;
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Aedp210"))
       {
         g_PrmED210 = PRMED210.LSet_A_PrmED210(g_Msg.Msg_Dat.toString());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp200_g_PrmPC080");
         data.getUser().removeTemp("Agtp200_g_PrmPC080");
         if (g_PrmED210.ED210_Rtn.toString().equals("OK"))
            {
              g_MsgED135 = MSGED135.Inicia_MsgED135();
              rg.MoverA(g_MsgED135.ED135_Idr, "X");
              rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
              rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
              rg.MoverA(g_MsgED135.ED135_Est, "");
              rg.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
              rg.MoverA(g_MsgED135.ED135_Ldv, g_PrmED210.ED210_Ldt);
              rg.MoverA(g_MsgED135.ED135_Ezt, "DVTSD");
              rg.MoverA(g_MsgED135.ED135_Exi, "TSD");
              rg.MoverA(g_MsgED135.ED135_Exn, rg.Zeros(g_MsgED135.ED135_Exn));
              rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
              g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
              Poblar_Panel(data, context, "E");
            }
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP200.vm" );
         return;
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Agtp214"))
       {
         g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
         if (g_PrmPC080.PC080_Rtn.toString().equals("OK"))
            {
              g_MsgED135 = MSGED135.Inicia_MsgED135();
              rg.MoverA(g_MsgED135.ED135_Idr, "M");
              rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
              rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
              rg.MoverA(g_MsgED135.ED135_Est, "");
              rg.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
              rg.MoverA(g_MsgED135.ED135_Ezt, "RGTSD");
              rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
              g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
              Poblar_Panel(data, context, "E");
            }
         setTemplate(data, "Garantias,Agt,AGTP200.vm" );
         return;
       }
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    if (g_PrmPC080.PC080_Rtn.toString().equals("OK"))
       {
         Poblar_Panel(data, context, "E");
       }
    setTemplate(data, "Garantias,Agt,AGTP200.vm" );
    return;
  }
}