// Source File Name:   Agtp070.java

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;

import java.util.*;
import org.apache.turbine.util.*;
import org.apache.velocity.context.Context;

public class Agtp070 extends FHTServletAction
{
  //---------------------------------------------------------------------------------------
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  //---------------------------------------------------------------------------------------
  public Agtp070()
  {
  }
  //---------------------------------------------------------------------------------------
  public void doPerform(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP070[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp070-DIN", data);
    doAgtp070_Init(data, context);
  }
  //---------------------------------------------------------------------------------------
  public void doCancel(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP070[doCancel.start]", "[CANCELADO]");
    setTemplate(data, "Control,AGTP070.vm");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp070_Init(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP070[doAgtp070_Init.start]", "[" + data.getUser().getUserName() + "]");
    setTemplate(data, "Control,AGTP070.vm");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp070(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP070[doAgtp070.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp070-MAN", data);
    g_Msg = BF_MSG.Inicia_BfMsg();
    String Opc = data.getParameters().getString("Opc", "");	
    if (Opc.equals("7")||Opc.equals("6")||Opc.equals("C"))
       { g_Msg = BF_MSG.InitIntegraTasador("Agtp070", data, context); }
    else
       { g_Msg = BF_MSG.InitIntegra("Agtp070", data, context); }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (Opc.equals("1"))                           //Men� Administraci�n
       { BF_MSG.Link_Program("Agtp070", "Btgp080", data, g_Msg); }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (Opc.equals("2"))                           //Men� Ejecutivo
       { BF_MSG.Link_Program("Agtp070", "Agtp080", data, g_Msg); }
    if (Opc.equals("3"))                           //Men� Operador Comercial
       { BF_MSG.Link_Program("Agtp070", "Agtp081", data, g_Msg); }
    if (Opc.equals("F"))                           //Men� Operador Hipotecario
       { BF_MSG.Link_Program("Agtp070", "Agtp083", data, g_Msg); }
    if (Opc.equals("4"))                           //Men� UVT
       { BF_MSG.Link_Program("Agtp070", "Agtp800", data, g_Msg); }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (Opc.equals("D"))                           //Men� Supervisor Comercial
       { BF_MSG.Link_Program("Agtp070", "Agtp854", data, g_Msg); }
    if (Opc.equals("E"))                           //Men� Consultas
       { BF_MSG.Link_Program("Agtp070", "Agtp859", data, g_Msg); }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (Opc.equals("A"))                           //Men� Centro Hipotecario
       { BF_MSG.Link_Program("Agtp070", "Agtp855", data, g_Msg); }
    if (Opc.equals("B"))                           //Men� Ejecutivo Hipotecario
       { BF_MSG.Link_Program("Agtp070", "Agtp856", data, g_Msg); }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (Opc.equals("5"))                           //Men� Cursatura Comercial
       { BF_MSG.Link_Program("Agtp070", "Agtp850", data, g_Msg); }
    if (Opc.equals("8"))                           //Men� Cursatura Hipotecarios
       { BF_MSG.Link_Program("Agtp070", "Agtp851", data, g_Msg); }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (Opc.equals("6"))                           //Men� Fiscal�a
       { BF_MSG.Link_Program("Agtp070", "Afsp080", data, g_Msg); }
    if (Opc.equals("C"))                           //Men� Abogado Hipotecario
       { BF_MSG.Link_Program("Agtp070", "Agtp857", data, g_Msg); }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (Opc.equals("7"))                           //Men� Tasador
       { BF_MSG.Link_Program("Agtp070", "Agtp082", data, g_Msg); }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (Opc.equals("G"))                           //Men� Operador CMA
       { BF_MSG.Link_Program("Agtp070", "Agtp088", data, g_Msg); }
    if (Opc.equals("H"))                           //Men� Supervisor CMA
       { BF_MSG.Link_Program("Agtp070", "Agtp858", data, g_Msg); }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (Opc.trim().equals("S"))                    //Boton Salir
       { BF_MSG.Return_Program(data, g_Msg); }
  }
  //---------------------------------------------------------------------------------------
}