// Source File Name:   Agtp424.java
// Descripcion     :   Consulta Detalle Hipoteca - Suelo.Agricola (GT424)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.BF_LOB;

import com.intGarantias.modules.global.MSGGT424;
import com.intGarantias.modules.global.PRMGT421;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp424 extends FHTServletAction
{
  //---------------------------------------------------------------------------------------
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  BF_LOB lob                       = new BF_LOB();
  MSGGT424.Buf_MsgGT424 g_MsgGT424 = new MSGGT424.Buf_MsgGT424();
  PRMGT421.Buf_PrmGT421 g_PrmGT421 = new PRMGT421.Buf_PrmGT421();
  //---------------------------------------------------------------------------------------
  public Agtp424()
  {
  }
  //---------------------------------------------------------------------------------------
  public void doPerform(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP424[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp424-DIN", data);
    doAgtp424_Init(data, context);
  }
  //---------------------------------------------------------------------------------------
  public void doCancel(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP424[doCancel.start]", "[CANCELADO]");
    setTemplate(data, "Garantias,Agt,AGTP424.vm");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp424_Init(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP424[doAgtp424_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_PrmGT421 = PRMGT421.LSet_A_PrmGT421(g_Msg.Msg_Dat.toString());
    g_MsgGT424 = MSGGT424.Inicia_MsgGT424();
    rg.MoverA(g_MsgGT424.GT424_Idr, "I");
    rg.MoverA(g_MsgGT424.GT424_Gti, g_PrmGT421.GT421_Gti);
    rg.MoverA(g_MsgGT424.GT424_Seq, g_PrmGT421.GT421_Seq);
    rg.MoverA(g_MsgGT424.GT424_Sqd, g_PrmGT421.GT421_Sqd);
    rg.MoverA(g_Msg.Msg_Dat, MSGGT424.LSet_De_MsgGT424(g_MsgGT424));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GT424");
    g_MsgGT424 = MSGGT424.LSet_A_MsgGT424(g_Msg.Msg_Dat.toString());
    data.getUser().removeTemp("Agtp424_MsgGT424");
    data.getUser().setTemp("Agtp424_MsgGT424", g_MsgGT424);
    if (g_MsgGT424.GT424_Obt.toString().trim().equals("")
      ||g_MsgGT424.GT424_Obt.toString().trim().equals("000000000"))
       { data.getUser().setTemp("Agtp424_Obt", ""); }
    else
       { data.getUser().setTemp("Agtp424_Obt", lob.leeCLOB(g_MsgGT424.GT424_Obt.toString())); }
    if (g_MsgGT424.GT424_Ovb.toString().trim().equals("")
      ||g_MsgGT424.GT424_Ovb.toString().trim().equals("000000000"))
       { data.getUser().setTemp("Agtp424_Ovb", ""); }
    else
       { data.getUser().setTemp("Agtp424_Ovb", lob.leeCLOB(g_MsgGT424.GT424_Ovb.toString())); }
    setTemplate(data, "Garantias,Agt,AGTP424.vm" );
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp424(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP424[doAgtp424.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp424-MAN", data);
    String Opc = data.getParameters().getString("Opc" ,"");
    if (Opc.trim().equals("S"))
       {
         //Boton Salir
         data.getUser().removeTemp("Agtp424_MsgGT424");
         BF_MSG.Return_Data("Agtp424", data, g_Msg);
         return;
       }
  }
  //---------------------------------------------------------------------------------------
}