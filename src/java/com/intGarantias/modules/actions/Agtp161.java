// Source File Name:   Agtp161.java
// Descripcion:        Inscripciones.Fiscalia (-)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.BF_IMG;

import com.intGlobal.modules.global.PRMTG130;

import com.intGarantias.modules.global.GT_IMG;
import com.intGarantias.modules.global.PRMGT161;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp161 extends FHTServletAction
{
  //---------------------------------------------------------------------------------------
  public int    i = 0;
  public int    p = 0;
  public String lcData = "";
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  BF_IMG img                       = new BF_IMG();
  BF_IMG.Buf_Img g_Img             = new BF_IMG.Buf_Img();
  BF_IMG.Buf_Img_Bse g_Img_Base    = new BF_IMG.Buf_Img_Bse();
  GT_IMG gtimg                     = new GT_IMG();
  GT_IMG.Buf_Img_GtHyp g_Img_GtHyp = new GT_IMG.Buf_Img_GtHyp();
  Vector v_Img_GtHyp               = new Vector();
  PRMGT161.Buf_PrmGT161 g_PrmGT161 = new PRMGT161.Buf_PrmGT161();
  PRMTG130.Buf_PrmTG130 g_PrmTG130 = new PRMTG130.Buf_PrmTG130();
  //---------------------------------------------------------------------------------------
  public Agtp161()
  {
  }
  //---------------------------------------------------------------------------------------
  public void doPerform(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP161[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp161-DIN", data);
    if (g_Msg.Msg_Pgr.toString().equals("RT"))
       { doAgtp161_Continue(data, context); }
    else
       { doAgtp161_Init(data, context); }
  }
  //---------------------------------------------------------------------------------------
  public void doCancel(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP161[doCancel.start]", "[CANCELADO]");
    setTemplate(data, "Garantias,Agt,AGTP161.vm");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp161_Init(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP161[doAgtp161_Init.start]", "[" + data.getUser().getUserName() + "]");
    //g_PrmGT161 = PRMGT161.LSet_A_PrmGT161(g_Msg.Msg_Dat.toString());
    g_PrmGT161 = (PRMGT161.Buf_PrmGT161)data.getUser().getTemp("Agtp310_g_PrmGT161");
    v_Img_GtHyp.clear();
    Vector g_Tab_Img = new Vector();
    if (g_PrmGT161.GT161_Idr.toString().equals("Z"))
       { g_Tab_Img = (Vector)data.getUser().getTemp("Agtp410_vTabImg"); }
    else if (g_PrmGT161.GT161_Idr.toString().equals("T"))
       { g_Tab_Img = (Vector)data.getUser().getTemp("Agtp311_vTabImg"); }
    else
       { g_Tab_Img = (Vector)data.getUser().getTemp("Agtp310_vTabImg"); }
    De_Tab_Img(g_Tab_Img, data);
    data.getUser().removeTemp("Agtp161_PrmGT161");
    data.getUser().setTemp("Agtp161_PrmGT161", g_PrmGT161);
    data.getUser().removeTemp("Agtp161_vImgGtHyp");
    data.getUser().setTemp("Agtp161_vImgGtHyp", v_Img_GtHyp);
    setTemplate(data, "Garantias,Agt,AGTP161.vm" );
  }
  //---------------------------------------------------------------------------------------
  public void De_Tab_Img(Vector v_Tab_Img, RunData data)
  {
    g_Img       = img.Inicia_Img();
    lcData      = img.LSet_De_Img(g_Img);
    for (i=0; i<v_Tab_Img.size(); i++)
        {
          lcData = img.LSet_De_Img((BF_IMG.Buf_Img)v_Tab_Img.elementAt(i));
          g_Img  = img.LSet_A_Img(lcData);
          if (g_Img.Img_Dax.toString().trim().equals("XXHIP")
           || g_Img.Img_Dax.toString().trim().equals("YYPRD"))
             {
               g_Img_GtHyp = gtimg.LSet_A_ImgGtHyp(g_Img.Img_Dat.toString());
               v_Img_GtHyp.add (g_Img_GtHyp); 
             }
        }
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp161(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP161[doAgtp161.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp161-MAN", data);
    //g_PrmGT161 = PRMGT161.LSet_A_PrmGT161(g_Msg.Msg_Dat.toString());
    g_PrmGT161 = (PRMGT161.Buf_PrmGT161)data.getUser().getTemp("Agtp310_g_PrmGT161");
    String Opc = data.getParameters().getString("Opc" ,"");
    if (Opc.trim().equals("S"))
       {
         //Boton Salir
         data.getUser().removeTemp("Agtp161_PrmGT161");
         data.getUser().removeTemp("Agtp161_vImgGtHyp");
         rg.MoverA(g_PrmGT161.GT161_Idr, "C");
         //rg.MoverA(g_Msg.Msg_Dat, PRMGT161.LSet_De_PrmGT161(g_PrmGT161));
         rg.MoverA(g_Msg.Msg_Dat,"");
         data.getUser().removeTemp("Agtp310_g_PrmGT161");
         data.getUser().setTemp("Agtp310_g_PrmGT161" ,g_PrmGT161);
         BF_MSG.Return_Data("Agtp161", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("C"))
       {
         //Boton OK (Aceptar)
         Actualiza_TabImg(data, context);
         data.getUser().removeTemp("Agtp161_PrmGT161");
         data.getUser().removeTemp("Agtp161_vImgGtHyp");
         rg.MoverA(g_PrmGT161.GT161_Idr, "A");
         //rg.MoverA(g_Msg.Msg_Dat, PRMGT161.LSet_De_PrmGT161(g_PrmGT161));
         rg.MoverA(g_Msg.Msg_Dat,"");
         data.getUser().removeTemp("Agtp310_g_PrmGT161");
         data.getUser().setTemp("Agtp310_g_PrmGT161" ,g_PrmGT161);
         BF_MSG.Return_Data("Agtp161", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("R"))
       {
         //Boton Amarrar
         String Seq  = data.getParameters().getString("Seq", "");
         g_PrmGT161  = (PRMGT161.Buf_PrmGT161)data.getUser().getTemp("Agtp161_PrmGT161");
         i = Integer.parseInt(Seq);
         PRMGT161.Bff_PrmGT161 d_PrmGT161 = new PRMGT161.Bff_PrmGT161();
         d_PrmGT161 = (PRMGT161.Bff_PrmGT161)g_PrmGT161.GT161_Tap.elementAt(i);
         String Sqe  = data.getParameters().getString("Sqe", "");
         v_Img_GtHyp = (Vector)data.getUser().getTemp("Agtp161_vImgGtHyp");
         p = Integer.parseInt(Sqe);
         g_Img_GtHyp = (GT_IMG.Buf_Img_GtHyp)v_Img_GtHyp.elementAt(p);
         rg.MoverA(g_Img_GtHyp.GtHyp_Csv, d_PrmGT161.GT161_Csv);
         rg.MoverA(g_Img_GtHyp.GtHyp_Lcl, d_PrmGT161.GT161_Lcl);
         rg.MoverA(g_Img_GtHyp.GtHyp_Rpo, d_PrmGT161.GT161_Rpo);
         rg.MoverA(g_Img_GtHyp.GtHyp_Foj, d_PrmGT161.GT161_Foj);
         rg.MoverA(g_Img_GtHyp.GtHyp_Nin, d_PrmGT161.GT161_Nin);
         v_Img_GtHyp.set(p, g_Img_GtHyp); 
         data.getUser().removeTemp("Agtp161_vImgGtHyp");
         data.getUser().setTemp("Agtp161_vImgGtHyp", v_Img_GtHyp);
         setTemplate(data, "Garantias,Agt,AGTP161.vm" );
         return;
       }
    if (Opc.trim().equals("A"))
       {
         //Boton Agregar
         rg.MoverA(g_PrmGT161.GT161_Psw, "A");
         rg.MoverA(g_PrmGT161.GT161_Idx, rg.Suma(g_PrmGT161.GT161_Nrv,0,"1",0,2,0));
         rg.MoverA(g_PrmGT161.GT161_Cti, "");
         rg.MoverA(g_PrmGT161.GT161_Nti, "");
         rg.MoverA(g_PrmGT161.GT161_Csv, "");
         rg.MoverA(g_PrmGT161.GT161_Lcl, "");
         rg.MoverA(g_PrmGT161.GT161_Rpo, "");
         rg.MoverA(g_PrmGT161.GT161_Foj, "");
         rg.MoverA(g_PrmGT161.GT161_Nin, "");
         rg.MoverA(g_PrmGT161.GT161_Grd, "");
         data.getUser().removeTemp("Agtp161_PrmGT161");
         data.getUser().setTemp("Agtp161_PrmGT161", g_PrmGT161);
         //rg.MoverA(g_Msg.Msg_Dat, PRMGT161.LSet_De_PrmGT161(g_PrmGT161));
         rg.MoverA(g_Msg.Msg_Dat,"");
         data.getUser().removeTemp("Agtp310_g_PrmGT161");
         data.getUser().setTemp("Agtp310_g_PrmGT161" ,g_PrmGT161);
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP161.vm" );
         return;
       }
    if (Opc.trim().equals("M"))
       {
         //Boton Modificar
         String Seq = data.getParameters().getString("Seq", "");
         g_PrmGT161 = (PRMGT161.Buf_PrmGT161)data.getUser().getTemp("Agtp161_PrmGT161");
         rg.MoverA(g_PrmGT161.GT161_Psw, "M");
         rg.MoverA(g_PrmGT161.GT161_Idx, rg.Suma(Seq,0,"1",0,2,0));
         i = Integer.parseInt(g_PrmGT161.GT161_Idx.toString()) - 1;
         PRMGT161.Bff_PrmGT161 d_PrmGT161 = new PRMGT161.Bff_PrmGT161();
         d_PrmGT161 = (PRMGT161.Bff_PrmGT161)g_PrmGT161.GT161_Tap.elementAt(i);
         rg.MoverA(g_PrmGT161.GT161_Cti, d_PrmGT161.GT161_Cti);
         rg.MoverA(g_PrmGT161.GT161_Nti, d_PrmGT161.GT161_Nti);
         rg.MoverA(g_PrmGT161.GT161_Csv, d_PrmGT161.GT161_Csv);
         rg.MoverA(g_PrmGT161.GT161_Lcl, d_PrmGT161.GT161_Lcl);
         rg.MoverA(g_PrmGT161.GT161_Rpo, d_PrmGT161.GT161_Rpo);
         rg.MoverA(g_PrmGT161.GT161_Foj, d_PrmGT161.GT161_Foj);
         rg.MoverA(g_PrmGT161.GT161_Nin, d_PrmGT161.GT161_Nin);
         rg.MoverA(g_PrmGT161.GT161_Grd, d_PrmGT161.GT161_Grd);
         data.getUser().removeTemp("Agtp161_PrmGT161");
         data.getUser().setTemp("Agtp161_PrmGT161", g_PrmGT161);
         //rg.MoverA(g_Msg.Msg_Dat, PRMGT161.LSet_De_PrmGT161(g_PrmGT161));
         rg.MoverA(g_Msg.Msg_Dat,"");
         data.getUser().removeTemp("Agtp310_g_PrmGT161");
         data.getUser().setTemp("Agtp310_g_PrmGT161" ,g_PrmGT161);
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP161.vm" );
         return;
       }
    if (Opc.trim().equals("E"))
       {
         //Boton Eliminar
         String Seq = data.getParameters().getString("Seq", "");
         g_PrmGT161 = (PRMGT161.Buf_PrmGT161)data.getUser().getTemp("Agtp161_PrmGT161");
         rg.MoverA(g_PrmGT161.GT161_Psw, "E");
         rg.MoverA(g_PrmGT161.GT161_Idx, rg.Suma(Seq,0,"1",0,2,0));
         i = Integer.parseInt(g_PrmGT161.GT161_Idx.toString()) - 1;
         PRMGT161.Bff_PrmGT161 d_PrmGT161 = new PRMGT161.Bff_PrmGT161();
         d_PrmGT161 = (PRMGT161.Bff_PrmGT161)g_PrmGT161.GT161_Tap.elementAt(i);
         rg.MoverA(g_PrmGT161.GT161_Cti, d_PrmGT161.GT161_Cti);
         rg.MoverA(g_PrmGT161.GT161_Nti, d_PrmGT161.GT161_Nti);
         rg.MoverA(g_PrmGT161.GT161_Csv, d_PrmGT161.GT161_Csv);
         rg.MoverA(g_PrmGT161.GT161_Lcl, d_PrmGT161.GT161_Lcl);
         rg.MoverA(g_PrmGT161.GT161_Rpo, d_PrmGT161.GT161_Rpo);
         rg.MoverA(g_PrmGT161.GT161_Foj, d_PrmGT161.GT161_Foj);
         rg.MoverA(g_PrmGT161.GT161_Nin, d_PrmGT161.GT161_Nin);
         rg.MoverA(g_PrmGT161.GT161_Grd, d_PrmGT161.GT161_Grd);
         data.getUser().removeTemp("Agtp161_PrmGT161");
         data.getUser().setTemp("Agtp161_PrmGT161", g_PrmGT161);
         //rg.MoverA(g_Msg.Msg_Dat, PRMGT161.LSet_De_PrmGT161(g_PrmGT161));
         rg.MoverA(g_Msg.Msg_Dat,"");
         data.getUser().removeTemp("Agtp310_g_PrmGT161");
         data.getUser().setTemp("Agtp310_g_PrmGT161" ,g_PrmGT161);
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP161.vm" );
         return;
       }
    if (Opc.trim().equals("1"))
       {
         //Consulta Lugares Disponibles
         Actualiza_Datos(data, context);
         g_PrmTG130 = PRMTG130.Inicia_PrmTG130();
         rg.MoverA(g_PrmTG130.TG130_Cmp, "GRT-CTI");
         rg.MoverA(g_PrmTG130.TG130_Ncm, "Tipos de Inscripciones de Bienes");
         data.getUser().removeTemp("Agtp161_PrmGT161");
         data.getUser().setTemp("Agtp161_PrmGT161" ,g_PrmGT161);
         rg.MoverA(g_Msg.Msg_Dat, PRMTG130.LSet_De_PrmTG130(g_PrmTG130));
         BF_MSG.Link_Program("Agtp161", "Atgp130", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("2"))
       {
         //Consulta Sectores Disponibles
         Actualiza_Datos(data, context);
         g_PrmTG130 = PRMTG130.Inicia_PrmTG130();
         rg.MoverA(g_PrmTG130.TG130_Cmp, "GRT-KTP");
         rg.MoverA(g_PrmTG130.TG130_Ncm, "Tipos de Conservadores de Bienes");
         data.getUser().removeTemp("Agtp161_PrmGT161");
         data.getUser().setTemp("Agtp161_PrmGT161" ,g_PrmGT161);
         rg.MoverA(g_Msg.Msg_Dat, PRMTG130.LSet_De_PrmTG130(g_PrmTG130));
         BF_MSG.Link_Program("Agtp161", "Atgp130", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("Y"))
       {
         //Boton Si
         Actualiza_Datos(data, context);
         i = Integer.parseInt(g_PrmGT161.GT161_Idx.toString()) - 1;
         PRMGT161.Bff_PrmGT161 d_PrmGT161 = new PRMGT161.Bff_PrmGT161();
         d_PrmGT161 = (PRMGT161.Bff_PrmGT161)g_PrmGT161.GT161_Tap.elementAt(i);
         rg.MoverA(d_PrmGT161.GT161_Cti, g_PrmGT161.GT161_Cti);
         rg.MoverA(d_PrmGT161.GT161_Nti, g_PrmGT161.GT161_Nti);
         rg.MoverA(d_PrmGT161.GT161_Csv, g_PrmGT161.GT161_Csv);
         rg.MoverA(d_PrmGT161.GT161_Lcl, g_PrmGT161.GT161_Lcl);
         rg.MoverA(d_PrmGT161.GT161_Rpo, g_PrmGT161.GT161_Rpo);
         rg.MoverA(d_PrmGT161.GT161_Foj, g_PrmGT161.GT161_Foj);
         rg.MoverA(d_PrmGT161.GT161_Nin, g_PrmGT161.GT161_Nin);
         rg.MoverA(d_PrmGT161.GT161_Grd, g_PrmGT161.GT161_Grd);
         if (g_PrmGT161.GT161_Psw.toString().trim().equals("M"))
            { g_PrmGT161.GT161_Tap.set(i, d_PrmGT161); }
         else
            {
              if (g_PrmGT161.GT161_Psw.toString().trim().equals("E"))
                 { 
                   g_PrmGT161.GT161_Tap.remove(i);
                   rg.MoverA(g_PrmGT161.GT161_Nrv, rg.Resta(g_PrmGT161.GT161_Nrv,0,"1",0,3,0));
                   PRMGT161.Bff_PrmGT161 f_PrmGT161 = new PRMGT161.Bff_PrmGT161();
                   g_PrmGT161.GT161_Tap.add(f_PrmGT161);
                 }
              else
                 { 
                   g_PrmGT161.GT161_Tap.set(i, d_PrmGT161);
                   rg.MoverA(g_PrmGT161.GT161_Nrv, rg.Suma(g_PrmGT161.GT161_Nrv,0,"1",0,3,0));
                 }
            }
         rg.MoverA(g_PrmGT161.GT161_Psw, " ");
         rg.MoverA(g_PrmGT161.GT161_Idx, " ");
         data.getUser().removeTemp("Agtp161_PrmGT161");
         data.getUser().setTemp("Agtp161_PrmGT161", g_PrmGT161);
         //rg.MoverA(g_Msg.Msg_Dat, PRMGT161.LSet_De_PrmGT161(g_PrmGT161));
         rg.MoverA(g_Msg.Msg_Dat,"");
         data.getUser().removeTemp("Agtp310_g_PrmGT161");
         data.getUser().setTemp("Agtp310_g_PrmGT161" ,g_PrmGT161);
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP161.vm" );
         return;
       }
    if (Opc.trim().equals("N"))
       {
         //Boton No
         g_PrmGT161 = (PRMGT161.Buf_PrmGT161)data.getUser().getTemp("Agtp161_PrmGT161");
         rg.MoverA(g_PrmGT161.GT161_Psw, " ");
         rg.MoverA(g_PrmGT161.GT161_Idx, " ");
         data.getUser().removeTemp("Agtp161_PrmGT161");
         data.getUser().setTemp("Agtp161_PrmGT161", g_PrmGT161);
         //rg.MoverA(g_Msg.Msg_Dat, PRMGT161.LSet_De_PrmGT161(g_PrmGT161));
         rg.MoverA(g_Msg.Msg_Dat,"");
         data.getUser().removeTemp("Agtp310_g_PrmGT161");
         data.getUser().setTemp("Agtp310_g_PrmGT161" ,g_PrmGT161);
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP161.vm" );
         return;
       }
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp161_Continue(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP161[doAgtp161_Continue.start]", "[" + data.getUser().getUserName() + "]");
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Atgp130"))
       {
         g_PrmTG130 = PRMTG130.LSet_A_PrmTG130(g_Msg.Msg_Dat.toString());
         g_PrmGT161 = (PRMGT161.Buf_PrmGT161)data.getUser().getTemp("Agtp161_PrmGT161");
         if (g_PrmTG130.TG130_Cmp.toString().trim().equals("GRT-CTI"))
             {
               if (g_PrmTG130.TG130_Cod.toString().trim().compareTo("")!=0)
                  {
                    rg.MoverA(g_PrmGT161.GT161_Cti, g_PrmTG130.TG130_Cod);
                    rg.MoverA(g_PrmGT161.GT161_Nti, g_PrmTG130.TG130_Dsc);
                  }
             }
         if (g_PrmTG130.TG130_Cmp.toString().trim().equals("GRT-KTP"))
             {
               if (g_PrmTG130.TG130_Cod.toString().trim().compareTo("")!=0)
                  { rg.MoverA(g_PrmGT161.GT161_Csv, g_PrmTG130.TG130_Cod); }
             }
         data.getUser().removeTemp("Agtp161_PrmGT161");
         data.getUser().setTemp("Agtp161_PrmGT161", g_PrmGT161);
         rg.MoverA(g_Msg.Msg_Dat, PRMGT161.LSet_De_PrmGT161(g_PrmGT161));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP161.vm" );
         return;
       }
  }
  //---------------------------------------------------------------------------------------
  public void Actualiza_Datos(RunData data, Context context)
              throws Exception 
  {
    rg.MoverA(g_PrmGT161.GT161_Lcl, data.getParameters().getString("GT161_Lcl", ""));
    rg.MoverA(g_PrmGT161.GT161_Rpo, data.getParameters().getString("GT161_Rpo", ""));
    rg.MoverA(g_PrmGT161.GT161_Foj, data.getParameters().getString("GT161_Foj", ""));
    rg.MoverA(g_PrmGT161.GT161_Nin, data.getParameters().getString("GT161_Nin", ""));
    rg.MoverA(g_PrmGT161.GT161_Grd, data.getParameters().getString("GT161_Grd", ""));
  }
  //---------------------------------------------------------------------------------------
  public void Actualiza_TabImg(RunData data, Context context)
              throws Exception 
  {
    g_Img            = img.Inicia_Img();
    lcData           = img.LSet_De_Img(g_Img);
    Vector g_Tab_Img = new Vector();
    if (g_PrmGT161.GT161_Idr.toString().equals("Z"))
       { g_Tab_Img = (Vector)data.getUser().getTemp("Agtp410_vTabImg"); }
    else if (g_PrmGT161.GT161_Idr.toString().equals("T"))
       { g_Tab_Img = (Vector)data.getUser().getTemp("Agtp311_vTabImg"); }
    else
       { g_Tab_Img = (Vector)data.getUser().getTemp("Agtp310_vTabImg"); }
    v_Img_GtHyp      = (Vector)data.getUser().getTemp("Agtp161_vImgGtHyp");
    for (i=0; i<g_Tab_Img.size(); i++)
        {
          lcData = img.LSet_De_Img((BF_IMG.Buf_Img)g_Tab_Img.elementAt(i));
          g_Img  = img.LSet_A_Img(lcData);
          if (g_Img.Img_Dax.toString().trim().equals("XXHIP")
           || g_Img.Img_Dax.toString().trim().equals("YYPRD"))
             {
               GT_IMG.Buf_Img_GtHyp l_Img_GtHyp = new GT_IMG.Buf_Img_GtHyp();
               l_Img_GtHyp = gtimg.LSet_A_ImgGtHyp(g_Img.Img_Dat.toString());
               for (p=0; p<v_Img_GtHyp.size(); p++)
                   {
                     g_Img_GtHyp = (GT_IMG.Buf_Img_GtHyp)v_Img_GtHyp.elementAt(p);
                     if (l_Img_GtHyp.GtHyp_Seq.toString().equals(g_Img_GtHyp.GtHyp_Seq.toString()))
                        {
                          rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtHyp(g_Img_GtHyp));
                          Vector vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
                          g_Tab_Img.set(i, (BF_IMG.Buf_Img)vPaso.elementAt(0));
                          break;
                        }
                   }
             }
        }
    if (g_PrmGT161.GT161_Idr.toString().equals("Z"))
       { 
       	 data.getUser().removeTemp("Agtp410_vTabImg");
       	 data.getUser().setTemp("Agtp410_vTabImg", g_Tab_Img);
       }
    else if (g_PrmGT161.GT161_Idr.toString().equals("T"))
       { 
       	 data.getUser().removeTemp("Agtp311_vTabImg");
       	 data.getUser().setTemp("Agtp311_vTabImg", g_Tab_Img);
       }
    else
       { 
       	 data.getUser().removeTemp("Agtp310_vTabImg");
       	 data.getUser().setTemp("Agtp310_vTabImg", g_Tab_Img);
       }
  }
  //---------------------------------------------------------------------------------------
}