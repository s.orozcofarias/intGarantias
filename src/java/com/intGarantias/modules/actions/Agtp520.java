// Source File Name:   Agtp520.java
// Descripcion:        Consulta Hipotecas y Prendas UVT (GT520)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.PRMPC080;

import com.intGlobal.modules.global.PRMTG123;

import com.intGarantias.modules.global.MSGGT520;
import com.intGarantias.modules.global.PRMGT093;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp520 extends FHTServletAction
{
  //---------------------------------------------------------------------------------------
  public int    i = 0;
  public String lcRetorno = "";
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  PRMPC080.PrmPC080 g_PrmPC080     = new PRMPC080.PrmPC080();
  MSGGT520.Buf_MsgGT520 g_MsgGT520 = new MSGGT520.Buf_MsgGT520();
  PRMGT093.Buf_PrmGT093 g_PrmGT093 = new PRMGT093.Buf_PrmGT093();
  PRMTG123.Buf_PrmTG123 g_PrmTG123 = new PRMTG123.Buf_PrmTG123();
  //---------------------------------------------------------------------------------------
  public Agtp520()
  {
  }
  //---------------------------------------------------------------------------------------
  public void doPerform(RunData data, Context context)
              throws Exception
  {
  //Log.debug("AGTP520[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp520-DIN", data);
    if (g_Msg.Msg_Pgr.toString().equals("RT"))
       { doAgtp520_Continue(data, context); }
    else
       { doAgtp520_Init(data, context); }
  }
  //---------------------------------------------------------------------------------------
  public void doCancel(RunData data, Context context)
              throws Exception
  {
  //Log.debug("AGTP520[doCancel.start]", "[CANCELADO]");
    setTemplate(data, "Garantias,Agt,AGTP520.vm");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp520_Init(RunData data, Context context)
              throws Exception
  {
    Log.debug("AGTP520[doAgtp520_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    g_MsgGT520 = MSGGT520.Inicia_MsgGT520();
    rg.MoverA(g_MsgGT520.GT520_Idr, "I");
//    rg.MoverA(g_MsgGT520.GT520_Itg, g_PrmGT093.GT093_Dpc);
//    rg.MoverA(g_MsgGT520.GT520_Cmn, g_Prm_TG123.TG123_Prv.toString() + g_Prm_TG123.TG123_Cmn.toString());
    Poblar_Panel(data, context, "I");
    data.getUser().removeTemp("Prd");
    data.getUser().setTemp("Prd", "");
    data.getUser().removeTemp("Cmn");
    data.getUser().setTemp("Cmn", "");
    data.getUser().removeTemp("Producto");
    data.getUser().setTemp("Producto", "");
    data.getUser().removeTemp("Comuna");
    data.getUser().setTemp("Comuna", "");
    setTemplate(data, "Garantias,Agt,AGTP520.vm" );
    Log.debug("AGTP520[doAgtp520_Init.end]", "[" + data.getUser().getUserName() + "]");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp520(RunData data, Context context)
              throws Exception
  {
  //Log.debug("AGTP520[doAgtp520.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp520-MAN", data);
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    String Opc = data.getParameters().getString("Opc");
    if  (Opc.equals("S"))
       //Salir
        {
          data.getUser().removeTemp("Prd");
          data.getUser().removeTemp("Cmn");
          data.getUser().removeTemp("Producto");
          data.getUser().removeTemp("Comuna");
          data.getUser().removeTemp("Agtp520_PrmPC080");
          data.getUser().removeTemp("Agtp520_MsgGT520");
          BF_MSG.Return_Program(data, g_Msg);
          return;
        }
    if  (Opc.equals("K"))
       //Cambiar
        {
          String Prd = data.getParameters().getString("Prd", "");
          String Cmn = data.getParameters().getString("Cmn", "");
          String Npr = data.getParameters().getString("Npr", "");
          String Ncm = data.getParameters().getString("Ncm", "");
          g_MsgGT520 = MSGGT520.Inicia_MsgGT520();
          rg.MoverA(g_MsgGT520.GT520_Idr, "I");
          rg.MoverA(g_MsgGT520.GT520_Itg, Prd);
          rg.MoverA(g_MsgGT520.GT520_Cmn, Cmn);
          Poblar_Panel(data, context, "I");
          data.getUser().removeTemp("Prd");
          data.getUser().setTemp("Prd", Prd);
          data.getUser().removeTemp("Cmn");
          data.getUser().setTemp("Cmn", Cmn);
          data.getUser().removeTemp("Producto");
          data.getUser().setTemp("Producto", Npr);
          data.getUser().removeTemp("Comuna");
          data.getUser().setTemp("Comuna", Ncm);
          setTemplate(data, "Garantias,Agt,AGTP520.vm" );
          return;
        }
    g_MsgGT520 = (MSGGT520.Buf_MsgGT520)data.getUser().getTemp("Agtp520_MsgGT520");
    MSGGT520.Bff_MsgGT520 d_MsgGT520 = new MSGGT520.Bff_MsgGT520();
    if  (Opc.equals("T"))
       //Detalle
        {
          String Seq = data.getParameters().getString("Seq", "");
          i = Integer.parseInt(Seq);
          d_MsgGT520 = (MSGGT520.Bff_MsgGT520)g_MsgGT520.GT520_Tab.elementAt(i);
          rg.MoverA(g_PrmPC080.PC080_Cli,     "");
          rg.MoverA(g_PrmPC080.PC080_Ncl,     "");
          rg.MoverA(g_PrmPC080.PC080_Cnr.Sis, d_MsgGT520.GT520_Sis);
          rg.MoverA(g_PrmPC080.PC080_Cnr.Ncn, d_MsgGT520.GT520_Ncn);
          rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
          data.getUser().removeTemp("Agtp520_g_PrmPC080");
          data.getUser().setTemp("Agtp520_g_PrmPC080", g_PrmPC080);
          //lcRetorno = BF_MSG.Link_ProgramN("Agtp520", "Agtp451", data, g_Msg); 
BF_MSG.Link_Program("Agtp520", "Agtp451", data, g_Msg); 
          if (lcRetorno.trim().compareTo("")!=0)
             {
               data.getUser().removeTemp("Agtp520_g_PrmPC080");
               data.getUser().setTemp("Agtp520_g_PrmPC080", g_PrmPC080);
               BF_MSG.MsgInt("Agtp520", data, g_Msg, lcRetorno, "1"); 
      	       return;
             }
       	  return;
        }
    if  (Opc.equals("N"))
       //Siguiente
        {	
          Poblar_Panel(data, context, "S");
          setTemplate(data, "garantias,Agt,AGTP520.vm" );
          return;
        }
    if  (Opc.equals("P"))
       //Previo
        {	
          Poblar_Panel(data, context, "P");
          setTemplate(data, "garantias,Agt,AGTP520.vm" );
          return;
        }		
  }
  //---------------------------------------------------------------------------------------
  public void Poblar_Panel(RunData data, Context context, String pcIdr)
              throws Exception
  {
    MSGGT520.Bff_MsgGT520 d_MsgGT520 = new MSGGT520.Bff_MsgGT520();
    if (pcIdr.equals("E"))
       {
         g_MsgGT520 = (MSGGT520.Buf_MsgGT520)data.getUser().getTemp("Agtp520_MsgGT520");
         i = 0;
       }
    if (pcIdr.equals("S"))
       { i = Integer.parseInt(g_MsgGT520.GT520_Nro.toString())-1; }
    if (pcIdr.equals("P"))
       { i = 0; }
    if (pcIdr.compareTo("I")!=0)
       {
         String lcItg = g_MsgGT520.GT520_Itg.toString();
         String lcCmn = g_MsgGT520.GT520_Cmn.toString();
         d_MsgGT520 = (MSGGT520.Bff_MsgGT520)g_MsgGT520.GT520_Tab.elementAt(i);
         g_MsgGT520 = MSGGT520.Inicia_MsgGT520();
         rg.MoverA(g_MsgGT520.GT520_Idr, pcIdr);
         rg.MoverA(g_MsgGT520.GT520_Itg, lcItg);
         rg.MoverA(g_MsgGT520.GT520_Cmn, lcCmn);
         g_MsgGT520.GT520_Tab.set(0, d_MsgGT520);
       }
    rg.MoverA(g_Msg.Msg_Dat, MSGGT520.LSet_De_MsgGT520(g_MsgGT520));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GT520");
    g_MsgGT520 = MSGGT520.LSet_A_MsgGT520(g_Msg.Msg_Dat.toString());
    data.getUser().removeTemp("Agtp520_MsgGT520");
    data.getUser().setTemp("Agtp520_MsgGT520", g_MsgGT520);
    data.getUser().removeTemp("Agtp520_PrmPC080");
    data.getUser().setTemp("Agtp520_PrmPC080", g_PrmPC080);
  }
//---------------------------------------------------------------------------------------
  public void doAgtp520_Continue(RunData data, Context context)
              throws Exception
  {
  //Log.debug("AGTP520[doAgtp520_Continue.start]", "[" + data.getUser().getUserName() + "]");
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Appp030"))
       {
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp520_g_PrmPC080");
         data.getUser().removeTemp("Agtp520_g_PrmPC080");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "garantias,Agt,AGTP520.vm" );
         return;
       }
    g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp520_g_PrmPC080");
    data.getUser().removeTemp("Agtp520_g_PrmPC080");
    rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
    BF_MSG.Param_Program(data, g_Msg);
    setTemplate(data, "garantias,Agt,AGTP520.vm" );
    return;
  }
}