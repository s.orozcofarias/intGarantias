// Source File Name:   Agtp762.java
// Descripcion:        Ingreso/Seleccion Roles SII (-)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.BF_IMG;
import com.FHTServlet.modules.global.PRMPC080;

import com.intGarantias.modules.global.GT_IMG;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp762 extends FHTServletAction
{
  //---------------------------------------------------------------------------------------
  public int    i = 0;
  public String lcData = "";
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  PRMPC080.PrmPC080 g_PrmPC080     = new PRMPC080.PrmPC080();
  BF_IMG img                       = new BF_IMG();
  BF_IMG.Buf_Img g_Img             = new BF_IMG.Buf_Img();
  GT_IMG gtimg                     = new GT_IMG();
  Vector v_Img_GtHpd               = new Vector();
  GT_IMG.Buf_Img_GtHpd g_Img_GtHpd = new GT_IMG.Buf_Img_GtHpd();
  //---------------------------------------------------------------------------------------
  public Agtp762()
  {
  }
  //---------------------------------------------------------------------------------------
  public void doPerform(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP762[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp762-DIN", data);
    doAgtp762_Init(data, context);
  }
  //---------------------------------------------------------------------------------------
  public void doCancel(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP762[doCancel.start]", "[CANCELADO]");
    setTemplate(data, "Garantias,Agt,AGTP762.vm");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp762_Init(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP762[doAgtp762_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    v_Img_GtHpd = (Vector)data.getUser().getTemp("Agtp727_vImgGtHpd");
    g_Img_GtHpd = (GT_IMG.Buf_Img_GtHpd)data.getUser().getTemp("Agtp763_ImgGtHpd");
    lcData = gtimg.LSet_De_ImgGtHpd(g_Img_GtHpd);
    g_Img_GtHpd = gtimg.LSet_A_ImgGtHpd(lcData);
    data.getUser().removeTemp("Agtp762_ImgGtHpd");
    data.getUser().setTemp("Agtp762_ImgGtHpd", g_Img_GtHpd);
    data.getUser().removeTemp("Agtp762_vImgGtHpd");
    data.getUser().setTemp("Agtp762_vImgGtHpd", v_Img_GtHpd);
    data.getUser().removeTemp("Agtp762_PrmPC080");
    data.getUser().setTemp("Agtp762_PrmPC080", g_PrmPC080);
    setTemplate(data, "Garantias,Agt,AGTP762.vm" );
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp762(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP762[doAgtp762.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp762-MAN", data);
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    String Opc = data.getParameters().getString("Opc", "");
    if (Opc.trim().equals("S"))
       {
         //Boton Salir
         data.getUser().removeTemp("Agtp762_PrmPC080");
         data.getUser().removeTemp("Agtp762_vImgGtHpd");
         data.getUser().removeTemp("Agtp762_ImgGtHpd");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "NK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp762", data, g_Msg);
         return;
       }
    if (Opc.equals("E"))
       {
         //selecci�n (Especifico)
         String Seq = data.getParameters().getString("Seq", "");
         i = Integer.parseInt(Seq);
         v_Img_GtHpd = (Vector)data.getUser().getTemp("Agtp762_vImgGtHpd");
         GT_IMG.Buf_Img_GtHpd l_Img_GtHpd = new GT_IMG.Buf_Img_GtHpd();
         l_Img_GtHpd = (GT_IMG.Buf_Img_GtHpd)v_Img_GtHpd.elementAt(i);
         g_Img_GtHpd = (GT_IMG.Buf_Img_GtHpd)data.getUser().getTemp("Agtp762_ImgGtHpd");
         rg.MoverA(g_Img_GtHpd.GtHpd_Rs1, l_Img_GtHpd.GtHpd_Rs1);
         rg.MoverA(g_Img_GtHpd.GtHpd_Rs2, l_Img_GtHpd.GtHpd_Rs2);
         rg.MoverA(g_Img_GtHpd.GtHpd_Rds, l_Img_GtHpd.GtHpd_Rds);
         rg.MoverA(g_Img_GtHpd.GtHpd_Faf, l_Img_GtHpd.GtHpd_Faf);
         rg.MoverA(g_Img_GtHpd.GtHpd_Vaf, l_Img_GtHpd.GtHpd_Vaf);
         data.getUser().removeTemp("Agtp762_ImgGtHpd");
         data.getUser().setTemp("Agtp762_ImgGtHpd", g_Img_GtHpd);
         setTemplate(data, "Garantias,Agt,AGTP762.vm" );
         return;
       }
    if (Opc.trim().equals("A"))
       {
         //Boton Aceptar
         g_Img_GtHpd = (GT_IMG.Buf_Img_GtHpd)data.getUser().getTemp("Agtp762_ImgGtHpd");
         lcData = gtimg.LSet_De_ImgGtHpd(g_Img_GtHpd);
         g_Img_GtHpd = gtimg.LSet_A_ImgGtHpd(lcData);
         Actualiza_Datos(data, context);
         data.getUser().removeTemp("Agtp763_ImgGtHpd");
         data.getUser().setTemp("Agtp763_ImgGtHpd", g_Img_GtHpd);
         data.getUser().removeTemp("Agtp762_PrmPC080");
         data.getUser().removeTemp("Agtp762_vImgGtHpd");
         data.getUser().removeTemp("Agtp762_ImgGtHpd");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp762", data, g_Msg);
         return;
       }
  }
  //---------------------------------------------------------------------------------------
  public void Actualiza_Datos(RunData data, Context context)
              throws Exception 
  {
    rg.MoverA(g_Img_GtHpd.GtHpd_Rs1, data.getParameters().getString("GtHpd_Rs1", ""));
    rg.MoverA(g_Img_GtHpd.GtHpd_Rs2, data.getParameters().getString("GtHpd_Rs2", ""));
    rg.MoverA(g_Img_GtHpd.GtHpd_Rds, data.getParameters().getString("GtHpd_Rds", ""));
    rg.MoverA(g_Img_GtHpd.GtHpd_Faf, data.getParameters().getString("GtHpd_Faf", ""));
    rg.MoverA(g_Img_GtHpd.GtHpd_Vaf, data.getParameters().getString("GtHpd_Vaf", ""));
  }
  //---------------------------------------------------------------------------------------
}