// Source File Name:   Agtp423.java
// Descripcion     :   Consulta Detalle Hipoteca - Estados.Avance (GT423)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.BF_LOB;
import com.FHTServlet.modules.global.PRMPC080;

import com.intGarantias.modules.global.MSGGT423;
import com.intGarantias.modules.global.PRMGT421;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp423 extends FHTServletAction
{
  //---------------------------------------------------------------------------------------
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  BF_LOB lob                       = new BF_LOB();
  PRMPC080.PrmPC080 g_PrmPC080     = new PRMPC080.PrmPC080();
  MSGGT423.Buf_MsgGT423 g_MsgGT423 = new MSGGT423.Buf_MsgGT423();
  PRMGT421.Buf_PrmGT421 g_PrmGT421 = new PRMGT421.Buf_PrmGT421();
  //---------------------------------------------------------------------------------------
  public Agtp423()
  {
  }
  //---------------------------------------------------------------------------------------
  public void doPerform(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP423[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp423-DIN", data);
    if (g_Msg.Msg_Pgr.toString().equals("RT"))
       { doAgtp423_Continue(data, context); }
    else
       { doAgtp423_Init(data, context); }
  }
  //---------------------------------------------------------------------------------------
  public void doCancel(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP423[doCancel.start]", "[CANCELADO]");
    setTemplate(data, "Garantias,Agt,AGTP423.vm");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp423_Init(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP423[doAgtp423_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_PrmGT421 = PRMGT421.LSet_A_PrmGT421(g_Msg.Msg_Dat.toString());
    g_MsgGT423 = MSGGT423.Inicia_MsgGT423();
    rg.MoverA(g_MsgGT423.GT423_Idr, "I");
    rg.MoverA(g_MsgGT423.GT423_Gti, g_PrmGT421.GT421_Gti);
    rg.MoverA(g_MsgGT423.GT423_Seq, g_PrmGT421.GT421_Seq);
    rg.MoverA(g_MsgGT423.GT423_Sqd, g_PrmGT421.GT421_Sqd);
    rg.MoverA(g_Msg.Msg_Dat, MSGGT423.LSet_De_MsgGT423(g_MsgGT423));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GT423");
    g_MsgGT423 = MSGGT423.LSet_A_MsgGT423(g_Msg.Msg_Dat.toString());
    rg.MoverA(g_MsgGT423.GT423_Idx, "01");
    Carga_vEao(data, context);
    data.getUser().removeTemp("Agtp423_MsgGT423");
    data.getUser().setTemp("Agtp423_MsgGT423", g_MsgGT423);
    setTemplate(data, "Garantias,Agt,AGTP423.vm" );
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp423(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP423[doAgtp423.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp423-MAN", data);
    String Opc = data.getParameters().getString("Opc" ,"");
    if (Opc.trim().equals("S"))
       {
         //Boton Salir
         data.getUser().removeTemp("Agtp423_MsgGT423");
         data.getUser().removeTemp("Agtp423_vEao");
         BF_MSG.Return_Data("Agtp423", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("<"))
       {
         //Boton Atras
         g_MsgGT423 = (MSGGT423.Buf_MsgGT423)data.getUser().getTemp("Agtp423_MsgGT423");
         rg.MoverA(g_MsgGT423.GT423_Idx, rg.Resta(g_MsgGT423.GT423_Idx,0,"1",0,2,0));
         Actualiza_vEao(data, context);
         data.getUser().removeTemp("Agtp423_MsgGT423");
         data.getUser().setTemp("Agtp423_MsgGT423", g_MsgGT423);
         setTemplate(data, "Garantias,Agt,AGTP423.vm" );
         return;
       }
    if (Opc.trim().equals(">"))
       {
         //Boton Adelante
         g_MsgGT423 = (MSGGT423.Buf_MsgGT423)data.getUser().getTemp("Agtp423_MsgGT423");
         rg.MoverA(g_MsgGT423.GT423_Idx, rg.Suma(g_MsgGT423.GT423_Idx,0,"1",0,2,0));
         Actualiza_vEao(data, context);
         data.getUser().removeTemp("Agtp423_MsgGT423");
         data.getUser().setTemp("Agtp423_MsgGT423", g_MsgGT423);
         setTemplate(data, "Garantias,Agt,AGTP423.vm" );
         return;
       }
    if (Opc.trim().equals("I"))
       {
         //Boton Imagen
         g_MsgGT423 = (MSGGT423.Buf_MsgGT423)data.getUser().getTemp("Agtp423_MsgGT423");
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp411_PrmPC080");
       //rg.MoverA(g_PrmPC080.PC080_Cli,     g_MsgGT423.GT423_Cli);
       //rg.MoverA(g_PrmPC080.PC080_Ncl,     g_MsgGT423.GT423_Ncl);
       //rg.MoverA(g_PrmPC080.PC080_Dir,     g_MsgGT423.GT423_Dir);
       //rg.MoverA(g_PrmPC080.PC080_Tfc,     g_MsgGT423.GT423_Tfc);
       //rg.MoverA(g_PrmPC080.PC080_Dcn,     g_MsgGT423.GT423_Dcn);
       //rg.MoverA(g_PrmPC080.PC080_Ndc,     g_MsgGT423.GT423_Ndc);
         rg.MoverA(g_PrmPC080.PC080_Cnr.Sis, g_MsgGT423.GT423_Gti.toString().substring(0,3));
         rg.MoverA(g_PrmPC080.PC080_Cnr.Ncn, g_MsgGT423.GT423_Gti.toString().substring(3,10));
         rg.MoverA(g_PrmPC080.PC080_Ntc,     g_MsgGT423.GT423_Seq);
         rg.MoverA(g_PrmPC080.PC080_Dtt,     g_MsgGT423.GT423_Dsb);
         rg.MoverA(g_PrmPC080.PC080_Mnt,     g_MsgGT423.GT423_Tps);
         rg.MoverA(g_PrmPC080.PC080_Amv,     "F");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Link_Program("Agtp423", "Agtp459", data, g_Msg);
         return;
       }
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp423_Continue(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP423[doAgtp423_Continue.start]", "[" + data.getUser().getUserName() + "]");
    //if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Appp030"))
    //   {
    //     g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp423_g_PrmPC080");
    //     data.getUser().removeTemp("Agtp423_g_PrmPC080");
    //     rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
    //     BF_MSG.Param_Program(data, g_Msg);
    //     setTemplate(data, "Garantias,Agt,AGTP423.vm" );
    //     return;
    //   }
    setTemplate(data, "Garantias,Agt,AGTP423.vm" );
    return;
  }
  //---------------------------------------------------------------------------------------
  public void Carga_vEao(RunData data, Context context)
              throws Exception 
  {
    Vector v_Eao = new Vector();
    v_Eao.clear();
    for (int i=0;i<15;i++)
        {
          MSGGT423.Buf_vEao vEao = new MSGGT423.Buf_vEao();
          if (i==0)   { rg.MoverA(vEao.vEao_Tit, "Obras Previas"); }
          if (i==1)   { rg.MoverA(vEao.vEao_Tit, "Obra Gruesa"); }
          if (i==2)   { rg.MoverA(vEao.vEao_Tit, "Terminaciones"); }
          if (i==3)   { rg.MoverA(vEao.vEao_Tit, "Instalaciones"); }
          if (i==4)   { rg.MoverA(vEao.vEao_Tit, "Obras Exteriores"); }
          if (i==5)   { rg.MoverA(vEao.vEao_Tit, "O. Complementarias"); }
          if (i==6)   { rg.MoverA(vEao.vEao_Tit, "Urbanizacion"); }
          if (i==7)   { rg.MoverA(vEao.vEao_Tit, "Obras Adicionales"); }
          if (i==8)   { rg.MoverA(vEao.vEao_Tit, "Otras Obras"); }
          if (i==9)   { rg.MoverA(vEao.vEao_Tit, "Costo Directo Obras"); }
          if (i==10)  { rg.MoverA(vEao.vEao_Tit, "Gastos Generales"); }
          if (i==11)  { rg.MoverA(vEao.vEao_Tit, "Utilidad"); }
          if (i==12)  { rg.MoverA(vEao.vEao_Tit, "Costo Construccion"); }
          if (i==13)  { rg.MoverA(vEao.vEao_Tit, "IVA"); }
          if (i==14)  { rg.MoverA(vEao.vEao_Tit, "Costo Total"); }
          rg.MoverA(vEao.vEao_Ppv, ((MSGGT423.vPpv)g_MsgGT423.vGT423_Ppv.elementAt(i)).GT423_Ppv.toString());
          rg.MoverA(vEao.vEao_Oep, ((MSGGT423.vOep)g_MsgGT423.vGT423_Oep.elementAt(i)).GT423_Oep.toString());
          rg.MoverA(vEao.vEao_Oev, ((MSGGT423.vOev)g_MsgGT423.vGT423_Oev.elementAt(i)).GT423_Oev.toString());
          rg.MoverA(vEao.vEao_Ep1, rg.Zeros(vEao.vEao_Ep1));
          rg.MoverA(vEao.vEao_Ev1, rg.Zeros(vEao.vEao_Ev1));
          rg.MoverA(vEao.vEao_Ep2, rg.Zeros(vEao.vEao_Ep2));
          rg.MoverA(vEao.vEao_Ev2, rg.Zeros(vEao.vEao_Ev2));
          v_Eao.add(vEao);
        }               
    int i = 0;
    i = Integer.parseInt(g_MsgGT423.GT423_Idx.toString()) - 1;
    MSGGT423.Bff_MsgGT423 d_MsgGT423 = new MSGGT423.Bff_MsgGT423();
    d_MsgGT423 = (MSGGT423.Bff_MsgGT423)g_MsgGT423.GT423_Tab.elementAt(i);
    rg.MoverA(g_MsgGT423.GT423_Fp1, d_MsgGT423.GT423_Frv);
    for (int j=0; j<15; j++)
        {
          MSGGT423.Buf_vEao vEao = new MSGGT423.Buf_vEao();
          vEao = (MSGGT423.Buf_vEao)v_Eao.elementAt(j);
          MSGGT423.vEpp Tap = new MSGGT423.vEpp();
          Tap = (MSGGT423.vEpp)d_MsgGT423.vGT423_Epp.elementAt(j);
          rg.MoverA(vEao.vEao_Ep1, Tap.GT423_Epp);
          MSGGT423.vEpv Tav = new MSGGT423.vEpv();
          Tav = (MSGGT423.vEpv)d_MsgGT423.vGT423_Epv.elementAt(j);
          rg.MoverA(vEao.vEao_Ev1, Tav.GT423_Epv);
          v_Eao.set(j, vEao);
        }
    i = i + 1;
    if  (i < Integer.parseInt(g_MsgGT423.GT423_Nro.toString()))
        {
        //MSGGT423.Bff_MsgGT423 d_MsgGT423 = new MSGGT423.Bff_MsgGT423();
          d_MsgGT423 = (MSGGT423.Bff_MsgGT423)g_MsgGT423.GT423_Tab.elementAt(i);
          rg.MoverA(g_MsgGT423.GT423_Fp2, d_MsgGT423.GT423_Frv);
          for (int j=0; j<15; j++)
              {
                MSGGT423.Buf_vEao vEao = new MSGGT423.Buf_vEao();
                vEao = (MSGGT423.Buf_vEao)v_Eao.elementAt(j);
                MSGGT423.vEpp Tap = new MSGGT423.vEpp();
                Tap = (MSGGT423.vEpp)d_MsgGT423.vGT423_Epp.elementAt(j);
                rg.MoverA(vEao.vEao_Ep2, Tap.GT423_Epp);
                MSGGT423.vEpv Tav = new MSGGT423.vEpv();
                Tav = (MSGGT423.vEpv)d_MsgGT423.vGT423_Epv.elementAt(j);
                rg.MoverA(vEao.vEao_Ev2, Tav.GT423_Epv);
                v_Eao.set(j, vEao);
              }
        }
    data.getUser().removeTemp("Agtp423_vEao");
    data.getUser().setTemp("Agtp423_vEao", v_Eao);
    //Log.debug("AGTP423[Vector.Eao]", "[" + pcStr + "]");
  }
  //---------------------------------------------------------------------------------------
  public void Actualiza_vEao(RunData data, Context context)
              throws Exception 
  {
    Vector v_Eao = new Vector();
    v_Eao = (Vector)data.getUser().getTemp("Agtp423_vEao");
    int i = 0;
    i = Integer.parseInt(g_MsgGT423.GT423_Idx.toString()) - 1;
    MSGGT423.Bff_MsgGT423 d_MsgGT423 = new MSGGT423.Bff_MsgGT423();
    d_MsgGT423 = (MSGGT423.Bff_MsgGT423)g_MsgGT423.GT423_Tab.elementAt(i);
    rg.MoverA(g_MsgGT423.GT423_Fp1, d_MsgGT423.GT423_Frv);
    for (int j=0; j<15; j++)
        {
          MSGGT423.Buf_vEao vEao = new MSGGT423.Buf_vEao();
          vEao = (MSGGT423.Buf_vEao)v_Eao.elementAt(j);
          MSGGT423.vEpp Tap = new MSGGT423.vEpp();
          Tap = (MSGGT423.vEpp)d_MsgGT423.vGT423_Epp.elementAt(j);
          rg.MoverA(vEao.vEao_Ep1, Tap.GT423_Epp);
          MSGGT423.vEpv Tav = new MSGGT423.vEpv();
          Tav = (MSGGT423.vEpv)d_MsgGT423.vGT423_Epv.elementAt(j);
          rg.MoverA(vEao.vEao_Ev1, Tav.GT423_Epv);
          v_Eao.set(j, vEao);
        }
    i = i + 1;
    if  (i < Integer.parseInt(g_MsgGT423.GT423_Nro.toString()))
        {
        //MSGGT423.Bff_MsgGT423 d_MsgGT423 = new MSGGT423.Bff_MsgGT423();
          d_MsgGT423 = (MSGGT423.Bff_MsgGT423)g_MsgGT423.GT423_Tab.elementAt(i);
          rg.MoverA(g_MsgGT423.GT423_Fp2, d_MsgGT423.GT423_Frv);
          for (int j=0; j<15; j++)
              {
                MSGGT423.Buf_vEao vEao = new MSGGT423.Buf_vEao();
                vEao = (MSGGT423.Buf_vEao)v_Eao.elementAt(j);
                MSGGT423.vEpp Tap = new MSGGT423.vEpp();
                Tap = (MSGGT423.vEpp)d_MsgGT423.vGT423_Epp.elementAt(j);
                rg.MoverA(vEao.vEao_Ep2, Tap.GT423_Epp);
                MSGGT423.vEpv Tav = new MSGGT423.vEpv();
                Tav = (MSGGT423.vEpv)d_MsgGT423.vGT423_Epv.elementAt(j);
                rg.MoverA(vEao.vEao_Ev2, Tav.GT423_Epv);
                v_Eao.set(j, vEao);
              }
        }
    data.getUser().removeTemp("Agtp423_vEao");
    data.getUser().setTemp("Agtp423_vEao", v_Eao);
    //Log.debug("AGTP423[Vector.Eao]", "[" + pcStr + "]");
  }
  //---------------------------------------------------------------------------------------
}