// Source File Name:   Agtp315.java
// Descripcion     :   Ingreso Valores (ED135, GT096)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.BF_IMG;
import com.FHTServlet.modules.global.PRMPC080;

import com.intGlobal.modules.global.MSGED135;
import com.intGlobal.modules.global.PRMED210;
import com.intGlobal.modules.global.PRMTG130;

import com.intGarantias.modules.global.GT_IMG;
import com.intGarantias.modules.global.MSGGT096;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp315 extends FHTServletAction
{
  //---------------------------------------------------------------------------------------
  public int    i = 0;
  public String lcData = "";
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  PRMPC080.PrmPC080 g_PrmPC080     = new PRMPC080.PrmPC080();
  Vector v_Tab_Img                 = new Vector();
  Vector v_Img_GtVfi               = new Vector();
  BF_IMG img                       = new BF_IMG();
  BF_IMG.Buf_Img g_Img             = new BF_IMG.Buf_Img();
  BF_IMG.Buf_Img_Bse g_Img_Base    = new BF_IMG.Buf_Img_Bse();
  GT_IMG gtimg                     = new GT_IMG();
  GT_IMG.Buf_Img_GtApe g_Img_GtApe = new GT_IMG.Buf_Img_GtApe();
  GT_IMG.Buf_Img_GtVfi g_Img_GtVfi = new GT_IMG.Buf_Img_GtVfi();
  MSGED135.Buf_MsgED135 g_MsgED135 = new MSGED135.Buf_MsgED135();
  MSGGT096.Buf_MsgGT096 g_MsgGT096 = new MSGGT096.Buf_MsgGT096();
  PRMED210.Buf_PrmED210 g_PrmED210 = new PRMED210.Buf_PrmED210();
  PRMTG130.Buf_PrmTG130 g_PrmTG130 = new PRMTG130.Buf_PrmTG130();
  //---------------------------------------------------------------------------------------
  public Agtp315()
  {
  }
  //---------------------------------------------------------------------------------------
  public void doPerform(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP315[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp315-DIN", data);
    if (g_Msg.Msg_Pgr.toString().equals("RT"))
       { doAgtp315_Continue(data, context); }
    else
       { doAgtp315_Init(data, context); }
  }
  //---------------------------------------------------------------------------------------
  public void doCancel(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP315[doCancel.start]", "[CANCELADO]");
    setTemplate(data, "Garantias,Agt,AGTP315.vm");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp315_Init(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP315[doAgtp315_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    Carga_de_Host(data, context);
    Carga_Inicial(data, context);
    data.getUser().removeTemp("Agtp315_PrmPC080");
    data.getUser().setTemp("Agtp315_PrmPC080", g_PrmPC080);
    setTemplate(data, "Garantias,Agt,AGTP315.vm" );
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp315(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP315[doAgtp315.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp315-MAN", data);
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    String Opc = data.getParameters().getString("Opc" ,"");
    if (Opc.trim().equals("R"))
       {
         //Boton Errores
         g_MsgED135 = (MSGED135.Buf_MsgED135)data.getUser().getTemp("Agtp315_MsgED135");
         g_PrmED210 = PRMED210.Inicia_PrmED210();
         rg.MoverA(g_PrmED210.ED210_Rtn, "RO");
         rg.MoverA(g_PrmED210.ED210_Ldt, g_MsgED135.ED135_Ldv);
         data.getUser().removeTemp("Agtp315_g_PrmPC080");
         data.getUser().setTemp("Agtp315_g_PrmPC080" ,g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMED210.LSet_De_PrmED210(g_PrmED210));
         BF_MSG.Link_Program("Agtp315", "Aedp210", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("D"))
       {
         //Boton Devolver
         g_PrmED210 = PRMED210.Inicia_PrmED210();
         rg.MoverA(g_PrmED210.ED210_Rtn, "RW");
         data.getUser().removeTemp("Agtp315_g_PrmPC080");
         data.getUser().setTemp("Agtp315_g_PrmPC080" ,g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMED210.LSet_De_PrmED210(g_PrmED210));
         BF_MSG.Link_Program("Agtp315", "Aedp210", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("T"))
       {
         //Imagen Cambio Tipo Documento
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp315_ImgGtApe");
         g_Img_GtVfi = (GT_IMG.Buf_Img_GtVfi)data.getUser().getTemp("Agtp315_ImgGtVfi");
         Actualiza_Datos(data, context);
         data.getUser().removeTemp("Agtp315_ImgGtVfi");
         data.getUser().setTemp("Agtp315_ImgGtVfi", g_Img_GtVfi);
         g_PrmTG130 = PRMTG130.Inicia_PrmTG130();
         rg.MoverA(g_PrmTG130.TG130_Cmp, "GRT-TDO-FIN");
         rg.MoverA(g_PrmTG130.TG130_Ncm, "Tipos de Documentos");
         data.getUser().removeTemp("Agtp315_g_PrmPC080");
         data.getUser().setTemp("Agtp315_g_PrmPC080" ,g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMTG130.LSet_De_PrmTG130(g_PrmTG130));
         BF_MSG.Link_Program("Agtp315", "Atgp130", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("B"))
       {
         //Imagen Cambio Banco
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp315_ImgGtApe");
         g_Img_GtVfi = (GT_IMG.Buf_Img_GtVfi)data.getUser().getTemp("Agtp315_ImgGtVfi");
         Actualiza_Datos(data, context);
         data.getUser().removeTemp("Agtp315_ImgGtVfi");
         data.getUser().setTemp("Agtp315_ImgGtVfi", g_Img_GtVfi);
         g_PrmTG130 = PRMTG130.Inicia_PrmTG130();
         rg.MoverA(g_PrmTG130.TG130_Cmp, "GRT-BCO-EMI");
         rg.MoverA(g_PrmTG130.TG130_Ncm, "Bancos Emisores");
         data.getUser().removeTemp("Agtp315_g_PrmPC080");
         data.getUser().setTemp("Agtp315_g_PrmPC080" ,g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMTG130.LSet_De_PrmTG130(g_PrmTG130));
         BF_MSG.Link_Program("Agtp315", "Atgp130", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("S"))
       {
         //Boton Salir
         data.getUser().removeTemp("Agtp315_MsgED135");
         data.getUser().removeTemp("Agtp315_vTabImg");
         data.getUser().removeTemp("Agtp315_vImgGtVfi");
         data.getUser().removeTemp("Agtp315_ImgBase");
         data.getUser().removeTemp("Agtp315_ImgGtApe");
         data.getUser().removeTemp("Agtp315_PrmPC080");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp315", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("O"))
       {
         //Boton OK (Aceptar)
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp315_ImgGtApe");
         v_Img_GtVfi = (Vector)data.getUser().getTemp("Agtp315_vImgGtVfi");
         Vector g_Tab_Img = A_Tab_Img(data);
         data.getUser().removeTemp("Agtp305_gTabImg");
         data.getUser().setTemp("Agtp305_gTabImg", g_Tab_Img);
         //img.Graba_Img_Host(g_Msg, g_Tab_Img, data, context);
         //g_MsgED135 = MSGED135.Inicia_MsgED135();
         //rg.MoverA(g_MsgED135.ED135_Idr, "M");
         //rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
         //rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
         //rg.MoverA(g_MsgED135.ED135_Est, "");
         //rg.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
         //rg.MoverA(g_MsgED135.ED135_Vlc, g_Img_GtApe.GtApe_Vcn);
         //if (g_Img_GtApe.GtApe_Ggr.toString().trim().equals("DMN")
         // || g_Img_GtApe.GtApe_Ggr.toString().trim().equals("DMX"))
         //   { rg.MoverA(g_MsgED135.ED135_Ezt, "ENVSD"); }
         //if (g_Img_GtApe.GtApe_Ggr.toString().trim().equals("PMN")
         // || g_Img_GtApe.GtApe_Ggr.toString().trim().equals("PMX"))
         //   { rg.MoverA(g_MsgED135.ED135_Ezt, "ENSPR"); }
         //rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
         //g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
         data.getUser().removeTemp("Agtp315_MsgED135");
         data.getUser().removeTemp("Agtp315_vTabImg");
         data.getUser().removeTemp("Agtp315_vImgGtVfi");
         data.getUser().removeTemp("Agtp315_ImgBase");
         data.getUser().removeTemp("Agtp315_ImgGtApe");
         data.getUser().removeTemp("Agtp315_PrmPC080");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp315", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("C"))
       {
         //Boton Cursar
         g_MsgED135 = MSGED135.Inicia_MsgED135();
         rg.MoverA(g_MsgED135.ED135_Idr, "M");
         rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
         rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
         rg.MoverA(g_MsgED135.ED135_Est, "");
         rg.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
         rg.MoverA(g_MsgED135.ED135_Ezt, "TERMI");
         rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
         g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
         data.getUser().removeTemp("Agtp315_MsgED135");
         data.getUser().removeTemp("Agtp315_vTabImg");
         data.getUser().removeTemp("Agtp315_vImgGtVfi");
         data.getUser().removeTemp("Agtp315_ImgBase");
         data.getUser().removeTemp("Agtp315_ImgGtApe");
         data.getUser().removeTemp("Agtp315_PrmPC080");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp315", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("F"))
       {
         //Imagen Cambio Fecha Emision
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp315_ImgGtApe");
         g_Img_GtVfi = (GT_IMG.Buf_Img_GtVfi)data.getUser().getTemp("Agtp315_ImgGtVfi");
         Actualiza_Datos(data, context);
         data.getUser().removeTemp("Agtp315_ImgGtVfi");
         data.getUser().setTemp("Agtp315_ImgGtVfi", g_Img_GtVfi);
         setTemplate(data, "Garantias,Agt,AGTP315.vm" );
         return;
       }
    if (Opc.trim().equals("A"))
       {
         //Boton Agregar
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp315_ImgGtApe");
         g_Img_Base  = img.LSet_A_ImgBase(g_Img_GtApe.GtApe_Bse.toString());
         g_Img_GtVfi = gtimg.LSet_A_ImgGtVfi(rg.Blancos(827));
         rg.MoverA(g_Img_GtApe.GtApe_Psw, "A");
         rg.MoverA(g_Img_GtApe.GtApe_Idx, rg.Suma(g_Img_GtApe.GtApe_Tfr,0,"1",0,3,0));
         rg.MoverA(g_Img_GtVfi.GtVfi_Mnd, g_Img_Base.Img_Base_Mnd);
         rg.MoverA(g_Img_GtVfi.GtVfi_Trj, g_Img_Base.Img_Base_Trj);
         rg.MoverA(g_Img_GtVfi.GtVfi_Fem, g_Msg.Msg_Fch);
         rg.MoverA(g_Img_GtVfi.GtVfi_Vuo, rg.FmtValor("1", 0, 2, 9, "+"));
         rg.MoverA(g_Img_GtVfi.GtVfi_Vln, rg.Zeros(g_Img_GtVfi.GtVfi_Vln));
         rg.MoverA(g_Img_GtVfi.GtVfi_Vlc, rg.Zeros(g_Img_GtVfi.GtVfi_Vlc));
         rg.MoverA(g_Img_GtVfi.GtVfi_Vlt, rg.Zeros(g_Img_GtVfi.GtVfi_Vlt));
         Valor_Trj(data, context);
         data.getUser().removeTemp("Agtp315_ImgGtApe");
         data.getUser().setTemp("Agtp315_ImgGtApe", g_Img_GtApe);
         data.getUser().removeTemp("Agtp315_ImgGtVfi");
         data.getUser().setTemp("Agtp315_ImgGtVfi", g_Img_GtVfi);
         setTemplate(data, "Garantias,Agt,AGTP315.vm" );
         return;
       }
    if (Opc.trim().equals("M"))
       {
         //Boton Modificar
         String Seq = data.getParameters().getString("Seq", "");
         i = Integer.parseInt(Seq);
         v_Img_GtVfi = (Vector)data.getUser().getTemp("Agtp315_vImgGtVfi");
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp315_ImgGtApe");
         g_Img_Base  = img.LSet_A_ImgBase(g_Img_GtApe.GtApe_Bse.toString());
         rg.MoverA(g_Img_GtApe.GtApe_Psw, "M");
         rg.MoverA(g_Img_GtApe.GtApe_Idx, rg.Suma(Seq,0,"1",0,3,0));
         g_Img_GtVfi = (GT_IMG.Buf_Img_GtVfi)v_Img_GtVfi.elementAt(i);    //Solo direcciona
         lcData      = gtimg.LSet_De_ImgGtVfi(g_Img_GtVfi);               //Lineariza
         g_Img_GtVfi = gtimg.LSet_A_ImgGtVfi(lcData);                     //Obliga a Copia
         data.getUser().removeTemp("Agtp315_ImgGtApe");
         data.getUser().setTemp("Agtp315_ImgGtApe", g_Img_GtApe);
         data.getUser().removeTemp("Agtp315_ImgGtVfi");
         data.getUser().setTemp("Agtp315_ImgGtVfi", g_Img_GtVfi);
         setTemplate(data, "Garantias,Agt,AGTP315.vm" );
         return;
       }
    if (Opc.trim().equals("E"))
       {
         //Boton Eliminar
         String Seq = data.getParameters().getString("Seq", "");
         i = Integer.parseInt(Seq);
         v_Img_GtVfi = (Vector)data.getUser().getTemp("Agtp315_vImgGtVfi");
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp315_ImgGtApe");
         g_Img_Base  = img.LSet_A_ImgBase(g_Img_GtApe.GtApe_Bse.toString());
         rg.MoverA(g_Img_GtApe.GtApe_Psw, "E");
         rg.MoverA(g_Img_GtApe.GtApe_Idx, rg.Suma(Seq,0,"1",0,3,0));
         g_Img_GtVfi = (GT_IMG.Buf_Img_GtVfi)v_Img_GtVfi.elementAt(i);
         data.getUser().removeTemp("Agtp315_ImgGtApe");
         data.getUser().setTemp("Agtp315_ImgGtApe", g_Img_GtApe);
         data.getUser().removeTemp("Agtp315_ImgGtVfi");
         data.getUser().setTemp("Agtp315_ImgGtVfi", g_Img_GtVfi);
         setTemplate(data, "Garantias,Agt,AGTP315.vm" );
         return;
       }
    if (Opc.trim().equals("Y"))
       {
         //Boton Si
         String Seq = data.getParameters().getString("Seq", "0");
         i = Integer.parseInt(Seq);
         g_Img_GtVfi = (GT_IMG.Buf_Img_GtVfi)data.getUser().getTemp("Agtp315_ImgGtVfi");
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp315_ImgGtApe");
         v_Img_GtVfi = (Vector)data.getUser().getTemp("Agtp315_vImgGtVfi");
         Actualiza_Datos(data, context);
         if (g_Img_GtApe.GtApe_Psw.toString().trim().equals("A"))
            {
              v_Img_GtVfi.add(g_Img_GtVfi);
              rg.MoverA(g_Img_GtApe.GtApe_Tfr, rg.Suma(g_Img_GtApe.GtApe_Tfr,0,"1",0,3,0));
            }
         else
            {
              if (g_Img_GtApe.GtApe_Psw.toString().trim().equals("M"))
                 { v_Img_GtVfi.set(i, g_Img_GtVfi); }
              else
                 {
                   if (g_Img_GtApe.GtApe_Psw.toString().trim().equals("E"))
                      { v_Img_GtVfi.remove(i); }
                 }
            }
         Recalcula_GtApe(data, context);
         rg.MoverA(g_Img_GtApe.GtApe_Idx, " ");
         rg.MoverA(g_Img_GtApe.GtApe_Psw, " ");
         data.getUser().removeTemp("Agtp315_ImgGtApe");
         data.getUser().setTemp("Agtp315_ImgGtApe", g_Img_GtApe);
         data.getUser().removeTemp("Agtp315_ImgGtVfi");
         data.getUser().removeTemp("Agtp315_vImgGtVfi");
         data.getUser().setTemp("Agtp315_vImgGtVfi", v_Img_GtVfi);
         setTemplate(data, "Garantias,Agt,AGTP315.vm" );
         return;
       }
    if (Opc.trim().equals("N"))
       {
         //Boton No
         data.getUser().removeTemp("Agtp315_ImgGtVfi");
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp315_ImgGtApe");
         rg.MoverA(g_Img_GtApe.GtApe_Idx, " ");
         rg.MoverA(g_Img_GtApe.GtApe_Psw, " ");
         data.getUser().removeTemp("Agtp315_ImgGtApe");
         data.getUser().setTemp("Agtp315_ImgGtApe", g_Img_GtApe);
         setTemplate(data, "Garantias,Agt,AGTP315.vm" );
         return;
       }
  }
  //---------------------------------------------------------------------------------------
  public void Recalcula_GtApe(RunData data, Context context)
              throws Exception
  {
    String lcVtt = rg.Zeros(15);
    String lcVts = rg.Zeros(15);
    String lcVtb = rg.Zeros(15);
    String lcVcn = rg.Zeros(15);
    for (i=0; i<v_Img_GtVfi.size(); i++)
        {
          g_Img_GtVfi = (GT_IMG.Buf_Img_GtVfi)v_Img_GtVfi.elementAt(i);
          lcVtt = rg.Suma(lcVtt,4,g_Img_GtVfi.GtVfi_Vln,4,15,4);
          lcVts = rg.Suma(lcVts,2,g_Img_GtVfi.GtVfi_Vlt,2,15,2);
          lcVtb = rg.Suma(lcVtb,2,g_Img_GtVfi.GtVfi_Vlt,2,15,2);
          lcVcn = rg.Suma(lcVcn,2,g_Img_GtVfi.GtVfi_Vlc,2,15,2);
        }
    rg.MoverA(g_Img_GtApe.GtApe_Vtt, lcVtt);
    rg.MoverA(g_Img_GtApe.GtApe_Vts, lcVts);
    rg.MoverA(g_Img_GtApe.GtApe_Vtb, lcVtb);
    rg.MoverA(g_Img_GtApe.GtApe_Vcn, lcVcn);
  }
  //---------------------------------------------------------------------------------------
  public void Valor_Trj(RunData data, Context context)
              throws Exception
  {
    if (g_Img_GtVfi.GtVfi_Trj.toString().trim().equals("UF")
     || g_Img_GtVfi.GtVfi_Trj.toString().trim().equals("IVP")
     || g_Img_GtVfi.GtVfi_Trj.toString().trim().equals("TC"))
       {
         g_MsgGT096 = MSGGT096.Inicia_MsgGT096();
         rg.MoverA(g_MsgGT096.GT096_Idr, "V");
         rg.MoverA(g_MsgGT096.GT096_Fec, g_Img_GtVfi.GtVfi_Fem);
         rg.MoverA(g_Msg.Msg_Dat, MSGGT096.LSet_De_MsgGT096(g_MsgGT096));
         g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GT096");
         g_MsgGT096 = MSGGT096.LSet_A_MsgGT096(g_Msg.Msg_Dat.toString());
         if (g_Img_GtVfi.GtVfi_Trj.toString().trim().equals("UF"))
            { rg.MoverA(g_Img_GtVfi.GtVfi_Vuo, rg.FmtValor(g_MsgGT096.GT096_Vuf,2,2,9,"+")); }
         if (g_Img_GtVfi.GtVfi_Trj.toString().trim().equals("IVP"))
            { rg.MoverA(g_Img_GtVfi.GtVfi_Vuo, rg.FmtValor(g_MsgGT096.GT096_Viv,2,2,9,"+")); }
         if (g_Img_GtVfi.GtVfi_Trj.toString().trim().equals("TC"))
            { rg.MoverA(g_Img_GtVfi.GtVfi_Vuo, rg.FmtValor(g_MsgGT096.GT096_Vtc,2,2,9,"+")); }
       }
  }
  //---------------------------------------------------------------------------------------
  public void Actualiza_Datos(RunData data, Context context)
              throws Exception
  {
    rg.MoverA(g_Img_GtVfi.GtVfi_Ntd, data.getParameters().getString("GtVfi_Ntd", ""));
    rg.MoverA(g_Img_GtVfi.GtVfi_Nbc, data.getParameters().getString("GtVfi_Nbc", ""));
    rg.MoverA(g_Img_GtVfi.GtVfi_Ser, data.getParameters().getString("GtVfi_Ser", ""));
    rg.MoverA(g_Img_GtVfi.GtVfi_Nro, data.getParameters().getString("GtVfi_Nro", ""));
    rg.MoverA(g_Img_GtVfi.GtVfi_Tpv, data.getParameters().getString("GtVfi_Tpv", ""));
    rg.MoverA(g_Img_GtVfi.GtVfi_Tdo, data.getParameters().getString("GtVfi_Tdo", ""));
    rg.MoverA(g_Img_GtVfi.GtVfi_Bco, data.getParameters().getString("GtVfi_Bco", ""));
    rg.MoverA(g_Img_GtVfi.GtVfi_Fem, data.getParameters().getString("GtVfi_Fem", ""));
    rg.MoverA(g_Img_GtVfi.GtVfi_Fvt, data.getParameters().getString("GtVfi_Fvt", ""));
    rg.MoverA(g_Img_GtVfi.GtVfi_Vln, data.getParameters().getString("GtVfi_Vln", ""));
    rg.MoverA(g_Img_GtVfi.GtVfi_Vlc, data.getParameters().getString("GtVfi_Vlc", ""));
    rg.MoverA(g_Img_GtVfi.GtVfi_Nbe, data.getParameters().getString("GtVfi_Nbe", ""));
    Valor_Trj(data, context);
    int Dcm = 0;
    if (g_Img_GtVfi.GtVfi_Trj.toString().trim().equals("MX"))
       { Dcm = 2; }
    rg.MoverA(g_Img_GtVfi.GtVfi_Vlt, rg.Multiplica(g_Img_GtVfi.GtVfi_Vln,4,g_Img_GtVfi.GtVfi_Vuo,2,15,2,Dcm));
    rg.MoverA(g_Img_GtVfi.GtVfi_Vlc, rg.Multiplica(g_Img_GtVfi.GtVfi_Vlt,2,g_Img_GtApe.GtApe_Pct,4,15,2,Dcm));
  }
  //---------------------------------------------------------------------------------------
  public void Carga_de_Host(RunData data, Context context)
              throws Exception
  {
    //v_Tab_Img = img.Carga_Img_Host(g_Msg, g_PrmPC080.PC080_Ntr.toString(), data, context);
    v_Tab_Img = (Vector)data.getUser().getTemp("Agtp305_gTabImg");
    De_Tab_Img(v_Tab_Img, data);
    if (g_Img_GtApe.GtApe_Tfr.toString().trim().equals(""))
       { rg.MoverA(g_Img_GtApe.GtApe_Tfr, rg.Zeros(3)); }
    if (g_Img_GtApe.GtApe_Fig.toString().trim().equals(""))
       {
         rg.MoverA(g_Img_GtApe.GtApe_Fig, g_Msg.Msg_Fch);
         rg.MoverA(g_Img_GtApe.GtApe_Vuo, rg.Zeros(g_Img_GtApe.GtApe_Vuo));
         rg.MoverA(g_Img_GtApe.GtApe_Fts, g_Msg.Msg_Fch);
       }
    if (g_Img_GtApe.GtApe_Vtt.toString().trim().equals(""))
       {
         rg.MoverA(g_Img_GtApe.GtApe_Vtt, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Vts, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Vtb, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Vcn, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Hvt, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Hvl, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Hvb, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Hvc, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Pvt, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Pvl, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Pvb, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Pvc, rg.Zeros(15));
       }
    data.getUser().removeTemp("Agtp315_ImgBase");
    data.getUser().setTemp("Agtp315_ImgBase", g_Img_Base);
    data.getUser().removeTemp("Agtp315_ImgGtApe");
    data.getUser().setTemp("Agtp315_ImgGtApe", g_Img_GtApe);
    data.getUser().removeTemp("Agtp315_vImgGtVfi");
    data.getUser().setTemp("Agtp315_vImgGtVfi", v_Img_GtVfi);
    data.getUser().removeTemp("Agtp315_vTabImg");
    data.getUser().setTemp("Agtp315_vTabImg", v_Tab_Img);
  }
  //---------------------------------------------------------------------------------------
  public void De_Tab_Img(Vector v_Tab_Img, RunData data)
  {
    g_Img       = img.Inicia_Img();
    lcData      = img.LSet_De_Img(g_Img);
    g_Img_Base  = img.LSet_A_ImgBase(lcData);
    g_Img_GtApe = gtimg.LSet_A_ImgGtApe(g_Img.Img_Dat.toString());
    v_Img_GtVfi.clear();
    for (i=0; i<v_Tab_Img.size(); i++)
        {
          lcData = img.LSet_De_Img((BF_IMG.Buf_Img)v_Tab_Img.elementAt(i));
          g_Img  = img.LSet_A_Img(lcData);
          if (g_Img.Img_Dax.toString().trim().equals("BASE"))
             {
               g_Img_GtApe = gtimg.LSet_A_ImgGtApe(g_Img.Img_Dat.toString());
               g_Img_Base  = img.LSet_A_ImgBase(g_Img_GtApe.GtApe_Bse.toString());
             }
          if (g_Img.Img_Dax.toString().trim().equals("ZZVFI"))
             {
               g_Img_GtVfi = gtimg.LSet_A_ImgGtVfi(g_Img.Img_Dat.toString());
               v_Img_GtVfi.add (g_Img_GtVfi);
             }
        }
  }
  //---------------------------------------------------------------------------------------
  public void Carga_Inicial(RunData data, Context context)
              throws Exception
  {
    g_MsgED135 = MSGED135.Inicia_MsgED135();
    rg.MoverA(g_MsgED135.ED135_Idr, "C");
    rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
    rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
    g_MsgED135 = MSGED135.LSet_A_MsgED135(g_Msg.Msg_Dat.toString());
    data.getUser().removeTemp("Agtp315_MsgED135");
    data.getUser().setTemp("Agtp315_MsgED135", g_MsgED135);
  }
  //---------------------------------------------------------------------------------------
  public Vector A_Tab_Img(RunData data)
  {
    v_Tab_Img = (Vector)data.getUser().getTemp("Agtp315_vTabImg");
    Vector v_Img = new Vector();
    v_Img.clear();
    rg.MoverA(g_Img.Img_Ntr, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_Img.Img_Dax, "BASE");
    rg.MoverA(g_Img.Img_Seq, "000");
    rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtApe(g_Img_GtApe));
    Vector vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
    v_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
    for (i=0; i<v_Img_GtVfi.size(); i++)
        {
          g_Img_GtVfi = (GT_IMG.Buf_Img_GtVfi)v_Img_GtVfi.elementAt(i);
          rg.MoverA(g_Img.Img_Dax, "ZZVFI");
          rg.MoverA(g_Img.Img_Seq, rg.Suma(g_Img.Img_Seq,0,"1",0,3,0));
          rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtVfi(g_Img_GtVfi));
          vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
          v_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
        }
    //--------------------------------------------------------------------------
    rg.MoverA(g_Img.Img_Seq, "000");
    for (i=0; i<v_Tab_Img.size(); i++)
        {
          lcData = img.LSet_De_Img((BF_IMG.Buf_Img)v_Tab_Img.elementAt(i));
          g_Img  = img.LSet_A_Img(lcData);
          if (g_Img.Img_Dax.toString().trim().equals("BASE")
           || g_Img.Img_Dax.toString().trim().equals("ZZVFI"))
             { }
          else
             {
               vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
               v_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
             }
        }
    //--------------------------------------------------------------------------
    return v_Img;
  }
//---------------------------------------------------------------------------------------
  public void doAgtp315_Continue(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP315[doAgtp315_Continue.start]", "[" + data.getUser().getUserName() + "]");
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Atgp130"))
       {
         g_PrmTG130 = PRMTG130.LSet_A_PrmTG130(g_Msg.Msg_Dat.toString());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp315_g_PrmPC080");
         data.getUser().removeTemp("Agtp315_g_PrmPC080");
         g_Img_GtVfi = (GT_IMG.Buf_Img_GtVfi)data.getUser().getTemp("Agtp315_ImgGtVfi");
         if (g_PrmTG130.TG130_Cmp.toString().trim().equals("GRT-TDO-FIN"))
            {
              if (g_PrmTG130.TG130_Cod.toString().trim().compareTo("")>0)
                 {
                   rg.MoverA(g_Img_GtVfi.GtVfi_Tdo, g_PrmTG130.TG130_Cod.substring(0,2));
                   rg.MoverA(g_Img_GtVfi.GtVfi_Ntd, g_PrmTG130.TG130_Dsc.substring(0,14));
                 }
            }
         if (g_PrmTG130.TG130_Cmp.toString().trim().equals("GRT-BCO-EMI"))
            {
              if (g_PrmTG130.TG130_Cod.toString().trim().compareTo("")>0)
                 {
                   rg.MoverA(g_Img_GtVfi.GtVfi_Bco, g_PrmTG130.TG130_Cod.substring(0,3));
                   rg.MoverA(g_Img_GtVfi.GtVfi_Nbc, g_PrmTG130.TG130_Dsc.substring(0,15));
                 }
            }
         data.getUser().removeTemp("Agtp315_ImgGtVfi");
         data.getUser().setTemp("Agtp315_ImgGtVfi", g_Img_GtVfi);
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP315.vm" );
         return;
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Aedp210"))
       {
         g_PrmED210 = PRMED210.LSet_A_PrmED210(g_Msg.Msg_Dat.toString());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp315_g_PrmPC080");
         data.getUser().removeTemp("Agtp315_g_PrmPC080");
         if (!g_PrmED210.ED210_Rtn.toString().equals("OK"))
            {
              //Carga_de_Host(data, context);
              //Carga_Inicial(data, context);
              data.getUser().removeTemp("Agtp315_PrmPC080");
              data.getUser().setTemp("Agtp315_PrmPC080", g_PrmPC080);
              rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
              BF_MSG.Param_Program(data, g_Msg);
              setTemplate(data, "Garantias,Agt,AGTP315.vm" );
              return;
            }
         else
            {
              g_MsgED135 = MSGED135.Inicia_MsgED135();
              rg.MoverA(g_MsgED135.ED135_Idr, "X");
              rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
              rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
              rg.MoverA(g_MsgED135.ED135_Est, "");
              rg.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
              rg.MoverA(g_MsgED135.ED135_Ldv, g_PrmED210.ED210_Ldt);
              rg.MoverA(g_MsgED135.ED135_Ezt, "DVSPR");
              rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
              g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
              data.getUser().removeTemp("Agtp315_MsgED135");
              data.getUser().removeTemp("Agtp315_vTabImg");
              data.getUser().removeTemp("Agtp315_vImgGtVfi");
              data.getUser().removeTemp("Agtp315_ImgBase");
              data.getUser().removeTemp("Agtp315_ImgGtApe");
              data.getUser().removeTemp("Agtp315_PrmPC080");
              rg.MoverA(g_PrmPC080.PC080_Rtn, "OA");
              rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
              BF_MSG.Return_Data("Agtp315", data, g_Msg);
              return;
            }
       }
  }
  //---------------------------------------------------------------------------------------
}