// Source File Name:   Agtp310.java
// Descripcion     :   Informe Fiscalia Constitucion (ED135)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.BF_LOB;
import com.FHTServlet.modules.global.BF_IMG;
import com.FHTServlet.modules.global.PRMPC080;

import com.intGlobal.modules.global.MSGED090;
import com.intGlobal.modules.global.MSGED135;
import com.intGlobal.modules.global.PRMTG123;
import com.intGlobal.modules.global.PRMTG134;
import com.intGlobal.modules.global.PRMCL095;
import com.intGlobal.modules.global.PRMED210;

import com.intGarantias.modules.global.GT_IMG;
import com.intGarantias.modules.global.PRMGT161;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp310 extends FHTServletAction
{
  //---------------------------------------------------------------------------------------
  public int i = 0;
  public String lcData = "";
  public Vector vDatos;
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  BF_LOB lob                       = new BF_LOB();
  BF_IMG img                       = new BF_IMG();
  BF_IMG.Buf_Img g_Img             = new BF_IMG.Buf_Img();
  BF_IMG.Buf_Img_Bse g_Img_Base    = new BF_IMG.Buf_Img_Bse();
  GT_IMG gtimg                     = new GT_IMG();
  GT_IMG.Buf_Img_GtApe g_Img_GtApe = new GT_IMG.Buf_Img_GtApe();
  GT_IMG.Buf_Img_GtEsl g_Img_GtEsl = new GT_IMG.Buf_Img_GtEsl();
  GT_IMG.Buf_Img_GtUsu g_Img_GtUsu = new GT_IMG.Buf_Img_GtUsu();
  GT_IMG.Buf_Img_GtPrp g_Img_GtPrp = new GT_IMG.Buf_Img_GtPrp();
  GT_IMG.Buf_Img_GtIns g_Img_GtIns = new GT_IMG.Buf_Img_GtIns();
  Vector v_Img_GtUsu               = new Vector();
  Vector v_Img_GtPrp               = new Vector();
  Vector v_Img_GtIns               = new Vector();
  PRMPC080.PrmPC080 g_PrmPC080     = new PRMPC080.PrmPC080();
  MSGED090.Buf_MsgED090 g_MsgED090 = new MSGED090.Buf_MsgED090();
  MSGED135.Buf_MsgED135 g_MsgED135 = new MSGED135.Buf_MsgED135();
  PRMTG123.Buf_PrmTG123 g_PrmTG123 = new PRMTG123.Buf_PrmTG123();
  PRMTG134.Buf_PrmTG134 g_PrmTG134 = new PRMTG134.Buf_PrmTG134();
  PRMCL095.Buf_PrmCL095 g_PrmCL095 = new PRMCL095.Buf_PrmCL095();
  PRMED210.Buf_PrmED210 g_PrmED210 = new PRMED210.Buf_PrmED210();
  PRMGT161.Buf_PrmGT161 g_PrmGT161 = new PRMGT161.Buf_PrmGT161();
  //---------------------------------------------------------------------------------------
  public Agtp310()
  {
  }
  //---------------------------------------------------------------------------------------
  public void doPerform(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP310[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp310-DIN", data);
    if (g_Msg.Msg_Pgr.toString().equals("RT"))
       { doAgtp310_Continue(data, context); }
    else
       { doAgtp310_Init(data, context); }
  }
  //---------------------------------------------------------------------------------------
  public void doCancel(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP310[doCancel.start]", "[CANCELADO]");
    setTemplate(data, "Garantias,Agt,AGTP310.vm");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp310_Init(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP310[doAgtp310_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    data.getUser().removeTemp("Agtp310_PrmPC080");
    data.getUser().setTemp("Agtp310_PrmPC080", g_PrmPC080);
    Carga_de_Host(data, context);
    Carga_Inicial(data, context);
    if (g_PrmPC080.PC080_Pai.toString().equals("CL"))
       {
         if (g_PrmPC080.PC080_Fte.toString().trim().compareTo("FIS")!=0
          && g_PrmPC080.PC080_Fte.toString().trim().compareTo("IFS")!=0
          && g_PrmPC080.PC080_Fte.toString().trim().compareTo("ABG")!=0)
            { rg.MoverA(g_PrmPC080.PC080_Rtn, "RO"); }
         else
            { rg.MoverA(g_PrmPC080.PC080_Rtn, "WR"); }
       }
    else
       {
         if (g_PrmPC080.PC080_Fte.toString().trim().compareTo("ABG")!=0)
            { rg.MoverA(g_PrmPC080.PC080_Rtn, "RO"); }
         else
            { rg.MoverA(g_PrmPC080.PC080_Rtn, "WR"); }
       }
    rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
    BF_MSG.Param_Program(data, g_Msg);
    setTemplate(data, "Garantias,Agt,AGTP310.vm" );
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp310(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP310[doAgtp310.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp310-MAN", data);
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    String Opc = data.getParameters().getString("Opc", "");
    if (Opc.trim().equals("D"))
       {
         //Boton Devolver (Pedir Nuevos Documentos)
         Actualiza_Datos(data, context);
         data.getUser().removeTemp("Agtp310_g_PrmPC080");
         data.getUser().setTemp("Agtp310_g_PrmPC080" ,g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Link_Program("Agtp310", "Aedp127", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("I"))
       {
         //Boton Imagen
         data.getUser().removeTemp("Agtp310_g_PrmPC080");
         data.getUser().setTemp("Agtp310_g_PrmPC080", g_PrmPC080);
         Actualiza_Datos(data, context);
         rg.MoverA(g_PrmPC080.PC080_Ntc, "000");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         //BF_MSG.Link_Program("Agtp310", "Agtp729", data, g_Msg);
         BF_MSG.Link_Program("Agtp310", "Appp096", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("R"))
       {
         //Boton Errores
         g_MsgED135 = (MSGED135.Buf_MsgED135)data.getUser().getTemp("Agtp310_MsgED135");
         g_PrmED210 = PRMED210.Inicia_PrmED210();
         rg.MoverA(g_PrmED210.ED210_Rtn, "RO");
         rg.MoverA(g_PrmED210.ED210_Ldt, g_MsgED135.ED135_Ldv);
         data.getUser().removeTemp("Agtp310_g_PrmPC080");
         data.getUser().setTemp("Agtp310_g_PrmPC080", g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMED210.LSet_De_PrmED210(g_PrmED210));
         BF_MSG.Link_Program("Agtp310", "Aedp210", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("V"))
       {
         //Boton Envio
         Actualiza_Datos(data, context);
         data.getUser().removeTemp("Agtp310_g_PrmPC080");
         data.getUser().setTemp("Agtp310_g_PrmPC080" ,g_PrmPC080);
         //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
         PRMPC080.PrmPC080 l_PrmPC080 = new PRMPC080.PrmPC080();
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         l_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
         rg.MoverA(l_PrmPC080.PC080_Rtn, "RO");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(l_PrmPC080));
         //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
         //BF_MSG.Link_Program("Agtp310", "Agtp320", data, g_Msg);
         BF_MSG.Link_Program("Agtp310", "Agtp130", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("T"))
       {
         //Boton Detalle
         Actualiza_Datos(data, context);
         data.getUser().removeTemp("Agtp310_g_PrmPC080");
         data.getUser().setTemp("Agtp310_g_PrmPC080" ,g_PrmPC080);
         //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
         PRMPC080.PrmPC080 l_PrmPC080 = new PRMPC080.PrmPC080();
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         l_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
         rg.MoverA(l_PrmPC080.PC080_Rtn, "RO");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(l_PrmPC080));
         //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
         String lcPrograma = "Agtp724";
         if (g_PrmPC080.PC080_Ggr.toString().trim().equals("AYF"))
            { lcPrograma = "Agtp313"; }
         if (g_PrmPC080.PC080_Ggr.toString().trim().equals("PMN")
          || g_PrmPC080.PC080_Ggr.toString().trim().equals("PMX"))
            { lcPrograma = "Agtp315"; }
         if (g_PrmPC080.PC080_Ggr.toString().trim().equals("PAC"))
            { lcPrograma = "Agtp316"; }
         if (g_PrmPC080.PC080_Ggr.toString().trim().equals("CRH")
          || g_PrmPC080.PC080_Ggr.toString().trim().equals("HYP"))
            { lcPrograma = "Agtp724"; }
         BF_MSG.Link_Program("Agtp310", lcPrograma, data, g_Msg);
         return;
       }
    if (Opc.trim().equals("P"))
       {
         //Boton Pendiente
         Actualiza_Datos(data, context);
         g_PrmED210 = PRMED210.Inicia_PrmED210();
         rg.MoverA(g_PrmED210.ED210_Rtn, "RW");
         rg.MoverA(g_PrmPC080.PC080_Amv, "P");
         data.getUser().removeTemp("Agtp310_g_PrmPC080");
         data.getUser().setTemp("Agtp310_g_PrmPC080" ,g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMED210.LSet_De_PrmED210(g_PrmED210));
         BF_MSG.Link_Program("Agtp310", "Aedp210", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("G"))
       {
         //Boton Guardar
         Actualiza_Datos(data, context);
         Vector g_Tab_Img = (Vector)data.getUser().getTemp("Agtp310_vTabImg");
         g_Tab_Img = A_Tab_Img(data, g_Tab_Img);
         img.Graba_Img_Host(g_Msg, g_Tab_Img, data, context);
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP310.vm" );
         return;
       }
    if (Opc.trim().equals("S"))
       {
         //Boton Salir
         data.getUser().removeTemp("Agtp310_PrmPC080");
         data.getUser().removeTemp("Agtp310_ImgBase");
         data.getUser().removeTemp("Agtp310_ImgGtApe");
         data.getUser().removeTemp("Agtp310_ImgGtEsl");
         data.getUser().removeTemp("Agtp310_vImgGtUsu");
         data.getUser().removeTemp("Agtp310_vImgGtPrp");
         data.getUser().removeTemp("Agtp310_vImgGtIns");
         data.getUser().removeTemp("Agtp310_vTabImg");
         data.getUser().removeTemp("Agtp310_MsgED135");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "NK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp310", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("A"))
       {
         //Boton Aceptar en Abogado
         Actualiza_Datos(data, context);
         Cursar(Opc.trim(), data, context);
         data.getUser().removeTemp("Agtp310_PrmPC080");
         data.getUser().removeTemp("Agtp310_ImgBase");
         data.getUser().removeTemp("Agtp310_ImgGtApe");
         data.getUser().removeTemp("Agtp310_ImgGtEsl");
         data.getUser().removeTemp("Agtp310_vImgGtUsu");
         data.getUser().removeTemp("Agtp310_vImgGtPrp");
         data.getUser().removeTemp("Agtp310_vImgGtIns");
         data.getUser().removeTemp("Agtp310_vTabImg");
         data.getUser().removeTemp("Agtp310_MsgED135");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp310", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("K"))
       {
         //Boton Aceptar en Fiscalia
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp310_ImgGtApe");
         g_Img_GtEsl = (GT_IMG.Buf_Img_GtEsl)data.getUser().getTemp("Agtp310_ImgGtEsl");
         Cursar(Opc.trim(), data, context);
         data.getUser().removeTemp("Agtp310_PrmPC080");
         data.getUser().removeTemp("Agtp310_ImgBase");
         data.getUser().removeTemp("Agtp310_ImgGtApe");
         data.getUser().removeTemp("Agtp310_ImgGtEsl");
         data.getUser().removeTemp("Agtp310_vImgGtUsu");
         data.getUser().removeTemp("Agtp310_vImgGtPrp");
         data.getUser().removeTemp("Agtp310_vImgGtIns");
         data.getUser().removeTemp("Agtp310_vTabImg");
         data.getUser().removeTemp("Agtp310_MsgED135");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp310", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("Z"))
       {
         //Boton Devolver a Abogado (Rechazar Fiscalia)
         g_PrmED210 = PRMED210.Inicia_PrmED210();
         rg.MoverA(g_PrmED210.ED210_Rtn, "RW");
         rg.MoverA(g_PrmPC080.PC080_Amv, "D");
         data.getUser().removeTemp("Agtp310_g_PrmPC080");
         data.getUser().setTemp("Agtp310_g_PrmPC080", g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMED210.LSet_De_PrmED210(g_PrmED210));
         BF_MSG.Link_Program("Agtp310", "Aedp210", data, g_Msg);
         return;
       }
    if (Opc.equals("U"))
       {
         //Devolver [a UVT]
         //OJO: Si es CRH, la devolucion sera al EJH para que este la devuelva a la UVT (es desde Abogado Externo)
         //     Si es HYP, la devolucion sera directa a la UVT (es desde Fiscalia)
         rg.MoverA(g_PrmPC080.PC080_Acc, "DEVUVT");
         g_PrmED210 = PRMED210.Inicia_PrmED210();
         rg.MoverA(g_PrmED210.ED210_Rtn, "RW");
         if (g_PrmPC080.PC080_Ggr.toString().trim().equals("CRH"))
            { rg.MoverA(g_PrmPC080.PC080_Amv, "W"); }
         else
            { rg.MoverA(g_PrmPC080.PC080_Amv, "U"); }
         data.getUser().removeTemp("Agtp310_g_PrmPC080");
         data.getUser().setTemp("Agtp310_g_PrmPC080", g_PrmPC080);
Log.debug("AGTP310[doAgtp310.DEVUVT].Ggr", "[" + g_PrmPC080.getGgr() + "]");
Log.debug("AGTP310[doAgtp310.DEVUVT].Amv", "[" + g_PrmPC080.getAmv() + "]");
         rg.MoverA(g_Msg.Msg_Dat, PRMED210.LSet_De_PrmED210(g_PrmED210));
         BF_MSG.Link_Program("Agtp310", "Aedp210", data, g_Msg); 
       	 return;
       }
    if (Opc.trim().equals("1"))
       {
         //Consulta Abogados Disponibles
         Actualiza_Datos(data, context);
         g_PrmTG134 = PRMTG134.Inicia_PrmTG134();
         data.getUser().removeTemp("Agtp310_g_PrmPC080");
         data.getUser().setTemp("Agtp310_g_PrmPC080" ,g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMTG134.LSet_De_PrmTG134(g_PrmTG134));
         BF_MSG.Link_Program("Agtp310", "Atgp134", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("2"))
       {
         //Consulta Comunas Disponibles
         Actualiza_Datos(data, context);
         g_PrmTG123 = PRMTG123.Inicia_PrmTG123();
         data.getUser().removeTemp("Agtp310_g_PrmPC080");
         data.getUser().setTemp("Agtp310_g_PrmPC080" ,g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMTG123.LSet_De_PrmTG123(g_PrmTG123));
         BF_MSG.Link_Program("Agtp310", "Atgp124", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("3"))
       {
         //Consulta Usuarios Disponibles
         Actualiza_Datos(data, context);
         v_Img_GtUsu = (Vector)data.getUser().getTemp("Agtp310_vImgGtUsu");
         int liUsu = v_Img_GtUsu.size();
         String lcUsu = Integer.toString(liUsu);
         g_PrmCL095 = PRMCL095.Inicia_PrmCL095();
         rg.MoverA(g_PrmCL095.CL095_Cli, g_PrmPC080.PC080_Cli);
         rg.MoverA(g_PrmCL095.CL095_Ncl, g_PrmPC080.PC080_Ncl);
         rg.MoverA(g_PrmCL095.CL095_Idr, "E");
         rg.MoverA(g_PrmCL095.CL095_Nro, rg.FmtValor(lcUsu,0,0,2,"+"));
         for (i=0; i<v_Img_GtUsu.size(); i++)
             {
               PRMCL095.Bff_PrmCL095 d_PrmCL095 = new PRMCL095.Bff_PrmCL095();
               g_Img_GtUsu = (GT_IMG.Buf_Img_GtUsu)v_Img_GtUsu.elementAt(i);
               rg.MoverA(d_PrmCL095.CL095_Cli, g_Img_GtUsu.GtUsu_Cli);
               rg.MoverA(d_PrmCL095.CL095_Rel, "");
               rg.MoverA(d_PrmCL095.CL095_Prc, "");
               rg.MoverA(d_PrmCL095.CL095_Orp, "");
               rg.MoverA(d_PrmCL095.CL095_Ncl, g_Img_GtUsu.GtUsu_Ncl);
               rg.MoverA(d_PrmCL095.CL095_Dir, "");
               rg.MoverA(d_PrmCL095.CL095_Rcy, "");
               rg.MoverA(d_PrmCL095.CL095_Ncy, "");
               rg.MoverA(d_PrmCL095.CL095_Rrp, "");
               rg.MoverA(d_PrmCL095.CL095_Nrp, "");
               rg.MoverA(d_PrmCL095.CL095_Rrs, "");
               rg.MoverA(d_PrmCL095.CL095_Nrs, "");
               rg.MoverA(d_PrmCL095.CL095_Obl, "");
               rg.MoverA(d_PrmCL095.CL095_Isg, "");
               g_PrmCL095.CL095_Tap.set(i, d_PrmCL095); 
             }
         data.getUser().removeTemp("Agtp310_g_PrmPC080");
         data.getUser().setTemp("Agtp310_g_PrmPC080" ,g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMCL095.LSet_De_PrmCL095(g_PrmCL095));
         BF_MSG.Link_Program("Agtp310", "Aclp095", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("4"))
       {
         //Consulta Propietarios Disponibles
         Actualiza_Datos(data, context);
         v_Img_GtPrp = (Vector)data.getUser().getTemp("Agtp310_vImgGtPrp");
         int liPrp = v_Img_GtPrp.size();
         String lcPrp = Integer.toString(liPrp);
         g_PrmCL095 = PRMCL095.Inicia_PrmCL095();
         rg.MoverA(g_PrmCL095.CL095_Cli, g_PrmPC080.PC080_Cli);
         rg.MoverA(g_PrmCL095.CL095_Ncl, g_PrmPC080.PC080_Ncl);
         rg.MoverA(g_PrmCL095.CL095_Idr, "F");
         rg.MoverA(g_PrmCL095.CL095_Nro, rg.FmtValor(lcPrp,0,0,2,"+"));
         for (i=0; i<v_Img_GtPrp.size(); i++)
             {
               PRMCL095.Bff_PrmCL095 d_PrmCL095 = new PRMCL095.Bff_PrmCL095();
               g_Img_GtPrp = (GT_IMG.Buf_Img_GtPrp)v_Img_GtPrp.elementAt(i);
               rg.MoverA(d_PrmCL095.CL095_Cli, g_Img_GtPrp.GtPrp_Cli);
               rg.MoverA(d_PrmCL095.CL095_Rel, "");
               rg.MoverA(d_PrmCL095.CL095_Prc, "");
               rg.MoverA(d_PrmCL095.CL095_Orp, "");
               rg.MoverA(d_PrmCL095.CL095_Ncl, g_Img_GtPrp.GtPrp_Ncl);
               rg.MoverA(d_PrmCL095.CL095_Dir, "");
               rg.MoverA(d_PrmCL095.CL095_Rcy, "");
               rg.MoverA(d_PrmCL095.CL095_Ncy, "");
               rg.MoverA(d_PrmCL095.CL095_Rrp, "");
               rg.MoverA(d_PrmCL095.CL095_Nrp, "");
               rg.MoverA(d_PrmCL095.CL095_Rrs, "");
               rg.MoverA(d_PrmCL095.CL095_Nrs, "");
               rg.MoverA(d_PrmCL095.CL095_Obl, "");
               rg.MoverA(d_PrmCL095.CL095_Isg, "");
               g_PrmCL095.CL095_Tap.set(i, d_PrmCL095); 
             }
         data.getUser().removeTemp("Agtp310_g_PrmPC080");
         data.getUser().setTemp("Agtp310_g_PrmPC080" ,g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMCL095.LSet_De_PrmCL095(g_PrmCL095));
         BF_MSG.Link_Program("Agtp310", "Aclp095", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("5"))
       {
         //Consulta Inscripciones Disponibles
         Actualiza_Datos(data, context);
         v_Img_GtIns = (Vector)data.getUser().getTemp("Agtp310_vImgGtIns");
         int liIns = v_Img_GtIns.size();
         String lcIns = Integer.toString(liIns);
         g_PrmGT161 = PRMGT161.Inicia_PrmGT161();
         rg.MoverA(g_PrmGT161.GT161_Idr, "I");
         rg.MoverA(g_PrmGT161.GT161_Nrv, rg.FmtValor(lcIns,0,0,3,"+"));
         for (i=0; i<v_Img_GtIns.size(); i++)
             {
               PRMGT161.Bff_PrmGT161 d_PrmGT161 = new PRMGT161.Bff_PrmGT161();
               g_Img_GtIns = (GT_IMG.Buf_Img_GtIns)v_Img_GtIns.elementAt(i);
               rg.MoverA(d_PrmGT161.GT161_Cti, g_Img_GtIns.GtIns_Cti);
               rg.MoverA(d_PrmGT161.GT161_Nti, g_Img_GtIns.GtIns_Nti);
               rg.MoverA(d_PrmGT161.GT161_Csv, g_Img_GtIns.GtIns_Csv);
               rg.MoverA(d_PrmGT161.GT161_Lcl, g_Img_GtIns.GtIns_Lcl);
               rg.MoverA(d_PrmGT161.GT161_Rpo, g_Img_GtIns.GtIns_Rpo);
               rg.MoverA(d_PrmGT161.GT161_Foj, g_Img_GtIns.GtIns_Foj);
               rg.MoverA(d_PrmGT161.GT161_Nin, g_Img_GtIns.GtIns_Nin);
               rg.MoverA(d_PrmGT161.GT161_Grd, g_Img_GtIns.GtIns_Grd);
               g_PrmGT161.GT161_Tap.set(i, d_PrmGT161); 
             }
         data.getUser().removeTemp("Agtp310_g_PrmPC080");
         data.getUser().setTemp("Agtp310_g_PrmPC080" ,g_PrmPC080);
         data.getUser().removeTemp("Agtp310_g_PrmGT161");
         data.getUser().setTemp("Agtp310_g_PrmGT161" ,g_PrmGT161);
         //rg.MoverA(g_Msg.Msg_Dat, PRMGT161.LSet_De_PrmGT161(g_PrmGT161));
         rg.MoverA(g_Msg.Msg_Dat,"");
         BF_MSG.Link_Program("Agtp310", "Agtp161", data, g_Msg);
         return;
       }
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp310_Continue(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP310[doAgtp310_Continue.start]", "[" + g_Msg.Msg_Pgl.toString() + "]");
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Agtp313")
     || rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Agtp315")
     || rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Agtp316")
     || rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Agtp724")
     || rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Agtp130")
     || rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Agtp320"))
       {
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp310_g_PrmPC080");
         data.getUser().removeTemp("Agtp310_g_PrmPC080");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP310.vm" );
         return;
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Aedp127"))
       {
         PRMPC080.PrmPC080 l_PrmPC080 = new PRMPC080.PrmPC080();
         l_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp310_g_PrmPC080");
         data.getUser().removeTemp("Agtp310_g_PrmPC080");
         if (l_PrmPC080.PC080_Rtn.toString().equals("NK"))
            { 
              rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
              BF_MSG.Param_Program(data, g_Msg);
              setTemplate(data, "Garantias,Agt,AGTP310.vm" );
              return;
            }
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp310_ImgGtApe");
         g_Img_GtEsl = (GT_IMG.Buf_Img_GtEsl)data.getUser().getTemp("Agtp310_ImgGtEsl");
         Vector g_Tab_Img = (Vector)data.getUser().getTemp("Agtp310_vTabImg");
         g_Tab_Img = A_Tab_Img(data, g_Tab_Img);
         img.Graba_Img_Host(g_Msg, g_Tab_Img, data, context);
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp310", data, g_Msg);
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Atgp124"))
       {
         g_PrmTG123 = PRMTG123.LSet_A_PrmTG123(g_Msg.Msg_Dat.toString());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp310_g_PrmPC080");
         data.getUser().removeTemp("Agtp310_g_PrmPC080");
         if (g_PrmTG123.TG123_Prv.toString().trim().compareTo("")!=0)
             {
               g_Img_GtEsl = (GT_IMG.Buf_Img_GtEsl)data.getUser().getTemp("Agtp310_ImgGtEsl");
               rg.MoverA(g_Img_GtEsl.GtEsl_Pcm, g_PrmTG123.TG123_Prv.toString() + g_PrmTG123.TG123_Cmn.toString());
               rg.MoverA(g_Img_GtEsl.GtEsl_Ncm, g_PrmTG123.TG123_Ncm);
               rg.MoverA(g_Img_GtEsl.GtEsl_Npv, g_PrmTG123.TG123_Npr);
               data.getUser().removeTemp("Agtp310_ImgGtEsl");
               data.getUser().setTemp("Agtp310_ImgGtEsl", g_Img_GtEsl);
             }
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP310.vm" );
         return;
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Atgp134"))
       {
         g_PrmTG134 = PRMTG134.LSet_A_PrmTG134(g_Msg.Msg_Dat.toString());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp310_g_PrmPC080");
         data.getUser().removeTemp("Agtp310_g_PrmPC080");
         if (g_PrmTG134.TG134_Abg.toString().trim().compareTo("")!=0)
            {
              g_Img_GtEsl = (GT_IMG.Buf_Img_GtEsl)data.getUser().getTemp("Agtp310_ImgGtEsl");
              rg.MoverA(g_Img_GtEsl.GtEsl_Abg, g_PrmTG134.TG134_Abg);
              rg.MoverA(g_Img_GtEsl.GtEsl_Nag, g_PrmTG134.TG134_Nag);
              data.getUser().removeTemp("Agtp310_ImgGtEsl");
              data.getUser().setTemp("Agtp310_ImgGtEsl", g_Img_GtEsl);
            }
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP310.vm" );
         return;
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Aedp210"))
       {
         g_PrmED210 = PRMED210.LSet_A_PrmED210(g_Msg.Msg_Dat.toString());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp310_g_PrmPC080");
Log.debug("AGTP310[doAgtp310_Continue.DEVUVT]", "[" + g_PrmPC080.getAmv() + "]");
         data.getUser().removeTemp("Agtp310_g_PrmPC080");
         if (g_PrmED210.ED210_Rtn.toString().equals("OK"))
            { 
              g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp310_ImgGtApe");
              g_Img_GtEsl = (GT_IMG.Buf_Img_GtEsl)data.getUser().getTemp("Agtp310_ImgGtEsl");
              if (g_PrmPC080.PC080_Amv.toString().equals("D"))
                 { CursaDevol("REABG", data, context); }
              else if (g_PrmPC080.PC080_Amv.toString().equals("W"))
                 { CursaDevol("DVIFS", data, context); }
              else if (g_PrmPC080.PC080_Amv.toString().equals("U"))
                 { CursaDevolUVT(data, context); }
              else
                 { CursaPendte(data, context); }
              rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
              rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
              BF_MSG.Return_Data("Agtp310", data, g_Msg);
            }
         else
            {
              rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
              BF_MSG.Param_Program(data, g_Msg);
              setTemplate(data, "Garantias,Agt,AGTP310.vm" );
            }
         return;
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Aclp095"))
       {
         g_PrmCL095 = PRMCL095.LSet_A_PrmCL095(g_Msg.Msg_Dat.toString());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp310_g_PrmPC080");
         data.getUser().removeTemp("Agtp310_g_PrmPC080");
         if (g_PrmCL095.CL095_Rtn.toString().equals("OK"))
            {
              PRMCL095.Bff_PrmCL095 d_PrmCL095 = new PRMCL095.Bff_PrmCL095();
              if (g_PrmCL095.CL095_Idr.toString().equals("E"))
                 {
                   v_Img_GtUsu.clear();
                   for (i=0; i<g_PrmCL095.CL095_Tap.size(); i++)
                       {
                         d_PrmCL095 = (PRMCL095.Bff_PrmCL095)g_PrmCL095.CL095_Tap.elementAt(i);
                         if (d_PrmCL095.CL095_Cli.toString().trim().equals("")) break;
                         GT_IMG.Buf_Img_GtUsu l_Img_GtUsu = new GT_IMG.Buf_Img_GtUsu();
                         rg.MoverA(l_Img_GtUsu.GtUsu_Cli, d_PrmCL095.CL095_Cli);
                         rg.MoverA(l_Img_GtUsu.GtUsu_Ncl, d_PrmCL095.CL095_Ncl);
                         rg.MoverA(l_Img_GtUsu.GtUsu_Idc, " ");
                         rg.MoverA(l_Img_GtUsu.GtUsu_Fll, " ");
                         v_Img_GtUsu.add (l_Img_GtUsu); 
                       }
                   data.getUser().removeTemp("Agtp310_vImgGtUsu");
                   data.getUser().setTemp("Agtp310_vImgGtUsu", v_Img_GtUsu);
                 }
              if (g_PrmCL095.CL095_Idr.toString().equals("F"))
                 {
                   v_Img_GtPrp.clear();
                   for (i=0; i<g_PrmCL095.CL095_Tap.size(); i++)
                       {
                         d_PrmCL095 = (PRMCL095.Bff_PrmCL095)g_PrmCL095.CL095_Tap.elementAt(i);
                         if (d_PrmCL095.CL095_Cli.toString().trim().equals("")) break;
                         GT_IMG.Buf_Img_GtPrp l_Img_GtPrp = new GT_IMG.Buf_Img_GtPrp();
                         rg.MoverA(l_Img_GtPrp.GtPrp_Cli, d_PrmCL095.CL095_Cli);
                         rg.MoverA(l_Img_GtPrp.GtPrp_Ncl, d_PrmCL095.CL095_Ncl);
                         rg.MoverA(l_Img_GtPrp.GtPrp_Fll, " ");
                         v_Img_GtPrp.add (l_Img_GtPrp); 
                       }
                   data.getUser().removeTemp("Agtp310_vImgGtPrp");
                   data.getUser().setTemp("Agtp310_vImgGtPrp", v_Img_GtPrp);
                 }
            }
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP310.vm" );
         return;
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Agtp161"))
       {
         //g_PrmGT161 = PRMGT161.LSet_A_PrmGT161(g_Msg.Msg_Dat.toString());
         g_PrmGT161 = (PRMGT161.Buf_PrmGT161)data.getUser().getTemp("Agtp310_g_PrmGT161");
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp310_g_PrmPC080");
         data.getUser().removeTemp("Agtp310_g_PrmPC080");
         data.getUser().removeTemp("Agtp310_g_PrmGT161");
         if (g_PrmGT161.GT161_Idr.toString().equals("A"))
            {
              PRMGT161.Bff_PrmGT161 d_PrmGT161 = new PRMGT161.Bff_PrmGT161();
              v_Img_GtIns.clear();
              for (i=0; i<g_PrmGT161.GT161_Tap.size(); i++)
                  {
                    d_PrmGT161 = (PRMGT161.Bff_PrmGT161)g_PrmGT161.GT161_Tap.elementAt(i);
                    if (d_PrmGT161.GT161_Cti.toString().trim().equals("")) break;
                    GT_IMG.Buf_Img_GtIns l_Img_GtIns = new GT_IMG.Buf_Img_GtIns();
                    rg.MoverA(l_Img_GtIns.GtIns_Cti, d_PrmGT161.GT161_Cti);
                    rg.MoverA(l_Img_GtIns.GtIns_Nti, d_PrmGT161.GT161_Nti);
                    rg.MoverA(l_Img_GtIns.GtIns_Csv, d_PrmGT161.GT161_Csv);
                    rg.MoverA(l_Img_GtIns.GtIns_Lcl, d_PrmGT161.GT161_Lcl);
                    rg.MoverA(l_Img_GtIns.GtIns_Rpo, d_PrmGT161.GT161_Rpo);
                    rg.MoverA(l_Img_GtIns.GtIns_Foj, d_PrmGT161.GT161_Foj);
                    rg.MoverA(l_Img_GtIns.GtIns_Nin, d_PrmGT161.GT161_Nin);
                    rg.MoverA(l_Img_GtIns.GtIns_Grd, d_PrmGT161.GT161_Grd);
                    rg.MoverA(l_Img_GtIns.GtIns_Fll, " ");
                    v_Img_GtIns.add (l_Img_GtIns); 
                  }
              data.getUser().removeTemp("Agtp310_vImgGtIns");
              data.getUser().setTemp("Agtp310_vImgGtIns", v_Img_GtIns);
            }
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP310.vm" );
         return;
       }
    //if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Agtp729"))
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Appp096"))
       {
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp310_g_PrmPC080");
         data.getUser().removeTemp("Agtp310_g_PrmPC080");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP310.vm" );
         return;
       }
  }
  //---------------------------------------------------------------------------------------
  public void Carga_de_Host(RunData data, Context context)
              throws Exception 
  {
    Vector g_Tab_Img = img.Carga_Img_Host(g_Msg, g_PrmPC080.PC080_Ntr.toString(), data, context);
    De_Tab_Img(g_Tab_Img, data);
    rg.MoverA(g_PrmPC080.PC080_Ggr, g_Img_GtApe.GtApe_Ggr);
    data.getUser().removeTemp("Agtp310_ImgBase");
    data.getUser().setTemp("Agtp310_ImgBase", g_Img_Base);
    data.getUser().removeTemp("Agtp310_ImgGtApe");
    data.getUser().setTemp("Agtp310_ImgGtApe", g_Img_GtApe);
    data.getUser().removeTemp("Agtp310_ImgGtEsl");
    data.getUser().setTemp("Agtp310_ImgGtEsl", g_Img_GtEsl);
    data.getUser().removeTemp("Agtp310_vImgGtUsu");
    data.getUser().setTemp("Agtp310_vImgGtUsu", v_Img_GtUsu);
    data.getUser().removeTemp("Agtp310_vImgGtPrp");
    data.getUser().setTemp("Agtp310_vImgGtPrp", v_Img_GtPrp);
    data.getUser().removeTemp("Agtp310_vImgGtIns");
    data.getUser().setTemp("Agtp310_vImgGtIns", v_Img_GtIns);
    data.getUser().removeTemp("Agtp310_vTabImg");
    data.getUser().setTemp("Agtp310_vTabImg", g_Tab_Img);
    data.getUser().removeTemp("Agtp310_Dsl");
    if (g_Img_GtEsl.GtEsl_Dsl.toString().trim().equals("")
      ||g_Img_GtEsl.GtEsl_Dsl.toString().trim().equals("000000000"))
       { data.getUser().setTemp("Agtp310_Dsl", ""); }
    else
       { data.getUser().setTemp("Agtp310_Dsl", lob.leeCLOB(g_Img_GtEsl.GtEsl_Dsl.toString())); }
    data.getUser().removeTemp("Agtp310_Anl");
    if (g_Img_GtEsl.GtEsl_Anl.toString().trim().equals("")
      ||g_Img_GtEsl.GtEsl_Anl.toString().trim().equals("000000000"))
       { data.getUser().setTemp("Agtp310_Anl", ""); }
    else
       { data.getUser().setTemp("Agtp310_Anl", lob.leeCLOB(g_Img_GtEsl.GtEsl_Anl.toString())); }
  }
  //---------------------------------------------------------------------------------------
  public void De_Tab_Img(Vector g_Tab_Img, RunData data)
  {
    g_Img       = img.Inicia_Img();
    lcData      = img.LSet_De_Img(g_Img);
    g_Img_Base  = img.LSet_A_ImgBase(lcData);
    g_Img_GtApe = gtimg.LSet_A_ImgGtApe(g_Img.Img_Dat.toString());
    g_Img_GtEsl = gtimg.LSet_A_ImgGtEsl(g_Img.Img_Dat.toString());
    v_Img_GtUsu.clear();
    v_Img_GtPrp.clear();
    v_Img_GtIns.clear();
    for (i=0; i<g_Tab_Img.size(); i++)
        {
          lcData = img.LSet_De_Img((BF_IMG.Buf_Img)g_Tab_Img.elementAt(i));
          g_Img  = img.LSet_A_Img(lcData);
          if (g_Img.Img_Dax.toString().trim().equals("BASE"))
             {
               g_Img_GtApe = gtimg.LSet_A_ImgGtApe(g_Img.Img_Dat.toString());
               g_Img_Base  = img.LSet_A_ImgBase(g_Img_GtApe.GtApe_Bse.toString());
             }
          if (g_Img.Img_Dax.toString().trim().equals("YESL"))
             { g_Img_GtEsl = gtimg.LSet_A_ImgGtEsl(g_Img.Img_Dat.toString()); }
          if (g_Img.Img_Dax.toString().trim().equals("YYUSU"))
             {
               g_Img_GtUsu = gtimg.LSet_A_ImgGtUsu(g_Img.Img_Dat.toString());
               v_Img_GtUsu.add (g_Img_GtUsu); 
             }
          if (g_Img.Img_Dax.toString().trim().equals("YYCMP"))
             {
               g_Img_GtUsu = gtimg.LSet_A_ImgGtUsu(g_Img.Img_Dat.toString());
               rg.MoverA(g_Img_GtUsu.GtUsu_Idc, "C");
               v_Img_GtUsu.add (g_Img_GtUsu); 
             }
          if (g_Img.Img_Dax.toString().trim().equals("YYPRP"))
             {
               g_Img_GtPrp = gtimg.LSet_A_ImgGtPrp(g_Img.Img_Dat.toString());
               v_Img_GtPrp.add (g_Img_GtPrp); 
             }
          if (g_Img.Img_Dax.toString().trim().equals("YYINS"))
             {
               g_Img_GtIns = gtimg.LSet_A_ImgGtIns(g_Img.Img_Dat.toString());
               v_Img_GtIns.add (g_Img_GtIns); 
             }
        }
    //if (v_Img_GtUsu.size()==0)
    //   {
    //     rg.MoverA(g_Img_GtUsu.GtUsu_Cli, g_PrmPC080.PC080_Cli);
    //     rg.MoverA(g_Img_GtUsu.GtUsu_Ncl, g_PrmPC080.PC080_Ncl);
    //     rg.MoverA(g_Img_GtUsu.GtUsu_Idc, " ");
    //     rg.MoverA(g_Img_GtUsu.GtUsu_Fll, " ");
    //     v_Img_GtUsu.add (g_Img_GtUsu); 
    //   }
    //if (v_Img_GtPrp.size()==0)
    //   {
    //     rg.MoverA(g_Img_GtPrp.GtPrp_Cli, g_Img_Base.Img_Base_Cli);
    //     rg.MoverA(g_Img_GtPrp.GtPrp_Ncl, g_Img_Base.Img_Base_Ncl);
    //     rg.MoverA(g_Img_GtPrp.GtPrp_Fll, " ");
    //     v_Img_GtPrp.add (g_Img_GtPrp); 
    //   }
  }
  //---------------------------------------------------------------------------------------
  public void Carga_Inicial(RunData data, Context context)
              throws Exception 
  {
    g_MsgED135 = MSGED135.Inicia_MsgED135();
    rg.MoverA(g_MsgED135.ED135_Idr, "C");
    rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
    rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
    g_MsgED135 = MSGED135.LSet_A_MsgED135(g_Msg.Msg_Dat.toString());
    data.getUser().removeTemp("Agtp310_MsgED135");
    data.getUser().setTemp("Agtp310_MsgED135", g_MsgED135);
  }
  //---------------------------------------------------------------------------------------
  public void Cursar(String Opc, RunData data, Context context)
              throws Exception 
  {
    Vector g_Tab_Img = (Vector)data.getUser().getTemp("Agtp310_vTabImg");
    g_Tab_Img = A_Tab_Img(data, g_Tab_Img);
    img.Graba_Img_Host(g_Msg, g_Tab_Img, data, context);
    g_MsgED135 = MSGED135.Inicia_MsgED135();
    rg.MoverA(g_MsgED135.ED135_Idr, "M");
    rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
    rg.MoverA(g_MsgED135.ED135_Est, "");
    rg.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
    if (g_PrmPC080.PC080_Fte.toString().trim().equals("ABG"))
       {
         if (Opc.equals("A"))
            { rg.MoverA(g_MsgED135.ED135_Ezt, "TMABG"); }
         else
            { rg.MoverA(g_MsgED135.ED135_Ezt, "TERMI"); }
       }
    else
       { rg.MoverA(g_MsgED135.ED135_Ezt, "TERMI"); }
    rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
  }
  //---------------------------------------------------------------------------------------
  public Vector A_Tab_Img(RunData data, Vector g_Tab_Img)
  {
    int    liSwt = 0;
    Vector v_Img = new Vector();
    v_Img.clear();
    for (i=0; i<g_Tab_Img.size(); i++)
        {
          lcData = img.LSet_De_Img((BF_IMG.Buf_Img)g_Tab_Img.elementAt(i));
          g_Img  = img.LSet_A_Img(lcData);
          if (g_Img.Img_Dax.toString().trim().equals("BASE"))
             { rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtApe(g_Img_GtApe)); }
          if (g_Img.Img_Dax.toString().trim().equals("YESL"))
             { 
               liSwt = 1;
               rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtEsl(g_Img_GtEsl));
             }
          if (g_Img.Img_Dax.toString().trim().equals("YYUSU")
           || g_Img.Img_Dax.toString().trim().equals("YYCMP")
           || g_Img.Img_Dax.toString().trim().equals("YYPRP")
           || g_Img.Img_Dax.toString().trim().equals("YYINS"))
             { }
          else
             {
               Vector vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
               v_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
             }
        }
    if (liSwt == 0)
       {
         rg.MoverA(g_Img.Img_Ntr, g_PrmPC080.PC080_Ntr);
         rg.MoverA(g_Img.Img_Dax, "YESL");
         rg.MoverA(g_Img.Img_Seq, "000");
         rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtEsl(g_Img_GtEsl));
         Vector vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
         v_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
       }
    v_Img_GtUsu = (Vector)data.getUser().getTemp("Agtp310_vImgGtUsu");
    rg.MoverA(g_Img.Img_Seq, "000");
    for (i=0; i<v_Img_GtUsu.size(); i++)
        {
          g_Img_GtUsu = (GT_IMG.Buf_Img_GtUsu)v_Img_GtUsu.elementAt(i);
          if (g_Img_GtUsu.GtUsu_Idc.toString().equals("C"))
             { rg.MoverA(g_Img.Img_Dax, "YYCMP"); }
          else
             { rg.MoverA(g_Img.Img_Dax, "YYUSU"); }
          rg.MoverA(g_Img.Img_Seq, rg.Suma(g_Img.Img_Seq,0,"1",0,3,0));
          rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtUsu(g_Img_GtUsu));
          Vector vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
          v_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
        }
    v_Img_GtPrp = (Vector)data.getUser().getTemp("Agtp310_vImgGtPrp");
    rg.MoverA(g_Img.Img_Seq, "000");
    for (i=0; i<v_Img_GtPrp.size(); i++)
        {
          g_Img_GtPrp = (GT_IMG.Buf_Img_GtPrp)v_Img_GtPrp.elementAt(i);
          rg.MoverA(g_Img.Img_Dax, "YYPRP");
          rg.MoverA(g_Img.Img_Seq, rg.Suma(g_Img.Img_Seq,0,"1",0,3,0));
          rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtPrp(g_Img_GtPrp));
          Vector vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
          v_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
        }
    v_Img_GtIns = (Vector)data.getUser().getTemp("Agtp310_vImgGtIns");
    rg.MoverA(g_Img.Img_Seq, "000");
    for (i=0; i<v_Img_GtIns.size(); i++)
        {
          g_Img_GtIns = (GT_IMG.Buf_Img_GtIns)v_Img_GtIns.elementAt(i);
          rg.MoverA(g_Img.Img_Dax, "YYINS");
          rg.MoverA(g_Img.Img_Seq, rg.Suma(g_Img.Img_Seq,0,"1",0,3,0));
          rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtIns(g_Img_GtIns));
          Vector vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
          v_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
        }
    return v_Img;
  }
  //---------------------------------------------------------------------------------------
  public void Actualiza_Datos(RunData data, Context context)
              throws Exception 
  {
    String lcAccion = "M";
    String lcFolio  = "";
    String lcChars  = "";
    g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp310_ImgGtApe");
    g_Img_GtEsl = (GT_IMG.Buf_Img_GtEsl)data.getUser().getTemp("Agtp310_ImgGtEsl");
    rg.MoverA(g_Img_GtEsl.GtEsl_Iln, data.getParameters().getString("GtEsl_Iln", ""));
    rg.MoverA(g_Img_GtEsl.GtEsl_Ilf, data.getParameters().getString("GtEsl_Ilf", ""));
    rg.MoverA(g_Img_GtEsl.GtEsl_Abg, data.getParameters().getString("GtEsl_Abg", ""));
    rg.MoverA(g_Img_GtEsl.GtEsl_Nag, data.getParameters().getString("GtEsl_Nag", ""));
    rg.MoverA(g_Img_GtEsl.GtEsl_Efc, data.getParameters().getString("GtEsl_Efc", ""));
    rg.MoverA(g_Img_GtEsl.GtEsl_Ent, data.getParameters().getString("GtEsl_Ent", ""));
    rg.MoverA(g_Img_GtEsl.GtEsl_Pcm, data.getParameters().getString("GtEsl_Pcm", ""));
    rg.MoverA(g_Img_GtEsl.GtEsl_Ncm, data.getParameters().getString("GtEsl_Ncm", ""));
    rg.MoverA(g_Img_GtEsl.GtEsl_Npv, data.getParameters().getString("GtEsl_Npv", ""));
    rg.MoverA(g_Img_GtEsl.GtEsl_Tta, data.getParameters().getString("GtEsl_Tta", ""));
    rg.MoverA(g_Img_GtEsl.GtEsl_Obl, data.getParameters().getString("GtEsl_Obl", ""));
    rg.MoverA(g_Img_GtApe.GtApe_Cbt, data.getParameters().getString("GtApe_Cbt", ""));
    rg.MoverA(g_Img_GtApe.GtApe_Cmp, data.getParameters().getString("GtApe_Cmp", ""));
    rg.MoverA(g_Img_GtApe.GtApe_Grd, data.getParameters().getString("GtApe_Grd", ""));
    rg.MoverA(g_Img_GtApe.GtApe_Ilm, data.getParameters().getString("GtApe_Ilm", ""));
    rg.MoverA(g_Img_GtApe.GtApe_Ulm, data.getParameters().getString("GtApe_Ulm", ""));
    rg.MoverA(g_Img_GtApe.GtApe_Mtl, data.getParameters().getString("GtApe_Mtl", ""));
    rg.MoverA(g_Img_GtApe.GtApe_Pcl, data.getParameters().getString("GtApe_Pcl", ""));
    lcChars = data.getParameters().getString("Chr_Dsl", "");
    if (!lcChars.trim().equals(""))
       { 
         if (g_Img_GtEsl.GtEsl_Dsl.toString().trim().equals("")
           ||g_Img_GtEsl.GtEsl_Dsl.toString().trim().equals("000000000"))
            { rg.MoverA(g_Img_GtEsl.GtEsl_Dsl, Asigna_Folio("FOLIO-CLOBS", data, context)); }
         lcFolio = g_Img_GtEsl.GtEsl_Dsl.toString();
         lob.mantieneCLOB(lcAccion, lcFolio, lcChars);
       }
    lcChars = data.getParameters().getString("Chr_Anl", "");
    if (!lcChars.trim().equals(""))
       { 
         if (g_Img_GtEsl.GtEsl_Anl.toString().trim().equals("")
           ||g_Img_GtEsl.GtEsl_Anl.toString().trim().equals("000000000"))
            { rg.MoverA(g_Img_GtEsl.GtEsl_Anl, Asigna_Folio("FOLIO-CLOBS", data, context)); }
         lcFolio = g_Img_GtEsl.GtEsl_Anl.toString();
         lob.mantieneCLOB(lcAccion, lcFolio, lcChars);
       }
    data.getUser().removeTemp("Agtp310_ImgGtEsl");
    data.getUser().setTemp("Agtp310_ImgGtEsl", g_Img_GtEsl);
    data.getUser().removeTemp("Agtp310_ImgGtApe");
    data.getUser().setTemp("Agtp310_ImgGtApe", g_Img_GtApe);
    data.getUser().removeTemp("Agtp310_Dsl");
    if (g_Img_GtEsl.GtEsl_Dsl.toString().trim().equals("")
      ||g_Img_GtEsl.GtEsl_Dsl.toString().trim().equals("000000000"))
       { data.getUser().setTemp("Agtp310_Dsl", ""); }
    else
       { data.getUser().setTemp("Agtp310_Dsl", lob.leeCLOB(g_Img_GtEsl.GtEsl_Dsl.toString())); }
    data.getUser().removeTemp("Agtp310_Anl");
    if (g_Img_GtEsl.GtEsl_Anl.toString().trim().equals("")
      ||g_Img_GtEsl.GtEsl_Anl.toString().trim().equals("000000000"))
       { data.getUser().setTemp("Agtp310_Anl", ""); }
    else
       { data.getUser().setTemp("Agtp310_Anl", lob.leeCLOB(g_Img_GtEsl.GtEsl_Anl.toString())); }
  }
  //---------------------------------------------------------------------------------------
  public String Asigna_Folio(String pcFolio, RunData data, Context context)
                throws Exception 
  {
    g_MsgED090 = MSGED090.Inicia_MsgED090();
    rg.MoverA(g_MsgED090.ED090_Idr, "F");
    rg.MoverA(g_MsgED090.ED090_Cod, pcFolio);
    rg.MoverA(g_Msg.Msg_Dat, MSGED090.LSet_De_MsgED090(g_MsgED090));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED090");
    g_MsgED090 = MSGED090.LSet_A_MsgED090(g_Msg.Msg_Dat.toString());
    String lcFolio = rg.FmtValor(g_MsgED090.ED090_Fol.toString(),0,0,9,"");
    return lcFolio;
  }
  //---------------------------------------------------------------------------------------
  public void CursaPendte(RunData data, Context context)
              throws Exception 
  {
    Vector g_Tab_Img = (Vector)data.getUser().getTemp("Agtp310_vTabImg");
    g_Tab_Img = A_Tab_Img(data, g_Tab_Img);
    img.Graba_Img_Host(g_Msg, g_Tab_Img, data, context);
    g_MsgED135 = MSGED135.Inicia_MsgED135();
    rg.MoverA(g_MsgED135.ED135_Idr, "X");
    rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
    rg.MoverA(g_MsgED135.ED135_Est, "");
    rg.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
    rg.MoverA(g_MsgED135.ED135_Ldv, g_PrmED210.ED210_Ldt);
    if (g_PrmPC080.PC080_Fte.toString().trim().equals("ABG"))
       {
         rg.MoverA(g_MsgED135.ED135_Ezt, "PDABG");
         rg.MoverA(g_MsgED135.ED135_Exi, "ABG");
         rg.MoverA(g_MsgED135.ED135_Exn, rg.FmtValor(g_Img_GtEsl.GtEsl_Abg,0,0,12,"+"));
       }
    else
       { rg.MoverA(g_MsgED135.ED135_Ezt, "PDFIS"); }
    rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
  }
  //---------------------------------------------------------------------------------------
  public void CursaDevol(String pcEzt, RunData data, Context context)
              throws Exception 
  {
    Vector g_Tab_Img = (Vector)data.getUser().getTemp("Agtp310_vTabImg");
    g_Tab_Img = A_Tab_Img(data, g_Tab_Img);
    img.Graba_Img_Host(g_Msg, g_Tab_Img, data, context);
    g_MsgED135 = MSGED135.Inicia_MsgED135();
    rg.MoverA(g_MsgED135.ED135_Idr, "X");
    rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
    rg.MoverA(g_MsgED135.ED135_Est, "");
    rg.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
    rg.MoverA(g_MsgED135.ED135_Ldv, g_PrmED210.ED210_Ldt);
    rg.MoverA(g_MsgED135.ED135_Ezt, pcEzt);
    if (g_PrmPC080.PC080_Fte.toString().trim().equals("ABG"))
       {
         rg.MoverA(g_MsgED135.ED135_Exi, "ABG");
         rg.MoverA(g_MsgED135.ED135_Exn, rg.FmtValor(g_Img_GtEsl.GtEsl_Abg,0,0,12,"+"));
       }
    rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
  }
  //---------------------------------------------------------------------------------------
  public void CursaDevolUVT(RunData data, Context context)
              throws Exception 
  {
    Vector g_Tab_Img = (Vector)data.getUser().getTemp("Agtp310_vTabImg");
    g_Tab_Img = A_Tab_Img(data, g_Tab_Img);
    img.Graba_Img_Host(g_Msg, g_Tab_Img, data, context);
    g_MsgED135 = MSGED135.Inicia_MsgED135();
    rg.MoverA(g_MsgED135.ED135_Idr, "U");
    rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
    rg.MoverA(g_MsgED135.ED135_Est, "");
    rg.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
    rg.MoverA(g_MsgED135.ED135_Ldv, g_PrmED210.ED210_Ldt);
Log.debug("AGTP310[CursaDevolUVT.PC080_Ggr]", "[" + g_PrmPC080.PC080_Ggr.toString().trim() + "]");
    if (g_PrmPC080.PC080_Ggr.toString().trim().equals("HYP"))
     //{ rg.MoverA(g_MsgED135.ED135_Ezt, "DVIFC"); }
       { rg.MoverA(g_MsgED135.ED135_Ezt, "DVIFS"); }
    if (g_PrmPC080.PC080_Ggr.toString().trim().equals("CRH"))
       { rg.MoverA(g_MsgED135.ED135_Ezt, "DVIFS"); }
    if (g_PrmPC080.PC080_Fte.toString().trim().equals("ABG"))
       {
         rg.MoverA(g_MsgED135.ED135_Exi, "ABG");
         rg.MoverA(g_MsgED135.ED135_Exn, rg.FmtValor(g_Img_GtEsl.GtEsl_Abg,0,0,12,"+"));
       }
    rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
  }
  //---------------------------------------------------------------------------------------
}