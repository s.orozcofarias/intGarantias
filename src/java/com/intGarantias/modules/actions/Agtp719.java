// Source File Name:   Agtp719.java
// Descripcion:        Documentacion Digitalizada (Contratos y Mandatos)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.BF_IXI;
import com.FHTServlet.modules.global.PRMPC080;

import com.intGarantias.modules.global.PRMGT719;

import java.util.*;
import org.apache.turbine.util.upload.FileItem;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp719 extends FHTServletAction
{
  //===============================================================================================================================
  public int    i = 0;
  public String lcData = "";
  public String lcRetorno = "";
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  PRMPC080.PrmPC080 g_PrmPC080     = new PRMPC080.PrmPC080();
  PRMGT719.Buf_PrmGT719 g_PrmGT719 = new PRMGT719.Buf_PrmGT719();
  //-------------------------------------------------------------------------------------------
  public Agtp719()
  {
  }
  //===============================================================================================================================
  public void doPerform(RunData data, Context context) throws Exception
  {
    //Log.debug("AGTP719[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp719-DIN", data);
    doAgtp719_Init(data, context);
  }
  //-------------------------------------------------------------------------------------------
  public void doAgtp719_Init(RunData data, Context context) throws Exception
  {
    //Log.debug("AGTP719[doAgtp719_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.getDat());
    Poblar_Panel(data, context, "I");
    setTemplate(data, "Garantias,Agt,AGTP719.vm");
  }
  //===============================================================================================================================
  public void doAgtp719(RunData data, Context context) throws Exception
  {
    //Log.debug("AGTP719[doAgtp719.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp719-MAN", data);
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.getDat());
    String Opc = data.getParameters().getString("Opc" ,"");
    if (Opc.trim().equals("S"))
       {
         //Boton Salir
         Limpieza(data, context);
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp719", data, g_Msg);
         return;
       }
    g_PrmGT719 = (PRMGT719.Buf_PrmGT719)data.getUser().getTemp("Agtp719_PrmGT719");
    if (Opc.trim().equals("y"))
       {
         //Boton File
         rg.MoverA(g_PrmGT719.GT719_Fch, g_Msg.getFch());
       //rg.MoverA(g_PrmGT719.GT719_Dsg, data.getParameters().getString("Ixi_Dsg", ""));
         rg.MoverA(g_PrmGT719.GT719_Tpi, data.getParameters().getString("Ixi_Tpi", ""));
         String wcFolio = Actualiza_Temp(data, context);
         rg.MoverA(g_PrmGT719.GT719_Fol, wcFolio);
         rg.MoverA(g_PrmGT719.GT719_Psw, "a");
         data.getUser().removeTemp("Agtp719_PrmGT719");
         data.getUser().setTemp("Agtp719_PrmGT719", g_PrmGT719);
         data.getUser().removeTemp("Appp090_Fol");
         data.getUser().setTemp("Appp090_Fol", wcFolio);
         setTemplate(data, "Garantias,Agt,AGTP719.vm");
         return;
       }
    if (Opc.trim().equals("Y"))
       {
         //Boton Si
         rg.MoverA(g_PrmGT719.GT719_Dsg, data.getParameters().getString("Ixi_Dsg", ""));
         Actualiza_Datos(data, context, "U");
         Limpieza(data, context);
         rg.MoverA(g_PrmPC080.PC080_Rtn, "DG");
         rg.MoverA(g_PrmPC080.PC080_Lob, g_PrmGT719.GT719_Fol);
         rg.MoverA(g_PrmPC080.PC080_Fll, g_PrmGT719.GT719_Fch);
         rg.MoverA(g_PrmPC080.PC080_Ccp.Dsg, g_PrmGT719.GT719_Dsg);
         rg.MoverA(g_PrmPC080.PC080_Ccp.Tpi, g_PrmGT719.GT719_Tpi);
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         Log.debug("Agtp719.doAgtp719.Opc=Y:[g_PrmPC080.PC080_Lob....]["+g_PrmPC080.PC080_Lob+"]");
         Log.debug("Agtp719.doAgtp719.Opc=Y:[g_PrmPC080.PC080_Fll....]["+g_PrmPC080.PC080_Fll+"]");
         Log.debug("Agtp719.doAgtp719.Opc=Y:[g_PrmPC080.PC080_Ccp.Dsg]["+g_PrmPC080.PC080_Ccp.Dsg+"]");
         Log.debug("Agtp719.doAgtp719.Opc=Y:[g_PrmPC080.PC080_Ccp.Tpi]["+g_PrmPC080.PC080_Ccp.Tpi+"]");
         BF_MSG.Return_Data("Agtp719", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("X"))
       {
         //Boton No
         Actualiza_Datos(data, context, "E");
         Limpieza(data, context);
         rg.MoverA(g_PrmPC080.PC080_Rtn, "ND");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp719", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("w"))
       {
         //Boton Si (Eliminar)
         Actualiza_Datos(data, context, "E");
         Limpieza(data, context);
         rg.MoverA(g_PrmPC080.PC080_Rtn, "EL");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp719", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("x"))
       {
         //Boton Salir Vista Imagen
         Limpieza(data, context);
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp719", data, g_Msg);
         return;
       }
  }
  //===============================================================================================================================
  public void Poblar_Panel(RunData data, Context context, String pcIdr) throws Exception
  {
    g_PrmGT719 = PRMGT719.Inicia_PrmGT719();
    rg.MoverA(g_PrmGT719.GT719_Idr, pcIdr);
    rg.MoverA(g_PrmGT719.GT719_Psw, g_PrmPC080.PC080_Idj);
    rg.MoverA(g_PrmGT719.GT719_Sis, g_PrmPC080.PC080_Cnr.Sis);
    rg.MoverA(g_PrmGT719.GT719_Ncn, g_PrmPC080.PC080_Cnr.Ncn);
    rg.MoverA(g_PrmGT719.GT719_Seq, g_PrmPC080.PC080_Ntc);
    rg.MoverA(g_PrmGT719.GT719_Fol, g_PrmPC080.PC080_Lob);
    rg.MoverA(g_PrmGT719.GT719_Fch, g_PrmPC080.PC080_Fll);
    rg.MoverA(g_PrmGT719.GT719_Dsg, g_PrmPC080.PC080_Ccp.Dsg);
    rg.MoverA(g_PrmGT719.GT719_Tpi, g_PrmPC080.PC080_Ccp.Tpi);
    rg.MoverA(g_PrmGT719.GT719_Fte, g_PrmPC080.PC080_Fte);
    if (g_PrmGT719.igetFol() != 0)
       {
         data.getUser().removeTemp("Appp090_Fol");
         data.getUser().setTemp("Appp090_Fol", g_PrmGT719.getFol());
       }
    data.getUser().removeTemp("Agtp719_PrmGT719");
    data.getUser().setTemp("Agtp719_PrmGT719", g_PrmGT719);
    data.getUser().removeTemp("Agtp719_PrmPC080");
    data.getUser().setTemp("Agtp719_PrmPC080", g_PrmPC080);
  }
  //-------------------------------------------------------------------------------------------
  public String Actualiza_Temp(RunData data, Context context) throws Exception
  {
    FileItem lfFile = null;
    BF_IXI.Bff_Ixi wAdjunto = new BF_IXI.Bff_Ixi();
    rg.MoverA(wAdjunto.Ixi_Idr, g_PrmGT719.GT719_Psw);
    rg.MoverA(wAdjunto.Ixi_Sis, g_PrmGT719.GT719_Sis);
    rg.MoverA(wAdjunto.Ixi_Ncn, g_PrmGT719.GT719_Ncn);
    rg.MoverA(wAdjunto.Ixi_Seq, g_PrmGT719.GT719_Seq);
    rg.MoverA(wAdjunto.Ixi_Fol, g_PrmGT719.GT719_Fol);
    rg.MoverA(wAdjunto.Ixi_Dsg, g_PrmGT719.GT719_Dsg);
    rg.MoverA(wAdjunto.Ixi_Fch, g_PrmGT719.GT719_Fch);
    rg.MoverA(wAdjunto.Ixi_Tpi, g_PrmGT719.GT719_Tpi);
    rg.MoverA(wAdjunto.Ixi_Fte, g_PrmGT719.GT719_Fte);
    try
       { lfFile = data.getParameters().getFileItem("Archivo"); }
    catch(Exception e_Msg)
       {
         e_Msg.printStackTrace();
         String MsgErr = "AGTP719[Actualiza_Temp.Exception]:[" + e_Msg.toString() + "]";
         Log.error(MsgErr);
         return "";
       //throw new fhtException(MsgErr);
       }
    String wcFolio = PRMGT719.Actualiza_IXI(g_PrmGT719.getPsw(), wAdjunto, lfFile);
    return wcFolio;
  }
  //-------------------------------------------------------------------------------------------
  public void Actualiza_Datos(RunData data, Context context, String pcPsw) throws Exception
  {
    FileItem lfFile = null;
    BF_IXI.Bff_Ixi wAdjunto = new BF_IXI.Bff_Ixi();
    rg.MoverA(wAdjunto.Ixi_Idr, pcPsw);
    rg.MoverA(wAdjunto.Ixi_Sis, g_PrmGT719.GT719_Sis);
    rg.MoverA(wAdjunto.Ixi_Ncn, g_PrmGT719.GT719_Ncn);
    rg.MoverA(wAdjunto.Ixi_Seq, g_PrmGT719.GT719_Seq);
    rg.MoverA(wAdjunto.Ixi_Fol, g_PrmGT719.GT719_Fol);
    rg.MoverA(wAdjunto.Ixi_Dsg, g_PrmGT719.GT719_Dsg);
    rg.MoverA(wAdjunto.Ixi_Fch, g_PrmGT719.GT719_Fch);
    rg.MoverA(wAdjunto.Ixi_Tpi, g_PrmGT719.GT719_Tpi);
    rg.MoverA(wAdjunto.Ixi_Fte, g_PrmGT719.GT719_Fte);
    String wcFolio = PRMGT719.Actualiza_IXI(pcPsw, wAdjunto, lfFile);
    data.getUser().removeTemp("Appp090_Fol");
  }
  //-------------------------------------------------------------------------------------------
  public void Limpieza(RunData data, Context context) throws Exception
  {
    data.getUser().removeTemp("Agtp719_PrmGT719");
    data.getUser().removeTemp("Agtp719_PrmPC080");
    data.getUser().removeTemp("Appp090_Fol");
  }
  //===============================================================================================================================
}