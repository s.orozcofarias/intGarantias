// Source File Name:   Agtp081.java

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.BF_PRA;
import com.FHTServlet.modules.global.PRMPC080;

import com.intGlobal.modules.global.PRMCL090;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp081 extends FHTServletAction
{
  //---------------------------------------------------------------------------------------
  public Vector vDatos;
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  PRMPC080.PrmPC080 g_PrmPC080     = new PRMPC080.PrmPC080();
  PRMCL090.Buf_PrmCL090 g_PrmCL090 = new PRMCL090.Buf_PrmCL090();
  //---------------------------------------------------------------------------------------
  public Agtp081()
  {
  }
  //---------------------------------------------------------------------------------------
  public void doPerform(RunData data, Context context)
              throws Exception
  {
  //Log.debug("AGTP081[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp081-DIN", data);
    if (g_Msg.Msg_Pgr.toString().equals("RT"))
       if (RUTGEN.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Appp030"))
          { doAgtp081_Init(data, context); }
       else
          { doAgtp081_Continue(data, context); }
    else
       { doAgtp081_Init(data, context); }
  }
  //---------------------------------------------------------------------------------------
  public void doCancel(RunData data, Context context)
              throws Exception
  {
  //Log.debug("AGTP081[doCancel.start]", "[CANCELADO]");
    setTemplate(data, "Garantias,Agt,AGTP081.vm");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp081_Init(RunData data, Context context)
              throws Exception
  {
  //Log.debug("AGTP081[doAgtp081_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp081-MAN", data);
    g_Msg = BF_MSG.InitIntegra("Agtp081", data, context); 
    Nueva_PrmPC080();
    data.getUser().removeTemp("Agtp081_PrmPC080");
    data.getUser().setTemp("Agtp081_PrmPC080", g_PrmPC080);
    BF_MSG.Param_Program(data, g_Msg);
    setTemplate(data, "Garantias,Agt,AGTP081.vm" );
  //Log.debug("AGTP081[doAgtp081_Init.end]", "[" + data.getUser().getUserName() + "]");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp081(RunData data, Context context)
              throws Exception
  {
    
      //String lc_MsgTot = (String)data.getUser().getTemp("g_Msg", "");
  //if (lc_MsgTot.trim().equals("") || lc_MsgTot.substring(150).trim().equals("BICE"))
  //   { 
      	 g_Msg = BF_MSG.InitIntegra("Agtp081", data, context); 
         Nueva_PrmPC080();
         BF_MSG.Param_Program(data, g_Msg);
  //   }
  //else
  //   { g_Msg = BF_MSG.Init_Program(data); }

    
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    String Opc = data.getParameters().getString("Opc", "");
    if (Opc.trim().equals("I"))
       {
       //Iniciar Centralizacion
         doAgtp081_Init(data, context);
       	 return;
       }
    if (Opc.trim().equals("S"))
       {
       //Boton Salir
         BF_MSG.Return_Program(data, g_Msg);
       	 return;
       }
    if (Opc.equals("1"))
       //Garant�as para Registro
       { 
         rg.MoverA(g_PrmPC080.PC080_Cnr.Sis, "GAR");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
       	 BF_MSG.Link_Program("Agtp081", "Aedp302", data, g_Msg);
       	 return;
       }
    if (Opc.equals("8"))
       {
         //Contratos y Mandatos en Tramitaci�n
         rg.MoverA(g_PrmPC080.PC080_Cnr.Sis, "MYC");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
       	 BF_MSG.Link_Program("Agtp081", "Aedp302", data, g_Msg);
         return;
       }
    String Ncn = data.getParameters().getString("Ncn", "");
    if (Ncn.trim().equals(""))
       {
         rg.MoverA(g_PrmPC080.PC080_Opc, Opc);
         data.getUser().removeTemp("Agtp081_g_PrmPC080");
         data.getUser().setTemp("Agtp081_g_PrmPC080" ,PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         g_PrmCL090 = PRMCL090.Inicia_PrmCL090();
         rg.MoverA(g_PrmCL090.CL090_Pcc, "N");
         rg.MoverA(g_Msg.Msg_Dat, PRMCL090.LSet_De_PrmCL090(g_PrmCL090));
         BF_MSG.Link_Program("Agtp081", "Eclp090", data, g_Msg);
         return;
       }
    else
       { 
         rg.MoverA(g_PrmPC080.PC080_Cnr.Ncn, Ncn);
         doAgtp081_Despacha(data, context, Opc);
       }
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp081_Continue(RunData data, Context context)
              throws Exception
  {
  //Log.debug("AGTP081[doAgtp081_Continue.start]", "[" + data.getUser().getUserName() + "]");
    if (RUTGEN.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Eclp090"))
       {
         PRMPC080 vPrmPC080 = new PRMPC080();
         vDatos = vPrmPC080.LSet_A_vPrmPC080((String)data.getUser().getTemp("Agtp081_g_PrmPC080"));
         g_PrmPC080 = (PRMPC080.PrmPC080)vDatos.elementAt(0);
         data.getUser().removeTemp("Agtp081_g_PrmPC080");
         g_PrmCL090 = PRMCL090.LSet_A_PrmCL090(g_Msg.Msg_Dat.toString());
         if (g_PrmCL090.CL090_Cli.toString().trim().equals(""))
            {
              doAgtp081_Init(data, context);
              return;
            }
         rg.MoverA(g_PrmPC080.PC080_Cli, g_PrmCL090.CL090_Cli);
         rg.MoverA(g_PrmPC080.PC080_Ncl, g_PrmCL090.CL090_Ncl);
         rg.MoverA(g_PrmPC080.PC080_Tpr, g_PrmCL090.CL090_Tpr);
         rg.MoverA(g_PrmPC080.PC080_Acc, "SELECCIONA");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Link_Program("Agtp081", "Agtp400", data, g_Msg);
         return;
       }
    if (RUTGEN.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Agtp400"))
       {
         g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
         if (g_PrmPC080.PC080_Rtn.toString().trim().equals("NK"))
            { doAgtp081_Init(data, context); }
         else
            { doAgtp081_Despacha(data, context, g_PrmPC080.PC080_Opc.toString().trim()); }
         return;
       }
    doAgtp081_Init(data, context);
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp081_Despacha(RunData data, Context context, String Opc)
              throws Exception
  {
    rg.MoverA(g_PrmPC080.PC080_Rtn, " ");
    String lcPrograma = "";
  //Log.debug("AGTP081[doAgtp081_Despacha.start]", "[" + data.getUser().getUserName() + "]");
    if (Opc.equals("5"))
       //Revalorizaci�n Garant�a
       {
         rg.MoverA(g_PrmPC080.PC080_Acc, "REVAL");
         rg.MoverA(g_PrmPC080.PC080_Ttr, "REVAL");
         rg.MoverA(g_PrmPC080.PC080_Ntt, "REVALORIZACION GARANTIA");
         lcPrograma = "Agtp626";
       }
    if (Opc.equals("2"))
       //Renovaci�n Seguro Garant�a
       {
         rg.MoverA(g_PrmPC080.PC080_Acc, "RVSEG");
         rg.MoverA(g_PrmPC080.PC080_Ttr, "RVSEG");
         rg.MoverA(g_PrmPC080.PC080_Ntt, "RENOVACION SEGURO DE GARANTIA");
         lcPrograma = "Agtp625";
       }
    if (Opc.equals("3"))
       //Conecci�n Cr�dito/Garant�a
       {
         rg.MoverA(g_PrmPC080.PC080_Acc, "CRGES");
         rg.MoverA(g_PrmPC080.PC080_Ttr, "CRGES");
         rg.MoverA(g_PrmPC080.PC080_Ntt, "CONECCION CREDITO/GARANTIA");
         lcPrograma = "Agtp175";
       }
    if (Opc.equals("4"))
       //Anulaci�n Ultima Transacci�n
       {
         rg.MoverA(g_PrmPC080.PC080_Acc, "ANULA");
         rg.MoverA(g_PrmPC080.PC080_Ttr, "ANULA");
         rg.MoverA(g_PrmPC080.PC080_Ntt, "ANULA ULT.TTR. GARANTIA");
         lcPrograma = "Agtp240";
       }
    rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
    //BF_MSG.Link_Program("Agtp081", lcPrograma, data, g_Msg);
  if (BF_PRA.getPra("FLAG-BPM",1).equals("1"))
     { BF_MSG.Link_Program("Agtp081", "Appp219", data, g_Msg); }
  else
     { BF_MSG.Link_Program("Agtp081", lcPrograma, data, g_Msg); }
  }
  //---------------------------------------------------------------------------------------
  public void Nueva_PrmPC080() throws Exception
  {
    g_PrmPC080 = PRMPC080.Inicia_PrmPC080();
    rg.MoverA(g_PrmPC080.PC080_Fte,     "OPE");
    rg.MoverA(g_PrmPC080.PC080_Pai,     "CL");
    rg.MoverA(g_PrmPC080.PC080_Cnr.Sis, "GAR");
    rg.MoverA(g_PrmPC080.PC080_Slr.Slc, rg.Zeros(7));
    rg.MoverA(g_PrmPC080.PC080_Suc,     g_Msg.Msg_Suc);
    rg.MoverA(g_PrmPC080.PC080_Nsu,     g_Msg.Msg_Nsu);
    rg.MoverA(g_PrmPC080.PC080_Mnd,     "000");
    rg.MoverA(g_PrmPC080.PC080_Nmn,     "PESOS CHILE");
    rg.MoverA(g_PrmPC080.PC080_Eje,     g_Msg.Msg_Cus);
    rg.MoverA(g_PrmPC080.PC080_Nej,     g_Msg.Msg_Nus);
    rg.MoverA(g_PrmPC080.PC080_Tej,     g_Msg.Msg_Fus);
    rg.MoverA(g_PrmPC080.PC080_Idj,     "S");
    
    if (BF_PRA.getPra("FLAG-CENTRA",1).equals("1"))
       {
       	Log.debug("FLAG-CENTRA:"+g_PrmPC080.getSuc());
         if (!g_PrmPC080.getSuc().equals("001"))
            { rg.MoverA(g_PrmPC080.PC080_Idj, "N"); }
       }
    rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
  }
  //---------------------------------------------------------------------------------------
}