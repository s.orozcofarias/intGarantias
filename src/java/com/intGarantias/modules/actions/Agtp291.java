// Source File Name:   Agtp291.java
// Descripcion     :   Impresion Liquidacion (-)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.MSGTXCUR;

import com.intGarantias.modules.global.GT_LIQ;
import com.intGarantias.modules.global.MSGGT902;

import java.util.*;
import org.apache.turbine.util.*;
import org.apache.velocity.context.Context;

public class Agtp291 extends FHTServletAction
{
  //---------------------------------------------------------------------------------------
  int i = 0;
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  MSGTXCUR.Buf_Tx900 g_MsgTx900    = new MSGTXCUR.Buf_Tx900();
  GT_LIQ gtliq                     = new GT_LIQ();
  GT_LIQ.Buf_TxGT900 g_TxGT900     = new GT_LIQ.Buf_TxGT900();
  GT_LIQ.Buf_Liq_GtGbl g_Liq_GtGbl = new GT_LIQ.Buf_Liq_GtGbl();
  GT_LIQ.Buf_Liq_GtPts g_Liq_GtPts = new GT_LIQ.Buf_Liq_GtPts();
  MSGGT902.Buf_MsgGT902 g_MsgGT902 = new MSGGT902.Buf_MsgGT902();
  //---------------------------------------------------------------------------------------
  public Agtp291()
  {
  }
  //---------------------------------------------------------------------------------------
  public void doPerform(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP291[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp291-DIN", data);
    doAgtp291_Init(data, context);
  }
  //---------------------------------------------------------------------------------------
  public void doCancel(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP291[doCancel.start]", "[CANCELADO]");
    setTemplate(data, "Control,AGTP291.vm");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp291_Init(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP291[doAgtp291_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_MsgTx900  = (MSGTXCUR.Buf_Tx900)data.getUser().getTemp("Agtp291_MsgTx900");
    //-------------------------------------------------------------------------------------
    rg.MoverA(g_TxGT900.GT900_Ntr, g_MsgTx900.Tx900_Ntr);
    rg.MoverA(g_TxGT900.GT900_Ttr, g_MsgTx900.Tx900_Ttr);
    rg.MoverA(g_TxGT900.GT900_Ntt, g_MsgTx900.Tx900_Ntt);
    rg.MoverA(g_TxGT900.GT900_Fla, g_MsgTx900.Tx900_Fla);
    rg.MoverA(g_TxGT900.GT900_Fem, g_MsgTx900.Tx900_Fem);
    rg.MoverA(g_TxGT900.GT900_Hrm, g_MsgTx900.Tx900_Hrm);
    rg.MoverA(g_TxGT900.GT900_Suc, g_MsgTx900.Tx900_Suc);
    rg.MoverA(g_TxGT900.GT900_Nsu, g_MsgTx900.Tx900_Nsu);
    rg.MoverA(g_TxGT900.GT900_Eje, g_MsgTx900.Tx900_Eje);
    rg.MoverA(g_TxGT900.GT900_Nej, g_MsgTx900.Tx900_Nej);
    rg.MoverA(g_TxGT900.GT900_Tej, g_MsgTx900.Tx900_Tej);
    rg.MoverA(g_TxGT900.GT900_Cli, g_MsgTx900.Tx900_Cli);
    rg.MoverA(g_TxGT900.GT900_Ncl, g_MsgTx900.Tx900_Ncl);
    rg.MoverA(g_TxGT900.GT900_Dir, g_MsgTx900.Tx900_Dir);
    rg.MoverA(g_TxGT900.GT900_Cnr, g_MsgTx900.Tx900_Cnr);
    rg.MoverA(g_TxGT900.GT900_Dcn, g_MsgTx900.Tx900_Dcn);
    rg.MoverA(g_TxGT900.GT900_Ndc, g_MsgTx900.Tx900_Ndc);
    if (g_MsgTx900.Tx900_Ttr.toString().equals("PGTSD"))
       { 
       	 g_Liq_GtPts = GT_LIQ.LSet_A_LiqGtPts(g_MsgTx900.Tx900_Dta.toString()); 
         rg.MoverA(g_TxGT900.GT900_Tmn, g_Liq_GtPts.GtPts_Tmn);
         rg.MoverA(g_TxGT900.GT900_Trj, g_Liq_GtPts.GtPts_Trj);
         rg.MoverA(g_TxGT900.GT900_Mnd, g_Liq_GtPts.GtPts_Mnd);
         rg.MoverA(g_TxGT900.GT900_Nmn, g_Liq_GtPts.GtPts_Nmn);
         rg.MoverA(g_TxGT900.GT900_Ggr, g_Liq_GtPts.GtPts_Ggr);
         rg.MoverA(g_TxGT900.GT900_Dsg, g_Liq_GtPts.GtPts_Dsg);
         rg.MoverA(g_TxGT900.GT900_Fec, g_Liq_GtPts.GtPts_Fec);
         rg.MoverA(g_TxGT900.GT900_Vuc, g_Liq_GtPts.GtPts_Vuc);
       }
    else
       { 
       	 g_Liq_GtGbl = GT_LIQ.LSet_A_LiqGtGbl(g_MsgTx900.Tx900_Dta.toString()); 
         rg.MoverA(g_TxGT900.GT900_Tmn, g_Liq_GtGbl.GtGbl_Tmn);
         rg.MoverA(g_TxGT900.GT900_Trj, g_Liq_GtGbl.GtGbl_Trj);
         rg.MoverA(g_TxGT900.GT900_Mnd, g_Liq_GtGbl.GtGbl_Mnd);
         rg.MoverA(g_TxGT900.GT900_Nmn, g_Liq_GtGbl.GtGbl_Nmn);
         rg.MoverA(g_TxGT900.GT900_Ggr, g_Liq_GtGbl.GtGbl_Ggr);
         rg.MoverA(g_TxGT900.GT900_Dsg, g_Liq_GtGbl.GtGbl_Dsg);
         rg.MoverA(g_TxGT900.GT900_Fec, g_Liq_GtGbl.GtGbl_Fec);
         rg.MoverA(g_TxGT900.GT900_Vuc, g_Liq_GtGbl.GtGbl_Vuc);
       }
    //-------------------------------------------------------------------------------------
    g_TxGT900.vGT900_Ipt.clear();
    if (g_MsgTx900.Tx900_Ttr.toString().equals("APERT")
     || g_MsgTx900.Tx900_Ttr.toString().equals("APMAN"))
       {
         GT_LIQ.Buf_vIpt l_vIpt = new GT_LIQ.Buf_vIpt();
         rg.MoverA(l_vIpt.vIpt_Ipt, "VALOR CONTABLE GARANTIA");
         rg.MoverA(l_vIpt.vIpt_Vcn, g_Liq_GtGbl.GtGbl_Vl1);
         g_TxGT900.vGT900_Ipt.add(l_vIpt);
       }
    else if (g_MsgTx900.Tx900_Ttr.toString().equals("REVAL")
          || g_MsgTx900.Tx900_Ttr.toString().equals("RETAS")
          || g_MsgTx900.Tx900_Ttr.toString().equals("RETPI")
          || g_MsgTx900.Tx900_Ttr.toString().equals("MDTAS")
          || g_MsgTx900.Tx900_Ttr.toString().equals("MDMAN"))
            {
              GT_LIQ.Buf_vIpt l1_vIpt = new GT_LIQ.Buf_vIpt();
              rg.MoverA(l1_vIpt.vIpt_Ipt, "VALOR CONTABLE ANTERIOR");
              rg.MoverA(l1_vIpt.vIpt_Vcn, g_Liq_GtGbl.GtGbl_Vl1);
              g_TxGT900.vGT900_Ipt.add(l1_vIpt);
              GT_LIQ.Buf_vIpt l2_vIpt = new GT_LIQ.Buf_vIpt();
              rg.MoverA(l2_vIpt.vIpt_Ipt, "VALOR CONTABLE ACTUAL");
              rg.MoverA(l2_vIpt.vIpt_Vcn, g_Liq_GtGbl.GtGbl_Vl2);
              g_TxGT900.vGT900_Ipt.add(l2_vIpt);
            }
    else if (g_MsgTx900.Tx900_Ttr.toString().equals("ALZMT")
          || g_MsgTx900.Tx900_Ttr.toString().equals("ALZPI")
          || g_MsgTx900.Tx900_Ttr.toString().equals("AZMAN"))
            {
              GT_LIQ.Buf_vIpt l_vIpt = new GT_LIQ.Buf_vIpt();
              rg.MoverA(l_vIpt.vIpt_Ipt, "VALOR CONTABLE ALZADO");
              rg.MoverA(l_vIpt.vIpt_Vcn, g_Liq_GtGbl.GtGbl_Vl1);
              g_TxGT900.vGT900_Ipt.add(l_vIpt);
            }
    else if (g_MsgTx900.Tx900_Ttr.toString().equals("TERPI"))
            {
              GT_LIQ.Buf_vIpt l1_vIpt = new GT_LIQ.Buf_vIpt();
              rg.MoverA(l1_vIpt.vIpt_Ipt, "VALOR CONTABLE TERRENO ANTERIOR");
              rg.MoverA(l1_vIpt.vIpt_Vcn, g_Liq_GtGbl.GtGbl_Vl1);
              g_TxGT900.vGT900_Ipt.add(l1_vIpt);
              GT_LIQ.Buf_vIpt l2_vIpt = new GT_LIQ.Buf_vIpt();
              rg.MoverA(l2_vIpt.vIpt_Ipt, "VALOR CONTABLE TERRENO ACTUAL");
              rg.MoverA(l2_vIpt.vIpt_Vcn, g_Liq_GtGbl.GtGbl_Vl2);
              g_TxGT900.vGT900_Ipt.add(l2_vIpt);
              GT_LIQ.Buf_vIpt l3_vIpt = new GT_LIQ.Buf_vIpt();
              rg.MoverA(l3_vIpt.vIpt_Ipt, "VALOR CONTABLE AVANCES");
              rg.MoverA(l3_vIpt.vIpt_Vcn, g_Liq_GtGbl.GtGbl_Vl3);
              g_TxGT900.vGT900_Ipt.add(l3_vIpt);
              GT_LIQ.Buf_vIpt l4_vIpt = new GT_LIQ.Buf_vIpt();
              rg.MoverA(l4_vIpt.vIpt_Ipt, "VALOR CONTABLE PROYECTO ACTUAL");
              rg.MoverA(l4_vIpt.vIpt_Vcn, g_Liq_GtGbl.GtGbl_Vl4);
              g_TxGT900.vGT900_Ipt.add(l4_vIpt);
            }
    else if (!g_MsgTx900.Tx900_Ttr.toString().equals("PGTSD"))
            {
              GT_LIQ.Buf_vIpt l_vIpt = new GT_LIQ.Buf_vIpt();
              rg.MoverA(l_vIpt.vIpt_Ipt, "MOVIMIENTO CONTABLE GARANTIA");
              rg.MoverA(l_vIpt.vIpt_Vcn, g_Liq_GtGbl.GtGbl_Vl1);
              g_TxGT900.vGT900_Ipt.add(l_vIpt);
            }
         else
            {
              if (Long.parseLong(g_Liq_GtPts.GtPts_Gst.toString()) != 0)
                 {
                   GT_LIQ.Buf_vIpt l_vIpt = new GT_LIQ.Buf_vIpt();
                   rg.MoverA(l_vIpt.vIpt_Ipt, "GASTO DE TASACION");
                   rg.MoverA(l_vIpt.vIpt_Vcn, g_Liq_GtPts.GtPts_Gst);
                   g_TxGT900.vGT900_Ipt.add(l_vIpt);
                 }
              if (Long.parseLong(g_Liq_GtPts.GtPts_Hon.toString()) != 0)
                 {
                   GT_LIQ.Buf_vIpt l_vIpt = new GT_LIQ.Buf_vIpt();
                   rg.MoverA(l_vIpt.vIpt_Ipt, "HONORARIOS TASADOR");
                   rg.MoverA(l_vIpt.vIpt_Vcn, g_Liq_GtPts.GtPts_Hon);
                   g_TxGT900.vGT900_Ipt.add(l_vIpt);
                 }
              if (Long.parseLong(g_Liq_GtPts.GtPts_Rti.toString()) != 0)
                 {
                   GT_LIQ.Buf_vIpt l_vIpt = new GT_LIQ.Buf_vIpt();
                   rg.MoverA(l_vIpt.vIpt_Ipt, "IMPUESTO HONORARIOS TASADOR");
                   rg.MoverA(l_vIpt.vIpt_Vcn, g_Liq_GtPts.GtPts_Rti);
                   g_TxGT900.vGT900_Ipt.add(l_vIpt);
                 }
              if (Long.parseLong(g_Liq_GtPts.GtPts_Com.toString()) != 0)
                 {
                   GT_LIQ.Buf_vIpt l_vIpt = new GT_LIQ.Buf_vIpt();
                   rg.MoverA(l_vIpt.vIpt_Ipt, "COMISION TASACION");
                   rg.MoverA(l_vIpt.vIpt_Vcn, g_Liq_GtPts.GtPts_Com);
                   g_TxGT900.vGT900_Ipt.add(l_vIpt);
                 }
              if (Long.parseLong(g_Liq_GtPts.GtPts_Iva.toString()) != 0)
                 {
                   GT_LIQ.Buf_vIpt l_vIpt = new GT_LIQ.Buf_vIpt();
                   rg.MoverA(l_vIpt.vIpt_Ipt, "IVA COMISION TASACION");
                   rg.MoverA(l_vIpt.vIpt_Vcn, g_Liq_GtPts.GtPts_Iva);
                   g_TxGT900.vGT900_Ipt.add(l_vIpt);
                 }
              if (Long.parseLong(g_Liq_GtPts.GtPts_Tot1.toString()) != 0)
                 {
                   GT_LIQ.Buf_vIpt l_vIpt = new GT_LIQ.Buf_vIpt();
                   if (g_Liq_GtPts.GtPts_Cbp1.toString().trim().equals("OPINT"))
                      { rg.MoverA(l_vIpt.vIpt_Ipt, "TOTAL PAGO TASADOR"); }
                   else
                      { rg.MoverA(l_vIpt.vIpt_Ipt, g_Liq_GtPts.getCbp1().trim()+": "+g_Liq_GtPts.fgetDbp1()); }
                   rg.MoverA(l_vIpt.vIpt_Vcn, g_Liq_GtPts.GtPts_Tot1);
                   g_TxGT900.vGT900_Ipt.add(l_vIpt);
                 }
              if (!g_Liq_GtPts.GtPts_Tot2.toString().trim().equals("")
               && Long.parseLong(g_Liq_GtPts.GtPts_Tot2.toString()) != 0)
                 {
                   GT_LIQ.Buf_vIpt l_vIpt = new GT_LIQ.Buf_vIpt();
                   rg.MoverA(l_vIpt.vIpt_Ipt, g_Liq_GtPts.getCbp2().trim()+": "+g_Liq_GtPts.fgetDbp2());
                   rg.MoverA(l_vIpt.vIpt_Vcn, g_Liq_GtPts.GtPts_Tot2);
                   g_TxGT900.vGT900_Ipt.add(l_vIpt);
                 }
              if (!g_Liq_GtPts.GtPts_Tot3.toString().trim().equals("")
               && Long.parseLong(g_Liq_GtPts.GtPts_Tot3.toString()) != 0)
                 {
                   GT_LIQ.Buf_vIpt l_vIpt = new GT_LIQ.Buf_vIpt();
                   rg.MoverA(l_vIpt.vIpt_Ipt, g_Liq_GtPts.getCbp3().trim()+": "+g_Liq_GtPts.fgetDbp3());
                   rg.MoverA(l_vIpt.vIpt_Vcn, g_Liq_GtPts.GtPts_Tot3);
                   g_TxGT900.vGT900_Ipt.add(l_vIpt);
                 }
              if (!g_Liq_GtPts.GtPts_Tot4.toString().trim().equals("")
               && Long.parseLong(g_Liq_GtPts.GtPts_Tot4.toString()) != 0)
                 {
                   GT_LIQ.Buf_vIpt l_vIpt = new GT_LIQ.Buf_vIpt();
                   rg.MoverA(l_vIpt.vIpt_Ipt, g_Liq_GtPts.getCbp4().trim()+": "+g_Liq_GtPts.fgetDbp4());
                   rg.MoverA(l_vIpt.vIpt_Vcn, g_Liq_GtPts.GtPts_Tot4);
                   g_TxGT900.vGT900_Ipt.add(l_vIpt);
                 }
              if (!g_Liq_GtPts.GtPts_Tot5.toString().trim().equals("")
               && Long.parseLong(g_Liq_GtPts.GtPts_Tot5.toString()) != 0)
                 {
                   GT_LIQ.Buf_vIpt l_vIpt = new GT_LIQ.Buf_vIpt();
                   rg.MoverA(l_vIpt.vIpt_Ipt, g_Liq_GtPts.getCbp5().trim()+": "+g_Liq_GtPts.fgetDbp5());
                   rg.MoverA(l_vIpt.vIpt_Vcn, g_Liq_GtPts.GtPts_Tot5);
                   g_TxGT900.vGT900_Ipt.add(l_vIpt);
                 }
            }
    //-------------------------------------------------------------------------------------
    g_TxGT900.vGT900_Bns.clear();
    boolean liSwtFin = false;
    String lcIdr = "I";
    String lcNtr = g_TxGT900.GT900_Ntr.toString();
    String lcDsd = "001";
    while (liSwtFin == false)
       {
         g_MsgGT902 = MSGGT902.Inicia_MsgGT902();
         rg.MoverA(g_MsgGT902.GT902_Idr, lcIdr);
         rg.MoverA(g_MsgGT902.GT902_Ntr, lcNtr);
         rg.MoverA(g_MsgGT902.GT902_Dsd, lcDsd);
         rg.MoverA(g_Msg.Msg_Dat, MSGGT902.LSet_De_MsgGT902(g_MsgGT902));
         g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GT902");
         g_MsgGT902 = MSGGT902.LSet_A_MsgGT902(g_Msg.Msg_Dat.toString());
         int tot = Integer.parseInt(g_MsgGT902.GT902_Nro.toString());
         if (tot==0)
            { liSwtFin = true; }
         else
            {
              for (i=0; i<tot; i++)
                  {
                    GT_LIQ.Buf_vBns t_vBns = new GT_LIQ.Buf_vBns();
                    rg.MoverA(t_vBns.vBns_Itb, ((MSGGT902.Bff_MsgGT902)g_MsgGT902.GT902_Tab.elementAt(i)).GT902_Itb);
                    rg.MoverA(t_vBns.vBns_Dpc, ((MSGGT902.Bff_MsgGT902)g_MsgGT902.GT902_Tab.elementAt(i)).GT902_Dpc);
                    rg.MoverA(t_vBns.vBns_Dsb, ((MSGGT902.Bff_MsgGT902)g_MsgGT902.GT902_Tab.elementAt(i)).GT902_Dsb);
                    rg.MoverA(t_vBns.vBns_Vcn, ((MSGGT902.Bff_MsgGT902)g_MsgGT902.GT902_Tab.elementAt(i)).GT902_Vcn);
                    rg.MoverA(t_vBns.vBns_Mrj, ((MSGGT902.Bff_MsgGT902)g_MsgGT902.GT902_Tab.elementAt(i)).GT902_Mrj);
                    rg.MoverA(t_vBns.vBns_Fvb, ((MSGGT902.Bff_MsgGT902)g_MsgGT902.GT902_Tab.elementAt(i)).GT902_Fvb);
                    g_TxGT900.vGT900_Bns.add(t_vBns);
                    if (i==tot-1 && tot<MSGGT902.g_Max_GT902)
                       {
                         liSwtFin = true;
                         break;
                       }
                  }
            }
         lcIdr = "S";
         lcDsd = rg.Suma(lcDsd,0,String.valueOf(MSGGT902.g_Max_GT902),0,3,0);
       }
    //-------------------------------------------------------------------------------------
    data.getUser().removeTemp("Agtp291_TxGT900");
    data.getUser().setTemp("Agtp291_TxGT900", g_TxGT900);
    setTemplate(data, "Garantias,Agt,AGTP291.vm" );
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp291(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP291[doAgtp291.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp291-MAN", data);
    String Opc = data.getParameters().getString("Opc", "");	
    if (Opc.trim().equals("S"))                    
       { 
         //Boton Salir
         data.getUser().removeTemp("Agtp291_TxGT900");
         BF_MSG.Return_Data("Agtp291", data, g_Msg);
       }
  }
  //---------------------------------------------------------------------------------------
}