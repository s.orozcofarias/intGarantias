// Source File Name:   Agtp720.java

package com.intGarantias.modules.actions;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.InputStreamReader;
import java.util.Vector;

import org.apache.turbine.util.Log;
import org.apache.turbine.util.RunData;
import org.apache.turbine.util.upload.FileItem;
import org.apache.velocity.context.Context;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.BF_IMG;
import com.FHTServlet.modules.global.BF_LOB;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.PRMPC080;
import com.FHTServlet.modules.global.RUTGEN;
import com.intGarantias.modules.global.GT_IMG;
import com.intGarantias.modules.global.MSGGT096;
import com.intGarantias.modules.global.PRMGT093;
import com.intGlobal.modules.global.MSGED090;
import com.intGlobal.modules.global.MSGED135;

public class Agtp720 extends FHTServletAction {

	public Agtp720() {
		rg = new RUTGEN();
		msg = new BF_MSG();
		g_Msg = new com.FHTServlet.modules.global.BF_MSG.Buf_Msg();
		lob = new BF_LOB();
		g_PrmPC080 = new com.FHTServlet.modules.global.PRMPC080.PrmPC080();
		img = new BF_IMG();
		g_Img = new com.FHTServlet.modules.global.BF_IMG.Buf_Img();
		g_Img_Base = new com.FHTServlet.modules.global.BF_IMG.Buf_Img_Bse();
		gtimg = new GT_IMG();
		g_Img_GtApe = new com.intGarantias.modules.global.GT_IMG.Buf_Img_GtApe();
		g_Img_GtTsd = new com.intGarantias.modules.global.GT_IMG.Buf_Img_GtTsd();
		g_Img_GtGfu = new com.intGarantias.modules.global.GT_IMG.Buf_Img_GtGfu();
		v_Img_GtGfu = new Vector();
		g_MsgED090 = new com.intGlobal.modules.global.MSGED090.Buf_MsgED090();
		g_MsgED135 = new com.intGlobal.modules.global.MSGED135.Buf_MsgED135();
		g_MsgGT096 = new com.intGarantias.modules.global.MSGGT096.Buf_MsgGT096();
		g_PrmGT093 = new com.intGarantias.modules.global.PRMGT093.Buf_PrmGT093();
	}

	public void doPerform(RunData data, Context context) throws Exception {
		g_Msg = BF_MSG.Init_Program("Agtp720-DIN", data);
		if (g_Msg.Msg_Pgr.toString().equals("RT"))
			doAgtp720_Continue(data, context);
		else
			doAgtp720_Init(data, context);
	}

	public void doAgtp720_Init(RunData data, Context context) throws Exception {
		g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
		RUTGEN.MoverA(g_PrmPC080.PC080_Rtn, "RO");
		if (g_PrmPC080.getFte().trim().equals("TSD"))
			RUTGEN.MoverA(g_PrmPC080.PC080_Rtn, "RW");
		if (g_PrmPC080.getFte().trim().equals("OPE"))
			RUTGEN.MoverA(g_PrmPC080.PC080_Rtn, "WR");
		if (g_PrmPC080.getFte().trim().equals("SPV"))
			RUTGEN.MoverA(g_PrmPC080.PC080_Rtn, "WS");
		Log.debug("AGTP720", "PC080_Fte:[" + g_PrmPC080.getFte().trim() + "]");
		Log.debug("AGTP720", "PC080_Rtn:[" + g_PrmPC080.getRtn().trim() + "]");
		Log.debug("AGTP720", "PC080_Err:[" + g_PrmPC080.getErr().trim() + "]");
		Log.debug("AGTP720", "PC080_Opc:[" + g_PrmPC080.getOpc().trim() + "]");
		Log.debug("AGTP720", "PC080_Amv:[" + g_PrmPC080.getAmv().trim() + "]");
		Log.debug("AGTP720", "PC080_Lob:[" + g_PrmPC080.getLob().trim() + "]");
		Log.debug("AGTP720", "PC080_Acc:[" + g_PrmPC080.getAcc().trim() + "]");
		Log.debug("AGTP720", "PC080_Ttr:[" + g_PrmPC080.getTtr().trim() + "]");
		Log.debug("AGTP720", "PC080_Trt:[" + g_PrmPC080.getTrt().trim() + "]");
		Log.debug("AGTP720", "PC080_Sis:[" + g_PrmPC080.getSis().trim() + "]");
		Log.debug("AGTP720", "PC080_Ncn:[" + g_PrmPC080.getNcn().trim() + "]");
		Log.debug("AGTP720", "PC080_Cli:[" + g_PrmPC080.getCli().trim() + "]");
		Log.debug("AGTP720", "PC080_Ncl:[" + g_PrmPC080.getNcl().trim() + "]");
		Log.debug("AGTP720", "PC080_Tpr:[" + g_PrmPC080.getTpr().trim() + "]");
		Log.debug("AGTP720", "PC080_Dir:[" + g_PrmPC080.getDir().trim() + "]");
		Log.debug("AGTP720", "PC080_Tfc:[" + g_PrmPC080.getTfc().trim() + "]");
		Carga_Data(data, context);
		Carga_Tablas_Locales(data, context);
		data.getUser().removeTemp("Agtp720_PrmPC080");
		data.getUser().setTemp("Agtp720_PrmPC080", g_PrmPC080);
		RUTGEN.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
		BF_MSG.Param_Program(data, g_Msg);
		setTemplate(data, "Garantias,Agt,AGTP720.vm");
	}

	public void doAgtp720(RunData data, Context context) throws Exception {
		com.FHTServlet.modules.global.PRMPC080.PrmPC080 lwPrmPC080 = new com.FHTServlet.modules.global.PRMPC080.PrmPC080();
		g_Msg = BF_MSG.Init_Program("Agtp720-MAN", data);
		g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
		String Opc = data.getParameters().getString("Opc", "");
		if (Opc.trim().equals("S")) {
			Limpieza(data, context);
			RUTGEN.MoverA(g_PrmPC080.PC080_Rtn, "NK");
			RUTGEN.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
			BF_MSG.Return_Data("Agtp720", data, g_Msg);
			return;
		}
		if (Opc.trim().equals("R")) {
			data.getUser().removeTemp("Agtp720_g_PrmPC080");
			data.getUser().setTemp("Agtp720_g_PrmPC080", g_PrmPC080);
			lwPrmPC080 = PRMPC080.LSet_A_PrmPC080(PRMPC080
					.LSet_De_PrmPC080(g_PrmPC080));
			g_MsgED135 = (com.intGlobal.modules.global.MSGED135.Buf_MsgED135) data
					.getUser().getTemp("Agtp720_MsgED135");
			RUTGEN.MoverA(lwPrmPC080.PC080_Opc, "RO");
			RUTGEN.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(lwPrmPC080));
			BF_MSG.Link_Program("Agtp720", "Aedm211", data, g_Msg);
			return;
		}
		if (Opc.trim().equals("V")) {
			if (!g_PrmPC080.getRtn().trim().equals("RO"))
				Actualiza_Datos("", "", data, context);
			data.getUser().removeTemp("Agtp720_g_PrmPC080");
			data.getUser().setTemp("Agtp720_g_PrmPC080", g_PrmPC080);
			lwPrmPC080 = PRMPC080.LSet_A_PrmPC080(PRMPC080
					.LSet_De_PrmPC080(g_PrmPC080));
			RUTGEN.MoverA(lwPrmPC080.PC080_Rtn, "RO");
			RUTGEN.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(lwPrmPC080));
			BF_MSG.Link_Program("Agtp720", "Agtp214", data, g_Msg);
			return;
		}
		if (Opc.trim().equals("D")) {
			data.getUser().removeTemp("Agtp720_g_PrmPC080");
			data.getUser().setTemp("Agtp720_g_PrmPC080", g_PrmPC080);
			lwPrmPC080 = PRMPC080.LSet_A_PrmPC080(PRMPC080
					.LSet_De_PrmPC080(g_PrmPC080));
			RUTGEN.MoverA(lwPrmPC080.PC080_Opc, "RW");
			RUTGEN.MoverA(lwPrmPC080.PC080_Amv, "D");
			RUTGEN.MoverA(lwPrmPC080.PC080_Lob, "");
			RUTGEN.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(lwPrmPC080));
			BF_MSG.Link_Program("Agtp720", "Aedm211", data, g_Msg);
			return;
		}
		if (Opc.trim().equals("P")) {
			Actualiza_Datos("", "", data, context);
			data.getUser().removeTemp("Agtp720_g_PrmPC080");
			data.getUser().setTemp("Agtp720_g_PrmPC080", g_PrmPC080);
			lwPrmPC080 = PRMPC080.LSet_A_PrmPC080(PRMPC080
					.LSet_De_PrmPC080(g_PrmPC080));
			RUTGEN.MoverA(lwPrmPC080.PC080_Opc, "RW");
			RUTGEN.MoverA(lwPrmPC080.PC080_Amv, "P");
			RUTGEN.MoverA(lwPrmPC080.PC080_Lob, "");
			RUTGEN.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(lwPrmPC080));
			BF_MSG.Link_Program("Agtp720", "Aedm211", data, g_Msg);
			return;
		}
		if (Opc.trim().equals("G")) {
			Actualiza_Datos("", "", data, context);
			Cursatura("G", "", data, context);
			RUTGEN.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
			BF_MSG.Param_Program(data, g_Msg);
			setTemplate(data, "Garantias,Agt,AGTP720.vm");
			return;
		}
		if (Opc.trim().equals("I")) {
			if (!g_PrmPC080.getRtn().trim().equals("RO"))
				Actualiza_Datos("", "", data, context);
			data.getUser().removeTemp("Agtp720_g_PrmPC080");
			data.getUser().setTemp("Agtp720_g_PrmPC080", g_PrmPC080);
			lwPrmPC080 = PRMPC080.LSet_A_PrmPC080(PRMPC080
					.LSet_De_PrmPC080(g_PrmPC080));
			RUTGEN.MoverA(lwPrmPC080.PC080_Ntc, "000");
			if (lwPrmPC080.getFte().trim().equals("SPV"))
				RUTGEN.MoverA(lwPrmPC080.PC080_Rtn, "RO");
			RUTGEN.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(lwPrmPC080));
			BF_MSG.Link_Program("Agtp720", "Aedm096", data, g_Msg);
			return;
		}
		if (Opc.trim().equals("K")) {
			Actualiza_Datos("K", "", data, context);
			RUTGEN.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
			BF_MSG.Param_Program(data, g_Msg);
			setTemplate(data, "Garantias,Agt,AGTP720.vm");
			return;
		}
		if (Opc.trim().equals("O")) {
			Actualiza_Datos("", "", data, context);
			if (g_PrmPC080.getFte().trim().equals("OPE"))
				Cursatura("O", "UNRGT", data, context);
			else
				Cursatura("O", "TMTSD", data, context);
			Limpieza(data, context);
			RUTGEN.MoverA(g_PrmPC080.PC080_Rtn, "OK");
			RUTGEN.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
			BF_MSG.Return_Data("Agtp720", data, g_Msg);
			return;
		}
		if (Opc.trim().equals("C")) {
			Actualiza_Datos("", "", data, context);
			Cursatura("O", "TERMI", data, context);
			Limpieza(data, context);
			RUTGEN.MoverA(g_PrmPC080.PC080_Rtn, "OK");
			RUTGEN.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
			BF_MSG.Return_Data("Agtp720", data, g_Msg);
			return;
		}
		if (Opc.trim().equals("a") || Opc.trim().equals("m")
				|| Opc.trim().equals("e") || Opc.trim().equals("x")
				|| Opc.trim().equals("y")) {
			String Seq = "";
			if (Opc.trim().equals("m") || Opc.trim().equals("e")
					|| Opc.trim().equals("y"))
				Seq = data.getParameters().getString("Seq", "0");
			Actualiza_Datos(Opc, Seq, data, context);
			setTemplate(data, "Garantias,Agt,AGTP720.vm");
			return;
		}
		if (Opc.trim().equals("q") || Opc.trim().equals("s")) {
			String Seq = "";
			if (Opc.trim().equals("q"))
				Seq = data.getParameters().getString("Seq", "0");
			Actualiza_Datos(Opc, Seq, data, context);
			setTemplate(data, "Garantias,Agt,AGTP720.vm");
			return;
		}
		if (Opc.trim().equals("1")) {
			Actualiza_Datos("", "", data, context);
			data.getUser().removeTemp("Agtp720_g_PrmPC080");
			data.getUser().setTemp("Agtp720_g_PrmPC080", g_PrmPC080);
			g_PrmGT093 = PRMGT093.Inicia_PrmGT093();
			RUTGEN.MoverA(g_PrmGT093.GT093_Sis, "GAR");
			RUTGEN.MoverA(g_PrmGT093.GT093_Dcn, g_Img_Base.Img_Base_Dcn);
			RUTGEN.MoverA(g_PrmGT093.GT093_Itb, "Y");
			RUTGEN.MoverA(g_Msg.Msg_Dat, PRMGT093.LSet_De_PrmGT093(g_PrmGT093));
			BF_MSG.Link_Program("Agtp720", "Agtp093", data, g_Msg);
			return;
		}
		if (Opc.trim().equals("M")) {
			g_Img_GtApe = (com.intGarantias.modules.global.GT_IMG.Buf_Img_GtApe) data
					.getUser().getTemp("Agtp720_ImgGtApe");
			RUTGEN.MoverA(g_Img_GtApe.GtApe_Psw, "I");
			data.getUser().removeTemp("Agtp720_ImgGtApe");
			data.getUser().setTemp("Agtp720_ImgGtApe", g_Img_GtApe);
			setTemplate(data, "Garantias,Agt,AGTP720.vm");
			return;
		}
		if (Opc.trim().equals("E")) {
			g_Img_GtApe = (com.intGarantias.modules.global.GT_IMG.Buf_Img_GtApe) data
					.getUser().getTemp("Agtp720_ImgGtApe");
			v_Img_GtGfu = (Vector) data.getUser().getTemp("Agtp720_vImgGtGfu");
			RUTGEN.MoverA(g_Img_GtApe.GtApe_Psw, "");
			v_Img_GtGfu.clear();
			data.getUser().removeTemp("Agtp720_ImgGtApe");
			data.getUser().setTemp("Agtp720_ImgGtApe", g_Img_GtApe);
			data.getUser().removeTemp("Agtp720_vImgGtGfu");
			data.getUser().setTemp("Agtp720_vImgGtGfu", v_Img_GtGfu);
			setTemplate(data, "Garantias,Agt,AGTP720.vm");
			return;
		}
		if (Opc.trim().equals("X")) {
			g_Img_GtApe = (com.intGarantias.modules.global.GT_IMG.Buf_Img_GtApe) data
					.getUser().getTemp("Agtp720_ImgGtApe");
			RUTGEN.MoverA(g_Img_GtApe.GtApe_Psw, "");
			data.getUser().removeTemp("Agtp720_ImgGtApe");
			data.getUser().setTemp("Agtp720_ImgGtApe", g_Img_GtApe);
			setTemplate(data, "Garantias,Agt,AGTP720.vm");
			return;
		}
		if (Opc.trim().equals("Y")) {
			g_Img_GtApe = (com.intGarantias.modules.global.GT_IMG.Buf_Img_GtApe) data
					.getUser().getTemp("Agtp720_ImgGtApe");
			v_Img_GtGfu = (Vector) data.getUser().getTemp("Agtp720_vImgGtGfu");
			File_Banco(data, context);
			RUTGEN.MoverA(g_Img_GtApe.GtApe_Psw, "");
			data.getUser().removeTemp("Agtp720_ImgGtApe");
			data.getUser().setTemp("Agtp720_ImgGtApe", g_Img_GtApe);
			data.getUser().removeTemp("Agtp720_vImgGtGfu");
			data.getUser().setTemp("Agtp720_vImgGtGfu", v_Img_GtGfu);
			setTemplate(data, "Garantias,Agt,AGTP720.vm");
			return;
		} else {
			RUTGEN.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
			BF_MSG.Param_Program(data, g_Msg);
			data.getUser().removeTemp("Agtp720_PrmPC080");
			data.getUser().setTemp("Agtp720_PrmPC080", g_PrmPC080);
			setTemplate(data, "Garantias,Agt,AGTP720.vm");
			return;
		}
	}

	public void doAgtp720_Continue(RunData data, Context context)
			throws Exception {
		g_PrmPC080 = (com.FHTServlet.modules.global.PRMPC080.PrmPC080) data
				.getUser().getTemp("Agtp720_PrmPC080");
		com.FHTServlet.modules.global.PRMPC080.PrmPC080 lwPrmPC080 = new com.FHTServlet.modules.global.PRMPC080.PrmPC080();
		if (RUTGEN.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Appp030")
				&& (com.FHTServlet.modules.global.PRMPC080.PrmPC080) data
						.getUser().getTemp("Agtp720_g_PrmPC080") != null) {
			g_PrmPC080 = (com.FHTServlet.modules.global.PRMPC080.PrmPC080) data
					.getUser().getTemp("Agtp720_g_PrmPC080");
			data.getUser().removeTemp("Agtp720_g_PrmPC080");
			RUTGEN.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
			BF_MSG.Param_Program(data, g_Msg);
			setTemplate(data, "Garantias,Agt,AGTP720.vm");
			return;
		}
		if (RUTGEN.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Aedm105")) {
			g_PrmPC080 = (com.FHTServlet.modules.global.PRMPC080.PrmPC080) data
					.getUser().getTemp("Agtp720_g_PrmPC080");
			data.getUser().removeTemp("Agtp720_g_PrmPC080");
			lwPrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
			if (!lwPrmPC080.PC080_Rtn.toString().equals("OK")) {
				RUTGEN.MoverA(g_Msg.Msg_Dat,
						PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
				BF_MSG.Param_Program(data, g_Msg);
				setTemplate(data, "Garantias,Agt,AGTP720.vm");
				return;
			}
		}
		if (RUTGEN.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Aedm211")) {
			g_PrmPC080 = (com.FHTServlet.modules.global.PRMPC080.PrmPC080) data
					.getUser().getTemp("Agtp720_g_PrmPC080");
			data.getUser().removeTemp("Agtp720_g_PrmPC080");
			lwPrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
			if (!lwPrmPC080.PC080_Opc.toString().trim().equals("CN")) {
				RUTGEN.MoverA(g_Msg.Msg_Dat,
						PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
				BF_MSG.Param_Program(data, g_Msg);
				setTemplate(data, "Garantias,Agt,AGTP720.vm");
				return;
			}
		}
		if (RUTGEN.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Agtp093")) {
			g_PrmPC080 = (com.FHTServlet.modules.global.PRMPC080.PrmPC080) data
					.getUser().getTemp("Agtp720_g_PrmPC080");
			data.getUser().removeTemp("Agtp720_g_PrmPC080");
			g_PrmGT093 = PRMGT093.LSet_A_PrmGT093(g_Msg.Msg_Dat.toString());
			if (g_PrmGT093.GT093_Rtn.toString().trim().equals("OK")) {
				g_Img_GtGfu = (com.intGarantias.modules.global.GT_IMG.Buf_Img_GtGfu) data
						.getUser().getTemp("Agtp720_ImgGtGfu");
				RUTGEN.MoverA(g_Img_GtGfu.GtGfu_Dpc, g_PrmGT093.GT093_Dpc);
				RUTGEN.MoverA(g_Img_GtGfu.GtGfu_Pct,
						RUTGEN.FmtValor(g_PrmGT093.GT093_Pct, 0, 2, 5, "+"));
				if (!g_Img_GtGfu.getVln().trim().equals("")
						&& !g_Img_GtGfu.getVum().trim().equals("")) {
					RUTGEN.MoverA(g_Img_GtGfu.GtGfu_Vlc, RUTGEN.Multiplica(
							g_Img_GtGfu.getVln(), 4, g_Img_GtGfu.getVum(), 2,
							15, 2, 0));
					RUTGEN.MoverA(g_Img_GtGfu.GtGfu_Vlc, RUTGEN.Multiplica(
							g_Img_GtGfu.getVlc(), 2, g_Img_GtGfu.getPct(), 4,
							15, 2, 0));
				}
				data.getUser().removeTemp("Agtp720_ImgGtGfu");
				data.getUser().setTemp("Agtp720_ImgGtGfu", g_Img_GtGfu);
			}
			RUTGEN.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
			BF_MSG.Param_Program(data, g_Msg);
			setTemplate(data, "Garantias,Agt,AGTP720.vm");
			return;
		}
		if (RUTGEN.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Agtp214")
				|| RUTGEN.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Aedm096")) {
			g_PrmPC080 = (com.FHTServlet.modules.global.PRMPC080.PrmPC080) data
					.getUser().getTemp("Agtp720_g_PrmPC080");
			data.getUser().removeTemp("Agtp720_g_PrmPC080");
			RUTGEN.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
			BF_MSG.Param_Program(data, g_Msg);
			setTemplate(data, "Garantias,Agt,AGTP720.vm");
			return;
		}
		Limpieza(data, context);
		if (g_PrmPC080 == null)
			g_PrmPC080 = PRMPC080.Inicia_PrmPC080();
		RUTGEN.MoverA(g_PrmPC080.PC080_Rtn, "");
		RUTGEN.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
		BF_MSG.Return_Data("Agtp720", data, g_Msg);
	}

	public void Actualiza_Datos(String pcIdr, String pcSeq, RunData data,
			Context context) throws Exception {
		String lcAccion = "M";
		String lcFolio = "";
		String lcChars = "";
		int i = 0;
		g_Img_GtApe = (com.intGarantias.modules.global.GT_IMG.Buf_Img_GtApe) data
				.getUser().getTemp("Agtp720_ImgGtApe");
		g_Img_Base = BF_IMG.LSet_A_ImgBase(g_Img_GtApe.getBse());
		g_Img_GtTsd = (com.intGarantias.modules.global.GT_IMG.Buf_Img_GtTsd) data
				.getUser().getTemp("Agtp720_ImgGtTsd");
		if (pcIdr.trim().equals("a") || pcIdr.trim().equals("m")
				|| pcIdr.trim().equals("e")) {
			if (pcIdr.trim().equals("a"))
				pcSeq = g_Img_GtApe.getTfr();
			RUTGEN.MoverA(g_Img_GtApe.GtApe_Psw, pcIdr);
			RUTGEN.MoverA(g_Img_GtApe.GtApe_Idx,
					RUTGEN.Suma(pcSeq, 0, "1", 0, 3, 0));
			if (pcIdr.trim().equals("a")) {
				g_Img_GtGfu = GT_IMG.LSet_A_ImgGtGfu(RUTGEN.Blancos(827));
				RUTGEN.MoverA(g_Img_GtGfu.GtGfu_Seq, g_Img_GtApe.GtApe_Idx);
				RUTGEN.MoverA(g_Img_GtGfu.GtGfu_Itb, "GF");
				RUTGEN.MoverA(g_Img_GtGfu.GtGfu_Fvt, g_Msg.Msg_Fch);
				RUTGEN.MoverA(g_Img_GtGfu.GtGfu_Vln,
						RUTGEN.Zeros(g_Img_GtGfu.GtGfu_Vln));
				RUTGEN.MoverA(g_Img_GtGfu.GtGfu_Vlc,
						RUTGEN.Zeros(g_Img_GtGfu.GtGfu_Vlc));
				RUTGEN.MoverA(g_Img_GtGfu.GtGfu_Dob,
						RUTGEN.Zeros(g_Img_GtGfu.GtGfu_Dob));
			} else {
				i = Integer.parseInt(pcSeq);
				v_Img_GtGfu = (Vector) data.getUser().getTemp(
						"Agtp720_vImgGtGfu");
				g_Img_GtGfu = GT_IMG
						.LSet_A_ImgGtGfu(gtimg
								.LSet_De_ImgGtGfu((com.intGarantias.modules.global.GT_IMG.Buf_Img_GtGfu) v_Img_GtGfu
										.elementAt(i)));
			}
			data.getUser().removeTemp("Agtp720_ImgGtGfu");
			data.getUser().setTemp("Agtp720_ImgGtGfu", g_Img_GtGfu);
			data.getUser().removeTemp("Agtp720_Dob");
			data.getUser().setTemp("Agtp720_Dob",
					lob.leeCLOB(g_Img_GtGfu.getDob()));
		}
		if (pcIdr.trim().equals("x") || pcIdr.trim().equals("y")) {
			if (pcIdr.trim().equals("y")) {
				i = Integer.parseInt(pcSeq);
				g_Img_GtGfu = (com.intGarantias.modules.global.GT_IMG.Buf_Img_GtGfu) data
						.getUser().getTemp("Agtp720_ImgGtGfu");
				RUTGEN.MoverA(g_Img_GtGfu.GtGfu_Dpc, data.getParameters()
						.getString("GtGfu_Dpc", ""));
				RUTGEN.MoverA(g_Img_GtGfu.GtGfu_Pct, data.getParameters()
						.getString("GtGfu_Pct", ""));
				RUTGEN.MoverA(g_Img_GtGfu.GtGfu_Umt, data.getParameters()
						.getString("GtGfu_Umt", ""));
				RUTGEN.MoverA(g_Img_GtGfu.GtGfu_Udc, data.getParameters()
						.getString("GtGfu_Udc", ""));
				RUTGEN.MoverA(g_Img_GtGfu.GtGfu_Vum, data.getParameters()
						.getString("GtGfu_Vum", ""));
				RUTGEN.MoverA(g_Img_GtGfu.GtGfu_Vln, data.getParameters()
						.getString("GtGfu_Vln", ""));
				RUTGEN.MoverA(g_Img_GtGfu.GtGfu_Vlc, data.getParameters()
						.getString("GtGfu_Vlc", ""));
				RUTGEN.MoverA(g_Img_GtGfu.GtGfu_Fvt, data.getParameters()
						.getString("GtGfu_Fvt", ""));
				RUTGEN.MoverA(g_Img_GtGfu.GtGfu_Dsb, data.getParameters()
						.getString("GtGfu_Dsb", ""));
				lcChars = data.getParameters().getString("GtGfu_Dob", "");
				if (!lcChars.trim().equals("")) {
					if (g_Img_GtGfu.getDob().trim().equals("")
							|| g_Img_GtGfu.getDob().trim().equals("000000000"))
						RUTGEN.MoverA(g_Img_GtGfu.GtGfu_Dob,
								Asigna_Folio("FOLIO-CLOBS", data, context));
					lcFolio = g_Img_GtGfu.getDob();
					lob.mantieneCLOB(lcAccion, lcFolio, lcChars);
				}
				v_Img_GtGfu = (Vector) data.getUser().getTemp(
						"Agtp720_vImgGtGfu");
				if (g_Img_GtApe.GtApe_Psw.toString().trim().equals("a")) {
					v_Img_GtGfu.add(g_Img_GtGfu);
					RUTGEN.MoverA(g_Img_GtApe.GtApe_Tfr,
							RUTGEN.Suma(g_Img_GtApe.GtApe_Tfr, 0, "1", 0, 3, 0));
				} else if (g_Img_GtApe.GtApe_Psw.toString().trim().equals("m"))
					v_Img_GtGfu.set(i, g_Img_GtGfu);
				else if (g_Img_GtApe.GtApe_Psw.toString().trim().equals("e"))
					v_Img_GtGfu.remove(i);
				Recalcula_GtApe(data, context);
				data.getUser().removeTemp("Agtp720_vImgGtGfu");
				data.getUser().setTemp("Agtp720_vImgGtGfu", v_Img_GtGfu);
			}
			RUTGEN.MoverA(g_Img_GtApe.GtApe_Idx, "");
			RUTGEN.MoverA(g_Img_GtApe.GtApe_Psw, "");
			data.getUser().removeTemp("Agtp720_ImgGtGfu");
			data.getUser().removeTemp("Agtp720_Dob");
		}
		if (pcIdr.trim().equals("q") || pcIdr.trim().equals("s"))
			if (pcIdr.trim().equals("q")) {
				i = Integer.parseInt(pcSeq);
				v_Img_GtGfu = (Vector) data.getUser().getTemp(
						"Agtp720_vImgGtGfu");
				g_Img_GtGfu = (com.intGarantias.modules.global.GT_IMG.Buf_Img_GtGfu) v_Img_GtGfu
						.elementAt(i);
				RUTGEN.MoverA(g_Img_GtApe.GtApe_Psw, pcIdr);
				RUTGEN.MoverA(g_Img_GtApe.GtApe_Idx,
						RUTGEN.Suma(pcSeq, 0, "1", 0, 3, 0));
				data.getUser().removeTemp("Agtp720_ImgGtGfu");
				data.getUser().setTemp("Agtp720_ImgGtGfu", g_Img_GtGfu);
				data.getUser().removeTemp("Agtp720_Dob");
				data.getUser().setTemp("Agtp720_Dob",
						lob.leeCLOB(g_Img_GtGfu.getDob()));
			} else {
				RUTGEN.MoverA(g_Img_GtApe.GtApe_Idx, "");
				RUTGEN.MoverA(g_Img_GtApe.GtApe_Psw, "");
				data.getUser().removeTemp("Agtp720_ImgGtGfu");
				data.getUser().removeTemp("Agtp720_Dob");
			}
		data.getUser().removeTemp("Agtp720_ImgBase");
		data.getUser().setTemp("Agtp720_ImgBase", g_Img_Base);
		data.getUser().removeTemp("Agtp720_ImgGtApe");
		data.getUser().setTemp("Agtp720_ImgGtApe", g_Img_GtApe);
		data.getUser().removeTemp("Agtp720_ImgGtTsd");
		data.getUser().setTemp("Agtp720_ImgGtTsd", g_Img_GtTsd);
	}

	public String Asigna_Folio(String pcFolio, RunData data, Context context)
			throws Exception {
		g_MsgED090 = MSGED090.Inicia_MsgED090();
		RUTGEN.MoverA(g_MsgED090.ED090_Idr, "F");
		RUTGEN.MoverA(g_MsgED090.ED090_Cod, pcFolio);
		RUTGEN.MoverA(g_Msg.Msg_Dat, MSGED090.LSet_De_MsgED090(g_MsgED090));
		g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED090");
		g_MsgED090 = MSGED090.LSet_A_MsgED090(g_Msg.Msg_Dat.toString());
		String lcFolio = RUTGEN.FmtValor(g_MsgED090.ED090_Fol.toString(), 0, 0,
				9, "");
		return lcFolio;
	}

	public void Recalcula_GtApe(RunData data, Context context) throws Exception {
		String lcVtt = RUTGEN.Zeros(15);
		String lcVts = RUTGEN.Zeros(15);
		String lcVtb = RUTGEN.Zeros(15);
		String lcVcn = RUTGEN.Zeros(15);
		String lcVsg = RUTGEN.Zeros(15);
		for (int i = 0; i < v_Img_GtGfu.size(); i++) {
			g_Img_GtGfu = (com.intGarantias.modules.global.GT_IMG.Buf_Img_GtGfu) v_Img_GtGfu
					.elementAt(i);
			String wkVts = RUTGEN.Multiplica(g_Img_GtGfu.getVln(), 4,
					g_Img_GtGfu.getVum(), 2, 15, 2, 0);
			lcVtt = RUTGEN.Suma(lcVtt, 4, g_Img_GtGfu.GtGfu_Vln, 4, 15, 4);
			lcVts = RUTGEN.Suma(lcVts, 2, wkVts, 2, 15, 2);
			lcVtb = RUTGEN.Suma(lcVtb, 2, g_Img_GtGfu.GtGfu_Vlc, 2, 15, 2);
			lcVcn = RUTGEN.Suma(lcVcn, 2, g_Img_GtGfu.GtGfu_Vlc, 2, 15, 2);
			lcVsg = RUTGEN.Suma(lcVsg, 2, g_Img_GtGfu.GtGfu_Vlc, 2, 15, 2);
		}
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Vtt, lcVtt);
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Vts, lcVts);
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Vtb, lcVtb);
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Vcn, lcVcn);
	}

	public void Cursatura(String pcIdr, String pcEzt, RunData data,
			Context context) throws Exception {
		Vector l_TabImg = A_Tab_Img(data);
		if (g_PrmPC080.getFte().trim().equals("OPE")) {
			data.getUser().removeTemp("Agtp120_vTabImg");
			data.getUser().setTemp("Agtp120_vTabImg", l_TabImg);
		} else {
			img.Graba_Img_Host(g_Msg, l_TabImg, data, context);
		}
		if (pcEzt.trim().equals("")) {
			return;
		} else {
			g_MsgED135 = MSGED135.Inicia_MsgED135();
			RUTGEN.MoverA(g_MsgED135.ED135_Idr, "M");
			RUTGEN.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
			RUTGEN.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
			RUTGEN.MoverA(g_MsgED135.ED135_Est, "");
			RUTGEN.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
			RUTGEN.MoverA(g_MsgED135.ED135_Vlc, g_Img_GtApe.GtApe_Vcn);
			RUTGEN.MoverA(g_MsgED135.ED135_Ezt, pcEzt);
			RUTGEN.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
			g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
			return;
		}
	}

	public Vector A_Tab_Img(RunData data) {
		com.FHTServlet.modules.global.BF_IMG.Buf_Img lwImg = null;
		Vector v_Img = new Vector();
		String Seq = "";
		v_Img_GtGfu = (Vector) data.getUser().getTemp("Agtp720_vImgGtGfu");
		Vector v_TabImg = (Vector) data.getUser().getTemp("Agtp720_vTabImg");
		lwImg = new com.FHTServlet.modules.global.BF_IMG.Buf_Img();
		RUTGEN.MoverA(lwImg.Img_Ntr, g_PrmPC080.PC080_Ntr);
		RUTGEN.MoverA(lwImg.Img_Dax, "BASE");
		RUTGEN.MoverA(lwImg.Img_Seq, "000");
		RUTGEN.MoverA(lwImg.Img_Dat, GT_IMG.LSet_De_ImgGtApe(g_Img_GtApe));
		v_Img.add(lwImg);
		lwImg = new com.FHTServlet.modules.global.BF_IMG.Buf_Img();
		RUTGEN.MoverA(lwImg.Img_Ntr, g_PrmPC080.PC080_Ntr);
		RUTGEN.MoverA(lwImg.Img_Dax, "XTSD");
		RUTGEN.MoverA(lwImg.Img_Seq, "000");
		RUTGEN.MoverA(lwImg.Img_Dat, GT_IMG.LSet_De_ImgGtTsd(g_Img_GtTsd));
		v_Img.add(lwImg);
		Seq = "000";
		for (int i = 0; i < v_Img_GtGfu.size(); i++) {
			Seq = RUTGEN.Suma(Seq, 0, "1", 0, 3, 0);
			g_Img_GtGfu = (com.intGarantias.modules.global.GT_IMG.Buf_Img_GtGfu) v_Img_GtGfu
					.elementAt(i);
			lwImg = new com.FHTServlet.modules.global.BF_IMG.Buf_Img();
			RUTGEN.MoverA(lwImg.Img_Ntr, g_PrmPC080.PC080_Ntr);
			RUTGEN.MoverA(lwImg.Img_Dax, "DTGFU");
			RUTGEN.MoverA(lwImg.Img_Seq, Seq);
			RUTGEN.MoverA(lwImg.Img_Dat, gtimg.LSet_De_ImgGtGfu(g_Img_GtGfu));
			v_Img.add(lwImg);
		}

		for (int i = 0; i < v_TabImg.size(); i++) {
			lwImg = new com.FHTServlet.modules.global.BF_IMG.Buf_Img();
			lwImg = (com.FHTServlet.modules.global.BF_IMG.Buf_Img) v_TabImg
					.elementAt(i);
			if (!lwImg.getDax().trim().equals("BASE")
					&& !lwImg.getDax().trim().equals("XTSD")
					&& !lwImg.getDax().trim().equals("DTGFU"))
				v_Img.add(lwImg);
		}

		return v_Img;
	}

	public void Carga_Data(RunData data, Context context) throws Exception {
		g_MsgED135 = MSGED135.Inicia_MsgED135();
		RUTGEN.MoverA(g_PrmPC080.PC080_Lob, "");
		Carga_de_Host(data, context);
		Log.debug("AGTP720", "PC080_Fte:[" + g_PrmPC080.getFte().trim() + "]");
		Log.debug("AGTP720", "PC080_Rtn:[" + g_PrmPC080.getRtn().trim() + "]");
		Log.debug("AGTP720", "PC080_Err:[" + g_PrmPC080.getErr().trim() + "]");
		Log.debug("AGTP720", "PC080_Opc:[" + g_PrmPC080.getOpc().trim() + "]");
		Log.debug("AGTP720", "PC080_Amv:[" + g_PrmPC080.getAmv().trim() + "]");
		Log.debug("AGTP720", "PC080_Lob:[" + g_PrmPC080.getLob().trim() + "]");
		Log.debug("AGTP720", "PC080_Acc:[" + g_PrmPC080.getAcc().trim() + "]");
		Log.debug("AGTP720", "PC080_Ttr:[" + g_PrmPC080.getTtr().trim() + "]");
		Log.debug("AGTP720", "PC080_Trt:[" + g_PrmPC080.getTrt().trim() + "]");
		RUTGEN.MoverA(g_PrmPC080.PC080_Acc, "");
		RUTGEN.MoverA(g_PrmPC080.PC080_Ntr, g_Img_Base.Img_Base_Ntr);
		RUTGEN.MoverA(g_PrmPC080.PC080_Ttr, g_Img_Base.Img_Base_Ttr);
		RUTGEN.MoverA(g_PrmPC080.PC080_Ntt, g_Img_Base.Img_Base_Ntt);
		RUTGEN.MoverA(g_PrmPC080.PC080_Cli, g_Img_Base.Img_Base_Cli);
		RUTGEN.MoverA(g_PrmPC080.PC080_Ncl, g_Img_Base.Img_Base_Ncl);
		RUTGEN.MoverA(g_PrmPC080.PC080_Tpr, g_Img_Base.Img_Base_Tpr);
		RUTGEN.MoverA(g_PrmPC080.PC080_Dir, g_Img_Base.Img_Base_Dir);
		RUTGEN.MoverA(g_PrmPC080.PC080_Cmn, "");
		RUTGEN.MoverA(g_PrmPC080.PC080_Tfc, g_Img_Base.Img_Base_Tfc);
		RUTGEN.MoverA(g_PrmPC080.PC080_Cnr.Sis, g_Img_Base.Img_Base_Sis);
		RUTGEN.MoverA(g_PrmPC080.PC080_Cnr.Ncn, g_Img_Base.Img_Base_Ncn);
		RUTGEN.MoverA(g_PrmPC080.PC080_Dcn, g_Img_Base.Img_Base_Dcn);
		RUTGEN.MoverA(g_PrmPC080.PC080_Ndc, g_Img_Base.Img_Base_Ndc);
		RUTGEN.MoverA(g_PrmPC080.PC080_Tmn, g_Img_Base.Img_Base_Tmn);
		RUTGEN.MoverA(g_PrmPC080.PC080_Trj, g_Img_Base.Img_Base_Trj);
		RUTGEN.MoverA(g_PrmPC080.PC080_Dcm, g_Img_Base.Img_Base_Dcm);
		RUTGEN.MoverA(g_PrmPC080.PC080_Prp, g_Img_Base.Img_Base_Prp);
		RUTGEN.MoverA(g_PrmPC080.PC080_Mnd, g_Img_Base.Img_Base_Mnd);
		RUTGEN.MoverA(g_PrmPC080.PC080_Nmn, g_Img_Base.Img_Base_Nmn);
		RUTGEN.MoverA(g_PrmPC080.PC080_Ggr, g_Img_GtApe.GtApe_Ggr);
		data.getUser().removeTemp("Agtp720_MsgED135");
		data.getUser().setTemp("Agtp720_MsgED135", g_MsgED135);
		data.getUser().removeTemp("Agtp720_ImgBase");
		data.getUser().setTemp("Agtp720_ImgBase", g_Img_Base);
		data.getUser().removeTemp("Agtp720_ImgGtApe");
		data.getUser().setTemp("Agtp720_ImgGtApe", g_Img_GtApe);
		data.getUser().removeTemp("Agtp720_ImgGtTsd");
		data.getUser().setTemp("Agtp720_ImgGtTsd", g_Img_GtTsd);
		data.getUser().removeTemp("Agtp720_vImgGtGfu");
		data.getUser().setTemp("Agtp720_vImgGtGfu", v_Img_GtGfu);
	}

	public void Carga_de_Host(RunData data, Context context) throws Exception {
		Vector wvTabImg = new Vector();
		if (g_PrmPC080.getFte().trim().equals("OPE")) {
			wvTabImg = (Vector) data.getUser().getTemp("Agtp120_vTabImg");
		} else {
			wvTabImg = img.Carga_Img_Host(g_Msg, g_PrmPC080.getNtr(), data,
					context);
			Recupera_ED135(data, context);
		}
		De_Tab_Img(wvTabImg, data);
		if (g_Img_GtApe.getFts().trim().equals("")) {
			Cambia_Fts(g_Msg.getFch(), data, context);
			Inicializa_Tasacion(data, context);
		}
	}

	public void Recupera_ED135(RunData data, Context context) throws Exception {
		RUTGEN.MoverA(g_MsgED135.ED135_Idr, "C");
		RUTGEN.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
		RUTGEN.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
		RUTGEN.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
		g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
		g_MsgED135 = MSGED135.LSet_A_MsgED135(g_Msg.getDat());
	}

	public void De_Tab_Img(Vector w_TabImg, RunData data) {
		g_Img = BF_IMG.Inicia_Img();
		g_Img_Base = BF_IMG.LSet_A_ImgBase(g_Img.getDat());
		g_Img_GtApe = GT_IMG.LSet_A_ImgGtApe(g_Img.getDat());
		g_Img_GtTsd = GT_IMG.LSet_A_ImgGtTsd(g_Img.getDat());
		v_Img_GtGfu.clear();
		for (int i = 0; i < w_TabImg.size(); i++) {
			g_Img = BF_IMG
					.LSet_A_Img(BF_IMG
							.LSet_De_Img((com.FHTServlet.modules.global.BF_IMG.Buf_Img) w_TabImg
									.elementAt(i)));
			if (g_Img.getDax().trim().equals("BASE")) {
				g_Img_GtApe = GT_IMG.LSet_A_ImgGtApe(g_Img.getDat());
				g_Img_Base = BF_IMG.LSet_A_ImgBase(g_Img_GtApe.getBse());
			}
			if (g_Img.getDax().trim().equals("XTSD"))
				g_Img_GtTsd = GT_IMG.LSet_A_ImgGtTsd(g_Img.getDat());
			if (g_Img.getDax().trim().equals("DTGFU")) {
				g_Img_GtGfu = GT_IMG.LSet_A_ImgGtGfu(g_Img.getDat());
				v_Img_GtGfu.add(g_Img_GtGfu);
			}
		}

		data.getUser().removeTemp("Agtp720_vTabImg");
		data.getUser().setTemp("Agtp720_vTabImg", w_TabImg);
	}

	public void Cambia_Fts(String Fecha, RunData data, Context context)
			throws Exception {
		g_MsgGT096 = MSGGT096.Inicia_MsgGT096();
		RUTGEN.MoverA(g_MsgGT096.GT096_Idr, "V");
		RUTGEN.MoverA(g_MsgGT096.GT096_Fec, Fecha);
		RUTGEN.MoverA(g_Msg.Msg_Dat, MSGGT096.LSet_De_MsgGT096(g_MsgGT096));
		g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GT096");
		g_MsgGT096 = MSGGT096.LSet_A_MsgGT096(g_Msg.Msg_Dat.toString());
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Fig, g_Msg.Msg_Fch);
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Fts, g_MsgGT096.GT096_Fec);
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Vuo, RUTGEN.FmtValor("1", 0, 2, 9, "+"));
		if (g_Img_Base.getTrj().trim().equals("UF"))
			RUTGEN.MoverA(g_Img_GtApe.GtApe_Vuo,
					RUTGEN.FmtValor(g_MsgGT096.GT096_Vuf, 2, 2, 9, "+"));
		if (g_Img_Base.getTrj().trim().equals("TC"))
			RUTGEN.MoverA(g_Img_GtApe.GtApe_Vuo,
					RUTGEN.FmtValor(g_MsgGT096.GT096_Vtc, 2, 2, 9, "+"));
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Vuf,
				RUTGEN.FmtValor(g_MsgGT096.GT096_Vuf, 2, 2, 9, "+"));
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Vus,
				RUTGEN.FmtValor(g_MsgGT096.GT096_Vtc, 2, 2, 9, "+"));
		RUTGEN.MoverA(g_Img_GtTsd.GtTsd_Fts, g_MsgGT096.GT096_Fec);
		RUTGEN.MoverA(g_Img_GtTsd.GtTsd_Vuf,
				RUTGEN.FmtValor(g_MsgGT096.GT096_Vuf, 2, 2, 9, "+"));
		RUTGEN.MoverA(g_Img_GtTsd.GtTsd_Vtc,
				RUTGEN.FmtValor(g_MsgGT096.GT096_Vtc, 2, 2, 9, "+"));
	}

	public void Inicializa_Tasacion(RunData data, Context context)
			throws Exception {
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Idx, g_Img_GtApe.GtApe_Psq);
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Tfr, "001");
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Vtt,
				RUTGEN.Zeros(g_Img_GtApe.GtApe_Vtt));
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Vts,
				RUTGEN.Zeros(g_Img_GtApe.GtApe_Vts));
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Vtb,
				RUTGEN.Zeros(g_Img_GtApe.GtApe_Vtb));
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Vcn,
				RUTGEN.Zeros(g_Img_GtApe.GtApe_Vcn));
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Hvt,
				RUTGEN.Zeros(g_Img_GtApe.GtApe_Hvt));
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Hvl,
				RUTGEN.Zeros(g_Img_GtApe.GtApe_Hvl));
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Hvb,
				RUTGEN.Zeros(g_Img_GtApe.GtApe_Hvb));
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Hvc,
				RUTGEN.Zeros(g_Img_GtApe.GtApe_Hvc));
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Pvt,
				RUTGEN.Zeros(g_Img_GtApe.GtApe_Pvt));
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Pvl,
				RUTGEN.Zeros(g_Img_GtApe.GtApe_Pvl));
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Pvb,
				RUTGEN.Zeros(g_Img_GtApe.GtApe_Pvb));
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Pvc,
				RUTGEN.Zeros(g_Img_GtApe.GtApe_Pvc));
	}

	public void File_Banco(RunData data, Context context) throws Exception {
		FileItem fi = data.getParameters().getFileItem("Archivo");
		java.io.InputStream in = fi.getStream();
		BufferedReader bf = new BufferedReader(new InputStreamReader(in));
		String linea = "";
		try {
			while (linea != null) {
				linea = bf.readLine();
				if (linea != null
						&& RUTGEN.Parsea("T", 1, linea).trim()
								.equalsIgnoreCase("Presupuesto")) {
					g_Img_GtGfu = GT_IMG.LSet_A_ImgGtGfu(RUTGEN.Blancos(827));
					v_Img_GtGfu.add(g_Img_GtGfu);
				}
			}
		} catch (EOFException e) {
			Log.debug("AGTP720.EOFException: [" + e.toString() + "]");
		}
		bf.close();
	}

	public void Carga_Tablas_Locales(RunData rundata, Context context1)
			throws Exception {
	}

	public void Limpieza(RunData data, Context context) throws Exception {
		data.getUser().removeTemp("Agtp720_PrmPC080");
		data.getUser().removeTemp("Agtp720_MsgED135");
		data.getUser().removeTemp("Agtp720_vTabImg");
		data.getUser().removeTemp("Agtp720_ImgBase");
		data.getUser().removeTemp("Agtp720_ImgGtApe");
		data.getUser().removeTemp("Agtp720_ImgGtTsd");
		data.getUser().removeTemp("Agtp720_Dsp");
		data.getUser().removeTemp("Agtp720_vImgGtGfu");
		data.getUser().removeTemp("Agtp720_tGblDpc");
		data.getUser().removeTemp("Agtp720_tGarGgr");
	}

	RUTGEN rg;
	BF_MSG msg;
	com.FHTServlet.modules.global.BF_MSG.Buf_Msg g_Msg;
	BF_LOB lob;
	com.FHTServlet.modules.global.PRMPC080.PrmPC080 g_PrmPC080;
	BF_IMG img;
	com.FHTServlet.modules.global.BF_IMG.Buf_Img g_Img;
	com.FHTServlet.modules.global.BF_IMG.Buf_Img_Bse g_Img_Base;
	GT_IMG gtimg;
	com.intGarantias.modules.global.GT_IMG.Buf_Img_GtApe g_Img_GtApe;
	com.intGarantias.modules.global.GT_IMG.Buf_Img_GtTsd g_Img_GtTsd;
	com.intGarantias.modules.global.GT_IMG.Buf_Img_GtGfu g_Img_GtGfu;
	Vector v_Img_GtGfu;
	com.intGlobal.modules.global.MSGED090.Buf_MsgED090 g_MsgED090;
	com.intGlobal.modules.global.MSGED135.Buf_MsgED135 g_MsgED135;
	com.intGarantias.modules.global.MSGGT096.Buf_MsgGT096 g_MsgGT096;
	com.intGarantias.modules.global.PRMGT093.Buf_PrmGT093 g_PrmGT093;
}
