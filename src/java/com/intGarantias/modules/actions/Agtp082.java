// Source File Name:   Agtp082.java
// Descripcion     :   Menu Tasador [Fte: TSD] (-)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.PRMPC080;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp082 extends FHTServletAction
{
  //---------------------------------------------------------------------------------------
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  PRMPC080.PrmPC080 g_PrmPC080     = new PRMPC080.PrmPC080();
  //---------------------------------------------------------------------------------------
  public Agtp082()
  {
  }
  //---------------------------------------------------------------------------------------
  public void doPerform(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP082[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    doAgtp082_Init(data, context);
  }
  //---------------------------------------------------------------------------------------
  public void doCancel(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP082[doCancel.start]", "[CANCELADO]");
    setTemplate(data, "Garantias,Agt,AGTP082.vm");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp082_Init(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP082[doAgtp082_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp082-DIN", data);
    Re_PRMPC080(data, context);
    setTemplate(data, "Garantias,Agt,AGTP082.vm" );
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp082(RunData data, Context context)
              throws Exception
  {
    g_Msg = BF_MSG.InitIntegraTasador("Agtp082", data, context);
    Re_PRMPC080(data, context);
    String Opc = data.getParameters().getString("Opc", "");
    String Ncn = data.getParameters().getString("Ncn", "");
    if (Opc.trim().equals("S"))
       {
         //Boton Salir
         BF_MSG.Return_Program(data, g_Msg);
         return;
       }
    if (Opc.equals("1"))
       {
         //Garantias para Registro
         Re_PRMPC080(data, context);
         BF_MSG.Link_Program("Agtp082", "Agtp200", data, g_Msg);
         return;
       }
    if (Opc.equals("2"))
       {
         //ITO: Estado Avance PI
         Re_PRMPC080(data, context);
         rg.MoverA(g_PrmPC080.PC080_Rtn, " ");
         rg.MoverA(g_PrmPC080.PC080_Acc, "APERT");
         rg.MoverA(g_PrmPC080.PC080_Ttr, "RETPI");
         rg.MoverA(g_PrmPC080.PC080_Ntt, "ESTADO DE AVANCE DE OBRAS");
         rg.MoverA(g_PrmPC080.PC080_Cnr.Ncn, Ncn);
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Link_Program("Agtp082", "Agtp116", data, g_Msg);
         return;
       }
    //HS nuevo link
    if (Opc.equals("3"))
    {
      //Re_PRMPC080(data, context);
      context.put("rol", "TSD");
      BF_MSG.Link_Program("Agtp082", "Agtp231", data, g_Msg);
      return;
    }
    
    if (Opc.equals("4"))
    {
      BF_MSG.Link_Program("Agtp082", "InformePagoTasadores", data, g_Msg);
      return;
    }
    
    //HS nuevo link
  }
  //---------------------------------------------------------------------------------------
  public void Re_PRMPC080(RunData data, Context context)
              throws Exception
  {
    g_PrmPC080 = PRMPC080.Inicia_PrmPC080();
    RUTGEN.MoverA(g_PrmPC080.PC080_Fte,     "TSD");
    RUTGEN.MoverA(g_PrmPC080.PC080_Pai,     "CL");
    RUTGEN.MoverA(g_PrmPC080.PC080_Cnr.Sis, "GAR");
    RUTGEN.MoverA(g_PrmPC080.PC080_Slr.Slc, RUTGEN.Zeros(7));
    RUTGEN.MoverA(g_PrmPC080.PC080_Suc,     g_Msg.Msg_Suc);
    RUTGEN.MoverA(g_PrmPC080.PC080_Nsu,     g_Msg.Msg_Nsu);
    RUTGEN.MoverA(g_PrmPC080.PC080_Mnd,     "000");
    RUTGEN.MoverA(g_PrmPC080.PC080_Nmn,     "PESOS CHILE");
    RUTGEN.MoverA(g_PrmPC080.PC080_Eje,     g_Msg.Msg_Cus);
    RUTGEN.MoverA(g_PrmPC080.PC080_Nej,     g_Msg.Msg_Nus);
    RUTGEN.MoverA(g_PrmPC080.PC080_Tej,     g_Msg.Msg_Fus);
    RUTGEN.MoverA(g_PrmPC080.PC080_Tsd,     g_Msg.Msg_Cus.substring(1));
    RUTGEN.MoverA(g_Msg.Msg_Dat,            PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
    BF_MSG.Param_Program(data, g_Msg);
  }
  //---------------------------------------------------------------------------------------
}