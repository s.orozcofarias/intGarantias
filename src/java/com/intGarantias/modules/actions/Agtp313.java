// Source File Name:   Agtp313.java
// Descripcion:        Ingreso de Fianzas (CL090, CL095, ED135)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.BF_IMG;
import com.FHTServlet.modules.global.PRMPC080;

import com.intGlobal.modules.global.MSGED135;
import com.intGlobal.modules.global.MSGCL090;
import com.intGlobal.modules.global.MSGCL095;
import com.intGlobal.modules.global.PRMCL090;

import com.intGarantias.modules.global.GT_IMG;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp313 extends FHTServletAction
{
  //---------------------------------------------------------------------------------------
  public int    i = 0;
  public String lcData = "";
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  PRMPC080.PrmPC080 g_PrmPC080     = new PRMPC080.PrmPC080();
  Vector v_Tab_Img                 = new Vector();
  Vector v_Img_GtFia               = new Vector();
  BF_IMG img                       = new BF_IMG();
  BF_IMG.Buf_Img g_Img             = new BF_IMG.Buf_Img();
  BF_IMG.Buf_Img_Bse g_Img_Base    = new BF_IMG.Buf_Img_Bse();
  GT_IMG gtimg                     = new GT_IMG();
  GT_IMG.Buf_Img_GtApe g_Img_GtApe = new GT_IMG.Buf_Img_GtApe();
  GT_IMG.Buf_Img_GtFia g_Img_GtFia = new GT_IMG.Buf_Img_GtFia();
  MSGED135.Buf_MsgED135 g_MsgED135 = new MSGED135.Buf_MsgED135();
  MSGCL090.Buf_MsgCL090 g_MsgCL090 = new MSGCL090.Buf_MsgCL090();
  MSGCL095.Buf_MsgCL095 g_MsgCL095 = new MSGCL095.Buf_MsgCL095();
  PRMCL090.Buf_PrmCL090 g_PrmCL090 = new PRMCL090.Buf_PrmCL090();
  //---------------------------------------------------------------------------------------
  public Agtp313()
  {
  }
  //---------------------------------------------------------------------------------------
  public void doPerform(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP313[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp313-DIN", data);
    if (g_Msg.Msg_Pgr.toString().equals("RT"))
       { doAgtp313_Continue(data, context); }
    else
       { doAgtp313_Init(data, context); }
  }
  //---------------------------------------------------------------------------------------
  public void doCancel(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP313[doCancel.start]", "[CANCELADO]");
    setTemplate(data, "Garantias,Agt,AGTP313.vm");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp313_Init(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP313[doAgtp313_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    Carga_de_Host(data, context);
    Carga_Inicial(data, context);
    data.getUser().removeTemp("Agtp313_PrmPC080");
    data.getUser().setTemp("Agtp313_PrmPC080", g_PrmPC080);
    setTemplate(data, "Garantias,Agt,AGTP313.vm" );
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp313(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP313[doAgtp313.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp313-MAN", data);
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    String Opc = data.getParameters().getString("Opc" ,"");
    if (Opc.trim().equals("S"))
       {
         //Boton Salir
         data.getUser().removeTemp("Agtp313_MsgED135");
         data.getUser().removeTemp("Agtp313_vTabImg");
         data.getUser().removeTemp("Agtp313_vImgGtFia");
         data.getUser().removeTemp("Agtp313_ImgBase");
         data.getUser().removeTemp("Agtp313_ImgGtApe");
         data.getUser().removeTemp("Agtp313_PrmPC080");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp313", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("C"))
       {
         //Boton OK (CursarIdt)
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp313_ImgGtApe");
         v_Img_GtFia = (Vector)data.getUser().getTemp("Agtp313_vImgGtFia");
         Vector g_Tab_Img = A_Tab_Img(data);
         img.Graba_Img_Host(g_Msg, g_Tab_Img, data, context);
         g_MsgED135 = MSGED135.Inicia_MsgED135();
         rg.MoverA(g_MsgED135.ED135_Idr, "M");
         rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
         rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
         rg.MoverA(g_MsgED135.ED135_Est, "");
         rg.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
         rg.MoverA(g_MsgED135.ED135_Vlc, g_Img_GtApe.GtApe_Vcn);
         rg.MoverA(g_MsgED135.ED135_Ezt, "TERMI");
         rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
         g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
         data.getUser().removeTemp("Agtp313_MsgED135");
         data.getUser().removeTemp("Agtp313_vTabImg");
         data.getUser().removeTemp("Agtp313_vImgGtFia");
         data.getUser().removeTemp("Agtp313_ImgBase");
         data.getUser().removeTemp("Agtp313_ImgGtApe");
         data.getUser().removeTemp("Agtp313_PrmPC080");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp313", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("A"))
       {
         //Boton Agregar
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp313_ImgGtApe");
         g_Img_Base  = img.LSet_A_ImgBase(g_Img_GtApe.GtApe_Bse.toString());
         g_Img_GtFia = gtimg.LSet_A_ImgGtFia(rg.Blancos(827));
         rg.MoverA(g_Img_GtApe.GtApe_Psw, "A");
         rg.MoverA(g_Img_GtApe.GtApe_Idx, rg.Suma(g_Img_GtApe.GtApe_Tfr,0,"1",0,3,0));
         data.getUser().removeTemp("Agtp313_ImgGtApe");
         data.getUser().setTemp("Agtp313_ImgGtApe", g_Img_GtApe);
         data.getUser().removeTemp("Agtp313_ImgGtFia");
         data.getUser().setTemp("Agtp313_ImgGtFia", g_Img_GtFia);
         setTemplate(data, "Garantias,Agt,AGTP313.vm" );
         return;
       }
    if (Opc.trim().equals("M"))
       {
         //Boton Modificar
         String Seq = data.getParameters().getString("Seq", "");
         i = Integer.parseInt(Seq);
         v_Img_GtFia = (Vector)data.getUser().getTemp("Agtp313_vImgGtFia");
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp313_ImgGtApe");
         g_Img_Base  = img.LSet_A_ImgBase(g_Img_GtApe.GtApe_Bse.toString());
         rg.MoverA(g_Img_GtApe.GtApe_Psw, "M");
         rg.MoverA(g_Img_GtApe.GtApe_Idx, rg.Suma(Seq,0,"1",0,3,0));
         g_Img_GtFia = (GT_IMG.Buf_Img_GtFia)v_Img_GtFia.elementAt(i);
         data.getUser().removeTemp("Agtp313_ImgGtApe");
         data.getUser().setTemp("Agtp313_ImgGtApe", g_Img_GtApe);
         data.getUser().removeTemp("Agtp313_ImgGtFia");
         data.getUser().setTemp("Agtp313_ImgGtFia", g_Img_GtFia);
         setTemplate(data, "Garantias,Agt,AGTP313.vm" );
         return;
       }
    if (Opc.trim().equals("E"))
       {
         //Boton Eliminar
         String Seq = data.getParameters().getString("Seq", "");
         i = Integer.parseInt(Seq);
         v_Img_GtFia = (Vector)data.getUser().getTemp("Agtp313_vImgGtFia");
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp313_ImgGtApe");
         g_Img_Base  = img.LSet_A_ImgBase(g_Img_GtApe.GtApe_Bse.toString());
         rg.MoverA(g_Img_GtApe.GtApe_Psw, "E");
         rg.MoverA(g_Img_GtApe.GtApe_Idx, rg.Suma(Seq,0,"1",0,3,0));
         g_Img_GtFia = (GT_IMG.Buf_Img_GtFia)v_Img_GtFia.elementAt(i);
         data.getUser().removeTemp("Agtp313_ImgGtApe");
         data.getUser().setTemp("Agtp313_ImgGtApe", g_Img_GtApe);
         data.getUser().removeTemp("Agtp313_ImgGtFia");
         data.getUser().setTemp("Agtp313_ImgGtFia", g_Img_GtFia);
         setTemplate(data, "Garantias,Agt,AGTP313.vm" );
         return;
       }
    if (Opc.trim().equals("F"))
       {
         //Imagen Cambio Fiador
         g_Img_GtFia = (GT_IMG.Buf_Img_GtFia)data.getUser().getTemp("Agtp313_ImgGtFia");
         Actualiza_Datos(data, context);
         data.getUser().removeTemp("Agtp313_ImgGtFia");
         data.getUser().setTemp("Agtp313_ImgGtFia", g_Img_GtFia);
         g_PrmCL090 = PRMCL090.Inicia_PrmCL090();
         rg.MoverA(g_PrmCL090.CL090_Pcc, "N");
         data.getUser().removeTemp("Agtp313_g_PrmPC080");
         data.getUser().setTemp("Agtp313_g_PrmPC080" ,g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMCL090.LSet_De_PrmCL090(g_PrmCL090));
         BF_MSG.Link_Program("Agtp313", "Eclp090", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("Y"))
       {
         //Boton Si
         String Seq = data.getParameters().getString("Seq", "0");
         i = Integer.parseInt(Seq);
         g_Img_GtFia = (GT_IMG.Buf_Img_GtFia)data.getUser().getTemp("Agtp313_ImgGtFia");
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp313_ImgGtApe");
         v_Img_GtFia = (Vector)data.getUser().getTemp("Agtp313_vImgGtFia");
         Actualiza_Datos(data, context);
         if (g_Img_GtApe.GtApe_Psw.toString().trim().equals("A"))
            {
              v_Img_GtFia.add(g_Img_GtFia);
              rg.MoverA(g_Img_GtApe.GtApe_Tfr, rg.Suma(g_Img_GtApe.GtApe_Tfr,0,"1",0,3,0));
            }
         else
            {
              if (g_Img_GtApe.GtApe_Psw.toString().trim().equals("M"))
                 { v_Img_GtFia.set(i, g_Img_GtFia); }
              else
                 {
                   if (g_Img_GtApe.GtApe_Psw.toString().trim().equals("E"))
                      { v_Img_GtFia.remove(i); }
                 }
            }
         Recalcula_GtApe(data, context);
         rg.MoverA(g_Img_GtApe.GtApe_Vcn, rg.FmtValor("1",0,2,15,"+"));
         rg.MoverA(g_Img_GtApe.GtApe_Idx, " ");
         rg.MoverA(g_Img_GtApe.GtApe_Psw, " ");
         data.getUser().removeTemp("Agtp313_ImgGtApe");
         data.getUser().setTemp("Agtp313_ImgGtApe", g_Img_GtApe);
         data.getUser().removeTemp("Agtp313_ImgGtFia");
         data.getUser().removeTemp("Agtp313_vImgGtFia");
         data.getUser().setTemp("Agtp313_vImgGtFia", v_Img_GtFia);
         setTemplate(data, "Garantias,Agt,AGTP313.vm" );
         return;
       }
    if (Opc.trim().equals("N"))
       {
         //Boton No
         data.getUser().removeTemp("Agtp313_ImgGtFia");
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp313_ImgGtApe");
         rg.MoverA(g_Img_GtApe.GtApe_Idx, " ");
         rg.MoverA(g_Img_GtApe.GtApe_Psw, " ");
         data.getUser().removeTemp("Agtp313_ImgGtApe");
         data.getUser().setTemp("Agtp313_ImgGtApe", g_Img_GtApe);
         setTemplate(data, "Garantias,Agt,AGTP313.vm" );
         return;
       }
  }
  //---------------------------------------------------------------------------------------
  public void Recalcula_GtApe(RunData data, Context context)
              throws Exception
  {
    String lcVtt = rg.Zeros(15);
    String lcVts = rg.Zeros(15);
    String lcVtb = rg.Zeros(15);
    String lcVcn = rg.Zeros(15);
    for (i=0; i<v_Img_GtFia.size(); i++)
        {
          g_Img_GtFia = (GT_IMG.Buf_Img_GtFia)v_Img_GtFia.elementAt(i);
          lcVtt = rg.Suma(lcVtt,4,"1",2,15,4);
          lcVts = rg.Suma(lcVts,2,"1",2,15,2);
          lcVtb = rg.Suma(lcVtb,2,"1",2,15,2);
          lcVcn = rg.Suma(lcVcn,2,"1",2,15,2);
        }
    rg.MoverA(g_Img_GtApe.GtApe_Vtt, lcVtt);
    rg.MoverA(g_Img_GtApe.GtApe_Vts, lcVts);
    rg.MoverA(g_Img_GtApe.GtApe_Vtb, lcVtb);
    rg.MoverA(g_Img_GtApe.GtApe_Vcn, lcVcn);
  }
  //---------------------------------------------------------------------------------------
  public void Actualiza_Datos(RunData data, Context context)
              throws Exception
  {
  }
  //---------------------------------------------------------------------------------------
  public void Carga_de_Host(RunData data, Context context)
              throws Exception
  {
    v_Tab_Img = img.Carga_Img_Host(g_Msg, g_PrmPC080.PC080_Ntr.toString(), data, context);
    De_Tab_Img(v_Tab_Img, data);
    if (g_Img_GtApe.GtApe_Tfr.toString().trim().equals(""))
       { rg.MoverA(g_Img_GtApe.GtApe_Tfr, rg.Zeros(3)); }
    if (g_Img_GtApe.GtApe_Fig.toString().trim().equals(""))
       {
         rg.MoverA(g_Img_GtApe.GtApe_Fig, g_Msg.Msg_Fch);
         rg.MoverA(g_Img_GtApe.GtApe_Vuo, rg.Zeros(g_Img_GtApe.GtApe_Vuo));
         rg.MoverA(g_Img_GtApe.GtApe_Fts, g_Msg.Msg_Fch);
       }
    if (g_Img_GtApe.GtApe_Vtt.toString().trim().equals(""))
       {
         rg.MoverA(g_Img_GtApe.GtApe_Vtt, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Vts, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Vtb, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Vcn, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Hvt, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Hvl, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Hvb, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Hvc, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Pvt, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Pvl, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Pvb, rg.Zeros(15));
         rg.MoverA(g_Img_GtApe.GtApe_Pvc, rg.Zeros(15));
       }
    data.getUser().removeTemp("Agtp313_ImgBase");
    data.getUser().setTemp("Agtp313_ImgBase", g_Img_Base);
    data.getUser().removeTemp("Agtp313_ImgGtApe");
    data.getUser().setTemp("Agtp313_ImgGtApe", g_Img_GtApe);
    data.getUser().removeTemp("Agtp313_vImgGtFia");
    data.getUser().setTemp("Agtp313_vImgGtFia", v_Img_GtFia);
    data.getUser().removeTemp("Agtp313_vTabImg");
    data.getUser().setTemp("Agtp313_vTabImg", v_Tab_Img);
  }
  //---------------------------------------------------------------------------------------
  public void De_Tab_Img(Vector v_Tab_Img, RunData data)
  {
    g_Img       = img.Inicia_Img();
    lcData      = img.LSet_De_Img(g_Img);
    g_Img_Base  = img.LSet_A_ImgBase(lcData);
    g_Img_GtApe = gtimg.LSet_A_ImgGtApe(g_Img.Img_Dat.toString());
    v_Img_GtFia.clear();
    for (i=0; i<v_Tab_Img.size(); i++)
        {
          lcData = img.LSet_De_Img((BF_IMG.Buf_Img)v_Tab_Img.elementAt(i));
          g_Img  = img.LSet_A_Img(lcData);
          if (g_Img.Img_Dax.toString().trim().equals("BASE"))
             {
               g_Img_GtApe = gtimg.LSet_A_ImgGtApe(g_Img.Img_Dat.toString());
               g_Img_Base  = img.LSet_A_ImgBase(g_Img_GtApe.GtApe_Bse.toString());
             }
          if (g_Img.Img_Dax.toString().trim().equals("ZZFIA"))
             {
               g_Img_GtFia = gtimg.LSet_A_ImgGtFia(g_Img.Img_Dat.toString());
               v_Img_GtFia.add (g_Img_GtFia);
             }
        }
  }
  //---------------------------------------------------------------------------------------
  public void Carga_Inicial(RunData data, Context context)
              throws Exception
  {
    g_MsgED135 = MSGED135.Inicia_MsgED135();
    rg.MoverA(g_MsgED135.ED135_Idr, "C");
    rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
    rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
    g_MsgED135 = MSGED135.LSet_A_MsgED135(g_Msg.Msg_Dat.toString());
    data.getUser().removeTemp("Agtp313_MsgED135");
    data.getUser().setTemp("Agtp313_MsgED135", g_MsgED135);
  }
  //---------------------------------------------------------------------------------------
  public Vector A_Tab_Img(RunData data)
  {
    v_Tab_Img = (Vector)data.getUser().getTemp("Agtp313_vTabImg");
    Vector v_Img = new Vector();
    v_Img.clear();
    rg.MoverA(g_Img.Img_Ntr, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_Img.Img_Dax, "BASE");
    rg.MoverA(g_Img.Img_Seq, "000");
    rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtApe(g_Img_GtApe));
    Vector vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
    v_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
    for (i=0; i<v_Img_GtFia.size(); i++)
        {
          g_Img_GtFia = (GT_IMG.Buf_Img_GtFia)v_Img_GtFia.elementAt(i);
          rg.MoverA(g_Img.Img_Dax, "ZZFIA");
          rg.MoverA(g_Img.Img_Seq, rg.Suma(g_Img.Img_Seq,0,"1",0,3,0));
          rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtFia(g_Img_GtFia));
          vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
          v_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
        }
    //--------------------------------------------------------------------------
    rg.MoverA(g_Img.Img_Seq, "000");
    for (i=0; i<v_Tab_Img.size(); i++)
        {
          lcData = img.LSet_De_Img((BF_IMG.Buf_Img)v_Tab_Img.elementAt(i));
          g_Img  = img.LSet_A_Img(lcData);
          if (g_Img.Img_Dax.toString().trim().equals("BASE")
           || g_Img.Img_Dax.toString().trim().equals("ZZFIA"))
             { }
          else
             {
               vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
               v_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
             }
        }
    //--------------------------------------------------------------------------
    return v_Img;
  }
//---------------------------------------------------------------------------------------
  public void doAgtp313_Continue(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP313[doAgtp313_Continue.start]", "[" + data.getUser().getUserName() + "]");
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Eclp090"))
       {
         g_PrmCL090 = PRMCL090.LSet_A_PrmCL090(g_Msg.Msg_Dat.toString());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp313_g_PrmPC080");
         data.getUser().removeTemp("Agtp313_g_PrmPC080");
         g_Img_GtFia = (GT_IMG.Buf_Img_GtFia)data.getUser().getTemp("Agtp313_ImgGtFia");
         if (g_PrmCL090.CL090_Cli.toString().trim().compareTo("")>0)
            {
              String RutCli = g_PrmCL090.CL090_Cli.toString();
              int liRetorno = Verifica_Cliente(RutCli, data, context);
              if (liRetorno!=0)
                 {
                   data.getUser().removeTemp("Agtp313_g_PrmPC080");
                   data.getUser().setTemp("Agtp313_g_PrmPC080", g_PrmPC080);
                   String lcError = "";
                   if (liRetorno==1)
                      { lcError = "Fiador no puede ser el Mismo que el Titular"; }
                   if (liRetorno==2)
                      { lcError = "Fiador ya se encuentra Seleccionado"; }
                   BF_MSG.MsgInt("Agtp313", data, g_Msg, lcError, "1");
                   return;
                 }
              rg.MoverA(g_Img_GtFia.GtFia_Cli, g_PrmCL090.CL090_Cli);
              rg.MoverA(g_Img_GtFia.GtFia_Ncl, g_PrmCL090.CL090_Ncl);
              if (g_PrmCL090.CL090_Tpr.toString().equals("N"))
                 {
                   MSGCL095.Bff_MsgCL095 d_MsgCL095 = new MSGCL095.Bff_MsgCL095();
                   g_MsgCL095 = MSGCL095.Inicia_MsgCL095();
                   rg.MoverA(g_MsgCL095.CL095_Idr, "I");
                   rg.MoverA(g_MsgCL095.CL095_Nro, "001");
                   d_MsgCL095 = (MSGCL095.Bff_MsgCL095)g_MsgCL095.CL095_Tap.elementAt(0);
                   rg.MoverA(d_MsgCL095.CL095_Cli, g_PrmCL090.CL090_Cli);
                   rg.MoverA(g_Msg.Msg_Dat, MSGCL095.LSet_De_MsgCL095(g_MsgCL095));
                   g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "CL095");
                   g_MsgCL095 = MSGCL095.LSet_A_MsgCL095(g_Msg.Msg_Dat.toString());
                   d_MsgCL095 = (MSGCL095.Bff_MsgCL095)g_MsgCL095.CL095_Tap.elementAt(0);
                   if (d_MsgCL095.CL095_Rcy.toString().trim().equals("ERROR"))
                      { rg.MoverA(d_MsgCL095.CL095_Rcy, rg.Zeros(d_MsgCL095.CL095_Rcy)); }
                   rg.MoverA(g_Img_GtFia.GtFia_Rcy, d_MsgCL095.CL095_Rcy);
                   rg.MoverA(g_Img_GtFia.GtFia_Ncy, d_MsgCL095.CL095_Ncy);
                 }
            }
         data.getUser().removeTemp("Agtp313_ImgGtFia");
         data.getUser().setTemp("Agtp313_ImgGtFia", g_Img_GtFia);
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP313.vm" );
         return;
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Appp030"))
       {
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp313_g_PrmPC080");
         data.getUser().removeTemp("Agtp313_g_PrmPC080");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP313.vm" );
         return;
       }
  }
  //---------------------------------------------------------------------------------------
  public int Verifica_Cliente(String RutCli, RunData data, Context context)
             throws Exception
  {
    if (g_PrmPC080.PC080_Cli.toString().equals(RutCli))
       { return 1; }
    v_Img_GtFia = (Vector)data.getUser().getTemp("Agtp313_vImgGtFia");
    for (i=0; i<v_Img_GtFia.size(); i++)
        {
          GT_IMG.Buf_Img_GtFia l_Img_GtFia = new GT_IMG.Buf_Img_GtFia();
          l_Img_GtFia = (GT_IMG.Buf_Img_GtFia)v_Img_GtFia.elementAt(i);
          if (l_Img_GtFia.GtFia_Cli.toString().equals(RutCli))
             { return 2; }
        }
    return 0;
  }
  //---------------------------------------------------------------------------------------
}