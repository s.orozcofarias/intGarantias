// Source File Name:   Agtp611.java
// Descripcion     :   Ingreso Nueva Garantia MYC [Mandatos y Contratos] (ED100, GT090, GT096)

package com.intGarantias.modules.actions;

//import com.FhtNucleo.services.configurator.FhtConfigurator;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.BF_IMG;
import com.FHTServlet.modules.global.BF_EVT;
import com.FHTServlet.modules.global.PRMPC080;
import com.FHTServlet.modules.global.PRMPP030;

import com.intGlobal.modules.global.PRMCL090;
import com.intGlobal.modules.global.PRMCL095;
import com.intGlobal.modules.global.MSGED100;
import com.intGlobal.modules.global.PRMTG132;

import com.intGarantias.modules.global.MSGGT090;
import com.intGarantias.modules.global.MSGGT096;
import com.intGarantias.modules.global.MSGGT677;
import com.intGarantias.modules.global.PRMGT111;
import com.intGarantias.modules.global.GT_IMG;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp611 extends FHTServletAction
{
  //===============================================================================================================================
  public int i = 0;
  public String lcData = "";
  public Vector vDatos;
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  BF_EVT.Buf_Evt g_Evt             = new BF_EVT.Buf_Evt();
  BF_IMG img                       = new BF_IMG();
  BF_IMG.Buf_Img g_Img             = new BF_IMG.Buf_Img();
  BF_IMG.Buf_Img_Bse g_Img_Base    = new BF_IMG.Buf_Img_Bse();
  GT_IMG gtimg                     = new GT_IMG();
  GT_IMG.Buf_Img_GtApe g_Img_GtApe = new GT_IMG.Buf_Img_GtApe();
  Vector v_Img_GtUsu               = new Vector();
  Vector v_Img_GtCmp               = new Vector();
  GT_IMG.Buf_Img_GtUsu g_Img_GtUsu = new GT_IMG.Buf_Img_GtUsu();
  PRMPC080.PrmPC080 g_PrmPC080     = new PRMPC080.PrmPC080();
  PRMCL090.Buf_PrmCL090 g_PrmCL090 = new PRMCL090.Buf_PrmCL090();
  PRMCL095.Buf_PrmCL095 g_PrmCL095 = new PRMCL095.Buf_PrmCL095();
  MSGED100.Buf_MsgED100 g_MsgED100 = new MSGED100.Buf_MsgED100();
  MSGGT090.Buf_MsgGT090 g_MsgGT090 = new MSGGT090.Buf_MsgGT090();
  MSGGT096.Buf_MsgGT096 g_MsgGT096 = new MSGGT096.Buf_MsgGT096();
  MSGGT677.Buf_MsgGT677 g_MsgGT677 = new MSGGT677.Buf_MsgGT677();
  PRMGT111.Buf_PrmGT111 g_PrmGT111 = new PRMGT111.Buf_PrmGT111();
  PRMTG132.Buf_PrmTG132 g_PrmTG132 = new PRMTG132.Buf_PrmTG132();
  PRMPP030.Buf_PrmPP030 g_PrmPP030 = new PRMPP030.Buf_PrmPP030();
  //-------------------------------------------------------------------------------------------
  public Agtp611()
  {
  }
  //===============================================================================================================================
  public void doPerform(RunData data, Context context) throws Exception
  {
    //Log.debug("AGTP611[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp611-DIN", data);
    if (g_Msg.Msg_Pgr.toString().equals("RT"))
       { doAgtp611_Continue(data, context); }
    else
       { doAgtp611_Init(data, context); }
  }
  //-------------------------------------------------------------------------------------------
  public void doAgtp611_Init(RunData data, Context context) throws Exception
  {
    //Log.debug("AGTP611[doAgtp611_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (g_PrmPC080.PC080_Cli.toString().trim().equals(""))
       {
         data.getUser().removeTemp("Agtp611_g_PrmPC080");
         data.getUser().setTemp("Agtp611_g_PrmPC080" ,g_PrmPC080);
         g_PrmCL090 = PRMCL090.Inicia_PrmCL090();
         rg.MoverA(g_PrmCL090.CL090_Pcc, "N");
         rg.MoverA(g_Msg.Msg_Dat, PRMCL090.LSet_De_PrmCL090(g_PrmCL090));
         BF_MSG.Link_Program("Agtp611", "Eclp090", data, g_Msg);
         return;
       }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (Asigna_Folios(data, context) == false )
       { return; }
    Trae_DcnDgt(data, context);
    Arma_ListGara(0, data, context);
    Arma_Inicial(data, context);
    Inicializa_Img(data, context);
    rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
    BF_MSG.Param_Program(data, g_Msg);
    data.getUser().removeTemp("Agtp611_PrmPC080");
    data.getUser().setTemp("Agtp611_PrmPC080", g_PrmPC080);
    setTemplate(data, "Garantias,Agt,AGTP611.vm");
  }
  //===============================================================================================================================
  public void doAgtp611(RunData data, Context context) throws Exception
  {
    //Log.debug("AGTP611[doAgtp611.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp611-MAN", data);
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    String Opc = data.getParameters().getString("Opc", "");
    if (Opc.trim().equals("S"))
       {
         //Boton Salir
         data.getUser().removeTemp("Agtp611_PrmPC080");
         data.getUser().removeTemp("Agtp611_g_MsgGT096");
         data.getUser().removeTemp("Agtp611_MsgGT090");
         data.getUser().removeTemp("Agtp611_Evt");
         data.getUser().removeTemp("Agtp611_g_ImgBase");
         data.getUser().removeTemp("Agtp611_ImgGtApe");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Program(data, g_Msg);
         return;
       }
    if (Opc.trim().equals("M"))
       {
         //Radio Cambio_Moneda
         Actualiza_Datos(data, context);
         String Tmn = data.getParameters().getString("Tmn", "");
         g_PrmTG132 = PRMTG132.Inicia_PrmTG132();
         if (Tmn.trim().equals("LOC"))
            {
              rg.MoverA(g_PrmTG132.TG132_Mnd, "000");
              rg.MoverA(g_PrmTG132.TG132_Nmn, "PESOS CHILE");
              Cambio_Moneda(data, context);
            }
         else
            {
              data.getUser().removeTemp("Agtp611_g_PrmPC080");
              data.getUser().setTemp("Agtp611_g_PrmPC080" ,g_PrmPC080);
              rg.MoverA(g_Msg.Msg_Dat, PRMTG132.LSet_De_PrmTG132(g_PrmTG132));
              BF_MSG.Link_Program("Agtp611", "Atgp132", data, g_Msg);
            }
         return;
       }
    if (Opc.trim().equals("C"))
       {
         //Boton Compartida
         Actualiza_Datos(data, context);
         v_Img_GtCmp = (Vector)data.getUser().getTemp("Agtp611_vImgGtCmp");
         int liCmp = v_Img_GtCmp.size();
         String lcCmp = Integer.toString(liCmp);
         g_PrmCL095 = PRMCL095.Inicia_PrmCL095();
         rg.MoverA(g_PrmCL095.CL095_Cli, g_PrmPC080.PC080_Cli);
         rg.MoverA(g_PrmCL095.CL095_Ncl, g_PrmPC080.PC080_Ncl);
         rg.MoverA(g_PrmCL095.CL095_Idr, "C");
         rg.MoverA(g_PrmCL095.CL095_Nro, rg.FmtValor(lcCmp,0,0,2,"+"));
         for (i=0; i<v_Img_GtCmp.size(); i++)
             {
               PRMCL095.Bff_PrmCL095 d_PrmCL095 = new PRMCL095.Bff_PrmCL095();
               g_Img_GtUsu = (GT_IMG.Buf_Img_GtUsu)v_Img_GtCmp.elementAt(i);
               rg.MoverA(d_PrmCL095.CL095_Cli, g_Img_GtUsu.GtUsu_Cli);
               rg.MoverA(d_PrmCL095.CL095_Rel, "");
               rg.MoverA(d_PrmCL095.CL095_Prc, "");
               rg.MoverA(d_PrmCL095.CL095_Orp, "");
               rg.MoverA(d_PrmCL095.CL095_Ncl, g_Img_GtUsu.GtUsu_Ncl);
               rg.MoverA(d_PrmCL095.CL095_Dir, "");
               rg.MoverA(d_PrmCL095.CL095_Rcy, "");
               rg.MoverA(d_PrmCL095.CL095_Ncy, "");
               rg.MoverA(d_PrmCL095.CL095_Rrp, "");
               rg.MoverA(d_PrmCL095.CL095_Nrp, "");
               rg.MoverA(d_PrmCL095.CL095_Rrs, "");
               rg.MoverA(d_PrmCL095.CL095_Nrs, "");
               rg.MoverA(d_PrmCL095.CL095_Obl, "");
               rg.MoverA(d_PrmCL095.CL095_Isg, "");
               g_PrmCL095.CL095_Tap.set(i, d_PrmCL095);
             }
         data.getUser().removeTemp("Agtp611_g_PrmPC080");
         data.getUser().setTemp("Agtp611_g_PrmPC080" ,g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMCL095.LSet_De_PrmCL095(g_PrmCL095));
         BF_MSG.Link_Program("Agtp611", "Aclp095", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("D"))
       {
         //Boton Deudores
         Actualiza_Datos(data, context);
         v_Img_GtUsu = (Vector)data.getUser().getTemp("Agtp611_vImgGtUsu");
         int liUsu = v_Img_GtUsu.size();
         String lcUsu = Integer.toString(liUsu);
         g_PrmCL095 = PRMCL095.Inicia_PrmCL095();
         rg.MoverA(g_PrmCL095.CL095_Cli, g_PrmPC080.PC080_Cli);
         rg.MoverA(g_PrmCL095.CL095_Ncl, g_PrmPC080.PC080_Ncl);
         rg.MoverA(g_PrmCL095.CL095_Idr, "E");
         rg.MoverA(g_PrmCL095.CL095_Nro, rg.FmtValor(lcUsu,0,0,2,"+"));
         for (i=0; i<v_Img_GtUsu.size(); i++)
             {
               PRMCL095.Bff_PrmCL095 d_PrmCL095 = new PRMCL095.Bff_PrmCL095();
               g_Img_GtUsu = (GT_IMG.Buf_Img_GtUsu)v_Img_GtUsu.elementAt(i);
               rg.MoverA(d_PrmCL095.CL095_Cli, g_Img_GtUsu.GtUsu_Cli);
               rg.MoverA(d_PrmCL095.CL095_Rel, "");
               rg.MoverA(d_PrmCL095.CL095_Prc, "");
               rg.MoverA(d_PrmCL095.CL095_Orp, "");
               rg.MoverA(d_PrmCL095.CL095_Ncl, g_Img_GtUsu.GtUsu_Ncl);
               rg.MoverA(d_PrmCL095.CL095_Dir, "");
               rg.MoverA(d_PrmCL095.CL095_Rcy, "");
               rg.MoverA(d_PrmCL095.CL095_Ncy, "");
               rg.MoverA(d_PrmCL095.CL095_Rrp, "");
               rg.MoverA(d_PrmCL095.CL095_Nrp, "");
               rg.MoverA(d_PrmCL095.CL095_Rrs, "");
               rg.MoverA(d_PrmCL095.CL095_Nrs, "");
               rg.MoverA(d_PrmCL095.CL095_Obl, "");
               rg.MoverA(d_PrmCL095.CL095_Isg, "");
               g_PrmCL095.CL095_Tap.set(i, d_PrmCL095);
             }
         data.getUser().removeTemp("Agtp611_g_PrmPC080");
         data.getUser().setTemp("Agtp611_g_PrmPC080" ,g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMCL095.LSet_De_PrmCL095(g_PrmCL095));
         BF_MSG.Link_Program("Agtp611", "Aclp095", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("P"))
       {
         //Grilla Producto
         Actualiza_Datos(data, context);
         String Prd = data.getParameters().getString("Prd", "");
         i = Integer.parseInt(Prd);
         g_MsgGT090 = (MSGGT090.Buf_MsgGT090)data.getUser().getTemp("Agtp611_MsgGT090");
         Arma_ListGara(i, data, context);
         Arma_Inicial(data, context);
         Inicializa_Img(data, context);
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         data.getUser().removeTemp("Agtp611_PrmPC080");
         data.getUser().setTemp("Agtp611_PrmPC080", g_PrmPC080);
         setTemplate(data, "Garantias,Agt,AGTP611.vm");
         return;
       }
    if (Opc.trim().equals("F"))
       {
         //Boton Amarrar Garantia Futura
         Actualiza_Datos(data, context);
         data.getUser().removeTemp("Agtp611_g_PrmPC080");
         data.getUser().setTemp("Agtp611_g_PrmPC080" ,g_PrmPC080);
         rg.MoverA(g_PrmPC080.PC080_Tgr.Gfu, g_Img_GtApe.GtApe_Gfu);
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Link_Program("Agtp611", "Agtp177", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("A"))
       {
         //Boton Aceptar
         Actualiza_Datos(data, context);
         Vector g_Tab_Img = A_Tab_Img(data);
         img.Graba_Img_Host(g_Msg, g_Tab_Img, data, context);
         rg.MoverA(g_Evt.Evt_Ndc, g_Img_GtApe.GtApe_Dsg);
         rg.MoverA(g_Evt.Evt_Vpr, rg.FmtValor(g_Img_GtApe.GtApe_Vcn, 2, 4, 15, "+"));
         BF_EVT t = new BF_EVT();
         g_MsgED100 = MSGED100.Inicia_MsgED100();
         rg.MoverA(g_MsgED100.ED100_Idr, "C");
         rg.MoverA(g_MsgED100.ED100_Nev, g_PrmPC080.PC080_Ntr);
         rg.MoverA(g_MsgED100.ED100_Evt, t.LSet_De_Evt(g_Evt));
         rg.MoverA(g_Msg.Msg_Dat, MSGED100.LSet_De_MsgED100(g_MsgED100));
         g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED100");
         rg.MoverA(g_MsgGT677.GT677_Idr, "C");
         rg.MoverA(g_MsgGT677.GT677_Gfu, g_Img_Base.Img_Base_Sis.toString() + g_Img_Base.Img_Base_Ncn.toString());
         rg.MoverA(g_MsgGT677.GT677_Gbs, g_Img_GtApe.GtApe_Gfu);
         rg.MoverA(g_MsgGT677.GT677_Est, "ENTMT");
         rg.MoverA(g_Msg.Msg_Dat, MSGGT677.LSet_De_MsgGT677(g_MsgGT677));
//         g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GT677");

         rg.MoverA(g_PrmPC080.PC080_Gti.Ind, "P");
         rg.MoverA(g_PrmPC080.PC080_Gti.Tip, g_Img_GtApe.GtApe_Ggr);
         rg.MoverA(g_PrmPC080.PC080_Gti.Dsc, g_Img_GtApe.GtApe_Dsg);
         rg.MoverA(g_PrmPC080.PC080_Gti.Fts, g_Evt.Evt_Fpr);
         rg.MoverA(g_PrmPC080.PC080_Gti.Vlc, g_Evt.Evt_Vpr);
         rg.MoverA(g_PrmPC080.PC080_Gti.Grd, g_Img_GtApe.GtApe_Grd);
         rg.MoverA(g_PrmPC080.PC080_Gti.Cbt, g_Img_GtApe.GtApe_Cbt);
         rg.MoverA(g_PrmPC080.PC080_Gti.Cmp, g_Img_GtApe.GtApe_Cmp);
         rg.MoverA(g_PrmPC080.PC080_Gti.Fuc, g_Img_GtApe.GtApe_Ftc);
         rg.MoverA(g_PrmPC080.PC080_Gti.Fvt, " ");
         data.getUser().removeTemp("Agtp611_PrmPC080");
         data.getUser().removeTemp("Agtp611_g_MsgGT096");
         data.getUser().removeTemp("Agtp611_MsgGT090");
         data.getUser().removeTemp("Agtp611_Evt");
         data.getUser().removeTemp("Agtp611_g_ImgBase");
         data.getUser().removeTemp("Agtp611_ImgGtApe");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "");
         rg.MoverA(g_PrmPC080.PC080_Mnt, g_Evt.Evt_Vpr);
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Link_Program("Agtp611", "Aedp120", data, g_Msg);
         return;
       }
  }
  //-------------------------------------------------------------------------------------------
  public void Actualiza_Datos(RunData data, Context context) throws Exception
  {
  //String Vle = data.getParameters().getString("GtApe_Vle", "");
    String Cbt = data.getParameters().getString("GtApe_Cbt", "");
    String Cmp = data.getParameters().getString("GtApe_Cmp", "");
    String Grd = data.getParameters().getString("GtApe_Grd", "");
    String Dsg = data.getParameters().getString("GtApe_Dsg", "");
    String Ilm = data.getParameters().getString("GtApe_Ilm", "");
    String Mtl = data.getParameters().getString("GtApe_Mtl", "");
    String Pcl = data.getParameters().getString("GtApe_Pcl", "");
    g_Evt       = (BF_EVT.Buf_Evt)data.getUser().getTemp("Agtp611_Evt");
    g_Img_Base  = (BF_IMG.Buf_Img_Bse)data.getUser().getTemp("Agtp611_g_ImgBase");
    g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp611_ImgGtApe");
  //rg.MoverA(g_Img_GtApe.GtApe_Vle, rg.Zeros(15));
    rg.MoverA(g_Img_GtApe.GtApe_Cbt, Cbt);
    rg.MoverA(g_Img_GtApe.GtApe_Cmp, Cmp);
    rg.MoverA(g_Img_GtApe.GtApe_Grd, Grd);
    rg.MoverA(g_Img_GtApe.GtApe_Dsg, Dsg);
    rg.MoverA(g_Img_GtApe.GtApe_Ilm, Ilm);
    rg.MoverA(g_Img_GtApe.GtApe_Mtl, Mtl);
    rg.MoverA(g_Img_GtApe.GtApe_Pcl, Pcl);
    rg.MoverA(g_Img_GtApe.GtApe_Bse, img.LSet_De_ImgBase_Bse(g_Img_Base));
    data.getUser().removeTemp("Agtp611_g_ImgBase");
    data.getUser().setTemp("Agtp611_g_ImgBase", g_Img_Base);
    data.getUser().removeTemp("Agtp611_ImgGtApe");
    data.getUser().setTemp("Agtp611_ImgGtApe", g_Img_GtApe);
  }
  //===============================================================================================================================
  public void doAgtp611_Continue(RunData data, Context context) throws Exception
  {
    //Log.debug("AGTP611[doAgtp611_Continue.start]", "[" + data.getUser().getUserName() + "]");
    if (RUTGEN.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Appp030"))
       {
         g_PrmPP030 = PRMPP030.LSet_A_PrmPP030(g_Msg.Msg_Dat.toString());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp611_g_PrmPC080");
         data.getUser().removeTemp("Agtp611_g_PrmPC080");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         if (g_PrmPP030.PP030_Rsp.toString().trim().equals("TERMINAR"))
            {
              //Limpieza(data, context);
              rg.MoverA(g_PrmPC080.PC080_Rtn, "NK");
              rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
              BF_MSG.Return_Data("Agtp611", data, g_Msg);
              return;
            }
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP611.vm");
         return;
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Agtp177"))
       {
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp611_g_PrmPC080");
         data.getUser().removeTemp("Agtp611_g_PrmPC080");
         PRMPC080.PrmPC080 l_PrmPC080 = new PRMPC080.PrmPC080();
         l_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
         if (l_PrmPC080.PC080_Rtn.toString().equals("OK"))
            {
              g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp611_ImgGtApe");
              rg.MoverA(g_Img_GtApe.GtApe_Gfu, l_PrmPC080.PC080_Tgr.Gfu);
              data.getUser().removeTemp("Agtp611_ImgGtApe");
              data.getUser().setTemp("Agtp611_ImgGtApe", g_Img_GtApe);
            }
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP611.vm");
         return;
       }
    if (RUTGEN.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Eclp090"))
       {
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp611_g_PrmPC080");
         data.getUser().removeTemp("Agtp611_g_PrmPC080");
         g_PrmCL090 = PRMCL090.LSet_A_PrmCL090(g_Msg.Msg_Dat.toString());
         if (g_PrmCL090.CL090_Cli.toString().trim().equals(""))
            {
              //Limpieza(data, context);
              rg.MoverA(g_PrmPC080.PC080_Rtn, "NK");
              rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
              BF_MSG.Return_Data("Agtp611", data, g_Msg);
              return;
            }
         rg.MoverA(g_PrmPC080.PC080_Cli, g_PrmCL090.CL090_Cli);
         rg.MoverA(g_PrmPC080.PC080_Ncl, g_PrmCL090.CL090_Ncl);
         rg.MoverA(g_PrmPC080.PC080_Tpr, g_PrmCL090.CL090_Tpr);
         rg.MoverA(g_PrmPC080.PC080_Acc, "APERT");
         rg.MoverA(g_PrmPC080.PC080_Ttr, "APERT");
         rg.MoverA(g_PrmPC080.PC080_Ntt, "CONSTITUCION DE GARANTIA");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         doAgtp611_Init(data, context);
         return;
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Atgp132"))
       {
         g_PrmTG132 = PRMTG132.LSet_A_PrmTG132(g_Msg.Msg_Dat.toString());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp611_g_PrmPC080");
         data.getUser().removeTemp("Agtp611_g_PrmPC080");
         Cambio_Moneda(data, context);
         return;
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Aedp120"))
       {
         BF_MSG.Return_Program(data, g_Msg);
         return;
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Aclp095"))
       {
         g_PrmCL095 = PRMCL095.LSet_A_PrmCL095(g_Msg.Msg_Dat.toString());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp611_g_PrmPC080");
         data.getUser().removeTemp("Agtp611_g_PrmPC080");
         if (g_PrmCL095.CL095_Rtn.toString().equals("OK"))
            {
              PRMCL095.Bff_PrmCL095 d_PrmCL095 = new PRMCL095.Bff_PrmCL095();
              if (g_PrmCL095.CL095_Idr.toString().equals("C"))                   //Compartidos
                 {
                   v_Img_GtCmp.clear();
                   for (i=0; i<g_PrmCL095.CL095_Tap.size(); i++)
                       {
                         d_PrmCL095 = (PRMCL095.Bff_PrmCL095)g_PrmCL095.CL095_Tap.elementAt(i);
                         if (d_PrmCL095.CL095_Cli.toString().trim().equals("")) break;
                         GT_IMG.Buf_Img_GtUsu l_Img_GtUsu = new GT_IMG.Buf_Img_GtUsu();
                         rg.MoverA(l_Img_GtUsu.GtUsu_Cli, d_PrmCL095.CL095_Cli);
                         rg.MoverA(l_Img_GtUsu.GtUsu_Ncl, d_PrmCL095.CL095_Ncl);
                         rg.MoverA(l_Img_GtUsu.GtUsu_Idc, "C");
                         rg.MoverA(l_Img_GtUsu.GtUsu_Fll, " ");
                         v_Img_GtCmp.add (l_Img_GtUsu);
                       }
                   data.getUser().removeTemp("Agtp611_vImgGtCmp");
                   data.getUser().setTemp("Agtp611_vImgGtCmp", v_Img_GtCmp);
                 }
              if (g_PrmCL095.CL095_Idr.toString().equals("E"))                   //Deudores
                 {
                   v_Img_GtUsu.clear();
                   for (i=0; i<g_PrmCL095.CL095_Tap.size(); i++)
                       {
                         d_PrmCL095 = (PRMCL095.Bff_PrmCL095)g_PrmCL095.CL095_Tap.elementAt(i);
                         if (d_PrmCL095.CL095_Cli.toString().trim().equals("")) break;
                         GT_IMG.Buf_Img_GtUsu l_Img_GtUsu = new GT_IMG.Buf_Img_GtUsu();
                         rg.MoverA(l_Img_GtUsu.GtUsu_Cli, d_PrmCL095.CL095_Cli);
                         rg.MoverA(l_Img_GtUsu.GtUsu_Ncl, d_PrmCL095.CL095_Ncl);
                         rg.MoverA(l_Img_GtUsu.GtUsu_Idc, " ");
                         rg.MoverA(l_Img_GtUsu.GtUsu_Fll, " ");
                         v_Img_GtUsu.add (l_Img_GtUsu);
                       }
                   data.getUser().removeTemp("Agtp611_vImgGtUsu");
                   data.getUser().setTemp("Agtp611_vImgGtUsu", v_Img_GtUsu);
                 }
            }
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP611.vm" );
         return;
       }
  }
  //===============================================================================================================================
  public void Cambio_Moneda(RunData data, Context context) throws Exception
  {
    g_Evt      = (BF_EVT.Buf_Evt)data.getUser().getTemp("Agtp611_Evt");
    g_Img_Base = (BF_IMG.Buf_Img_Bse)data.getUser().getTemp("Agtp611_g_ImgBase");
    rg.MoverA(g_Evt.Evt_Mnd, g_PrmTG132.TG132_Mnd);
    rg.MoverA(g_Evt.Evt_Nmn, g_PrmTG132.TG132_Nmn);
    data.getUser().removeTemp("Agtp611_Evt");
    data.getUser().setTemp("Agtp611_Evt", g_Evt);
    rg.MoverA(g_Img_Base.Img_Base_Mnd, g_Evt.Evt_Mnd);
    rg.MoverA(g_Img_Base.Img_Base_Nmn, g_Evt.Evt_Nmn);
    data.getUser().removeTemp("Agtp611_g_ImgBase");
    data.getUser().setTemp("Agtp611_g_ImgBase", g_Img_Base);
    rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
    BF_MSG.Param_Program(data, g_Msg);
    data.getUser().removeTemp("Agtp611_PrmPC080");
    data.getUser().setTemp("Agtp611_PrmPC080", g_PrmPC080);
    setTemplate(data, "Garantias,Agt,AGTP611.vm");
  }
  //-------------------------------------------------------------------------------------------
  public boolean Asigna_Folios(RunData data, Context context) throws Exception
  {
    g_MsgGT096 = MSGGT096.Inicia_MsgGT096();
    rg.MoverA(g_MsgGT096.GT096_Idr, "Q");
    rg.MoverA(g_MsgGT096.GT096_Cli, g_PrmPC080.PC080_Cli);
    rg.MoverA(g_Msg.Msg_Dat, MSGGT096.LSet_De_MsgGT096(g_MsgGT096));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GT096");
    g_MsgGT096 = MSGGT096.LSet_A_MsgGT096(g_Msg.Msg_Dat.toString());
    if (g_MsgGT096.GT096_Rtn.toString().trim().equals("E") )
       {
         data.getUser().removeTemp("Agtp611_g_PrmPC080");
         data.getUser().setTemp("Agtp611_g_PrmPC080", g_PrmPC080);
         BF_MSG.MsgInt("Agtp611", data, g_Msg, g_MsgGT096.GT096_Mse.toString(), "T");
         return false;
       }
    rg.MoverA(g_PrmPC080.PC080_Ntr, g_MsgGT096.GT096_Ntr);
    rg.MoverA(g_PrmPC080.PC080_Cnr.Ncn, g_MsgGT096.GT096_Ncn);
    data.getUser().removeTemp("Agtp611_g_MsgGT096");
    data.getUser().setTemp("Agtp611_g_MsgGT096", g_MsgGT096);
    return true;
  }
  //-------------------------------------------------------------------------------------------
  public void Trae_DcnDgt(RunData data, Context context) throws Exception
  {
    g_MsgGT090 = MSGGT090.Inicia_MsgGT090();
    rg.MoverA(g_MsgGT090.GT090_Idr, "I");
    rg.MoverA(g_MsgGT090.GT090_Sis, g_PrmPC080.PC080_Cnr.Sis);
    Vector wTmp = new Vector();
    int r = 0;
    while (g_MsgGT090.getIdr().compareTo("F")!=0 &&
           g_MsgGT090.getIdr().compareTo("L")!=0)
       {
         if (g_MsgGT090.getIdr().compareTo("I")!=0)
            {
              Vector vTmp = g_MsgGT090.getTab();
              int p = vTmp.size() - 1;
              MSGGT090.Bff_MsgGT090 dMsgGT090 = (MSGGT090.Bff_MsgGT090)vTmp.elementAt(p);
              g_MsgGT090 = MSGGT090.Inicia_MsgGT090();
              rg.MoverA(g_MsgGT090.GT090_Idr, "S");
              rg.MoverA(g_MsgGT090.GT090_Sis, g_PrmPC080.PC080_Cnr.Sis);
              g_MsgGT090.GT090_Tab.set(0, dMsgGT090);
              r = 1;
            }
         rg.MoverA(g_Msg.Msg_Dat, MSGGT090.LSet_De_MsgGT090(g_MsgGT090));
         g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GT090");
         g_MsgGT090 = MSGGT090.LSet_A_MsgGT090(g_Msg.Msg_Dat.toString());
         if (g_MsgGT090.getNro().trim().equals("")) break;
         for (int i=r; i<g_MsgGT090.getTab().size(); i++)
            {
              MSGGT090.Bff_MsgGT090 dtMsgGT090 = (MSGGT090.Bff_MsgGT090)g_MsgGT090.getTab().elementAt(i);
              wTmp.add(dtMsgGT090);
            }
       }
    g_MsgGT090.GT090_Tab = wTmp;
    data.getUser().removeTemp("Agtp611_MsgGT090");
    data.getUser().setTemp("Agtp611_MsgGT090" ,g_MsgGT090);
  }
  //-------------------------------------------------------------------------------------------
  public void Arma_ListGara(int p, RunData data, Context context) throws Exception
  {
    Vector vTmp = g_MsgGT090.getTab();
    MSGGT090.Bff_MsgGT090 d_MsgGT090 = new MSGGT090.Bff_MsgGT090();
    d_MsgGT090 = (MSGGT090.Bff_MsgGT090)vTmp.elementAt(p);
    rg.MoverA(g_PrmGT111.GT111_Idr,     " ");
    rg.MoverA(g_PrmGT111.GT111_Rtn,     "OK");
    rg.MoverA(g_PrmGT111.GT111_Dcn,     d_MsgGT090.GT090_Dcn);
    rg.MoverA(g_PrmGT111.GT111_Ndc,     d_MsgGT090.GT090_Dsc);
    rg.MoverA(g_PrmGT111.GT111_Tmn,     d_MsgGT090.GT090_Tmn);
    rg.MoverA(g_PrmGT111.GT111_Trj,     d_MsgGT090.GT090_Trj);
    rg.MoverA(g_PrmGT111.GT111_Dcm,     d_MsgGT090.GT090_Dcm);
    rg.MoverA(g_PrmGT111.GT111_Prp,     d_MsgGT090.GT090_Prp);
    rg.MoverA(g_PrmGT111.GT111_Grt_Ggr, d_MsgGT090.GT090_Ggr);
    rg.MoverA(g_PrmGT111.GT111_Grt_Itg, d_MsgGT090.GT090_Sgr);
    rg.MoverA(g_PrmGT111.GT111_Grt_Tob, d_MsgGT090.GT090_Tob);
    rg.MoverA(g_PrmGT111.GT111_Grt_Plm, d_MsgGT090.GT090_Plm);
    rg.MoverA(g_PrmGT111.GT111_Grt_Usg, "NA");
    rg.MoverA(g_PrmGT111.GT111_Grt_Nug, "NA");
    rg.MoverA(g_PrmGT111.GT111_Grt_Pct, d_MsgGT090.GT090_Cms);
  }
  //-------------------------------------------------------------------------------------------
  public void Arma_Inicial(RunData data, Context context ) throws Exception
  {
    g_MsgGT096 = (MSGGT096.Buf_MsgGT096)data.getUser().getTemp("Agtp611_g_MsgGT096");
    if (g_PrmGT111.getTmn().trim().equals("LOC"))
       {
         rg.MoverA(g_PrmPC080.PC080_Mnd, "000");
         rg.MoverA(g_PrmPC080.PC080_Nmn, "PESOS CHILE");
       }
    else
       {
         rg.MoverA(g_PrmPC080.PC080_Mnd, " ");
         rg.MoverA(g_PrmPC080.PC080_Nmn, " ");
       }
    rg.MoverA(g_PrmPC080.PC080_Ntr, g_MsgGT096.GT096_Ntr);
    rg.MoverA(g_PrmPC080.PC080_Ncl, g_MsgGT096.GT096_Ncl);
    rg.MoverA(g_PrmPC080.PC080_Tpr, g_MsgGT096.GT096_Tpr);
    rg.MoverA(g_PrmPC080.PC080_Dir, g_MsgGT096.GT096_Dir);
    rg.MoverA(g_PrmPC080.PC080_Cmn, g_MsgGT096.GT096_Cmn);
    rg.MoverA(g_PrmPC080.PC080_Tfc, g_MsgGT096.GT096_Tfc);
    rg.MoverA(g_Evt.Evt_Ttr, g_PrmPC080.PC080_Ttr);
    rg.MoverA(g_Evt.Evt_Ntt, g_PrmPC080.PC080_Ntt);
    rg.MoverA(g_Evt.Evt_Tsl, g_PrmPC080.PC080_Slr.Tsl);
    rg.MoverA(g_Evt.Evt_Slc, g_PrmPC080.PC080_Slr.Slc);
    rg.MoverA(g_Evt.Evt_Cli, g_PrmPC080.PC080_Cli);
    rg.MoverA(g_Evt.Evt_Ncl, g_PrmPC080.PC080_Ncl);
    rg.MoverA(g_Evt.Evt_Tpr, g_PrmPC080.PC080_Tpr);
    rg.MoverA(g_Evt.Evt_Dir, g_PrmPC080.PC080_Dir);
    rg.MoverA(g_Evt.Evt_Cmn, g_PrmPC080.PC080_Cmn);
    rg.MoverA(g_Evt.Evt_Tfc, g_PrmPC080.PC080_Tfc);
    rg.MoverA(g_Evt.Evt_Sis, g_PrmPC080.PC080_Cnr.Sis);
    rg.MoverA(g_Evt.Evt_Ncn, g_PrmPC080.PC080_Cnr.Ncn);
    rg.MoverA(g_Evt.Evt_Dcn, g_PrmGT111.GT111_Dcn);
    rg.MoverA(g_Evt.Evt_Ndc, g_PrmGT111.GT111_Ndc);
    rg.MoverA(g_Evt.Evt_Tmn, g_PrmGT111.GT111_Tmn);
    rg.MoverA(g_Evt.Evt_Trj, g_PrmGT111.GT111_Trj);
    rg.MoverA(g_Evt.Evt_Dcm, g_PrmGT111.GT111_Dcm);
    rg.MoverA(g_Evt.Evt_Prp, g_PrmGT111.GT111_Prp);
    rg.MoverA(g_Evt.Evt_Suc, g_PrmPC080.PC080_Suc);
    rg.MoverA(g_Evt.Evt_Nsu, g_PrmPC080.PC080_Nsu);
    rg.MoverA(g_Evt.Evt_Mnd, g_PrmPC080.PC080_Mnd);
    rg.MoverA(g_Evt.Evt_Nmn, g_PrmPC080.PC080_Nmn);
    rg.MoverA(g_Evt.Evt_Eje, g_PrmPC080.PC080_Eje);
    rg.MoverA(g_Evt.Evt_Nej, g_PrmPC080.PC080_Nej);
    rg.MoverA(g_Evt.Evt_Tej, g_PrmPC080.PC080_Tej);
    rg.MoverA(g_Evt.Evt_Est, "NUEVA");
    rg.MoverA(g_Evt.Evt_Fst, g_Msg.Msg_Fch);
    rg.MoverA(g_Evt.Evt_Upr, g_PrmGT111.GT111_Trj);
    rg.MoverA(g_Evt.Evt_Vpr, rg.Zeros(g_Evt.Evt_Vpr));
    rg.MoverA(g_Evt.Evt_Fpr, g_Msg.Msg_Fch);
    rg.MoverA(g_Evt.Evt_Spr, rg.Zeros(g_Evt.Evt_Spr));
    rg.MoverA(g_Evt.Evt_Tpp, " ");
    rg.MoverA(g_Evt.Evt_Ggr, g_PrmGT111.GT111_Grt_Ggr);
    rg.MoverA(g_Evt.Evt_Fll, " ");
    data.getUser().removeTemp("Agtp611_Evt");
    data.getUser().setTemp("Agtp611_Evt", g_Evt);
  }
  //-------------------------------------------------------------------------------------------
  public void Inicializa_Img(RunData data, Context context ) throws Exception
  {
    rg.MoverA(g_Img_Base.Img_Base_Ntr, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_Img_Base.Img_Base_Ttr, g_Evt.Evt_Ttr);
    rg.MoverA(g_Img_Base.Img_Base_Ntt, g_Evt.Evt_Ntt);
    rg.MoverA(g_Img_Base.Img_Base_Tsl, g_Evt.Evt_Tsl);
    rg.MoverA(g_Img_Base.Img_Base_Slc, g_Evt.Evt_Slc);
    rg.MoverA(g_Img_Base.Img_Base_Cmt, " ");
    rg.MoverA(g_Img_Base.Img_Base_Cli, g_Evt.Evt_Cli);
    rg.MoverA(g_Img_Base.Img_Base_Ncl, g_Evt.Evt_Ncl);
    rg.MoverA(g_Img_Base.Img_Base_Tpr, g_Evt.Evt_Tpr);
    rg.MoverA(g_Img_Base.Img_Base_Dir, g_Evt.Evt_Dir);
    rg.MoverA(g_Img_Base.Img_Base_Tfc, g_Evt.Evt_Tfc);
    rg.MoverA(g_Img_Base.Img_Base_Sis, g_Evt.Evt_Sis);
    rg.MoverA(g_Img_Base.Img_Base_Ncn, g_Evt.Evt_Ncn);
    rg.MoverA(g_Img_Base.Img_Base_Dcn, g_Evt.Evt_Dcn);
    rg.MoverA(g_Img_Base.Img_Base_Ndc, g_Evt.Evt_Ndc);
    rg.MoverA(g_Img_Base.Img_Base_Tmn, g_Evt.Evt_Tmn);
    rg.MoverA(g_Img_Base.Img_Base_Trj, g_Evt.Evt_Trj);
    rg.MoverA(g_Img_Base.Img_Base_Dcm, g_Evt.Evt_Dcm);
    rg.MoverA(g_Img_Base.Img_Base_Prp, g_Evt.Evt_Prp);
    rg.MoverA(g_Img_Base.Img_Base_Suc, g_Evt.Evt_Suc);
    rg.MoverA(g_Img_Base.Img_Base_Nsu, g_Evt.Evt_Nsu);
    rg.MoverA(g_Img_Base.Img_Base_Mnd, g_Evt.Evt_Mnd);
    rg.MoverA(g_Img_Base.Img_Base_Nmn, g_Evt.Evt_Nmn);
    rg.MoverA(g_Img_Base.Img_Base_Eje, g_Evt.Evt_Eje);
    rg.MoverA(g_Img_Base.Img_Base_Nej, g_Evt.Evt_Nej);
    rg.MoverA(g_Img_Base.Img_Base_Tej, g_Evt.Evt_Tej);
    data.getUser().removeTemp("Agtp611_g_ImgBase");
    data.getUser().setTemp("Agtp611_g_ImgBase", g_Img_Base);
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    rg.MoverA(g_Img_GtApe.GtApe_Fpr, g_MsgGT096.GT096_Fec);
    rg.MoverA(g_Img_GtApe.GtApe_Rgf, g_MsgGT096.GT096_Gfu);
    rg.MoverA(g_Img_GtApe.GtApe_Gfu, "");
    rg.MoverA(g_Img_GtApe.GtApe_Ftc, g_MsgGT096.GT096_Fec);
    rg.MoverA(g_Img_GtApe.GtApe_Cbp, "");
    rg.MoverA(g_Img_GtApe.GtApe_Dbp, rg.Zeros(g_Img_GtApe.GtApe_Dbp));
    rg.MoverA(g_Img_GtApe.GtApe_Tob, g_PrmGT111.GT111_Grt_Tob);
    rg.MoverA(g_Img_GtApe.GtApe_Plm, g_PrmGT111.GT111_Grt_Plm);
    rg.MoverA(g_Img_GtApe.GtApe_Ggr, g_PrmGT111.GT111_Grt_Ggr);
    rg.MoverA(g_Img_GtApe.GtApe_Dsg, "");
    if (g_Img_GtApe.getTob().equals("S"))
       { rg.MoverA(g_Img_GtApe.GtApe_Cbt, "G"); }
    else
       { rg.MoverA(g_Img_GtApe.GtApe_Cbt, g_Img_GtApe.GtApe_Tob); }
    rg.MoverA(g_Img_GtApe.GtApe_Grd, "1");
    rg.MoverA(g_Img_GtApe.GtApe_Cmp, "N");
    rg.MoverA(g_Img_GtApe.GtApe_Sgr, "N");
    rg.MoverA(g_Img_GtApe.GtApe_Ctb, "N");
    if (g_Img_GtApe.getPlm().equals("N"))
       { rg.MoverA(g_Img_GtApe.GtApe_Ilm, "N"); }
    else
       { rg.MoverA(g_Img_GtApe.GtApe_Ilm, "?"); }
    rg.MoverA(g_Img_GtApe.GtApe_Pcl, rg.Zeros(g_Img_GtApe.GtApe_Pcl));
    rg.MoverA(g_Img_GtApe.GtApe_Mtl, rg.Zeros(g_Img_GtApe.GtApe_Mtl));
    rg.MoverA(g_Img_GtApe.GtApe_Fig, "");
    rg.MoverA(g_Img_GtApe.GtApe_Tsd, "");
    rg.MoverA(g_Img_GtApe.GtApe_Fts, "");
    rg.MoverA(g_Img_GtApe.GtApe_Vuo, rg.Zeros(g_Img_GtApe.GtApe_Vuo));
    rg.MoverA(g_Img_GtApe.GtApe_Vtt, rg.Zeros(g_Img_GtApe.GtApe_Vtt));
    rg.MoverA(g_Img_GtApe.GtApe_Vts, rg.Zeros(g_Img_GtApe.GtApe_Vts));
    rg.MoverA(g_Img_GtApe.GtApe_Vtb, rg.Zeros(g_Img_GtApe.GtApe_Vtb));
    rg.MoverA(g_Img_GtApe.GtApe_Vcn, rg.Zeros(g_Img_GtApe.GtApe_Vcn));
    rg.MoverA(g_Img_GtApe.GtApe_Cbg, "");
    rg.MoverA(g_Img_GtApe.GtApe_Nct, "");
    rg.MoverA(g_Img_GtApe.GtApe_Fct, "");
    rg.MoverA(g_Img_GtApe.GtApe_Fvt, "");
    rg.MoverA(g_Img_GtApe.GtApe_Vuf, rg.Zeros(g_Img_GtApe.GtApe_Vuf));
    rg.MoverA(g_Img_GtApe.GtApe_Vus, rg.Zeros(g_Img_GtApe.GtApe_Vus));
    rg.MoverA(g_Img_GtApe.GtApe_Hvt, rg.Zeros(g_Img_GtApe.GtApe_Hvt));
    rg.MoverA(g_Img_GtApe.GtApe_Hvl, rg.Zeros(g_Img_GtApe.GtApe_Hvl));
    rg.MoverA(g_Img_GtApe.GtApe_Hvb, rg.Zeros(g_Img_GtApe.GtApe_Hvb));
    rg.MoverA(g_Img_GtApe.GtApe_Hvc, rg.Zeros(g_Img_GtApe.GtApe_Hvc));
    rg.MoverA(g_Img_GtApe.GtApe_Pvt, rg.Zeros(g_Img_GtApe.GtApe_Pvt));
    rg.MoverA(g_Img_GtApe.GtApe_Pvl, rg.Zeros(g_Img_GtApe.GtApe_Pvl));
    rg.MoverA(g_Img_GtApe.GtApe_Pvb, rg.Zeros(g_Img_GtApe.GtApe_Pvb));
    rg.MoverA(g_Img_GtApe.GtApe_Pvc, rg.Zeros(g_Img_GtApe.GtApe_Pvc));
    rg.MoverA(g_Img_GtApe.GtApe_Psw, "");
    rg.MoverA(g_Img_GtApe.GtApe_Psq, "");
    rg.MoverA(g_Img_GtApe.GtApe_Psd, "");
    rg.MoverA(g_Img_GtApe.GtApe_Idx, "");
    rg.MoverA(g_Img_GtApe.GtApe_Tfr, "");
    rg.MoverA(g_Img_GtApe.GtApe_Dpv, "");
    rg.MoverA(g_Img_GtApe.GtApe_Dpp, "");
    rg.MoverA(g_Img_GtApe.GtApe_Dsw, "");
    rg.MoverA(g_Img_GtApe.GtApe_Fll, "");
    v_Img_GtUsu.clear();
    v_Img_GtCmp.clear();
    data.getUser().removeTemp("Agtp611_ImgGtApe");
    data.getUser().setTemp("Agtp611_ImgGtApe", g_Img_GtApe);
    data.getUser().removeTemp("Agtp611_vImgGtCmp");
    data.getUser().setTemp("Agtp611_vImgGtCmp", v_Img_GtCmp);
    data.getUser().removeTemp("Agtp611_vImgGtUsu");
    data.getUser().setTemp("Agtp611_vImgGtUsu", v_Img_GtUsu);
  }
  //-------------------------------------------------------------------------------------------
  public Vector A_Tab_Img(RunData data)
  {
    rg.MoverA(g_Img.Img_Ntr, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_Img.Img_Dax, "BASE");
    rg.MoverA(g_Img.Img_Seq, "000");
    rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtApe(g_Img_GtApe));
    Vector v_Img = img.LSet_A_vImg(img.LSet_De_Img(g_Img));
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    v_Img_GtCmp = (Vector)data.getUser().getTemp("Agtp611_vImgGtCmp");
    v_Img_GtUsu = (Vector)data.getUser().getTemp("Agtp611_vImgGtUsu");
    rg.MoverA(g_Img.Img_Seq, "000");
    if (v_Img_GtCmp.size()!=0 || v_Img_GtUsu.size()!=0)
       {
         rg.MoverA(g_Img_GtUsu.GtUsu_Cli, g_PrmPC080.PC080_Cli);
         rg.MoverA(g_Img_GtUsu.GtUsu_Ncl, g_PrmPC080.PC080_Ncl);
         rg.MoverA(g_Img_GtUsu.GtUsu_Idc, " ");
         rg.MoverA(g_Img_GtUsu.GtUsu_Fll, " ");
         rg.MoverA(g_Img.Img_Dax, "YYUSU");
         rg.MoverA(g_Img.Img_Seq, rg.Suma(g_Img.Img_Seq,0,"1",0,3,0));
         rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtUsu(g_Img_GtUsu));
         Vector vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
         v_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
       }
    for (i=0; i<v_Img_GtCmp.size(); i++)
        {
          g_Img_GtUsu = (GT_IMG.Buf_Img_GtUsu)v_Img_GtCmp.elementAt(i);
          rg.MoverA(g_Img.Img_Dax, "YYCMP");
          rg.MoverA(g_Img.Img_Seq, rg.Suma(g_Img.Img_Seq,0,"1",0,3,0));
          rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtUsu(g_Img_GtUsu));
          Vector vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
          v_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
        }
    for (i=0; i<v_Img_GtUsu.size(); i++)
        {
          g_Img_GtUsu = (GT_IMG.Buf_Img_GtUsu)v_Img_GtUsu.elementAt(i);
          rg.MoverA(g_Img.Img_Dax, "YYUSU");
          rg.MoverA(g_Img.Img_Seq, rg.Suma(g_Img.Img_Seq,0,"1",0,3,0));
          rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtUsu(g_Img_GtUsu));
          Vector vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
          v_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
        }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    return v_Img;
  }
  //===============================================================================================================================
}