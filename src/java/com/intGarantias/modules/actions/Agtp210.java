// Source File Name:   Agtp210.java
// Descripcion     :   Eventos en Tramite [Abogado] (ED135, GT210)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.PRMPC080;

import com.intGlobal.modules.global.MSGED135;
import com.intGlobal.modules.global.PRMED210;

import com.intGarantias.modules.global.MSGGT210;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp210 extends FHTServletAction
{
  //---------------------------------------------------------------------------------------
  public int    i = 0;
  public String lcRetorno = "";
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  PRMPC080.PrmPC080 g_PrmPC080     = new PRMPC080.PrmPC080();
  MSGED135.Buf_MsgED135 g_MsgED135 = new MSGED135.Buf_MsgED135();
  MSGGT210.Buf_MsgGT210 g_MsgGT210 = new MSGGT210.Buf_MsgGT210();
  PRMED210.Buf_PrmED210 g_PrmED210 = new PRMED210.Buf_PrmED210();
  //---------------------------------------------------------------------------------------
  public Agtp210()
  {
  }
  //---------------------------------------------------------------------------------------
  public void doPerform(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP210[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp210-DIN", data);
    if (g_Msg.Msg_Pgr.toString().equals("RT"))
       { doAgtp210_Continue(data, context); }
    else
       { doAgtp210_Init(data, context); }
  }
  //---------------------------------------------------------------------------------------
  public void doCancel(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP210[doCancel.start]", "[CANCELADO]");
    setTemplate(data, "Garantias,Agt,AGTP210.vm");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp210_Init(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP210[doAgtp210_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    g_MsgGT210 = MSGGT210.Inicia_MsgGT210();
    rg.MoverA(g_MsgGT210.GT210_Idr, "I");
    rg.MoverA(g_MsgGT210.GT210_Abg, g_PrmPC080.PC080_Abg);
    Poblar_Panel(data, context, "I");
    setTemplate(data, "Garantias,Agt,AGTP210.vm" );
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp210(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP210[doAgtp210.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp210-MAN", data);
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    String Opc = data.getParameters().getString("Opc");
    if  (Opc.equals("S"))
        {
          //Salir
          data.getUser().removeTemp("Agtp210_Msg");
          data.getUser().removeTemp("Agtp210_MsgGT210");
          BF_MSG.Return_Program(data, g_Msg);
          return;
        }
    g_MsgGT210 = (MSGGT210.Buf_MsgGT210)data.getUser().getTemp("Agtp210_MsgGT210");
    MSGGT210.Bff_MsgGT210 d_MsgGT210 = new MSGGT210.Bff_MsgGT210();
    if  (Opc.equals("O"))
        {
          //Aceptar OK
          String Seq = data.getParameters().getString("Seq", "");
          i = Integer.parseInt(Seq);
          d_MsgGT210 = (MSGGT210.Bff_MsgGT210)g_MsgGT210.GT210_Tab.elementAt(i);
          if (d_MsgGT210.Tmt_Est.toString().compareTo("ENABG")!=0)
             {
               data.getUser().removeTemp("Agtp210_g_PrmPC080");
               data.getUser().setTemp("Agtp210_g_PrmPC080", g_PrmPC080);
               BF_MSG.MsgInt("Agtp210", data, g_Msg, "Ya Est� Aceptado", "1"); 
      	       return;
             }
          rg.MoverA(g_PrmPC080.PC080_Rtn,     "RA");
          rg.MoverA(g_PrmPC080.PC080_Cli,     d_MsgGT210.Evt_Cli);
          rg.MoverA(g_PrmPC080.PC080_Ncl,     d_MsgGT210.Evt_Ncl);
          rg.MoverA(g_PrmPC080.PC080_Cnr.Sis, d_MsgGT210.Evt_Sis);
          rg.MoverA(g_PrmPC080.PC080_Cnr.Ncn, d_MsgGT210.Evt_Ncn);
          rg.MoverA(g_PrmPC080.PC080_Dcn,     d_MsgGT210.Evt_Dcn);
          rg.MoverA(g_PrmPC080.PC080_Ndc,     d_MsgGT210.Evt_Dsc);
          rg.MoverA(g_PrmPC080.PC080_Ntr,     d_MsgGT210.Tmt_Nev);
          rg.MoverA(g_PrmPC080.PC080_Ttr,     d_MsgGT210.Evt_Ttr);
          rg.MoverA(g_PrmPC080.PC080_Trt,     d_MsgGT210.Tmt_Trt);
          rg.MoverA(g_PrmPC080.PC080_Mnd,     d_MsgGT210.Evt_Mnd);
          rg.MoverA(g_PrmPC080.PC080_Mnt,     d_MsgGT210.Evt_Vle);
          rg.MoverA(g_PrmPC080.PC080_Trj,     d_MsgGT210.Evt_Trj);
          rg.MoverA(g_PrmPC080.PC080_Dcm,     d_MsgGT210.Evt_Dcm);
          rg.MoverA(g_PrmPC080.PC080_Ggr,     " ");
          rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
          BF_MSG.Link_Program("Agtp210", "Agtp224", data, g_Msg); 
          if (lcRetorno.trim().compareTo("")!=0)
             {
               data.getUser().removeTemp("Agtp210_g_PrmPC080");
               data.getUser().setTemp("Agtp210_g_PrmPC080", g_PrmPC080);
               BF_MSG.MsgInt("Agtp210", data, g_Msg, lcRetorno, "1"); 
      	       return;
             }
          return;
        }
    if  (Opc.equals("A"))
        {
          //Accion
          String Seq = data.getParameters().getString("Seq", "");
          i = Integer.parseInt(Seq);
          d_MsgGT210 = (MSGGT210.Bff_MsgGT210)g_MsgGT210.GT210_Tab.elementAt(i);
          if (d_MsgGT210.Tmt_Est.toString().compareTo("RGABG")!=0
           && d_MsgGT210.Tmt_Est.toString().compareTo("PDABG")!=0
           && d_MsgGT210.Tmt_Est.toString().compareTo("REABG")!=0)
             {
               data.getUser().removeTemp("Agtp210_g_PrmPC080");
               data.getUser().setTemp("Agtp210_g_PrmPC080", g_PrmPC080);
               BF_MSG.MsgInt("Agtp210", data, g_Msg, "Antes Debe Aceptarlo", "1"); 
      	       return;
             }
          rg.MoverA(g_PrmPC080.PC080_Rtn,     "WR");
          rg.MoverA(g_PrmPC080.PC080_Cli,     d_MsgGT210.Evt_Cli);
          rg.MoverA(g_PrmPC080.PC080_Ncl,     d_MsgGT210.Evt_Ncl);
          rg.MoverA(g_PrmPC080.PC080_Cnr.Sis, d_MsgGT210.Evt_Sis);
          rg.MoverA(g_PrmPC080.PC080_Cnr.Ncn, d_MsgGT210.Evt_Ncn);
          rg.MoverA(g_PrmPC080.PC080_Dcn,     d_MsgGT210.Evt_Dcn);
          rg.MoverA(g_PrmPC080.PC080_Ndc,     d_MsgGT210.Evt_Dsc);
          rg.MoverA(g_PrmPC080.PC080_Ntr,     d_MsgGT210.Tmt_Nev);
          rg.MoverA(g_PrmPC080.PC080_Ttr,     d_MsgGT210.Evt_Ttr);
          rg.MoverA(g_PrmPC080.PC080_Trt,     d_MsgGT210.Tmt_Trt);
          rg.MoverA(g_PrmPC080.PC080_Mnd,     d_MsgGT210.Evt_Mnd);
          rg.MoverA(g_PrmPC080.PC080_Mnt,     d_MsgGT210.Evt_Vle);
          rg.MoverA(g_PrmPC080.PC080_Trj,     d_MsgGT210.Evt_Trj);
          rg.MoverA(g_PrmPC080.PC080_Dcm,     d_MsgGT210.Evt_Dcm);
          rg.MoverA(g_PrmPC080.PC080_Ggr,     " ");
          rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
          BF_MSG.Link_Program("Agtp210", rg.NormaPgm(d_MsgGT210.Tmt_Pgm), data, g_Msg); 
          if (lcRetorno.trim().compareTo("")!=0)
             {
               data.getUser().removeTemp("Agtp210_g_PrmPC080");
               data.getUser().setTemp("Agtp210_g_PrmPC080", g_PrmPC080);
               BF_MSG.MsgInt("Agtp210", data, g_Msg, lcRetorno, "1"); 
      	       return;
             }
       	  return;
        }
    if  (Opc.equals("D"))
        {
          //Devolver
          String Seq = data.getParameters().getString("Seq", "");
          i = Integer.parseInt(Seq);
          d_MsgGT210 = (MSGGT210.Bff_MsgGT210)g_MsgGT210.GT210_Tab.elementAt(i);
          if (d_MsgGT210.Tmt_Est.toString().compareTo("ENABG")!=0)
             {
               data.getUser().removeTemp("Agtp210_g_PrmPC080");
               data.getUser().setTemp("Agtp210_g_PrmPC080", g_PrmPC080);
               BF_MSG.MsgInt("Agtp210", data, g_Msg, "Ya No Puede Devolver; Deber Terminarlo", "1"); 
      	       return;
             }
          rg.MoverA(g_PrmPC080.PC080_Ntr, d_MsgGT210.Tmt_Nev);
          rg.MoverA(g_PrmPC080.PC080_Trt, d_MsgGT210.Tmt_Trt);
          g_PrmED210 = PRMED210.Inicia_PrmED210();
          rg.MoverA(g_PrmED210.ED210_Rtn, "RW");
          data.getUser().removeTemp("Agtp210_g_PrmPC080");
          data.getUser().setTemp("Agtp210_g_PrmPC080", g_PrmPC080);
          rg.MoverA(g_Msg.Msg_Dat, PRMED210.LSet_De_PrmED210(g_PrmED210));
          BF_MSG.Link_Program("Agtp210", "Aedp210", data, g_Msg); 
          if (lcRetorno.trim().compareTo("")!=0)
             {
               data.getUser().removeTemp("Agtp210_g_PrmPC080");
               data.getUser().setTemp("Agtp210_g_PrmPC080", g_PrmPC080);
               BF_MSG.MsgInt("Agtp210", data, g_Msg, lcRetorno, "1"); 
      	       return;
             }
       	  return;
        }
    if  (Opc.equals("N"))
        {	
          //Siguiente
          Poblar_Panel(data, context, "S");
          setTemplate(data, "Garantias,Agt,AGTP210.vm" );
          return;
        }
    if  (Opc.equals("P"))
        {	
          //Previo
          Poblar_Panel(data, context, "P");
          setTemplate(data, "Garantias,Agt,AGTP210.vm" );
          return;
        }		
  }
  //---------------------------------------------------------------------------------------
  public void Poblar_Panel(RunData data, Context context, String pcIdr)
              throws Exception
  {
    MSGGT210.Bff_MsgGT210 d_MsgGT210 = new MSGGT210.Bff_MsgGT210();
    if (pcIdr.equals("E"))
       {
         g_MsgGT210 = (MSGGT210.Buf_MsgGT210)data.getUser().getTemp("Agtp210_MsgGT210");
         i = 0;
       }
    if (pcIdr.equals("S"))
       { i = Integer.parseInt(g_MsgGT210.GT210_Nro.toString())-1; }
    if (pcIdr.equals("P"))
       { i = 0; }
    if (pcIdr.compareTo("I")!=0)
       {
         String lcAbg = g_MsgGT210.GT210_Abg.toString();
         String lcNag = g_MsgGT210.GT210_Nag.toString();
         d_MsgGT210 = (MSGGT210.Bff_MsgGT210)g_MsgGT210.GT210_Tab.elementAt(i);
         g_MsgGT210 = MSGGT210.Inicia_MsgGT210();
         rg.MoverA(g_MsgGT210.GT210_Idr, pcIdr);
         rg.MoverA(g_MsgGT210.GT210_Abg, lcAbg);
         rg.MoverA(g_MsgGT210.GT210_Nag, lcNag);
         g_MsgGT210.GT210_Tab.set(0, d_MsgGT210);
       }
    rg.MoverA(g_Msg.Msg_Dat, MSGGT210.LSet_De_MsgGT210(g_MsgGT210));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GT210");
    g_MsgGT210 = MSGGT210.LSet_A_MsgGT210(g_Msg.Msg_Dat.toString());
    data.getUser().removeTemp("Agtp210_MsgGT210");
    data.getUser().setTemp("Agtp210_MsgGT210", g_MsgGT210);
    data.getUser().removeTemp("Agtp210_Msg");
    data.getUser().setTemp("Agtp210_Msg", g_Msg);
  }
//---------------------------------------------------------------------------------------
  public void doAgtp210_Continue(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP210[doAgtp210_Continue.start]", "[" + data.getUser().getUserName() + "]");
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Appp030"))
       {
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp210_g_PrmPC080");
         data.getUser().removeTemp("Agtp210_g_PrmPC080");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP210.vm" );
         return;
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Aedp210"))
       {
         g_PrmED210 = PRMED210.LSet_A_PrmED210(g_Msg.Msg_Dat.toString());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp210_g_PrmPC080");
         data.getUser().removeTemp("Agtp210_g_PrmPC080");
         if (g_PrmED210.ED210_Rtn.toString().equals("OK"))
            {
              g_MsgED135 = MSGED135.Inicia_MsgED135();
              rg.MoverA(g_MsgED135.ED135_Idr, "X");
              rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
              rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
              rg.MoverA(g_MsgED135.ED135_Est, "");
              rg.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
              rg.MoverA(g_MsgED135.ED135_Ldv, g_PrmED210.ED210_Ldt);
              rg.MoverA(g_MsgED135.ED135_Ezt, "DVABG");
              rg.MoverA(g_MsgED135.ED135_Exi, "ABG");
              rg.MoverA(g_MsgED135.ED135_Exn, rg.Zeros(g_MsgED135.ED135_Exn));
              rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
              g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
              Poblar_Panel(data, context, "E");
            }
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP210.vm" );
         return;
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Agtp224"))
       {
         g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
         if (g_PrmPC080.PC080_Rtn.toString().equals("OK"))
            {
              g_MsgED135 = MSGED135.Inicia_MsgED135();
              rg.MoverA(g_MsgED135.ED135_Idr, "M");
              rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
              rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
              rg.MoverA(g_MsgED135.ED135_Est, "");
              rg.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
              rg.MoverA(g_MsgED135.ED135_Ezt, "RGABG");
              rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
              g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
              Poblar_Panel(data, context, "E");
            }
         setTemplate(data, "Garantias,Agt,AGTP210.vm" );
         return;
       }
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    if (g_PrmPC080.PC080_Rtn.toString().equals("OK"))
       {
         Poblar_Panel(data, context, "E");
       }
    setTemplate(data, "Garantias,Agt,AGTP210.vm" );
    return;
  }
}