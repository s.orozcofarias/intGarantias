// Source File Name:   Agtp410.java
// Descripcion     :   Informe Fiscalia Alzamiento (ED135)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.BF_IMG;
import com.FHTServlet.modules.global.PRMPC080;

import com.intGlobal.modules.global.MSGED135;
import com.intGlobal.modules.global.PRMTG123;
import com.intGlobal.modules.global.PRMTG134;
import com.intGlobal.modules.global.PRMED210;

import com.intGarantias.modules.global.PRMGT161;
import com.intGarantias.modules.global.GT_IMG;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp410 extends FHTServletAction
{
  //---------------------------------------------------------------------------------------
  public int i = 0;
  public String lcData = "";
  public Vector vDatos;
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  BF_IMG img                       = new BF_IMG();
  BF_IMG.Buf_Img g_Img             = new BF_IMG.Buf_Img();
  BF_IMG.Buf_Img_Bse g_Img_Base    = new BF_IMG.Buf_Img_Bse();
  GT_IMG gtimg                     = new GT_IMG();
  GT_IMG.Buf_Img_GtApe g_Img_GtApe = new GT_IMG.Buf_Img_GtApe();
  GT_IMG.Buf_Img_GtEsl g_Img_GtEsl = new GT_IMG.Buf_Img_GtEsl();
  GT_IMG.Buf_Img_GtUsu g_Img_GtUsu = new GT_IMG.Buf_Img_GtUsu();
  GT_IMG.Buf_Img_GtPrp g_Img_GtPrp = new GT_IMG.Buf_Img_GtPrp();
  GT_IMG.Buf_Img_GtIns g_Img_GtIns = new GT_IMG.Buf_Img_GtIns();
  Vector v_Img_GtUsu               = new Vector();
  Vector v_Img_GtPrp               = new Vector();
  Vector v_Img_GtIns               = new Vector();
  PRMPC080.PrmPC080 g_PrmPC080     = new PRMPC080.PrmPC080();
  MSGED135.Buf_MsgED135 g_MsgED135 = new MSGED135.Buf_MsgED135();
  PRMTG123.Buf_PrmTG123 g_PrmTG123 = new PRMTG123.Buf_PrmTG123();
  PRMTG134.Buf_PrmTG134 g_PrmTG134 = new PRMTG134.Buf_PrmTG134();
  PRMED210.Buf_PrmED210 g_PrmED210 = new PRMED210.Buf_PrmED210();
  PRMGT161.Buf_PrmGT161 g_PrmGT161 = new PRMGT161.Buf_PrmGT161();
  //---------------------------------------------------------------------------------------
  public Agtp410()
  {
  }
  //---------------------------------------------------------------------------------------
  public void doPerform(RunData data, Context context)
              throws Exception
  {
  //Log.debug("AGTP410[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp410-DIN", data);
    if (g_Msg.Msg_Pgr.toString().equals("RT"))
       { doAgtp410_Continue(data, context); }
    else
       { doAgtp410_Init(data, context); }
  }
  //---------------------------------------------------------------------------------------
  public void doCancel(RunData data, Context context)
              throws Exception
  {
  //Log.debug("AGTP410[doCancel.start]", "[CANCELADO]");
    setTemplate(data, "Garantias,Agt,AGTP410.vm");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp410_Init(RunData data, Context context)
              throws Exception
  {
  //Log.debug("AGTP410[doAgtp410_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    data.getUser().removeTemp("Agtp410_PrmPC080");
    data.getUser().setTemp("Agtp410_PrmPC080", g_PrmPC080);
    Carga_de_Host(data, context);
    Carga_Inicial(data, context);
    if (g_PrmPC080.PC080_Fte.toString().trim().compareTo("FIS")!=0)
       { rg.MoverA(g_PrmPC080.PC080_Rtn, "RO"); }
    else
       { rg.MoverA(g_PrmPC080.PC080_Rtn, "WR"); }
    rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
    BF_MSG.Param_Program(data, g_Msg);
    setTemplate(data, "Garantias,Agt,AGTP410.vm" );
  //Log.debug("AGTP410[doAgtp410_Init.end]", "[" + data.getUser().getUserName() + "]");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp410(RunData data, Context context)
              throws Exception
  {
  //Log.debug("AGTP410[doAgtp410.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp410-MAN", data);
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    String Opc = data.getParameters().getString("Opc", "");
    if (Opc.trim().equals("D"))
       {
       //Boton Devolver (Pedir Nuevos Documentos)
         Actualiza_Datos(data, context);
         data.getUser().removeTemp("Agtp410_g_PrmPC080");
         data.getUser().setTemp("Agtp410_g_PrmPC080" ,g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Link_Program("Agtp410", "Aedp127", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("R"))
       {
       //Boton Errores
         g_MsgED135 = (MSGED135.Buf_MsgED135)data.getUser().getTemp("Agtp410_MsgED135");
         g_PrmED210 = PRMED210.Inicia_PrmED210();
         rg.MoverA(g_PrmED210.ED210_Rtn, "RO");
         rg.MoverA(g_PrmED210.ED210_Ldt, g_MsgED135.ED135_Ldv);
         data.getUser().removeTemp("Agtp410_g_PrmPC080");
         data.getUser().setTemp("Agtp410_g_PrmPC080", g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMED210.LSet_De_PrmED210(g_PrmED210));
         BF_MSG.Link_Program("Agtp410", "Aedp210", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("V"))
       {
       //Boton Envio
         Actualiza_Datos(data, context);
         rg.MoverA(g_PrmPC080.PC080_Rtn, "RO");
         data.getUser().removeTemp("Agtp410_g_PrmPC080");
         data.getUser().setTemp("Agtp410_g_PrmPC080" ,g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Link_Program("Agtp410", "Agtp320", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("T"))
       {
       //Boton Detalle
         String lcPrograma = "";
         Actualiza_Datos(data, context);
         rg.MoverA(g_PrmPC080.PC080_Rtn, "RO");
         data.getUser().removeTemp("Agtp410_g_PrmPC080");
         data.getUser().setTemp("Agtp410_g_PrmPC080" ,g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Link_Program("Agtp410", "Agtp170", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("P"))
       {
       //Boton Pendiente
         Actualiza_Datos(data, context);
         g_PrmED210 = PRMED210.Inicia_PrmED210();
         rg.MoverA(g_PrmED210.ED210_Rtn, "RW");
         data.getUser().removeTemp("Agtp410_g_PrmPC080");
         data.getUser().setTemp("Agtp410_g_PrmPC080" ,g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMED210.LSet_De_PrmED210(g_PrmED210));
         BF_MSG.Link_Program("Agtp410", "Aedp210", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("S"))
       {
       //Boton Salir
         data.getUser().removeTemp("Agtp410_PrmPC080");
         data.getUser().removeTemp("Agtp410_ImgBase");
         data.getUser().removeTemp("Agtp410_ImgGtApe");
         data.getUser().removeTemp("Agtp410_ImgGtEsl");
         data.getUser().removeTemp("Agtp410_vImgGtUsu");
         data.getUser().removeTemp("Agtp410_vImgGtPrp");
         data.getUser().removeTemp("Agtp410_vImgGtIns");
         data.getUser().removeTemp("Agtp410_vTabImg");
         data.getUser().removeTemp("Agtp410_MsgED135");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "NK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp410", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("A"))
       {
       //Boton Aceptar
         Actualiza_Datos(data, context);
         Cursar(data, context);
         data.getUser().removeTemp("Agtp410_PrmPC080");
         data.getUser().removeTemp("Agtp410_ImgBase");
         data.getUser().removeTemp("Agtp410_ImgGtApe");
         data.getUser().removeTemp("Agtp410_ImgGtEsl");
         data.getUser().removeTemp("Agtp410_vImgGtUsu");
         data.getUser().removeTemp("Agtp410_vImgGtPrp");
         data.getUser().removeTemp("Agtp410_vImgGtIns");
         data.getUser().removeTemp("Agtp410_vTabImg");
         data.getUser().removeTemp("Agtp410_MsgED135");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp410", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("1"))
       {
       //Consulta Abogados Disponibles
         Actualiza_Datos(data, context);
         g_PrmTG134 = PRMTG134.Inicia_PrmTG134();
         data.getUser().removeTemp("Agtp410_g_PrmPC080");
         data.getUser().setTemp("Agtp410_g_PrmPC080" ,g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMTG134.LSet_De_PrmTG134(g_PrmTG134));
         BF_MSG.Link_Program("Agtp410", "Atgp134", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("2"))
       {
       //Consulta Comunas Disponibles
         Actualiza_Datos(data, context);
         g_PrmTG123 = PRMTG123.Inicia_PrmTG123();
         data.getUser().removeTemp("Agtp410_g_PrmPC080");
         data.getUser().setTemp("Agtp410_g_PrmPC080" ,g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMTG123.LSet_De_PrmTG123(g_PrmTG123));
         BF_MSG.Link_Program("Agtp410", "Atgp124", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("5"))
       {
       //Consulta Inscripciones Disponibles
         Actualiza_Datos(data, context);
         v_Img_GtIns = (Vector)data.getUser().getTemp("Agtp410_vImgGtIns");
         int liIns = v_Img_GtIns.size();
         String lcIns = Integer.toString(liIns);
         g_PrmGT161 = PRMGT161.Inicia_PrmGT161();
         rg.MoverA(g_PrmGT161.GT161_Idr, "Z");
         rg.MoverA(g_PrmGT161.GT161_Nrv, rg.FmtValor(lcIns,0,0,3,"+"));
         for (i=0; i<v_Img_GtIns.size(); i++)
             {
               PRMGT161.Bff_PrmGT161 d_PrmGT161 = new PRMGT161.Bff_PrmGT161();
               g_Img_GtIns = (GT_IMG.Buf_Img_GtIns)v_Img_GtIns.elementAt(i);
               rg.MoverA(d_PrmGT161.GT161_Cti, g_Img_GtIns.GtIns_Cti);
               rg.MoverA(d_PrmGT161.GT161_Nti, g_Img_GtIns.GtIns_Nti);
               rg.MoverA(d_PrmGT161.GT161_Csv, g_Img_GtIns.GtIns_Csv);
               rg.MoverA(d_PrmGT161.GT161_Lcl, g_Img_GtIns.GtIns_Lcl);
               rg.MoverA(d_PrmGT161.GT161_Rpo, g_Img_GtIns.GtIns_Rpo);
               rg.MoverA(d_PrmGT161.GT161_Foj, g_Img_GtIns.GtIns_Foj);
               rg.MoverA(d_PrmGT161.GT161_Nin, g_Img_GtIns.GtIns_Nin);
               rg.MoverA(d_PrmGT161.GT161_Grd, g_Img_GtIns.GtIns_Grd);
               g_PrmGT161.GT161_Tap.set(i, d_PrmGT161); 
             }
         data.getUser().removeTemp("Agtp410_g_PrmPC080");
         data.getUser().setTemp("Agtp410_g_PrmPC080" ,g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMGT161.LSet_De_PrmGT161(g_PrmGT161));
         BF_MSG.Link_Program("Agtp410", "Agtp161", data, g_Msg);
         return;
       }
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp410_Continue(RunData data, Context context)
              throws Exception
  {
  //Log.debug("AGTP410[doAgtp410_Continue.start]", "[" + data.getUser().getUserName() + "]");
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Agtp170")
     || rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Agtp320"))
       {
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp410_g_PrmPC080");
         data.getUser().removeTemp("Agtp410_g_PrmPC080");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP410.vm" );
         return;
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Aedp127"))
       {
         PRMPC080.PrmPC080 l_PrmPC080 = new PRMPC080.PrmPC080();
         l_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp410_g_PrmPC080");
         data.getUser().removeTemp("Agtp410_g_PrmPC080");
         if (l_PrmPC080.PC080_Rtn.toString().equals("NK"))
            { 
              rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
              BF_MSG.Param_Program(data, g_Msg);
              setTemplate(data, "Garantias,Agt,AGTP410.vm" );
              return;
            }
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp410_ImgGtApe");
         g_Img_GtEsl = (GT_IMG.Buf_Img_GtEsl)data.getUser().getTemp("Agtp410_ImgGtEsl");
         Vector g_Tab_Img = (Vector)data.getUser().getTemp("Agtp410_vTabImg");
         g_Tab_Img = A_Tab_Img(data, g_Tab_Img);
         img.Graba_Img_Host(g_Msg, g_Tab_Img, data, context);
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp410", data, g_Msg);
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Atgp124"))
       {
         g_PrmTG123 = PRMTG123.LSet_A_PrmTG123(g_Msg.Msg_Dat.toString());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp410_g_PrmPC080");
         data.getUser().removeTemp("Agtp410_g_PrmPC080");
         if (g_PrmTG123.TG123_Prv.toString().trim().compareTo("")!=0)
             {
               g_Img_GtEsl = (GT_IMG.Buf_Img_GtEsl)data.getUser().getTemp("Agtp410_ImgGtEsl");
               rg.MoverA(g_Img_GtEsl.GtEsl_Pcm, g_PrmTG123.TG123_Prv.toString() + g_PrmTG123.TG123_Cmn.toString());
               rg.MoverA(g_Img_GtEsl.GtEsl_Ncm, g_PrmTG123.TG123_Ncm);
               rg.MoverA(g_Img_GtEsl.GtEsl_Npv, g_PrmTG123.TG123_Npr);
               data.getUser().removeTemp("Agtp410_ImgGtEsl");
               data.getUser().setTemp("Agtp410_ImgGtEsl", g_Img_GtEsl);
             }
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP410.vm" );
         return;
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Atgp134"))
       {
         g_PrmTG134 = PRMTG134.LSet_A_PrmTG134(g_Msg.Msg_Dat.toString());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp410_g_PrmPC080");
         data.getUser().removeTemp("Agtp410_g_PrmPC080");
         if (g_PrmTG134.TG134_Abg.toString().trim().compareTo("")!=0)
            {
              g_Img_GtEsl = (GT_IMG.Buf_Img_GtEsl)data.getUser().getTemp("Agtp410_ImgGtEsl");
              rg.MoverA(g_Img_GtEsl.GtEsl_Abg, g_PrmTG134.TG134_Abg);
              rg.MoverA(g_Img_GtEsl.GtEsl_Nag, g_PrmTG134.TG134_Nag);
              data.getUser().removeTemp("Agtp410_ImgGtEsl");
              data.getUser().setTemp("Agtp410_ImgGtEsl", g_Img_GtEsl);
            }
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP410.vm" );
         return;
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Aedp210"))
       {
         g_PrmED210 = PRMED210.LSet_A_PrmED210(g_Msg.Msg_Dat.toString());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp410_g_PrmPC080");
         data.getUser().removeTemp("Agtp410_g_PrmPC080");
         if (g_PrmED210.ED210_Rtn.toString().equals("OK"))
            { 
              g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp410_ImgGtApe");
              g_Img_GtEsl = (GT_IMG.Buf_Img_GtEsl)data.getUser().getTemp("Agtp410_ImgGtEsl");
              CursaPendte(data, context);
              rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
              rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
              BF_MSG.Return_Data("Agtp410", data, g_Msg);
            }
         else
            {
              rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
              BF_MSG.Param_Program(data, g_Msg);
              setTemplate(data, "Garantias,Agt,AGTP410.vm" );
            }
         return;
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Agtp161"))
       {
         g_PrmGT161 = PRMGT161.LSet_A_PrmGT161(g_Msg.Msg_Dat.toString());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp410_g_PrmPC080");
         data.getUser().removeTemp("Agtp410_g_PrmPC080");
         if (g_PrmGT161.GT161_Idr.toString().equals("A"))
            {
              PRMGT161.Bff_PrmGT161 d_PrmGT161 = new PRMGT161.Bff_PrmGT161();
              v_Img_GtIns.clear();
              for (i=0; i<g_PrmGT161.GT161_Tap.size(); i++)
                  {
                    d_PrmGT161 = (PRMGT161.Bff_PrmGT161)g_PrmGT161.GT161_Tap.elementAt(i);
                    if (d_PrmGT161.GT161_Cti.toString().trim().equals("")) break;
                    GT_IMG.Buf_Img_GtIns l_Img_GtIns = new GT_IMG.Buf_Img_GtIns();
                    rg.MoverA(l_Img_GtIns.GtIns_Cti, d_PrmGT161.GT161_Cti);
                    rg.MoverA(l_Img_GtIns.GtIns_Nti, d_PrmGT161.GT161_Nti);
                    rg.MoverA(l_Img_GtIns.GtIns_Csv, d_PrmGT161.GT161_Csv);
                    rg.MoverA(l_Img_GtIns.GtIns_Lcl, d_PrmGT161.GT161_Lcl);
                    rg.MoverA(l_Img_GtIns.GtIns_Rpo, d_PrmGT161.GT161_Rpo);
                    rg.MoverA(l_Img_GtIns.GtIns_Foj, d_PrmGT161.GT161_Foj);
                    rg.MoverA(l_Img_GtIns.GtIns_Nin, d_PrmGT161.GT161_Nin);
                    rg.MoverA(l_Img_GtIns.GtIns_Grd, d_PrmGT161.GT161_Grd);
                    rg.MoverA(l_Img_GtIns.GtIns_Fll, " ");
                    v_Img_GtIns.add (l_Img_GtIns); 
                  }
              data.getUser().removeTemp("Agtp410_vImgGtIns");
              data.getUser().setTemp("Agtp410_vImgGtIns", v_Img_GtIns);
            }
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP410.vm" );
         return;
       }
  }
  //---------------------------------------------------------------------------------------
  public void Carga_de_Host(RunData data, Context context)
              throws Exception 
  {
    Vector g_Tab_Img = img.Carga_Img_Host(g_Msg, g_PrmPC080.PC080_Ntr.toString(), data, context);
    De_Tab_Img(g_Tab_Img, data);
    data.getUser().removeTemp("Agtp410_ImgBase");
    data.getUser().setTemp("Agtp410_ImgBase", g_Img_Base);
    data.getUser().removeTemp("Agtp410_ImgGtApe");
    data.getUser().setTemp("Agtp410_ImgGtApe", g_Img_GtApe);
    data.getUser().removeTemp("Agtp410_ImgGtEsl");
    data.getUser().setTemp("Agtp410_ImgGtEsl", g_Img_GtEsl);
    data.getUser().removeTemp("Agtp410_vImgGtUsu");
    data.getUser().setTemp("Agtp410_vImgGtUsu", v_Img_GtUsu);
    data.getUser().removeTemp("Agtp410_vImgGtPrp");
    data.getUser().setTemp("Agtp410_vImgGtPrp", v_Img_GtPrp);
    data.getUser().removeTemp("Agtp410_vImgGtIns");
    data.getUser().setTemp("Agtp410_vImgGtIns", v_Img_GtIns);
    data.getUser().removeTemp("Agtp410_vTabImg");
    data.getUser().setTemp("Agtp410_vTabImg", g_Tab_Img);
  }
  //---------------------------------------------------------------------------------------
  public void De_Tab_Img(Vector g_Tab_Img, RunData data)
  {
    g_Img       = img.Inicia_Img();
    lcData      = img.LSet_De_Img(g_Img);
    g_Img_Base  = img.LSet_A_ImgBase(lcData);
    g_Img_GtApe = gtimg.LSet_A_ImgGtApe(g_Img.Img_Dat.toString());
    g_Img_GtEsl = gtimg.LSet_A_ImgGtEsl(g_Img.Img_Dat.toString());
    v_Img_GtUsu.clear();
    v_Img_GtPrp.clear();
    v_Img_GtIns.clear();
    for (i=0; i<g_Tab_Img.size(); i++)
        {
          lcData = img.LSet_De_Img((BF_IMG.Buf_Img)g_Tab_Img.elementAt(i));
          g_Img  = img.LSet_A_Img(lcData);
          if (g_Img.Img_Dax.toString().trim().equals("BASE"))
             {
               g_Img_GtApe = gtimg.LSet_A_ImgGtApe(g_Img.Img_Dat.toString());
               g_Img_Base  = img.LSet_A_ImgBase(g_Img_GtApe.GtApe_Bse.toString());
             }
          if (g_Img.Img_Dax.toString().trim().equals("YESL"))
             { g_Img_GtEsl = gtimg.LSet_A_ImgGtEsl(g_Img.Img_Dat.toString()); }
          if (g_Img.Img_Dax.toString().trim().equals("YYUSU"))
             {
               g_Img_GtUsu = gtimg.LSet_A_ImgGtUsu(g_Img.Img_Dat.toString());
               v_Img_GtUsu.add (g_Img_GtUsu); 
             }
          if (g_Img.Img_Dax.toString().trim().equals("YYPRP"))
             {
               g_Img_GtPrp = gtimg.LSet_A_ImgGtPrp(g_Img.Img_Dat.toString());
               v_Img_GtPrp.add (g_Img_GtPrp); 
             }
          if (g_Img.Img_Dax.toString().trim().equals("YYINS"))
             {
               g_Img_GtIns = gtimg.LSet_A_ImgGtIns(g_Img.Img_Dat.toString());
               v_Img_GtIns.add (g_Img_GtIns); 
             }
        }
    if (v_Img_GtUsu.size()==0)
       {
         rg.MoverA(g_Img_GtUsu.GtUsu_Cli, g_PrmPC080.PC080_Cli);
         rg.MoverA(g_Img_GtUsu.GtUsu_Ncl, g_PrmPC080.PC080_Ncl);
         rg.MoverA(g_Img_GtUsu.GtUsu_Fll, " ");
         v_Img_GtUsu.add (g_Img_GtUsu); 
       }
    if (v_Img_GtPrp.size()==0)
       {
         rg.MoverA(g_Img_GtPrp.GtPrp_Cli, g_Img_Base.Img_Base_Cli);
         rg.MoverA(g_Img_GtPrp.GtPrp_Ncl, g_Img_Base.Img_Base_Ncl);
         rg.MoverA(g_Img_GtPrp.GtPrp_Fll, " ");
         v_Img_GtPrp.add (g_Img_GtPrp); 
       }
  }
  //---------------------------------------------------------------------------------------
  public void Carga_Inicial(RunData data, Context context)
              throws Exception 
  {
    g_MsgED135 = MSGED135.Inicia_MsgED135();
    rg.MoverA(g_MsgED135.ED135_Idr, "C");
    rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
    rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
  //If llRetorno > 0 Then Call Com_Salir_Click
    g_MsgED135 = MSGED135.LSet_A_MsgED135(g_Msg.Msg_Dat.toString());
    data.getUser().removeTemp("Agtp410_MsgED135");
    data.getUser().setTemp("Agtp410_MsgED135", g_MsgED135);
  }
  //---------------------------------------------------------------------------------------
  public void Cursar(RunData data, Context context)
              throws Exception 
  {
    Vector g_Tab_Img = (Vector)data.getUser().getTemp("Agtp410_vTabImg");
    g_Tab_Img = A_Tab_Img(data, g_Tab_Img);
    img.Graba_Img_Host(g_Msg, g_Tab_Img, data, context);
    g_MsgED135 = MSGED135.Inicia_MsgED135();
    rg.MoverA(g_MsgED135.ED135_Idr, "M");
    rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
    rg.MoverA(g_MsgED135.ED135_Est, "");
    rg.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
    rg.MoverA(g_MsgED135.ED135_Ezt, "TERMI");
    rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
  //If llRetorno > 0 Then Call Com_Salir_Click
  }
  //---------------------------------------------------------------------------------------
  public Vector A_Tab_Img(RunData data, Vector g_Tab_Img)
  {
    int    liSwt = 0;
    Vector v_Img = new Vector();
    v_Img.clear();
    for (i=0; i<g_Tab_Img.size(); i++)
        {
          lcData = img.LSet_De_Img((BF_IMG.Buf_Img)g_Tab_Img.elementAt(i));
          g_Img  = img.LSet_A_Img(lcData);
          if (g_Img.Img_Dax.toString().trim().equals("BASE"))
             { rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtApe(g_Img_GtApe)); }
          if (g_Img.Img_Dax.toString().trim().equals("YESL"))
             { 
               liSwt = 1;
               rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtEsl(g_Img_GtEsl));
             }
          if (g_Img.Img_Dax.toString().trim().equals("YYUSU")
           || g_Img.Img_Dax.toString().trim().equals("YYPRP")
           || g_Img.Img_Dax.toString().trim().equals("YYINS"))
             { }
          else
             {
               Vector vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
               v_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
             }
        }
    if (liSwt == 0)
       {
         rg.MoverA(g_Img.Img_Ntr, g_PrmPC080.PC080_Ntr);
         rg.MoverA(g_Img.Img_Dax, "YESL");
         rg.MoverA(g_Img.Img_Seq, "000");
         rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtEsl(g_Img_GtEsl));
         Vector vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
         v_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
       }
    v_Img_GtUsu = (Vector)data.getUser().getTemp("Agtp410_vImgGtUsu");
    rg.MoverA(g_Img.Img_Seq, "000");
    for (i=0; i<v_Img_GtUsu.size(); i++)
        {
          g_Img_GtUsu = (GT_IMG.Buf_Img_GtUsu)v_Img_GtUsu.elementAt(i);
          rg.MoverA(g_Img.Img_Dax, "YYUSU");
          rg.MoverA(g_Img.Img_Seq, rg.Suma(g_Img.Img_Seq,0,"1",0,3,0));
          rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtUsu(g_Img_GtUsu));
          Vector vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
          v_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
        }
    v_Img_GtPrp = (Vector)data.getUser().getTemp("Agtp410_vImgGtPrp");
    rg.MoverA(g_Img.Img_Seq, "000");
    for (i=0; i<v_Img_GtPrp.size(); i++)
        {
          g_Img_GtPrp = (GT_IMG.Buf_Img_GtPrp)v_Img_GtPrp.elementAt(i);
          rg.MoverA(g_Img.Img_Dax, "YYPRP");
          rg.MoverA(g_Img.Img_Seq, rg.Suma(g_Img.Img_Seq,0,"1",0,3,0));
          rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtPrp(g_Img_GtPrp));
          Vector vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
          v_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
        }
    v_Img_GtIns = (Vector)data.getUser().getTemp("Agtp410_vImgGtIns");
    rg.MoverA(g_Img.Img_Seq, "000");
    for (i=0; i<v_Img_GtIns.size(); i++)
        {
          g_Img_GtIns = (GT_IMG.Buf_Img_GtIns)v_Img_GtIns.elementAt(i);
          rg.MoverA(g_Img.Img_Dax, "YYINS");
          rg.MoverA(g_Img.Img_Seq, rg.Suma(g_Img.Img_Seq,0,"1",0,3,0));
          rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtIns(g_Img_GtIns));
          Vector vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
          v_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
        }
    return v_Img;
  }
  //---------------------------------------------------------------------------------------
  public void Actualiza_Datos(RunData data, Context context)
              throws Exception 
  {
    g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp410_ImgGtApe");
    g_Img_GtEsl = (GT_IMG.Buf_Img_GtEsl)data.getUser().getTemp("Agtp410_ImgGtEsl");
    rg.MoverA(g_Img_GtEsl.GtEsl_Iln, data.getParameters().getString("GtEsl_Iln", ""));
    rg.MoverA(g_Img_GtEsl.GtEsl_Ilf, data.getParameters().getString("GtEsl_Ilf", ""));
    rg.MoverA(g_Img_GtEsl.GtEsl_Abg, data.getParameters().getString("GtEsl_Abg", ""));
    rg.MoverA(g_Img_GtEsl.GtEsl_Nag, data.getParameters().getString("GtEsl_Nag", ""));
    rg.MoverA(g_Img_GtEsl.GtEsl_Efc, data.getParameters().getString("GtEsl_Efc", ""));
    rg.MoverA(g_Img_GtEsl.GtEsl_Ent, data.getParameters().getString("GtEsl_Ent", ""));
    rg.MoverA(g_Img_GtEsl.GtEsl_Pcm, data.getParameters().getString("GtEsl_Pcm", ""));
    rg.MoverA(g_Img_GtEsl.GtEsl_Ncm, data.getParameters().getString("GtEsl_Ncm", ""));
    rg.MoverA(g_Img_GtEsl.GtEsl_Npv, data.getParameters().getString("GtEsl_Npv", ""));
    rg.MoverA(g_Img_GtEsl.GtEsl_Tta, data.getParameters().getString("GtEsl_Tta", ""));
    rg.MoverA(g_Img_GtEsl.GtEsl_Obl, data.getParameters().getString("GtEsl_Obl", ""));
    rg.MoverA(g_Img_GtApe.GtApe_Cbt, data.getParameters().getString("GtApe_Cbt", ""));
    data.getUser().removeTemp("Agtp410_ImgGtEsl");
    data.getUser().setTemp("Agtp410_ImgGtEsl", g_Img_GtEsl);
    data.getUser().removeTemp("Agtp410_ImgGtApe");
    data.getUser().setTemp("Agtp410_ImgGtApe", g_Img_GtApe);
  }
  //---------------------------------------------------------------------------------------
  public void CursaPendte(RunData data, Context context)
              throws Exception 
  {
    Vector g_Tab_Img = (Vector)data.getUser().getTemp("Agtp410_vTabImg");
    g_Tab_Img = A_Tab_Img(data, g_Tab_Img);
    img.Graba_Img_Host(g_Msg, g_Tab_Img, data, context);
    g_MsgED135 = MSGED135.Inicia_MsgED135();
    rg.MoverA(g_MsgED135.ED135_Idr, "X");
    rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
    rg.MoverA(g_MsgED135.ED135_Est, "");
    rg.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
    rg.MoverA(g_MsgED135.ED135_Ldv, g_PrmED210.ED210_Ldt);
    rg.MoverA(g_MsgED135.ED135_Ezt, "PDFIS");
    rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
  //If llRetorno > 0 Then Call Com_Salir_Click
  }
  //---------------------------------------------------------------------------------------
}