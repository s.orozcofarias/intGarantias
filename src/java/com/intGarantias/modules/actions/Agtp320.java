// Source File Name:   Agtp320.java
// Descripcion:        Consulta Envio Fiscalia (ED125, ED135)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.BF_IMG;
import com.FHTServlet.modules.global.PRMPC080;

import com.intGlobal.modules.global.MSGED125;
import com.intGlobal.modules.global.MSGED135;

import com.intGarantias.modules.global.GT_IMG;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp320 extends FHTServletAction
{
  //---------------------------------------------------------------------------------------
  public int i = 0;
  public String lcData = "";
  public Vector vDatos;
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  BF_IMG img                       = new BF_IMG();
  BF_IMG.Buf_Img g_Img             = new BF_IMG.Buf_Img();
  BF_IMG.Buf_Img_Bse g_Img_Base    = new BF_IMG.Buf_Img_Bse();
  GT_IMG gtimg                     = new GT_IMG();
  GT_IMG.Buf_Img_GtApe g_Img_GtApe = new GT_IMG.Buf_Img_GtApe();
  GT_IMG.Buf_Img_GtEsl g_Img_GtEsl = new GT_IMG.Buf_Img_GtEsl();
  PRMPC080.PrmPC080 g_PrmPC080     = new PRMPC080.PrmPC080();
  MSGED125.Buf_MsgED125 g_MsgED125 = new MSGED125.Buf_MsgED125();
  MSGED135.Buf_MsgED135 g_MsgED135 = new MSGED135.Buf_MsgED135();
  //---------------------------------------------------------------------------------------
  public Agtp320()
  {
  }
  //---------------------------------------------------------------------------------------
  public void doPerform(RunData data, Context context)
              throws Exception
  {
  //Log.debug("AGTP320[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp320-DIN", data);
    doAgtp320_Init(data, context);
  }
  //---------------------------------------------------------------------------------------
  public void doCancel(RunData data, Context context)
              throws Exception
  {
  //Log.debug("AGTP320[doCancel.start]", "[CANCELADO]");
    setTemplate(data, "Garantias,Agt,AGTP320.vm");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp320_Init(RunData data, Context context)
              throws Exception
  {
  //Log.debug("AGTP320[doAgtp320_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    data.getUser().removeTemp("Agtp320_PrmPC080");
    data.getUser().setTemp("Agtp320_PrmPC080", g_PrmPC080);
    Carga_de_Host(data, context);
    Carga_Inicial(data, context);
    rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
    BF_MSG.Param_Program(data, g_Msg);
    setTemplate(data, "Garantias,Agt,AGTP320.vm" );
  //Log.debug("AGTP320[doAgtp320_Init.end]", "[" + data.getUser().getUserName() + "]");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp320(RunData data, Context context)
              throws Exception
  {
  //Log.debug("AGTP320[doAgtp320.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp320-MAN", data);
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    String Opc = data.getParameters().getString("Opc", "");
    if (Opc.trim().equals("S"))
       {
       //Boton Salir
         data.getUser().removeTemp("Agtp320_PrmPC080");
         data.getUser().removeTemp("Agtp320_ImgGtApe");
         data.getUser().removeTemp("Agtp320_ImgGtEsl");
         data.getUser().removeTemp("Agtp320_vTabImg");
         data.getUser().removeTemp("Agtp320_MsgED125");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "NK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp320", data, g_Msg);
         return;
       }
  }
  //---------------------------------------------------------------------------------------
  public void Carga_de_Host(RunData data, Context context)
              throws Exception 
  {
    Vector g_Tab_Img = img.Carga_Img_Host(g_Msg, g_PrmPC080.PC080_Ntr.toString(), data, context);
    De_Tab_Img(g_Tab_Img, data);
    data.getUser().removeTemp("Agtp320_ImgGtApe");
    data.getUser().setTemp("Agtp320_ImgGtApe", g_Img_GtApe);
    data.getUser().removeTemp("Agtp320_ImgGtEsl");
    data.getUser().setTemp("Agtp320_ImgGtEsl", g_Img_GtEsl);
    data.getUser().removeTemp("Agtp320_vTabImg");
    data.getUser().setTemp("Agtp320_vTabImg", g_Tab_Img);
  }
  //---------------------------------------------------------------------------------------
  public void De_Tab_Img(Vector g_Tab_Img, RunData data)
  {
    g_Img       = img.Inicia_Img();
    lcData      = img.LSet_De_Img(g_Img);
    g_Img_Base  = img.LSet_A_ImgBase(lcData);
    g_Img_GtApe = gtimg.LSet_A_ImgGtApe(g_Img.Img_Dat.toString());
    g_Img_GtEsl = gtimg.LSet_A_ImgGtEsl(g_Img.Img_Dat.toString());
    for (i=0; i<g_Tab_Img.size(); i++)
        {
          lcData = img.LSet_De_Img((BF_IMG.Buf_Img)g_Tab_Img.elementAt(i));
          g_Img  = img.LSet_A_Img(lcData);
          if (g_Img.Img_Dax.toString().trim().equals("BASE"))
             {
               g_Img_GtApe = gtimg.LSet_A_ImgGtApe(g_Img.Img_Dat.toString());
               g_Img_Base  = img.LSet_A_ImgBase(g_Img_GtApe.GtApe_Bse.toString());
             }
          if (g_Img.Img_Dax.toString().trim().equals("YESL"))
             { g_Img_GtEsl = gtimg.LSet_A_ImgGtEsl(g_Img.Img_Dat.toString()); }
        }
  }
  //---------------------------------------------------------------------------------------
  public void Carga_Inicial(RunData data, Context context)
              throws Exception 
  {
    g_MsgED125 = MSGED125.Inicia_MsgED125();
    rg.MoverA(g_MsgED125.ED125_Idr, "I");
    rg.MoverA(g_MsgED125.ED125_Nev, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_MsgED125.ED125_Trt, g_PrmPC080.PC080_Trt);
    rg.MoverA(g_Msg.Msg_Dat, MSGED125.LSet_De_MsgED125(g_MsgED125));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED125");
    g_MsgED125 = MSGED125.LSet_A_MsgED125(g_Msg.Msg_Dat.toString());
    data.getUser().removeTemp("Agtp320_MsgED125");
    data.getUser().setTemp("Agtp320_MsgED125", g_MsgED125);
  }
  //---------------------------------------------------------------------------------------
}