// Source File Name:   Agtp714.java
// Descripcion     :   Ingreso Inspeccion (ED090, ED135, GTCUR)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.MSGTXCUR;
import com.FHTServlet.modules.global.BF_TRN;
import com.FHTServlet.modules.global.BF_IMG;
import com.FHTServlet.modules.global.PRMPC080;
import com.FHTServlet.modules.global.PRMPP030;

import com.intGlobal.modules.global.MSGED090;
import com.intGlobal.modules.global.MSGED135;

import com.intGarantias.modules.global.GT_IMG;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp714 extends FHTServletAction
{
  //---------------------------------------------------------------------------------------
  public int    i = 0;
  public int    j = 0;
  public int    p = 0;
  public int    q = 0;
  public String lcData = "";
  public Vector vDatos;
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  MSGTXCUR.Buf_TxCur g_MsgTxCur    = new MSGTXCUR.Buf_TxCur();
  MSGTXCUR.Buf_Tx900 g_MsgTx900    = new MSGTXCUR.Buf_Tx900();
  BF_TRN trn                       = new BF_TRN();
  BF_TRN.Buf_Trn g_Trn             = new BF_TRN.Buf_Trn();
  PRMPC080.PrmPC080 g_PrmPC080     = new PRMPC080.PrmPC080();
  Vector v_Tab_Img                 = new Vector();
  BF_IMG img                       = new BF_IMG();
  BF_IMG.Buf_Img g_Img             = new BF_IMG.Buf_Img();
  BF_IMG.Buf_Img_Bse g_Img_Base    = new BF_IMG.Buf_Img_Bse();
  GT_IMG gtimg                     = new GT_IMG();
  GT_IMG.Buf_Img_GtApe g_Img_GtApe = new GT_IMG.Buf_Img_GtApe();
  GT_IMG.Buf_Img_GtTsd g_Img_GtTsd = new GT_IMG.Buf_Img_GtTsd();
  GT_IMG.Buf_Img_GtPts g_Img_GtPts = new GT_IMG.Buf_Img_GtPts();
  GT_IMG.Buf_Img_GtMff g_Img_GtMff = new GT_IMG.Buf_Img_GtMff();
  MSGED090.Buf_MsgED090 g_MsgED090 = new MSGED090.Buf_MsgED090();
  MSGED135.Buf_MsgED135 g_MsgED135 = new MSGED135.Buf_MsgED135();
  PRMPP030.Buf_PrmPP030 g_PrmPP030 = new PRMPP030.Buf_PrmPP030();
  //---------------------------------------------------------------------------------------
  public Agtp714()
  {
  }
  //---------------------------------------------------------------------------------------
  public void doPerform(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP714[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp714-DIN", data);
    if (g_Msg.Msg_Pgr.toString().equals("RT"))
       { doAgtp714_Continue(data, context); }
    else
       { doAgtp714_Init(data, context); }
  }
  //---------------------------------------------------------------------------------------
  public void doCancel(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP714[doCancel.start]", "[CANCELADO]");
    setTemplate(data, "Garantias,Agt,AGTP714.vm");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp714_Init(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP714[doAgtp714_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    Carga_de_Host(data, context);
    Carga_Inicial(data, context);
    if (g_PrmPC080.PC080_Fte.toString().trim().equals("SPV")
     && g_PrmPC080.PC080_Rtn.toString().trim().equals("WR"))
       { rg.MoverA(g_PrmPC080.PC080_Rtn, "RA"); }
    else
       {
         if (g_PrmPC080.PC080_Fte.toString().trim().compareTo("TSD")!=0)
            { rg.MoverA(g_PrmPC080.PC080_Rtn, "RO"); }
       }
    data.getUser().removeTemp("Agtp714_PrmPC080");
    data.getUser().setTemp("Agtp714_PrmPC080", g_PrmPC080);
    rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
    BF_MSG.Param_Program(data, g_Msg);
    setTemplate(data, "Garantias,Agt,AGTP714.vm" );
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp714(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP714[doAgtp714.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp714-MAN", data);
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    String Opc = data.getParameters().getString("Opc" ,"");
    if (Opc.trim().equals("S"))
       {
         //Boton Salir
         Limpieza(data, context);
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp714", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("I"))
       {
         //Boton Imagen
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp714_ImgGtApe");
         g_Img_GtTsd = (GT_IMG.Buf_Img_GtTsd)data.getUser().getTemp("Agtp714_ImgGtTsd");
         Actualiza_Datos(data, context);
         data.getUser().removeTemp("Agtp714_ImgGtApe");
         data.getUser().setTemp("Agtp714_ImgGtApe", g_Img_GtApe);
         data.getUser().removeTemp("Agtp714_ImgGtTsd");
         data.getUser().setTemp("Agtp714_ImgGtTsd", g_Img_GtTsd);
         data.getUser().removeTemp("Agtp714_g_PrmPC080");
         data.getUser().setTemp("Agtp714_g_PrmPC080", g_PrmPC080);
         rg.MoverA(g_PrmPC080.PC080_Ntc, "000");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         //BF_MSG.Link_Program("Agtp714", "Agtp729", data, g_Msg);
         BF_MSG.Link_Program("Agtp714", "Appp096", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("O"))
       {
         //Boton OK (Aceptar Tasador)
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp714_ImgGtApe");
         g_Img_GtTsd = (GT_IMG.Buf_Img_GtTsd)data.getUser().getTemp("Agtp714_ImgGtTsd");
         Actualiza_Datos(data, context);
         Vector g_Tab_Img = A_Tab_Img(data);
         img.Graba_Img_Host(g_Msg, g_Tab_Img, data, context);
         g_MsgED135 = MSGED135.Inicia_MsgED135();
         rg.MoverA(g_MsgED135.ED135_Idr, "M");
         rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
         rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
         rg.MoverA(g_MsgED135.ED135_Est, "");
         rg.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
         rg.MoverA(g_MsgED135.ED135_Vlc, g_Img_GtApe.GtApe_Vts);
         rg.MoverA(g_MsgED135.ED135_Ezt, "TMTSD");
         rg.MoverA(g_MsgED135.ED135_Exi, "TSD");
         rg.MoverA(g_MsgED135.ED135_Exn, rg.Zeros(12));
         rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
         g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
         Limpieza(data, context);
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp714", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("C"))
       {
         //Boton Cursar (Uvidad Visado)
         g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp714_ImgGtApe");
         g_Img_GtTsd = (GT_IMG.Buf_Img_GtTsd)data.getUser().getTemp("Agtp714_ImgGtTsd");
         Vector g_Tab_Img = A_Tab_Img(data);
         img.Graba_Img_Host(g_Msg, g_Tab_Img, data, context);
         if (PagoTasador(g_Tab_Img, data, context) == false )
            {
              data.getUser().removeTemp("Agtp714_g_PrmPC080");
              data.getUser().setTemp("Agtp714_g_PrmPC080", g_PrmPC080);
              BF_MSG.MsgInt("Agtp714", data, g_Msg, "Msg_Rtn", "T"); 
              return;
            }
         g_MsgED135 = MSGED135.Inicia_MsgED135();
         rg.MoverA(g_MsgED135.ED135_Idr, "M");
         rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
         rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
         rg.MoverA(g_MsgED135.ED135_Est, "");
         rg.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
         rg.MoverA(g_MsgED135.ED135_Ezt, "TERMI");
         rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
         g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
         data.getUser().removeTemp("Agtp714_g_PrmPC080");
         data.getUser().setTemp("Agtp714_g_PrmPC080", g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, MSGTXCUR.LSet_De_MsgTxCur(g_MsgTxCur));
         g_MsgTx900 = MSGTXCUR.LSet_A_MsgTx900(g_Msg.Msg_Dat.toString());
         data.getUser().removeTemp("Agtp291_MsgTx900");
         data.getUser().setTemp("Agtp291_MsgTx900", g_MsgTx900);
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Link_Program("Agtp714", "Agtp291", data, g_Msg);
         return;
       }
  }
  //---------------------------------------------------------------------------------------
  public void Actualiza_Datos(RunData data, Context context)
              throws Exception 
  {
    rg.MoverA(g_Img_GtTsd.GtTsd_Fts, data.getParameters().getString("GtTsd_Fts", ""));
    rg.MoverA(g_Img_GtTsd.GtTsd_Nbf, data.getParameters().getString("GtTsd_Nbf", ""));
    rg.MoverA(g_Img_GtTsd.GtTsd_Pcb, data.getParameters().getString("GtTsd_Pcb", ""));
    rg.MoverA(g_Img_GtTsd.GtTsd_Hon, data.getParameters().getString("GtTsd_Hon", ""));
    rg.MoverA(g_Img_GtTsd.GtTsd_Vst, data.getParameters().getString("GtTsd_Vst", ""));
    rg.MoverA(g_Img_GtTsd.GtTsd_Obt, data.getParameters().getString("GtTsd_Obt", ""));
  }
  //---------------------------------------------------------------------------------------
  public void Carga_de_Host(RunData data, Context context)
              throws Exception 
  {
    v_Tab_Img = img.Carga_Img_Host(g_Msg, g_PrmPC080.PC080_Ntr.toString(), data, context);
    De_Tab_Img(v_Tab_Img, data);
    data.getUser().removeTemp("Agtp714_ImgBase");
    data.getUser().setTemp("Agtp714_ImgBase", g_Img_Base);
    data.getUser().removeTemp("Agtp714_ImgGtApe");
    data.getUser().setTemp("Agtp714_ImgGtApe", g_Img_GtApe);
    data.getUser().removeTemp("Agtp714_ImgGtTsd");
    data.getUser().setTemp("Agtp714_ImgGtTsd", g_Img_GtTsd);
    data.getUser().removeTemp("Agtp714_vTabImg");
    data.getUser().setTemp("Agtp714_vTabImg", v_Tab_Img);
  }
  //---------------------------------------------------------------------------------------
  public void De_Tab_Img(Vector v_Tab_Img, RunData data)
  {
    g_Img       = img.Inicia_Img();
    lcData      = img.LSet_De_Img(g_Img);
    g_Img_Base  = img.LSet_A_ImgBase(lcData);
    g_Img_GtApe = gtimg.LSet_A_ImgGtApe(g_Img.Img_Dat.toString());
    g_Img_GtTsd = gtimg.LSet_A_ImgGtTsd(g_Img.Img_Dat.toString());
    for (i=0; i<v_Tab_Img.size(); i++)
        {
          lcData = img.LSet_De_Img((BF_IMG.Buf_Img)v_Tab_Img.elementAt(i));
          g_Img  = img.LSet_A_Img(lcData);
          if (g_Img.Img_Dax.toString().trim().equals("BASE"))
             {
               g_Img_GtApe = gtimg.LSet_A_ImgGtApe(g_Img.Img_Dat.toString());
               g_Img_Base  = img.LSet_A_ImgBase(g_Img_GtApe.GtApe_Bse.toString());
             }
          if (g_Img.Img_Dax.toString().trim().equals("XTSD"))
             { g_Img_GtTsd = gtimg.LSet_A_ImgGtTsd(g_Img.Img_Dat.toString()); }
        }
  }
  //---------------------------------------------------------------------------------------
  public void Carga_Inicial(RunData data, Context context)
              throws Exception 
  {
    g_MsgED135 = MSGED135.Inicia_MsgED135();
    rg.MoverA(g_MsgED135.ED135_Idr, "C");
    rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
    rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
    g_MsgED135 = MSGED135.LSet_A_MsgED135(g_Msg.Msg_Dat.toString());
    data.getUser().removeTemp("Agtp714_MsgED135");
    data.getUser().setTemp("Agtp714_MsgED135", g_MsgED135);
  }
  //---------------------------------------------------------------------------------------
  public Vector A_Tab_Img(RunData data)
  {
    Vector v_TabImg = (Vector)data.getUser().getTemp("Agtp714_vTabImg");
    Vector v_Img = new Vector();
    v_Img.clear();
    rg.MoverA(g_Img.Img_Ntr, g_PrmPC080.PC080_Ntr);
    //--------------------------------------------------------------------------
    rg.MoverA(g_Img.Img_Dax, "BASE");
    rg.MoverA(g_Img.Img_Seq, "000");
    rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtApe(g_Img_GtApe));
    Vector vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
    v_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
    //--------------------------------------------------------------------------
    rg.MoverA(g_Img.Img_Dax, "XTSD");
    rg.MoverA(g_Img.Img_Seq, "000");
    rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtTsd(g_Img_GtTsd));
    vPaso = (Vector)img.LSet_A_vImg(img.LSet_De_Img(g_Img));
    v_Img.add((BF_IMG.Buf_Img)vPaso.elementAt(0));
    //--------------------------------------------------------------------------
    return v_Img;
  }
//---------------------------------------------------------------------------------------
  public void doAgtp714_Continue(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP714[doAgtp714_Continue.start]", "[" + data.getUser().getUserName() + "]");
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Appp030"))
       {
         g_PrmPP030 = PRMPP030.LSet_A_PrmPP030(g_Msg.Msg_Dat.toString());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp714_g_PrmPC080");
         data.getUser().removeTemp("Agtp714_g_PrmPC080");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         if (g_PrmPP030.PP030_Rsp.toString().trim().equals("TERMINAR"))
            {
              Limpieza(data, context);
              BF_MSG.Return_Data("Agtp714", data, g_Msg);
              return;
            }
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP714.vm" );
         return;
       }
    //if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Agtp729"))
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Appp096"))
       {
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp714_g_PrmPC080");
         data.getUser().removeTemp("Agtp714_g_PrmPC080");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP714.vm" );
         return;
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Agtp291"))
       {
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp714_g_PrmPC080");
         data.getUser().removeTemp("Agtp714_g_PrmPC080");
         Limpieza(data, context);
         rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp714", data, g_Msg);
         return;
       }
  }
  //---------------------------------------------------------------------------------------
  public boolean PagoTasador(Vector g_Tab_Img, RunData data, Context context)
              throws Exception 
  {
    g_Img      = img.Inicia_Img();
    rg.MoverA(g_Img.Img_Ntr, Asigna_Folio("TRN-NTR", data, context));
    rg.MoverA(g_Img.Img_Dax, "BASE");
    rg.MoverA(g_Img.Img_Seq, "000");
    //------------------------------------------------------------------------------
    g_Img_Base = img.LSet_A_ImgBase(g_Img_GtApe.GtApe_Bse.toString());
    rg.MoverA(g_Img_Base.Img_Base_Ntr, g_Img.Img_Ntr);
    rg.MoverA(g_Img_Base.Img_Base_Ttr, "PGTSD");
    rg.MoverA(g_Img_Base.Img_Base_Ntt, "PAGO A TASADORES");
    if (g_PrmPC080.PC080_Pai.toString().trim().equals("CL"))
       { rg.MoverA(g_Img_Base.Img_Base_Dcm, "0"); }
    else
       { rg.MoverA(g_Img_Base.Img_Base_Dcm, "2"); }
    //------------------------------------------------------------------------------
    rg.MoverA(g_Trn.Trn_Ntr, g_Img.Img_Ntr);
    rg.MoverA(g_Trn.Trn_Trm, "PCVB");
    rg.MoverA(g_Trn.Trn_Fem, g_Msg.Msg_Fch);
    rg.MoverA(g_Trn.Trn_Hrm, rg.Zeros(6));
    rg.MoverA(g_Trn.Trn_Ttr, g_Img_Base.Img_Base_Ttr);
    rg.MoverA(g_Trn.Trn_Cli, g_Img_Base.Img_Base_Cli);
    rg.MoverA(g_Trn.Trn_Cnr, g_Img_Base.Img_Base_Sis.toString() + g_Img_Base.Img_Base_Ncn.toString());
    rg.MoverA(g_Trn.Trn_Suc, g_Img_Base.Img_Base_Suc);
    rg.MoverA(g_Trn.Trn_Eje, g_Img_Base.Img_Base_Eje);
    rg.MoverA(g_Trn.Trn_Est, "ENPRO");
    rg.MoverA(g_Trn.Trn_Usr, g_Msg.Msg_Cus);
    rg.MoverA(g_Trn.Trn_Cja, rg.Zeros(4));
    rg.MoverA(g_Trn.Trn_Stf, rg.Zeros(9));
    rg.MoverA(g_Trn.Trn_Dcn, g_Img_Base.Img_Base_Dcn);
    rg.MoverA(g_Trn.Trn_Tmn, g_Img_Base.Img_Base_Tmn);
    rg.MoverA(g_Trn.Trn_Trj, g_Img_Base.Img_Base_Trj);
    rg.MoverA(g_Trn.Trn_Dcm, g_Img_Base.Img_Base_Dcm);
    rg.MoverA(g_Trn.Trn_Mnd, g_Img_Base.Img_Base_Mnd);
    rg.MoverA(g_Trn.Trn_Mnt, rg.Zeros(15));
    //------------------------------------------------------------------------------
    g_Img_GtPts = gtimg.LSet_A_ImgGtPts(g_Img.Img_Dat.toString());
    rg.MoverA(g_Img_GtPts.GtPts_Bse, img.LSet_De_ImgBase_Bse(g_Img_Base));
    rg.MoverA(g_Img_GtPts.GtPts_Fec, g_Msg.Msg_Fch);
    rg.MoverA(g_Img_GtPts.GtPts_Cbp, g_Img_GtApe.GtApe_Cbp);
    rg.MoverA(g_Img_GtPts.GtPts_Dbp, g_Img_GtApe.GtApe_Dbp);
    rg.MoverA(g_Img_GtPts.GtPts_Gts, g_Img_GtTsd.GtTsd_Vst);
    rg.MoverA(g_Img_GtPts.GtPts_Hon, g_Img_GtTsd.GtTsd_Hon);
    rg.MoverA(g_Img_GtPts.GtPts_Pcb, g_Img_GtTsd.GtTsd_Pcb);
    rg.MoverA(g_Img_GtPts.GtPts_Cct, g_Img_GtTsd.GtTsd_Cct);
    rg.MoverA(g_Img_GtPts.GtPts_Bof, g_Img_GtTsd.GtTsd_Bof);
    rg.MoverA(g_Img_GtPts.GtPts_Nbf, g_Img_GtTsd.GtTsd_Nbf);
    rg.MoverA(g_Img_GtPts.GtPts_Nts, g_Img_GtTsd.GtTsd_Nts);
    rg.MoverA(g_Img_GtPts.GtPts_Rts, g_Img_GtTsd.GtTsd_Rts);
    rg.MoverA(g_Img_GtPts.GtPts_Dpc, "CASA");
    int p = 0;
    for (i=0; i<g_Tab_Img.size(); i++)
        {
          BF_IMG.Buf_Img l_Img = new BF_IMG.Buf_Img();
          l_Img = (BF_IMG.Buf_Img)g_Tab_Img.elementAt(i);
          if (l_Img.Img_Dax.toString().trim().equals("MFF"))
             { 
               g_Img_GtMff = gtimg.LSet_A_ImgGtMff(l_Img.Img_Dat.toString());
               rg.MoverA(g_Img_GtPts.GtPts_Cbn, g_Img_GtMff.GtMff_Cct);
               rg.MoverA(g_Img_GtPts.GtPts_Dbn, g_Img_GtMff.GtMff_Dct);
               rg.MoverA(g_Img_GtPts.GtPts_Prc, g_Img_GtMff.GtMff_Prc);
               String lcData = g_Img_GtPts.GtPts_Cbn.toString() + g_Img_GtPts.GtPts_Dbn.toString() 
                                                                + g_Img_GtPts.GtPts_Prc.toString();
               g_Img_GtPts.vGtPts_Ctg.set(p, lcData);
               p = p + 1;
             }
        }
    rg.MoverA(g_Img_GtPts.GtPts_Fll, "");
    rg.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtPts(g_Img_GtPts));
    //------------------------------------------------------------------------------
    rg.MoverA(g_MsgTxCur.TxCur_Bco, "DEMO");                                //"BICE"
    rg.MoverA(g_MsgTxCur.TxCur_Cic, "NO");
    rg.MoverA(g_MsgTxCur.TxCur_Lin, " ");
    rg.MoverA(g_MsgTxCur.TxCur_Rsp, " ");
    rg.MoverA(g_MsgTxCur.TxCur_Idc, "00");
    rg.MoverA(g_MsgTxCur.TxCur_Stp, " ");
    rg.MoverA(g_MsgTxCur.TxCur_Imr, " ");
    rg.MoverA(g_MsgTxCur.TxCur_Msg, " ");
    rg.MoverA(g_MsgTxCur.TxCur_Trn, BF_TRN.LSet_De_Trn(g_Trn));
    rg.MoverA(g_MsgTxCur.TxCur_Img, img.LSet_De_Img(g_Img));
    rg.MoverA(g_Msg.Msg_Dat, MSGTXCUR.LSet_De_MsgTxCur(g_MsgTxCur));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GTCUR");
    if (g_Msg.Msg_Rtn.toString().compareTo("0")!=0)
       { return false; }
    g_MsgTxCur = MSGTXCUR.LSet_A_MsgTxCur(g_Msg.Msg_Dat.toString());
    return true;
  }
  //---------------------------------------------------------------------------------------
  public String Asigna_Folio(String pcFolio, RunData data, Context context)
                throws Exception 
  {
    g_MsgED090 = MSGED090.Inicia_MsgED090();
    rg.MoverA(g_MsgED090.ED090_Idr, "F");
    rg.MoverA(g_MsgED090.ED090_Cod, pcFolio);
    rg.MoverA(g_Msg.Msg_Dat, MSGED090.LSet_De_MsgED090(g_MsgED090));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED090");
    g_MsgED090 = MSGED090.LSet_A_MsgED090(g_Msg.Msg_Dat.toString());
    String lcFolio = g_MsgED090.ED090_Fol.toString();
    return lcFolio;
  }
  //---------------------------------------------------------------------------------------
  public void Limpieza(RunData data, Context context)
              throws Exception 
  {
    data.getUser().removeTemp("Agtp714_PrmPC080");
    data.getUser().removeTemp("Agtp714_MsgED135");
    data.getUser().removeTemp("Agtp714_ImgBase");
    data.getUser().removeTemp("Agtp714_ImgGtApe");
    data.getUser().removeTemp("Agtp714_ImgGtTsd");
    data.getUser().removeTemp("Agtp714_vImgGtHyp");
    data.getUser().removeTemp("Agtp714_vImgGtHpd");
    data.getUser().removeTemp("Agtp714_vTabImg");
  }
  //---------------------------------------------------------------------------------------
}