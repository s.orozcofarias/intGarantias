// Source File Name:   Agtp088.java
// Descripcion     :   Menu Operador CMA [Fte: OCM] (-)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.PRMPC080;

import com.intGlobal.modules.global.PRMCL090;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp088 extends FHTServletAction
{
  //===============================================================================================================================
  public Vector vDatos;
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  PRMPC080.PrmPC080 g_PrmPC080     = new PRMPC080.PrmPC080();
  PRMCL090.Buf_PrmCL090 g_PrmCL090 = new PRMCL090.Buf_PrmCL090();
  //-------------------------------------------------------------------------------------------
  public Agtp088()
  {
  }
  //===============================================================================================================================
  public void doPerform(RunData data, Context context) throws Exception
  {
    //Log.debug("AGTP088[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp088-DIN", data);
    if (g_Msg.getPgr().equals("RT"))
       {
         if (RUTGEN.FH_RTrim(g_Msg.getPgl()).equals("Appp030"))
            { doAgtp088_Init(data, context); }
         else
            { doAgtp088_Continue(data, context); }
       }
    else
       { doAgtp088_Init(data, context); }
  }
  //-------------------------------------------------------------------------------------------
  public void doAgtp088_Init(RunData data, Context context) throws Exception
  {
    //Log.debug("AGTP088[doAgtp088_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp088-MAN", data);
    Nueva_PrmPC080(data);
    BF_MSG.Param_Program(data, g_Msg);
    setTemplate(data, "Garantias,Agt,AGTP088.vm");
  }
  //===============================================================================================================================
  public void doAgtp088(RunData data, Context context) throws Exception
  {
    g_Msg = BF_MSG.InitIntegra("Agtp088", data, context);
    Nueva_PrmPC080(data);
    BF_MSG.Param_Program(data, g_Msg);
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.getDat());
    String Opc = data.getParameters().getString("Opc", "");
    String Ncn = data.getParameters().getString("Ncn", "");
    if (Opc.trim().equals("S"))
       {
         //Boton Salir
         BF_MSG.Return_Program(data, g_Msg);
         return;
       }
    if (Opc.equals("1"))
       {
         //Garant�as para Registro
         BF_MSG.Link_Program("Agtp088", "Aedp302", data, g_Msg);
         return;
       }
    if (Opc.equals("2"))
       {
         //Apertura Garant�a CMA
         rg.MoverA(g_PrmPC080.PC080_Acc, "APERT");
         rg.MoverA(g_PrmPC080.PC080_Ttr, "APERT");
         rg.MoverA(g_PrmPC080.PC080_Ntt, "APERTURA DE GARANTIA");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Link_Program("Agtp088", "Agtp110", data, g_Msg);
         return;
       }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (Ncn.trim().equals(""))
       {
         rg.MoverA(g_PrmPC080.PC080_Opc, Opc);
         data.getUser().removeTemp("Agtp088_g_PrmPC080");
         data.getUser().setTemp("Agtp088_g_PrmPC080" ,PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         g_PrmCL090 = PRMCL090.Inicia_PrmCL090();
         rg.MoverA(g_PrmCL090.CL090_Pcc, "N");
         rg.MoverA(g_Msg.Msg_Dat, PRMCL090.LSet_De_PrmCL090(g_PrmCL090));
         BF_MSG.Link_Program("Agtp088", "Eclp090", data, g_Msg);
         return;
       }
    else
       {
         rg.MoverA(g_PrmPC080.PC080_Cnr.Ncn, Ncn);
         doAgtp088_Despacha(data, context, Opc);
       }
  }
  //===============================================================================================================================
  public void doAgtp088_Continue(RunData data, Context context) throws Exception
  {
    //Log.debug("AGTP088[doAgtp088_Continue.start]", "[" + data.getUser().getUserName() + "]");
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (RUTGEN.FH_RTrim(g_Msg.getPgl()).equals("Eclp090"))
       {
         PRMPC080 vPrmPC080 = new PRMPC080();
         vDatos = vPrmPC080.LSet_A_vPrmPC080((String)data.getUser().getTemp("Agtp088_g_PrmPC080"));
         g_PrmPC080 = (PRMPC080.PrmPC080)vDatos.elementAt(0);
         data.getUser().removeTemp("Agtp088_g_PrmPC080");
         g_PrmCL090 = PRMCL090.LSet_A_PrmCL090(g_Msg.getDat());
         if (g_PrmCL090.getCli().trim().equals(""))
            {
              doAgtp088_Init(data, context);
              return;
            }
         rg.MoverA(g_PrmPC080.PC080_Cli, g_PrmCL090.CL090_Cli);
         rg.MoverA(g_PrmPC080.PC080_Ncl, g_PrmCL090.CL090_Ncl);
         rg.MoverA(g_PrmPC080.PC080_Tpr, g_PrmCL090.CL090_Tpr);
         rg.MoverA(g_PrmPC080.PC080_Acc, "SELECCIONA");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Link_Program("Agtp088", "Agtp400", data, g_Msg);
         return;
       }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (RUTGEN.FH_RTrim(g_Msg.getPgl()).equals("Agtp400"))
       {
         g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.getDat());
         if (g_PrmPC080.getRtn().trim().equals("NK"))
            { doAgtp088_Init(data, context); }
         else
            { doAgtp088_Despacha(data, context, g_PrmPC080.getOpc().trim()); }
         return;
       }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    doAgtp088_Init(data, context);
  }
  //===============================================================================================================================
  public void doAgtp088_Despacha(RunData data, Context context, String Opc) throws Exception
  {
    rg.MoverA(g_PrmPC080.PC080_Rtn, " ");
    String lcPrograma = "";
    if (Opc.equals("2"))
       {
         //Apertura Garant�a CMA
         rg.MoverA(g_PrmPC080.PC080_Acc, "APERT");
         rg.MoverA(g_PrmPC080.PC080_Ttr, "APERT");
         rg.MoverA(g_PrmPC080.PC080_Ntt, "APERTURA DE GARANTIA");
         lcPrograma = "Agtp110";
       }
    if (Opc.equals("3"))
       {
         //Revalorizaci�n Garant�a CMA
         rg.MoverA(g_PrmPC080.PC080_Acc, "REVAL");
         rg.MoverA(g_PrmPC080.PC080_Ttr, "REVAL");
         rg.MoverA(g_PrmPC080.PC080_Ntt, "REVALORIZACION GARANTIA");
         lcPrograma = "Agtp626";
       //lcPrograma = "Agtp110";
       }
    if (Opc.equals("4"))
       {
         //Alzamiento Garant�a CMA
         rg.MoverA(g_PrmPC080.PC080_Acc, "ALZMT");
         rg.MoverA(g_PrmPC080.PC080_Ttr, "ALZMT");
         rg.MoverA(g_PrmPC080.PC080_Ntt, "ALZAMIENTO DE GARANTIA");
         lcPrograma = "Agtp170";
       //lcPrograma = "Agtp110";
       }
    if (Opc.equals("5"))
       {
         //Conecci�n Cr�dito/Garant�a CMA
         rg.MoverA(g_PrmPC080.PC080_Acc, "CRGES");
         rg.MoverA(g_PrmPC080.PC080_Ttr, "CRGES");
         rg.MoverA(g_PrmPC080.PC080_Ntt, "CONECCION CREDITO/GARANTIA");
         lcPrograma = "Agtp175";
       }
    if (Opc.equals("6"))
       {
         //Anulaci�n Ultima Transacci�n
         rg.MoverA(g_PrmPC080.PC080_Acc, "ANULA");
         rg.MoverA(g_PrmPC080.PC080_Ttr, "ANULA");
         rg.MoverA(g_PrmPC080.PC080_Ntt, "ANULA ULT.TTR. GARANTIA");
         lcPrograma = "Agtp240";
       }
    rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
    BF_MSG.Link_Program("Agtp088", lcPrograma, data, g_Msg);
  }
  //-------------------------------------------------------------------------------------------
  public void Nueva_PrmPC080(RunData data)
  {
    PRMPC080 pPrmPC080 = new PRMPC080(g_Msg.getFch(), "OCM", "Agtp088", g_Msg.getCus(), g_Msg.getNus(), g_Msg.getFus());
    data.getUser().removeTemp("pPrmPC080");
    data.getUser().setTemp("pPrmPC080", pPrmPC080);
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    g_PrmPC080 = PRMPC080.Inicia_PrmPC080();
    rg.MoverA(g_PrmPC080.PC080_Fte,     "OCM");
    rg.MoverA(g_PrmPC080.PC080_Pai,     "CL");
    rg.MoverA(g_PrmPC080.PC080_Cnr.Sis, "GAR");
    rg.MoverA(g_PrmPC080.PC080_Slr.Slc, rg.Zeros(7));
    rg.MoverA(g_PrmPC080.PC080_Suc,     g_Msg.Msg_Suc);
    rg.MoverA(g_PrmPC080.PC080_Nsu,     g_Msg.Msg_Nsu);
    rg.MoverA(g_PrmPC080.PC080_Mnd,     "000");
    rg.MoverA(g_PrmPC080.PC080_Nmn,     "PESOS CHILE");
    rg.MoverA(g_PrmPC080.PC080_Eje,     g_Msg.Msg_Cus);
    rg.MoverA(g_PrmPC080.PC080_Nej,     g_Msg.Msg_Nus);
    rg.MoverA(g_PrmPC080.PC080_Tej,     g_Msg.Msg_Fus);
    rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
  }
  //===============================================================================================================================
}