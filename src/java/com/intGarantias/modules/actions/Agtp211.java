// Source File Name:   Agtp211.java
// Descripcion     :   Eventos en Tramite [EjecHipotecario] (ED135, GT211)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.PRMPC080;

import com.intGlobal.modules.global.MSGED135;
import com.intGlobal.modules.global.PRMED210;

import com.intGarantias.modules.global.MSGGT211;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp211 extends FHTServletAction
{
  //---------------------------------------------------------------------------------------
  public int    i = 0;
  public String lcRetorno = "";
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  PRMPC080.PrmPC080 g_PrmPC080     = new PRMPC080.PrmPC080();
  MSGED135.Buf_MsgED135 g_MsgED135 = new MSGED135.Buf_MsgED135();
  MSGGT211.Buf_MsgGT211 g_MsgGT211 = new MSGGT211.Buf_MsgGT211();
  PRMED210.Buf_PrmED210 g_PrmED210 = new PRMED210.Buf_PrmED210();
  //---------------------------------------------------------------------------------------
  public Agtp211()
  {
  }
  //---------------------------------------------------------------------------------------
  public void doPerform(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP211[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp211-DIN", data);
    if (g_Msg.Msg_Pgr.toString().equals("RT"))
       { doAgtp211_Continue(data, context); }
    else
       { doAgtp211_Init(data, context); }
  }
  //---------------------------------------------------------------------------------------
  public void doCancel(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP211[doCancel.start]", "[CANCELADO]");
    setTemplate(data, "Garantias,Agt,AGTP211.vm");
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp211_Init(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP211[doAgtp211_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    g_MsgGT211 = MSGGT211.Inicia_MsgGT211();
    rg.MoverA(g_MsgGT211.GT211_Idr, "I");
    rg.MoverA(g_MsgGT211.GT211_Tpr, "A");
    rg.MoverA(g_MsgGT211.GT211_Ejh, g_PrmPC080.PC080_Eje);
    Poblar_Panel(data, context, "I");
    setTemplate(data, "Garantias,Agt,AGTP211.vm" );
  }
  //---------------------------------------------------------------------------------------
  public void doAgtp211(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP211[doAgtp211.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp211-MAN", data);
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    String Opc = data.getParameters().getString("Opc");
    if  (Opc.equals("S"))
        {
          //Salir
          data.getUser().removeTemp("Agtp211_Msg");
          data.getUser().removeTemp("Agtp211_PrmPC080");
          data.getUser().removeTemp("Agtp211_MsgGT211");
          BF_MSG.Return_Program(data, g_Msg);
          return;
        }
    g_MsgGT211 = (MSGGT211.Buf_MsgGT211)data.getUser().getTemp("Agtp211_MsgGT211");
    MSGGT211.Bff_MsgGT211 d_MsgGT211 = new MSGGT211.Bff_MsgGT211();
    if  (Opc.equals("Pasivo"))
        {
          //Activo/Pasivo
          String lcTpr = g_MsgGT211.GT211_Tpr.toString();
          String lcEjh = g_MsgGT211.GT211_Ejh.toString();
          g_MsgGT211 = MSGGT211.Inicia_MsgGT211();
          rg.MoverA(g_MsgGT211.GT211_Idr, "I");
          if (lcTpr.equals("A"))
             { rg.MoverA(g_MsgGT211.GT211_Tpr, "P"); }
          else
             { rg.MoverA(g_MsgGT211.GT211_Tpr, "A"); }
          rg.MoverA(g_MsgGT211.GT211_Ejh, lcEjh);
          Poblar_Panel(data, context, "I");
          setTemplate(data, "Garantias,Agt,AGTP211.vm" );
          return;
        }
    if  (Opc.equals("B"))
        {
          //Detalle de Observaciones Pendientes
          String Seq = data.getParameters().getString("Seq", "");
          i = Integer.parseInt(Seq);
          d_MsgGT211 = (MSGGT211.Bff_MsgGT211)g_MsgGT211.GT211_Tab.elementAt(i);
          g_MsgED135 = MSGED135.Inicia_MsgED135();
          rg.MoverA(g_MsgED135.ED135_Idr, "C");
          rg.MoverA(g_MsgED135.ED135_Nev, d_MsgGT211.Tmt_Nev);
          rg.MoverA(g_MsgED135.ED135_Trt, d_MsgGT211.Tmt_Trt);
          rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
          g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
          g_MsgED135 = MSGED135.LSet_A_MsgED135(g_Msg.Msg_Dat.toString());
          g_PrmED210 = PRMED210.Inicia_PrmED210();
          rg.MoverA(g_PrmED210.ED210_Rtn, "RO");
          rg.MoverA(g_PrmED210.ED210_Ldt, g_MsgED135.ED135_Ldv);
          data.getUser().removeTemp("Agtp211_g_PrmPC080");
          data.getUser().setTemp("Agtp211_g_PrmPC080", g_PrmPC080);
          rg.MoverA(g_Msg.Msg_Dat, PRMED210.LSet_De_PrmED210(g_PrmED210));
          BF_MSG.Link_Program("Agtp211", "Aedp210", data, g_Msg);
          return;
        }               
    if  (Opc.equals("A"))
        {
          //Accion
          String lcPrograma = "";
          String Seq = data.getParameters().getString("Seq", "");
          i = Integer.parseInt(Seq);
          d_MsgGT211 = (MSGGT211.Bff_MsgGT211)g_MsgGT211.GT211_Tab.elementAt(i);
          if (g_MsgGT211.GT211_Tpr.toString().equals("A"))
             { rg.MoverA(g_PrmPC080.PC080_Rtn, "WR"); }
          else
             { rg.MoverA(g_PrmPC080.PC080_Rtn, "RO"); }
          rg.MoverA(g_PrmPC080.PC080_Cli,     d_MsgGT211.Evt_Cli);
          rg.MoverA(g_PrmPC080.PC080_Ncl,     d_MsgGT211.Evt_Ncl);
          rg.MoverA(g_PrmPC080.PC080_Cnr.Sis, d_MsgGT211.Evt_Sis);
          rg.MoverA(g_PrmPC080.PC080_Cnr.Ncn, d_MsgGT211.Evt_Ncn);
          rg.MoverA(g_PrmPC080.PC080_Dcn,     d_MsgGT211.Evt_Dcn);
          rg.MoverA(g_PrmPC080.PC080_Ndc,     d_MsgGT211.Evt_Dsg);
          rg.MoverA(g_PrmPC080.PC080_Ntr,     d_MsgGT211.Tmt_Nev);
          rg.MoverA(g_PrmPC080.PC080_Ttr,     d_MsgGT211.Evt_Ttr);
          rg.MoverA(g_PrmPC080.PC080_Trt,     d_MsgGT211.Tmt_Trt);
          rg.MoverA(g_PrmPC080.PC080_Mnd,     d_MsgGT211.Evt_Mnd);
          rg.MoverA(g_PrmPC080.PC080_Mnt,     d_MsgGT211.Evt_Vle);
          rg.MoverA(g_PrmPC080.PC080_Trj,     d_MsgGT211.Evt_Trj);
          rg.MoverA(g_PrmPC080.PC080_Dcm,     d_MsgGT211.Evt_Dcm);
          rg.MoverA(g_PrmPC080.PC080_Ggr,     d_MsgGT211.Evt_Ggr);
//          if (g_PrmPC080.PC080_Pai.toString().equals("CL"))
//             { lcPrograma = rg.NormaPgm(d_MsgGT211.Tmt_Pgm); }
//          else
//             {
               if (d_MsgGT211.Tmt_Est.toString().equals("TMABG"))
                  { lcPrograma = rg.NormaPgm(d_MsgGT211.Tmt_Pgm); }
               else if (d_MsgGT211.Tmt_Est.toString().equals("DVIFS"))
                  { lcPrograma = "Agtp225"; }
               else
                  { lcPrograma = "Agtp224"; }
//             }
          rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
          BF_MSG.Link_Program("Agtp211", lcPrograma, data, g_Msg); 
          if (lcRetorno.trim().compareTo("")!=0)
             {
               data.getUser().removeTemp("Agtp211_g_PrmPC080");
               data.getUser().setTemp("Agtp211_g_PrmPC080", g_PrmPC080);
               BF_MSG.MsgInt("Agtp211", data, g_Msg, lcRetorno, "1"); 
      	       return;
             }
       	  return;
        }
    if  (Opc.equals("D"))
        {
          //Devolver
          String Seq = data.getParameters().getString("Seq", "");
          i = Integer.parseInt(Seq);
          d_MsgGT211 = (MSGGT211.Bff_MsgGT211)g_MsgGT211.GT211_Tab.elementAt(i);
          if (d_MsgGT211.Tmt_Est.toString().compareTo("ENEJH")!=0)
             {
               data.getUser().removeTemp("Agtp211_g_PrmPC080");
               data.getUser().setTemp("Agtp211_g_PrmPC080", g_PrmPC080);
               BF_MSG.MsgInt("Agtp211", data, g_Msg, "No Se Puede Devolver a Ejecutivo", "1"); 
      	       return;
             }
          rg.MoverA(g_PrmPC080.PC080_Ntr, d_MsgGT211.Tmt_Nev);
          rg.MoverA(g_PrmPC080.PC080_Trt, d_MsgGT211.Tmt_Trt);
          g_PrmED210 = PRMED210.Inicia_PrmED210();
          rg.MoverA(g_PrmED210.ED210_Rtn, "RW");
          data.getUser().removeTemp("Agtp211_g_PrmPC080");
          data.getUser().setTemp("Agtp211_g_PrmPC080", g_PrmPC080);
          rg.MoverA(g_Msg.Msg_Dat, PRMED210.LSet_De_PrmED210(g_PrmED210));
          BF_MSG.Link_Program("Agtp211", "Aedp210", data, g_Msg); 
          if (lcRetorno.trim().compareTo("")!=0)
             {
               data.getUser().removeTemp("Agtp211_g_PrmPC080");
               data.getUser().setTemp("Agtp211_g_PrmPC080", g_PrmPC080);
               BF_MSG.MsgInt("Agtp211", data, g_Msg, lcRetorno, "1"); 
      	       return;
             }
       	  return;
        }
    if  (Opc.equals("Q"))
        {
          //Quitar
          String Seq = data.getParameters().getString("Seq", "");
          i = Integer.parseInt(Seq);
          d_MsgGT211 = (MSGGT211.Bff_MsgGT211)g_MsgGT211.GT211_Tab.elementAt(i);
          g_MsgED135 = MSGED135.Inicia_MsgED135();
          rg.MoverA(g_MsgED135.ED135_Idr, "X");
          rg.MoverA(g_MsgED135.ED135_Nev, d_MsgGT211.Tmt_Nev);
          rg.MoverA(g_MsgED135.ED135_Trt, d_MsgGT211.Tmt_Trt);
          rg.MoverA(g_MsgED135.ED135_Est, "");
          rg.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
          rg.MoverA(g_MsgED135.ED135_Ldv, "Esta garantia le ha sido QUITADA al Abogado por este CLegal");
          rg.MoverA(g_MsgED135.ED135_Ezt, "DVABG");
          rg.MoverA(g_MsgED135.ED135_Exi, "ABG");
          rg.MoverA(g_MsgED135.ED135_Exn, rg.Zeros(g_MsgED135.ED135_Exn));
          rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
          g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
          Poblar_Panel(data, context, "E");
          rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
          BF_MSG.Param_Program(data, g_Msg);
          setTemplate(data, "Garantias,Agt,AGTP211.vm" );
          return;
        }
    if  (Opc.equals("N"))
        {	
          //Siguiente
          Poblar_Panel(data, context, "S");
          setTemplate(data, "Garantias,Agt,AGTP211.vm" );
          return;
        }
    if  (Opc.equals("P"))
        {	
          //Previo
          Poblar_Panel(data, context, "P");
          setTemplate(data, "Garantias,Agt,AGTP211.vm" );
          return;
        }		
  }
  //---------------------------------------------------------------------------------------
  public void Poblar_Panel(RunData data, Context context, String pcIdr)
              throws Exception
  {
    MSGGT211.Bff_MsgGT211 d_MsgGT211 = new MSGGT211.Bff_MsgGT211();
    if (pcIdr.equals("E"))
       {
         g_MsgGT211 = (MSGGT211.Buf_MsgGT211)data.getUser().getTemp("Agtp211_MsgGT211");
         i = 0;
       }
    if (pcIdr.equals("S"))
       { i = Integer.parseInt(g_MsgGT211.GT211_Nro.toString())-1; }
    if (pcIdr.equals("P"))
       { i = 0; }
    if (pcIdr.compareTo("I")!=0)
       {
         String lcTpr = g_MsgGT211.GT211_Tpr.toString();
         String lcEjh = g_MsgGT211.GT211_Ejh.toString();
         d_MsgGT211 = (MSGGT211.Bff_MsgGT211)g_MsgGT211.GT211_Tab.elementAt(i);
         g_MsgGT211 = MSGGT211.Inicia_MsgGT211();
         rg.MoverA(g_MsgGT211.GT211_Idr, pcIdr);
         rg.MoverA(g_MsgGT211.GT211_Tpr, lcTpr);
         rg.MoverA(g_MsgGT211.GT211_Ejh, lcEjh);
         g_MsgGT211.GT211_Tab.set(0, d_MsgGT211);
       }
    rg.MoverA(g_Msg.Msg_Dat, MSGGT211.LSet_De_MsgGT211(g_MsgGT211));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GT211");
    g_MsgGT211 = MSGGT211.LSet_A_MsgGT211(g_Msg.Msg_Dat.toString());
    data.getUser().removeTemp("Agtp211_MsgGT211");
    data.getUser().setTemp("Agtp211_MsgGT211", g_MsgGT211);
    data.getUser().removeTemp("Agtp211_PrmPC080");
    data.getUser().setTemp("Agtp211_PrmPC080", g_PrmPC080);
    data.getUser().removeTemp("Agtp211_Msg");
    data.getUser().setTemp("Agtp211_Msg", g_Msg);
  }
//---------------------------------------------------------------------------------------
  public void doAgtp211_Continue(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AGTP211[doAgtp211_Continue.start]", "[" + data.getUser().getUserName() + "]");
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Appp030"))
       {
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp211_g_PrmPC080");
         data.getUser().removeTemp("Agtp211_g_PrmPC080");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP211.vm" );
         return;
       }
    if (rg.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Aedp210"))
       {
         g_PrmED210 = PRMED210.LSet_A_PrmED210(g_Msg.Msg_Dat.toString());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp211_g_PrmPC080");
         data.getUser().removeTemp("Agtp211_g_PrmPC080");
         if (g_PrmED210.ED210_Rtn.toString().equals("OK"))
            {
              g_MsgED135 = MSGED135.Inicia_MsgED135();
              rg.MoverA(g_MsgED135.ED135_Idr, "X");
              rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
              rg.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
              rg.MoverA(g_MsgED135.ED135_Est, "");
              rg.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
              rg.MoverA(g_MsgED135.ED135_Ldv, g_PrmED210.ED210_Ldt);
              rg.MoverA(g_MsgED135.ED135_Ezt, "DVEJH");
              rg.MoverA(g_MsgED135.ED135_Exi, "ABG");
              rg.MoverA(g_MsgED135.ED135_Exn, rg.Zeros(g_MsgED135.ED135_Exn));
              rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
              g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
              Poblar_Panel(data, context, "E");
            }
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP211.vm" );
         return;
       }
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    if (g_PrmPC080.PC080_Rtn.toString().equals("OK"))
       {
         Poblar_Panel(data, context, "E");
       }
    setTemplate(data, "Garantias,Agt,AGTP211.vm" );
    return;
  }
//---------------------------------------------------------------------------------------
}