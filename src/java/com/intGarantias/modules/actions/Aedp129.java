// Source File Name:   Aedp129.java
// Descripcion:        Hojas Presentacion BICE (-)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.BF_IMG;
import com.FHTServlet.modules.global.PRMPC080;

import com.intGarantias.modules.global.GT_IMG;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Aedp129 extends FHTServletAction
{
  //---------------------------------------------------------------------------------------
  public int    i = 0;
  public String lcData = "";
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  BF_IMG img                       = new BF_IMG();
  BF_IMG.Buf_Img g_Img             = new BF_IMG.Buf_Img();
  BF_IMG.Buf_Img_Bse g_Img_Base    = new BF_IMG.Buf_Img_Bse();
  GT_IMG gtimg                     = new GT_IMG();
  GT_IMG.Buf_Img_GtApe g_Img_GtApe = new GT_IMG.Buf_Img_GtApe();
  GT_IMG.Buf_Img_GtAlz g_Img_GtAlz = new GT_IMG.Buf_Img_GtAlz();
  GT_IMG.Buf_Img_GtAld g_Img_GtAld = new GT_IMG.Buf_Img_GtAld();
  Vector v_Img_GtAld               = new Vector();
  PRMPC080.PrmPC080 g_PrmPC080     = new PRMPC080.PrmPC080();
  //---------------------------------------------------------------------------------------
  public Aedp129()
  {
  }
  //---------------------------------------------------------------------------------------
  public void doPerform(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AEDP129[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Aedp129-DIN", data);
    doAedp129_Init(data, context);
  }
  //---------------------------------------------------------------------------------------
  public void doCancel(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AEDP129[doCancel.start]", "[CANCELADO]");
    setTemplate(data, "Garantias,Aed,AEDP129.vm");
  }
  //---------------------------------------------------------------------------------------
  public void doAedp129_Init(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AEDP129[doAedp129_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    Carga_de_Host(data, context);
    if (g_PrmPC080.PC080_Ttr.toString().trim().equals("ALZMT")
     || g_PrmPC080.PC080_Ttr.toString().trim().equals("ALZPI"))
       { setTemplate(data, "Garantias,Aed,AEDP129Z.vm" ); }
    else
       { setTemplate(data, "Garantias,Aed,AEDP129A.vm" ); }
  }
  //---------------------------------------------------------------------------------------
  public void doAedp129(RunData data, Context context)
              throws Exception
  {
    //Log.debug("AEDP129[doAedp129.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Aedp129-MAN", data);
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
    String Opc = data.getParameters().getString("Opc" ,"");
    if (Opc.trim().equals("S"))
       {
         //Boton Salir
         data.getUser().removeTemp("Aedp129_ImgBase");
         data.getUser().removeTemp("Aedp129_ImgGtApe");
         data.getUser().removeTemp("Aedp129_ImgGtAlz");
         data.getUser().removeTemp("Aedp129_vImgGtAld");
         rg.MoverA(g_PrmPC080.PC080_Rtn, "NK");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Aedp129", data, g_Msg);
         return;
       }
  }
  //---------------------------------------------------------------------------------------
  public void Carga_de_Host(RunData data, Context context)
              throws Exception
  {
    Vector g_Tab_Img = img.Carga_Img_Host(g_Msg, g_PrmPC080.PC080_Ntr.toString(), data, context);
    De_Tab_Img(g_Tab_Img, data);
    data.getUser().removeTemp("Aedp129_ImgBase");
    data.getUser().setTemp("Aedp129_ImgBase", g_Img_Base);
    data.getUser().removeTemp("Aedp129_ImgGtApe");
    data.getUser().setTemp("Aedp129_ImgGtApe", g_Img_GtApe);
    data.getUser().removeTemp("Aedp129_ImgGtAlz");
    data.getUser().setTemp("Aedp129_ImgGtAlz", g_Img_GtAlz);
    data.getUser().removeTemp("Aedp129_vImgGtAld");
    data.getUser().setTemp("Aedp129_vImgGtAld", v_Img_GtAld);
  }
  //---------------------------------------------------------------------------------------
  public void De_Tab_Img(Vector g_Tab_Img, RunData data)
  {
    g_Img       = img.Inicia_Img();
    lcData      = img.LSet_De_Img(g_Img);
    g_Img_Base  = img.LSet_A_ImgBase(lcData);
    g_Img_GtApe = gtimg.LSet_A_ImgGtApe(g_Img.Img_Dat.toString());
    g_Img_GtAlz = gtimg.LSet_A_ImgGtAlz(g_Img.Img_Dat.toString());
    v_Img_GtAld.clear();
    for (i=0; i<g_Tab_Img.size(); i++)
        {
          lcData = img.LSet_De_Img((BF_IMG.Buf_Img)g_Tab_Img.elementAt(i));
          g_Img  = img.LSet_A_Img(lcData);
          if (g_Img.Img_Dax.toString().trim().equals("BASE"))
             {
               if (g_PrmPC080.PC080_Ttr.toString().trim().equals("ALZMT")
                || g_PrmPC080.PC080_Ttr.toString().trim().equals("ALZPI"))
                  {
                    g_Img_GtAlz = gtimg.LSet_A_ImgGtAlz(g_Img.Img_Dat.toString());
                    g_Img_Base  = img.LSet_A_ImgBase(g_Img_GtAlz.GtAlz_Bse.toString());
                  }
               else
                  {
                    g_Img_GtApe = gtimg.LSet_A_ImgGtApe(g_Img.Img_Dat.toString());
                    g_Img_Base  = img.LSet_A_ImgBase(g_Img_GtApe.GtApe_Bse.toString());
                  }
             }
          if (g_Img.Img_Dax.toString().trim().equals("DTALZ"))
             {
               g_Img_GtAld = gtimg.LSet_A_ImgGtAld(g_Img.Img_Dat.toString());
               v_Img_GtAld.add (g_Img_GtAld);
             }
        }
  }
  //---------------------------------------------------------------------------------------
}