// Source File Name:   Agtp110.java
// Descripcion     :   Ingreso Nueva Garantia (Operacional) (ED500, GT090, GT096, GT110)

package com.intGarantias.modules.actions;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.BF_IMG;
import com.FHTServlet.modules.global.BF_EVT;
import com.FHTServlet.modules.global.PRMPC080;
import com.FHTServlet.modules.global.PRMPP030;
import com.FHTServlet.modules.util.ScreenHelper;

import com.intGlobal.modules.global.PRMCL090;
import com.intGlobal.modules.global.MSGED100;
import com.intGlobal.modules.global.MSGED135;
import com.intGlobal.modules.global.PRMED210;

import com.intGarantias.modules.global.MSGGT090;
import com.intGarantias.modules.global.MSGGT093;
import com.intGarantias.modules.global.MSGGT096;
import com.intGarantias.modules.global.GT_IMG;

import java.util.*;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.apache.turbine.util.Log;

public class Agtp110 extends FHTServletAction
{
  //===============================================================================================================================
  public int i = 0;
  public String lcData = "";
  public Vector vDatos;
  RUTGEN rg                        = new RUTGEN();
  BF_MSG msg                       = new BF_MSG();
  BF_MSG.Buf_Msg g_Msg             = new BF_MSG.Buf_Msg();
  BF_EVT.Buf_Evt g_Evt             = new BF_EVT.Buf_Evt();
  BF_IMG img                       = new BF_IMG();
  BF_IMG.Buf_Img g_Img             = new BF_IMG.Buf_Img();
  BF_IMG.Buf_Img_Bse g_Img_Base    = new BF_IMG.Buf_Img_Bse();
  GT_IMG gtimg                     = new GT_IMG();
  GT_IMG.Buf_Img_GtApe g_Img_GtApe = new GT_IMG.Buf_Img_GtApe();
  GT_IMG.Buf_Img_GtCma g_Img_GtCma = new GT_IMG.Buf_Img_GtCma();
  PRMPC080.PrmPC080 g_PrmPC080     = new PRMPC080.PrmPC080();
  PRMCL090.Buf_PrmCL090 g_PrmCL090 = new PRMCL090.Buf_PrmCL090();
  MSGED100.Buf_MsgED100 g_MsgED100 = new MSGED100.Buf_MsgED100();
  MSGED135.Buf_MsgED135 g_MsgED135 = new MSGED135.Buf_MsgED135();
  PRMED210.Buf_PrmED210 g_PrmED210 = new PRMED210.Buf_PrmED210();
  MSGGT090.Buf_MsgGT090 g_MsgGT090 = new MSGGT090.Buf_MsgGT090();
  MSGGT093.Buf_MsgGT093 g_MsgGT093 = new MSGGT093.Buf_MsgGT093();
  MSGGT096.Buf_MsgGT096 g_MsgGT096 = new MSGGT096.Buf_MsgGT096();
  PRMPP030.Buf_PrmPP030 g_PrmPP030 = new PRMPP030.Buf_PrmPP030();
  //-------------------------------------------------------------------------------------------
  public Agtp110()
  {
  }
  //===============================================================================================================================
  public void doPerform(RunData data, Context context) throws Exception
  {
    //Log.debug("AGTP110[doPerform.start]", "[SOLO.LLAMADAS.DINAMICAS]");
    g_Msg = BF_MSG.Init_Program("Agtp110-DIN", data);
    if (g_Msg.getPgr().equals("RT"))
       { doAgtp110_Continue(data, context); }
    else
       { doAgtp110_Init(data, context); }
  }
  //-------------------------------------------------------------------------------------------
  public void doAgtp110_Init(RunData data, Context context) throws Exception
  {
    //Log.debug("AGTP110[doAgtp110_Init.start]", "[" + data.getUser().getUserName() + "]");
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.getDat());
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (g_PrmPC080.getTrt().trim().equals(""))
       { rg.MoverA(g_PrmPC080.PC080_Trt, "UNICO"); }
    if (g_PrmPC080.getCli().trim().equals(""))
       {
         data.getUser().removeTemp("Agtp110_g_PrmPC080");
         data.getUser().setTemp("Agtp110_g_PrmPC080" ,g_PrmPC080);
         g_PrmCL090 = PRMCL090.Inicia_PrmCL090();
         rg.MoverA(g_PrmCL090.CL090_Pcc, "N");
         rg.MoverA(g_Msg.Msg_Dat, PRMCL090.LSet_De_PrmCL090(g_PrmCL090));
         BF_MSG.Link_Program("Agtp110", "Eclp090", data, g_Msg);
         return;
       }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    String XX = "";
    if (g_PrmPC080.getNtr().trim().compareTo("")!=0)
       { Carga_Data("PREVIA", data, context);  XX = "PREVIA"; }
    else
       { Carga_Data("NUEVA", data, context);  XX = "NUEVA"; }
    if (!g_PrmPC080.getRtn().equals("OK"))
       { return; }
    Carga_Combos(data, context);
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (XX.trim().equals("PREVIA"))
       {
         if (g_PrmPC080.getAcc().trim().equals("CONSU"))
            { rg.MoverA(g_PrmPC080.PC080_Rtn, "RO"); }
         else if (g_PrmPC080.getAcc().trim().equals("APERT"))
            { rg.MoverA(g_PrmPC080.PC080_Rtn, "RW"); }
         else if (g_PrmPC080.getAcc().trim().equals("REVAL"))
            { rg.MoverA(g_PrmPC080.PC080_Rtn, "RV"); }
         else if (g_PrmPC080.getAcc().trim().equals("ALZMT"))
            { rg.MoverA(g_PrmPC080.PC080_Rtn, "RZ"); }
         else if (g_PrmPC080.getAcc().trim().equals("DEVOL"))
            { rg.MoverA(g_PrmPC080.PC080_Rtn, "RD"); }
         else
            { rg.MoverA(g_PrmPC080.PC080_Rtn, "RO"); }
       }
    else
       { rg.MoverA(g_PrmPC080.PC080_Rtn, "RW"); }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    data.getUser().removeTemp("Agtp110_PrmPC080");
    data.getUser().setTemp("Agtp110_PrmPC080", g_PrmPC080);
    rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
    BF_MSG.Param_Program(data, g_Msg);
    setTemplate(data, "Garantias,Agt,AGTP110.vm");
  }
  //===============================================================================================================================
  public void doAgtp110(RunData data, Context context) throws Exception
  {
    //Log.debug("AGTP110[doAgtp110.start]", "[" + data.getUser().getUserName() + "]");
    g_Msg = BF_MSG.Init_Program("Agtp110-MAN", data);
    g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.getDat());
    String Opc = data.getParameters().getString("Opc", "");
    if (Opc.trim().equals("S"))
       {
         //Boton Salir
         Limpieza(data, context);
         rg.MoverA(g_PrmPC080.PC080_Rtn, "");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp110", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("R"))
       {
         //Boton Errores
         Actualiza_Datos(data, context);
         g_MsgED135 = (MSGED135.Buf_MsgED135)data.getUser().getTemp("Agtp110_MsgED135");
         g_PrmED210 = PRMED210.Inicia_PrmED210();
         rg.MoverA(g_PrmED210.ED210_Rtn, "RO");
         rg.MoverA(g_PrmED210.ED210_Ldt, g_MsgED135.ED135_Ldv);
         data.getUser().removeTemp("Agtp110_g_PrmPC080");
         data.getUser().setTemp("Agtp110_g_PrmPC080" ,g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMED210.LSet_De_PrmED210(g_PrmED210));
         BF_MSG.Link_Program("Agtp110", "Aedp210", data, g_Msg);
         return;
       }
    if (Opc.trim().equals("V"))
       {
         //Validacion
         Actualiza_Datos(data, context);
         data.getUser().removeTemp("Agtp110_PrmPC080");
         data.getUser().setTemp("Agtp110_PrmPC080", g_PrmPC080);
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP110.vm");
         return;
       }
    if (Opc.trim().equals("A"))
       {
         //Boton Aceptar
         Actualiza_Datos(data, context);
         Cursatura(g_PrmPC080.getTrt(), data, context);
         Limpieza(data, context);
         rg.MoverA(g_PrmPC080.PC080_Rtn, "");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Return_Data("Agtp110", data, g_Msg);
         return;
       }
  }
  //===============================================================================================================================
  public void doAgtp110_Continue(RunData data, Context context) throws Exception
  {
    //Log.debug("AGTP110[doAgtp110_Continue.start]", "[" + data.getUser().getUserName() + "]");
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (RUTGEN.FH_RTrim(g_Msg.getPgl()).equals("Eclp090"))
       {
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp110_g_PrmPC080");
         data.getUser().removeTemp("Agtp110_g_PrmPC080");
         g_PrmCL090 = PRMCL090.LSet_A_PrmCL090(g_Msg.getDat());
         if (!g_PrmCL090.getCli().trim().equals(""))
            {
              rg.MoverA(g_PrmPC080.PC080_Cli, g_PrmCL090.CL090_Cli);
              rg.MoverA(g_PrmPC080.PC080_Ncl, g_PrmCL090.CL090_Ncl);
              rg.MoverA(g_PrmPC080.PC080_Tpr, g_PrmCL090.CL090_Tpr);
              rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
              BF_MSG.Param_Program(data, g_Msg);
              doAgtp110_Init(data, context);
              return;
            }
       }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (RUTGEN.FH_RTrim(g_Msg.getPgl()).equals("Aedp210"))
       {
         g_PrmED210 = PRMED210.LSet_A_PrmED210(g_Msg.getDat());
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp110_g_PrmPC080");
         data.getUser().removeTemp("Agtp110_g_PrmPC080");
         rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
         BF_MSG.Param_Program(data, g_Msg);
         setTemplate(data, "Garantias,Agt,AGTP110.vm");
         return;
       }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (RUTGEN.FH_RTrim(g_Msg.getPgl()).equals("Appp030"))
       {
         g_PrmPC080 = (PRMPC080.PrmPC080)data.getUser().getTemp("Agtp110_g_PrmPC080");
         data.getUser().removeTemp("Agtp110_g_PrmPC080");
         g_PrmPP030 = PRMPP030.LSet_A_PrmPP030(g_Msg.getDat());
         if (!g_PrmPP030.getRsp().trim().equals("TERMINAR"))
            {
              rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
              BF_MSG.Param_Program(data, g_Msg);
              setTemplate(data, "Garantias,Agt,AGTP110.vm");
              return;
            }
       }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    Limpieza(data, context);
    rg.MoverA(g_PrmPC080.PC080_Rtn, "");
    rg.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
    BF_MSG.Return_Data("Agtp110", data, g_Msg);
  }
  //===============================================================================================================================
  public void Actualiza_Datos(RunData data, Context context) throws Exception
  {
    g_Evt       = (BF_EVT.Buf_Evt)data.getUser().getTemp("Agtp110_Evt");
    g_Img_Base  = (BF_IMG.Buf_Img_Bse)data.getUser().getTemp("Agtp110_ImgBase");
    g_Img_GtApe = (GT_IMG.Buf_Img_GtApe)data.getUser().getTemp("Agtp110_ImgGtApe");
    g_Img_GtCma = (GT_IMG.Buf_Img_GtCma)data.getUser().getTemp("Agtp110_ImgGtCma");
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    String lSuc = data.getParameters().getString("Base_Suc", "");
    String lNsu = data.getParameters().getString("Base_Nsu", "");
    Cambio_Sucursal(lSuc, lNsu);
    String lMnd = data.getParameters().getString("Base_Mnd", "");
    String lNmn = data.getParameters().getString("Base_Nmn", "");
    String lTmn = data.getParameters().getString("Base_Tmn", "");
    String lDcm = data.getParameters().getString("Base_Dcm", "");
    Cambio_Moneda(lMnd, lNmn, lTmn, lDcm);
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    rg.MoverA(g_Img_GtApe.GtApe_Bse, img.LSet_De_ImgBase_Bse(g_Img_Base));
    rg.MoverA(g_Img_GtApe.GtApe_Dsg, data.getParameters().getString("GtApe_Dsg", ""));
    rg.MoverA(g_Img_GtApe.GtApe_Cbt, data.getParameters().getString("GtApe_Cbt", ""));
    rg.MoverA(g_Img_GtApe.GtApe_Dpv, data.getParameters().getString("GtApe_Dpv", ""));
    rg.MoverA(g_Img_GtApe.GtApe_Dpp, data.getParameters().getString("GtApe_Dpp", ""));
    rg.MoverA(g_Img_GtApe.GtApe_Pct, data.getParameters().getString("GtApe_Pct", ""));
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    rg.MoverA(g_Img_GtCma.GtCma_Fui, data.getParameters().getString("GtCma_Fui", ""));
    rg.MoverA(g_Img_GtCma.GtCma_Nmi, data.getParameters().getString("GtCma_Nmi", ""));
    rg.MoverA(g_Img_GtCma.GtCma_Nml, data.getParameters().getString("GtCma_Nml", ""));
    rg.MoverA(g_Img_GtCma.GtCma_Lin, data.getParameters().getString("GtCma_Lin", ""));
    rg.MoverA(g_Img_GtCma.GtCma_Fev, data.getParameters().getString("GtCma_Fev", ""));
    rg.MoverA(g_Img_GtCma.GtCma_Cnt, data.getParameters().getString("GtCma_Cnt", ""));
    rg.MoverA(g_Img_GtCma.GtCma_Vcn, data.getParameters().getString("GtCma_Vcn", ""));
    rg.MoverA(g_Img_GtCma.GtCma_Dvf, data.getParameters().getString("GtCma_Dvf", ""));
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    rg.MoverA(g_Img_GtApe.GtApe_Vtt, rg.FmtValor(g_Img_GtCma.GtCma_Nml,2,4,15,"+"));
    rg.MoverA(g_Img_GtApe.GtApe_Vts, g_Img_GtCma.GtCma_Nml);
    rg.MoverA(g_Img_GtApe.GtApe_Vtb, g_Img_GtCma.GtCma_Nml);
    rg.MoverA(g_Img_GtApe.GtApe_Vcn, g_Img_GtCma.GtCma_Vcn);
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    data.getUser().removeTemp("Agtp110_Evt");
    data.getUser().setTemp("Agtp110_Evt", g_Evt);
    data.getUser().removeTemp("Agtp110_ImgBase");
    data.getUser().setTemp("Agtp110_ImgBase", g_Img_Base);
    data.getUser().removeTemp("Agtp110_ImgGtApe");
    data.getUser().setTemp("Agtp110_ImgGtApe", g_Img_GtApe);
    data.getUser().removeTemp("Agtp110_ImgGtCma");
    data.getUser().setTemp("Agtp110_ImgGtCma", g_Img_GtCma);
  }
  //-------------------------------------------------------------------------------------------
  public void Cambio_Sucursal(String pSuc, String pNsu)
  {
    rg.MoverA(g_Evt.Evt_Suc,           pSuc);
    rg.MoverA(g_Evt.Evt_Nsu,           pNsu);
    rg.MoverA(g_Img_Base.Img_Base_Suc, g_Evt.Evt_Suc);
    rg.MoverA(g_Img_Base.Img_Base_Nsu, g_Evt.Evt_Nsu);
    rg.MoverA(g_PrmPC080.PC080_Suc,    g_Evt.Evt_Suc);
    rg.MoverA(g_PrmPC080.PC080_Nsu,    g_Evt.Evt_Nsu);
  }
  //-------------------------------------------------------------------------------------------
  public void Cambio_Moneda(String pMnd, String pNmn, String pTmn, String pDcm)
  {
    rg.MoverA(g_Evt.Evt_Mnd,           pMnd);
    rg.MoverA(g_Evt.Evt_Nmn,           pNmn);
    rg.MoverA(g_Evt.Evt_Tmn,           pTmn);
    rg.MoverA(g_Evt.Evt_Dcm,           pDcm);
    rg.MoverA(g_Img_Base.Img_Base_Mnd, g_Evt.Evt_Mnd);
    rg.MoverA(g_Img_Base.Img_Base_Nmn, g_Evt.Evt_Nmn);
    rg.MoverA(g_Img_Base.Img_Base_Tmn, g_Evt.Evt_Tmn);
    rg.MoverA(g_Img_Base.Img_Base_Dcm, g_Evt.Evt_Dcm);
    rg.MoverA(g_PrmPC080.PC080_Mnd,    g_Evt.Evt_Mnd);
    rg.MoverA(g_PrmPC080.PC080_Nmn,    g_Evt.Evt_Nmn);
    rg.MoverA(g_PrmPC080.PC080_Tmn,    g_Evt.Evt_Tmn);
    rg.MoverA(g_PrmPC080.PC080_Dcm,    g_Evt.Evt_Dcm);
  }
  //===============================================================================================================================
  public void Cursatura(String pcTrt, RunData data, Context context) throws Exception
  {
    Vector v_TabImg = A_Tab_Img(data);
    img.Graba_Img_Host(g_Msg, v_TabImg, data, context);
  //rg.MoverA(g_Evt.Evt_Vpr, g_Img_GtApe.GtApe_Vle);
  //rg.MoverA(g_Evt.Evt_Dsg, g_Img_GtApe.GtApe_Dsg);
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    g_Evt = (BF_EVT.Buf_Evt)data.getUser().getTemp("Agtp110_Evt");
    rg.MoverA(g_Evt.Evt_Est, "ENTMT");
    rg.MoverA(g_Evt.Evt_Fpr, g_Msg.Msg_Fch);
    BF_EVT t = new BF_EVT();
    g_MsgED100 = MSGED100.Inicia_MsgED100();
    rg.MoverA(g_MsgED100.ED100_Idr, "U");
    rg.MoverA(g_MsgED100.ED100_Nev, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_MsgED100.ED100_Evt, t.LSet_De_Evt(g_Evt));
    rg.MoverA(g_Msg.Msg_Dat, MSGED100.LSet_De_MsgED100(g_MsgED100));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED500");
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    g_MsgED135 = MSGED135.Inicia_MsgED135();
    rg.MoverA(g_MsgED135.ED135_Idr, "M");
    rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_MsgED135.ED135_Trt, "UNICO");
    rg.MoverA(g_MsgED135.ED135_Est, "");
    rg.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
    rg.MoverA(g_MsgED135.ED135_Vlc, g_Img_GtCma.GtCma_Vcn);
    rg.MoverA(g_MsgED135.ED135_Ezt, "ENSCM");
    rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
  }
  //-------------------------------------------------------------------------------------------
  public Vector A_Tab_Img(RunData data)
  {
    BF_IMG.Buf_Img lwImg = null;
    Vector v_Img = new Vector();
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    lwImg = new BF_IMG.Buf_Img();
    rg.MoverA(lwImg.Img_Ntr, g_PrmPC080.PC080_Ntr);
    rg.MoverA(lwImg.Img_Dax, "BASE");
    rg.MoverA(lwImg.Img_Seq, 0);
    rg.MoverA(lwImg.Img_Dat, gtimg.LSet_De_ImgGtApe(g_Img_GtApe));
    v_Img.add(lwImg);
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    lwImg = new BF_IMG.Buf_Img();
    rg.MoverA(lwImg.Img_Ntr, g_PrmPC080.PC080_Ntr);
    rg.MoverA(lwImg.Img_Dax, "ZZCMA");
    rg.MoverA(lwImg.Img_Seq, 1);
    rg.MoverA(lwImg.Img_Dat, gtimg.LSet_De_ImgGtCma(g_Img_GtCma));
    v_Img.add(lwImg);
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    return v_Img;
  }
  //===============================================================================================================================
  public void Carga_Data(String pcIdr, RunData data, Context context) throws Exception
  {
    g_MsgED135 = MSGED135.Inicia_MsgED135();
    if (pcIdr.trim().equals("NUEVA"))
       {
         if (Asigna_Folios(data, context)==false)
            {
              rg.MoverA(g_PrmPC080.PC080_Rtn, "NK");
              return;
            }
         Trae_DcnDgt(data, context);
         Arma_Inicial(data, context);
         Inicializa_Img(data, context);
       }
    else
       {
         Carga_de_Host(data, context);
       }
    rg.MoverA(g_PrmPC080.PC080_Rtn, "OK");
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    data.getUser().removeTemp("Agtp110_MsgED135");
    data.getUser().setTemp("Agtp110_MsgED135", g_MsgED135);
    data.getUser().removeTemp("Agtp110_Evt");
    data.getUser().setTemp("Agtp110_Evt", g_Evt);
    data.getUser().removeTemp("Agtp110_ImgBase");
    data.getUser().setTemp("Agtp110_ImgBase", g_Img_Base);
    data.getUser().removeTemp("Agtp110_ImgGtApe");
    data.getUser().setTemp("Agtp110_ImgGtApe", g_Img_GtApe);
    data.getUser().removeTemp("Agtp110_ImgGtCma");
    data.getUser().setTemp("Agtp110_ImgGtCma", g_Img_GtCma);
  }
  //-------------------------------------------------------------------------------------------
  public boolean Asigna_Folios(RunData data, Context context) throws Exception
  {
    g_MsgGT096 = MSGGT096.Inicia_MsgGT096();
    rg.MoverA(g_MsgGT096.GT096_Idr, "Q");
    rg.MoverA(g_MsgGT096.GT096_Cli, g_PrmPC080.PC080_Cli);
  //rg.MoverA(g_MsgGT096.GT096_Sis, g_PrmPC080.PC080_Cnr.Sis);
  //rg.MoverA(g_MsgGT096.GT096_Ttr, g_PrmPC080.PC080_Ttr);
  //rg.MoverA(g_MsgGT096.GT096_Fec, g_Msg.Msg_Fch);
  //rg.MoverA(g_MsgGT096.GT096_Suc, g_Msg.Msg_Suc);
    rg.MoverA(g_Msg.Msg_Dat, MSGGT096.LSet_De_MsgGT096(g_MsgGT096));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GT096");
    g_MsgGT096 = MSGGT096.LSet_A_MsgGT096(g_Msg.getDat());
    if (g_MsgGT096.getRtn().trim().equals("E") )
       {
         data.getUser().removeTemp("Agtp110_g_PrmPC080");
         data.getUser().setTemp("Agtp110_g_PrmPC080", g_PrmPC080);
         BF_MSG.MsgInt("Agtp110", data, g_Msg, g_MsgGT096.getMse(), "T");
         return false;
       }
    rg.MoverA(g_PrmPC080.PC080_Ntr,     g_MsgGT096.GT096_Ntr);
  //rg.MoverA(g_PrmPC080.PC080_Ntt,     g_MsgGT096.GT096_Ntt);
    rg.MoverA(g_PrmPC080.PC080_Ncl,     g_MsgGT096.GT096_Ncl);
    rg.MoverA(g_PrmPC080.PC080_Tpr,     g_MsgGT096.GT096_Tpr);
    rg.MoverA(g_PrmPC080.PC080_Dir,     g_MsgGT096.GT096_Dir);
    rg.MoverA(g_PrmPC080.PC080_Cmn,     g_MsgGT096.GT096_Cmn);
    rg.MoverA(g_PrmPC080.PC080_Tfc,     g_MsgGT096.GT096_Tfc);
    rg.MoverA(g_PrmPC080.PC080_Cnr.Ncn, g_MsgGT096.GT096_Ncn);
    return true;
  }
  //===============================================================================================================================
  public void Trae_DcnDgt(RunData data, Context context) throws Exception
  {
    g_MsgGT090 = MSGGT090.Inicia_MsgGT090();
    rg.MoverA(g_MsgGT090.GT090_Idr, "I");
    rg.MoverA(g_MsgGT090.GT090_Sis, g_PrmPC080.PC080_Cnr.Sis);
    rg.MoverA(g_MsgGT090.GT090_Ggr, "CMA");
  //MSGGT090.Consulta_MsgGT090(g_MsgGT090);
    rg.MoverA(g_Msg.Msg_Dat, MSGGT090.LSet_De_MsgGT090(g_MsgGT090));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GT090");
    g_MsgGT090 = MSGGT090.LSet_A_MsgGT090(g_Msg.getDat());
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    Vector vTmp = g_MsgGT090.getTab();
    MSGGT090.Bff_MsgGT090 d_MsgGT090 = new MSGGT090.Bff_MsgGT090();
    d_MsgGT090 = (MSGGT090.Bff_MsgGT090)vTmp.elementAt(0);
    rg.MoverA(g_PrmPC080.PC080_Dcn, d_MsgGT090.GT090_Dcn);
    rg.MoverA(g_PrmPC080.PC080_Ndc, d_MsgGT090.GT090_Dsc);
    rg.MoverA(g_PrmPC080.PC080_Tmn, d_MsgGT090.GT090_Tmn);
    rg.MoverA(g_PrmPC080.PC080_Trj, d_MsgGT090.GT090_Trj);
    rg.MoverA(g_PrmPC080.PC080_Dcm, d_MsgGT090.GT090_Dcm);
    rg.MoverA(g_PrmPC080.PC080_Prp, d_MsgGT090.GT090_Prp);
    rg.MoverA(g_PrmPC080.PC080_Ggr, d_MsgGT090.GT090_Ggr);
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    rg.MoverA(g_PrmPC080.PC080_Tmn, "LOC");
    rg.MoverA(g_PrmPC080.PC080_Mnd, "000");
    rg.MoverA(g_PrmPC080.PC080_Nmn, "PESO");
    rg.MoverA(g_PrmPC080.PC080_Dcm, "0");
    rg.MoverA(g_PrmPC080.PC080_Suc, "001");
    rg.MoverA(g_PrmPC080.PC080_Nsu, "PRINCIPAL");
  }
  //-------------------------------------------------------------------------------------------
  public void Arma_Inicial(RunData data, Context context) throws Exception
  {
  //rg.MoverA(g_Evt.Evt_Nev, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_Evt.Evt_Ttr, g_PrmPC080.PC080_Ttr);
    rg.MoverA(g_Evt.Evt_Ntt, g_PrmPC080.PC080_Ntt);
    rg.MoverA(g_Evt.Evt_Tsl, "");
    rg.MoverA(g_Evt.Evt_Slc, 0);
    rg.MoverA(g_Evt.Evt_Cli, g_PrmPC080.PC080_Cli);
    rg.MoverA(g_Evt.Evt_Ncl, g_PrmPC080.PC080_Ncl);
    rg.MoverA(g_Evt.Evt_Tpr, g_PrmPC080.PC080_Tpr);
    rg.MoverA(g_Evt.Evt_Dir, g_PrmPC080.PC080_Dir);
    rg.MoverA(g_Evt.Evt_Cmn, g_PrmPC080.PC080_Cmn);
    rg.MoverA(g_Evt.Evt_Tfc, g_PrmPC080.PC080_Tfc);
    rg.MoverA(g_Evt.Evt_Sis, g_PrmPC080.PC080_Cnr.Sis);
    rg.MoverA(g_Evt.Evt_Ncn, g_PrmPC080.PC080_Cnr.Ncn);
    rg.MoverA(g_Evt.Evt_Dcn, g_PrmPC080.PC080_Dcn);
    rg.MoverA(g_Evt.Evt_Ndc, g_PrmPC080.PC080_Ndc);
    rg.MoverA(g_Evt.Evt_Tmn, g_PrmPC080.PC080_Tmn);
    rg.MoverA(g_Evt.Evt_Trj, g_PrmPC080.PC080_Trj);
    rg.MoverA(g_Evt.Evt_Dcm, g_PrmPC080.PC080_Dcm);
    rg.MoverA(g_Evt.Evt_Prp, g_PrmPC080.PC080_Prp);
    rg.MoverA(g_Evt.Evt_Suc, g_PrmPC080.PC080_Suc);
    rg.MoverA(g_Evt.Evt_Nsu, g_PrmPC080.PC080_Nsu);
    rg.MoverA(g_Evt.Evt_Mnd, g_PrmPC080.PC080_Mnd);
    rg.MoverA(g_Evt.Evt_Nmn, g_PrmPC080.PC080_Nmn);
    rg.MoverA(g_Evt.Evt_Eje, g_PrmPC080.PC080_Eje);
    rg.MoverA(g_Evt.Evt_Nej, g_PrmPC080.PC080_Nej);
    rg.MoverA(g_Evt.Evt_Tej, g_PrmPC080.PC080_Tej);
    rg.MoverA(g_Evt.Evt_Est, "NUEVA");
    rg.MoverA(g_Evt.Evt_Fst, g_Msg.Msg_Fch);
    rg.MoverA(g_Evt.Evt_Upr, g_PrmPC080.PC080_Trj);
    rg.MoverA(g_Evt.Evt_Vpr, rg.Zeros(g_Evt.Evt_Vpr));
    rg.MoverA(g_Evt.Evt_Fpr, g_Msg.Msg_Fch);
    rg.MoverA(g_Evt.Evt_Spr, rg.Zeros(g_Evt.Evt_Spr));
    rg.MoverA(g_Evt.Evt_Tpp, "");
    rg.MoverA(g_Evt.Evt_Ggr, g_PrmPC080.PC080_Ggr);
    rg.MoverA(g_Evt.Evt_Fll, "");
  //rg.MoverA(g_Evt.Evt_Dsg, "");
  }
  //-------------------------------------------------------------------------------------------
  public void Inicializa_Img(RunData data, Context context ) throws Exception
  {
    rg.MoverA(g_Img_Base.Img_Base_Ntr, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_Img_Base.Img_Base_Ttr, g_Evt.Evt_Ttr);
    rg.MoverA(g_Img_Base.Img_Base_Ntt, g_Evt.Evt_Ntt);
    rg.MoverA(g_Img_Base.Img_Base_Tsl, g_Evt.Evt_Tsl);
    rg.MoverA(g_Img_Base.Img_Base_Slc, g_Evt.Evt_Slc);
    rg.MoverA(g_Img_Base.Img_Base_Cmt, "");
    rg.MoverA(g_Img_Base.Img_Base_Cli, g_Evt.Evt_Cli);
    rg.MoverA(g_Img_Base.Img_Base_Ncl, g_Evt.Evt_Ncl);
    rg.MoverA(g_Img_Base.Img_Base_Tpr, g_Evt.Evt_Tpr);
    rg.MoverA(g_Img_Base.Img_Base_Dir, g_Evt.Evt_Dir);
  //rg.MoverA(g_Img_Base.Img_Base_Cmn, g_Evt.Evt_Cmn);
    rg.MoverA(g_Img_Base.Img_Base_Tfc, g_Evt.Evt_Tfc);
    rg.MoverA(g_Img_Base.Img_Base_Sis, g_Evt.Evt_Sis);
    rg.MoverA(g_Img_Base.Img_Base_Ncn, g_Evt.Evt_Ncn);
    rg.MoverA(g_Img_Base.Img_Base_Dcn, g_Evt.Evt_Dcn);
    rg.MoverA(g_Img_Base.Img_Base_Ndc, g_Evt.Evt_Ndc);
    rg.MoverA(g_Img_Base.Img_Base_Tmn, g_Evt.Evt_Tmn);
    rg.MoverA(g_Img_Base.Img_Base_Trj, g_Evt.Evt_Trj);
    rg.MoverA(g_Img_Base.Img_Base_Dcm, g_Evt.Evt_Dcm);
    rg.MoverA(g_Img_Base.Img_Base_Prp, g_Evt.Evt_Prp);
    rg.MoverA(g_Img_Base.Img_Base_Suc, g_Evt.Evt_Suc);
    rg.MoverA(g_Img_Base.Img_Base_Nsu, g_Evt.Evt_Nsu);
    rg.MoverA(g_Img_Base.Img_Base_Mnd, g_Evt.Evt_Mnd);
    rg.MoverA(g_Img_Base.Img_Base_Nmn, g_Evt.Evt_Nmn);
    rg.MoverA(g_Img_Base.Img_Base_Eje, g_Evt.Evt_Eje);
    rg.MoverA(g_Img_Base.Img_Base_Nej, g_Evt.Evt_Nej);
    rg.MoverA(g_Img_Base.Img_Base_Tej, g_Evt.Evt_Tej);
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    rg.MoverA(g_Img_GtApe.GtApe_Fpr, g_MsgGT096.GT096_Fec);
  //rg.MoverA(g_Img_GtApe.GtApe_Vle, 0);
    rg.MoverA(g_Img_GtApe.GtApe_Rgf, "");
    rg.MoverA(g_Img_GtApe.GtApe_Gfu, "");
    rg.MoverA(g_Img_GtApe.GtApe_Ftc, 0);
    rg.MoverA(g_Img_GtApe.GtApe_Cbp, "");
    rg.MoverA(g_Img_GtApe.GtApe_Dbp, 0);
    rg.MoverA(g_Img_GtApe.GtApe_Tob, "E");
    rg.MoverA(g_Img_GtApe.GtApe_Plm, "N");
    rg.MoverA(g_Img_GtApe.GtApe_Ggr, g_PrmPC080.PC080_Ggr);
    rg.MoverA(g_Img_GtApe.GtApe_Dsg, "");
    rg.MoverA(g_Img_GtApe.GtApe_Cbt, "E");
    rg.MoverA(g_Img_GtApe.GtApe_Grd, "1");
    rg.MoverA(g_Img_GtApe.GtApe_Cmp, "N");
    rg.MoverA(g_Img_GtApe.GtApe_Sgr, "N");
    rg.MoverA(g_Img_GtApe.GtApe_Ctb, "N");
    rg.MoverA(g_Img_GtApe.GtApe_Ilm, "N");
    rg.MoverA(g_Img_GtApe.GtApe_Pcl, 0);
    rg.MoverA(g_Img_GtApe.GtApe_Mtl, 0);
    rg.MoverA(g_Img_GtApe.GtApe_Fig, g_Msg.Msg_Fch);
    rg.MoverA(g_Img_GtApe.GtApe_Tsd, "");
    rg.MoverA(g_Img_GtApe.GtApe_Fts, "");
    rg.MoverA(g_Img_GtApe.GtApe_Vuo, 0);
    rg.MoverA(g_Img_GtApe.GtApe_Vtt, 0);
    rg.MoverA(g_Img_GtApe.GtApe_Vts, 0);
    rg.MoverA(g_Img_GtApe.GtApe_Vtb, 0);
    rg.MoverA(g_Img_GtApe.GtApe_Vcn, 0);
    rg.MoverA(g_Img_GtApe.GtApe_Cbg, "");
    rg.MoverA(g_Img_GtApe.GtApe_Nct, "");
    rg.MoverA(g_Img_GtApe.GtApe_Fct, "");
    rg.MoverA(g_Img_GtApe.GtApe_Fvt, 0);
    rg.MoverA(g_Img_GtApe.GtApe_Vuf, rg.FmtValor(g_MsgGT096.GT096_Vuf,2,2,9,"+"));
    rg.MoverA(g_Img_GtApe.GtApe_Vus, rg.FmtValor(g_MsgGT096.GT096_Vtc,2,2,9,"+"));
    rg.MoverA(g_Img_GtApe.GtApe_Hvt, 0);
    rg.MoverA(g_Img_GtApe.GtApe_Hvl, 0);
    rg.MoverA(g_Img_GtApe.GtApe_Hvb, 0);
    rg.MoverA(g_Img_GtApe.GtApe_Hvc, 0);
    rg.MoverA(g_Img_GtApe.GtApe_Pvt, 0);
    rg.MoverA(g_Img_GtApe.GtApe_Pvl, 0);
    rg.MoverA(g_Img_GtApe.GtApe_Pvb, 0);
    rg.MoverA(g_Img_GtApe.GtApe_Pvc, 0);
    rg.MoverA(g_Img_GtApe.GtApe_Psw, "");
    rg.MoverA(g_Img_GtApe.GtApe_Psq, "");
    rg.MoverA(g_Img_GtApe.GtApe_Psd, "");
    rg.MoverA(g_Img_GtApe.GtApe_Idx, "");
    rg.MoverA(g_Img_GtApe.GtApe_Tfr, "");
    rg.MoverA(g_Img_GtApe.GtApe_Dpv, "");
    rg.MoverA(g_Img_GtApe.GtApe_Dpp, "");
    rg.MoverA(g_Img_GtApe.GtApe_Dsw, "");
    rg.MoverA(g_Img_GtApe.GtApe_Pct, "10000");
    rg.MoverA(g_Img_GtApe.GtApe_Ulm, "");
    rg.MoverA(g_Img_GtApe.GtApe_Fll, "");
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    rg.MoverA(g_Img_GtCma.GtCma_Fui, "");
    rg.MoverA(g_Img_GtCma.GtCma_Nmi, "");
    rg.MoverA(g_Img_GtCma.GtCma_Nml, "");
    rg.MoverA(g_Img_GtCma.GtCma_Lin, "");
    rg.MoverA(g_Img_GtCma.GtCma_Fev, g_Msg.Msg_Fch);
    rg.MoverA(g_Img_GtCma.GtCma_Cnt, "");
    rg.MoverA(g_Img_GtCma.GtCma_Vcn, "");
    rg.MoverA(g_Img_GtCma.GtCma_Dvf, "");
    rg.MoverA(g_Img_GtCma.GtCma_Fll, "");
  }
  //===============================================================================================================================
  public void Carga_de_Host(RunData data, Context context) throws Exception
  {
    g_MsgED100 = MSGED100.Inicia_MsgED100();
    rg.MoverA(g_MsgED100.ED100_Idr, "Q");
    rg.MoverA(g_MsgED100.ED100_Nev, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_Msg.Msg_Dat, MSGED100.LSet_De_MsgED100(g_MsgED100));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED500");
    g_MsgED100 = MSGED100.LSet_A_MsgED100(g_Msg.getDat());
    g_Evt = BF_EVT.LSet_A_Evt(g_MsgED100.getEvt());
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    rg.MoverA(g_PrmPC080.PC080_Dcn, g_Evt.Evt_Dcn);
    rg.MoverA(g_PrmPC080.PC080_Ndc, g_Evt.Evt_Ndc);
    rg.MoverA(g_PrmPC080.PC080_Tmn, g_Evt.Evt_Tmn);
    rg.MoverA(g_PrmPC080.PC080_Trj, g_Evt.Evt_Trj);
    rg.MoverA(g_PrmPC080.PC080_Dcm, g_Evt.Evt_Dcm);
    rg.MoverA(g_PrmPC080.PC080_Prp, g_Evt.Evt_Prp);
    rg.MoverA(g_PrmPC080.PC080_Ggr, g_Evt.Evt_Ggr);
    rg.MoverA(g_PrmPC080.PC080_Mnd, g_Evt.Evt_Mnd);
    rg.MoverA(g_PrmPC080.PC080_Nmn, g_Evt.Evt_Nmn);
    rg.MoverA(g_PrmPC080.PC080_Suc, g_Evt.Evt_Suc);
    rg.MoverA(g_PrmPC080.PC080_Nsu, g_Evt.Evt_Nsu);
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    Vector g_Tab_Img = img.Carga_Img_Host(g_Msg, g_PrmPC080.getNtr(), data, context);
    De_Tab_Img(g_Tab_Img, data);
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    rg.MoverA(g_MsgED135.ED135_Idr, "C");
    rg.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
    rg.MoverA(g_MsgED135.ED135_Trt, "UNICO");
    rg.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED535");
    g_MsgED135 = MSGED135.LSet_A_MsgED135(g_Msg.getDat());
  }
  //-------------------------------------------------------------------------------------------
  public void De_Tab_Img(Vector g_Tab_Img, RunData data)
  {
    g_Img       = img.Inicia_Img();
    lcData      = img.LSet_De_Img(g_Img);
    g_Img_Base  = img.LSet_A_ImgBase(lcData);
    g_Img_GtApe = gtimg.LSet_A_ImgGtApe(g_Img.getDat());
    g_Img_GtCma = gtimg.LSet_A_ImgGtCma(g_Img.getDat());
    for (i=0; i<g_Tab_Img.size(); i++)
        {
          lcData = img.LSet_De_Img((BF_IMG.Buf_Img)g_Tab_Img.elementAt(i));
          g_Img  = img.LSet_A_Img(lcData);
          if (g_Img.Img_Dax.toString().trim().equals("BASE"))
             {
               g_Img_GtApe = gtimg.LSet_A_ImgGtApe(g_Img.getDat());
               g_Img_Base  = img.LSet_A_ImgBase(g_Img_GtApe.getBse());
             }
          if (g_Img.Img_Dax.toString().trim().equals("ZZCMA"))
             {
               g_Img_GtCma = gtimg.LSet_A_ImgGtCma(g_Img.getDat());
             }
        }
  }
  //===============================================================================================================================
  public void Carga_Combos(RunData data, Context context) throws Exception
  {
    Vector v_Suc = (Vector)ScreenHelper.vecSuc();
    data.getUser().removeTemp("Agtp110_tGarSuc");
    data.getUser().setTemp("Agtp110_tGarSuc", v_Suc);
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    Vector v_Mnd = (Vector)ScreenHelper.vecMnd();
    data.getUser().removeTemp("Agtp110_tGarMnd");
    data.getUser().setTemp("Agtp110_tGarMnd", v_Mnd);
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //Datos.de.Productos.Contables
    g_MsgGT093 = MSGGT093.Inicia_MsgGT093();
    rg.MoverA(g_MsgGT093.GT093_Idr, "I");
    rg.MoverA(g_MsgGT093.GT093_Sis, g_PrmPC080.PC080_Cnr.Sis);
    rg.MoverA(g_MsgGT093.GT093_Dcn, g_PrmPC080.PC080_Dcn);
    rg.MoverA(g_MsgGT093.GT093_Itb, "V");
    rg.MoverA(g_Msg.Msg_Dat, MSGGT093.LSet_De_MsgGT093(g_MsgGT093));
    g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GT093");
    g_MsgGT093 = MSGGT093.LSet_A_MsgGT093(g_Msg.getDat());
    data.getUser().removeTemp("Agtp110_tGarDpc");
    data.getUser().setTemp("Agtp110_tGarDpc", g_MsgGT093);
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //Cuentas.del.Cliente.para.Linea.CMA
    Vector v_Cct = ScreenHelper.Consulta_CtasCtes(g_PrmPC080.getCli());
    data.getUser().removeTemp("Agtp110_tGarCct");
    data.getUser().setTemp("Agtp110_tGarCct", v_Cct);
  }
  //-------------------------------------------------------------------------------------------
  public void Limpieza(RunData data, Context context) throws Exception
  {
    data.getUser().removeTemp("Agtp110_PrmPC080");
    data.getUser().removeTemp("Agtp110_MsgED135");
    data.getUser().removeTemp("Agtp110_Evt");
    data.getUser().removeTemp("Agtp110_ImgBase");
    data.getUser().removeTemp("Agtp110_ImgGtApe");
    data.getUser().removeTemp("Agtp110_ImgGtCma");
    data.getUser().removeTemp("Agtp110_tGarSuc");
    data.getUser().removeTemp("Agtp110_tGarMnd");
    data.getUser().removeTemp("Agtp110_tGarDpc");
    data.getUser().removeTemp("Agtp110_tGarCct");
  }
  //===============================================================================================================================
}