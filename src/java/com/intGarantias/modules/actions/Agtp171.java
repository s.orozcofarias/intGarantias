// Source File Name:   Agtp171.java

package com.intGarantias.modules.actions;

import java.util.Vector;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.FHTServlet.modules.actions.FHTServletAction;
import com.FHTServlet.modules.global.BF_EVT;
import com.FHTServlet.modules.global.BF_IMG;
import com.FHTServlet.modules.global.BF_MSG;
import com.FHTServlet.modules.global.PRMPC080;
import com.FHTServlet.modules.global.RUTGEN;
import com.intGarantias.modules.global.GT_IMG;
import com.intGarantias.modules.global.MSGGT171;
import com.intGlobal.modules.global.MSGED090;
import com.intGlobal.modules.global.MSGED100;
import com.intGlobal.modules.global.MSGED135;
import com.intGlobal.modules.global.PRMED210;

public class Agtp171 extends FHTServletAction {

	public Agtp171() {
		i = 0;
		lcData = "";
		rg = new RUTGEN();
		msg = new BF_MSG();
		g_Msg = new com.FHTServlet.modules.global.BF_MSG.Buf_Msg();
		g_Evt = new com.FHTServlet.modules.global.BF_EVT.Buf_Evt();
		img = new BF_IMG();
		g_Img = new com.FHTServlet.modules.global.BF_IMG.Buf_Img();
		g_Img_Base = new com.FHTServlet.modules.global.BF_IMG.Buf_Img_Bse();
		gtimg = new GT_IMG();
		g_Img_GtAlz = new com.intGarantias.modules.global.GT_IMG.Buf_Img_GtAlz();
		g_Img_GtAld = new com.intGarantias.modules.global.GT_IMG.Buf_Img_GtAld();
		v_Img_GtAld = new Vector();
		g_Img_GtApe = new com.intGarantias.modules.global.GT_IMG.Buf_Img_GtApe();
		v_Img_GtAcr = new Vector();
		g_PrmPC080 = new com.FHTServlet.modules.global.PRMPC080.PrmPC080();
		g_MsgED090 = new com.intGlobal.modules.global.MSGED090.Buf_MsgED090();
		g_MsgED100 = new com.intGlobal.modules.global.MSGED100.Buf_MsgED100();
		g_MsgED135 = new com.intGlobal.modules.global.MSGED135.Buf_MsgED135();
		g_PrmED210 = new com.intGlobal.modules.global.PRMED210.Buf_PrmED210();
		g_MsgGT171 = new com.intGarantias.modules.global.MSGGT171.Buf_MsgGT171();
	}

	public void doPerform(RunData data, Context context) throws Exception {
		g_Msg = BF_MSG.Init_Program("Agtp171-DIN", data);
		if (g_Msg.Msg_Pgr.toString().equals("RT"))
			doAgtp171_Continue(data, context);
		else
			doAgtp171_Init(data, context);
	}

	public void doCancel(RunData data, Context context) throws Exception {
		setTemplate(data, "Garantias,Agt,AGTP171.vm");
	}

	public void doAgtp171_Init(RunData data, Context context) throws Exception {
		g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
		g_MsgED135 = MSGED135.Inicia_MsgED135();
		if (g_PrmPC080.PC080_Ntr.toString().trim().compareTo("") != 0) {
			Recupera_Evt(data, context);
			Carga_de_Host(data, context);
			if (v_Img_GtAld.size() == 0) {
				RUTGEN.MoverA(g_Img_GtAlz.GtAlz_Idx, "");
				RUTGEN.MoverA(g_Img_GtAlz.GtAlz_Dsg, "");
				RUTGEN.MoverA(g_Img_GtAlz.GtAlz_Suc, "");
				RUTGEN.MoverA(g_Img_GtAlz.GtAlz_Mnd, "");
				RUTGEN.MoverA(g_Img_GtAlz.GtAlz_Vcn, "");
				RUTGEN.MoverA(g_Img_GtAlz.GtAlz_Ggr, "");
				RUTGEN.MoverA(g_Img_GtAlz.GtAlz_Fpr, "");
				RUTGEN.MoverA(g_Img_GtAlz.GtAlz_Fig, "");
				RUTGEN.MoverA(g_Img_GtAlz.GtAlz_Cbt, "");
				RUTGEN.MoverA(g_Img_GtAlz.GtAlz_Fll, "");
				Validar_Host("V", data, context);
			}
			RUTGEN.MoverA(g_PrmPC080.PC080_Ntr, g_Img_Base.Img_Base_Ntr);
			RUTGEN.MoverA(g_PrmPC080.PC080_Trt, g_MsgED135.ED135_Trt);
			RUTGEN.MoverA(g_PrmPC080.PC080_Cli, g_Img_Base.Img_Base_Cli);
			RUTGEN.MoverA(g_PrmPC080.PC080_Ncl, g_Img_Base.Img_Base_Ncl);
			RUTGEN.MoverA(g_PrmPC080.PC080_Ndc, g_Img_Base.Img_Base_Ndc);
		} else {
			Carga_Inicial(data, context);
			Init_Evt_GRT(data, context);
		}
		data.getUser().removeTemp("Agtp171_MsgED135");
		data.getUser().setTemp("Agtp171_MsgED135", g_MsgED135);
		data.getUser().removeTemp("Agtp171_Evt");
		data.getUser().setTemp("Agtp171_Evt", g_Evt);
		data.getUser().removeTemp("Agtp171_ImgBase");
		data.getUser().setTemp("Agtp171_ImgBase", g_Img_Base);
		data.getUser().removeTemp("Agtp171_ImgGtAlz");
		data.getUser().setTemp("Agtp171_ImgGtAlz", g_Img_GtAlz);
		data.getUser().removeTemp("Agtp171_vImgGtAld");
		data.getUser().setTemp("Agtp171_vImgGtAld", v_Img_GtAld);
		data.getUser().removeTemp("Agtp171_vImgGtAcr");
		data.getUser().setTemp("Agtp171_vImgGtAcr", v_Img_GtAcr);
		RUTGEN.MoverA(g_PrmPC080.PC080_Cli, g_Evt.Evt_Cli);
		RUTGEN.MoverA(g_PrmPC080.PC080_Ncl, g_Evt.Evt_Ncl);
		RUTGEN.MoverA(g_PrmPC080.PC080_Dir, g_Evt.Evt_Dir);
		RUTGEN.MoverA(g_PrmPC080.PC080_Tfc, g_Evt.Evt_Tfc);
		RUTGEN.MoverA(g_PrmPC080.PC080_Dcn, g_Evt.Evt_Dcn);
		RUTGEN.MoverA(g_PrmPC080.PC080_Ndc, g_Evt.Evt_Ndc);
		RUTGEN.MoverA(g_PrmPC080.PC080_Ntt, g_Evt.Evt_Ntt);
		data.getUser().removeTemp("Agtp171_PrmPC080");
		data.getUser().setTemp("Agtp171_PrmPC080", g_PrmPC080);
		RUTGEN.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
		BF_MSG.Param_Program(data, g_Msg);
		setTemplate(data, "Garantias,Agt,AGTP171.vm");
	}

	public void doAgtp171(RunData data, Context context) throws Exception {
		g_Msg = BF_MSG.Init_Program("Agtp171-MAN", data);
		g_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
		String Opc = data.getParameters().getString("Opc", "");
		if (Opc.trim().equals("S")) {
			data.getUser().removeTemp("Agtp171_vImgGtAld");
			data.getUser().removeTemp("Agtp171_ImgGtAlz");
			data.getUser().removeTemp("Agtp171_ImgBase");
			data.getUser().removeTemp("Agtp171_Evt");
			data.getUser().removeTemp("Agtp171_PrmPC080");
			data.getUser().removeTemp("Agtp171_vImgGtAcr");
			RUTGEN.MoverA(g_PrmPC080.PC080_Rtn, "NK");
			RUTGEN.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
			BF_MSG.Return_Data("Agtp171", data, g_Msg);
			return;
		}
		if (Opc.trim().equals("R")) {
			g_MsgED135 = (com.intGlobal.modules.global.MSGED135.Buf_MsgED135) data
					.getUser().getTemp("Agtp171_MsgED135");
			g_PrmED210 = PRMED210.Inicia_PrmED210();
			RUTGEN.MoverA(g_PrmED210.ED210_Rtn, "RO");
			RUTGEN.MoverA(g_PrmED210.ED210_Ldt, g_MsgED135.ED135_Ldv);
			data.getUser().removeTemp("Agtp171_g_PrmPC080");
			data.getUser().setTemp("Agtp171_g_PrmPC080", g_PrmPC080);
			RUTGEN.MoverA(g_Msg.Msg_Dat, PRMED210.LSet_De_PrmED210(g_PrmED210));
			BF_MSG.Link_Program("Agtp171", "Aedp210", data, g_Msg);
			return;
		}
		if (Opc.equals("T")) {
			g_Img_GtAlz = (com.intGarantias.modules.global.GT_IMG.Buf_Img_GtAlz) data
					.getUser().getTemp("Agtp171_ImgGtAlz");
			String lcDcn = "CRRES";
			if (g_Img_GtAlz.GtAlz_Rcr.toString().equals("B"))
				lcDcn = "CARES";
			if (g_Img_GtAlz.GtAlz_Rcr.toString().equals("N"))
				lcDcn = "CAREN";
			if (g_Img_GtAlz.GtAlz_Rcr.toString().equals("B")
					|| g_Img_GtAlz.GtAlz_Rcr.toString().equals("N")) {
				generaBaseA(data, context);
				RUTGEN.MoverA(g_Img_GtAlz.GtAlz_Rcr, "Y");
				data.getUser().removeTemp("Agtp171_ImgGtAlz");
				data.getUser().setTemp("Agtp171_ImgGtAlz", g_Img_GtAlz);
			}
			v_Img_GtAcr = (Vector) data.getUser().getTemp("Agtp171_vImgGtAcr");
			data.getUser().removeTemp("Agtp305_gTabImg");
			data.getUser().setTemp("Agtp305_gTabImg", v_Img_GtAcr);
			data.getUser().removeTemp("Agtp171_g_PrmPC080");
			data.getUser().setTemp("Agtp171_g_PrmPC080", g_PrmPC080);
			RUTGEN.MoverA(g_PrmPC080.PC080_Dcn, lcDcn);
			RUTGEN.MoverA(g_PrmPC080.PC080_Ndc, "CARTA DE RESGUARDO");
			RUTGEN.MoverA(g_PrmPC080.PC080_Est, "CRALZ");
			RUTGEN.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
			BF_MSG.Link_Program("Agtp171", "Agtp305", data, g_Msg);
			return;
		}
		if (Opc.trim().equals("G")) {
			v_Img_GtAcr = (Vector) data.getUser().getTemp("Agtp171_vImgGtAcr");
			v_Img_GtAld = (Vector) data.getUser().getTemp("Agtp171_vImgGtAld");
			g_Img_GtAlz = (com.intGarantias.modules.global.GT_IMG.Buf_Img_GtAlz) data
					.getUser().getTemp("Agtp171_ImgGtAlz");
			RUTGEN.MoverA(g_Img_GtAlz.GtAlz_Fig, g_Msg.Msg_Fch);
			RUTGEN.MoverA(g_Img_GtAlz.GtAlz_Vcn,
					RUTGEN.Zeros(g_Img_GtAlz.GtAlz_Vcn));
			RUTGEN.MoverA(g_Img_GtAlz.GtAlz_Vts,
					RUTGEN.Zeros(g_Img_GtAlz.GtAlz_Vts));
			RUTGEN.MoverA(g_Img_GtAlz.GtAlz_Rcr, data.getParameters()
					.getString("Rcr", ""));
			for (i = 0; i < v_Img_GtAld.size(); i++) {
				g_Img_GtAld = (com.intGarantias.modules.global.GT_IMG.Buf_Img_GtAld) v_Img_GtAld
						.elementAt(i);
				if (g_Img_GtAld.GtAld_Est.toString().equals("ALZMT")) {
					RUTGEN.MoverA(g_Img_GtAlz.GtAlz_Vcn, RUTGEN.Suma(
							g_Img_GtAlz.GtAlz_Vcn, 2, g_Img_GtAld.GtAld_Vcn, 2,
							15, 2));
					RUTGEN.MoverA(g_Img_GtAlz.GtAlz_Vts, RUTGEN.Suma(
							g_Img_GtAlz.GtAlz_Vts, 2, g_Img_GtAld.GtAld_Vts, 2,
							15, 2));
				}
			}

			Vector g_Tab_Img = A_Tab_Img(data);
			img.Graba_Img_Host(g_Msg, g_Tab_Img, data, context);
			g_MsgED135 = MSGED135.Inicia_MsgED135();
			RUTGEN.MoverA(g_MsgED135.ED135_Idr, "M");
			RUTGEN.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
			RUTGEN.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
			RUTGEN.MoverA(g_MsgED135.ED135_Est, "");
			RUTGEN.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
			RUTGEN.MoverA(g_MsgED135.ED135_Vlc, g_Img_GtAlz.GtAlz_Vcn);
			if (g_Img_GtAlz.GtAlz_Rcr.toString().equals("B")
					|| g_Img_GtAlz.GtAlz_Rcr.toString().equals("N"))
				RUTGEN.MoverA(g_MsgED135.ED135_Ezt, "ENOPR");
			else
				RUTGEN.MoverA(g_MsgED135.ED135_Ezt, "TERMI");
			RUTGEN.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
			g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
			data.getUser().removeTemp("Agtp171_PrmPC080");
			RUTGEN.MoverA(g_PrmPC080.PC080_Rtn, "OO");
			RUTGEN.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
			BF_MSG.Return_Data("Agtp171", data, g_Msg);
			return;
		}
		if (Opc.trim().equals("B")) {
			g_Img_GtAlz = (com.intGarantias.modules.global.GT_IMG.Buf_Img_GtAlz) data
					.getUser().getTemp("Agtp171_ImgGtAlz");
			if (!g_Img_GtAlz.GtAlz_Rcr.toString().equals("I")) {
				data.getUser().removeTemp("Agtp171_g_PrmPC080");
				data.getUser().setTemp("Agtp171_g_PrmPC080", g_PrmPC080);
				BF_MSG.MsgInt("Agtp171", data, g_Msg,
						"Debe Ingresar Carta de Resguardo", "1");
				return;
			} else {
				v_Img_GtAcr = (Vector) data.getUser().getTemp(
						"Agtp171_vImgGtAcr");
				v_Img_GtAld = (Vector) data.getUser().getTemp(
						"Agtp171_vImgGtAld");
				Vector g_Tab_Img = A_Tab_Img(data);
				img.Graba_Img_Host(g_Msg, g_Tab_Img, data, context);
				g_MsgED135 = MSGED135.Inicia_MsgED135();
				RUTGEN.MoverA(g_MsgED135.ED135_Idr, "M");
				RUTGEN.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
				RUTGEN.MoverA(g_MsgED135.ED135_Trt, g_PrmPC080.PC080_Trt);
				RUTGEN.MoverA(g_MsgED135.ED135_Est, "");
				RUTGEN.MoverA(g_MsgED135.ED135_Fen, g_Msg.Msg_Fch);
				RUTGEN.MoverA(g_MsgED135.ED135_Vlc, g_Img_GtAlz.GtAlz_Vcn);
				RUTGEN.MoverA(g_MsgED135.ED135_Ezt, "TERMI");
				RUTGEN.MoverA(g_Msg.Msg_Dat,
						MSGED135.LSet_De_MsgED135(g_MsgED135));
				g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED135");
				data.getUser().removeTemp("Agtp171_PrmPC080");
				RUTGEN.MoverA(g_PrmPC080.PC080_Rtn, "OO");
				RUTGEN.MoverA(g_Msg.Msg_Dat,
						PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
				BF_MSG.Return_Data("Agtp171", data, g_Msg);
				return;
			}
		}
		if (Opc.trim().equals("K")) {
			String Sel = data.getParameters().getString("Sel", "");
			String Seq = data.getParameters().getString("Seq", "");
			i = Integer.parseInt(Seq);
			v_Img_GtAld = (Vector) data.getUser().getTemp("Agtp171_vImgGtAld");
			g_Img_GtAlz = (com.intGarantias.modules.global.GT_IMG.Buf_Img_GtAlz) data
					.getUser().getTemp("Agtp171_ImgGtAlz");
			g_Img_Base = BF_IMG
					.LSet_A_ImgBase(g_Img_GtAlz.GtAlz_Bse.toString());
			g_Img_GtAld = (com.intGarantias.modules.global.GT_IMG.Buf_Img_GtAld) v_Img_GtAld
					.elementAt(i);
			RUTGEN.MoverA(g_Img_GtAlz.GtAlz_Rcr, data.getParameters()
					.getString("Rcr", ""));
			if (Sel.equals("A")) {
				RUTGEN.MoverA(g_Img_GtAld.GtAld_Est, "ALZMT");
				RUTGEN.MoverA(g_Img_GtAlz.GtAlz_Vts, RUTGEN.Suma(
						g_Img_GtAlz.GtAlz_Vts, 2, g_Img_GtAld.GtAld_Vts, 2, 15,
						2));
				RUTGEN.MoverA(g_Img_GtAlz.GtAlz_Vcn, RUTGEN.Suma(
						g_Img_GtAlz.GtAlz_Vcn, 2, g_Img_GtAld.GtAld_Vcn, 2, 15,
						2));
			} else {
				RUTGEN.MoverA(g_Img_GtAld.GtAld_Est, "NUEVA");
				RUTGEN.MoverA(g_Img_GtAlz.GtAlz_Vts, RUTGEN.Resta(
						g_Img_GtAlz.GtAlz_Vts, 2, g_Img_GtAld.GtAld_Vts, 2, 15,
						2));
				RUTGEN.MoverA(g_Img_GtAlz.GtAlz_Vcn, RUTGEN.Resta(
						g_Img_GtAlz.GtAlz_Vcn, 2, g_Img_GtAld.GtAld_Vcn, 2, 15,
						2));
			}
			v_Img_GtAld.set(i, g_Img_GtAld);
			data.getUser().removeTemp("Agtp171_ImgGtAlz");
			data.getUser().setTemp("Agtp171_ImgGtAlz", g_Img_GtAlz);
			data.getUser().removeTemp("Agtp171_vImgGtAld");
			data.getUser().setTemp("Agtp171_vImgGtAld", v_Img_GtAld);
			setTemplate(data, "Garantias,Agt,AGTP171.vm");
			return;
		} else {
			return;
		}
	}

	public void Recupera_Evt(RunData data, Context context) throws Exception {
		g_MsgED100 = MSGED100.Inicia_MsgED100();
		RUTGEN.MoverA(g_MsgED100.ED100_Idr, "Q");
		RUTGEN.MoverA(g_MsgED100.ED100_Nev, g_PrmPC080.PC080_Ntr);
		RUTGEN.MoverA(g_Msg.Msg_Dat, MSGED100.LSet_De_MsgED100(g_MsgED100));
		g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED500");
		g_MsgED100 = MSGED100.LSet_A_MsgED100(g_Msg.Msg_Dat.toString());
		g_Evt = BF_EVT.LSet_A_Evt(g_MsgED100.ED100_Evt.toString());
	}

	public void Carga_de_Host(RunData data, Context context) throws Exception {
		String lcSvc = "";
		Vector g_Tab_Img = img.Carga_Img_Host(g_Msg,
				g_PrmPC080.PC080_Ntr.toString(), data, context);
		De_Tab_Img(g_Tab_Img, data);
		RUTGEN.MoverA(g_MsgED135.ED135_Idr, "C");
		RUTGEN.MoverA(g_MsgED135.ED135_Nev, g_PrmPC080.PC080_Ntr);
		RUTGEN.MoverA(g_MsgED135.ED135_Trt, "ALZMT");
		lcSvc = "ED535";
		RUTGEN.MoverA(g_Msg.Msg_Dat, MSGED135.LSet_De_MsgED135(g_MsgED135));
		g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, lcSvc);
		g_MsgED135 = MSGED135.LSet_A_MsgED135(g_Msg.Msg_Dat.toString());
	}

	public void De_Tab_Img(Vector g_Tab_Img, RunData data) {
		g_Img = BF_IMG.Inicia_Img();
		lcData = BF_IMG.LSet_De_Img(g_Img);
		g_Img_Base = BF_IMG.LSet_A_ImgBase(lcData);
		g_Img_GtAlz = GT_IMG.LSet_A_ImgGtAlz(g_Img.Img_Dat.toString());
		v_Img_GtAld.clear();
		v_Img_GtAcr.clear();
		for (i = 0; i < g_Tab_Img.size(); i++) {
			lcData = BF_IMG
					.LSet_De_Img((com.FHTServlet.modules.global.BF_IMG.Buf_Img) g_Tab_Img
							.elementAt(i));
			g_Img = BF_IMG.LSet_A_Img(lcData);
			if (g_Img.Img_Dax.toString().trim().equals("BASE")) {
				g_Img_GtAlz = GT_IMG.LSet_A_ImgGtAlz(g_Img.Img_Dat.toString());
				g_Img_Base = BF_IMG.LSet_A_ImgBase(g_Img_GtAlz.GtAlz_Bse
						.toString());
			}
			if (g_Img.Img_Dax.toString().trim().equals("DTALZ")) {
				g_Img_GtAld = GT_IMG.LSet_A_ImgGtAld(g_Img.Img_Dat.toString());
				v_Img_GtAld.add(g_Img_GtAld);
			}
			if (!g_Img.Img_Dax.toString().trim().equals("BASE")
					&& !g_Img.Img_Dax.toString().trim().equals("DTALZ")) {
				if (g_Img.Img_Dax.toString().trim().equals("BASEA"))
					RUTGEN.MoverA(g_Img.Img_Dax, "BASE");
				v_Img_GtAcr.add(g_Img);
			}
		}

	}

	public void Carga_Inicial(RunData data, Context context) throws Exception {
		g_Img = BF_IMG.Inicia_Img();
		lcData = BF_IMG.LSet_De_Img(g_Img);
		g_Img_Base = BF_IMG.LSet_A_ImgBase(lcData);
		g_Img_GtAlz = GT_IMG.LSet_A_ImgGtAlz(g_Img.Img_Dat.toString());
		RUTGEN.MoverA(g_Img_Base.Img_Base_Ttr, "ALZMT");
		RUTGEN.MoverA(g_Img_Base.Img_Base_Sis, g_PrmPC080.PC080_Cnr.Sis);
		RUTGEN.MoverA(g_Img_Base.Img_Base_Ncn, g_PrmPC080.PC080_Cnr.Ncn);
		Validar_Host("N", data, context);
		RUTGEN.MoverA(g_PrmPC080.PC080_Ntr, g_Img_Base.Img_Base_Ntr);
	}

	public void Validar_Host(String pcIdr, RunData data, Context context)
			throws Exception {
		com.intGarantias.modules.global.MSGGT171.Bff_MsgGT171 d_MsgGT171 = new com.intGarantias.modules.global.MSGGT171.Bff_MsgGT171();
		boolean liSwtFin = false;
		v_Img_GtAld.clear();
		v_Img_GtAcr.clear();
		g_MsgGT171 = MSGGT171.Inicia_MsgGT171();
		while (!liSwtFin) {
			Valida_Host(pcIdr, data, context);
			int p = 0;
			if (pcIdr.equals("S"))
				p = 1;
			for (i = p; i < MSGGT171.g_Max_GT171; i++) {
				d_MsgGT171 = (com.intGarantias.modules.global.MSGGT171.Bff_MsgGT171) g_MsgGT171.GT171_Alz
						.elementAt(i);
				if (d_MsgGT171.GtAlz_Seq.toString().trim().equals("")) {
					liSwtFin = true;
					break;
				}
				com.intGarantias.modules.global.GT_IMG.Buf_Img_GtAld g_Img_GtAld = new com.intGarantias.modules.global.GT_IMG.Buf_Img_GtAld();
				RUTGEN.MoverA(g_Img_GtAld.GtAld_Seq, d_MsgGT171.GtAlz_Seq);
				RUTGEN.MoverA(g_Img_GtAld.GtAld_Itb, d_MsgGT171.GtAlz_Itb);
				RUTGEN.MoverA(g_Img_GtAld.GtAld_Dsb, d_MsgGT171.GtAlz_Dsb);
				RUTGEN.MoverA(g_Img_GtAld.GtAld_Vts, d_MsgGT171.GtAlz_Vts);
				RUTGEN.MoverA(g_Img_GtAld.GtAld_Vcn, d_MsgGT171.GtAlz_Vcn);
				RUTGEN.MoverA(g_Img_GtAld.GtAld_Est, d_MsgGT171.GtAlz_Est);
				RUTGEN.MoverA(g_Img_GtAld.GtAld_Sqb, d_MsgGT171.GtAlz_Sqb);
				RUTGEN.MoverA(g_Img_GtAld.GtAld_Fll, "");
				v_Img_GtAld.add(g_Img_GtAld);
			}

			String Sqb = g_MsgGT171.GT171_Sqb.toString();
			g_MsgGT171 = MSGGT171.Inicia_MsgGT171();
			RUTGEN.MoverA(g_MsgGT171.GT171_Idt, "S");
			RUTGEN.MoverA(g_MsgGT171.GT171_Sqb, Sqb);
			g_MsgGT171.GT171_Alz.set(0, d_MsgGT171);
			pcIdr = "S";
		}
	}

	public void Valida_Host(String pcIdr, RunData data, Context context)
			throws Exception {
		if (pcIdr.equals("N"))
			RUTGEN.MoverA(g_Img_GtAlz.GtAlz_Bse,
					BF_IMG.LSet_De_ImgBase_Bse(g_Img_Base));
		RUTGEN.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtAlz(g_Img_GtAlz));
		RUTGEN.MoverA(g_MsgGT171.GT171_Idr, pcIdr);
		RUTGEN.MoverA(g_MsgGT171.GT171_Img, g_Img.Img_Dat);
		RUTGEN.MoverA(g_Msg.Msg_Dat, MSGGT171.LSet_De_MsgGT171(g_MsgGT171));
		g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "GT171");
		g_MsgGT171 = MSGGT171.LSet_A_MsgGT171(g_Msg.Msg_Dat.toString());
		RUTGEN.MoverA(g_Img.Img_Dat, g_MsgGT171.GT171_Img);
		g_Img_GtAlz = GT_IMG.LSet_A_ImgGtAlz(g_Img.Img_Dat.toString());
		if (pcIdr.equals("N")) {
			g_Img_Base = BF_IMG
					.LSet_A_ImgBase(g_Img_GtAlz.GtAlz_Bse.toString());
			RUTGEN.MoverA(g_Img_Base.Img_Base_Suc, g_Msg.Msg_Suc);
			RUTGEN.MoverA(g_Img_Base.Img_Base_Nsu, g_Msg.Msg_Nsu);
			RUTGEN.MoverA(g_Img_Base.Img_Base_Eje, g_Msg.Msg_Cus);
			RUTGEN.MoverA(g_Img_Base.Img_Base_Nej, g_Msg.Msg_Nus);
			RUTGEN.MoverA(g_Img_Base.Img_Base_Tej, g_Msg.Msg_Fus);
			RUTGEN.MoverA(g_Img_GtAlz.GtAlz_Bse,
					BF_IMG.LSet_De_ImgBase_Bse(g_Img_Base));
		}
		RUTGEN.MoverA(g_Img_GtAlz.GtAlz_Fpr, g_Msg.Msg_Fch);
		RUTGEN.MoverA(g_Img_GtAlz.GtAlz_Fig, g_Msg.Msg_Fch);
		RUTGEN.MoverA(g_Img_GtAlz.GtAlz_Vts,
				RUTGEN.Zeros(g_Img_GtAlz.GtAlz_Vts));
		RUTGEN.MoverA(g_Img_GtAlz.GtAlz_Vcn,
				RUTGEN.Zeros(g_Img_GtAlz.GtAlz_Vcn));
	}

	public void Init_Evt_GRT(RunData data, Context context) throws Exception {
		RUTGEN.MoverA(g_Evt.Evt_Ttr, g_Img_Base.Img_Base_Ttr);
		RUTGEN.MoverA(g_Evt.Evt_Ntt, g_Img_Base.Img_Base_Ntt);
		RUTGEN.MoverA(g_Evt.Evt_Tsl, g_Img_Base.Img_Base_Tsl);
		RUTGEN.MoverA(g_Evt.Evt_Slc, g_Img_Base.Img_Base_Slc);
		RUTGEN.MoverA(g_Evt.Evt_Cli, g_Img_Base.Img_Base_Cli);
		RUTGEN.MoverA(g_Evt.Evt_Ncl, g_Img_Base.Img_Base_Ncl);
		RUTGEN.MoverA(g_Evt.Evt_Tpr, g_Img_Base.Img_Base_Tpr);
		RUTGEN.MoverA(g_Evt.Evt_Dir, g_Img_Base.Img_Base_Dir);
		RUTGEN.MoverA(g_Evt.Evt_Cmn, " ");
		RUTGEN.MoverA(g_Evt.Evt_Tfc, g_Img_Base.Img_Base_Tfc);
		RUTGEN.MoverA(g_Evt.Evt_Sis, g_Img_Base.Img_Base_Sis);
		RUTGEN.MoverA(g_Evt.Evt_Ncn, g_Img_Base.Img_Base_Ncn);
		RUTGEN.MoverA(g_Evt.Evt_Dcn, g_Img_Base.Img_Base_Dcn);
		RUTGEN.MoverA(g_Evt.Evt_Ndc, g_Img_Base.Img_Base_Ndc);
		RUTGEN.MoverA(g_Evt.Evt_Tmn, g_Img_Base.Img_Base_Tmn);
		RUTGEN.MoverA(g_Evt.Evt_Trj, g_Img_Base.Img_Base_Trj);
		RUTGEN.MoverA(g_Evt.Evt_Dcm, g_Img_Base.Img_Base_Dcm);
		RUTGEN.MoverA(g_Evt.Evt_Prp, g_Img_Base.Img_Base_Prp);
		RUTGEN.MoverA(g_Evt.Evt_Suc, g_Img_Base.Img_Base_Suc);
		RUTGEN.MoverA(g_Evt.Evt_Nsu, g_Img_Base.Img_Base_Nsu);
		RUTGEN.MoverA(g_Evt.Evt_Mnd, g_Img_Base.Img_Base_Mnd);
		RUTGEN.MoverA(g_Evt.Evt_Nmn, g_Img_Base.Img_Base_Nmn);
		RUTGEN.MoverA(g_Evt.Evt_Eje, g_Img_Base.Img_Base_Eje);
		RUTGEN.MoverA(g_Evt.Evt_Nej, g_Img_Base.Img_Base_Nej);
		RUTGEN.MoverA(g_Evt.Evt_Tej, g_Img_Base.Img_Base_Tej);
		RUTGEN.MoverA(g_Evt.Evt_Est, "NUEVA");
		RUTGEN.MoverA(g_Evt.Evt_Fst, g_Msg.Msg_Fch);
		RUTGEN.MoverA(g_Evt.Evt_Upr, g_Img_Base.Img_Base_Trj);
		RUTGEN.MoverA(g_Evt.Evt_Vpr, RUTGEN.Zeros(g_Evt.Evt_Vpr));
		RUTGEN.MoverA(g_Evt.Evt_Fpr, g_Msg.Msg_Fch);
		RUTGEN.MoverA(g_Evt.Evt_Spr, RUTGEN.Zeros(g_Evt.Evt_Spr));
		RUTGEN.MoverA(g_Evt.Evt_Tpp, " ");
		RUTGEN.MoverA(g_Evt.Evt_Ggr, g_Img_GtAlz.GtAlz_Ggr);
		RUTGEN.MoverA(g_Evt.Evt_Fll, " ");
	}

	public Vector A_Tab_Img(RunData data) {
		Vector v_Img = new Vector();
		v_Img.clear();
		RUTGEN.MoverA(g_Img.Img_Ntr, g_PrmPC080.PC080_Ntr);
		RUTGEN.MoverA(g_Img.Img_Dax, "BASE");
		RUTGEN.MoverA(g_Img.Img_Seq, "000");
		RUTGEN.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtAlz(g_Img_GtAlz));
		Vector vPaso = BF_IMG.LSet_A_vImg(BF_IMG.LSet_De_Img(g_Img));
		v_Img.add((com.FHTServlet.modules.global.BF_IMG.Buf_Img) vPaso
				.elementAt(0));
		RUTGEN.MoverA(g_Img.Img_Seq, "000");
		for (i = 0; i < v_Img_GtAld.size(); i++) {
			g_Img_GtAld = (com.intGarantias.modules.global.GT_IMG.Buf_Img_GtAld) v_Img_GtAld
					.elementAt(i);
			RUTGEN.MoverA(g_Img.Img_Dax, "DTALZ");
			RUTGEN.MoverA(g_Img.Img_Seq,
					RUTGEN.Suma(g_Img.Img_Seq, 0, "1", 0, 3, 0));
			RUTGEN.MoverA(g_Img.Img_Dat, gtimg.LSet_De_ImgGtAld(g_Img_GtAld));
			vPaso = BF_IMG.LSet_A_vImg(BF_IMG.LSet_De_Img(g_Img));
			v_Img.add((com.FHTServlet.modules.global.BF_IMG.Buf_Img) vPaso
					.elementAt(0));
		}

		RUTGEN.MoverA(g_Img.Img_Seq, "000");
		for (i = 0; i < v_Img_GtAcr.size(); i++) {
			g_Img = (com.FHTServlet.modules.global.BF_IMG.Buf_Img) v_Img_GtAcr
					.elementAt(i);
			RUTGEN.MoverA(g_Img.Img_Ntr, g_PrmPC080.PC080_Ntr);
			if (g_Img.Img_Dax.toString().trim().equals("BASE"))
				RUTGEN.MoverA(g_Img.Img_Dax, "BASEA");
			RUTGEN.MoverA(g_Img.Img_Seq,
					RUTGEN.Suma(g_Img.Img_Seq, 0, "1", 0, 3, 0));
			vPaso = BF_IMG.LSet_A_vImg(BF_IMG.LSet_De_Img(g_Img));
			v_Img.add((com.FHTServlet.modules.global.BF_IMG.Buf_Img) vPaso
					.elementAt(0));
		}

		return v_Img;
	}

	public void Cursatura(String pcIdr, Vector g_Tab_Img, RunData data,
			Context context) throws Exception {
		g_Evt = (com.FHTServlet.modules.global.BF_EVT.Buf_Evt) data.getUser()
				.getTemp("Agtp171_Evt");
		RUTGEN.MoverA(g_Evt.Evt_Est, "ENTMT");
		RUTGEN.MoverA(g_Evt.Evt_Fpr, g_Msg.Msg_Fch);
		BF_EVT t = new BF_EVT();
		g_MsgED100 = MSGED100.Inicia_MsgED100();
		RUTGEN.MoverA(g_MsgED100.ED100_Idr, pcIdr);
		RUTGEN.MoverA(g_MsgED100.ED100_Nev, g_PrmPC080.PC080_Ntr);
		RUTGEN.MoverA(g_MsgED100.ED100_Evt, t.LSet_De_Evt(g_Evt));
		RUTGEN.MoverA(g_Msg.Msg_Dat, MSGED100.LSet_De_MsgED100(g_MsgED100));
		g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED500");
	}

	public void doAgtp171_Continue(RunData data, Context context)
			throws Exception {
		if (RUTGEN.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Aedp210")) {
			g_PrmED210 = PRMED210.LSet_A_PrmED210(g_Msg.Msg_Dat.toString());
			g_PrmPC080 = (com.FHTServlet.modules.global.PRMPC080.PrmPC080) data
					.getUser().getTemp("Agtp171_g_PrmPC080");
			data.getUser().removeTemp("Agtp171_g_PrmPC080");
			RUTGEN.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
			BF_MSG.Param_Program(data, g_Msg);
			setTemplate(data, "Garantias,Agt,AGTP171.vm");
			return;
		}
		if (RUTGEN.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Agtp305")) {
			com.FHTServlet.modules.global.PRMPC080.PrmPC080 l_PrmPC080 = new com.FHTServlet.modules.global.PRMPC080.PrmPC080();
			l_PrmPC080 = PRMPC080.LSet_A_PrmPC080(g_Msg.Msg_Dat.toString());
			g_PrmPC080 = (com.FHTServlet.modules.global.PRMPC080.PrmPC080) data
					.getUser().getTemp("Agtp171_g_PrmPC080");
			if (l_PrmPC080.PC080_Rtn.toString().equals("OK")) {
				g_Img_GtAlz = (com.intGarantias.modules.global.GT_IMG.Buf_Img_GtAlz) data
						.getUser().getTemp("Agtp171_ImgGtAlz");
				v_Img_GtAcr = (Vector) data.getUser()
						.getTemp("Agtp305_gTabImg");
				data.getUser().removeTemp("Agtp171_vImgGtAcr");
				data.getUser().setTemp("Agtp171_vImgGtAcr", v_Img_GtAcr);
				data.getUser().removeTemp("Agtp305_gTabImg");
				RUTGEN.MoverA(g_Img_GtAlz.GtAlz_Rcr, "I");
				data.getUser().removeTemp("Agtp171_ImgGtAlz");
				data.getUser().setTemp("Agtp171_ImgGtAlz", g_Img_GtAlz);
			}
			RUTGEN.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
			BF_MSG.Param_Program(data, g_Msg);
			setTemplate(data, "Garantias,Agt,AGTP171.vm");
			return;
		}
		if (RUTGEN.FH_RTrim(g_Msg.Msg_Pgl.toString()).equals("Appp030")) {
			g_PrmPC080 = (com.FHTServlet.modules.global.PRMPC080.PrmPC080) data
					.getUser().getTemp("Agtp171_g_PrmPC080");
			data.getUser().removeTemp("Agtp171_g_PrmPC080");
			RUTGEN.MoverA(g_Msg.Msg_Dat, PRMPC080.LSet_De_PrmPC080(g_PrmPC080));
			BF_MSG.Param_Program(data, g_Msg);
			setTemplate(data, "Garantias,Agt,AGTP171.vm");
			return;
		} else {
			return;
		}
	}

	public void generaBaseA(RunData data, Context context) throws Exception {
		com.FHTServlet.modules.global.BF_IMG.Buf_Img_Bse p_Img_Base = new com.FHTServlet.modules.global.BF_IMG.Buf_Img_Bse();
		g_Img_Base = BF_IMG.LSet_A_ImgBase(g_Img_GtAlz.GtAlz_Bse.toString());
		RUTGEN.MoverA(p_Img_Base.Img_Base_Ntr,
				Asigna_Folio("TRN-NTR", data, context));
		RUTGEN.MoverA(p_Img_Base.Img_Base_Ttr, "APERT");
		RUTGEN.MoverA(p_Img_Base.Img_Base_Ntt, "APERTURA DE GARANTIA");
		RUTGEN.MoverA(p_Img_Base.Img_Base_Tsl, g_Img_Base.Img_Base_Tsl);
		RUTGEN.MoverA(p_Img_Base.Img_Base_Slc, g_Img_Base.Img_Base_Slc);
		RUTGEN.MoverA(p_Img_Base.Img_Base_Cmt, g_Img_Base.Img_Base_Cmt);
		RUTGEN.MoverA(p_Img_Base.Img_Base_Cli, g_Img_Base.Img_Base_Cli);
		RUTGEN.MoverA(p_Img_Base.Img_Base_Ncl, g_Img_Base.Img_Base_Ncl);
		RUTGEN.MoverA(p_Img_Base.Img_Base_Tpr, g_Img_Base.Img_Base_Tpr);
		RUTGEN.MoverA(p_Img_Base.Img_Base_Dir, g_Img_Base.Img_Base_Dir);
		RUTGEN.MoverA(p_Img_Base.Img_Base_Tfc, g_Img_Base.Img_Base_Tfc);
		RUTGEN.MoverA(p_Img_Base.Img_Base_Sis, g_Img_Base.Img_Base_Sis);
		RUTGEN.MoverA(p_Img_Base.Img_Base_Ncn,
				Asigna_Folio("GRT-NCN", data, context));
		RUTGEN.MoverA(p_Img_Base.Img_Base_Dcn, "CRRES");
		if (g_Img_GtAlz.GtAlz_Rcr.toString().equals("B"))
			RUTGEN.MoverA(p_Img_Base.Img_Base_Dcn, "CARES");
		if (g_Img_GtAlz.GtAlz_Rcr.toString().equals("N"))
			RUTGEN.MoverA(p_Img_Base.Img_Base_Dcn, "CAREN");
		RUTGEN.MoverA(p_Img_Base.Img_Base_Ndc, "CARTA DE RESGUARDO");
		RUTGEN.MoverA(p_Img_Base.Img_Base_Tmn, g_Img_Base.Img_Base_Tmn);
		RUTGEN.MoverA(p_Img_Base.Img_Base_Trj, g_Img_Base.Img_Base_Trj);
		RUTGEN.MoverA(p_Img_Base.Img_Base_Dcm, g_Img_Base.Img_Base_Dcm);
		RUTGEN.MoverA(p_Img_Base.Img_Base_Prp, g_Img_Base.Img_Base_Prp);
		RUTGEN.MoverA(p_Img_Base.Img_Base_Suc, g_Img_Base.Img_Base_Suc);
		RUTGEN.MoverA(p_Img_Base.Img_Base_Nsu, g_Img_Base.Img_Base_Nsu);
		RUTGEN.MoverA(p_Img_Base.Img_Base_Mnd, g_Img_Base.Img_Base_Mnd);
		RUTGEN.MoverA(p_Img_Base.Img_Base_Nmn, g_Img_Base.Img_Base_Nmn);
		RUTGEN.MoverA(p_Img_Base.Img_Base_Eje, g_Img_Base.Img_Base_Eje);
		RUTGEN.MoverA(p_Img_Base.Img_Base_Nej, g_Img_Base.Img_Base_Nej);
		RUTGEN.MoverA(p_Img_Base.Img_Base_Tej, g_Img_Base.Img_Base_Tej);
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Bse,
				BF_IMG.LSet_De_ImgBase_Bse(p_Img_Base));
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Fpr, g_Msg.Msg_Fch);
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Rgf, "");
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Gfu, "");
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Ftc, g_Msg.Msg_Fch);
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Cbp, " ");
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Dbp,
				RUTGEN.Zeros(g_Img_GtApe.GtApe_Dbp));
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Tob, "E");
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Plm, "N");
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Ggr, "DMN");
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Dsg, g_Evt.Evt_Ndc.toString().trim());
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Cbt, "E");
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Grd, "1");
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Cmp, "N");
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Sgr, "?");
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Ctb, "?");
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Ilm, "N");
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Pcl,
				RUTGEN.Zeros(g_Img_GtApe.GtApe_Pcl));
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Mtl,
				RUTGEN.Zeros(g_Img_GtApe.GtApe_Mtl));
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Fig, g_Msg.Msg_Fch);
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Tsd, " ");
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Fts, " ");
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Vuo,
				RUTGEN.Zeros(g_Img_GtApe.GtApe_Vuo));
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Vtt,
				RUTGEN.Zeros(g_Img_GtApe.GtApe_Vtt));
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Vts,
				RUTGEN.Zeros(g_Img_GtApe.GtApe_Vts));
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Vtb,
				RUTGEN.Zeros(g_Img_GtApe.GtApe_Vtb));
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Vcn,
				RUTGEN.Zeros(g_Img_GtApe.GtApe_Vcn));
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Cbg, " ");
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Nct, " ");
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Fct, " ");
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Fvt, " ");
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Vuf,
				RUTGEN.Zeros(g_Img_GtApe.GtApe_Vuf));
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Vus,
				RUTGEN.Zeros(g_Img_GtApe.GtApe_Vus));
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Hvt,
				RUTGEN.Zeros(g_Img_GtApe.GtApe_Hvt));
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Hvl,
				RUTGEN.Zeros(g_Img_GtApe.GtApe_Hvl));
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Hvb,
				RUTGEN.Zeros(g_Img_GtApe.GtApe_Hvb));
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Hvc,
				RUTGEN.Zeros(g_Img_GtApe.GtApe_Hvc));
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Pvt,
				RUTGEN.Zeros(g_Img_GtApe.GtApe_Pvt));
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Pvl,
				RUTGEN.Zeros(g_Img_GtApe.GtApe_Pvl));
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Pvb,
				RUTGEN.Zeros(g_Img_GtApe.GtApe_Pvb));
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Pvc,
				RUTGEN.Zeros(g_Img_GtApe.GtApe_Pvc));
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Psw, " ");
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Psq, " ");
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Psd, " ");
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Idx, " ");
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Tfr, " ");
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Dpv, " ");
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Dpp, " ");
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Dsw, " ");
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Pct, " ");
		RUTGEN.MoverA(g_Img_GtApe.GtApe_Fll, " ");
		v_Img_GtAcr.clear();
		RUTGEN.MoverA(g_Img.Img_Ntr, g_PrmPC080.PC080_Ntr);
		RUTGEN.MoverA(g_Img.Img_Dax, "BASE");
		RUTGEN.MoverA(g_Img.Img_Seq, "000");
		RUTGEN.MoverA(g_Img.Img_Dat, GT_IMG.LSet_De_ImgGtApe(g_Img_GtApe));
		v_Img_GtAcr.add(g_Img);
		data.getUser().removeTemp("Agtp171_vImgGtAcr");
		data.getUser().setTemp("Agtp171_vImgGtAcr", v_Img_GtAcr);
	}

	public String Asigna_Folio(String pcFolio, RunData data, Context context)
			throws Exception {
		g_MsgED090 = MSGED090.Inicia_MsgED090();
		RUTGEN.MoverA(g_MsgED090.ED090_Idr, "F");
		RUTGEN.MoverA(g_MsgED090.ED090_Cod, pcFolio);
		RUTGEN.MoverA(g_Msg.Msg_Dat, MSGED090.LSet_De_MsgED090(g_MsgED090));
		g_Msg = BF_MSG.Transmite_Svc(data, g_Msg, "ED090");
		g_MsgED090 = MSGED090.LSet_A_MsgED090(g_Msg.Msg_Dat.toString());
		String lcFolio = g_MsgED090.ED090_Fol.toString();
		return lcFolio;
	}

	public int i;
	public String lcData;
	RUTGEN rg;
	BF_MSG msg;
	com.FHTServlet.modules.global.BF_MSG.Buf_Msg g_Msg;
	com.FHTServlet.modules.global.BF_EVT.Buf_Evt g_Evt;
	BF_IMG img;
	com.FHTServlet.modules.global.BF_IMG.Buf_Img g_Img;
	com.FHTServlet.modules.global.BF_IMG.Buf_Img_Bse g_Img_Base;
	GT_IMG gtimg;
	com.intGarantias.modules.global.GT_IMG.Buf_Img_GtAlz g_Img_GtAlz;
	com.intGarantias.modules.global.GT_IMG.Buf_Img_GtAld g_Img_GtAld;
	Vector v_Img_GtAld;
	com.intGarantias.modules.global.GT_IMG.Buf_Img_GtApe g_Img_GtApe;
	Vector v_Img_GtAcr;
	com.FHTServlet.modules.global.PRMPC080.PrmPC080 g_PrmPC080;
	com.intGlobal.modules.global.MSGED090.Buf_MsgED090 g_MsgED090;
	com.intGlobal.modules.global.MSGED100.Buf_MsgED100 g_MsgED100;
	com.intGlobal.modules.global.MSGED135.Buf_MsgED135 g_MsgED135;
	com.intGlobal.modules.global.PRMED210.Buf_PrmED210 g_PrmED210;
	com.intGarantias.modules.global.MSGGT171.Buf_MsgGT171 g_MsgGT171;
}
