package com.intGarantias.bean;

import java.util.Date;

public class GtiasTasablesEnTramiteBean {
	
	int numGarantia;
	String codTransaccion;
	String glsTransaccion;
	String codProducto;
	int codTasador;
	String codTramite;
	String glsTramite;
	Date fecIniTram;
	String hrIniTram;
	String codEstado;
	String glsEstado;
	String rutCliente;
	String nomCliente;
	String codEjecutivo;
	String glsEjeCli;
	int ctaCorriente;
	double valTasado;
	String fecTasacion;
	String fecAceptadoTas;
	String diasPenFec;
	int reenvioTas;
	int terminadoTas;
	
	public int getNumGarantia() {
		return numGarantia;
	}
	public void setNumGarantia(int numGarantia) {
		this.numGarantia = numGarantia;
	}
	public String getCodTransaccion() {
		return codTransaccion;
	}
	public void setCodTransaccion(String codTransaccion) {
		this.codTransaccion = codTransaccion;
	}
	public String getGlsTransaccion() {
		return glsTransaccion;
	}
	public void setGlsTransaccion(String glsTransaccion) {
		this.glsTransaccion = glsTransaccion;
	}
	public String getCodProducto() {
		return codProducto;
	}
	public void setCodProducto(String codProducto) {
		this.codProducto = codProducto;
	}
	public int getCodTasador() {
		return codTasador;
	}
	public void setCodTasador(int codTasador) {
		this.codTasador = codTasador;
	}
	public String getCodTramite() {
		return codTramite;
	}
	public void setCodTramite(String codTramite) {
		this.codTramite = codTramite;
	}
	public String getGlsTramite() {
		return glsTramite;
	}
	public void setGlsTramite(String glsTramite) {
		this.glsTramite = glsTramite;
	}
	public Date getFecIniTram() {
		return fecIniTram;
	}
	public void setFecIniTram(Date fecIniTram) {
		this.fecIniTram = fecIniTram;
	}
	public String getHrIniTram() {
		return hrIniTram;
	}
	public void setHrIniTram(String hrIniTram) {
		this.hrIniTram = hrIniTram;
	}
	public String getCodEstado() {
		return codEstado;
	}
	public void setCodEstado(String codEstado) {
		this.codEstado = codEstado;
	}
	public String getGlsEstado() {
		return glsEstado;
	}
	public void setGlsEstado(String glsEstado) {
		this.glsEstado = glsEstado;
	}
	public String getRutCliente() {
		return rutCliente;
	}
	public void setRutCliente(String rutCliente) {
		this.rutCliente = rutCliente;
	}
	public String getNomCliente() {
		return nomCliente;
	}
	public void setNomCliente(String nomCliente) {
		this.nomCliente = nomCliente;
	}
	public String getCodEjecutivo() {
		return codEjecutivo;
	}
	public void setCodEjecutivo(String codEjecutivo) {
		this.codEjecutivo = codEjecutivo;
	}
	public String getGlsEjeCli() {
		return glsEjeCli;
	}
	public void setGlsEjeCli(String glsEjeCli) {
		this.glsEjeCli = glsEjeCli;
	}
	public int getCtaCorriente() {
		return ctaCorriente;
	}
	public void setCtaCorriente(int ctaCorriente) {
		this.ctaCorriente = ctaCorriente;
	}
	public double getValTasado() {
		return valTasado;
	}
	public void setValTasado(double valTasado) {
		this.valTasado = valTasado;
	}
	
	public String getFecTasacion() {
		return fecTasacion;
	}
	public void setFecTasacion(String fecTasacion) {
		this.fecTasacion = fecTasacion;
	}
	
	public String getFecAceptadoTas() {
		return fecAceptadoTas;
	}
	public void setFecAceptadoTas(String fecAceptadoTas) {
		this.fecAceptadoTas = fecAceptadoTas;
	}
	public String getDiasPenFec() {
		return diasPenFec;
	}
	public void setDiasPenFec(String diasPenFec) {
		this.diasPenFec = diasPenFec;
	}
	public int getReenvioTas() {
		return reenvioTas;
	}
	public void setReenvioTas(int reenvioTas) {
		this.reenvioTas = reenvioTas;
	}
	public int getTerminadoTas() {
		return terminadoTas;
	}
	public void setTerminadoTas(int terminadoTas) {
		this.terminadoTas = terminadoTas;
	}
	
	

}

	

