package com.intGarantias.bean;


public class FacturaGtiasBean  {

    public long numFolio;
    public String numBoleta;
    public String fecha;
    public String codTasador;
    public String nomTasador;
    public String nomDoc;
    public String descDoc;
    public String tipDoc;
	private byte[] boletaBlob;
	
	private long numEvento;
	private long numGtia;
	private String fecEvento;
	private String tipEvento;
	
	
	public long getNumFolio() {
		return numFolio;
	}
	public void setNumFolio(long numFolio) {
		this.numFolio = numFolio;
	}

	public String getNumBoleta() {
		return numBoleta;
	}
	public void setNumBoleta(String numBoleta) {
		this.numBoleta = numBoleta;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getCodTasador() {
		return codTasador;
	}
	public void setCodTasador(String codTasador) {
		this.codTasador = codTasador;
	}
	public String getNomTasador() {
		return nomTasador;
	}
	public void setNomTasador(String nomTasador) {
		this.nomTasador = nomTasador;
	}
	public String getNomDoc() {
		return nomDoc;
	}
	public void setNomDoc(String nomDoc) {
		this.nomDoc = nomDoc;
	}
	public String getDescDoc() {
		return descDoc;
	}
	public void setDescDoc(String descDoc) {
		this.descDoc = descDoc;
	}
	
	public String getTipDoc() {
		return tipDoc;
	}
	public void setTipDoc(String tipDoc) {
		this.tipDoc = tipDoc;
	}
	public byte[] getBoletaBlob() {
		return boletaBlob;
	}
	public void setBoletaBlob(byte[] boletaBlob) {
		this.boletaBlob = boletaBlob;
	}
	public long getNumEvento() {
		return numEvento;
	}
	public void setNumEvento(long numEvento) {
		this.numEvento = numEvento;
	}
	public long getNumGtia() {
		return numGtia;
	}
	public void setNumGtia(long numGtia) {
		this.numGtia = numGtia;
	}
	public String getFecEvento() {
		return fecEvento;
	}
	public void setFecEvento(String fecEvento) {
		this.fecEvento = fecEvento;
	}
	public String getTipEvento() {
		return tipEvento;
	}
	public void setTipEvento(String tipEvento) {
		this.tipEvento = tipEvento;
	}
	
		

}
