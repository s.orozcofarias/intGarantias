package com.intGarantias.bean;

public class ValorMonedaBean {
	
	private String glsCorta;
	private String glsLarga;
	private String valor;
	
	public String getGlsCorta() {
		return glsCorta;
	}
	public void setGlsCorta(String glsCorta) {
		this.glsCorta = glsCorta;
	}
	public String getGlsLarga() {
		return glsLarga;
	}
	public void setGlsLarga(String glsLarga) {
		this.glsLarga = glsLarga;
	}
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}

}
