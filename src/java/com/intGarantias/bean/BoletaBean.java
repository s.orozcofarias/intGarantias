package com.intGarantias.bean;

import java.sql.Blob;
import java.sql.Date;

public class BoletaBean {
	
	private long   idFID;
	private long   idNID;
	private String nombreCodificadoArchivo;
	private String tipoArchivo;
	private Blob   blobArchivo;
	private String descripcion;
	private String nombreArchivo;
	private Date   fecha;
	private String numeroBoleta;
	
	public long getIdFID() {
		return idFID;
	}
	public void setIdFID(long idFID) {
		this.idFID = idFID;
	}
	public long getIdNID() {
		return idNID;
	}
	public void setIdNID(long idNID) {
		this.idNID = idNID;
	}
	public String getNombreCodificadoArchivo() {
		return nombreCodificadoArchivo;
	}
	public void setNombreCodificadoArchivo(String nombreCodificadoArchivo) {
		this.nombreCodificadoArchivo = nombreCodificadoArchivo;
	}
	public String getTipoArchivo() {
		return tipoArchivo;
	}
	public void setTipoArchivo(String tipoArchivo) {
		this.tipoArchivo = tipoArchivo;
	}
	public Blob getBlobArchivo() {
		return blobArchivo;
	}
	public void setBlobArchivo(Blob blobArchivo) {
		this.blobArchivo = blobArchivo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getNombreArchivo() {
		return nombreArchivo;
	}
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public String getNumeroBoleta() {
		return numeroBoleta;
	}
	public void setNumeroBoleta(String numeroBoleta) {
		this.numeroBoleta = numeroBoleta;
	}
	
	

}
