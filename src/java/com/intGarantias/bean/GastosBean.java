package com.intGarantias.bean;

public class GastosBean {
	
	
	private String numeroGarantia;
	private String numeroEvento;
	private String dvcNID;//DVC_NID NOT NULL NUMBER(10)          
	private String monto;
	private String montoEnUF;
	private String detalle;
	private String vctNID;//VCT_NID          NUMBER(10)
	
	
	
	public GastosBean() {
		this.montoEnUF 	= "0";
		this.monto 		= "0";
	}
	public String getNumeroGarantia() {
		return numeroGarantia;
	}
	public void setNumeroGarantia(String numeroGarantia) {
		this.numeroGarantia = numeroGarantia;
	}
	public String getNumeroEvento() {
		return numeroEvento;
	}
	public void setNumeroEvento(String numeroEvento) {
		this.numeroEvento = numeroEvento;
	}
	public String getDvcNID() {
		return dvcNID;
	}
	public void setDvcNID(String dvcNID) {
		this.dvcNID = dvcNID;
	}
	public String getMonto() {
		return monto;
	}
	public void setMonto(String monto) {
		this.monto = monto;
	}
	public String getDetalle() {
		return detalle;
	}
	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}
	public String getVctNID() {
		return vctNID;
	}
	public void setVctNID(String vctNID) {
		this.vctNID = vctNID;
	}
	public String getMontoEnUF() {
		return montoEnUF;
	}
	public void setMontoEnUF(String montoEnUF) {
		this.montoEnUF = montoEnUF;
	}
	
	 
	    
	
	

}
