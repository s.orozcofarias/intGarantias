package com.intGarantias.bean;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.turbine.util.Log;

public class CargaPropiedades {
	
	Properties prop = new Properties();
	
	public  CargaPropiedades(){

		Log.debug("cargaArchivoPropiedades");
		
		InputStream planillaProps = null;
		planillaProps = this.getClass().getClassLoader().getResourceAsStream("properties/garantias.properties");		

		try {
			prop.load(planillaProps);
			Log.debug("Properties cargado");
			
		} catch (IOException e) {
			Log.error("Error al cargar archivo properties planilla", e);			
		}finally{
			try{
				planillaProps.close();
			}catch(IOException io){
				Log.error("Error al cargar archivo properties planilla",io);
			}
		}
	}
	
	public String getPropertie(String propiedad){
		Log.debug("Properties cargado " + prop.getProperty(propiedad));
		return prop.getProperty(propiedad);
	}

}
