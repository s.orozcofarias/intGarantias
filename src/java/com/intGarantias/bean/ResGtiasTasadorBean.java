package com.intGarantias.bean;

public class ResGtiasTasadorBean {
	
	String nomTasador;
	int codTasador;
	int cantTasaciones;
	double porcTasaciones;
	String agricola;
	int porAceptar;
	int registroTas;
	int pendienteTas;
	int reenvioTas;
	
	
	public String getNomTasador() {
		return nomTasador;
	}
	public void setNomTasador(String nomTasador) {
		this.nomTasador = nomTasador;
	}
	public int getCodTasador() {
		return codTasador;
	}
	public void setCodTasador(int codTasador) {
		this.codTasador = codTasador;
	}
	public int getCantTasaciones() {
		return cantTasaciones;
	}
	public void setCantTasaciones(int cantTasaciones) {
		this.cantTasaciones = cantTasaciones;
	}
	public double getPorcTasaciones() {
		return porcTasaciones;
	}
	public void setPorcTasaciones(double porcTasaciones) {
		this.porcTasaciones = porcTasaciones;
	}
	public String getAgricola() {
		return agricola;
	}
	public void setAgricola(String agricola) {
		this.agricola = agricola;
	}
	public int getPorAceptar() {
		return porAceptar;
	}
	public void setPorAceptar(int porAceptar) {
		this.porAceptar = porAceptar;
	}
	public int getRegistroTas() {
		return registroTas;
	}
	public void setRegistroTas(int registroTas) {
		this.registroTas = registroTas;
	}
	public int getPendienteTas() {
		return pendienteTas;
	}
	public void setPendienteTas(int pendienteTas) {
		this.pendienteTas = pendienteTas;
	}
	public int getReenvioTas() {
		return reenvioTas;
	}
	public void setReenvioTas(int reenvioTas) {
		this.reenvioTas = reenvioTas;
	}
	
	
	
	

}

	

