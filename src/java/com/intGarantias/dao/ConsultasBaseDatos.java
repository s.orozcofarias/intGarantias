package com.intGarantias.dao;

import java.io.InputStream;
import java.io.OutputStream;
import java.math.RoundingMode;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.security.auth.login.FailedLoginException;

import oracle.jdbc.OracleConnection;
import oracle.jdbc.OraclePreparedStatement;
import oracle.jdbc.OracleStatement;
import oracle.jdbc.OracleTypes;
import oracle.sql.BLOB;

import org.apache.turbine.om.peer.BasePeer;
import org.apache.turbine.util.Log;
import org.apache.turbine.util.RunData;
import org.apache.turbine.util.db.pool.DBConnection;
import org.apache.turbine.util.upload.FileItem;
import org.apache.velocity.context.Context;

import com.FHTServlet.modules.global.RUTGEN;
import com.FHTServlet.modules.util.Constants;
import com.intGarantias.bean.BoletaBean;
import com.intGarantias.bean.FacturaGtiasBean;
import com.intGarantias.bean.GastosBean;
import com.intGarantias.bean.GtiasTasablesEnTramiteBean;
import com.intGarantias.bean.ResGtiasTasadorBean;
import com.intGarantias.bean.ValorMonedaBean;
import com.intGarantias.util.Constantes;

public class ConsultasBaseDatos {
	
	
	public List consultaDetalleTasadores(){
		Log.debug("consultaDetalleTasadores()");

		DBConnection dbConnGrt = null;
		CallableStatement cs  = null;
		ResultSet rs  = null;
		List detalleTasadores  =  new ArrayList(); 

		try {

			dbConnGrt = BasePeer.beginTransaction(Constants.BASE_GAR);
			String sp = "{call GRT_ADMIN.GRT_PAC_INFORMES_UVT.GRT_SP_LIS_DETGARTRAM_TAS(?,?,?)}"; 

			Log.debug("sp : " + sp);
			cs = dbConnGrt.getConnection().prepareCall(sp);

			cs.registerOutParameter(1, Types.NUMERIC);
			cs.registerOutParameter(2, Types.VARCHAR);
			cs.registerOutParameter(3, OracleTypes.CURSOR);

			cs.executeQuery();

			if (cs.getInt(1) == 0) {
				Log.debug("consultaDetalleTasadores() EXITOSO ");
				rs = (ResultSet)  cs.getObject(3);
				   

				while (rs.next()) {
					GtiasTasablesEnTramiteBean gtiasTasEnTramiteBean = new GtiasTasablesEnTramiteBean();

					gtiasTasEnTramiteBean.setNumGarantia(rs.getInt(1));
					gtiasTasEnTramiteBean.setCodTransaccion(rs.getString(2));
					gtiasTasEnTramiteBean.setGlsTransaccion(rs.getString(3));
					gtiasTasEnTramiteBean.setCodProducto(rs.getString(4));
					gtiasTasEnTramiteBean.setCodTasador(rs.getInt(5));
					gtiasTasEnTramiteBean.setCodTramite(rs.getString(6));
					gtiasTasEnTramiteBean.setGlsTramite(rs.getString(7));
					gtiasTasEnTramiteBean.setFecIniTram(rs.getDate(8));
					gtiasTasEnTramiteBean.setHrIniTram(rs.getString(9));
					gtiasTasEnTramiteBean.setCodEstado(rs.getString(10));
					gtiasTasEnTramiteBean.setGlsEstado(rs.getString(11));
					gtiasTasEnTramiteBean.setRutCliente(rs.getString(12));
					gtiasTasEnTramiteBean.setNomCliente(rs.getString(13));
					gtiasTasEnTramiteBean.setCodEjecutivo(rs.getString(14));
					gtiasTasEnTramiteBean.setGlsEjeCli(rs.getString(15));
					gtiasTasEnTramiteBean.setCtaCorriente(rs.getInt(16));
					gtiasTasEnTramiteBean.setValTasado(rs.getDouble(17));
					gtiasTasEnTramiteBean.setFecTasacion(rs.getString(18));
					gtiasTasEnTramiteBean.setFecAceptadoTas(rs.getString(19));
					gtiasTasEnTramiteBean.setDiasPenFec(rs.getString(20));
					gtiasTasEnTramiteBean.setReenvioTas(rs.getInt(21));
					gtiasTasEnTramiteBean.setTerminadoTas(rs.getInt(22));

					detalleTasadores.add(gtiasTasEnTramiteBean);//inserta datos en lista

				}

				BasePeer.commitTransaction(dbConnGrt);   

			}

			return detalleTasadores;
			
		} catch (Exception e) {		
			e.printStackTrace();
			return detalleTasadores;
		}finally {
			 try 
	         {
	             cs.close();
	             rs.close();
	         }
	         catch (SQLException ee)
	         {
	             ee.printStackTrace(); 
	         }
	         try 
	         {
	        	 dbConnGrt.close();
	         }
	         catch (SQLException ee)
	         {
	             ee.printStackTrace(); 
	         }
		}

	}
	
	
	public List consultaResumenTasador(){
		Log.debug("consultaResumenTasador()");

		DBConnection dbConnGrt = null;
		CallableStatement cs  = null;
		ResultSet rs  = null;
		List resumenTasadores  =  new ArrayList(); 

		try {

			dbConnGrt = BasePeer.beginTransaction(Constants.BASE_GAR);
			String sp = "{call GRT_ADMIN.GRT_PAC_INFORMES_UVT.GRT_SP_LIS_TASACIONES_TAS(?,?,?)}"; 

			Log.debug("sp : " + sp);
			cs = dbConnGrt.getConnection().prepareCall(sp);

			cs.registerOutParameter(1, Types.NUMERIC);
			cs.registerOutParameter(2, Types.VARCHAR);
			cs.registerOutParameter(3, OracleTypes.CURSOR);

			cs.executeQuery();

			if (cs.getInt(1) == 0) {
				Log.debug("consultaResumenTasador() EXITOSO ");
				rs = (ResultSet)  cs.getObject(3);
				   

				while (rs.next()) {
					ResGtiasTasadorBean resGtiasTasadorBean = new ResGtiasTasadorBean();

					resGtiasTasadorBean.setNomTasador(rs.getString(1));
					resGtiasTasadorBean.setCodTasador(rs.getInt(2));
					resGtiasTasadorBean.setCantTasaciones(rs.getInt(3));
					resGtiasTasadorBean.setPorcTasaciones(rs.getDouble(4));
//					Log.debug("resGtiasTasadorBean.getPorcTasaciones()"+resGtiasTasadorBean.getPorcTasaciones());
					resGtiasTasadorBean.setAgricola(rs.getString(5));
					resGtiasTasadorBean.setPorAceptar(rs.getInt(6));
					resGtiasTasadorBean.setRegistroTas(rs.getInt(7));
					resGtiasTasadorBean.setPendienteTas(rs.getInt(8));
					resGtiasTasadorBean.setReenvioTas(rs.getInt(9));
					
					resumenTasadores.add(resGtiasTasadorBean);//inserta datos en lista

				}

				BasePeer.commitTransaction(dbConnGrt);   

			}

			return resumenTasadores;
			
		} catch (Exception e) {		
			e.printStackTrace();
			return resumenTasadores;
		}finally {
			 try 
	         {
	             cs.close();
	             rs.close();
	         }
	         catch (SQLException ee)
	         {
	             ee.printStackTrace(); 
	         }
	         try 
	         {
	        	 dbConnGrt.close();
	         }
	         catch (SQLException ee)
	         {
	             ee.printStackTrace(); 
	         }
		}

	}
	

	
	public FacturaGtiasBean consultaFacturaTasador(long numFolio){
		Log.debug("consultaFacturaTasador()");

		DBConnection dbConnGrt = null;
		CallableStatement cs  = null;
		ResultSet rs  = null;
		FacturaGtiasBean facturaGtiasBean = new FacturaGtiasBean();
		
		try {

			dbConnGrt = BasePeer.beginTransaction(Constants.BASE_GAR);
			String sp = "{call GRT_ADMIN.GRT_SP_CON_FACTURA(?,?,?,?)}"; 

			Log.debug("sp : " + sp);
			cs = dbConnGrt.getConnection().prepareCall(sp);

			cs.setLong(1, numFolio);
			cs.registerOutParameter(2, Types.NUMERIC);
			cs.registerOutParameter(3, Types.VARCHAR);
			cs.registerOutParameter(4, OracleTypes.CURSOR);

			cs.executeQuery();

			if (cs.getInt(2) == 0) {
				Log.debug("consultaDetalleFacturaTas() EXITOSO ");
				rs = (ResultSet)  cs.getObject(4);
				   

				while (rs.next()) {
					
					facturaGtiasBean.setNumFolio(rs.getLong("NUM_FOLIO"));
					facturaGtiasBean.setTipDoc(rs.getString("COD_TIP_DOC"));
					
					byte[] bits = null;
					BLOB blb = BLOB.createTemporary(dbConnGrt.getConnection(), false, 1);

					blb.open(0);
					bits = dumpBlob((BLOB) rs.getBlob("BOLETA_BLOB"));
					blb.close();
					
					facturaGtiasBean.setBoletaBlob(bits);
					
				}

				BasePeer.commitTransaction(dbConnGrt);   

			}

			return facturaGtiasBean;
			
		} catch (Exception e) {		
			e.printStackTrace();
			return facturaGtiasBean;
		}finally {
			 try 
	         {
	             cs.close();
	             rs.close();
	         }
	         catch (SQLException ee)
	         {
	             ee.printStackTrace(); 
	         }
	         try 
	         {
	        	 dbConnGrt.close();
	         }
	         catch (SQLException ee)
	         {
	             ee.printStackTrace(); 
	         }
		}

	}
	
	
	public List consultaEventosGtia(RunData data){
		Log.debug("consultaEventosGtia()");

		DBConnection dbConnGrt = null;
		CallableStatement cs  = null;
		ResultSet rs  = null;
		List eventosGtiasList  =  new ArrayList(); 
		
		String 	numGtiaAux 	= data.getParameters().getString("num_gtia", "");
		Long 	numGtia 	= Long.valueOf(numGtiaAux);
		
		String userName = "";
		if(data.getUser()!=null && data.getUser().getUserName()!=null){
			userName = data.getUser().getUserName();
		}
		
		
		try {
			
			dbConnGrt = BasePeer.beginTransaction(Constants.BASE_GAR);
			String sp = "{call GRT_ADMIN.GRT_SP_CON_EVENTOS(?,?,?,?,?)}"; 
			
			Log.debug("sp : " + sp);
			cs = dbConnGrt.getConnection().prepareCall(sp);
			
			cs.setLong(1, numGtia);
			cs.setString(2, userName);
			cs.registerOutParameter(3, Types.NUMERIC);
			cs.registerOutParameter(4, Types.VARCHAR);
			cs.registerOutParameter(5, OracleTypes.CURSOR);

			cs.executeQuery();

			if (cs.getInt(3) == 0) {
				Log.debug("consultaEventosGtia() EXITOSO ");
				rs = (ResultSet)  cs.getObject(5);
				   

				while (rs.next()) {
					
					FacturaGtiasBean eventosGtias = new FacturaGtiasBean();

					eventosGtias.setNumEvento(rs.getLong("NUM_EVENTO"));
					eventosGtias.setTipEvento(rs.getString("TIP_EVENTO"));
					eventosGtias.setNumGtia(rs.getLong("NUM_GTIA"));
					eventosGtias.setFecEvento(rs.getString("FEC_EVENTO"));
				
					eventosGtiasList.add(eventosGtias);

				}

				BasePeer.commitTransaction(dbConnGrt);   

			}

			
			
		} catch (Exception e) {		
			e.printStackTrace();
			
		}finally {
			
			try 
            {
				cs.close();
                
            }
            catch (SQLException ee)
            {
                ee.printStackTrace(); 
            }
            try 
            {
            	rs.close();                
            }
            catch (SQLException ee)
            {
                ee.printStackTrace(); 
            }
            try 
            {
            	dbConnGrt.close();
            }
            catch (SQLException ee)
            {
                ee.printStackTrace(); 
            }

		}
		return eventosGtiasList;
		
	}
	
	//hs 
	
	public String insertarGastos(RunData data, Context context) throws Exception {
		DBConnection 		conn 	= null;
		PreparedStatement 	pstmt 	= null;
		String msjSalida = "";
		
		try {
			Log.debug("ConsultasBaseDatos.java :: insertarGastos - INICIO");
			
			conn = BasePeer.beginTransaction(Constants.BASE_GAR);
			
			String numEvento = data.getParameters().getString("num_evento", "");
			String numGtia = data.getParameters().getString("num_gtia", "");
			
			String honorarioDetalle = data.getParameters().getString("honorario_detalle", "");
			String honorarioUF = data.getParameters().getString("honorario_uf", "");
			String honorarioPesos = data.getParameters().getString("honorario_pesos", "").replace(".","");
			String VCTNIDHonorarios = Constantes.VCT_NID_HONORARIOS;
			String honorariosNID = data.getParameters().getString("honorariosNID", ""); 
			
			String peajesDetalle = data.getParameters().getString("peajes_detalle", "");
			String peajesUF = data.getParameters().getString("peajes_uf", "");
			String peajesPesos = data.getParameters().getString("peajes_pesos", "").replace(".","");
			String VCTNIDPeajes = Constantes.VCT_NID_PEAJES; //peaje
			String peajesNID = data.getParameters().getString("peajesNID", "");
			
			String kilometrosDetalle = data.getParameters().getString("kilometros_detalle", "");
			String kilometrosUF = data.getParameters().getString("kilometros_uf", "");
			String kilometrosPesos = data.getParameters().getString("kilometros_pesos", "").replace(".","");
			String VCTNIDKilometros = Constantes.VCT_NID_KILOMETROS; //kilometros
			String kilometrosNID = data.getParameters().getString("kilometrosNID", "");
			
			String otrosDetalle = data.getParameters().getString("otros_detalle", "");
			String otrosUF = data.getParameters().getString("otros_uf", "");
			String otrosPesos = data.getParameters().getString("otros_pesos", "").replace(".","");
			String VCTNIDOtros = Constantes.VCT_NID_OTROS; //otros
			String otrosNID = data.getParameters().getString("otrosNID", "");
			
			Log.debug("ConsultasBaseDatos.java :: insertarGastos - parametros entrada:");
			Log.debug("numEvento -> " + numEvento);
			Log.debug("numGtia -> " + numGtia );

			Log.debug("honorario_detalle -> " + honorarioDetalle );
			Log.debug("honorario_uf -> " + honorarioUF );
			Log.debug("honorario_pesos -> " + honorarioPesos );
			
			Log.debug("peajes_detalle -> " + peajesDetalle );
			Log.debug("peajes_uf -> " + peajesUF );
			Log.debug("peajes_pesos -> " + peajesPesos );
			
			Log.debug("kilometros_detalle -> " + kilometrosDetalle );
			Log.debug("kilometros_uf -> " + kilometrosUF );
			Log.debug("kilometros_pesos -> " + kilometrosPesos );
			
			Log.debug("otros_detalle -> " + otrosDetalle );
			Log.debug("otros_uf -> " + otrosUF );
			Log.debug("otros_pesos -> " + otrosPesos );
			
			Log.debug("VCTNIDHonorarios -> " + VCTNIDHonorarios);
			Log.debug("VCTNIDPeajes -> " + VCTNIDPeajes);
			Log.debug("VCTNIDKilometros -> " + VCTNIDKilometros);
			Log.debug("kilometrosNID -> " + kilometrosNID);
			
			Log.debug("honorariosNID -> " + honorariosNID);
			Log.debug("peajesNID -> " + peajesNID);
			Log.debug("VCTNIDHonorarios -> " + VCTNIDHonorarios);
			Log.debug("otrosNID -> " + otrosNID);
			
			
			
			
			//actualiza o inserta honorarios
			String sqlHonorarios = "";
			if(existeGasto(numGtia,numEvento,VCTNIDHonorarios)){
				sqlHonorarios 	= 	"UPDATE grt_admin.TT_DVC_TRA SET DVC_MNT='"+honorarioPesos+"',DVC_NDT='"+honorarioDetalle+"' " +
								   " WHERE DVC_NID = '"+honorariosNID+"'";
			}else{
				sqlHonorarios 	= "Insert into grt_admin.TT_DVC_TRA (DVC_NCN,DVC_NEV,DVC_NID,DVC_MNT,DVC_NDT,VCT_NID) " +
					 "values ('"+numGtia+"','"+numEvento+"',grt_admin.TT_DVC_TRA_SEQ.nextVal,'"+honorarioPesos+"','"+honorarioDetalle+"','"+VCTNIDHonorarios+"')";
			}
			
			//actualiza o inserta peajes			
			String sqlPeajes = "";
			if(existeGasto(numGtia,numEvento,VCTNIDPeajes)){
				sqlPeajes	 	= "UPDATE grt_admin.TT_DVC_TRA SET DVC_MNT='"+peajesPesos+"',DVC_NDT='"+peajesDetalle+"' " +
						   " WHERE DVC_NID = '"+peajesNID+"'";
			}else{
				sqlPeajes	 	= "Insert into grt_admin.TT_DVC_TRA (DVC_NCN,DVC_NEV,DVC_NID,DVC_MNT,DVC_NDT,VCT_NID) " +
						 "values ('"+numGtia+"','"+numEvento+"',grt_admin.TT_DVC_TRA_SEQ.nextVal,'"+peajesPesos+"','"+peajesDetalle+"','"+VCTNIDPeajes+"')";
			}
			
			//actualiza o inserta kilometros
			String sqlKilometros = "";
			if(existeGasto(numGtia,numEvento,VCTNIDKilometros)){
				sqlKilometros 	= "UPDATE grt_admin.TT_DVC_TRA SET DVC_MNT='"+kilometrosPesos+"',DVC_NDT='"+kilometrosDetalle+"' " +
						   " WHERE DVC_NID = '"+kilometrosNID+"'";
			}else{
				sqlKilometros 	= "Insert into grt_admin.TT_DVC_TRA (DVC_NCN,DVC_NEV,DVC_NID,DVC_MNT,DVC_NDT,VCT_NID) " +
						 "values ('"+numGtia+"','"+numEvento+"',grt_admin.TT_DVC_TRA_SEQ.nextVal,'"+kilometrosPesos+"','"+kilometrosDetalle+"','"+VCTNIDKilometros+"')";
			}
			
			//actualiza o inserta otros
			String sqlOtros = "";
			if(existeGasto(numGtia,numEvento,VCTNIDOtros)){
				sqlOtros 		= "UPDATE grt_admin.TT_DVC_TRA SET DVC_MNT='"+otrosPesos+"',DVC_NDT='"+otrosDetalle+"' " +
						   " WHERE DVC_NID = '"+otrosNID+"'";
			}else{
				sqlOtros 		= "Insert into grt_admin.TT_DVC_TRA (DVC_NCN,DVC_NEV,DVC_NID,DVC_MNT,DVC_NDT,VCT_NID) " +
						 "values ('"+numGtia+"','"+numEvento+"',grt_admin.TT_DVC_TRA_SEQ.nextVal,'"+otrosPesos+"','"+otrosDetalle+"','"+VCTNIDOtros+"')";
			}
			
			Log.debug("ConsultasBaseDatos.java :: insertarGastos - SQL a ejecutar:");
			Log.debug("SQL Honorarios-> " 	+ sqlHonorarios);
			Log.debug("SQL Peajes-> " 		+ sqlPeajes);
			Log.debug("SQL Kilometros-> " 	+ sqlKilometros);
			Log.debug("SQL Otros-> " 		+ sqlOtros);
			
			pstmt =  conn.prepareStatement(sqlHonorarios);
			pstmt.execute();
			pstmt 					=  conn.prepareStatement(sqlPeajes);
			pstmt.execute();
			pstmt					=  conn.prepareStatement(sqlKilometros);
			pstmt.execute();
			pstmt 					=  conn.prepareStatement(sqlOtros);
			pstmt.execute();
			
			BasePeer.commitTransaction(conn);  
			msjSalida = "exito";
			Log.debug("ConsultasBaseDatos.java :: insertarGastos - SQL ejecutado y commitTransaction ejecutado.");
			
		} catch (SQLException e) {
			BasePeer.rollBackTransaction(conn);
			Log.debug("JDBCConnection.connect: [" + e.toString() + "]");
		} catch (Exception e) {
			BasePeer.rollBackTransaction(conn);
			Log.debug("Excepcion generico: [" + e.toString() + "]");
		} finally{
			
			try 
            {
				pstmt.close();
                
            }
            catch (SQLException ee)
            {
                ee.printStackTrace(); 
            }
			try 
            {
				conn.close();
                
            }
            catch (SQLException ee)
            {
                ee.printStackTrace(); 
            }
			
			
			Log.debug("Cierre de conexion y statement.");
		}
		
		Log.debug("ConsultasBaseDatos.java :: insertarGastos - FIN");
		return msjSalida;
	}
	
	
	
	
	private boolean existeGasto(String numGtia, String numEvento,
			String vctNid) throws Exception {

		
		DBConnection conn 			= null;
		PreparedStatement pstmt 	= null;
		ResultSet rs  				= null;
		boolean existeGasto 		= false;
		
		try {
			
			Log.debug("ConsultaBaseDatos :: existeGasto -> INICIO");
			conn = BasePeer.beginTransaction(Constants.BASE_GAR);
			
			String sql = "SELECT * FROM grt_admin.TT_DVC_TRA WHERE DVC_NCN = '"+numGtia+"' AND DVC_NEV = '"+numEvento+"' AND VCT_NID = '"+vctNid+"'";
			Log.debug("ConsultaBaseDatos :: existeGasto -> sql: " + sql);
			pstmt =  conn.prepareStatement(sql);
			rs  = pstmt.executeQuery();
			
			if (rs.next()) {
				Log.debug("ConsultaBaseDatos :: existeGasto -> TRUE ");
				existeGasto = true;
			}
			
			BasePeer.commitTransaction(conn);
			
		} catch (SQLException e) {
			BasePeer.rollBackTransaction(conn);
			Log.debug("JDBCConnection.connect: [" + e.toString() + "]");
		} finally{
			try 
            {
				rs.close();
                
            }
            catch (SQLException ee)
            {
                ee.printStackTrace(); 
            }
			try 
            {
				pstmt.close();
                
            }
            catch (SQLException ee)
            {
                ee.printStackTrace(); 
            }
			try 
            {
				conn.close();
                
            }
            catch (SQLException ee)
            {
                ee.printStackTrace(); 
            }
		}
		
		Log.debug("ConsultaBaseDatos :: insertarFacturaBoleta -> FIN");
	
		return existeGasto;
	}


	public String insertarFacturaBoleta(RunData data, Context context) throws Exception {
		
		DBConnection conn 			= null;
		PreparedStatement pstmt 	= null;
		ResultSet rs  				= null;
		String nextVal				= "";
		String msjSalida            = "";
		
		try {
			
			Log.debug("ConsultaBaseDatos :: insertarFacturaBoleta -> INICIO");
			FileItem fi = data.getParameters().getFileItem("Archivo");
			long largo = fi.getSize();
			Log.debug("ConsultaBaseDatos :: fi.getContentType() -> "+fi.getContentType());
			
			String contentType = fi.getContentType();//application/pdf
			Log.debug("ConsultaBaseDatos :: insertarFacturaBoleta -> tamano archivo: " + largo + "byte");
			
			//if(largo > 90000){			
			if(largo > 1000000){
				msjSalida = "0";	
				Log.debug(" ConsultaBaseDatos :: insertarFacturaBoleta -> msjSalida: " + msjSalida);
			}else if(!Constantes.CONTENT_TYPE_PDF.equals(contentType)){
				msjSalida = "1";	
				Log.debug(" ConsultaBaseDatos :: insertarFacturaBoleta -> msjSalida: " + msjSalida);				
			}else{
				
				conn = BasePeer.beginTransaction(Constants.BASE_GAR);
				
				BLOB GIF_GIF = BLOB.createTemporary(conn.getConnection(), false, 1);
				GIF_GIF.open(1);
				
				String nombreArchivo = fi.getFileName();
				String nombreArchivoEncritado = fi.getFileName().hashCode() + ""+ System.currentTimeMillis();
				InputStream instream = fi.getStream();
			    String tipoArchivo = tipoArchivoAbreviado(fi.getContentType());
			    String fechaDocumento = data.getParameters().getString("FecFin", "");
				String n_boleta = data.getParameters().getString("n_boleta", "");
				String numEvento = data.getParameters().getString("num_evento", "");
				String numGtia = data.getParameters().getString("num_gtia", "");
				
				Log.debug("consultaFacturaBoleta :: numGtia -> " 			+ numGtia);
				Log.debug("consultaFacturaBoleta :: numEvento -> " 			+ numEvento);
				Log.debug("tipo archivo -> "+tipoArchivo);
				Log.debug("nombreArchivo ->"+nombreArchivo);
				Log.debug("nombreArchivoEncritado ->"+nombreArchivoEncritado);
				Log.debug("n_boleta ->"+n_boleta);
				Log.debug("fechaDocumento ->"+fechaDocumento);
				fillBlob(GIF_GIF, instream, largo);
				
				nextVal = getIdDesTra(numGtia,numEvento);
				if("".equals(nextVal)){	
					String sqlSeq = "SELECT grt_admin.TT_DES_TRA_SEQ.nextval FROM DUAL";
					Log.debug("ConsultaBaseDatos :: insertarFacturaBoleta -> sqlSEQ: " + sqlSeq);
					pstmt =  conn.prepareStatement(sqlSeq);
					rs  = pstmt.executeQuery();
					
					if (rs.next()) {
						nextVal = rs.getString("NEXTVAL");
						Log.debug("ConsultaBaseDatos :: insertarFacturaBoleta -> NEXTVAL: " + nextVal);
					}
					
					String sqlDesTra = "Insert into grt_admin.TT_DES_TRA (DES_SIS,DES_NCN,DES_NEV,DES_NID,DES_DES,DES_TIP) values ('GAR','"+numGtia+"','"+numEvento+"','"+nextVal+"','Datos_tas_boleta','Datos_tas_boleta')";
					Log.debug("ConsultaBaseDatos :: insertarFacturaBoleta -> sqlDesTra: " + sqlDesTra);
		
					pstmt =  conn.prepareStatement(sqlDesTra);
					pstmt.execute();
					rs.close();
		           
				}
				
				String sqlSrcTra = "INSERT INTO grt_admin.TT_SRC_TRA(SRC_FID, SRC_NID, SRC_NOM,SRC_TPI,SRC_RFN,SRC_FIN,SRC_NBL,SRC_BLB) " +
					     "VALUES ('"+nextVal+"',grt_admin.TT_SRC_TRA_SEQ.NEXTVAL,'"+nombreArchivoEncritado+"'," +
					     		"'"+tipoArchivo+"','"+nombreArchivo+"',to_date('"+fechaDocumento+"','DD/MM/YYYY'),'"+n_boleta+"',?)";
				
	
				pstmt =  conn.prepareStatement(sqlSrcTra);
				((OraclePreparedStatement) pstmt).setBLOB(1, GIF_GIF);
				Log.debug("ConsultaBaseDatos :: insertarFacturaBoleta -> sqlSrcTra: " + sqlSrcTra);
				pstmt.execute();
				BasePeer.commitTransaction(conn);
				
				pstmt.close();
				conn.close();
				
			}
		} catch (SQLException e) {
			BasePeer.rollBackTransaction(conn);
			Log.debug("JDBCConnection.connect: [" + e.toString() + "]");
		} 
		
		Log.debug("ConsultaBaseDatos :: insertarFacturaBoleta -> FIN");
		return msjSalida;
	}
	
	
	
	private String getIdDesTra(String numGtia, String numEvento) throws Exception {

	
		DBConnection conn 			= null;
		PreparedStatement pstmt 	= null;
		ResultSet rs  				= null;
		String idDestra 			= "";
		
		try {
			
			Log.debug("ConsultaBaseDatos :: getIdDesTra -> INICIO");
			conn = BasePeer.beginTransaction(Constants.BASE_GAR);
			
			String sql = "SELECT DES_NID FROM grt_admin.TT_DES_TRA WHERE DES_NCN = '"+numGtia+"' AND DES_NEV = '"+numEvento+"' AND DES_SIS = 'GAR' AND DES_DES = 'Datos_tas_boleta' AND DES_TIP = 'Datos_tas_boleta'  ORDER BY DES_NID DESC";
			Log.debug("ConsultaBaseDatos :: getIdDesTra -> sql: " + sql);
			pstmt =  conn.prepareStatement(sql);
			rs  = pstmt.executeQuery();
			
			if (rs.next()) {
				Log.debug("ConsultaBaseDatos :: getIdDesTra -> rs.getString(DES_NID) " + rs.getString("DES_NID"));
				idDestra = rs.getString("DES_NID");
				
			}
			
			BasePeer.commitTransaction(conn);
			
		} catch (SQLException e) {
			BasePeer.rollBackTransaction(conn);
			Log.debug("JDBCConnection.connect: [" + e.toString() + "]");
		} finally{
			try 
	        {
				rs.close();
	            
	        }
	        catch (SQLException ee)
	        {
	            ee.printStackTrace(); 
	        }
			try 
	        {
				pstmt.close();
	            
	        }
	        catch (SQLException ee)
	        {
	            ee.printStackTrace(); 
	        }
			try 
	        {
				conn.close();
	            
	        }
	        catch (SQLException ee)
	        {
	            ee.printStackTrace(); 
	        }
		}
		
		Log.debug("ConsultaBaseDatos :: getIdDesTra -> FIN");
	
		return idDestra;
		
	}
	
	public String getFechaTasacion(String numEvento) throws Exception {

		
		DBConnection conn 			= null;
		PreparedStatement pstmt 	= null;
		ResultSet rs  				= null;
		String fechaTasacion 		= "";
		
		try {
			
			Log.debug("ConsultaBaseDatos :: getFechaTasacion -> INICIO");
			conn = BasePeer.beginTransaction(Constants.BASE_GAR);
			
			String sql = "SELECT SUBSTR(ANX_DAT,549,8) F_TASACION FROM TT_ANX WHERE ANX_DAX = 'BASE' AND ANX_NEV = '"+numEvento+"'";
			Log.debug("ConsultaBaseDatos :: getFechaTasacion -> sql: " + sql);
			pstmt =  conn.prepareStatement(sql);
			rs  = pstmt.executeQuery();
			
			if (rs.next()) {
				Log.debug("ConsultaBaseDatos :: getFechaTasacion -> F_TASACION" + rs.getString("F_TASACION"));
				fechaTasacion = cambiarFormatoFecha(rs.getString("F_TASACION"));				
			}
			
			
			
			BasePeer.commitTransaction(conn);
			
		} catch (SQLException e) {
			BasePeer.rollBackTransaction(conn);
			Log.debug("JDBCConnection.connect: [" + e.toString() + "]");
		} finally{
			try 
	        {
				rs.close();
	            
	        }
	        catch (SQLException ee)
	        {
	            ee.printStackTrace(); 
	        }
			try 
	        {
				pstmt.close();
	            
	        }
	        catch (SQLException ee)
	        {
	            ee.printStackTrace(); 
	        }
			try 
	        {
				conn.close();
	            
	        }
	        catch (SQLException ee)
	        {
	            ee.printStackTrace(); 
	        }
		}
		
		Log.debug("ConsultaBaseDatos :: getFechaTasacion -> FIN");
	
		return fechaTasacion;
		
	}
	
	
	private String cambiarFormatoFecha(String fecha){
		
		
		Log.debug("ConsultaBaseDatos :: cambiarFormatoFecha -> INICIO");
		Log.debug("ConsultaBaseDatos :: fecha -> "+fecha);
		String fechaFormateada = "";
		SimpleDateFormat dt = new SimpleDateFormat("yyyyMMdd"); 
		Date date;
		try {
			date = dt.parse(fecha);
			Log.debug("ConsultaBaseDatos :: date -> "+date);
			SimpleDateFormat dt1 = new SimpleDateFormat("dd/MM/yyyy");
			fechaFormateada  = dt1.format(date);
			
		} catch (ParseException e) {

			fechaFormateada  = "00/00/0000";
		} 
		
		Log.debug("ConsultaBaseDatos :: fechaFormateada -> "+fechaFormateada);
		Log.debug("ConsultaBaseDatos :: cambiarFormatoFecha -> FIN");
		return fechaFormateada;
		
	}
	
	public void eliminarFacturaBoleta(RunData data, Context context) throws Exception {
		
		DBConnection conn 			= null;
		PreparedStatement pstmt 	= null;
		
		
		try {
			
			Log.debug("ConsultaBaseDatos :: eliminarFacturaBoleta -> INICIO");
			conn = BasePeer.beginTransaction(Constants.BASE_GAR);
			
			
			String numEvento 	= data.getParameters().getString("num_evento", "");
			String numGtia 		= data.getParameters().getString("num_gtia", "");
			String srcFid 		= data.getParameters().getString("srcFid", "");
			String srcNid 		= data.getParameters().getString("srcNid", "");

			
			Log.debug("eliminarFacturaBoleta :: numGtia -> " 			+ numGtia);
			Log.debug("eliminarFacturaBoleta :: numEvento -> " 			+ numEvento);
			Log.debug("eliminarFacturaBoleta :: srcFid -> "				+ srcFid);
			Log.debug("eliminarFacturaBoleta :: srcNid -> "				+ srcNid);
			
			String sqlDelSrc = "DELETE grt_admin.TT_SRC_TRA WHERE SRC_FID = '"+srcFid+"' AND SRC_NID = '"+srcNid+"'";
			Log.debug("ConsultaBaseDatos :: eliminarFacturaBoleta -> sqlSrcTra: " + sqlDelSrc);
			pstmt =  conn.prepareStatement(sqlDelSrc);
			pstmt.execute(sqlDelSrc);
			BasePeer.commitTransaction(conn);

			
//			String sqlDelDes = "DELETE grt_admin.TT_DES_TRA WHERE DES_NCN = '"+numGtia+"' AND DES_NEV = '"+numEvento+"' AND DES_NID = '"+srcFid+"' ";
//			Log.debug("ConsultaBaseDatos :: eliminarFacturaBoleta -> sqlDelDes: " + sqlDelDes);
//			pstmt =  conn.prepareStatement(sqlDelDes);
//			pstmt.execute();
//			BasePeer.commitTransaction(conn);
			
		} catch (SQLException e) {
			BasePeer.rollBackTransaction(conn);
			Log.debug("JDBCConnection.connect: [" + e.toString() + "]");
		} catch (Exception e) {
			BasePeer.rollBackTransaction(conn);
			Log.debug("Exception generica - Realizo rollbackTransaction");
		} finally{			
			try 
            {
				pstmt.close();
                
            }
            catch (SQLException ee)
            {
                ee.printStackTrace(); 
            }
			try 
            {
				conn.close();
                
            }
            catch (SQLException ee)
            {
                ee.printStackTrace(); 
            }
		}
		
		Log.debug("ConsultaBaseDatos :: eliminarFacturaBoleta -> FIN");
	}
	




	
	public ValorMonedaBean valorUF(String fechaEvento) throws Exception {
	    DBConnection conn 		= null;
	    PreparedStatement pstmt = null;
	    ResultSet rs  			= null;
	    ValorMonedaBean uf 		= null;
	    
		try {
			Log.debug("ConsultasBaseDatos::valorUF");
			conn = BasePeer.beginTransaction(Constants.BASE_GAR);
			
			//String fechaEvento 		= data.getParameters().getString("fechaEvento", "");
			//fechaEvento = "15/12/2016";
			Log.debug("ConsultasBaseDatos::valorUF - fechaEvento: " + fechaEvento);
			
			String sql 	= 	" SELECT GLS_CORTA,GLS_LARGA,VALOR FROM BCT_VIEW_TABVAL WHERE " +
							//" FEC_VALIDEZ  = '"+fechaEvento+"' AND " +
							" FEC_VALIDEZ  = to_date('"+fechaEvento+"','DD/MM/YYYY') AND " +							 
							" COD_TIPOVALOR = "+Constantes.COD_TIPO_VALOR_UF+" " +
						    " AND COD_GENERICO  = "+Constantes.COD_GENERICO+" " +
						    " AND COD_TABLA = '"+Constantes.COD_MONEDA+"' " +
						    " ORDER BY FEC_VALIDEZ DESC";
			
			Log.debug("consultaGastoTasador:: sql -> " 	+ sql);
			
			pstmt =  conn.prepareStatement(sql);
			rs  = pstmt.executeQuery();
	
			if (rs.next()) {
				uf = new ValorMonedaBean();
				uf.setGlsCorta(rs.getString("GLS_CORTA"));
				uf.setGlsLarga(rs.getString("GLS_LARGA"));
				uf.setValor(rs.getString("VALOR"));
			}
			
			Log.debug("ConsultasBaseDatos::valorUF - glosa corta: " + uf.getGlsCorta());
			Log.debug("ConsultasBaseDatos::valorUF - glosa larga: " + uf.getGlsLarga());
			Log.debug("ConsultasBaseDatos::valorUF - valor: " + uf.getValor());
			
			BasePeer.commitTransaction(conn);  
			
		} catch (SQLException e) {
			BasePeer.rollBackTransaction(conn);
			Log.debug("JDBCConnection.connect: [" + e.toString() + "]");
		} finally{
			try 
	        {
				rs.close();
	        }
	        catch (SQLException ee)
	        {
	            ee.printStackTrace(); 
	        }
			try 
	        {
				pstmt.close();
	        }
	        catch (SQLException ee)
	        {
	            ee.printStackTrace(); 
	        }
			try 
	        {
				conn.close();
	        }
	        catch (SQLException ee)
	        {
	            ee.printStackTrace(); 
	        }
			
			
		}
		
		return uf;
	}







	
	public List<BoletaBean> consultaFacturaBoleta(RunData data, Context context) throws Exception {
        List<BoletaBean> listaBoletas = new ArrayList<BoletaBean>();
        DBConnection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs  = null;
		try {
			Log.debug("ConsultaBaseDatos :: consultaFacturaBoleta - INICIO");
			conn = BasePeer.beginTransaction(Constants.BASE_GAR);
			
			String numEvento = data.getParameters().getString("num_evento", "");
			String numGtia = data.getParameters().getString("num_gtia", "");
			
			Log.debug("consultaFacturaBoleta :: numGtia -> " 			+ numGtia);
			Log.debug("consultaFacturaBoleta :: numEvento -> " 			+ numEvento);
			
			String sql = "SELECT SRC.SRC_FID,SRC.SRC_NID,SRC.SRC_NOM,SRC.SRC_TPI,SRC.SRC_BLB,SRC.SRC_DES,SRC.SRC_RFN,SRC.SRC_FIN,SRC.SRC_NBL " +
					     " FROM grt_admin.TT_DES_TRA DES INNER JOIN grt_admin.TT_SRC_TRA SRC ON (DES.DES_NID=SRC.SRC_FID)" +
					     " WHERE DES.DES_NEV = '"+numEvento+"' AND DES.DES_NCN = '"+numGtia+"' AND DES.DES_TIP = 'Datos_tas_boleta'";
			Log.debug("consultaFacturaBoleta :: SQL -> " + sql);
			
			pstmt =  conn.prepareStatement(sql);

			rs  = pstmt.executeQuery();
			
			while (rs.next()) {
				Log.debug(" SRC_FID -> "+rs.getString("SRC_FID"));
				Log.debug(" SRC_NID -> "+rs.getString("SRC_NID"));
				Log.debug(" SRC_NOM -> "+rs.getString("SRC_NOM"));
				
				Log.debug(" SRC_TPI -> "+rs.getString("SRC_TPI"));
				Log.debug(" SRC_BLB -> "+rs.getString("SRC_BLB"));
				Log.debug(" SRC_DES -> "+rs.getString("SRC_DES"));
				
				Log.debug(" SRC_RFN -> "+rs.getString("SRC_RFN"));
				Log.debug(" SRC_FIN -> "+rs.getDate("SRC_FIN"));
				Log.debug(" SRC_NBL -> "+rs.getString("SRC_NBL"));

				BoletaBean boleta = new BoletaBean();
				boleta.setIdFID(rs.getLong("SRC_FID"));
				boleta.setIdNID(rs.getLong("SRC_NID"));
				boleta.setBlobArchivo(rs.getBlob("SRC_BLB"));
				
				boleta.setDescripcion(rs.getString("SRC_DES"));
				boleta.setFecha(rs.getDate("SRC_FIN"));
				boleta.setNombreArchivo(rs.getString("SRC_RFN"));
				
				boleta.setNumeroBoleta(rs.getString("SRC_NBL"));
				boleta.setTipoArchivo(rs.getString("SRC_TPI"));
				boleta.setNombreCodificadoArchivo(rs.getString("SRC_NOM"));
				
				
				listaBoletas.add(boleta);
			}
			BasePeer.commitTransaction(conn); 
				
		} catch (SQLException e) {
			BasePeer.rollBackTransaction(conn);
			Log.debug("JDBCConnection.connect: [" + e.toString() + "]");
		} finally{
			try 
            {
				rs.close();
                
            }
            catch (SQLException ee)
            {
                ee.printStackTrace(); 
            }
			try 
            {
				pstmt.close();
                
            }
            catch (SQLException ee)
            {
                ee.printStackTrace(); 
            }
			try 
            {
				conn.close();
                
            }
            catch (SQLException ee)
            {
                ee.printStackTrace(); 
            }
			
		}
		Log.debug("ConsultaBaseDatos :: consultaFacturaBoleta - FIN");
		return listaBoletas;
	}
	
	public GastosBean consultaGastoTasador(RunData data, Context context,String tipoGasto) throws Exception {
        DBConnection conn 		= null;
        PreparedStatement pstmt = null;
        ResultSet rs  = null;
        GastosBean gasto = new GastosBean();
        
		try {
			Log.debug("ConsultasBaseDatos::consultaGastoTasador - INICIO");
			conn = BasePeer.beginTransaction(Constants.BASE_GAR);
			
			String numEvento = data.getParameters().getString("num_evento", "");
			String numGtia = data.getParameters().getString("num_gtia", "");
			
			Log.debug("consultaGastoTasador :: numGtia -> " 			+ numGtia);
			Log.debug("consultaGastoTasador :: numEvento -> " 			+ numEvento);
			Log.debug("consultaGastoTasador :: tipoGasto -> " 			+ tipoGasto);
			
			String sql 	= "SELECT DVC_NCN,DVC_NEV,DVC_NID,DVC_MNT,DVC_NDT,VCT_NID FROM grt_admin.TT_DVC_TRA WHERE DVC_NCN = '"+numGtia+"' AND DVC_NEV = '"+numEvento+"' AND VCT_NID = '"+tipoGasto+"' ORDER BY DVC_NID DESC";
			
			Log.debug("consultaGastoTasador:: sql -> " 	+ sql);

			//1 HONORARIOS, 2 PEAJE, 3 KILOMETROS, 4 OTROS
			pstmt =  conn.prepareStatement(sql);
			rs  = pstmt.executeQuery();
			
			Log.debug("consultaGastoTasador:: antes de obtener valor uf");
			String fechaTasacion = this.getFechaTasacion(numEvento);
			ValorMonedaBean uf = this.valorUF(fechaTasacion);
			Log.debug("consultaGastoTasador:: despues de obtener valor uf");
			if (rs.next()) {
				gasto = new GastosBean();
				gasto.setNumeroGarantia(rs.getString("DVC_NCN"));
				gasto.setNumeroEvento(rs.getString("DVC_NEV"));
				gasto.setDetalle(rs.getString("DVC_NDT"));
				gasto.setDvcNID(rs.getString("DVC_NID"));
				gasto.setMonto(rs.getString("DVC_MNT"));
				gasto.setVctNID(rs.getString("VCT_NID"));				
				gasto.setMontoEnUF(obtenerMontoEnUF(gasto.getMonto(),uf.getValor()));
			}
			
			Log.debug("consultaGastoTasador:: llenar datos rs.next");
			Log.debug("consultaGastoTasador :: gasto.getDetalle() -> " 			+ gasto.getDetalle());
			Log.debug("consultaGastoTasador :: gasto.getDvcNID() -> " 			+ gasto.getDvcNID());
			Log.debug("consultaGastoTasador :: gasto.getMonto() -> " 			+ gasto.getMonto());
			Log.debug("consultaGastoTasador :: gasto.getMontoEnUF() -> " 			+ gasto.getMontoEnUF());
			Log.debug("consultaGastoTasador :: gasto.getNumeroEvento() -> " 	+ gasto.getNumeroEvento());
			Log.debug("consultaGastoTasador :: gasto.getNumeroGarantia() -> " 	+ gasto.getNumeroGarantia());
			Log.debug("consultaGastoTasador :: gasto.getVctNID() -> " 			+ gasto.getVctNID());
		
			BasePeer.commitTransaction(conn);  
			
		} catch (SQLException e) {
			BasePeer.rollBackTransaction(conn);
			Log.debug("JDBCConnection.connect: [" + e.toString() + "]");
		} finally{
			try 
            {
				rs.close();
            }
            catch (SQLException ee)
            {
                ee.printStackTrace(); 
            }
			try 
            {
				pstmt.close();
            }
            catch (SQLException ee)
            {
                ee.printStackTrace(); 
            }
			try 
            {
				conn.close();
            }
            catch (SQLException ee)
            {
                ee.printStackTrace(); 
            }
			
			
		}
		Log.debug("ConsultasBaseDatos::consultaGastoTasador - FIN");
		return gasto;
	}
	


	private String obtenerMontoEnUF(String monto, String valorUF) {
		Log.debug("ConstultaBaseDatos :: obtenerMontoEnUF -> INICIO");
		
		String montoEnUF = "0";
		float valorEnUF = 0;		
		try {
			Log.debug("Float.valueOf(monto)/Float.valueOf(valorUF) -> " + Float.valueOf(monto)/Float.valueOf(valorUF));
			valorEnUF = Float.valueOf(monto)/Float.valueOf(valorUF);
			
			DecimalFormat df = new DecimalFormat("##.####");
			//df.setRoundingMode(RoundingMode.UP);			
			Log.debug("valorEnUF -> " + df.format(valorEnUF));
			montoEnUF = df.format(valorEnUF);
			Log.debug("String.valueOf(valorEnUF) -> " + String.valueOf(valorEnUF));
			Log.debug("montoEnUF -> " + montoEnUF);
		} catch (Exception e) {
			montoEnUF = "0";
			Log.debug("consultaGastoTasador :: obtenerMontoEnUF -> ERROR al calcular monto en UF (por defecto retorna 0)");
			
		}
		
		Log.debug("ConstultaBaseDatos :: obtenerMontoEnUF -> FIN");
		return montoEnUF.replace(".",",");
		
	}

	//hs 
	
	
	
	private String tipoArchivoAbreviado(String contentType) {

		String nombreTipoAbreviado = "";
		
		if(contentType==null || "".equals(contentType)){
			nombreTipoAbreviado = "error";
		}else if(Constantes.CONTENT_TYPE_PDF.equals(contentType)){
			nombreTipoAbreviado = Constantes.EXTENSION_PDF;//"pdf";
		}
		
		return nombreTipoAbreviado;
	}


	static void fillBlob(BLOB blob, InputStream instream, long length)
			throws Exception {
		OutputStream outstream = blob.getBinaryOutputStream();
		int c;
		while ((c = instream.read()) != -1)
			outstream.write(c);
		outstream.close();
		instream.close();
	}
	
	private static byte[] dumpBlob(BLOB blob) throws Exception {
		InputStream isRead = blob.getBinaryStream();
		int lenBuf = (int) blob.length();
		byte[] buffer = new byte[lenBuf];
		isRead.read(buffer);
		isRead.close();
		return buffer;
	}

}