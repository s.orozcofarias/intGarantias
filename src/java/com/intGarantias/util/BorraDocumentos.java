package com.intGarantias.util;

import java.io.File;

import org.apache.turbine.util.Log;

public class BorraDocumentos {

	
	public void borrarArchivo(String nombreArchivo){
		
		 File archivoBorrar = new File(nombreArchivo);
		 Log.debug("archivoBorrar.getAbsolutePath() : " + archivoBorrar.getAbsolutePath());
		 if(archivoBorrar.delete())
			 Log.debug("Borrado Ok " + nombreArchivo);
	}
}
