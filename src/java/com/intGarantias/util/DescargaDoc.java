package com.intGarantias.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletResponse;

import org.apache.turbine.util.Log;
import org.apache.turbine.util.RunData;

public class DescargaDoc {
	
	public void descargarArchivo(RunData data, File archivoTemporal) throws IOException{
		HttpServletResponse response = data.getResponse();
		File archivoDescarga = archivoTemporal;
		String file = archivoDescarga.getName();



		InputStream in = new FileInputStream(archivoDescarga.getAbsolutePath());
		Log.debug("file : " + archivoDescarga.getName());
		Log.debug("archivoDescarga.getAbsolutePath() : " + archivoDescarga.getAbsolutePath());


		//	      response.setContentType("application/octet-stream");	
		response.setContentType("application/vnd.ms-excel");	
		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename=\""+  file + "\"";
		response.setHeader(headerKey, headerValue);

		OutputStream servletoutputstream = response.getOutputStream();
		int BUFFER_SIZE = 1024;
		byte[] buffer = new byte[BUFFER_SIZE];
		int sizeRead = 0;
		while ((sizeRead = in.read(buffer)) >= 0) { //leyendo del host

			servletoutputstream.write(buffer, 0, sizeRead); //escribiendo para el navegador
		}

		in.close();			
		servletoutputstream.close();
	}	
	


}
