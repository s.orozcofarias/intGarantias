package com.intGarantias.util;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;

public class CrearExcel {

	public static HSSFRow createRowFrom(HSSFSheet sheet, HSSFRow rowSource, int index)
    {
        HSSFCell cellSource = null;
        HSSFRow row = null;
        HSSFCell cell = null;

        row = sheet.createRow(index);
        for (short i = 0; i <= rowSource.getLastCellNum(); i++)
        {
            cellSource = rowSource.getCell(i);	            	           
            if (cellSource != null)
            {
            	cell = row.createCell(i);
                cell.setCellStyle(cellSource.getCellStyle());
                
                cell.setCellType(cellSource.getCellType());
                if (cellSource.getCellType() == cellSource.CELL_TYPE_STRING)
                {
                    if (cellSource.getStringCellValue().length() > 0)
                        cell.setCellValue(cellSource.getStringCellValue());
                        
                }
                else if (cellSource.getCellType() == cellSource.CELL_TYPE_NUMERIC)
                    cell.setCellValue(cellSource.getNumericCellValue());
            }
        }
        return row;
    }
		
	
}
