// Source File Name:   Constants.java

package com.intGarantias.util;

public class Constantes
{
  //-------------------------------------------------------------------------------------------
	public static final String VCT_NID_HONORARIOS 	= "1"; //honorario
	public static final String VCT_NID_PEAJES 		= "2"; //peaje
	public static final String VCT_NID_KILOMETROS 	= "3"; //kilometros
	public static final String VCT_NID_OTROS 		= "4"; //otros
	public static final String COD_TIPO_VALOR_UF 	= "1";
	public static final String COD_GENERICO  		= "998"; 
	public static final String COD_MONEDA  			= "MONEDAS";
	public static final String CONTENT_TYPE_PDF 	= "application/pdf";
	public static final String EXTENSION_PDF	 	= "pdf";													
	
	
			
  //-------------------------------------------------------------------------------------------
}